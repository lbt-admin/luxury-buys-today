<?php
/**
 * The template used for displaying page content in page.php
 *
 * @package Novus
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
	</header><!-- .entry-header -->

	<div class="entry-content">
		<?php if ( has_post_thumbnail() && get_theme_mod( 'featured_image_in_header', true ) == false ) : ?>
			<?php $image_url = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' ); ?>
			<div class="entry-image"><a href="<?php echo esc_url( $image_url[0] ); ?>"><?php the_post_thumbnail( 'post-thumb' ); ?></a></div>
		<?php endif; ?>
		<?php the_content(); ?>
		<?php
			wp_link_pages( array(
				'before' => '<div class="page-links">' . __( 'Pages:', 'novus' ),
				'after'  => '</div>',
			) );
		?>
	</div><!-- .entry-content -->

	<?php
		// WooCommerce check
		$is_woocommerce = false;
		if ( NOVUS_WOOCOMMERCE_ACTIVE ) {
		  $is_woocommerce = is_cart() || is_checkout() || is_account_page();
		}
		// Share post links
		if ( get_theme_mod( 'page_share_links', true ) && ! $is_woocommerce ) {
			novus_share_post();
		}
	?>

</article><!-- #post-## -->
