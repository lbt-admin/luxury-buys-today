<?php
/**
 * Customizer styles rendered in header.php in head tag
 *
 * @package Novus
 */

function novus_customizer_settings() {
?>
	<?php if ( ( ! function_exists( 'has_site_icon' ) || ! has_site_icon() ) && get_theme_mod( 'site_favicon', false ) ) : ?>
	  <link rel="shortcut icon" href="<?php echo esc_url( get_theme_mod( 'site_favicon', false ) ); ?>">
	<?php endif; ?>

	<script type="text/javascript">
		var novus = novus || {};
		<?php
		if ( get_theme_mod( 'article_layout' ) == 'masonry' ) : ?>
			novus.layout = 'masonry';
		<?php
		elseif ( get_theme_mod( 'article_layout' ) == 'masonry-full' ) : ?>
			novus.layout = 'masonry-full';
		<?php
		endif;
		if ( class_exists( 'Jetpack' ) && Jetpack::is_module_active( 'infinite-scroll' ) ) : ?>
			novus.jetpack = {
				active: true
			};
		<?php
		endif;
		if ( is_rtl() ) : ?>
			novus.rtl = true;
		<?php
		endif;
		?>
	</script>

	<style type="text/css">
	<?php
	if ( get_theme_mod( 'color_base', false ) ) : ?>
		body {
			color: <?php echo get_theme_mod( 'color_base', false ); ?>;
		}
	<?php
	endif;
	if ( get_theme_mod( 'color_theme_a', false ) ) : ?>
		.entry-meta,
		.entry-footer,
		.comment-meta .comment-date,
		.comment-meta .comment-awaiting-moderation,
		.site-info,
		.entry-author-content p,
		.format-gallery .entry-image span,
		.format-link .entry-image span,
		.widget_recent_entries .post-date,
		.taxonomy-description {
			color: <?php echo get_theme_mod( 'color_theme_a', false ); ?>;
		}
	<?php
	endif;
	if ( get_theme_mod( 'color_theme_b', false ) ) : ?>
		.widget-title,
		.page-title,
		.entry-title,
		.entry-content h1, .comment-content h1,
		.entry-content h2, .comment-content h2,
		.entry-content h3, .comment-content h3,
		.entry-content h4, .comment-content h4,
		.entry-content h5, .comment-content h5,
		.entry-content h6, .comment-content h6,
		.entry-content q, .comment-body q,
		.related-posts-title,
		.comments-title,
		.comment-reply-title {
			color: <?php echo get_theme_mod( 'color_theme_b', false ); ?>;
		}
	<?php
	endif;
	if ( get_theme_mod( 'color_theme_c', false ) ) : ?>
		.hentry:after,
		.page-content:after
		.comment-meta,
		.entry-share a {
			border-color: <?php echo get_theme_mod( 'color_theme_c', false ); ?>;
		}
	<?php
	endif;
	if ( get_theme_mod( 'color_theme_d', false ) ) : ?>
		.entry-title,
		.widget-title {
			border-color: <?php echo get_theme_mod( 'color_theme_d', false ); ?>;
		}
		.woocommerce span.onsale {
			background-color: <?php echo get_theme_mod( 'color_theme_d', false ); ?>;
		}
		.woocommerce ul.products li.product .price,
		.woocommerce div.product p.price,
		.woocommerce div.product span.price {
			color: <?php echo get_theme_mod( 'color_theme_d', false ); ?>;
		}
	<?php
	endif;
	if ( get_theme_mod( 'color_footer_bg', false ) ) : ?>
		.site-footer {
			background-color: <?php echo get_theme_mod( 'color_footer_bg', false ); ?>;
		}
	<?php
	endif;
	if ( get_theme_mod( 'color_content', false ) ) : ?>
		.layout-list .content-area,
		.layout-full-width .content-area,
		.layout-classic .content-area,
		.layout-sidebar-right .content-area,
		.woocommerce-page .content-area,
		.hentry--masonry .entry-wrap,
		.hentry--grid .entry-footer,
		.layout-masonry .page-header,
		.layout-grid .page-header,
		.featured-posts {
			background-color: <?php echo get_theme_mod( 'color_content', false ); ?>;
		}
	<?php
	endif;
	if ( get_theme_mod( 'color_content_shadow', false ) ) : ?>
		.layout-list .content-area,
		.layout-full-width .content-area,
		.layout-classic .content-area,
		.layout-sidebar-right .content-area,
		.hentry--masonry .entry-wrap,
		.hentry--grid .entry-wrap,
		.featured-posts {
			box-shadow: none;
		}
	<?php
	endif;
	if ( get_theme_mod( 'color_link', false ) ) : ?>
		a,
		.entry-title a {
			color: <?php echo get_theme_mod( 'color_link', false ); ?>;
		}
		.widget_tag_cloud a {
			background-color: <?php echo get_theme_mod( 'color_link', false ); ?>;
		}
		.widget_tag_cloud a:after {
			border-right-color: <?php echo get_theme_mod( 'color_link', false ); ?>;
		}
	<?php
	endif;
	if ( get_theme_mod( 'color_link_hover', false ) ) : ?>
		a:hover,
		.entry-title a:hover {
			color: <?php echo get_theme_mod( 'color_link_hover', false ); ?>;
		}
		.widget_tag_cloud a:hover {
			background-color: <?php echo get_theme_mod( 'color_link_hover', false ); ?>;
		}
		.widget_tag_cloud a:hover:after {
			border-right-color: <?php echo get_theme_mod( 'color_link_hover', false ); ?>;
		}
	<?php
	endif;
	if ( get_theme_mod( 'color_button', false ) ) : ?>
		.btn,
		a.btn,
		input.btn,
		input[type="button"],
		input[type="submit"],
		input[type="reset"],
		.more-link-wrap .more-link,
		.posts-navigation a,
		.woocommerce #respond input#submit,
		.woocommerce a.button,
		.woocommerce button.button,
		.woocommerce input.button,
		.woocommerce #respond input#submit.alt,
		.woocommerce a.button.alt,
		.woocommerce button.button.alt,
		.woocommerce input.button.alt {
			color: <?php echo get_theme_mod( 'color_button', false ); ?>;
		}
	<?php
	endif;
	if ( get_theme_mod( 'color_button_hover', false ) ) : ?>
		.btn:hover,
		a.btn:hover,
		input.btn:hover,
		input[type="button"]:hover,
		input[type="submit"]:hover,
		input[type="reset"]:hover,
		.more-link-wrap .more-link:hover,
		.posts-navigation a:hover,
		.woocommerce #respond input#submit:hover,
		.woocommerce a.button:hover,
		.woocommerce button.button:hover,
		.woocommerce input.button:hover,
		.woocommerce #respond input#submit.alt:hover,
		.woocommerce a.button.alt:hover,
		.woocommerce button.button.alt:hover,
		.woocommerce input.button.alt:hover {
			color: <?php echo get_theme_mod( 'color_button_hover', false ); ?>;
		}
	<?php
	endif;
	if ( get_theme_mod( 'color_button_background', false ) ) : ?>
		.btn,
		a.btn,
		input.btn,
		input[type="button"],
		input[type="submit"],
		input[type="reset"],
		.more-link-wrap .more-link,
		.posts-navigation a,
		.woocommerce #respond input#submit,
		.woocommerce a.button,
		.woocommerce button.button,
		.woocommerce input.button,
		.woocommerce #respond input#submit.alt,
		.woocommerce a.button.alt,
		.woocommerce button.button.alt,
		.woocommerce input.button.alt {
			background-color: <?php echo get_theme_mod( 'color_button_background', false ); ?>;
		}
	<?php
	endif;
	if ( get_theme_mod( 'color_button_background_hover', false ) ) : ?>
		.btn:hover,
		a.btn:hover,
		input.btn:hover,
		input[type="button"]:hover,
		input[type="submit"]:hover,
		input[type="reset"]:hover,
		.more-link-wrap .more-link:hover,
		.posts-navigation a:hover,
		.woocommerce #respond input#submit:hover,
		.woocommerce a.button:hover,
		.woocommerce button.button:hover,
		.woocommerce input.button:hover,
		.woocommerce #respond input#submit.alt:hover,
		.woocommerce a.button.alt:hover,
		.woocommerce button.button.alt:hover,
		.woocommerce input.button.alt:hover {
			background-color: <?php echo get_theme_mod( 'color_button_background_hover', false ); ?>;
		}
	<?php
	endif;
	if ( get_theme_mod( 'color_form_field_text', false ) ) : ?>
		input[type="text"],
		input[type="email"],
		input[type="tel"],
		input[type="password"],
		input[type="url"],
		input[type="date"],
		input[type="search"],
		textarea {
			color: <?php echo get_theme_mod( 'color_form_field_text', false ); ?>;
		}
	<?php
	endif;
	if ( get_theme_mod( 'color_form_field_border', false ) ) : ?>
		input[type="text"],
		input[type="email"],
		input[type="tel"],
		input[type="password"],
		input[type="url"],
		input[type="date"],
		input[type="search"],
		textarea {
			border-color: <?php echo get_theme_mod( 'color_form_field_border', false ); ?>;
		}
	<?php
	endif;
	if ( get_theme_mod( 'color_form_field_border_focus', false ) ) : ?>
		input[type="text"]:focus,
		input[type="email"]:focus,
		input[type="tel"]:focus,
		input[type="password"]:focus,
		input[type="url"]:focus,
		input[type="date"]:focus,
		input[type="search"]:focus,
		textarea:focus {
			border-color: <?php echo get_theme_mod( 'color_form_field_border_focus', false ); ?>;
		}
	<?php
	endif;
	if ( get_theme_mod( 'color_form_field_bg', false ) ) : ?>
		input[type="text"],
		input[type="email"],
		input[type="tel"],
		input[type="password"],
		input[type="url"],
		input[type="date"],
		input[type="search"],
		textarea {
			background-color: <?php echo get_theme_mod( 'color_form_field_bg', false ); ?>;
		}
	<?php
	endif;
	if ( get_theme_mod( 'color_form_field_bg_focus', false ) ) : ?>
		input[type="text"]:focus,
		input[type="email"]:focus,
		input[type="tel"]:focus,
		input[type="password"]:focus,
		input[type="url"]:focus,
		input[type="date"]:focus,
		input[type="search"]:focus,
		textarea:focus {
			background-color: <?php echo get_theme_mod( 'color_form_field_bg_focus', false ); ?>;
		}
	<?php
	endif;
	if ( get_theme_mod( 'color_nav_bar_bg', false ) ) : ?>
		.site-nav-bar,
		.site-nav-bar.site-nav-bar--regular,
		.tiny .site-nav-bar.site-nav-bar--sticky-transparent,
		.main-navigation ul ul {
			background: <?php echo get_theme_mod( 'color_nav_bar_bg', false ); ?>;
		}
		@media only screen and (max-width: 50.1875em) {
			.main-navigation {
				background: <?php echo get_theme_mod( 'color_nav_bar_bg', false ); ?>;
			}
		}
	<?php
	endif;
	if ( get_theme_mod( 'color_nav_bar_link', false ) ) : ?>
		.site-nav-bar a,
		.site-nav-bar a:hover,
		.menu-toggle,
		.search-toggle,
		.search-toggle:hover,
		.cart-contents,
		.cart-contents:hover {
			color: <?php echo get_theme_mod( 'color_nav_bar_link', false ); ?>;
		}
		.main-navigation li.menu-item-has-children>a:after {
			border-top-color: <?php echo get_theme_mod( 'color_nav_bar_link', false ); ?>;
		}
	<?php
	endif;
	if ( get_theme_mod( 'color_nav_bar_link_hover', false ) ) : ?>
		.main-navigation li:hover > a,
		.main-navigation li.focus > a {
			background-color: <?php echo get_theme_mod( 'color_nav_bar_link_hover', false ); ?>;
		}
	<?php
	endif;
	if ( get_theme_mod( 'color_nav_bar_border', false ) ) : ?>
		.site-nav-bar.site-nav-bar--sticky-transparent .container,
		.site-nav-bar.site-nav-bar--transparent .container {
			border-bottom-color: <?php echo get_theme_mod( 'color_nav_bar_border', false ); ?>;
		}
	<?php
	endif;
	if ( get_theme_mod( 'article_overlay_top', false ) && get_theme_mod( 'article_overlay_bottom', false ) ) : ?>
		.layout-masonry .entry-image:after,
		.layout-grid .entry-image:after,
		.layout-masonry .slider-gallery:after,
		.layout-grid .slider-gallery:after {
			background: linear-gradient(to bottom, <?php echo get_theme_mod( 'article_overlay_top', 'rgba(186,154,100,0.1)' ) ?> 0%, <?php echo get_theme_mod( 'article_overlay_bottom', 'rgba(94,76,42,0.4)' ) ?> 100%);
		}
	<?php
	endif;
	?>
	</style>

<?php
}

add_action( 'wp_head', 'novus_customizer_settings', 10000 );
