<?php
/**
 * The template part for displaying gallery post type.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package Novus
 */
?>

<?php $article_layout = get_theme_mod( 'article_layout', 'classic' ); ?>
<?php $layout_single = get_theme_mod( 'article_layout_single' ); ?>
<?php if ( $article_layout == 'classic' || $article_layout == 'sidebar-right' || $article_layout == 'full-width' || is_single() ) : ?>

	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<header class="entry-header">
			<div class="entry-meta">
				<?php novus_entry_meta(); ?>
			</div><!-- .entry-meta -->
			<?php the_title( sprintf( '<h1 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h1>' ); ?>
		</header><!-- .entry-header -->

		<footer class="entry-footer">
			<?php novus_posted_on(); ?>
			<?php novus_entry_footer(); ?>
		</footer><!-- .entry-footer -->

		<div class="entry-content">
			<?php
			$gallery = get_post_gallery(get_the_ID(), false);
			if ( empty( $gallery['type'] ) ) : // Show custom gallery slider ?>
				<div class="slider-gallery js-slider"<?php if ( is_rtl() ) : ?> dir="rtl"<?php endif; ?>>
				<?php
				if ( !empty( $gallery['ids']) ) {
					$image_ids = explode( ',', $gallery['ids'] );
					foreach ($image_ids as $image_id) :
						$post_thumb_size = ( $article_layout == 'full-width' || $layout_single == 'full-width' ) ? 'post-thumb-full-width-cropped' : 'post-thumb-cropped';
						$image = wp_get_attachment_image_src( $image_id, $post_thumb_size );
						$image_full = wp_get_attachment_image_src( $image_id, 'full' );
					?>
					<a href="<?php echo esc_url( $image_full[0] ); ?>"><img src="<?php echo esc_attr( $image[0] ); ?>" alt=""></a>
				<?php
					endforeach;
				}
				?>
				</div>
				<?php the_excerpt(); ?>
			<?php else : // Show Jetpack gallery ?>
				<?php the_content(); ?>
			<?php endif; ?>
		</div>

		<?php
			// Share post links
			if ( ( get_theme_mod( 'article_share_links', true ) && is_single() ) || ( get_theme_mod( 'archive_share_links', true ) && !is_single() ) ) {
				novus_share_post();
			}
		?>

	</article><!-- #post-## -->

<?php elseif ( $article_layout == 'grid' ) : ?>

	<article id="post-<?php the_ID(); ?>" <?php post_class( 'hentry--grid' ); ?>>
		<div class="entry-wrap">
			<?php	$gallery = get_post_gallery(get_the_ID(), false); ?>
			<div class="slider-gallery js-slider"<?php if ( is_rtl() ) : ?> dir="rtl"<?php endif; ?>>
			<?php
			if ( !empty( $gallery['ids']) ) {
				$image_ids = explode( ',', $gallery['ids'] );
				foreach ($image_ids as $image_id) :
					$image = wp_get_attachment_image_src( $image_id, 'post-thumb-cropped' );
					$image_full = wp_get_attachment_image_src( $image_id, 'full' );
				?>
				<a class="js-slider-link" href="<?php echo esc_url( $image_full[0] ); ?>"><img src="<?php echo esc_attr( $image[0] ); ?>" alt=""></a>
			<?php
				endforeach;
			}
			?>
			</div>
			<header class="entry-header">
				<?php if ( 'post' == get_post_type() ) : ?>
				<div class="entry-meta">
					<?php novus_entry_meta(); ?>
				</div><!-- .entry-meta -->
				<?php endif; ?>
				<?php the_title( sprintf( '<h1 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h1>' ); ?>
			</header><!-- .entry-header -->
			<footer class="entry-footer">
				<?php novus_posted_on(); ?>
				<?php novus_entry_footer( false ); ?>
			</footer><!-- .entry-footer -->
		</div>
	</article><!-- #post-## -->

<?php elseif ( $article_layout == 'masonry' || $article_layout == 'masonry-full' ) : ?>

	<article id="post-<?php the_ID(); ?>" <?php post_class( 'hentry--masonry' ); ?>>
		<div class="entry-wrap">
			<?php	$gallery = get_post_gallery(get_the_ID(), false); ?>
			<div class="slider-gallery js-slider"<?php if ( is_rtl() ) : ?> dir="rtl"<?php endif; ?>>
			<?php
			if ( !empty( $gallery['ids']) ) {
				$image_ids = explode( ',', $gallery['ids'] );
				foreach ($image_ids as $image_id) :
					$image = wp_get_attachment_image_src( $image_id, 'post-thumb-cropped' );
					$image_full = wp_get_attachment_image_src( $image_id, 'full' );
				?>
				<a class="js-slider-link" href="<?php echo esc_url( $image_full[0] ); ?>"><img src="<?php echo esc_attr( $image[0] ); ?>" alt=""></a>
			<?php
				endforeach;
			}
			?>
			</div>
			<header class="entry-header">
				<?php if ( 'post' == get_post_type() ) : ?>
				<div class="entry-meta">
					<?php novus_entry_meta(); ?>
				</div><!-- .entry-meta -->
				<?php endif; ?>
				<?php the_title( sprintf( '<h1 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h1>' ); ?>
			</header><!-- .entry-header -->
			<div class="entry-summary"><?php the_excerpt(); ?></div>
			<footer class="entry-footer">
				<?php novus_posted_on(); ?>
				<?php novus_entry_footer( false ); ?>
			</footer><!-- .entry-footer -->
		</div>
	</article><!-- #post-## -->

<?php else : ?>

	<article id="post-<?php the_ID(); ?>" <?php post_class( 'flag hentry--list' ); ?>>
		<div class="flag-img">
			<div class="entry-image">
			<?php if ( has_post_thumbnail() ) : ?>
				<?php the_post_thumbnail( 'thumbnail' ); ?>
			<?php else : ?>
				<span class="genericon genericon-gallery"></span>
			<?php endif; ?>
			</div>
		</div>
		<div class="flag-bd">
			<header class="entry-header">
				<div class="entry-meta">
					<?php novus_entry_meta(); ?>
				</div><!-- .entry-meta -->
				<?php the_title( sprintf( '<h1 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h1>' ); ?>
			</header><!-- .entry-header -->
			<footer class="entry-footer">
				<?php novus_posted_on(); ?>
				<?php novus_entry_footer(); ?>
			</footer><!-- .entry-footer -->
		</div>
	</article><!-- #post-## -->

<?php endif; ?>
