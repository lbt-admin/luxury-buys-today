<?php
/**
 * @package Novus
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<?php if ( has_post_thumbnail() && false === get_post_format() && get_theme_mod( 'featured_image_in_header', true ) == false ) : ?>
		<?php $image_url = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' ); ?>
		<div class="entry-image"><a href="<?php echo esc_url( $image_url[0] ); ?>"><?php the_post_thumbnail( 'post-thumb' ); ?></a></div>
	<?php endif; ?>

	<header class="entry-header">
		<div class="entry-meta">
			<?php novus_entry_meta(); ?>
		</div><!-- .entry-meta -->
		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
	</header><!-- .entry-header -->

	<footer class="entry-footer">
		<?php novus_posted_on(); ?>
		<?php novus_entry_footer(); ?>
	</footer><!-- .entry-footer -->

	<div class="entry-content">
		<?php the_content(); ?>
		<?php
			wp_link_pages( array(
				'before' => '<div class="page-links">' . __( 'Pages:', 'novus' ),
				'after'  => '</div>',
			) );
		?>
	</div><!-- .entry-content -->

	<?php
		// Share post links
		if ( get_theme_mod( 'article_share_links', true ) ) {
			novus_share_post();
		}
	?>

	<?php if ( get_theme_mod( 'author_info_display', true ) ) : ?>
		<?php get_template_part( 'partials/author-info' ); ?>
	<?php endif; ?>

</article><!-- #post-## -->
