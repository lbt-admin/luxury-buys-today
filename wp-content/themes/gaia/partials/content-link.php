<?php
/**
 * The template part for displaying link post type.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package Novus
 */
?>

<?php $article_layout = get_theme_mod( 'article_layout', 'classic' ); ?>
<?php if ( $article_layout == 'classic' || $article_layout == 'sidebar-right' || $article_layout == 'full-width' ) : ?>

	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<header class="entry-header">
			<div class="entry-meta">
				<?php novus_entry_meta(); ?>
			</div><!-- .entry-meta -->
			<div class="entry-image">
			<?php if ( has_post_thumbnail() ) : ?>
				<?php the_post_thumbnail( 'thumbnail' ); ?>
			<?php else : ?>
				<span class="genericon genericon-link"></span>
			<?php endif; ?>
			</div>
			<?php the_title( sprintf( '<h1 class="entry-title"><a href="%s" rel="bookmark">', esc_url( novus_get_link_url() ) ), '</a></h1>' ); ?>
		</header><!-- .entry-header -->

		<footer class="entry-footer">
			<?php novus_posted_on(); ?>
			<?php novus_entry_footer(); ?>
		</footer><!-- .entry-footer -->

		<div class="entry-content">
			<?php
				/* translators: %s: Name of current post */
				the_content( sprintf(
					__( 'Continue reading %s <span class="meta-nav">&rarr;</span>', 'novus' ),
					the_title( '<span class="screen-reader-text">"', '"</span>', false )
				) );
			?>
		</div>

		<?php
			// Share post links
			if ( ( get_theme_mod( 'article_share_links', true ) && is_single() ) || ( get_theme_mod( 'archive_share_links', true ) && !is_single() ) ) {
				novus_share_post();
			}
		?>

	</article><!-- #post-## -->

<?php elseif ( $article_layout == 'grid' ) : ?>

	<article id="post-<?php the_ID(); ?>" <?php post_class( 'hentry--grid' ); ?>>
		<div class="entry-wrap">
			<div class="entry-image">
			<?php if ( has_post_thumbnail() ) : ?>
				<?php the_post_thumbnail( 'post-thumb-cropped' ); ?>
			<?php else : ?>
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/post-thumb-cropped-link.svg" alt="thumbnail">
			<?php endif; ?>
			</div>
			<header class="entry-header">
				<div class="entry-meta">
					<?php novus_entry_meta(); ?>
				</div><!-- .entry-meta -->
				<?php the_title( sprintf( '<h1 class="entry-title"><a href="%s" rel="bookmark">', esc_url( novus_get_link_url() ) ), '</a></h1>' ); ?>
			</header><!-- .entry-header -->
			<footer class="entry-footer">
				<?php novus_posted_on(); ?>
				<?php novus_entry_footer( false ); ?>
			</footer><!-- .entry-footer -->
		</div>
	</article><!-- #post-## -->

<?php elseif ( $article_layout == 'masonry' || $article_layout == 'masonry-full' ) : ?>

	<article id="post-<?php the_ID(); ?>" <?php post_class( 'hentry--masonry' ); ?>>
		<div class="entry-wrap">
			<div class="entry-image">
			<?php if ( has_post_thumbnail() ) : ?>
				<?php the_post_thumbnail( 'post-thumb-cropped' ); ?>
			<?php else : ?>
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/post-thumb-cropped-link.svg" alt="thumbnail">
			<?php endif; ?>
			</div>
			<header class="entry-header">
				<div class="entry-meta">
					<?php novus_entry_meta(); ?>
				</div><!-- .entry-meta -->
				<?php the_title( sprintf( '<h1 class="entry-title"><a href="%s" rel="bookmark">', esc_url( novus_get_link_url() ) ), '</a></h1>' ); ?>
			</header><!-- .entry-header -->
			<div class="entry-summary">
				<?php the_excerpt(); ?>
			</div>
			<footer class="entry-footer">
				<?php novus_posted_on(); ?>
				<?php novus_entry_footer( false ); ?>
			</footer><!-- .entry-footer -->
		</div>
	</article><!-- #post-## -->

<?php else : ?>

	<article id="post-<?php the_ID(); ?>" <?php post_class( 'flag hentry--list' ); ?>>
		<div class="flag-img">
			<div class="entry-image">
			<?php if ( has_post_thumbnail() ) : ?>
				<?php the_post_thumbnail( 'thumbnail' ); ?>
			<?php else : ?>
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/thumb-link.svg" alt="thumbnail">
			<?php endif; ?>
			</div>
		</div>
		<div class="flag-bd">
			<header class="entry-header">
				<div class="entry-meta">
					<?php novus_entry_meta(); ?>
				</div><!-- .entry-meta -->
				<?php the_title( sprintf( '<h1 class="entry-title"><a href="%s" rel="bookmark">', esc_url( novus_get_link_url() ) ), '</a></h1>' ); ?>
			</header><!-- .entry-header -->
			<footer class="entry-footer">
				<?php novus_posted_on(); ?>
				<?php novus_entry_footer(); ?>
			</footer><!-- .entry-footer -->
		</div>
	</article><!-- #post-## -->

<?php endif; ?>
