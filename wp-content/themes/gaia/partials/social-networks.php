<?php
/**
 * Social networks links
 *
 * @package Novus
 */
?>
<div class="social-networks">
	<?php if ( get_theme_mod( 'sn_facebook', false ) ) : ?>
	<a href="<?php echo esc_url( get_theme_mod( 'sn_facebook', false ) ); ?>" class="genericon genericon-facebook"></a>
	<?php endif; ?>
	<?php if ( get_theme_mod( 'sn_twitter', false ) ) : ?>
	<a href="<?php echo esc_url( get_theme_mod( 'sn_twitter', false ) ); ?>" class="genericon genericon-twitter"></a>
	<?php endif; ?>
	<?php if ( get_theme_mod( 'sn_instagram', false ) ) : ?>
	<a href="<?php echo esc_url( get_theme_mod( 'sn_instagram', false ) ); ?>" class="genericon genericon-instagram"></a>
	<?php endif; ?>
	<?php if ( get_theme_mod( 'sn_pinterest', false ) ) : ?>
	<a href="<?php echo esc_url( get_theme_mod( 'sn_pinterest', false ) ); ?>" class="genericon genericon-pinterest"></a>
	<?php endif; ?>
	<?php if ( get_theme_mod( 'sn_linkedin', false ) ) : ?>
	<a href="<?php echo esc_url( get_theme_mod( 'sn_linkedin', false ) ); ?>" class="genericon genericon-linkedin-alt"></a>
	<?php endif; ?>
	<?php if ( get_theme_mod( 'sn_google_plus', false ) ) : ?>
	<a href="<?php echo esc_url( get_theme_mod( 'sn_google_plus', false ) ); ?>" class="genericon genericon-googleplus"></a>
	<?php endif; ?>
	<?php if ( get_theme_mod( 'sn_tumblr', false ) ) : ?>
	<a href="<?php echo esc_url( get_theme_mod( 'sn_tumblr', false ) ); ?>" class="genericon genericon-tumblr"></a>
	<?php endif; ?>
	<?php if ( get_theme_mod( 'sn_reddit', false ) ) : ?>
	<a href="<?php echo esc_url( get_theme_mod( 'sn_reddit', false ) ); ?>" class="genericon genericon-reddit"></a>
	<?php endif; ?>
	<?php if ( get_theme_mod( 'sn_dribbble', false ) ) : ?>
	<a href="<?php echo esc_url( get_theme_mod( 'sn_dribbble', false ) ); ?>" class="genericon genericon-dribbble"></a>
	<?php endif; ?>
	<?php if ( get_theme_mod( 'sn_github', false ) ) : ?>
	<a href="<?php echo esc_url( get_theme_mod( 'sn_github', false ) ); ?>" class="genericon genericon-github"></a>
	<?php endif; ?>
	<?php if ( get_theme_mod( 'sn_vimeo', false ) ) : ?>
	<a href="<?php echo esc_url( get_theme_mod( 'sn_vimeo', false ) ); ?>" class="genericon genericon-vimeo"></a>
	<?php endif; ?>
	<?php if ( get_theme_mod( 'sn_youtube', false ) ) : ?>
	<a href="<?php echo esc_url( get_theme_mod( 'sn_youtube', false ) ); ?>" class="genericon genericon-youtube"></a>
	<?php endif; ?>
</div>
