<?php
/**
 * The sidebar containing the footer widget area.
 *
 * @package Novus
 */

if ( ! is_active_sidebar( 'footer' ) ) {
  return;
}
?>

<div class="widget-area-footer">
  <div class="container">
    <div class="widget-area-footer-wrap">
      <?php dynamic_sidebar( 'footer' ); ?>
    </div>
  </div>
</div>
