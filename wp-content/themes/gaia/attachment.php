<?php
/**
 * The template for displaying single attachment.
 *
 * @package Novus
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<?php while ( have_posts() ) : the_post(); ?>

      <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
        <header class="entry-header">
          <?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
        </header><!-- .entry-header -->
        <div class="entry-content">
          <?php the_content(); ?>
          <p><?php echo wp_get_attachment_image( get_the_ID(), 'large' ); ?></p>
          <p class="attachment-info"><?php _e ( 'Downloads:', 'novus'); ?>
          <?php
            $images = array();
            $image_sizes = get_intermediate_image_sizes();
            array_push( $image_sizes, 'full' );
            foreach( $image_sizes as $image_size ) {
              $image = wp_get_attachment_image_src( get_the_ID(), $image_size );
              $name = $image_size;
              $images[] = '<a href="' . $image[0] . '">' . $name . '</a>';
            }
            echo implode( ' / ', $images );
          ?>
          </p>
        </div>
      </article>

		<?php endwhile; // end of the loop. ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>
