<?php
/**
 * The sidebar containing the full width footer widget area.
 *
 * @package Novus
 */

if ( ! is_active_sidebar( 'footer-full-width' ) ) {
	return;
}
?>

<div class="widget-area-footer-full-width">
  <?php dynamic_sidebar( 'footer-full-width' ); ?>
</div>
