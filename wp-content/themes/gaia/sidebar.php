<?php
/**
 * The sidebar containing the main widget area.
 *
 * @package Novus
 */
$layout = get_theme_mod( 'article_layout' );
$layouts = array( 'grid', 'masonry', 'masonry-full', 'full-width' );
$is_layout = in_array( $layout, $layouts );
$layout_single = get_theme_mod( 'article_layout_single' );
if ( ( $is_layout && is_home() ) || ( $is_layout && is_archive() ) || ( $is_layout && is_search() ) || ( $layout_single == 'full-width' && is_singular() ) ) {
  return;
}
if ( ! is_active_sidebar( 'sidebar-1' ) ) {
	return;
}
// WooCommerce check
if ( NOVUS_WOOCOMMERCE_ACTIVE ) {
  if ( is_cart() || is_checkout() || is_account_page() ) {
    return;
  }
}
?>

<div id="secondary" class="widget-area" role="complementary">
	<?php dynamic_sidebar( 'sidebar-1' ); ?>
</div><!-- #secondary -->
