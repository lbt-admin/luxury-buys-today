<?php
/**
 * The template for displaying all single posts.
 *
 * @package Novus
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<?php while ( have_posts() ) : the_post(); ?>

			<?php if ( has_post_format( 'gallery' ) ) : ?>
				<?php get_template_part( 'partials/content', 'gallery' ); ?>
			<?php else : ?>
				<?php get_template_part( 'partials/content', 'single' ); ?>
			<?php endif; ?>

			<?php if ( get_theme_mod( 'related_posts_display', true ) ) novus_related_posts(); ?>

			<?php
				// If comments are open or we have at least one comment, load up the comment template
				if ( comments_open() || get_comments_number() ) :
					comments_template();
				endif;
			?>

		<?php endwhile; // end of the loop. ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>
