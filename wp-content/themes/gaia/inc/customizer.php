<?php
/**
 * Novus Theme Customizer
 *
 * @package Novus
 */

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer-controls/tags-dropdown.php';
require get_template_directory() . '/inc/customizer-controls/color-alpha.php';

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function novus_customize_register( $wp_customize ) {
  $wp_customize->get_setting( 'blogname' )->transport            = 'postMessage';
  $wp_customize->get_setting( 'blogdescription' )->transport     = 'postMessage';
  $wp_customize->get_setting( 'header_textcolor' )->transport    = 'postMessage';


  /**
   * Panels
   */
  $wp_customize->add_panel( 'advanced_panel', array (
    'title' => __( 'Advanced Options', 'novus' ),
  ));


  /**
   * Sections
   */
  $wp_customize->add_section( 'novus_header', array(
    'title' => __( 'Header', 'novus' ),
    'priority' => 10,
  ) );
  $wp_customize->add_section( 'novus_colors_nav_bar', array(
    'title' => __( 'Colors: Nav Bar', 'novus' ),
    'priority' => 41,
  ) );
  $wp_customize->add_section( 'novus_colors_article', array(
    'title' => __( 'Colors: Article', 'novus' ),
    'priority' => 42,
  ) );
  $wp_customize->add_section( 'novus_colors_buttons_forms', array(
    'title' => __( 'Colors: Buttons and Forms', 'novus' ),
    'priority' => 43,
  ) );
  $wp_customize->add_section( 'novus_social_networks', array(
    'title' => __( 'Social Networks', 'novus' ),
    'priority' => 86,
    'description' => __( 'Insert full links to your social network profiles. <br>Example: http://twitter.com/username', 'novus' ),
  ) );
  $wp_customize->add_section( 'novus_featured_posts', array(
    'title' => __( 'Featured Posts', 'novus' ),
    'priority' => 84,
  ) );
  $wp_customize->add_section( 'novus_article_display', array(
    'title' => __( 'Article Display', 'novus' ),
    'priority' => 80,
  ) );
  $wp_customize->add_section( 'novus_comments', array(
    'title' => __( 'Comments Display', 'novus' ),
    'priority' => 82,
  ) );
  // $wp_customize->add_section( 'novus_jetpack', array(
  //   'title' => __( 'Jetpack', 'novus' ),
  //   'priority' => 95,
  // ) );
  $wp_customize->add_section( 'novus_misc', array(
    'title' => __( 'Miscellaneous', 'novus' ),
    'priority' => 100,
  ) );
  $wp_customize->add_section( 'novus_header_footer', array(
    'title' => __( 'Header and Footer Scripts', 'novus' ),
    'panel'    => 'advanced_panel',
    'priority' => 10,
  ) );


  // SECTION - Site title and tagline

  // Show tagline
  $wp_customize->add_setting( 'header_tagline_show' , array(
    'default'           => true,
    'transport'         => 'postMessage',
    'sanitize_callback' => 'novus_sanitize_checkbox',
  ) );
  $wp_customize->add_control( new WP_Customize_Control(
    $wp_customize,
    'header_tagline_show', array(
      'label'    => __( 'Display Tagline', 'novus' ),
      'section'  => 'title_tagline',
      'settings' => 'header_tagline_show',
      'type'     => 'checkbox',
    )
  ) );

  // Custom logo
  $wp_customize->add_setting( 'site_logo' , array(
    'default'           => false,
    'sanitize_callback' => 'novus_sanitize_image',
  ) );
  $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize,
    'site_logo', array(
      'label'      => __( 'Upload Logo', 'novus' ),
      'section'    => 'title_tagline',
      'settings'   => 'site_logo',
    )
  ) );

  if ( ! function_exists( 'has_site_icon' ) ) { // For WordPress lower than 4.3
    // Favicon
    $wp_customize->add_setting( 'site_favicon' , array(
      'default'           => false,
      'sanitize_callback' => 'novus_sanitize_image',
    ) );
    $wp_customize->add_control( new WP_Customize_Upload_Control( $wp_customize,
      'site_favicon', array(
        'label'      => __( 'Favicon', 'novus' ),
        'section'    => 'title_tagline',
        'settings'   => 'site_favicon',
      )
    ) );
  }


  // SECTION - Colors

  // Base color
  $wp_customize->add_setting( 'color_base' , array(
    'default'           => '#4f4f4f',
    'transport'         => 'postMessage',
    'sanitize_callback' => 'novus_sanitize_hex_color',
  ) );
  $wp_customize->add_control( new WP_Customize_Color_Control(
    $wp_customize,
    'color_base', array(
      'label'    => __( 'Base Text', 'novus' ),
      'section'  => 'colors',
      'settings' => 'color_base',
    )
  ) );
  // Text color A
  $wp_customize->add_setting( 'color_theme_a' , array(
    'default'           => '#a5a5a5',
    'transport'         => 'postMessage',
    'sanitize_callback' => 'novus_sanitize_hex_color',
  ) );
  $wp_customize->add_control( new WP_Customize_Color_Control(
    $wp_customize,
    'color_theme_a', array(
      'label'    => __( 'Theme Color A', 'novus' ),
      'section'  => 'colors',
      'settings' => 'color_theme_a',
      'description' => __( 'Meta, footer, author info', 'novus' ),
    )
  ) );
  // Text color B
  $wp_customize->add_setting( 'color_theme_b' , array(
    'default'           => '#646464',
    'transport'         => 'postMessage',
    'sanitize_callback' => 'novus_sanitize_hex_color',
  ) );
  $wp_customize->add_control( new WP_Customize_Color_Control(
    $wp_customize,
    'color_theme_b', array(
      'label'    => __( 'Theme Color B', 'novus' ),
      'section'  => 'colors',
      'settings' => 'color_theme_b',
      'description' => __( 'Widget titles, headings in post content', 'novus' ),
    )
  ) );
  // Text color C
  $wp_customize->add_setting( 'color_theme_c' , array(
    'default'           => '#f2f2f2',
    'transport'         => 'postMessage',
    'sanitize_callback' => 'novus_sanitize_hex_color',
  ) );
  $wp_customize->add_control( new WP_Customize_Color_Control(
    $wp_customize,
    'color_theme_c', array(
      'label'    => __( 'Theme Color C', 'novus' ),
      'section'  => 'colors',
      'settings' => 'color_theme_c',
      'description' => __( 'Post separators, comment meta border', 'novus' ),
    )
  ) );
  // Text color D
  $wp_customize->add_setting( 'color_theme_d' , array(
    'default'           => '#daaa29',
    'transport'         => 'postMessage',
    'sanitize_callback' => 'novus_sanitize_hex_color',
  ) );
  $wp_customize->add_control( new WP_Customize_Color_Control(
    $wp_customize,
    'color_theme_d', array(
      'label'    => __( 'Theme Color D', 'novus' ),
      'section'  => 'colors',
      'settings' => 'color_theme_d',
      'description' => __( 'Post title bottom border, widget bottom border', 'novus' ),
    )
  ) );
  // Footer background
  $wp_customize->add_setting( 'color_footer_bg' , array(
    'default'           => '#f1f1ee',
    'transport'         => 'postMessage',
    'sanitize_callback' => 'novus_sanitize_hex_color',
  ) );
  $wp_customize->add_control( new WP_Customize_Color_Control(
    $wp_customize,
    'color_footer_bg', array(
      'label'    => __( 'Footer Background', 'novus' ),
      'section'  => 'colors',
      'settings' => 'color_footer_bg',
    )
  ) );
  // Post content color
  $wp_customize->add_setting( 'color_content' , array(
    'default'           => '#ffffff',
    'transport'         => 'postMessage',
    'sanitize_callback' => 'novus_sanitize_hex_color',
  ) );
  $wp_customize->add_control( new WP_Customize_Color_Control(
    $wp_customize,
    'color_content', array(
      'label'    => __( 'Post content', 'novus' ),
      'section'  => 'novus_colors_article',
      'settings' => 'color_content',
      'description' => __( 'Main content box, boxes in masonry and grid layout.', 'novus' ),
    )
  ) );
  // Post content shadow
  $wp_customize->add_setting( 'color_content_shadow' , array(
    'default'           => false,
    'sanitize_callback' => 'novus_sanitize_checkbox',
  ) );
  $wp_customize->add_control( new WP_Customize_Control(
    $wp_customize,
    'color_content_shadow', array(
      'label'    => __( 'Hide shadow below post content', 'novus' ),
      'section'  => 'novus_colors_article',
      'settings' => 'color_content_shadow',
      'type'     => 'checkbox',
    )
  ) );
  // Link color
  $wp_customize->add_setting( 'color_link' , array(
    'default'           => '',
    'sanitize_callback' => 'novus_sanitize_hex_color',
  ) );
  $wp_customize->add_control( new WP_Customize_Color_Control(
    $wp_customize,
    'color_link', array(
      'label'    => __( 'Link', 'novus' ),
      'section'  => 'colors',
      'settings' => 'color_link',
    )
  ) );
  $wp_customize->add_setting( 'color_link_hover' , array(
    'default'           => '',
    'sanitize_callback' => 'novus_sanitize_hex_color',
  ) );
  $wp_customize->add_control( new WP_Customize_Color_Control(
    $wp_customize,
    'color_link_hover', array(
      'label'    => __( 'Link Hover', 'novus' ),
      'section'  => 'colors',
      'settings' => 'color_link_hover',
    )
  ) );
  // Button color
  $wp_customize->add_setting( 'color_button' , array(
    'default'           => '#ffffff',
    'sanitize_callback' => 'novus_sanitize_hex_color',
  ) );
  $wp_customize->add_control( new WP_Customize_Color_Control(
    $wp_customize,
    'color_button', array(
      'label'    => __( 'Button Text', 'novus' ),
      'section'  => 'novus_colors_buttons_forms',
      'settings' => 'color_button',
    )
  ) );
  // Button hover color
  $wp_customize->add_setting( 'color_button_hover' , array(
    'default'           => '#ffffff',
    'sanitize_callback' => 'novus_sanitize_hex_color',
  ) );
  $wp_customize->add_control( new WP_Customize_Color_Control(
    $wp_customize,
    'color_button_hover', array(
      'label'    => __( 'Button Text Hover', 'novus' ),
      'section'  => 'novus_colors_buttons_forms',
      'settings' => 'color_button_hover',
    )
  ) );
  // Button background color
  $wp_customize->add_setting( 'color_button_background' , array(
    'default'           => '#4f4f4f',
    'sanitize_callback' => 'novus_sanitize_hex_color',
  ) );
  $wp_customize->add_control( new WP_Customize_Color_Control(
    $wp_customize,
    'color_button_background', array(
      'label'    => __( 'Button Background', 'novus' ),
      'section'  => 'novus_colors_buttons_forms',
      'settings' => 'color_button_background',
    )
  ) );
  // Button background hover color
  $wp_customize->add_setting( 'color_button_background_hover' , array(
    'default'           => '#646464',
    'sanitize_callback' => 'novus_sanitize_hex_color',
  ) );
  $wp_customize->add_control( new WP_Customize_Color_Control(
    $wp_customize,
    'color_button_background_hover', array(
      'label'    => __( 'Button Background Hover', 'novus' ),
      'section'  => 'novus_colors_buttons_forms',
      'settings' => 'color_button_background_hover',
    )
  ) );
  // Form input text color
  $wp_customize->add_setting( 'color_form_field_text' , array(
    'default'           => '#4f4f4f',
    'sanitize_callback' => 'novus_sanitize_hex_color',
  ) );
  $wp_customize->add_control( new WP_Customize_Color_Control(
    $wp_customize,
    'color_form_field_text', array(
      'label'    => __( 'Form Field Text', 'novus' ),
      'section'  => 'novus_colors_buttons_forms',
      'settings' => 'color_form_field_text',
    )
  ) );
  // Form input border color
  $wp_customize->add_setting( 'color_form_field_border' , array(
    'default'           => '#e8e8e0',
    'sanitize_callback' => 'novus_sanitize_hex_color',
  ) );
  $wp_customize->add_control( new WP_Customize_Color_Control(
    $wp_customize,
    'color_form_field_border', array(
      'label'    => __( 'Form Field Border', 'novus' ),
      'section'  => 'novus_colors_buttons_forms',
      'settings' => 'color_form_field_border',
    )
  ) );
  // Form input border focus color
  $wp_customize->add_setting( 'color_form_field_border_focus' , array(
    'default'           => '#d2d2c8',
    'sanitize_callback' => 'novus_sanitize_hex_color',
  ) );
  $wp_customize->add_control( new WP_Customize_Color_Control(
    $wp_customize,
    'color_form_field_border_focus', array(
      'label'    => __( 'Form Field Border Focus', 'novus' ),
      'section'  => 'novus_colors_buttons_forms',
      'settings' => 'color_form_field_border_focus',
    )
  ) );
  // Form input background color
  $wp_customize->add_setting( 'color_form_field_bg' , array(
    'default'           => '#f1f1ee',
    'sanitize_callback' => 'novus_sanitize_hex_color',
  ) );
  $wp_customize->add_control( new WP_Customize_Color_Control(
    $wp_customize,
    'color_form_field_bg', array(
      'label'    => __( 'Form Field Background', 'novus' ),
      'section'  => 'novus_colors_buttons_forms',
      'settings' => 'color_form_field_bg',
    )
  ) );
  // Form input background focus color
  $wp_customize->add_setting( 'color_form_field_bg_focus' , array(
    'default'           => '#f1f1ee',
    'sanitize_callback' => 'novus_sanitize_hex_color',
  ) );
  $wp_customize->add_control( new WP_Customize_Color_Control(
    $wp_customize,
    'color_form_field_bg_focus', array(
      'label'    => __( 'Form Field Background Focus', 'novus' ),
      'section'  => 'novus_colors_buttons_forms',
      'settings' => 'color_form_field_bg_focus',
    )
  ) );
  // Nav bar background
  $wp_customize->add_setting( 'color_nav_bar_bg' , array(
    'default' => 'rgba(32,32,32,0.9)',
  ) );
  $wp_customize->add_control( new Novus_Customize_Alpha_Color_Control(
    $wp_customize,
    'color_nav_bar_bg', array(
      'label'    => __( 'Nav Bar Background', 'novus' ),
      'section'  => 'novus_colors_nav_bar',
      'palette'  => false,
      'settings' => 'color_nav_bar_bg',
    )
  ) );
  // Nav bar link color
  $wp_customize->add_setting( 'color_nav_bar_link' , array(
    'default'           => '#ffffff',
    'sanitize_callback' => 'novus_sanitize_hex_color',
  ) );
  $wp_customize->add_control( new WP_Customize_Color_Control(
    $wp_customize,
    'color_nav_bar_link', array(
      'label'    => __( 'Nav Bar Link', 'novus' ),
      'section'  => 'novus_colors_nav_bar',
      'settings' => 'color_nav_bar_link',
    )
  ) );
  // Nav bar link hover color
  $wp_customize->add_setting( 'color_nav_bar_link_hover' , array(
    'default'           => 'rgba(255,255,255,0.2)',
  ) );
  $wp_customize->add_control( new Novus_Customize_Alpha_Color_Control(
    $wp_customize,
    'color_nav_bar_link_hover', array(
      'label'    => __( 'Nav Bar Link Background Hover', 'novus' ),
      'section'  => 'novus_colors_nav_bar',
      'palette'  => false,
      'settings' => 'color_nav_bar_link_hover',
    )
  ) );
  // Nav bar border color
  $wp_customize->add_setting( 'color_nav_bar_border' , array(
    'default'           => 'rgba(255,255,255,0.5)',
  ) );
  $wp_customize->add_control( new Novus_Customize_Alpha_Color_Control(
    $wp_customize,
    'color_nav_bar_border', array(
      'label'    => __( 'Nav Bar Bottom Border', 'novus' ),
      'section'  => 'novus_colors_nav_bar',
      'palette'  => false,
      'settings' => 'color_nav_bar_border',
    )
  ) );
  // Overlay color
  $wp_customize->add_setting( 'article_overlay_top' , array(
    'default'   => 'rgba(186,154,100,0.1)',
  ) );
  $wp_customize->add_control( new Novus_Customize_Alpha_Color_Control(
    $wp_customize,
    'article_overlay_top', array(
      'label'    => __( 'Article Overlay Gradient Top', 'novus' ),
      'section'  => 'novus_colors_article',
      'palette'  => false,
      'settings' => 'article_overlay_top',
    )
  ) );
  // Overlay color bottom
  $wp_customize->add_setting( 'article_overlay_bottom' , array(
    'default'   => 'rgba(94,76,42,0.4)',
  ) );
  $wp_customize->add_control( new Novus_Customize_Alpha_Color_Control(
    $wp_customize,
    'article_overlay_bottom', array(
      'label'    => __( 'Article Overlay Gradient Bottom', 'novus' ),
      'section'  => 'novus_colors_article',
      'palette'  => false,
      'settings' => 'article_overlay_bottom',
    )
  ) );


  // SECTION - Header

  // Header type
  $wp_customize->add_setting( 'header_type' , array(
    'default'           => 'sticky',
    'sanitize_callback' => 'novus_sanitize_select',
  ) );
  $wp_customize->add_control( new WP_Customize_Control(
    $wp_customize,
    'header_type', array(
      'label'    => __( 'Type', 'novus' ),
      'section'  => 'novus_header',
      'settings' => 'header_type',
      'type'     => 'select',
      'choices' => array(
        'sticky'             => __( 'Sticky', 'novus' ),
        'sticky-transparent' => __( 'Sticky Transparent', 'novus' ),
        'regular'            => __( 'Regular', 'novus' ),
        'transparent'        => __( 'Regular Transparent', 'novus' ),
      ),
    )
  ) );


  // SECTION - Social networks

  // Social networks loop
  $social_networks = array( 'Facebook', 'Twitter', 'Instagram', 'Pinterest', 'LinkedIn', 'Google Plus', 'Tumblr', 'Reddit', 'Dribbble', 'GitHub', 'Vimeo', 'YouTube' );
  $social_networks_prefix = 'sn_';
  foreach ( $social_networks as $network ) {
    $setting = $social_networks_prefix . str_replace( ' ', '_', strtolower( $network ) );
    $wp_customize->add_setting( $setting, array(
      'default'     => '',
      'sanitize_callback' => 'esc_url_raw',
    ) );
    $wp_customize->add_control( new WP_Customize_Control(
      $wp_customize,
      $setting, array(
        'label'    => $network,
        'section'  => 'novus_social_networks',
        'settings' => $setting,
      )
    ) );
  }


  // SECTION - Featured posts

  // Display featured posts
  $wp_customize->add_setting( 'featured_posts_display' , array(
    'default'           => true,
    'sanitize_callback' => 'novus_sanitize_checkbox',
  ) );
  $wp_customize->add_control( new WP_Customize_Control(
    $wp_customize,
    'featured_posts_display', array(
      'label'    => __( 'Display Featured Posts on Home Page', 'novus' ),
      'section'  => 'novus_featured_posts',
      'settings' => 'featured_posts_display',
      'type'     => 'checkbox',
    )
  ) );
  // Display featured posts other pages
  $wp_customize->add_setting( 'featured_posts_display_all_pages' , array(
    'default'           => false,
    'sanitize_callback' => 'novus_sanitize_checkbox',
  ) );
  $wp_customize->add_control( new WP_Customize_Control(
    $wp_customize,
    'featured_posts_display_all_pages', array(
      'label'    => __( 'Display Featured Posts on All Pages', 'novus' ),
      'section'  => 'novus_featured_posts',
      'settings' => 'featured_posts_display_all_pages',
      'type'     => 'checkbox',
    )
  ) );
  // Featured posts tag
  $wp_customize->add_setting( 'featured_posts_tag' , array(
    'default'   => false,
  ) );
  $wp_customize->add_control( new Tags_Dropdown_Custom_Control(
    $wp_customize,
    'featured_posts_tag', array(
      'label'    => __( 'Featured Tag', 'novus' ),
      'section'  => 'novus_featured_posts',
      'settings' => 'featured_posts_tag',
    )
  ) );
  // Featured posts number
  $wp_customize->add_setting( 'featured_posts_number' , array(
    'default'           => 6,
    'sanitize_callback' => 'novus_sanitize_select',
  ) );
  $wp_customize->add_control( new WP_Customize_Control(
    $wp_customize,
    'featured_posts_number', array(
      'label'    => __( 'Featured Posts Number', 'novus' ),
      'section'  => 'novus_featured_posts',
      'settings' => 'featured_posts_number',
      'type'     => 'select',
      'choices' => array(
        2 => 2,
        3 => 3,
        4 => 4,
        5 => 5,
        6 => 6,
        7 => 7,
        8 => 8,
        9 => 9,
        10 => 10,
      ),
    )
  ) );
  // Featured posts style
  $wp_customize->add_setting( 'featured_posts_style' , array(
    'default'           => 'boxed',
    'sanitize_callback' => 'novus_sanitize_select',
  ) );
  $wp_customize->add_control( new WP_Customize_Control(
    $wp_customize,
    'featured_posts_style', array(
      'label'    => __( 'Featured Posts Style', 'novus' ),
      'section'  => 'novus_featured_posts',
      'settings' => 'featured_posts_style',
      'type'     => 'select',
      'choices' => array(
        'default' => __( 'No Borders', 'novus' ),
        'boxed'   => __( 'Boxed', 'novus' ),
      ),
    )
  ) );


  // SECTION - Article display

  // Article display layout
  $wp_customize->add_setting( 'article_layout' , array(
    'default'           => 'classic',
    'sanitize_callback' => 'novus_sanitize_select',
  ) );
  $wp_customize->add_control( new WP_Customize_Control(
    $wp_customize,
    'article_layout', array(
      'label'       => __( 'Archive Layout', 'novus' ),
      'description' => __( 'Main blog page, archives and categories.', 'novus' ),
      'section'     => 'novus_article_display',
      'settings'    => 'article_layout',
      'type'        => 'select',
      'choices'     => array(
        'classic'       => __( 'Sidebar Left', 'novus' ),
        'sidebar-right' => __( 'Sidebar Right', 'novus' ),
        'grid'          => __( 'Grid', 'novus' ),
        'list'          => __( 'List', 'novus' ),
        'masonry'       => __( 'Masonry', 'novus' ),
        'masonry-full'  => __( 'Masonry Full Width', 'novus' ),
        'full-width'    => __( 'Full Width', 'novus' ),
      ),
    )
  ) );

  // Article single post display layout
  $wp_customize->add_setting( 'article_layout_single' , array(
    'default'           => 'classic',
    'sanitize_callback' => 'novus_sanitize_select',
  ) );
  $wp_customize->add_control( new WP_Customize_Control(
    $wp_customize,
    'article_layout_single', array(
      'label'       => __( 'Single Posts Layout', 'novus' ),
      'description' => __( 'Single posts and pages.', 'novus' ),
      'section'     => 'novus_article_display',
      'settings'    => 'article_layout_single',
      'type'        => 'select',
      'choices'     => array(
        'classic'       => __( 'Sidebar Left', 'novus' ),
        'sidebar-right' => __( 'Sidebar Right', 'novus' ),
        'full-width'    => __( 'Full Width', 'novus' ),
      ),
    )
  ) );

  // Featured image as main header background
  $wp_customize->add_setting( 'featured_image_in_header' , array(
    'default'           => true,
    'sanitize_callback' => 'novus_sanitize_checkbox',
  ) );
  $wp_customize->add_control( new WP_Customize_Control(
    $wp_customize,
    'featured_image_in_header', array(
      'label'       => __( 'Use Featured Image as Header Background', 'novus' ),
      'description' => __( 'Display Featured Image as Site\'s Header Background on Single Posts and Pages. Does not apply to WooCommerce product pages', 'novus' ),
      'section'     => 'novus_article_display',
      'settings'    => 'featured_image_in_header',
      'type'        => 'checkbox',
    )
  ) );

  // Article relative time
  $wp_customize->add_setting( 'relative_time_display' , array(
    'default'           => true,
    'sanitize_callback' => 'novus_sanitize_checkbox',
  ) );
  $wp_customize->add_control( new WP_Customize_Control(
    $wp_customize,
    'relative_time_display', array(
      'label'       => __( 'Use Relative Time', 'novus' ),
      'section'     => 'novus_article_display',
      'settings'    => 'relative_time_display',
      'type'        => 'checkbox',
      'description' => __( 'If unchecked it will display publishing date', 'novus' ),
    )
  ) );

  // Author info
  $wp_customize->add_setting( 'author_info_display' , array(
    'default'           => true,
    'sanitize_callback' => 'novus_sanitize_checkbox',
  ) );
  $wp_customize->add_control( new WP_Customize_Control(
    $wp_customize,
    'author_info_display', array(
      'label'    => __( 'Display Author Info on Single Posts', 'novus' ),
      'section'  => 'novus_article_display',
      'settings' => 'author_info_display',
      'type'     => 'checkbox',
    )
  ) );

  // Article share links
  $wp_customize->add_setting( 'article_share_links' , array(
    'default'           => true,
    'sanitize_callback' => 'novus_sanitize_checkbox',
  ) );
  $wp_customize->add_control( new WP_Customize_Control(
    $wp_customize,
    'article_share_links', array(
      'label'    => __( 'Display Share Links on Single Posts', 'novus' ),
      'section'  => 'novus_article_display',
      'settings' => 'article_share_links',
      'type'     => 'checkbox',
    )
  ) );

  // Page share links
  $wp_customize->add_setting( 'page_share_links' , array(
    'default'           => true,
    'sanitize_callback' => 'novus_sanitize_checkbox',
  ) );
  $wp_customize->add_control( new WP_Customize_Control(
    $wp_customize,
    'page_share_links', array(
      'label'    => __( 'Display Share Links on Pages', 'novus' ),
      'section'  => 'novus_article_display',
      'settings' => 'page_share_links',
      'type'     => 'checkbox',
    )
  ) );

  // Archive share links
  $wp_customize->add_setting( 'archive_share_links' , array(
    'default'           => true,
    'sanitize_callback' => 'novus_sanitize_checkbox',
  ) );
  $wp_customize->add_control( new WP_Customize_Control(
    $wp_customize,
    'archive_share_links', array(
      'label'    => __( 'Display Share Links in Archives', 'novus' ),
      'section'  => 'novus_article_display',
      'settings' => 'archive_share_links',
      'type'     => 'checkbox',
    )
  ) );

  // Display related posts
  $wp_customize->add_setting( 'related_posts_display' , array(
    'default'           => true,
    'sanitize_callback' => 'novus_sanitize_checkbox',
  ) );
  $wp_customize->add_control( new WP_Customize_Control(
    $wp_customize,
    'related_posts_display', array(
      'label'    => __( 'Display Related Posts', 'novus' ),
      'section'  => 'novus_article_display',
      'settings' => 'related_posts_display',
      'type'     => 'checkbox',
    )
  ) );


  // SECTION - Comments

  // Comments relative time
  $wp_customize->add_setting( 'comments_relative_time' , array(
    'default'           => true,
    'sanitize_callback' => 'novus_sanitize_checkbox',
  ) );
  $wp_customize->add_control( new WP_Customize_Control(
    $wp_customize,
    'comments_relative_time', array(
      'label'       => __( 'Use Relative Time', 'novus' ),
      'section'     => 'novus_comments',
      'settings'    => 'comments_relative_time',
      'type'        => 'checkbox',
      'description' => __( 'If unchecked it will display publishing date', 'novus' ),
    )
  ) );


  // SECTION - Jetpack

  // Infinite scroll type
  // $wp_customize->add_setting( 'infinite_scroll_type' , array(
  //   'default'           => false,
  //   'sanitize_callback' => 'novus_sanitize_checkbox',
  // ) );
  // $wp_customize->add_control( new WP_Customize_Control(
  //   $wp_customize,
  //   'infinite_scroll_type', array(
  //     'label'    => __( 'Load New Posts on Scroll', 'novus' ),
  //     'section'  => 'novus_jetpack',
  //     'settings' => 'infinite_scroll_type',
  //     'type'     => 'checkbox',
  //     'description' => __( 'If unchecked posts will load on click', 'novus' ),
  //   )
  // ) );


  // SECTION - Miscellaneous

  // Footer text
  $wp_customize->add_setting( 'footer_text' , array(
    'default'           => '&copy; 2017. Gaia by <a href="http://www.nvslbs.com" rel="nofollow" title="Making Art with WordPress">Novus Labs</a>',
    'transport'         => 'postMessage',
    'sanitize_callback' => 'wp_kses_post',
  ) );
  $wp_customize->add_control( new WP_Customize_Control(
    $wp_customize,
    'footer_text', array(
      'label'    => __( 'Footer Text', 'novus' ),
      'section'  => 'novus_misc',
      'settings' => 'footer_text',
      'type'     => 'textarea',
    )
  ) );


  // PANEL - Advanced options

  // SECTION - Header and footer scripts

  // Header scripts
  $wp_customize->add_setting( 'header_scripts' , array(
    'default'   => '',
  ) );
  $wp_customize->add_control( new WP_Customize_Control(
    $wp_customize,
    'header_scripts', array(
      'label'    => __( 'Header Scripts', 'novus' ),
      'section'  => 'novus_header_footer',
      'settings' => 'header_scripts',
      'type'     => 'textarea',
      'description' => __( 'Insert scripts that needs to be inside HEAD tag', 'novus' ),
    )
  ) );

  // Footer scripts
  $wp_customize->add_setting( 'footer_scripts' , array(
    'default'   => '',
  ) );
  $wp_customize->add_control( new WP_Customize_Control(
    $wp_customize,
    'footer_scripts', array(
      'label'    => __( 'Footer Scripts', 'novus' ),
      'section'  => 'novus_header_footer',
      'settings' => 'footer_scripts',
      'type'     => 'textarea',
      'description' => __( 'Insert scripts that needs to be before BODY tag ends', 'novus' ),
    )
  ) );
}
add_action( 'customize_register', 'novus_customize_register' );

/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function novus_customize_preview_js() {
  wp_enqueue_script( 'novus_customizer', get_template_directory_uri() . '/assets/js/customizer.js', array( 'customize-preview' ), '1', true );
}
add_action( 'customize_preview_init', 'novus_customize_preview_js' );

/**
 * Add customizer controls scripts
 */
function novus_enqueue_customizer_admin_scripts() {
  wp_enqueue_script( 'customizer-admin-js', get_template_directory_uri() . '/inc/customizer-controls/js/customizer-admin.js', array( 'jquery' ), NULL, true );
}
add_action( 'customize_controls_print_footer_scripts', 'novus_enqueue_customizer_admin_scripts' );

/**
 * Add customizer controls styles
 */
function novus_enqueue_customizer_controls_styles() {
  wp_enqueue_style( 'pluto-customizer-controls', get_template_directory_uri() . '/inc/customizer-controls/css/customizer.css', NULL, NULL, 'all' );
}
add_action( 'customize_controls_print_styles', 'novus_enqueue_customizer_controls_styles' );


if ( ! function_exists( 'novus_customizer_header_scripts' ) ) :
/**
 * Header scripts
 */
function novus_customizer_header_scripts() {
  if ( get_theme_mod( 'header_scripts', false ) ) {
    echo get_theme_mod( 'header_scripts', false );
  }
}
add_action( 'wp_head', 'novus_customizer_header_scripts' );
endif;

if ( ! function_exists( 'novus_customizer_footer_scripts' ) ) :
/**
 * Footer scripts
 */
function novus_customizer_footer_scripts() {
  if ( get_theme_mod( 'footer_scripts', false ) ) {
    echo get_theme_mod( 'footer_scripts', false );
  }
}
add_action( 'wp_footer', 'novus_customizer_footer_scripts' );
endif;
