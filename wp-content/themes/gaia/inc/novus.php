<?php

/**
 * Custom excerpt more
 * @return string
 */
function novus_excerpt_more( $more ) {
  global $post;
  return ' ... <a href="'. get_permalink( $post->ID ) . '">read more</a>';
}
add_filter( 'excerpt_more', 'novus_excerpt_more' );

/**
 * Custom read more link
 * @return string
 */
function novus_read_more_link() {
  return '<span class="more-link-wrap"><a class="more-link" href="' . get_permalink() . '">' . __( 'Continue reading', 'novus' ) . '</a></span>';
}
add_filter( 'the_content_more_link', 'novus_read_more_link' );

/**
 * Masonry wrap before loop
 */
function novus_masonry_wrap_loop_before() {
  $layouts = array( 'masonry', 'masonry-full' );
  $layout = get_theme_mod( 'article_layout' );
  if ( in_array( $layout, $layouts ) ) {
    echo '<div class="masonry-wrap js-masonry-wrap">';
  }
}
add_action( 'novus_loop_before', 'novus_masonry_wrap_loop_before', 10 );

/**
 * Masonry wrap after loop
 */
function novus_masonry_wrap_loop_after() {
  $layouts = array( 'masonry', 'masonry-full' );
  $layout = get_theme_mod( 'article_layout' );
  if ( in_array( $layout, $layouts ) ) {
    echo '</div>';
  }
}
add_action( 'novus_loop_after', 'novus_masonry_wrap_loop_after', 10 );

/**
 * Comments display
 */
function novus_comment( $comment, $args, $depth ) {
  $GLOBALS['comment'] = $comment;
  extract($args, EXTR_SKIP);

  if ( 'div' == $args['style'] ) {
    $tag = 'div';
    $add_below = 'comment';
  } else {
    $tag = 'li';
    $add_below = 'div-comment';
  }
?>
  <<?php echo $tag ?> <?php comment_class( empty( $args['has_children'] ) ? 'media' : 'media parent' ) ?> id="comment-<?php comment_ID() ?>">
  <?php if ( 'div' != $args['style'] ) : ?>
  <div id="div-comment-<?php comment_ID() ?>" class="comment-body">
  <?php endif; ?>
  <div class="comment-author vcard media-img">
    <a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ); ?>"><?php if ( $args['avatar_size'] != 0 ) echo get_avatar( $comment, $args['avatar_size'] ); ?></a>
  </div>
  <div class="media-bd">

    <?php comment_text(); ?>

    <div class="comment-meta commentmetadata">
      <?php if ( $comment->comment_approved == '0' ) : ?>
        <em class="comment-awaiting-moderation"><?php _e( 'Your comment is awaiting moderation.', 'novus' ); ?></em>
        <br>
      <?php endif; ?>
      <span class="comment-author-name"><?php printf( __( '<cite class="fn">%s</cite>' ), get_comment_author_link() ); ?></span>

      <span class="comment-reply"><?php comment_reply_link( array_merge( $args, array( 'add_below' => $add_below, 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) ); ?></span>

      <span class="comment-date">
        <?php
          // Get theme modifications
          $theme_mods = get_theme_mods();
          $relative_time_display = get_theme_mod( 'comments_relative_time', true );
        ?>
        <?php if ( empty( $theme_mods ) || !empty( $relative_time_display ) ) : ?>
          <?php echo human_time_diff( get_comment_time('U'), current_time('timestamp') ) . __( ' ago', 'novus' ); ?>
        <?php else : ?>
          <?php
            /* translators: 1: date, 2: time */
            printf( __( '%1$s at %2$s', 'novus' ), get_comment_date(),  get_comment_time() ); ?>
        <?php endif; ?>
      </span>

      <?php edit_comment_link( __( 'Edit', 'novus' ), '<span>', '</span>' ); ?>

    </div>

    <?php if ( 'div' != $args['style'] ) : ?>
    </div>
    <?php endif; ?>
  </div>
<?php
}

/**
 * Add CSS class based on selected layout
 * @return string
 */
function novus_body_class( $classes ) {
  // Check if WooCommerce is active
  $is_woocommerce = false;
  if ( NOVUS_WOOCOMMERCE_ACTIVE ) {
    $is_woocommerce = is_woocommerce();
  }
  // Archive
  if ( ( is_home() || is_archive() || is_search() ) && ! $is_woocommerce ) {
    $layout = get_theme_mod( 'article_layout', 'classic' );
    $classes[] = 'layout-' . $layout;
    if ( $layout == 'masonry-full' ) {
      $classes[] = 'layout-masonry';
    }
  }
  // Single
  if ( is_singular() || is_404() ) {
    $layout = get_theme_mod( 'article_layout_single', 'classic' );
    $classes[] = 'layout-' . $layout;
  }
  if ( get_theme_mod( 'featured_posts_display_all_pages', false ) || ( get_theme_mod( 'featured_posts_display', true ) && is_home() ) ) {
    $classes[] = 'featured-posts-visible';
  }
  return $classes;
}
add_filter( 'body_class', 'novus_body_class' );
