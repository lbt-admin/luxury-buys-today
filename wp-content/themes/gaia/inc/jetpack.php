<?php
/**
 * Jetpack Compatibility File
 * See: http://jetpack.me/
 *
 * @package Novus
 */

/**
 * Add theme support for Infinite Scroll.
 * See: http://jetpack.me/support/infinite-scroll/
 */
function novus_jetpack_setup() {
  $type = get_theme_mod( 'infinite_scroll_type', false ) ? 'scroll' : 'click';
  add_theme_support( 'infinite-scroll', array(
    'container' => 'main',
    'footer'    => false,
    'type'      => $type,
    'wrapper'   => false,
    'render'    => 'novus_infinite_scroll_render',
  ) );
}
add_action( 'after_setup_theme', 'novus_jetpack_setup' );

function novus_infinite_scroll_render() {
  while ( have_posts() ) {
    the_post();
    get_template_part( 'partials/content', get_post_format() );
  }
}
