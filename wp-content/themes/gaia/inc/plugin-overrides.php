<?php
/**
 * Ovveride Contact Form 7 loader image
 * @return string
 */
function novus_wpcf7_ajax_loader() {
  return get_stylesheet_directory_uri() . '/assets/images/loader.gif';
}
add_filter( 'wpcf7_ajax_loader', 'novus_wpcf7_ajax_loader', 10 );
