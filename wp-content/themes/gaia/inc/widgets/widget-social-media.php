<?php
/**
 * Novus Social Media widget.
 */
class Novus_Social_Media_Widget extends WP_Widget {

  /**
   * Register widget with WordPress.
   */
  function __construct() {
    parent::__construct(
      'social_media', // Base ID
      __( 'Novus / Social Media', 'novus' ), // Name
      array( 'description' => __( 'Display links to social media profiles.', 'novus' ), ) // Args
    );
  }

  /**
   * Front-end display of widget.
   *
   * @see WP_Widget::widget()
   *
   * @param array $args     Widget arguments.
   * @param array $instance Saved values from database.
   */
  public function widget( $args, $instance ) {
    echo $args['before_widget'];

    $title = ( ! empty( $instance['title'] ) ) ? $instance['title'] : __( 'Connect', 'novus' );
    $title = apply_filters( 'widget_title', $title, $instance );
    echo $args['before_title'] . $title . $args['after_title'];

    $target = ! empty( $instance['target'] ) ? '_blank' : '_self';

    $icon_css_class = empty( $instance['icon_style'] ) ? 'square' : $instance['icon_style'];
    ?>

    <?php if ( ! empty( $instance['facebook'] ) ) : ?>
      <a class="genericon genericon-facebook-alt <?php echo esc_attr( $icon_css_class ); ?>" href="<?php echo esc_url( $instance['facebook'] ); ?>" target="<?php echo esc_attr( $target ); ?>"></a>
    <?php endif; ?>
    <?php if ( ! empty( $instance['twitter'] ) ) : ?>
      <a class="genericon genericon-twitter <?php echo esc_attr( $icon_css_class ); ?>" href="<?php echo esc_url( $instance['twitter'] ); ?>" target="<?php echo esc_attr( $target ); ?>"></a>
    <?php endif; ?>
    <?php if ( ! empty( $instance['instagram'] ) ) : ?>
      <a class="genericon genericon-instagram <?php echo esc_attr( $icon_css_class ); ?>" href="<?php echo esc_url( $instance['instagram'] ); ?>" target="<?php echo esc_attr( $target ); ?>"></a>
    <?php endif; ?>
    <?php if ( ! empty( $instance['pinterest'] ) ) : ?>
      <a class="genericon genericon-pinterest <?php echo esc_attr( $icon_css_class ); ?>" href="<?php echo esc_url( $instance['pinterest'] ); ?>" target="<?php echo esc_attr( $target ); ?>"></a>
    <?php endif; ?>
    <?php if ( ! empty( $instance['linkedin'] ) ) : ?>
      <a class="genericon genericon-linkedin <?php echo esc_attr( $icon_css_class ); ?>" href="<?php echo esc_url( $instance['linkedin'] ); ?>" target="<?php echo esc_attr( $target ); ?>"></a>
    <?php endif; ?>
    <?php if ( ! empty( $instance['googleplus'] ) ) : ?>
      <a class="genericon genericon-googleplus <?php echo esc_attr( $icon_css_class ); ?>" href="<?php echo esc_url( $instance['googleplus'] ); ?>" target="<?php echo esc_attr( $target ); ?>"></a>
    <?php endif; ?>
    <?php if ( ! empty( $instance['tumblr'] ) ) : ?>
      <a class="genericon genericon-tumblr <?php echo esc_attr( $icon_css_class ); ?>" href="<?php echo esc_url( $instance['tumblr'] ); ?>" target="<?php echo esc_attr( $target ); ?>"></a>
    <?php endif; ?>
    <?php if ( ! empty( $instance['reddit'] ) ) : ?>
      <a class="genericon genericon-reddit <?php echo esc_attr( $icon_css_class ); ?>" href="<?php echo esc_url( $instance['reddit'] ); ?>" target="<?php echo esc_attr( $target ); ?>"></a>
    <?php endif; ?>
    <?php if ( ! empty( $instance['dribbble'] ) ) : ?>
      <a class="genericon genericon-dribbble <?php echo esc_attr( $icon_css_class ); ?>" href="<?php echo esc_url( $instance['dribbble'] ); ?>" target="<?php echo esc_attr( $target ); ?>"></a>
    <?php endif; ?>
    <?php if ( ! empty( $instance['github'] ) ) : ?>
      <a class="genericon genericon-github <?php echo esc_attr( $icon_css_class ); ?>" href="<?php echo esc_url( $instance['github'] ); ?>" target="<?php echo esc_attr( $target ); ?>"></a>
    <?php endif; ?>
    <?php if ( ! empty( $instance['vimeo'] ) ) : ?>
      <a class="genericon genericon-vimeo <?php echo esc_attr( $icon_css_class ); ?>" href="<?php echo esc_url( $instance['vimeo'] ); ?>" target="<?php echo esc_attr( $target ); ?>"></a>
    <?php endif; ?>
    <?php if ( ! empty( $instance['youtube'] ) ) : ?>
      <a class="genericon genericon-youtube <?php echo esc_attr( $icon_css_class ); ?>" href="<?php echo esc_url( $instance['youtube'] ); ?>" target="<?php echo esc_attr( $target ); ?>"></a>
    <?php endif; ?>

    <?php
    echo $args['after_widget'];
  }

  /**
   * Back-end widget form.
   *
   * @see WP_Widget::form()
   *
   * @param array $instance Previously saved values from database.
   */
  public function form( $instance ) {
    $instance = wp_parse_args( (array) $instance, array(
      'icon_style' => 'square',
      'facebook'   => '',
      'twitter'    => '',
      'pinterest'  => '',
      'instagram'  => '',
      'linkedin'   => '',
      'googleplus' => '',
      'tumblr'     => '',
      'reddit'     => '',
      'dribbble'   => '',
      'github'     => '',
      'vimeo'      => '',
      'youtube'    => '',
    ) );
    $title = ! empty( $instance['title'] ) ? $instance['title'] : '';
    $target = ! empty( $instance['target'] ) ? 1 : 0;
    ?>
    <p>
      <label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php _e( 'Title:', 'novus' ); ?></label>
      <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
    </p>
    <p>
      <label for="<?php echo esc_attr( $this->get_field_id('icon_style') ); ?>"><?php _e( 'Style:', 'novus' ); ?></label>
      <select name="<?php echo esc_attr( $this->get_field_name('icon_style') ); ?>" id="<?php echo esc_attr( $this->get_field_id('icon_style') ); ?>" class="widefat">
        <option value="round"<?php selected( $instance['icon_style'], 'round' ); ?>><?php _e( 'Round', 'novus'); ?></option>
        <option value="round-big"<?php selected( $instance['icon_style'], 'round-big' ); ?>><?php _e( 'Round Big', 'novus'); ?></option>
        <option value="square"<?php selected( $instance['icon_style'], 'square' ); ?>><?php _e( 'Square', 'novus' ); ?></option>
        <option value="square-big"<?php selected( $instance['icon_style'], 'square-big' ); ?>><?php _e( 'Square Big', 'novus' ); ?></option>
        <option value="circle"<?php selected( $instance['icon_style'], 'circle' ); ?>><?php _e( 'Circle', 'novus' ); ?></option>
        <option value="circle-big"<?php selected( $instance['icon_style'], 'circle-big' ); ?>><?php _e( 'Circle Big', 'novus' ); ?></option>
      </select>
    </p>
    <p>
      <input class="checkbox" type="checkbox" <?php checked( 1, $target ); ?> id="<?php echo esc_attr( $this->get_field_id( 'target' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'target' ) ); ?>" /> <label for="<?php echo esc_attr( $this->get_field_id( 'target' ) ); ?>"><?php _e( 'Open links in new tab', 'novus' ); ?></label>
    </p>
    <p>
      <label for="<?php echo esc_attr( $this->get_field_id( 'facebook' ) ); ?>"><?php _e( 'Facebook:', 'novus' ); ?></label>
      <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'facebook' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'facebook' ) ); ?>" type="text" value="<?php echo esc_attr( $instance['facebook'] ); ?>">
    </p>
    <p>
      <label for="<?php echo esc_attr( $this->get_field_id( 'twitter' ) ); ?>"><?php _e( 'Twitter:', 'novus' ); ?></label>
      <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'twitter' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'twitter' ) ); ?>" type="text" value="<?php echo esc_attr( $instance['twitter'] ); ?>">
    </p>
    <p>
      <label for="<?php echo esc_attr( $this->get_field_id( 'pinterest' ) ); ?>"><?php _e( 'Pinterest:', 'novus' ); ?></label>
      <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'pinterest' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'pinterest' ) ); ?>" type="text" value="<?php echo esc_attr( $instance['pinterest'] ); ?>">
    </p>
    <p>
      <label for="<?php echo esc_attr( $this->get_field_id( 'instagram' ) ); ?>"><?php _e( 'Instagram:', 'novus' ); ?></label>
      <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'instagram' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'instagram' ) ); ?>" type="text" value="<?php echo esc_attr( $instance['instagram'] ); ?>">
    </p>
    <p>
      <label for="<?php echo esc_attr( $this->get_field_id( 'linkedin' ) ); ?>"><?php _e( 'LinkedIn:', 'novus' ); ?></label>
      <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'linkedin' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'linkedin' ) ); ?>" type="text" value="<?php echo esc_attr( $instance['linkedin'] ); ?>">
    </p>
    <p>
      <label for="<?php echo esc_attr( $this->get_field_id( 'googleplus' ) ); ?>"><?php _e( 'Google+:', 'novus' ); ?></label>
      <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'googleplus' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'googleplus' ) ); ?>" type="text" value="<?php echo esc_attr( $instance['googleplus'] ); ?>">
    </p>
    <p>
      <label for="<?php echo esc_attr( $this->get_field_id( 'tumblr' ) ); ?>"><?php _e( 'Tumblr:', 'novus' ); ?></label>
      <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'tumblr' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'tumblr' ) ); ?>" type="text" value="<?php echo esc_attr( $instance['tumblr'] ); ?>">
    </p>
    <p>
      <label for="<?php echo esc_attr( $this->get_field_id( 'reddit' ) ); ?>"><?php _e( 'Reddit:', 'novus' ); ?></label>
      <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'reddit' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'reddit' ) ); ?>" type="text" value="<?php echo esc_attr( $instance['reddit'] ); ?>">
    </p>
    <p>
      <label for="<?php echo esc_attr( $this->get_field_id( 'dribbble' ) ); ?>"><?php _e( 'Dribbble:', 'novus' ); ?></label>
      <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'dribbble' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'dribbble' ) ); ?>" type="text" value="<?php echo esc_attr( $instance['dribbble'] ); ?>">
    </p>
    <p>
      <label for="<?php echo esc_attr( $this->get_field_id( 'github' ) ); ?>"><?php _e( 'Github:', 'novus' ); ?></label>
      <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'github' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'github' ) ); ?>" type="text" value="<?php echo esc_attr( $instance['github'] ); ?>">
    </p>
    <p>
      <label for="<?php echo esc_attr( $this->get_field_id( 'vimeo' ) ); ?>"><?php _e( 'Vimeo:', 'novus' ); ?></label>
      <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'vimeo' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'vimeo' ) ); ?>" type="text" value="<?php echo esc_attr( $instance['vimeo'] ); ?>">
    </p>
    <p>
      <label for="<?php echo esc_attr( $this->get_field_id( 'youtube' ) ); ?>"><?php _e( 'Youtube:', 'novus' ); ?></label>
      <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'youtube' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'youtube' ) ); ?>" type="text" value="<?php echo esc_attr( $instance['youtube'] ); ?>">
    </p>
    <?php
  }

  /**
   * Sanitize widget form values as they are saved.
   *
   * @see WP_Widget::update()
   *
   * @param array $new_instance Values just sent to be saved.
   * @param array $old_instance Previously saved values from database.
   *
   * @return array Updated safe values to be saved.
   */
  public function update( $new_instance, $old_instance ) {
    $instance = $old_instance;
    $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
    if ( in_array( $new_instance['icon_style'], array( 'round', 'round-big', 'square', 'square-big', 'circle', 'circle-big' ) ) ) {
      $instance['icon_style'] = $new_instance['icon_style'];
    } else {
      $instance['icon_style'] = 'round';
    }
    $instance['target'] = $new_instance['target'] ? 1 : 0;
    $instance['facebook'] = ( ! empty( $new_instance['facebook'] ) ) ? esc_url( $new_instance['facebook'] ) : '';
    $instance['twitter'] = ( ! empty( $new_instance['twitter'] ) ) ? esc_url( $new_instance['twitter'] ) : '';
    $instance['pinterest'] = ( ! empty( $new_instance['pinterest'] ) ) ? esc_url( $new_instance['pinterest'] ) : '';
    $instance['instagram'] = ( ! empty( $new_instance['instagram'] ) ) ? esc_url( $new_instance['instagram'] ) : '';
    $instance['linkedin'] = ( ! empty( $new_instance['linkedin'] ) ) ? esc_url( $new_instance['linkedin'] ) : '';
    $instance['googleplus'] = ( ! empty( $new_instance['googleplus'] ) ) ? esc_url( $new_instance['googleplus'] ) : '';
    $instance['tumblr'] = ( ! empty( $new_instance['tumblr'] ) ) ? esc_url( $new_instance['tumblr'] ) : '';
    $instance['reddit'] = ( ! empty( $new_instance['reddit'] ) ) ? esc_url( $new_instance['reddit'] ) : '';
    $instance['dribbble'] = ( ! empty( $new_instance['dribbble'] ) ) ? esc_url( $new_instance['dribbble'] ) : '';
    $instance['github'] = ( ! empty( $new_instance['github'] ) ) ? esc_url( $new_instance['github'] ) : '';
    $instance['vimeo'] = ( ! empty( $new_instance['vimeo'] ) ) ? esc_url( $new_instance['vimeo'] ) : '';
    $instance['youtube'] = ( ! empty( $new_instance['youtube'] ) ) ? esc_url( $new_instance['youtube'] ) : '';

    return $instance;
  }

} // class Novus_Social_Media_Widget

add_action( 'widgets_init', function(){
  register_widget( 'Novus_Social_Media_Widget' );
});
