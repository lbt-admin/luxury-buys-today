<?php
/**
 * Novus Posts List widget.
 */
class Novus_Posts_List_Widget extends WP_Widget {

  /**
   * Register widget with WordPress.
   */
  function __construct() {
    parent::__construct(
      'posts_list', // Base ID
      __( 'Novus / Posts List', 'novus' ), // Name
      array( 'description' => __( 'Recent/Popular/Featured list of posts.', 'novus' ), ) // Args
    );
  }

  /**
   * Front-end display of widget.
   *
   * @see WP_Widget::widget()
   *
   * @param array $args     Widget arguments.
   * @param array $instance Saved values from database.
   */
  public function widget( $args, $instance ) {
    echo $args['before_widget'];

    $title = ( ! empty( $instance['title'] ) ) ? $instance['title'] : __( 'Recent Posts', 'novus' );
    $title = apply_filters( 'widget_title', $title, $instance );
    echo $args['before_title'] . $title . $args['after_title'];

    // List type
    $list_type = empty( $instance['list_type'] ) ? 'recent' : $instance['list_type'];

    // Create WP Query object
    $posts_list = new WP_Query();

    // Number of posts
    $number = ( ! empty( $instance['number'] ) ) ? absint( $instance['number'] ) : 5;

    // Query arguments
    $query_args = array(
      'posts_per_page' => $number,
    );

    if ( $list_type == 'featured' ) {
      // featured posts
      $featured_tag_id = get_theme_mod( 'featured_posts_tag', false );
      if ( empty( $featured_tag_id ) ) return;
      $query_args = wp_parse_args( $query_args, array(
        'orderby' => 'rand',
        'tag_id'  => $featured_tag_id,
      ) );
    } elseif ( $list_type == 'popular' ) {
      // popular posts
      $time = ( ! empty( $instance['time'] ) ) ? $instance['time'] : 'All time';
      $date_query = array();
      if ( $time !== 'All time' ) {
        $date_query[] = array(
          'after' => $time
        );
      }
      $query_args = wp_parse_args( $query_args, array(
        'orderby'    => 'comment_count',
        'date_query' => $date_query,
      ) );
    }

    $posts_list->query( $query_args );

    // List style
    $list_style = empty( $instance['list_style'] ) ? 'rounded' : $instance['list_style'];

    if ($posts_list->have_posts()) : ?>
      <ul class="posts_list--<?php echo esc_attr( $list_style ); ?>">
        <?php while ($posts_list->have_posts()) : $posts_list->the_post(); ?>
          <li class="flag">
            <div class="flag-img">
              <a href="<?php the_permalink(); ?>">
              <?php if ( has_post_thumbnail() ) : ?>
                <?php the_post_thumbnail( 'thumbnail' ); ?>
              <?php else : ?>
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/thumb.svg" alt="thumbnail">
              <?php endif; ?>
              </a>
            </div>
            <div class="flag-bd">
              <h3 class="post-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
              <?php echo '<span class="posted-on">' . human_time_diff( get_the_time('U'), current_time('timestamp') ) . __( ' ago', 'novus' ) . '</span>'; ?>
            </div>
          </li>
        <?php endwhile; ?>
      </ul>
    <?php
    wp_reset_postdata();
    else : ?>
      <p><?php _e( 'Oh...there are no posts.', 'novus' ); ?></p>
    <?php
    endif;

    echo $args['after_widget'];
  }

  /**
   * Back-end widget form.
   *
   * @see WP_Widget::form()
   *
   * @param array $instance Previously saved values from database.
   */
  public function form( $instance ) {
    $instance = wp_parse_args( (array) $instance, array( 'list_type' => 'recent', 'list_style' => 'rounded' ) );
    $title = ! empty( $instance['title'] ) ? $instance['title'] : '';
    $number = isset( $instance['number'] ) ? absint( $instance['number'] ) : 5;
    $time = isset( $instance['time'] ) ? $instance['time'] : 'All time';
    ?>
    <p>
      <label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php _e( 'Title:', 'novus' ); ?></label>
      <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
    </p>
    <p>
      <label for="<?php echo esc_attr( $this->get_field_id('list_type') ); ?>"><?php _e( 'Type:', 'novus' ); ?></label>
      <select name="<?php echo esc_attr( $this->get_field_name('list_type') ); ?>" id="<?php echo esc_attr( $this->get_field_id('list_type') ); ?>" class="widefat">
        <option value="recent"<?php selected( $instance['list_type'], 'recent' ); ?>><?php _e( 'Recent Posts', 'novus' ); ?></option>
        <option value="popular"<?php selected( $instance['list_type'], 'popular' ); ?>><?php _e( 'Popular Posts', 'novus' ); ?></option>
        <option value="featured"<?php selected( $instance['list_type'], 'featured' ); ?>><?php _e( 'Featured Posts', 'novus' ); ?></option>
      </select>
    </p>
    <p>
      <label for="<?php echo esc_attr( $this->get_field_id('list_style') ); ?>"><?php _e( 'Style:', 'novus' ); ?></label>
      <select name="<?php echo esc_attr( $this->get_field_name('list_style') ); ?>" id="<?php echo esc_attr( $this->get_field_id('list_style') ); ?>" class="widefat">
        <option value="rounded"<?php selected( $instance['list_style'], 'rounded' ); ?>><?php _e( 'Rounded', 'novus'); ?></option>
        <option value="rounded-big"<?php selected( $instance['list_style'], 'rounded-big' ); ?>><?php _e( 'Rounded Big', 'novus'); ?></option>
        <option value="squared"<?php selected( $instance['list_style'], 'squared' ); ?>><?php _e( 'Squared', 'novus' ); ?></option>
        <option value="squared-big"<?php selected( $instance['list_style'], 'squared-big' ); ?>><?php _e( 'Squared Big', 'novus' ); ?></option>
      </select>
    </p>
    <p>
      <label for="<?php echo esc_attr( $this->get_field_id( 'number' ) ); ?>"><?php _e( 'Number of posts to show:', 'novus' ); ?></label>
      <input id="<?php echo esc_attr( $this->get_field_id( 'number' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'number' ) ); ?>" type="text" value="<?php echo esc_attr( $number ); ?>" size="3" />
    </p>
    <hr>
    <p>
      <label for="<?php echo esc_attr( $this->get_field_id('time') ); ?>"><?php _e( 'Time Range (Popular):', 'novus'); ?></label>
      <select id="<?php echo esc_attr( $this->get_field_id('time') ); ?>" name="<?php echo esc_attr( $this->get_field_name('time') ); ?>">
        <option value="All time" <?php selected( $time, 'All time' ); ?>><?php _e( 'All time', 'novus' ) ?></option>
        <option value="1 year ago" <?php selected( $time, '1 year ago' ); ?>><?php _e( 'Last year', 'novus' ) ?></option>
        <option value="1 month ago" <?php selected( $time, '1 month ago' ); ?>><?php _e( 'Last month', 'novus' ) ?></option>
        <option value="1 week ago" <?php selected( $time, '1 week ago' ); ?>><?php _e( 'Last week', 'novus' ) ?></option>
      </select>
    </p>
    <?php
  }

  /**
   * Sanitize widget form values as they are saved.
   *
   * @see WP_Widget::update()
   *
   * @param array $new_instance Values just sent to be saved.
   * @param array $old_instance Previously saved values from database.
   *
   * @return array Updated safe values to be saved.
   */
  public function update( $new_instance, $old_instance ) {
    $instance = $old_instance;
    $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
    $instance['number'] = (int) $new_instance['number'];
    if ( in_array( $new_instance['list_type'], array( 'recent', 'popular', 'featured' ) ) ) {
      $instance['list_type'] = $new_instance['list_type'];
    } else {
      $instance['list_type'] = 'recent';
    }
    if ( in_array( $new_instance['list_style'], array( 'rounded', 'rounded-big', 'squared', 'squared-big' ) ) ) {
      $instance['list_style'] = $new_instance['list_style'];
    } else {
      $instance['list_style'] = 'rounded';
    }
    $instance['time'] = ( ! empty( $new_instance['time'] ) ) ? strip_tags( $new_instance['time'] ) : 'All time';

    return $instance;
  }

} // class Novus_Posts_List_Widget

add_action( 'widgets_init', function(){
  register_widget( 'Novus_Posts_List_Widget' );
});
