<?php
/**
 * Novus Flickr widget.
 */
class Novus_Flickr_Widget extends WP_Widget {

  /**
   * Register widget with WordPress.
   */
  function __construct() {
    parent::__construct(
      'flickr_photos', // Base ID
      __( 'Novus / Flickr', 'novus' ), // Name
      array( 'description' => __( 'Displays your latest Flickr photos.', 'novus' ), ) // Args
    );
  }

  /**
   * Front-end display of widget.
   *
   * @see WP_Widget::widget()
   *
   * @param array $args     Widget arguments.
   * @param array $instance Saved values from database.
   */
  public function widget( $args, $instance ) {

    echo $args['before_widget'];
    if ( ! empty( $instance['title'] ) ) {
      echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ). $args['after_title'];
    }

    $flickr_id = $instance['id'];
    $flickr_number = $instance['number'];
    ?>

      <div class="flickr-wrap">

        <script type="text/javascript" src="https://www.flickr.com/badge_code_v2.gne?count=<?php echo esc_attr( $flickr_number ); ?>&amp;display=latest&amp;size=s&amp;layout=x&amp;source=user&amp;user=<?php echo esc_attr( $flickr_id ); ?>"></script>

      </div>

    <?php
    echo $args['after_widget'];
  }

  /**
   * Back-end widget form.
   *
   * @see WP_Widget::form()
   *
   * @param array $instance Previously saved values from database.
   */
  public function form($instance) {
    $title = ! empty( $instance['title'] ) ? $instance['title'] : '';
    $flickr_id = ! empty( $instance['id'] ) ? $instance['id'] : '';
    $flickr_number = ! empty( $instance['number'] ) ? $instance['number'] : '';
    ?>
    <p>
      <label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php _e( 'Title', 'novus' ); ?>:
      <input id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" class="widefat" value="<?php echo esc_attr( $title ); ?>" /></label>
    </p>
    <p>
      <label for="<?php echo esc_attr( $this->get_field_id( 'id' ) ); ?>"><?php _e( 'Flickr ID (use <a target="_blank" href="http://www.idgettr.com">idGettr</a>)', 'novus' ); ?>:
      <input id="<?php echo esc_attr( $this->get_field_id( 'id' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'id' ) ); ?>" type="text" class="widefat" value="<?php echo esc_attr( $flickr_id ); ?>" /></label>
    </p>
    <p>
      <label for="<?php echo esc_attr( $this->get_field_id( 'number' ) ); ?>"><?php _e( 'Number of images to display', 'novus' ); ?>:
      <input id="<?php echo esc_attr( $this->get_field_id( 'number' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'number' ) ); ?>" type="text" class="widefat" value="<?php echo esc_attr( $flickr_number ); ?>" /></label>
    </p>
    <?php
  }

  /**
   * Sanitize widget form values as they are saved.
   *
   * @see WP_Widget::update()
   *
   * @param array $new_instance Values just sent to be saved.
   * @param array $old_instance Previously saved values from database.
   *
   * @return array Updated safe values to be saved.
   */
  public function update($new_instance, $old_instance) {
    $instance = $old_instance;

    $instance['title'] = strip_tags( $new_instance['title'] );
    $instance['id'] = preg_match( '|[0-9]{8}\@N([0-9]){2}|', $new_instance['id'] ) ? $new_instance['id'] : '';
    $instance['number'] = is_int( intval( $new_instance['number'] ) ) ? intval( $new_instance['number']): 6;

    return $instance;

  }
}

add_action( 'widgets_init', function(){
  register_widget( 'Novus_Flickr_Widget' );
});
