<?php
/**
 * Novus Latest Comments widget.
 */
class Novus_Latest_Comments_Widget extends WP_Widget {

  /**
   * Register widget with WordPress.
   */
  function __construct() {
    parent::__construct(
      'latest_comments', // Base ID
      __( 'Novus / Latest Comments', 'novus' ), // Name
      array( 'description' => __( 'Latest comments posted on site.', 'novus' ), ) // Args
    );
  }

  /**
   * Front-end display of widget.
   *
   * @see WP_Widget::widget()
   *
   * @param array $args     Widget arguments.
   * @param array $instance Saved values from database.
   */
  public function widget( $args, $instance ) {
    echo $args['before_widget'];

    $title = ( ! empty( $instance['title'] ) ) ? $instance['title'] : __( 'Latest Comments', 'novus' );
    $title = apply_filters( 'widget_title', $title, $instance );
    echo $args['before_title'] . $title . $args['after_title'];

    // Create WP Comment Query object
    $comment_query = new WP_Comment_Query();

    // Number of posts
    $number = ( ! empty( $instance['number'] ) ) ? absint( $instance['number'] ) : 5;

    // Query arguments
    $query_args = array(
      'number'  => $number,
      'orderby' => 'date',
      'status'  => 'approve'
    );

    $comments = $comment_query->query( $query_args );

    // List style
    $list_style = empty( $instance['list_style'] ) ? 'rounded' : $instance['list_style'];

    if ( $comments ) : ?>
      <ul class="posts_list--<?php echo esc_attr( $list_style ); ?>">
        <?php foreach ( $comments as $comment ) : ?>
          <li class="flag flag--top">
            <div class="flag-img">
              <a href="<?php echo get_permalink( $comment->comment_post_ID ); ?>#comment-<?php echo esc_attr( $comment->comment_ID ); ?>">
                <?php echo get_avatar( get_comment_author_email( $comment->comment_ID ), $size = '150' ); ?>
              </a>
            </div>
            <div class="flag-bd">
              <h3 class="post-title"><a href="<?php echo get_the_permalink( $comment->comment_post_ID ); ?>"><?php printf( __( '%1$s on %2$s', 'novus' ), get_comment_author( $comment->comment_ID ), get_the_title( $comment->comment_post_ID ) ); ?></a></h3>
              <div class="post-excerpt"><?php echo get_comment_excerpt( $comment->comment_ID ); ?></div>
              <?php echo '<span class="posted-on">' . human_time_diff( get_comment_date( 'U', $comment->comment_ID ), current_time( 'timestamp' ) ) . __( ' ago', 'novus' ) . '</span>'; ?>
            </div>
          </li>
        <?php endforeach; ?>
      </ul>
    <?php
    endif;

    echo $args['after_widget'];
  }

  /**
   * Back-end widget form.
   *
   * @see WP_Widget::form()
   *
   * @param array $instance Previously saved values from database.
   */
  public function form( $instance ) {
    $instance = wp_parse_args( (array) $instance, array( 'list_style' => 'rounded' ) );
    $title = ! empty( $instance['title'] ) ? $instance['title'] : '';
    $number = isset( $instance['number'] ) ? absint( $instance['number'] ) : 5;
    ?>
    <p>
      <label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php _e( 'Title:', 'novus' ); ?></label>
      <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
    </p>
    <p>
      <label for="<?php echo esc_attr( $this->get_field_id('list_style') ); ?>"><?php _e( 'Style:', 'novus' ); ?></label>
      <select name="<?php echo esc_attr( $this->get_field_name('list_style') ); ?>" id="<?php echo esc_attr( $this->get_field_id('list_style') ); ?>" class="widefat">
        <option value="rounded"<?php selected( $instance['list_style'], 'rounded' ); ?>><?php _e( 'Rounded', 'novus'); ?></option>
        <option value="rounded-big"<?php selected( $instance['list_style'], 'rounded-big' ); ?>><?php _e( 'Rounded Big', 'novus'); ?></option>
        <option value="squared"<?php selected( $instance['list_style'], 'squared' ); ?>><?php _e( 'Squared', 'novus' ); ?></option>
        <option value="squared-big"<?php selected( $instance['list_style'], 'squared-big' ); ?>><?php _e( 'Squared Big', 'novus' ); ?></option>
      </select>
    </p>
    <p>
      <label for="<?php echo esc_attr( $this->get_field_id( 'number' ) ); ?>"><?php _e( 'Number of posts to show:', 'novus' ); ?></label>
      <input id="<?php echo esc_attr( $this->get_field_id( 'number' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'number' ) ); ?>" type="text" value="<?php echo esc_attr( $number ); ?>" size="3" />
    </p>
    <?php
  }

  /**
   * Sanitize widget form values as they are saved.
   *
   * @see WP_Widget::update()
   *
   * @param array $new_instance Values just sent to be saved.
   * @param array $old_instance Previously saved values from database.
   *
   * @return array Updated safe values to be saved.
   */
  public function update( $new_instance, $old_instance ) {
    $instance = $old_instance;
    $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
    $instance['number'] = (int) $new_instance['number'];
    if ( in_array( $new_instance['list_style'], array( 'rounded', 'rounded-big', 'squared', 'squared-big' ) ) ) {
      $instance['list_style'] = $new_instance['list_style'];
    } else {
      $instance['list_style'] = 'rounded';
    }

    return $instance;
  }

} // class Novus_Latest_Comments_Widget

add_action( 'widgets_init', function(){
  register_widget( 'Novus_Latest_Comments_Widget' );
});
