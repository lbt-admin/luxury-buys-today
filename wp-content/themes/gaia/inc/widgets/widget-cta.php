<?php
/**
 * Call to Action widget.
 */
class Novus_CTA_Widget extends WP_Widget {

  /**
   * Register widget with WordPress.
   */
  function __construct() {
    parent::__construct(
      'cta', // Base ID
      __( 'Novus / Call to Action', 'novus' ), // Name
      array( 'description' => __( 'Simple call to action widget, add it to full width widget area.', 'novus' ), ) // Args
    );
  }

  /**
   * Front-end display of widget.
   *
   * @see WP_Widget::widget()
   *
   * @param array $args     Widget arguments.
   * @param array $instance Saved values from database.
   */
  public function widget( $args, $instance ) {
    echo $args['before_widget'];
    // Background color
    if ( ! empty( $instance['cta_bg_color'] ) && strlen( $instance['cta_bg_color'] ) === 7 ) {
      echo '<div class="widget-cta-wrap" style="background-color: ' . $instance['cta_bg_color'] . ';">';
    } else {
      echo '<div class="widget-cta-wrap">';
    }
    ?>
    <div class="container">
      <?php if ( ! empty( $instance['cta_title'] ) ) : ?>
      <h2 class="cta-title"><?php echo esc_html( $instance['cta_title'] ); ?></h2>
      <?php endif; ?>
      <?php if ( ! empty( $instance['cta_subtitle'] ) ) : ?>
      <h3 class="cta-subtitle"><?php echo esc_html( $instance['cta_subtitle'] ); ?></h3>
      <?php endif; ?>
      <?php if ( ! empty( $instance['cta_button_link'] ) ) : ?>
      <a href="<?php echo esc_url( $instance['cta_button_link'] ); ?>" class="btn btn--md btn--transparent"><?php echo esc_html( $instance['cta_button_text'] ); ?></a>
      <?php endif; ?>
    </div>
    <?php
    echo '</div>';
    echo $args['after_widget'];
  }

  /**
   * Back-end widget form.
   *
   * @see WP_Widget::form()
   *
   * @param array $instance Previously saved values from database.
   */
  public function form( $instance ) {
    $cta_title = ! empty( $instance['cta_title'] ) ? $instance['cta_title'] : '';
    $cta_subtitle = ! empty( $instance['cta_subtitle'] ) ? $instance['cta_subtitle'] : '';
    $cta_button_link = ! empty( $instance['cta_button_link'] ) ? $instance['cta_button_link'] : '';
    $cta_button_text = ! empty( $instance['cta_button_text'] ) ? $instance['cta_button_text'] : '';
    $cta_bg_color = ( ! empty( $instance['cta_bg_color'] ) && strlen( $instance['cta_bg_color'] ) === 7 ) ? $instance['cta_bg_color'] : '#f1f1ee';
    ?>
    <p>
    <label for="<?php echo esc_attr( $this->get_field_id( 'cta_title' ) ); ?>"><?php _e( 'Title:', 'novus' ); ?></label>
    <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'cta_title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'cta_title' ) ); ?>" type="text" value="<?php echo esc_attr( $cta_title ); ?>">
    </p>
    <p>
    <label for="<?php echo esc_attr( $this->get_field_id( 'cta_subtitle' ) ); ?>"><?php _e( 'Subtitle:', 'novus' ); ?></label>
    <textarea class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'cta_subtitle' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'cta_subtitle' ) ); ?>" rows="3"><?php echo esc_attr( $cta_subtitle ); ?></textarea>
    </p>
    <p>
    <label for="<?php echo esc_attr( $this->get_field_id( 'cta_button_text' ) ); ?>"><?php _e( 'Button text:', 'novus' ); ?></label>
    <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'cta_button_text' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'cta_button_text' ) ); ?>" type="text" value="<?php echo esc_attr( $cta_button_text ); ?>">
    </p>
    <p>
    <label for="<?php echo esc_attr( $this->get_field_id( 'cta_button_link' ) ); ?>"><?php _e( 'Button link:', 'novus' ); ?></label>
    <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'cta_button_link' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'cta_button_link' ) ); ?>" type="text" value="<?php echo esc_attr( $cta_button_link ); ?>">
    </p>
    <p>
    <label for="<?php echo esc_attr( $this->get_field_id( 'cta_bg_color' ) ); ?>"><?php _e( 'Background color (hex format):', 'novus' ); ?></label>
    <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'cta_bg_color' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'cta_bg_color' ) ); ?>" type="text" value="<?php echo esc_attr( $cta_bg_color ); ?>">
    </p>
    <?php
  }

  /**
   * Sanitize widget form values as they are saved.
   *
   * @see WP_Widget::update()
   *
   * @param array $new_instance Values just sent to be saved.
   * @param array $old_instance Previously saved values from database.
   *
   * @return array Updated safe values to be saved.
   */
  public function update( $new_instance, $old_instance ) {
    $instance = array();
    $instance['cta_title'] = ( ! empty( $new_instance['cta_title'] ) ) ? strip_tags( $new_instance['cta_title'] ) : '';
    $instance['cta_subtitle'] = ( ! empty( $new_instance['cta_subtitle'] ) ) ? strip_tags( $new_instance['cta_subtitle'] ) : '';
    $instance['cta_button_text'] = ( ! empty( $new_instance['cta_button_text'] ) ) ? strip_tags( $new_instance['cta_button_text'] ) : '';
    $instance['cta_button_link'] = ( ! empty( $new_instance['cta_button_link'] ) ) ? esc_url_raw( $new_instance['cta_button_link'] ) : '';
    $instance['cta_bg_color'] = ( ! empty( $new_instance['cta_bg_color'] ) ) ? esc_attr( $new_instance['cta_bg_color'] ) : '';

    return $instance;
  }

} // class Novus_CTA_Widget

add_action( 'widgets_init', function(){
  register_widget( 'Novus_CTA_Widget' );
});
