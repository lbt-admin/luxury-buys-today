<?php
/**
 * Custom template tags for this theme.
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package Novus
 */

if ( ! function_exists( 'the_posts_navigation' ) ) :
	/**
	 * Display navigation to next/previous set of posts when applicable.
	 *
	 * @todo Remove this function when WordPress 4.3 is released.
	 */
	function the_posts_navigation() {
		// Don't print empty markup if there's only one page.
		if ( $GLOBALS['wp_query']->max_num_pages < 2 ) {
			return;
		}
		?>
		<nav class="navigation posts-navigation" role="navigation">
			<h2 class="screen-reader-text"><?php _e( 'Posts navigation', 'novus' ); ?></h2>
			<div class="nav-links">

				<?php if ( get_next_posts_link() ) : ?>
					<div class="nav-previous"><?php next_posts_link( __( 'Older posts', 'novus' ) ); ?></div>
				<?php endif; ?>

				<?php if ( get_previous_posts_link() ) : ?>
					<div class="nav-next"><?php previous_posts_link( __( 'Newer posts', 'novus' ) ); ?></div>
				<?php endif; ?>

			</div><!-- .nav-links -->
		</nav><!-- .navigation -->
		<?php
	}
endif;

if ( ! function_exists( 'the_post_navigation' ) ) :
	/**
	 * Display navigation to next/previous post when applicable.
	 *
	 * @todo Remove this function when WordPress 4.3 is released.
	 */
	function the_post_navigation() {
		// Don't print empty markup if there's nowhere to navigate.
		$previous = ( is_attachment() ) ? get_post( get_post()->post_parent ) : get_adjacent_post( false, '', true );
		$next     = get_adjacent_post( false, '', false );

		if ( ! $next && ! $previous ) {
			return;
		}
		?>
		<nav class="navigation post-navigation" role="navigation">
			<h2 class="screen-reader-text"><?php _e( 'Post navigation', 'novus' ); ?></h2>
			<div class="nav-links">
				<?php
				previous_post_link( '<div class="nav-previous">%link</div>', '%title' );
				next_post_link( '<div class="nav-next">%link</div>', '%title' );
				?>
			</div><!-- .nav-links -->
		</nav><!-- .navigation -->
		<?php
	}
endif;

if ( ! function_exists( 'novus_posted_on' ) ) :
	/**
	 * Prints HTML with meta information for the current post-date/time and author.
	 */
	function novus_posted_on( $show_author = true ) {
		$relative_time_display = get_theme_mod( 'relative_time_display', true );

		if ( ! empty( $relative_time_display ) ) {
			echo '<span class="posted-on">' . human_time_diff( get_the_time( 'U' ), current_time( 'timestamp' ) ) . __( ' ago', 'novus' ) . '</span>';
		} else {
			$time_string = '<time class="entry-date published updated" datetime="%1$s">%2$s</time>';
			if ( get_the_time( 'U' ) !== get_the_modified_time( 'U' ) ) {
				$time_string = '<time class="entry-date published" datetime="%1$s">%2$s</time><time class="updated" datetime="%3$s">%4$s</time>';
			}

			$time_string = sprintf( $time_string,
				esc_attr( get_the_date( 'c' ) ),
				esc_html( get_the_date() ),
				esc_attr( get_the_modified_date( 'c' ) ),
				esc_html( get_the_modified_date() )
			);

			$posted_on = sprintf(
				_x( '%s', 'post date', 'novus' ),
				$time_string
			);

			echo '<span class="posted-on">' . $posted_on . '</span>';
		}
		if ( true === $show_author ) {
			$byline = sprintf(
				esc_html_x( '%s', 'post author', 'novus' ),
				'<span class="author vcard"><a class="url fn n" href="' . esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ) . '">' . esc_html( get_the_author() ) . '</a></span>'
			);
			echo $byline;
		}

	}
endif;

if ( ! function_exists( 'novus_posted_on_old' ) ) :
	/**
	 * Prints HTML with meta information for the current post-date/time and author.
	 */
	function novus_posted_on_old() {
		$time_string = '<time class="entry-date published updated" datetime="%1$s">%2$s</time>';
		if ( get_the_time( 'U' ) !== get_the_modified_time( 'U' ) ) {
			$time_string = '<time class="entry-date published" datetime="%1$s">%2$s</time><time class="updated" datetime="%3$s">%4$s</time>';
		}

		$time_string = sprintf( $time_string,
			esc_attr( get_the_date( 'c' ) ),
			esc_html( get_the_date() ),
			esc_attr( get_the_modified_date( 'c' ) ),
			esc_html( get_the_modified_date() )
		);

		$posted_on = sprintf(
			_x( '%s', 'post date', 'novus' ),
			$time_string
		);

		echo '<span class="posted-on">' . $posted_on . '</span>';

	}
endif;

if ( ! function_exists( 'novus_entry_meta' ) ) :
	/**
	 * Prints HTML with meta information.
	 */
	function novus_entry_meta() {
		// Hide category and tag text for pages.
		if ( 'post' == get_post_type() ) {
			/* translators: used between list items, there is a space after the comma */
			$categories_list = get_the_category_list( ' ' );
			if ( $categories_list && novus_categorized_blog() ) {
				printf( '<span class="cat-links">' . __( '%1$s', 'novus' ) . '</span>', $categories_list );
			}
		}
	}
endif;

if ( ! function_exists( 'novus_entry_footer' ) ) :
	/**
	 * Prints HTML with meta information for the categories, tags and comments.
	 */
	function novus_entry_footer( $show_tags = true ) {

		// Hide category and tag text for pages.
		if ( 'post' == get_post_type() && true === $show_tags ) {
			/* translators: used between list items, there is a space after the comma */
			// $categories_list = get_the_category_list( __( ', ', 'novus' ) );
			// if ( $categories_list && novus_categorized_blog() ) {
			// 	printf( '<span class="cat-links">' . __( 'Posted in %1$s', 'novus' ) . '</span>', $categories_list );
			// }

			/* translators: used between list items, there is a space after the comma */
			$tags_list = get_the_tag_list( '', __( ', ', 'novus' ) );
			if ( $tags_list ) {
				printf( '<span class="tags-links">%1$s</span>', $tags_list );
			}
		}

		if ( ! is_single() && ! post_password_required() && ( comments_open() || get_comments_number() ) ) {
			echo '<span class="comments-link">';
			comments_popup_link( __( 'Comment', 'novus' ), __( '1 Comment', 'novus' ), __( '% Comments', 'novus' ) );
			echo '</span>';
		}

		edit_post_link( __( 'Edit', 'novus' ), '<span class="edit-link">', '</span>' );

	}
endif;

if ( ! function_exists( 'the_archive_title' ) ) :
	/**
	 * Shim for `the_archive_title()`.
	 *
	 * Display the archive title based on the queried object.
	 *
	 * @todo Remove this function when WordPress 4.3 is released.
	 *
	 * @param string $before Optional. Content to prepend to the title. Default empty.
	 * @param string $after  Optional. Content to append to the title. Default empty.
	 */
	function the_archive_title( $before = '', $after = '' ) {
		if ( is_category() ) {
			$title = sprintf( __( 'Category: %s', 'novus' ), single_cat_title( '', false ) );
		} elseif ( is_tag() ) {
			$title = sprintf( __( 'Tag: %s', 'novus' ), single_tag_title( '', false ) );
		} elseif ( is_author() ) {
			$title = sprintf( __( 'Author: %s', 'novus' ), '<span class="vcard">' . get_the_author() . '</span>' );
		} elseif ( is_year() ) {
			$title = sprintf( __( 'Year: %s', 'novus' ), get_the_date( _x( 'Y', 'yearly archives date format', 'novus' ) ) );
		} elseif ( is_month() ) {
			$title = sprintf( __( 'Month: %s', 'novus' ), get_the_date( _x( 'F Y', 'monthly archives date format', 'novus' ) ) );
		} elseif ( is_day() ) {
			$title = sprintf( __( 'Day: %s', 'novus' ), get_the_date( _x( 'F j, Y', 'daily archives date format', 'novus' ) ) );
		} elseif ( is_tax( 'post_format' ) ) {
			if ( is_tax( 'post_format', 'post-format-aside' ) ) {
				$title = _x( 'Asides', 'post format archive title', 'novus' );
			} elseif ( is_tax( 'post_format', 'post-format-gallery' ) ) {
				$title = _x( 'Galleries', 'post format archive title', 'novus' );
			} elseif ( is_tax( 'post_format', 'post-format-image' ) ) {
				$title = _x( 'Images', 'post format archive title', 'novus' );
			} elseif ( is_tax( 'post_format', 'post-format-video' ) ) {
				$title = _x( 'Videos', 'post format archive title', 'novus' );
			} elseif ( is_tax( 'post_format', 'post-format-quote' ) ) {
				$title = _x( 'Quotes', 'post format archive title', 'novus' );
			} elseif ( is_tax( 'post_format', 'post-format-link' ) ) {
				$title = _x( 'Links', 'post format archive title', 'novus' );
			} elseif ( is_tax( 'post_format', 'post-format-status' ) ) {
				$title = _x( 'Statuses', 'post format archive title', 'novus' );
			} elseif ( is_tax( 'post_format', 'post-format-audio' ) ) {
				$title = _x( 'Audio', 'post format archive title', 'novus' );
			} elseif ( is_tax( 'post_format', 'post-format-chat' ) ) {
				$title = _x( 'Chats', 'post format archive title', 'novus' );
			}
		} elseif ( is_post_type_archive() ) {
			$title = sprintf( __( 'Archives: %s', 'novus' ), post_type_archive_title( '', false ) );
		} elseif ( is_tax() ) {
			$tax = get_taxonomy( get_queried_object()->taxonomy );
			/* translators: 1: Taxonomy singular name, 2: Current taxonomy term */
			$title = sprintf( __( '%1$s: %2$s', 'novus' ), $tax->labels->singular_name, single_term_title( '', false ) );
		} else {
			$title = __( 'Archives', 'novus' );
		}

		/**
		 * Filter the archive title.
		 *
		 * @param string $title Archive title to be displayed.
		 */
		$title = apply_filters( 'get_the_archive_title', $title );

		if ( ! empty( $title ) ) {
			echo $before . $title . $after;
		}
	}
endif;

if ( ! function_exists( 'novus_archives_author_title' ) ) :
	/**
	 * Show author image in author archives
	 *
	 * @param  string $title
	 *
	 * @return string
	 */
	function novus_archives_author_title( $title ) {
		if ( is_author() ) {
			return get_avatar( get_the_author_meta( 'email' ), '101' ) . '<br>' . $title;
		}

		return $title;
	}
endif;
add_filter( 'get_the_archive_title', 'novus_archives_author_title' );

if ( ! function_exists( 'the_archive_description' ) ) :
	/**
	 * Shim for `the_archive_description()`.
	 *
	 * Display category, tag, or term description.
	 *
	 * @todo Remove this function when WordPress 4.3 is released.
	 *
	 * @param string $before Optional. Content to prepend to the description. Default empty.
	 * @param string $after  Optional. Content to append to the description. Default empty.
	 */
	function the_archive_description( $before = '', $after = '' ) {
		$description = apply_filters( 'get_the_archive_description', term_description() );

		if ( ! empty( $description ) ) {
			/**
			 * Filter the archive description.
			 *
			 * @see term_description()
			 *
			 * @param string $description Archive description to be displayed.
			 */
			echo $before . $description . $after;
		}
	}
endif;

/**
 * Returns true if a blog has more than 1 category.
 *
 * @return bool
 */
function novus_categorized_blog() {
	if ( false === ( $all_the_cool_cats = get_transient( 'novus_categories' ) ) ) {
		// Create an array of all the categories that are attached to posts.
		$all_the_cool_cats = get_categories( array(
			'fields'     => 'ids',
			'hide_empty' => 1,

			// We only need to know if there is more than one category.
			'number'     => 2,
		) );

		// Count the number of categories that are attached to the posts.
		$all_the_cool_cats = count( $all_the_cool_cats );

		set_transient( 'novus_categories', $all_the_cool_cats );
	}

	if ( $all_the_cool_cats > 1 ) {
		// This blog has more than 1 category so novus_categorized_blog should return true.
		return true;
	} else {
		// This blog has only 1 category so novus_categorized_blog should return false.
		return false;
	}
}

/**
 * Flush out the transients used in novus_categorized_blog.
 */
function novus_category_transient_flusher() {
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return;
	}
	// Like, beat it. Dig?
	delete_transient( 'novus_categories' );
}

add_action( 'edit_category', 'novus_category_transient_flusher' );
add_action( 'save_post', 'novus_category_transient_flusher' );


if ( ! function_exists( 'novus_related_posts' ) ) :
	/**
	 * Shows related posts from same category
	 *
	 * @param  integer $showposts Number of posts
	 * @param  string  $orderby   Ordering
	 *
	 * @return string
	 */
	function novus_related_posts( $showposts = 3, $orderby = "rand" ) {
		global $post;

		// Get post categories
		$post_taxonomies        = array();
		$post_taxonomies_result = wp_get_object_terms( $post->ID, 'category' );
		foreach ( $post_taxonomies_result as $taxonomy ) {
			$post_taxonomies[] = $taxonomy->term_id;
		}

		// Get post ID
		$current_post_id = array( $post->ID );

		// get posts
		$related_posts = new WP_Query();
		$args          = array(
			'orderby'      => $orderby,
			'showposts'    => $showposts,
			'post__not_in' => $current_post_id,
			'category__in' => $post_taxonomies,
		);
		$related_posts->query( $args );

		// display posts
		if ( $related_posts->have_posts() ) :
			echo '<div class="related-posts">';
			echo '<h3 class="related-posts-title">' . __( 'You may also like', 'novus' ) . '</h3>';
			echo '<ul class="related-posts-list clearfix">';
			while ( $related_posts->have_posts() ) : $related_posts->the_post(); ?>
				<li>
					<a href="<?php the_permalink() ?>" class="related-post-link">
						<?php if ( has_post_thumbnail() ) : ?>
							<?php the_post_thumbnail( 'thumbnail' ); ?>
						<?php else : ?>
							<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/thumb.svg" alt="thumbnail">
						<?php endif; ?>
						<?php the_title( '<h4 class="related-post-title">', '</h4>' ); ?>
					</a>
				</li>
			<?php
			endwhile;
			echo '</ul>';
			echo '</div>';
		endif;
		wp_reset_postdata();
	}
endif;


if ( ! function_exists( 'novus_share_post' ) ) :
	/**
	 * Displays post's share links
	 * @return string
	 */
	function novus_share_post() {
		global $post;
		$url       = urlencode( esc_url( get_permalink( $post->ID ) ) );
		$title     = esc_attr( $post->post_title );
		$thumb_url = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'post-thumb' );
		if ( $thumb_url == false ) {
			$thumb_url = wpr_get_external_image( $post->ID );
		}

		if ( $thumb_url == false ) {
			$social_options = get_option( 'wpseo_social' );
			if ( ! empty( $social_options['og_default_image'] ) ) {
				$thumb_url    = array();
				$thumb_url[0] = $social_options['og_default_image'];
			}
		}

		?>
		<div class="entry-share">
			<a class="genericon genericon-facebook-alt" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $url; ?>" target="_blank"></a>
			<a class="genericon genericon-twitter" href="https://twitter.com/intent/tweet/?text=<?php echo $title; ?>&url=<?php echo $url; ?>" target="_blank"></a>
			<a class="genericon genericon-linkedin" href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo $url; ?>&title=<?php echo $title; ?>" target="_blank"></a>
			<a class="genericon genericon-googleplus" href="https://plus.google.com/share?url=<?php echo $url; ?>" target="_blank"></a>
			<a class="genericon genericon-pinterest" data-pin-do="buttonPin" href="https://www.pinterest.com/pin/create/button/?url=<?php echo $url; ?>&media=<?php echo $thumb_url[0]; ?>&description=<?php echo $title; ?>" data-pin-custom="true"></a>
			<a class="genericon genericon-reddit" href="http://www.reddit.com/submit/?url=<?php echo $url; ?>" target="_blank"></a>
			<a class="genericon genericon-mail" href="mailto:?subject=<?php echo $title; ?>&body=<?php _e( 'Check out this great post I read on', 'novus' ); ?> <?php echo bloginfo( 'name' ); ?>: <?php echo $title; ?> - <?php echo $url; ?>" target="_blank"></a>
		</div>
		<?php
	}
endif;


if ( ! function_exists( 'novus_featured_posts' ) ) :
	/**
	 * Shows featured posts that have featured tag
	 *
	 * @param  integer $showposts Number of posts
	 * @param  string  $orderby   Ordering
	 *
	 * @return string
	 */
	function novus_featured_posts( $showposts = 6, $orderby = 'rand', $posted_on = false ) {
		global $post;

		// get posts
		$featured_posts  = new WP_Query();
		$featured_tag_id = get_theme_mod( 'featured_posts_tag', false );
		if ( empty( $featured_tag_id ) ) {
			return;
		}
		$featured_posts_args = array(
			'orderby'   => $orderby,
			'showposts' => $showposts,
			'tag_id'    => $featured_tag_id,
		);
		$featured_posts->query( $featured_posts_args );

		// display posts
		if ( $featured_posts->have_posts() ) :
			echo '<div class="container">';
			echo '<div class="featured-posts featured-posts--' . get_theme_mod( 'featured_posts_style', 'default' ) . '">';
			echo '<ul class="featured-posts-list clearfix js-slider-featured">';
			while ( $featured_posts->have_posts() ) : $featured_posts->the_post(); ?>
				<li class="effect-chico">
					<?php if ( has_post_thumbnail() ) : ?>
						<?php the_post_thumbnail( 'featured-thumb' ); ?>
					<?php else : ?>
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/featured-thumb.svg" alt="thumbnail">
					<?php endif; ?>
					<div class="featured-post-content">
						<?php the_title( '<h2 class="featured-post-title">', '</h2>' ); ?>
						<div class="featured-post-excerpt"><?php echo wp_trim_words( get_the_excerpt(), 20 ); ?></div>
						<a href="<?php echo esc_url( get_permalink() ); ?>"><?php _e( 'Read more', 'novus' ); ?></a>
					</div>
				</li>
			<?php
			endwhile;
			echo '</ul>';
			echo '</div>';
			echo '</div>';
		endif;
		wp_reset_postdata();
	}
endif;


if ( ! function_exists( 'novus_get_link_url' ) ) :
	/**
	 * Return the post URL.
	 *
	 * Falls back to the post permalink if no URL is found in the post.
	 *
	 * @see get_url_in_content()
	 *
	 * @return string The Link format URL.
	 */
	function novus_get_link_url() {
		$has_url = get_url_in_content( get_the_content() );

		return $has_url ? $has_url : apply_filters( 'the_permalink', get_permalink() );
	}
endif;
