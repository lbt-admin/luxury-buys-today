<?php

function novus_shortcode_button( $atts, $content ) {
	$a = shortcode_atts( array(
		'link' => '#',
		'class' => ''
	), $atts );

	ob_start(); ?>
	<p><a class="btn <?php echo esc_attr( $a['class'] ); ?>" href="<?php echo esc_url( $a['link'] ); ?>"><?php echo esc_html( $content ); ?></a></p>
	<?php return ob_get_clean();
}
add_shortcode( 'button', 'novus_shortcode_button' );
