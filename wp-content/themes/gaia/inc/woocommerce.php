<?php
/**
 * WooCommerce customizations and support.
 *
 * @package Novus
 */

// Declare WooCommerce support
add_action( 'after_setup_theme', 'novus_woocommerce_support' );
function novus_woocommerce_support() {
	add_theme_support( 'woocommerce' );
	add_theme_support( 'wc-product-gallery-zoom' );
	add_theme_support( 'wc-product-gallery-lightbox' );
	add_theme_support( 'wc-product-gallery-slider' );
}

remove_action( 'woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10 );
remove_action( 'woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10 );

add_action( 'woocommerce_before_main_content', 'novus_woocommerce_wrapper_start', 10 );
add_action( 'woocommerce_after_main_content', 'novus_woocommerce_wrapper_end', 10 );

function novus_woocommerce_wrapper_start() {
	echo '<div id="primary" class="content-area">';
}
function novus_woocommerce_wrapper_end() {
	echo '</div>';
}

// Remove sidebar from product pages
remove_action( 'woocommerce_sidebar', 'woocommerce_get_sidebar', 10 );

// Display 12 products per page
add_filter( 'loop_shop_per_page', function() {
	return 12;
}, 20 );

// Change number or products per row to 3
add_filter( 'loop_shop_columns', 'loop_columns' );
if ( ! function_exists( 'loop_columns' ) ) {
	function loop_columns() {
		return 3; // 3 products per row
	}
}

// Upsell products display options
remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_upsell_display', 15 );
add_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_upsells', 15 );
if ( ! function_exists( 'woocommerce_output_upsells' ) ) {
	function woocommerce_output_upsells() {
		woocommerce_upsell_display( 3, 3 ); // Display 3 products in rows of 3
	}
}

// Related products display options
add_filter( 'woocommerce_output_related_products_args', 'novus_related_products_args' );
function novus_related_products_args( $args ) {
	$args['posts_per_page'] = 3; // 4 related products
	$args['columns'] = 3; // arranged in 2 columns
	return $args;
}

// Cross sells
function novus_cross_sells_number( $number ) {
	return 2;
}
add_filter( 'woocommerce_cross_sells_total', 'novus_cross_sells_number', 10 );


// Ensure cart contents update when products are added to the cart via AJAX (place the following in functions.php)
add_filter( 'woocommerce_add_to_cart_fragments', 'woocommerce_header_add_to_cart_fragment' );
function woocommerce_header_add_to_cart_fragment( $fragments ) {
	ob_start();
	?>
	<a class="cart-contents" href="<?php echo WC()->cart->get_cart_url(); ?>" title="<?php _e( 'View your shopping cart', 'novus' ); ?>"><span class="genericon genericon-cart"></span> <?php echo sprintf ( _n( '%d item', '%d items', WC()->cart->cart_contents_count, 'novus' ), WC()->cart->cart_contents_count ); ?> - <?php echo WC()->cart->get_cart_total(); ?></a>
	<?php
	$fragments['a.cart-contents'] = ob_get_clean();
	return $fragments;
}


// Display the second thumbnails
function woocommerce_template_loop_second_product_thumbnail() {
	global $product, $woocommerce;

	$attachment_ids = $product->get_gallery_image_ids();

	if ( $attachment_ids ) {
		$secondary_image_id = $attachment_ids['0'];
		echo wp_get_attachment_image( $secondary_image_id, 'shop_catalog', '', $attr = array( 'class' => 'alt-image attachment-shop-catalog' ) );
	}
}
add_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_second_product_thumbnail' );
