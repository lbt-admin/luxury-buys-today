var novus = novus || {};
jQuery(document).ready(function($) {
  "use strict";

  /**
   * Preloader
   */
  $('.site-main').imagesLoaded( function() {
    $('.js-loader').fadeOut(600);
  });

  /**
   * Add class if it's scrolled already
   */
  $('body').toggleClass('tiny', $(document).scrollTop() > 100);

  /**
   * Add class after scroll
   */
  $(window).on("scroll touchmove", function () {
    $('body').toggleClass('tiny', $(document).scrollTop() > 100);
  });

  /**
   * Toggle search
   */
  $('.search-toggle, .site-search-close').on('click', function(event){
    event.preventDefault();
    $('body').toggleClass('search-visible');
    if ( $(this).hasClass('search-toggle') ) {
      $('.site-search .search-field').focus().select();
    }
  });

  /**
   * Fitvids.js
   */
  $('.entry-content').fitVids();

  /**
   * Fastclick
   */
  // Add .needsclick class to disable Fastclick for main navigation
  $('.main-navigation a').addClass('needsclick');
  FastClick.attach(document.body);

  /**
   * Slider activation
   */
  $('.js-slider').slick({
    speed: 500,
    draggable: false,
    infinite: false,
    rtl: "undefined" != typeof novus.rtl && novus.rtl === true ? true : false,
    cssEase: 'cubic-bezier(0.22, 0.61, 0.36, 1)',
    prevArrow: '<a class="btn slider-arrow slider-arrow-prev"><span class="genericon genericon-leftarrow"></span></a>',
    nextArrow: '<a class="btn slider-arrow slider-arrow-next"><span class="genericon genericon-rightarrow"></span></a>'
  });
  var slider_featured_options = {
    speed: 800,
    draggable: true,
    autoplay: true,
    rtl: "undefined" != typeof novus.rtl && novus.rtl === true ? true : false,
    cssEase: 'cubic-bezier(0.22, 0.61, 0.36, 1)',
    prevArrow: '<a class="btn slider-arrow slider-arrow-prev"><span class="genericon genericon-leftarrow"></span></a>',
    nextArrow: '<a class="btn slider-arrow slider-arrow-next"><span class="genericon genericon-rightarrow"></span></a>',
  };
  // Full width masonry
  if ( "undefined" != typeof novus.layout && novus.layout === 'masonry-full' ) {
    $.extend(slider_featured_options, {
      slidesToShow: 3,
      slidesToScroll: 3,
      responsive: [
        {
          breakpoint: 1470,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2,
          }
        },
        {
          breakpoint: 992,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false
          }
        },
      ]
    });
  } else {
    $.extend(slider_featured_options, {
      slidesToShow: 2,
      slidesToScroll: 2,
      responsive: [
        {
          breakpoint: 992,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false
          }
        },
      ]
    });
  }
  $('.js-slider-featured').slick(slider_featured_options);

  /**
   * Activate masonry
   */
  if ( "undefined" != typeof novus.layout && ( novus.layout === 'masonry' || novus.layout === 'masonry-full' ) ) {
    $('.js-masonry-wrap').imagesLoaded( function() {
      setTimeout(function () {
      $('.js-masonry-wrap').masonry({
        // options
        itemSelector: '.hentry--masonry',
        columnWidth: '.hentry--masonry',
        percentPosition: true
      });
      }, 3000);
    });
  }

  /**
   * Activate popup
   */
  $('.entry-image a, .entry-content a[href$="jpg"], .entry-content a[href$="jpeg"], .entry-content a[href$="png"], .entry-content a[href$="gif"], .js-slider-link').magnificPopup(popupCfg);
  // Gallery popup
  $('.slider-gallery').each(function() {
    $(this).find('div a').magnificPopup(popupCfgGallery);
  });
  $('.gallery').each(function() {
    $(this).find('a').magnificPopup(popupCfgGallery);
  });

  // Check if JetPack is installed
  if ( "undefined" != typeof novus.jetpack && novus.jetpack.active === true ) {
    /**
     * Reactivate scripts after Infinite scroll load
     */
    jQuery(document.body).on('post-load', function () {
      $('.entry-content').fitVids();
      $('.js-slider').slick({
        speed: 400,
        draggable: false,
        infinite: false,
        rtl: "undefined" != typeof novus.rtl && novus.rtl === true ? true : false,
        cssEase: 'cubic-bezier(0.22, 0.61, 0.36, 1)',
        prevArrow: '<a class="btn slider-arrow slider-arrow-prev"><span class="genericon genericon-leftarrow"></span></a>',
        nextArrow: '<a class="btn slider-arrow slider-arrow-next"><span class="genericon genericon-rightarrow"></span></a>'
      });
      $('.entry-image a, .entry-content a[href$="jpg"], .entry-content a[href$="jpeg"], .entry-content a[href$="png"], .entry-content a[href$="gif"], .js-slider-link').magnificPopup(popupCfg);
      $('.slider-gallery').each(function() {
        $(this).find('div a').magnificPopup(popupCfgGallery);
      });
      $('.gallery').each(function() {
        $(this).find('a').magnificPopup(popupCfgGallery);
      });
    });
  }

});

/**
 * Popup config
 */
var popupCfg = {
  type: 'image',
  removalDelay: 250,
  mainClass: 'mfp-novus',
  closeOnContentClick: true,
  showCloseBtn: false
};

/**
 * Popup config gallery
 */
var popupCfgGallery = {
  type: 'image',
  removalDelay: 250,
  mainClass: 'mfp-novus',
  closeOnContentClick: true,
  showCloseBtn: false,
  gallery: {
    enabled: true
  }
};
