/**
 * Theme Customizer enhancements for a better user experience.
 *
 * Contains handlers to make Theme Customizer preview reload changes asynchronously.
 */

( function( $ ) {
	// Site title and description.
	wp.customize( 'blogname', function( value ) {
		value.bind( function( to ) {
			if ( !$( '.site-title a img' ).length ) {
				$( '.site-title a' ).text( to );
			}
		} );
	} );
	wp.customize( 'blogdescription', function( value ) {
		value.bind( function( to ) {
			$( '.site-description' ).text( to );
		} );
	} );
	wp.customize( 'header_tagline_show', function( value ) {
		value.bind( function( to ) {
			if ( to == 1 ) {
				$( '.site-description' ).fadeIn();
			} else {
				$( '.site-description' ).fadeOut();
			}
		} );
	} );

	// Header text color.
	wp.customize( 'header_textcolor', function( value ) {
		value.bind( function( to ) {
			if ( 'blank' === to ) {
				$( '.site-title, .site-description' ).css( {
					'clip': 'rect(1px, 1px, 1px, 1px)',
					'position': 'absolute'
				} );
			} else {
				$( '.site-title, .site-title a, .site-description' ).css( {
					'clip': 'auto',
					'color': to,
					'position': 'relative'
				} );
			}
		} );
	} );

	// Base text color.
	wp.customize( 'color_base', function( value ) {
		value.bind( function( to ) {
			$( 'body' ).css( 'color', to );
		} );
	} );

	// Theme color A.
	wp.customize( 'color_theme_a', function( value ) {
		value.bind( function( to ) {
			$( '.entry-meta, .entry-footer, .comment-meta .comment-date, .comment-meta .comment-awaiting-moderation, .site-info, .entry-author-content p, .format-gallery .entry-image span, .format-link .entry-image span, .widget_recent_entries .post-date, .taxonomy-description' ).css( 'color', to );
		} );
	} );

	// Theme color B.
	wp.customize( 'color_theme_b', function( value ) {
		value.bind( function( to ) {
			$( '.widget-title, .page-title, .entry-title, .entry-content h1, .comment-content h1, .entry-content h2, .comment-content h2, .entry-content h3, .comment-content h3, .entry-content h4, .comment-content h4, .entry-content h5, .comment-content h5, .entry-content h6, .comment-content h6, .entry-content q, .comment-body q, .related-posts-title, .comments-title, .comment-reply-title' ).css( 'color', to );
		} );
	} );

	// Theme color C.
	wp.customize( 'color_theme_c', function( value ) {
		value.bind( function( to ) {
			$( '.hentry:after, .page-content:after .comment-meta, .entry-share a' ).css( 'border-color', to );
		} );
	} );

	// Theme color D.
	wp.customize( 'color_theme_d', function( value ) {
		value.bind( function( to ) {
			$( '.entry-title, .widget-title' ).css( 'border-color', to );
			$( '.woocommerce span.onsale' ).css( 'background-color', to );
			$( '.woocommerce ul.products li.product .price, .woocommerce div.product p.price, .woocommerce div.product span.price' ).css( 'color', to );
		} );
	} );

	// Footer bg color
	wp.customize( 'color_footer_bg', function( value ) {
		value.bind( function( to ) {
			$( '.site-footer' ).css( 'background-color', to );
		} );
	} );

	// Post content color
	wp.customize( 'color_content', function( value ) {
		value.bind( function( to ) {
			$( '.layout-list .content-area, .layout-full-width .content-area, .layout-classic .content-area, .layout-sidebar-right .content-area, .woocommerce-page .content-area, .hentry--masonry .entry-wrap, .hentry--grid .entry-footer, .layout-masonry .page-header, .layout-grid .page-header, .featured-posts' ).css( 'background-color', to );
		} );
	} );

	// Footer text
	wp.customize( 'footer_text', function( value ) {
		value.bind( function( to ) {
			$( '.site-info' ).html( to );
		} );
	} );

} )( jQuery );
