<?php
/**
 * Novus functions and definitions
 *
 * @package Novus
 */

define( 'NOVUS_WOOCOMMERCE_ACTIVE', in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) );

/**
 * Set the content width based on the theme's design and stylesheet.
 */
if ( ! isset( $content_width ) ) {
	if ( get_theme_mod( 'article_layout' ) == 'full-width' ) {
		$content_width = 960; /* pixels */
	} else {
		$content_width = 605; /* pixels */
	}
}

if ( ! function_exists( 'novus_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function novus_setup() {

	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on Novus, use a find and replace
	 * to change 'novus' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'novus', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	add_theme_support( 'post-thumbnails' );
	add_image_size( 'post-thumb', 605, 9999 );
	add_image_size( 'post-thumb-cropped', 605, 454, true );
	add_image_size( 'post-thumb-full-width-cropped', 960, 720, true );
	add_image_size( 'post-thumb-header', 1440, 370, true );
	add_image_size( 'featured-thumb', 713, 401, true ); // Featured posts

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => __( 'Primary Menu', 'novus' ),
		'footer'  => __( 'Footer Menu', 'novus' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form', 'comment-form', 'comment-list', 'gallery', 'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 * See http://codex.wordpress.org/Post_Formats
	 */
	add_theme_support( 'post-formats', array(
		'gallery', 'link', 'video',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'novus_custom_background_args', array(
		'default-color' => 'f1f1ee',
		'default-image' => '',
	) ) );
}
endif; // novus_setup
add_action( 'after_setup_theme', 'novus_setup' );

/**
 * Register widget area.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_sidebar
 */
function novus_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Sidebar', 'novus' ),
		'id'            => 'sidebar-1',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h1 class="widget-title">',
		'after_title'   => '</h1>',
	) );
	register_sidebar( array(
		'name'          => __( 'Footer', 'novus' ),
		'id'            => 'footer',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h1 class="widget-title">',
		'after_title'   => '</h1>',
	) );
	register_sidebar( array(
		'name'          => __( 'Footer Full Width', 'novus' ),
		'id'            => 'footer-full-width',
		'description'   => __( 'Below footer widgets area.', 'novus' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h1 class="widget-title">',
		'after_title'   => '</h1>',
	) );
}
add_action( 'widgets_init', 'novus_widgets_init' );

/**
 * Custom editor css style
 */
function novus_add_editor_styles() {
	add_editor_style( 'style-editor.css' );
	$font_url = str_replace( ',', '%2C', '//fonts.googleapis.com/css?family=Lato:400,300,700' );
	add_editor_style( $font_url );
}
add_action( 'admin_init', 'novus_add_editor_styles' );

/**
 * Enqueue scripts and styles.
 */
function novus_scripts() {
	$protocol = is_ssl() ? 'https' : 'http';
	wp_enqueue_style( 'novus-style', get_template_directory_uri() . '/assets/css/main.min.css' );
	wp_enqueue_style( 'gaia-style', get_stylesheet_uri() );
	wp_enqueue_style( 'novus-google-font', $protocol . '://fonts.googleapis.com/css?family=Lato:300,400,700,400i' );

	wp_enqueue_script( 'novus-navigation', get_template_directory_uri() . '/assets/js/navigation.js', array(), '20120206', true );

	wp_enqueue_script( 'novus-skip-link-focus-fix', get_template_directory_uri() . '/assets/js/skip-link-focus-fix.js', array(), '20130115', true );

	if ( get_theme_mod( 'article_layout' ) == 'masonry' || get_theme_mod( 'article_layout' ) == 'masonry-full' ) {
		wp_enqueue_script( 'jquery-masonry' );
	}

	wp_enqueue_script( 'novus-script-main', get_template_directory_uri() . '/assets/js/all.min.js', array('jquery'), false, true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'novus_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';
require get_template_directory() . '/partials/customizer-settings.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

/**
 * WooCommerce customizations and support.
 */
require get_template_directory() . '/inc/woocommerce.php';

/**
 * Plugin overrides.
 */
require get_template_directory() . '/inc/plugin-overrides.php';

/**
 * Novus additional functionalities.
 */
require get_template_directory() . '/inc/novus.php';

/**
 * Novus shortcodes.
 */
require get_template_directory() . '/inc/shortcodes.php';

/**
 * Sanitization callbacks.
 */
require get_template_directory() . '/inc/sanitization-callbacks.php';

/**
 * Widgets.
 */
require get_template_directory() . '/inc/widgets/widget-cta.php';
require get_template_directory() . '/inc/widgets/widget-about.php';
require get_template_directory() . '/inc/widgets/widget-flickr.php';
require get_template_directory() . '/inc/widgets/widget-posts-list.php';
require get_template_directory() . '/inc/widgets/widget-social-media.php';
require get_template_directory() . '/inc/widgets/widget-latest-comments.php';

/**
 * TGM Plugin Activation.
 */
require get_template_directory() . '/inc/tgm.php';
