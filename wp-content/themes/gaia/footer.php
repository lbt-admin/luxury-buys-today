<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package Novus
 */
?>

		</div>
	</div><!-- #content -->

	<?php get_sidebar('footer-full-width'); ?>
	<?php get_sidebar('footer'); ?>
	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="container">
			<nav class="footer-navigation" role="navigation">
				<?php wp_nav_menu( array( 'theme_location' => 'footer', 'menu_id' => 'footer-menu' ) ); ?>
			</nav><!-- #site-navigation -->
			<?php get_template_part( 'partials/social-networks' ); ?>
			<div class="site-info">
				<?php if ( get_theme_mod( 'footer_text', false ) ) : ?>
					<?php echo get_theme_mod( 'footer_text', false ); ?>
				<?php else : ?>
					<?php printf( __( '&copy; 2017. %1$s by %2$s', 'novus' ), 'Gaia', '<a href="http://www.nvslbs.com" rel="nofollow" title="Making Art with WordPress">Novus Labs</a>' ); ?>
				<?php endif ?>
			</div>
		</div><!-- .site-info -->
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
