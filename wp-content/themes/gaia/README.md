# Novus Labs - Making art with WordPress

[http://www.nvslbs.com](http://www.nvslbs.com)

## Instructions
- Site is built with Gulp.js
- Run 'npm install' after git clone
- 'gulp watcher' - watch for changes and do default tasks
- 'gulp' - do default tasks
