<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package Novus
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<div class="loader js-loader">
	<div class="loader-status"></div>
	<div class="loader-status-small"></div>
</div>

<div id="page" class="hfeed site">
	<a class="skip-link screen-reader-text" href="#content"><?php _e( 'Skip to content', 'novus' ); ?></a>

	<header id="masthead" class="site-header" role="banner">
		<?php $header_type = 'site-nav-bar--' . get_theme_mod( 'header_type', 'sticky' ); ?>
		<div class="site-nav-bar <?php echo esc_attr( $header_type ); ?>">
			<div class="container">
				<button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false"><span class="genericon genericon-menu"></span></button>
				<a href="#" class="search-toggle genericon genericon-search"></a>
				<?php if ( NOVUS_WOOCOMMERCE_ACTIVE ) : ?>
				<a class="cart-contents" href="<?php echo WC()->cart->get_cart_url(); ?>" title="<?php _e( 'View your shopping cart', 'novus' ); ?>"><span class="genericon genericon-cart"></span> <?php echo sprintf ( _n( '%d item', '%d items', WC()->cart->cart_contents_count, 'novus' ), WC()->cart->cart_contents_count ); ?> - <?php echo WC()->cart->get_cart_total(); ?></a>
				<?php endif; ?>
				<nav id="site-navigation" class="main-navigation" role="navigation">
					<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_id' => 'primary-menu' ) ); ?>
				</nav><!-- #site-navigation -->
			</div>
		</div>
		<div class="site-search">
			<form role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">
				<a href="#" class="site-search-close genericon genericon-close"></a>
				<label>
					<span class="screen-reader-text"><?php echo _x( 'Search for:', 'label', 'novus' ) ?></span>
					<input type="search" class="search-field" placeholder="<?php echo esc_attr_x( 'Type to search ...', 'placeholder', 'novus' ) ?>" value="<?php echo get_search_query() ?>" name="s" title="<?php echo esc_attr_x( 'Search for:', 'label', 'novus' ) ?>" />
				</label>
			</form>
		</div>

		<?php
		// WooCommerce check
		$is_woocommerce = false;
		if ( NOVUS_WOOCOMMERCE_ACTIVE ) {
		  $is_woocommerce = is_woocommerce();
		}
		?>
		<?php if ( get_theme_mod( 'featured_image_in_header', true ) && is_singular() && has_post_thumbnail() && ! $is_woocommerce ) : ?>
		<?php $bg_image_url = wp_get_attachment_image_src( get_post_thumbnail_id(), 'post-thumb-header' ); ?>
		<div class="site-branding" style="background: url(<?php echo esc_url( $bg_image_url[0] ); ?>); background-size: cover; background-position: 50% 50%;">
		<?php elseif( get_header_image() ) : ?>
		<div class="site-branding" style="background: url(<?php header_image(); ?>); background-size: cover; background-position: 50% 50%;">
		<?php else : ?>
		<div class="site-branding">
		<?php endif; ?>
			<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
			<?php if ( get_theme_mod( 'site_logo' ) ) : ?>
				<img src="<?php echo get_theme_mod( 'site_logo' ); ?>" alt="<?php bloginfo( 'name' ); ?>">
			<?php else : ?>
				<?php bloginfo( 'name' ); ?>
			<?php endif; ?>
			</a></h1>
			<?php $theme_mods = get_theme_mods(); ?>
			<?php if ( empty( $theme_mods ) || !array_key_exists( 'header_tagline_show', $theme_mods ) || !empty( $theme_mods['header_tagline_show'] ) ) : ?>
			<h2 class="site-description"><?php bloginfo( 'description' ); ?></h2>
			<?php endif; ?>
		</div><!-- .site-branding -->

	</header><!-- #masthead -->

	<div id="content" class="site-content">
		<?php if ( get_theme_mod( 'featured_posts_display_all_pages', false ) || ( get_theme_mod( 'featured_posts_display', true ) && is_home() ) ) : ?>
			<?php novus_featured_posts( get_theme_mod( 'featured_posts_number', 5 ) ); ?>
		<?php endif; ?>
		<div class="container">
