<?php
/**
 * Template name: BA First Interaction
 *
 * @package Novus
 */
get_header( 'landing-page' ); ?>
<div id="wpr-custom-template" class="wrap wpr-abc-first-interaction">
    <div id="primary" class="content-area">
        <main id="main" class="site-main" role="main">
			<?php

			echo do_shortcode( '[wpr_abc_lc_interact]' );

			if ( have_posts() ) :
				while ( have_posts() ) : the_post();
					the_content();
				endwhile;
			endif;

			$abc    = wp_get_current_user();
			$abc_id = $abc->ID;
			if ( is_user_logged_in() && ( in_array( 'business_associate', (array) $abc->roles ) || current_user_can( 'administrator' ) ) ) {
				switch_to_blog( 1 );
				$lc_id = absint( get_user_meta( $abc_id, 'wpr_lc_id', true ) );
				if ( $lc_id ) {
					$lc = get_user_by( 'id', $lc_id );

					if ( $lc && ( in_array( 'wpr_prospect', (array) $lc->roles ) || current_user_can( 'administrator' ) ) ) {
						$lc_avatar    = get_avatar( $lc->ID, 32 );
						$lc_phone     = get_user_meta( $lc->ID, 'phone', true );
						$lc_phone_num = preg_replace( '/[^0-9]/', '', $lc_phone );

						$lc_email_subject = __( 'About that LBT offer you\'re interested in...', 'wpr-dealers' );
						$lc_email_body    = __( 'Email body here', 'wpr-dealers' );

						?>

                        <div class="wpr-abc-cta">
							<?php if ( ! empty( $lc_phone_num ) ) : ?>
                                <div class="wpr-call-cta">
                                    <a href="tel:<?php echo $lc_phone_num; ?>" class="wpr-abc-action datatable_buttons" data-cta="call"><?php _e( 'Call the shopper', 'wpr-dealers' ); ?></a>
                                </div>
							<?php endif; ?>

                            <div class="wpr-email-cta">
                                <a href="mailto:<?php echo $lc->user_email; ?>?subject=<?php echo $lc_email_subject; ?>&body=<?php echo $lc_email_body; ?>" class="wpr-abc-action datatable_buttons"
                                   data-cta="email"><?php _e( 'Email the shopper', 'wpr-dealers' ); ?></a>
                            </div>

                            <div class="wpr-text-info">
                                <a href="#" class="wpr-abc-action datatable_buttons" data-cta="text"><?php _e( 'Text me the Info', 'wpr-dealers' ); ?></a>
                            </div>
                        </div>
						<?php
					} else {
						_e( 'You can\'t access this user.', 'wpr-table' );
					}
				}
				restore_current_blog();
			} else {
				$login_url    = get_permalink( get_page_by_path( 'login' ) );
				$register_url = get_permalink( get_page_by_path( 'luxurybuystoday/register' ) );
				echo sprintf( 'Please <a href="%s" class="wpr-url">%s</a> in order to access this page', $login_url, __( 'Login' ) );
			} ?>

        </main><!-- #main -->
    </div><!-- #primary -->
</div><!-- .wrap -->
<?php get_footer(); ?>
