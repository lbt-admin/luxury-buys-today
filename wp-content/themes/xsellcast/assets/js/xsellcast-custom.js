jQuery(document).ready(function ($) {
    if ($('.wpr-login-redirect-field').length > 0) {
        setTimeout(function () {
            if (getUrlParameter('redirect')) {
                $('.wpr-login-redirect-field').find('input[type="text"]').attr('value', getUrlParameter('redirect'));
            }
        }, 200);
    }

    if ($('.wpr-register-redirect-field').length > 0) {
        setTimeout(function () {
            if (getUrlParameter('redirect')) {
                $('.wpr-register-redirect-field').find('input[type="text"]').attr('value', getUrlParameter('redirect'));
            }
        }, 200);
    }

    $('.wpr_social_login_right').on('mouseover', function () {
        var $accept = $('#wpr_register_accept');
        if (!$accept.is(':checked')) {
            if (0 === $('#wpr_register_overlay').length) {
                $("<div id='wpr_register_overlay' />").css({
                    position: "absolute",
                    width: "100%",
                    height: "100%",
                    left: 0,
                    top: 0,
                    zIndex: 1000000
                }).appendTo($(".wpr_social_login_right").css("position", "relative"));

                if (0 === $('#wpr_please_accept').length) {
                    $('<p id="wpr_please_accept">Please accept terms & conditions</p>').insertAfter('#wpr_register_social');
                }
            }
        } else {
            $("#wpr_register_overlay, #wpr_please_accept").remove();
        }
    });

    $('#wpr_register_accept').on('change', function () {
        if ($(this).is(":checked")) {
            $("#wpr_please_accept").remove();
        } else {
            $('<p id="wpr_please_accept">Please accept terms & conditions</p>').insertAfter('#wpr_register_social');
        }
    });

    $('#wprlistingTable').DataTable({
        "columnDefs": [
            {"width": "30%", "targets": 0}
        ],
        "bFilter": true,
        "bInfo": false,
        "bUseColVis": true,
        rowReorder: {
            selector: 'td:nth-child(2)'
        },
        responsive: true,
        "oLanguage": {
            "sLengthMenu": '<select>' +
            '<option value="10">10 entries</option>' +
            '<option value="25">25 entries</option>' +
            '<option value="50">50 entries</option>' +
            '<option value="100">100 entries</option>' +
            '<option value="-1">All entries</option>' +
            '</select>',
            "sSearch": "",
            "oPaginate": {
                "sNext": ">",
                "sPrevious": "<"
            }
        }
    });

    $('#wprlistingTableLook').DataTable({
        "bFilter": true,
        "bInfo": false,
        "bUseColVis": true,
        rowReorder: {
            selector: 'td:nth-child(2)'
        },
        responsive: true,
        "oLanguage": {
            "sLengthMenu": '<select>' +
            '<option value="10">10 entries</option>' +
            '<option value="25">25 entries</option>' +
            '<option value="50">50 entries</option>' +
            '<option value="100">100 entries</option>' +
            '<option value="-1">All entries</option>' +
            '</select>',
            "sSearch": "",
            "oPaginate": {
                "sNext": ">",
                "sPrevious": "<"
            }
        }
    });

    $('#prospectorInfio').DataTable({
        "bFilter": false,
        "bInfo": false,
        "bUseColVis": true,
        "bPaginate": false,
        "ordering": false,
        "info": false,
        rowReorder: {
            selector: 'td:nth-child(2)'
        },
        responsive: true,
        "oLanguage": {
            "sLengthMenu": '<select>' +
            '<option value="10">10 entries</option>' +
            '<option value="25">25 entries</option>' +
            '<option value="50">50 entries</option>' +
            '<option value="100">100 entries</option>' +
            '<option value="-1">All entries</option>' +
            '</select>',
            "sSearch": "",
            "oPaginate": {
                "sNext": ">",
                "sPrevious": "<"
            }
        }
    });

    $('div.dataTables_filter input').attr('placeholder', 'Search');

    if ($('.wpr-brand-selectbox').length) {
        var brandClass = $('.wpr-brand-selectbox select'),
            dealerClass = $('.populate-dealer-info select'),
            brandval = brandClass.val(),
            dealerSelect = brandClass.parents('form').find(dealerClass),
            bakey = getUrlParameter('bakey');

        wpr_call_dealers(brandval, bakey, dealerSelect);

        $(brandClass).change(function () {
            var brand = $(this),
                brandval = brand.val();
            wpr_call_dealers(brandval, bakey, dealerSelect);
        });
    }

    if ($('.wpua-edit').length) {
        $('.wpua-edit').find('#submit').val($('#submit').val() + ' Picture');
    }

    if ($('.wpfep-form-profile').length) {
        jQuery.ajax({
            type: 'POST',
            url: wpr_ajax_ba.wpr_ba_ajax_url,
            data: {action: 'wpr_get_ba_brand_dealer_info', banonce: wpr_ajax_ba.ba_nonce},
            success: function (data) {
                if (data) {
                    var obj = jQuery.parseJSON(data);

                    $('#wpbrand').val(obj.response.user_brand).prop('disabled', true).css("background-color", "#DDD");
                    $('#wpdealer').val(obj.response.user_dealer).prop('disabled', true).css("background-color", "#DDD");
                    $('#wpdealerzip').val(obj.response.user_dealer_zip).prop('disabled', true).css("background-color", "#DDD");
                }
            }
        });
    }

    $('#wpfep-tabs').on('click', 'a', function (e) {
        e.preventDefault();
        if ($(this).attr("href") === '#profile') {
            $('.wpr-profile-form').show();
        } else {
            $('.wpr-profile-form').hide();
        }
    });

    $('#gform_2 #input_2_6, #gform_8 #input_8_3').on('change', function (evt, params) {
        var selectedValue = params.selected;
        setCookie('wpr_dealer_selection', selectedValue);
    });


    // ABC first interaction CTAs
    if ($('.wpr-abc-cta').length) {
        $('.wpr-abc-action').on('click', function () {
            var btn = $(this),
                cta = $(this).data('cta');

            // Text CTA has to wait for response from the server, set loading state
            if (cta == 'text') {
                btn.addClass('loading');
                btn.data('text', btn.html()).html('Loading...');
            }

            jQuery.ajax({
                type: 'POST',
                url: wpr_ajax_ba.wpr_ba_ajax_url,
                data: {
                    action: 'wpr_op_send_abc_action',
                    cta: cta,
                    nonce: wpr_ajax_ba.ba_nonce,
                },

                success: function () {
                    if (cta == 'text') {
                        btn.removeClass('loading');
                        btn.html(btn.data('text'));
                        alert('Your request has been received');
                    }
                },
            });
        });
    }
});

'use strict';
( function (document, window, index) {
    var inputs = document.querySelectorAll('.wpr-inputfile');

    Array.prototype.forEach.call(inputs, function (input) {
        var label = input.nextElementSibling,
            labelVal = label.innerHTML;

        input.addEventListener('change', function (e) {
            var fileName = e.target.value.split('\\').pop();

            if (fileName)
                document.querySelector('.wpr_input_file').innerHTML = fileName;
            else
                label.innerHTML = labelVal;
        });
    });
}(document, window, 0));

function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
}

function wpr_call_dealers(brandval, bakey, dealerSelect) {
    jQuery.ajax({
        type: 'POST',
        url: wpr_ajax_ba.wpr_ba_ajax_url,
        data: {brand: brandval, bakey: bakey, action: 'wpr_get_ba_dealer_name', banonce: wpr_ajax_ba.ba_nonce},
        success: function (data) {
            dealerSelect.empty();
            var options = jQuery.parseJSON(data);
            var selectedItem;
            for (i = 0; i < options.length; i++) {
                if (getCookie('wpr_dealer_selection') == options[i].value) {
                    selectedItem = 'selected';
                } else {
                    selectedItem = options[i].selected;
                }
                dealerSelect.append('<option value="' + options[i].value + '" ' + selectedItem + '>' + options[i].text + '</option>');
            }
            dealerSelect.removeAttr('disabled').trigger("chosen:updated");
        }
    });
}

function setCookie(key, value) {
    var expires = new Date();
    expires.setTime(expires.getTime() + (24 * 60 * 60 * 1000));
    document.cookie = key + '=' + value + ';expires=' + expires.toUTCString();
}

function getCookie(key) {
    var keyValue = document.cookie.match('(^|;) ?' + key + '=([^;]*)(;|$)');
    return keyValue ? keyValue[2] : null;
}
