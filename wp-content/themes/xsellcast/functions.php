<?php

// Disable theme & plugin auto updates.
add_filter( 'auto_update_plugin', '__return_false' );
add_filter( 'auto_update_theme', '__return_false' );

add_action( 'wp_enqueue_scripts', 'wpr_enqueue_script', 100 );
/**
 * Enqueue
 */
function wpr_enqueue_script() {
	$scheme = 'http';
	if ( is_ssl() ) {
		$scheme = 'https';
	}

	wp_enqueue_style( 'wpr-parent-style', get_template_directory_uri() . '/style.css' );
	wp_enqueue_style( 'wpr-child-style', get_stylesheet_directory_uri() . '/style.css', array( 'wpr-parent-style' ) );

	wp_enqueue_style( 'datatables-style', get_stylesheet_directory_uri() . '/assets/css/datatables.min.css' );
	wp_enqueue_style( 'datatables-rowreorder-style', get_stylesheet_directory_uri() . '/assets/css/rowReorder.dataTables.min.css' );
	wp_enqueue_style( 'datatables-responsive-style', get_stylesheet_directory_uri() . '/assets/css/responsive.dataTables.min.css' );

	wp_enqueue_script( 'datatables-js', get_stylesheet_directory_uri() . '/assets/js/datatables.min.js', array(), '20170206', true );
	wp_enqueue_script( 'xsellcast-custom', get_stylesheet_directory_uri() . '/assets/js/xsellcast-custom.js', array(), '1.0.0', true );
	$args = array(
		'ba_nonce'        => wp_create_nonce( 'wpr-ba' ),
		'wpr_ba_ajax_url' => admin_url( 'admin-ajax.php', $scheme ),
	);
	wp_localize_script( 'xsellcast-custom', 'wpr_ajax_ba', $args );
	wp_enqueue_script( 'xsellcast-custom' );

	wp_enqueue_style( 'wpr-custom-style', get_stylesheet_directory_uri() . '/assets/css/wpr-custom.css' );
}

add_action( 'admin_enqueue_scripts', 'wpr_add_admin_ba' );
/**
 * Enqueue ADMIN
 */
function wpr_add_admin_ba() {
	wp_enqueue_script( 'wpr-ba-admin-script', get_stylesheet_directory_uri() . '/assets/js/admin_script.js', array( 'jquery' ), '1.0.0', true );
	$scheme = 'http';
	if ( is_ssl() ) {
		$scheme = 'https';
	}
	$args = array(
		'ba_nonce'        => wp_create_nonce( 'wpr-ba' ),
		'wpr_ba_ajax_url' => admin_url( 'admin-ajax.php', $scheme ),
	);
	wp_localize_script( 'wpr-ba-admin-script', 'wpr_ajax_ba', $args );
	wp_enqueue_script( 'wpr-ba-admin-script' );
}

add_action( 'login_enqueue_scripts', 'wpr_login_enqueue_styles', 5 );
/**
 *
 */
function wpr_login_enqueue_styles() {
	if ( ! wp_style_is( 'font-awesome', 'enqueued' ) ) {
		wp_enqueue_style( 'font-awesome', '//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css', array(), '4.3.0' );
	}
	wp_enqueue_style( 'login-style', get_stylesheet_directory_uri() . '/style-login.css' );
}

add_filter( 'gform_field_validation_1', 'login_validate_field', 10, 4 );
/**
 * @param $result
 * @param $value
 * @param $form
 * @param $field
 *
 * @return mixed
 */
function login_validate_field( $result, $value, $form, $field ) {
	// make sure this variable is global
	// this function is fired via recurrence for each field, s
	global $user;
	// validate username
	if ( $field['cssClass'] === 'username' ) {
		$user = get_user_by( 'email', $value );
		if ( ! empty( $user->user_login ) ) {
			$username = $user->user_login;
		}

		if ( empty( $username ) ) {
			$user = get_user_by( 'login', $value );
			if ( empty( $user->user_login ) ) {
				$result["is_valid"] = false;
				$result["message"]  = _( 'Invalid username or email provided.' );
			}
		}

		if ( ! is_user_member_of_blog( $user->ID, get_current_blog_id() ) ) {
			$result["is_valid"] = false;
			$result["message"]  = _( 'The account does not exists.' );
		}
	}
	// validate pass
	if ( $field['cssClass'] === 'password' ) {
		if ( ! $user or ! wp_check_password( $value, $user->data->user_pass, $user->ID ) ) {
			$result["is_valid"] = false;
			$result["message"]  = _( 'Invalid password provided.' );
		}
	}

	return $result;
}

add_action( 'gform_after_submission_1', 'login_form_after_submission', 10, 2 );
/**
 * @param $entry
 * @param $form
 */
function login_form_after_submission( $entry, $form ) {
	// get the username and pass
	$username = $entry[1];
	$pass     = $entry[2];
	$creds    = array();
	// create the credentials array
	if ( filter_var( $username, FILTER_VALIDATE_EMAIL ) ) {
		$user = get_user_by( 'email', $username );
	} else {
		$user = get_user_by( 'login', sanitize_user( $username ) );
	}

	$creds['user_login']    = $user->data->user_login;
	$creds['user_password'] = $pass;

	$user = wp_signon( $creds, true );
	wp_set_current_user( $user->ID );

	$key    = wp_generate_password( 50, false );
	$hashed = time() . ':' . wp_hash_password( $key );

	update_user_meta( $user->ID, 'wpr_ba_unique_token', $hashed );
}

add_filter( 'gform_confirmation_1', 'wpr_custom_confirmation_redirect', 10, 4 );
/**
 * Redirect after login
 *
 * @param $confirmation
 * @param $form
 * @param $entry
 * @param $ajax
 *
 * @return array
 */
function wpr_custom_confirmation_redirect( $confirmation, $form, $entry, $ajax ) {
	$redirect_url = rgar( $entry, '4' );
	if ( ! empty( $redirect_url ) ) {
		$confirmation = array( 'redirect' => esc_url( $redirect_url ) );
	}

	return $confirmation;
}

add_action( 'gform_user_registered', 'wpr_auto_login_subscriber', 10, 4 );
/**
 * Auto login after registration
 *
 * @param $user_id
 * @param $config
 * @param $entry
 * @param $password
 */
function wpr_auto_login_subscriber( $user_id, $config, $entry, $password ) {
	$brand_id = rgar( $entry, '8' );
	if ( is_int( $entry ) ) {
		update_user_meta( $user_id, 'wpr_brand_association', $brand_id );
	}

	$dealer_id = rgar( $entry, '6' );
	if ( is_int( $entry ) ) {
		update_user_meta( $user_id, 'wpr_dealer_association', $dealer_id );

		$dealer_zip = get_post_meta( $dealer_id, 'wpr_store_zip_code', true );
		if ( $dealer_zip ) {
			update_user_meta( $user_id, 'zip_code', esc_attr( $dealer_zip ) );
		}
	}

	wp_set_auth_cookie( $user_id, false, '' );

	if ( ! empty( $entry['post_id'] ) ) {
		wp_delete_post( $entry['post_id'] );
	}

	$key    = wp_generate_password( 50, false );
	$hashed = time() . ':' . wp_hash_password( $key );

	update_user_meta( $user_id, 'wpr_ba_unique_token', $hashed );

	// Add BA to
	add_user_to_blog( 1, $user_id, 'wpr_prospect' );
}


add_filter( 'login_redirect', 'my_login_redirect', 10, 3 );
/**
 * Redirect user after successful login.
 *
 * @param string $redirect_to URL to redirect to.
 * @param string $request URL the user is coming from.
 * @param object $user Logged user's data.
 *
 * @return string
 */
function my_login_redirect( $redirect_to, $request, $user ) {
	if ( isset( $user->roles ) && is_array( $user->roles ) ) {
		//check for admins
		if ( in_array( 'administrator', $user->roles ) ) {
			return $redirect_to;
		} else {
			return home_url();
		}
	} else {
		return $redirect_to;
	}
}

add_action( 'wp_logout', 'logout_redirect_home' );
/**
 * Redirect logout
 */
function logout_redirect_home() {
	ob_start();
	wp_safe_redirect( home_url() );
	exit;
}

add_action( 'after_setup_theme', 'remove_admin_bar' );
/**
 * Hide admin bar menu
 */
function remove_admin_bar() {
	if ( ! current_user_can( 'administrator' ) && ! is_admin() ) {
		show_admin_bar( false );
	}
}

add_action( 'admin_init', 'redirect_non_admin_user' );
/**
 * Redirect to homepage
 */
function redirect_non_admin_user() {
	if ( is_user_logged_in() ) {
		if ( ! defined( 'DOING_AJAX' ) && ! current_user_can( 'administrator' ) ) {
			ob_start();
			wp_redirect( site_url() );
			exit;
		}
	}
}

/**
 * Delete BA offer
 *
 * @param $post_id
 */
function wpr_ba_delete_offer( $post_id, $my_offers_page ) {
	switch_to_blog( 1 );
	$ba_id       = get_current_user_id();
	$post_author = get_post_field( 'post_author', $post_id );
	if ( $ba_id != $post_author ) {
		return;
	}
	wp_delete_post( $post_id );
	restore_current_blog();
	ob_start();
	wp_safe_redirect( esc_url( get_permalink( $my_offers_page ) ) );
	exit;
}

add_action( 'init', 'wpr_frontend_update_offer' );
/**
 * Edit current offer
 *
 * @param $post_id
 * @param $my_offers_page
 */
function wpr_frontend_update_offer() {
	$ba_id = get_current_user_id();
	if ( ! $ba_id ) {
		return;
	}

	$user_zip    = get_user_meta( $ba_id, 'zip_code', true );
	$user_dealer = get_user_meta( $ba_id, 'wpr_dealer_association', true );
	$dealer_zip  = get_post_meta( $user_dealer, 'wpr_store_zip_code', true );
	if ( ! $user_zip && $dealer_zip ) {
		update_user_meta( $ba_id, 'zip_code', esc_attr( $dealer_zip ) );
	}

	if ( ! isset( $_POST['wpr_offer_nonce'] ) || ! wp_verify_nonce( $_POST['wpr_offer_nonce'], 'wpr_offer_edit' ) ) {
		return;
	}

	if ( empty( $_POST['frontend'] ) ) {
		return;
	}

	switch_to_blog( 1 );

	$post_id = absint( $_POST['ID'] );

	if (
		empty( $_POST['post_title'] ) ||
		empty( $_POST['post_content'] ) ||
		(
			! has_post_thumbnail( $post_id ) &&
			( ! isset( $_FILES['image'] ) || UPLOAD_ERR_NO_FILE === $_FILES['image']['error'] )
		)
	) {
		restore_current_blog();
		return;
	}

	if ( ! empty( $post_id ) ) {
		$data    = array(
			'ID'           => $post_id,
			'post_title'   => esc_attr( $_POST['post_title'] ),
			'post_content' => htmlspecialchars_decode( $_POST['post_content'] ),
		);
		$post_id = wp_update_post( $data );

		foreach ( $_FILES as $file => $details ) {
			$wp_filetype = wp_check_filetype_and_ext( $details['tmp_name'], $details['name'] );

			//Only accept image uploads
			if ( ! in_array( $wp_filetype['ext'], array( 'png', 'jpg' ) ) ) {
				continue;
			}

			wpr_offer_add_media( $file, $post_id, true );
		}
	} else {
		$ba_brand = get_user_meta( $ba_id, 'wpr_brand_association', true );

		$data = array(
			'ID'            => $post_id,
			'post_title'    => esc_attr( $_POST['post_title'] ),
			'post_content'  => htmlspecialchars_decode( $_POST['post_content'] ),
			'post_status'   => 'publish',
			'post_author'   => absint( $ba_id ),
			'post_category' => array( $ba_brand ),
		);

		$post_id = wp_insert_post( $data );
		update_post_meta( $post_id, 'wpr_brand_id', $ba_brand );

		$ba_dealer = get_user_meta( $ba_id, 'wpr_dealer_association', true );
		if ( $ba_dealer ) {
			update_post_meta( $post_id, 'wpr_dealer_id', $ba_dealer );
		}

		foreach ( $_FILES as $file => $details ) {
			$wp_filetype = wp_check_filetype_and_ext( $details['tmp_name'], $details['name'] );

			if ( ! in_array( $wp_filetype['ext'], array( 'png', 'jpg' ) ) ) {
				continue;
			}

			wpr_offer_add_media( $file, $post_id, true );
		}
	}

	restore_current_blog();

	ob_start();
	$current_url = ( isset( $_SERVER['HTTPS'] ) && $_SERVER['HTTPS'] == 'on' ? "https" : "http" ) . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
	$id          = url_to_postid( $current_url );
	wp_safe_redirect( esc_url( get_permalink( $id ) ) );
	exit;
}

/**
 * @param $file_handler
 * @param $post_id
 * @param string $setthumb
 *
 * @return bool|int|WP_Error
 */
function wpr_offer_add_media( $file_handler, $post_id, $setthumb = 'false' ) {

	// check to make sure its a successful upload
	if ( $_FILES[ $file_handler ]['error'] !== UPLOAD_ERR_OK ) {
		return false;
	}

	require_once( ABSPATH . "wp-admin/includes/image.php" );
	require_once( ABSPATH . "wp-admin/includes/file.php" );
	require_once( ABSPATH . "wp-admin/includes/media.php" );

	$attach_id = media_handle_upload( $file_handler, $post_id );

	if ( $setthumb ) {
		set_post_thumbnail( $post_id, $attach_id );
	}

	return $attach_id;
}

add_action( 'show_user_profile', 'wpr_ba_extra_fields' );
add_action( 'user_new_form', 'wpr_ba_extra_fields' );
add_action( 'edit_user_profile', 'wpr_ba_extra_fields' );
/**
 * User extra fields
 *
 * @param $user
 */
function wpr_ba_extra_fields( $user ) {
	if ( in_array( 'business_associate', $user->roles ) ) {
		$brand_selected  = get_user_meta( $user->ID, 'wpr_brand_association', true );
		$dealer_selected = get_user_meta( $user->ID, 'wpr_dealer_association', true );
		$dealer_csr      = get_user_meta( $user->ID, 'wpr_csr_association', true );
		?>
        <h3><?php _e( 'BA Dealer selection' ); ?></h3>

        <table class="form-table">
            <tr>
                <th><label for="twitter"><?php _e( 'Brand', 'luxury' ); ?></label></th>
                <td>
					<?php
					switch_to_blog( 1 );
					wp_dropdown_categories( array(
						'show_option_none'  => __( 'Brand', 'luxury' ),
						'option_none_value' => 'all',
						'taxonomy'          => 'category',
						'hierarchical'      => 1,
						'class'             => 'userbrand',
						'name'              => 'userbrand',
						'selected'          => $brand_selected,
						'orderby'           => 'name',
						'hide_empty'        => false,
					) );
					restore_current_blog();
					?>
                </td>
            </tr>

            <tr>
                <th><label for="twitter"><?php _e( 'Dealer', 'luxury' ); ?></label></th>
                <td>
                    <select name="userdealer" id="userdealer" class="userdealer"></select>
					<?php if ( ! empty( $dealer_selected ) ) { ?>
                        <script>
                            jQuery(document).ajaxComplete(function () {
                                jQuery('#userdealer').val('<?php echo absint( $dealer_selected ); ?>').trigger('change.select2');
                            });
                        </script>
					<?php } ?>
                </td>
            </tr>
        </table>

        <h3><?php _e( 'Assigned Customer Service Representative' ); ?></h3>

        <table class="form-table">
            <tr>
                <th><label for="twitter"><?php _e( 'CSR', 'luxury' ); ?></label></th>
                <td>
					<?php
					$args = array(
						'orderby'          => 'display_name',
						'order'            => 'ASC',
						'name'             => 'wpr_csr',
						'show_option_none' => 'None',
						'selected'         => $dealer_csr,
						'role'             => array( 'customer_service_representative' ),
					);
					wp_dropdown_users( $args );
					?>
                </td>
            </tr>
        </table>
	<?php }
}

add_action( 'personal_options_update', 'save_ba_form_fields' );
add_action( 'edit_user_profile_update', 'save_ba_form_fields' );
add_action( 'user_register', 'save_ba_form_fields' );
/**
 * Save/update BA extra fields
 *
 * @param $user_id
 */
function save_ba_form_fields( $user_id ) {
	if ( isset( $_POST['userbrand'] ) && isset( $_POST['userdealer'] ) ) {
		update_user_meta( $user_id, 'wpr_brand_association', absint( $_POST['userbrand'] ) );
		update_user_meta( $user_id, 'wpr_dealer_association', absint( $_POST['userdealer'] ) );
	}

	if ( isset( $_POST['wpr_csr'] ) && $_POST['wpr_csr'] > 0 ) {
		update_user_meta( $user_id, 'wpr_csr_association', absint( $_POST['wpr_csr'] ) );
	} else {
		delete_user_meta( $user_id, 'wpr_csr_association' );
	}
}

add_action( 'wp_ajax_wpr_get_ba_dealer_name', 'wpr_get_ba_dealer_name' );
add_action( 'wp_ajax_nopriv_wpr_get_ba_dealer_name', 'wpr_get_ba_dealer_name' );
/**
 * Dealer ajax call
 */
function wpr_get_ba_dealer_name() {
//	check_ajax_referer( 'wpr-ba', 'banonce' );

	$items           = array();
	$brand           = absint( $_POST['brand'] );
	$dealer_selected = '';
	$user_id         = get_current_user_id();

	if ( isset( $_POST['bakey'] ) && ! empty( $_POST['bakey'] ) ) {
		$users = get_users( array(
			'meta_key'   => 'wpr_ba_unique_token',
			'meta_value' => esc_attr( $_POST['bakey'] ),
		) );

		if ( ! empty( $users ) ) {
			$user_id = (int) $users[0]->ID;
		}
	}

	if ( $user_id ) {
		$dealer_selected = get_user_meta( $user_id, 'wpr_dealer_association', true );
	}

	if ( $brand > 0 ) {
		$dealers = get_posts( array(
			'post_type'      => 'wpr_dealer_info',
			'meta_key'       => 'wpr_category_id',
			'meta_value'     => $brand,
			'post_status'    => 'publish',
			'orderby'        => 'title',
			'order'          => 'ASC',
			'posts_per_page' => - 1,
		) );

		$items[] = array( 'text' => __( 'Select dealer...', 'wpr-dealer-info' ), 'value' => '' );
		foreach ( $dealers as $dealer ) {
			$selected = '';
			if ( $dealer_selected == $dealer->ID ) {
				$selected = 'selected';
			}
			$items[] = array( 'text' => $dealer->post_title, 'value' => $dealer->ID, 'selected' => $selected );
		}
	}

	echo json_encode( $items );
	wp_die();
}

add_filter( 'gform_pre_render', 'wpr_populate_categories' );
add_filter( 'gform_admin_pre_render', 'wpr_populate_categories' );
/**
 * GF populate brand dropdown
 *
 * @param $form
 *
 * @return mixed
 */
function wpr_populate_categories( $form ) {
	$taxonomy = 'category';
	$formid   = 2;
	$fieldid  = 8;

	if ( $form['id'] != $formid ) {
		return $form;
	}

	switch_to_blog( 1 );
	$terms   = get_terms( $taxonomy );
	$items   = array();
	$exclude = array( 36, 37, 38, 39 );

	foreach ( $terms as $term ) {
		$is_selected = $term->name == 'testing' ? true : false;
		if ( $term->parent != 0 && ! in_array( $term->term_id, $exclude ) ) {
			$items[] = array( 'value' => $term->term_id, 'text' => $term->name, 'isSelected' => $is_selected );
		}
	}
	foreach ( $form['fields'] as &$field ) {
		if ( $field['id'] == $fieldid ) {
			$field['type']    = 'select';
			$field['choices'] = $items;
		}
	}
	restore_current_blog();

	return $form;
}

add_filter( 'gform_pre_render', 'wpr_fill_brand_industry' );
add_filter( 'gform_admin_pre_render', 'wpr_fill_brand_industry' );
/**
 * GF populate brand dropdown
 *
 * @param $form
 *
 * @return mixed
 */
function wpr_fill_brand_industry( $form ) {
	$taxonomy = 'category';
	$formid   = 8;
	$fieldid  = 2;

	if ( $form['id'] != $formid ) {
		return $form;
	}

	switch_to_blog( 1 );
	$terms   = get_terms( $taxonomy );
	$items   = array();
	$exclude = array( 36, 37, 38, 39 );

	foreach ( $terms as $term ) {
		$is_selected = $term->name == 'testing' ? true : false;
		if ( $term->parent != 0 && ! in_array( $term->term_id, $exclude ) ) {
			$items[] = array( 'value' => $term->term_id, 'text' => $term->name, 'isSelected' => $is_selected );
		}
	}
	foreach ( $form['fields'] as &$field ) {
		if ( $field['id'] == $fieldid ) {
			$field['type']    = 'select';
			$field['choices'] = $items;
		}
	}
	restore_current_blog();

	return $form;
}

add_action( 'gform_post_submission', 'wpr_ba_fill_brand_industry', 10, 2 );
/**
 * Associate BA post to his brand category & save extra data
 *
 * @param $entry
 * @param $form
 */
function wpr_ba_fill_brand_industry( $entry, $form ) {
	$formid = 8;
	if ( $form['id'] != $formid ) {
		return;
	}

	$ba_id = get_current_user_id();
	if ( ! $ba_id ) {
		return;
	}

	$ba_id          = absint( $ba_id );
	$current_avatar = get_user_meta( $ba_id, 'wpr_user_avatar', true );

	$ba_brand    = rgar( $entry, '2' );
	$ba_dealer   = rgar( $entry, '3' );
	$first_name  = rgar( $entry, '4' );
	$last_name   = rgar( $entry, '5' );
	$description = rgar( $entry, '6' );
	$phone       = rgar( $entry, '9' );
	$position    = rgar( $entry, '8' );
	$email       = rgar( $entry, '7' );
	$avatar      = rgar( $entry, '11' );

	if ( $ba_brand ) {
		update_user_meta( $ba_id, 'wpr_brand_association', absint( $ba_brand ) );
	}

	if ( $ba_dealer ) {
		update_user_meta( $ba_id, 'wpr_dealer_association', absint( $ba_dealer ) );
	}

	if ( $first_name ) {
		update_user_meta( $ba_id, 'first_name', esc_attr( $first_name ) );
	}

	if ( $last_name ) {
		update_user_meta( $ba_id, 'last_name', esc_attr( $last_name ) );
	}

	if ( $description ) {
		update_user_meta( $ba_id, 'description', esc_attr( $description ) );
	}

	if ( $phone ) {
		update_user_meta( $ba_id, 'phone', esc_attr( $phone ) );
	}

	if ( $position ) {
		update_user_meta( $ba_id, 'position', esc_attr( $position ) );
	}

	if ( $current_avatar != $avatar ) {
		unlink( $current_avatar );
	}

	if ( $avatar ) {
		update_user_meta( $ba_id, 'wpr_user_avatar', esc_url( $avatar ) );
	}

	if ( $email ) {
		$args = array(
			'ID'         => $ba_id,
			'user_email' => esc_attr( $email ),
		);
		wp_update_user( $args );
	}
}

add_action( 'template_redirect', 'wpr_redirect_to_fill_brand_industry' );
/**
 * Redirect BA to fill Brand&Industry
 */
function wpr_redirect_to_fill_brand_industry() {
	if ( is_user_logged_in() ) {
		$ba_id = get_current_user_id();
		if ( ! $ba_id ) {
			return;
		}

		$redirect_page_slug = 'fill-industry-brand';
		$terms_and_conds    = 'terms-and-conditions';
		$qualified_enquiry  = 'qualified-enquiry';
		if ( wpr_is_profile_not_complete( $ba_id ) && ! wp_doing_ajax() && ! is_page( $redirect_page_slug ) && ! is_page( $terms_and_conds ) && ! is_page( $qualified_enquiry ) ) {
			$redirect_to = get_permalink( get_page_by_path( $redirect_page_slug ) );
			ob_start();
			wp_safe_redirect( esc_url( $redirect_to ) );
			die();
		}

		if ( ! wpr_is_profile_not_complete( $ba_id ) && ! wp_doing_ajax() && is_page( $redirect_page_slug ) ) {
			ob_start();
			wp_safe_redirect( home_url() );
			die();
		}

//		if ( ! is_user_member_of_blog( $ba_id, get_current_blog_id() ) && ! wp_doing_ajax() ) {
//			wp_clear_auth_cookie();
//			ob_start();
//			switch_to_blog( 1 );
//			$link = home_url();
//			restore_current_blog();
//			wp_safe_redirect( esc_url( $link ) );
//			die();
//		}

		$used_qualified_form = get_user_meta( $ba_id, 'wpr_used_qualified_form', true );
		if ( ! wp_doing_ajax() && ! $used_qualified_form && ! is_page( $qualified_enquiry ) && ! is_page( $terms_and_conds ) ) {
			$redirect_to = get_permalink( get_page_by_path( $qualified_enquiry ) );
			ob_start();
			wp_safe_redirect( esc_url( $redirect_to ) );
			die();
		}
	}
}

/**
 * Check if BA have all data info filled
 *
 * @param $user_id
 *
 * @return bool
 */
function wpr_is_profile_not_complete( $user_id ) {
	$incomplete = false;

	$user_info   = get_user_by( 'id', $user_id );
	$ba_brand    = get_user_meta( $user_id, 'wpr_brand_association', true );
	$ba_dealer   = get_user_meta( $user_id, 'wpr_dealer_association', true );
	$first_name  = get_user_meta( $user_id, 'first_name', true );
	$last_name   = get_user_meta( $user_id, 'last_name', true );
	$description = get_user_meta( $user_id, 'description', true );
	$phone       = get_user_meta( $user_id, 'phone', true );
	$position    = get_user_meta( $user_id, 'position', true );
	$avatar      = get_user_meta( $user_id, 'wpr_user_avatar', true );
	if ( ! $avatar ) {
		$avatar = get_avatar_url( $user_id );
	}
	$avatar = parse_url( $avatar );
	if ( preg_match( "/gravatar.com/", $avatar['host'] ) ) {
		$avatar = '';
	}
	if ( in_array( 'business_associate', $user_info->roles ) ) {
		if (
			empty( $ba_brand ) ||
			empty( $ba_dealer ) ||
			empty( $first_name ) ||
			empty( $last_name ) ||
			empty( $description ) ||
			empty( $phone ) ||
			empty( $position ) ||
			empty( $user_info->user_email ) ||
			empty( $avatar )
		) {
			$incomplete = true;
		}
	}

	return $incomplete;
}

//add_action( 'pre_get_posts', 'wpr_hide_show_ba_offers_by_default' );
/**
 * Hide BA custom offers by default
 *
 * @param $where
 *
 * @return string
 */
function wpr_hide_show_ba_offers_by_default( $query ) {
	$meta_query = array(
		array(
			'key'     => 'wpr_dealer_id',
			'compare' => 'NOT EXISTS',
		),
	);
	$query->set( 'meta_query', $meta_query );

	return $query;
}

add_action( 'gform_post_submission', 'wpr_ba_post_submission', 10, 2 );
/**
 * Associate BA post to his brand category & save extra data
 *
 * @param $entry
 * @param $form
 */
function wpr_ba_post_submission( $entry, $form ) {
	$formid = 3;
	if ( $form['id'] != $formid ) {
		return;
	}
	switch_to_blog( 1 );

	$post_title   = rgar( $entry, '1' );
	$post_content = rgar( $entry, '2' );

	$ba_id = get_current_user_id();
	if ( ! $ba_id ) {
		return;
	}

	$ba_brand = get_user_meta( $ba_id, 'wpr_brand_association', true );
	if ( $ba_brand ) {
		$my_post = array(
			'post_title'    => esc_attr( $post_title ),
			'post_content'  => htmlspecialchars_decode( $post_content ),
			'post_status'   => 'publish',
			'post_author'   => absint( $ba_id ),
			'post_category' => array( $ba_brand ),
		);

		$post_id = wp_insert_post( $my_post );
		update_post_meta( $post_id, 'wpr_brand_id', $ba_brand );

		$ba_dealer = get_user_meta( $ba_id, 'wpr_dealer_association', true );
		if ( $ba_dealer ) {
			update_post_meta( $post_id, 'wpr_dealer_id', $ba_dealer );
		}

		foreach ( $_FILES as $file => $details ) {
			$wp_filetype = wp_check_filetype_and_ext( $details['tmp_name'], $details['name'] );

			if ( ! in_array( $wp_filetype['ext'], array( 'png', 'jpg' ) ) ) {
				continue;
			}

			wpr_offer_add_media( $file, $post_id, true );
		}
	}

	restore_current_blog();
}

/**
 * Return Category ID's
 *
 * @param $posts
 *
 * @return array
 */
function wpr_obtain_category_id( $posts ) {
	$category_id = array();
	if ( is_array( $posts ) && ! empty( $posts ) ) {
		foreach ( $posts as $post ) {
			$post_categories = get_the_category( $post );

			if ( ! empty( $post_categories ) ) {
				foreach ( $post_categories as $term ) {
					array_push( $category_id, $term->term_id );
				}
			}
		}
	}

	return $category_id;
}


/**
 * Password reset start
 */
add_filter( "gform_field_validation", 'wp_doing_validation', 10, 4 );
/**
 * Let's verify for the user email provided
 *
 * @param $result
 * @param $value
 * @param $form
 * @param $field
 *
 * @return mixed
 */
function wp_doing_validation( $result, $value, $form, $field ) {
	$formid = 6;
	if ( $form['id'] == $formid ) {
		$classes            = explode( ' ', $field['cssClass'] );
		$result['is_valid'] = true;

		if ( in_array( 'user-email', $classes ) ) {
			$user_data = get_user_by( 'email', trim( $value ) );
			if ( empty( $user_data ) ) {
				$result['is_valid'] = false;
				$result['message']  = __( 'No such email in database.' );
			}
			$allow = check_if_reset_is_allowed( $user_data->ID );
		}
		if ( ! $allow ) {
			$result['is_valid'] = false;
			$result['message']  = __( 'Password change is not allowed.' );
		}
	}

	return $result;
}

add_action( "gform_pre_submission", "wp_doing_pre_submission" );
/**
 * Before submission
 *
 * @param $form
 */
function wp_doing_pre_submission( $form ) {
	$formid = 6;
	if ( $form['id'] == $formid ) {
		global $wpdb, $wp_hasher;
		$email_or_username = $_POST['input_1'];

		$email     = sanitize_email( $email_or_username );
		$user_data = get_user_by( 'email', $email );

		if ( $user_data ) {
			$user_login = $user_data->user_login;
			$user_email = $user_data->user_email;
			$key        = wp_generate_password( 20, false );

			if ( empty( $wp_hasher ) ) {
				require_once ABSPATH . WPINC . '/class-phpass.php';
				$wp_hasher = new PasswordHash( 8, true );
			}
			$wp_hasher = new PasswordHash( 8, true );
			$hashed    = time() . ':' . wp_hash_password( $key );

			$wpdb->update( $wpdb->users, array( 'user_activation_key' => $hashed ), array( 'user_login' => $user_login ) );

			$reset_pass_url = get_bloginfo( 'url' ) . '/set-new-password/?action=rp&method=gf&key=' . $key . '&login=' . rawurlencode( $user_login );

			$message = __( 'Someone requested the password to be reset for the following account:' ) . "\r\n\r\n";
			$message .= get_bloginfo( 'url' ) . "\r\n\r\n";
			$message .= sprintf( __( 'Username: %s' ), $user_login ) . "\r\n\r\n";
			$message .= __( 'If this was a mistake, just ignore this email and nothing will happen.' ) . "\r\n\r\n";
			$message .= __( 'To reset your password, visit the following address:' ) . "\r\n\r\n";
			$message .= sprintf( '<a href="%s">%s</a>', esc_url( $reset_pass_url ), esc_url( $reset_pass_url ) );

			$blogname = wp_specialchars_decode( get_bloginfo( 'name' ), ENT_QUOTES );

			$title = sprintf( __( '[%s] Password Reset' ), $blogname );

			$title = apply_filters( 'retrieve_password_title', $title );

			$message = apply_filters( 'retrieve_password_message', $message, $key, $user_login, $user_data );
			if ( $message && ! wp_mail( $user_email, wp_specialchars_decode( $title ), $message ) ) {
				wp_die( __( 'The e-mail could not be sent.' ) . "<br />\n" . __( 'Possible reason: your host may have disabled the mail() function.' ) );
			}
		}
	}

	return;
}

add_action( 'init', 'wp_doing_verify_user_key', 999 );
/**
 * Check if the user has hit the proper rest password page. The check is identical to that
 * from wp-login.php, hence extra $_GET['method'] parameter was included to exclude redirects
 * from wp-login.php file on standard password reset method.
 */
function wp_doing_verify_user_key() {
	global $gf_reset_user;

	list( $rp_path ) = explode( '?', wp_unslash( $_SERVER['REQUEST_URI'] ) );
	$rp_cookie = 'wp-resetpass-' . COOKIEHASH;

	if ( isset( $_GET['key'] ) and isset( $_GET['method'] ) ) {
		if ( $_GET['method'] == 'gf' ) {
			$value = sprintf( '%s:%s', wp_unslash( $_GET['login'] ), wp_unslash( $_GET['key'] ) );
			setcookie( $rp_cookie, $value, 0, $rp_path, COOKIE_DOMAIN, is_ssl(), true );
			wp_safe_redirect( remove_query_arg( array( 'key', 'login', 'method' ) ) );
			exit;
		}
	}

	if ( isset( $_COOKIE[ $rp_cookie ] ) && 0 < strpos( $_COOKIE[ $rp_cookie ], ':' ) ) {
		list( $rp_login, $rp_key ) = explode( ':', wp_unslash( $_COOKIE[ $rp_cookie ] ), 2 );
		$user = check_password_reset_key( $rp_key, $rp_login );
		if ( isset( $_POST['pass1'] ) && ! hash_equals( $rp_key, $_POST['rp_key'] ) ) {
			$user = false;
		}
	} else {
		$user = false;
	}

	if ( ! $user || is_wp_error( $user ) ) {
		setcookie( $rp_cookie, ' ', time() - YEAR_IN_SECONDS, $rp_path, COOKIE_DOMAIN, is_ssl(), true );
	}

	$gf_reset_user = $user;
}

add_shortcode( 'verify_user_pass', 'wp_doing_verify_user_pass' );
/**
 * Shortcode which is used to cover Gravity Forms shortcode. It will not render the password
 * reset form in case of invalid pass.
 *
 * @param $args
 * @param null $content
 *
 * @return string
 */
function wp_doing_verify_user_pass( $args, $content = null ) {
	global $gf_reset_user;

	ob_start();
	if ( ! $gf_reset_user || is_wp_error( $gf_reset_user ) ) {
		if ( $gf_reset_user && $gf_reset_user->get_error_code() === 'expired_key' ) {
			echo '<h2 class="error">The key has expired.</h2>';
		} else {
			echo '<h2 class="error">The key is invalid</h2>';
		}
	} else {
		echo '<h2>Please update your password below.</h2>';
		echo do_shortcode( $content );
	}

	return ob_get_clean();
}

add_filter( 'gform_field_validation', 'wp_doing_password_validation', 10, 4 );
/**
 * Custom GF validation function used for pagination and required fields
 *
 * @param $result
 * @param $value
 * @param $form
 * @param $field
 *
 * @return mixed
 */
function wp_doing_password_validation( $result, $value, $form, $field ) {
	$formid = 7;
	if ( $form['id'] == $formid ) {
		global $pass;
		$result['is_valid'] = true;
		$classes            = explode( ' ', $field['cssClass'] );

		if ( in_array( 'new-pass', $classes ) ) {
			$pass = $value;
		}

		if ( in_array( 'repeat-pass', $classes ) ) {

			if ( $pass != $value ) {
				$result['is_valid'] = false;
				$result['message']  = __( 'Password mismatch.' );
			}
		}
	}

	return $result;
}

add_action( 'gform_pre_submission', 'wp_doing_password_pre_submission' );
/**
 * Password submission
 *
 * @param $form
 */
function wp_doing_password_pre_submission( $form ) {
	$formid = 7;
	if ( $form['id'] == $formid ) {
		global $gf_reset_user;
		list( $rp_path ) = explode( '?', wp_unslash( $_SERVER['REQUEST_URI'] ) );
		$rp_cookie = 'wp-resetpass-' . COOKIEHASH;

		$pass         = $_POST['input_1'];
		$pass_confirm = $_POST['input_2'];

		if ( defined( 'DOING_CRON' ) || isset( $_GET['doing_wp_cron'] ) ) {
			return;
		}

		$user_id = username_exists( $gf_reset_user->ID );

		if ( ! $user_id ) {
			if ( ! empty( $pass ) and ! empty( $pass_confirm ) and $pass === $pass_confirm ) {
				reset_password( $gf_reset_user, $pass );
				setcookie( $rp_cookie, ' ', time() - YEAR_IN_SECONDS, $rp_path, COOKIE_DOMAIN, is_ssl(), true );
				wp_logout();
			}
		} else {
			return;
		}
	}
}

/**
 * Utility to check if password reset is allowed based on user id.
 *
 * @param INT $user_id
 *
 * @return BOOL true / false
 */
function check_if_reset_is_allowed( $user_id ) {
	$allow = apply_filters( 'allow_password_reset', true, $user_id );
	if ( ! $allow ) {
		return false;
	} elseif ( is_wp_error( $allow ) ) {
		return false;
	}

	return true;
}

/**
 * Password reset end
 */

add_filter( 'wp_nav_menu_items', 'wti_loginout_menu_link', 10, 2 );
/**
 * Add logout menu item
 *
 * @param $items
 * @param $args
 *
 * @return string
 */
function wti_loginout_menu_link( $items, $args ) {
	if ( $args->theme_location == 'top' ) {
		if ( is_user_logged_in() ) {
			$items .= '<li class="right"><a href="' . wp_logout_url() . '">' . __( "Log Out" ) . '</a></li>';
		}
	}

	return $items;
}

add_action( 'wp_head', 'wpr_hotjar_script' );
/**
 * Add hotjar script
 */
function wpr_hotjar_script() {
	?>
    <!-- Hotjar Tracking Code for https://xsellcast.com -->
    <script>
        (function (h, o, t, j, a, r) {
            h.hj = h.hj || function () {
                (h.hj.q = h.hj.q || []).push(arguments)
            };
            h._hjSettings = {hjid: 527996, hjsv: 5};
            a = o.getElementsByTagName('head')[0];
            r = o.createElement('script');
            r.async = 1;
            r.src = t + h._hjSettings.hjid + j + h._hjSettings.hjsv;
            a.appendChild(r);
        })(window, document, '//static.hotjar.com/c/hotjar-', '.js?sv=');
    </script>
	<?php
}

add_filter( 'user_contactmethods', 'lbt_contactmethods', 10, 1 );
/**
 * Add additional contact methods
 */
function lbt_contactmethods( $contactmethods ) {
	$contactmethods['street']   = __( 'Street', 'novus' );
	$contactmethods['city']     = __( 'City', 'novus' );
	$contactmethods['state']    = __( 'State', 'novus' );
	$contactmethods['zip_code'] = __( 'Zip code', 'novus' );
	$contactmethods['phone']    = __( 'Phone', 'novus' );
	$contactmethods['position'] = __( 'Position', 'novus' );

	return $contactmethods;
}

remove_filter( 'wpfep_fields_profile', 'wpfep_add_profile_tab_meta_fields', 10 );
add_filter( 'wpfep_fields_profile', 'wpr_add_profile_tab_meta_fields', 15, 1 );
/**
 * Add front end profile edit extra fields
 *
 * @param $fields
 *
 * @return array
 */
function wpr_add_profile_tab_meta_fields( $fields ) {

	$fields[] = array(
		'id'      => 'user_email',
		'label'   => 'Email Address',
//		'desc'    => '',
		'desc'    => 'Edit your email address',
		'type'    => 'email',
		'classes' => 'user_email',
	);

	$fields[] = array(
		'id'      => 'first_name',
		'label'   => 'First Name',
//		'desc'    => '',
		'desc'    => 'Edit your first name.',
		'type'    => 'text',
		'classes' => 'first_name',
	);

	$fields[] = array(
		'id'      => 'last_name',
		'label'   => 'Last Name',
//		'desc'    => '',
		'desc'    => 'Edit your last name.',
		'type'    => 'text',
		'classes' => 'last_name',
	);

	$fields[] = array(
		'id'      => 'position',
		'label'   => 'Job title',
//		'desc'    => '',
		'desc'    => 'Edit your title.',
		'type'    => 'text',
		'classes' => 'position',
	);

	$fields[] = array(
		'id'      => 'dealership_url',
		'label'   => 'My dealership URL',
		'desc'    => 'Edit your dealership URL.',
		'type'    => 'text',
		'classes' => 'dealership_url',
	);

	$fields[] = array(
		'id'      => 'phone',
		'label'   => 'Phone',
//		'desc'    => '',
		'desc'    => 'Edit your phone.',
		'type'    => 'text',
		'classes' => 'phone',
	);

	$fields[] = array(
		'id'      => 'description',
		'label'   => 'Description/Bio',
		'desc'    => 'Edit your description/bio.',
		'type'    => 'wysiwyg',
		'classes' => 'description',
	);

	$fields[] = array(
		'id'      => 'wpbrand',
		'label'   => 'Brand',
		'desc'    => '',
		'type'    => 'text',
		'classes' => 'wpbrand',
	);

	$fields[] = array(
		'id'      => 'wpdealer',
		'label'   => 'Dealer',
		'desc'    => '',
		'type'    => 'text',
		'classes' => 'wpdealer',
	);

	$fields[] = array(
		'id'      => 'wpdealerzip',
		'label'   => 'Dealer ZipCode',
		'desc'    => '',
		'type'    => 'text',
		'classes' => 'wpdealerzip',
	);

	return $fields;
}

function wpr_replace_zip_code( $zip ) {
	$zip_code = $zip;
	if ( ! empty( $zip ) ) {
		$exp = explode( '-', $zip );
		if ( ! empty( $exp[0] ) ) {
			$zip_code = $exp[0];
		}
	}

	return $zip_code;
}

add_filter( 'wsl_render_auth_widget_alter_provider_name', 'wpr_linkedin_icon', 10, 1 );
/**
 * Change html elements to social login
 *
 * @param $provider_name
 *
 * @return string
 */
function wpr_linkedin_icon( $provider_name ) {
	if ( 'Facebook' == $provider_name ) {
		$provider_name = sprintf( '<span class="fa fa-facebook"></span> %s', __( 'Log in with Facebook' ) );
	}

	if ( 'LinkedIn' == $provider_name ) {
		$provider_name = sprintf( '<span class="fa fa-linkedin"></span> %s', __( 'Log in with LinkedIn' ) );
	}

	return $provider_name;
}

add_filter( 'oa_social_login_filter_registration_redirect_url', 'oa_social_login_do_before_user_redirect', 10, 2 );
add_filter( 'oa_social_login_filter_login_redirect_url', 'oa_social_login_do_before_user_redirect', 10, 2 );
/**
 * Redirect url Social Login
 *
 * @param $authenticate_url
 * @param $provider_id
 * @param $auth_mode
 * @param $redirect_to
 * @param $wsl_settings_use_popup
 *
 * @return string
 */
function oa_social_login_do_before_user_redirect( $url, $user_data ) {
	if ( isset( $_COOKIE['wpr_user_click'] ) && '' != $_COOKIE['wpr_user_click'] ) {
		setcookie( 'wpr_button_clicked', esc_attr( $_COOKIE['wpr_user_click'] ), 30 * DAYS_IN_SECONDS, COOKIEPATH, COOKIE_DOMAIN );

		$data = json_decode( stripslashes( $_COOKIE['wpr_user_click'] ), true );
		// redirect url
		if ( ! empty( $data['current_url'] ) ) {
			return esc_url( $data['current_url'] );
		}
	}

	if ( isset( $_GET['redirect'] ) && '' !== $_GET['redirect'] ) {
		return urlencode( $_GET['redirect'] );
	} else {
		return site_url();
	}
}

add_filter( 'body_class', 'wpr_body_classes' );
/**
 * @param $classes
 *
 * @return array
 */
function wpr_body_classes( $classes ) {
	if ( is_page( array( 'login', 'register' ) ) ) {
		$classes[] = 'wpr-login-register-body-class';
	}

	return $classes;
}

add_action( 'wp_ajax_wpr_get_ba_brand_dealer_info', 'wpr_get_ba_brand_dealer_info' );
/**
 * Fill BA Brand & Dealer Fields
 */
function wpr_get_ba_brand_dealer_info() {
	check_ajax_referer( 'wpr-ba', 'banonce' );

	$send_data = array();
	if ( is_user_logged_in() ) {
		$user_id = get_current_user_id();

		if ( $user_id ) {
			$user_brand  = get_user_meta( $user_id, 'wpr_brand_association', true );
			$user_dealer = get_user_meta( $user_id, 'wpr_dealer_association', true );

			if ( $user_brand && $user_dealer ) {
				switch_to_blog( 1 );
				$send_data['user_brand'] = esc_attr( get_cat_name( absint( $user_brand ) ) );
				restore_current_blog();
				$send_data['user_dealer'] = htmlspecialchars_decode( get_the_title( absint( $user_dealer ) ) );
			}

			$user_dealer_zip = get_user_meta( $user_id, 'zip_code', true );
			if ( $user_dealer_zip ) {
				$send_data['user_dealer_zip'] = esc_attr( $user_dealer_zip );
			}
		}
	}

	echo wp_json_encode( array( 'response' => $send_data ) );

	wp_die();
}

add_filter( 'gform_submit_button_1', 'add_paragraph_below_submit', 10, 2 );
/**
 * Add content before submit button
 *
 * @param $button
 * @param $form
 *
 * @return string
 */
function add_paragraph_below_submit( $button, $form ) {

	$before_button = '<a href="' . esc_url( get_permalink( get_page_by_path( 'password-reset' ) ) ) . '" class="wpr-forgot-password">' . __( 'Forgot password?' ) . '</a>';
	$before_button .= $button;

	return $before_button;
}

add_filter( 'gform_username', 'wpr_auto_generate_username', 10, 4 );
/**
 * Generate user name on user register
 *
 * @param $username
 * @param $feed
 * @param $form
 * @param $entry
 *
 * @return string
 */
function wpr_auto_generate_username( $username, $feed, $form, $entry ) {
	$username = strtolower( rgar( $entry, '4.3' ) . rgar( $entry, '4.6' ) );
	$username = trim( $username ) . uniqid();

	if ( empty( $username ) ) {
		return $username;
	}

	if ( ! function_exists( 'username_exists' ) ) {
		require_once( ABSPATH . WPINC . '/registration.php' );
	}

	if ( username_exists( $username ) ) {
		$i = 2;
		while ( username_exists( $username . $i ) ) {
			$i ++;
		}
		$username = $username . $i;
	};

	return $username;
}

add_action( 'wpfep_before_tab_content', 'wpr_add_avatar_before_profile', 10, 2 );
/**
 * @param $tab
 * @param $user_id
 */
function wpr_add_avatar_before_profile( $tab, $user_id ) {
	if ( $tab != 'profile' ) {
		return;
	}

	echo '<div class="wpr-profile-form">';
//	echo do_shortcode( '[avatar_upload]' );
}

add_action( 'wpfep_after_tab_content', 'wpr_after_profile', 10, 2 );
/**
 * @param $tab
 * @param $user_id
 */
function wpr_after_profile( $tab, $user_id ) {
	if ( $tab != 'profile' ) {
		return;
	}

	echo '</div>';
}

add_filter( 'get_avatar', 'wpr_custom_user_avatar', 10, 5 );
/**
 * User avatar
 *
 * @param $avatar
 * @param $id_or_email
 * @param $size
 * @param $default
 * @param $alt
 *
 * @return string
 */
function wpr_custom_user_avatar( $avatar, $id_or_email, $size, $default, $alt ) {
	$user   = false;
	$screen = get_current_screen();

	if ( is_numeric( $id_or_email ) ) {

		$id   = (int) $id_or_email;
		$user = get_user_by( 'id', $id );

	} elseif ( is_object( $id_or_email ) ) {

		if ( ! empty( $id_or_email->user_id ) ) {
			$id   = (int) $id_or_email->user_id;
			$user = get_user_by( 'id', $id );
		}

	} else {
		$user = get_user_by( 'email', $id_or_email );
	}

	if ( $user && is_object( $user ) ) {
		if ( is_admin() && 'user-edit' != $screen->base ) {
			$size = 32;
		} else {
			$size = 200;
		}

		$avatar_url = get_user_meta( $user->data->ID, 'wpr_user_avatar', true );
		if ( ! $avatar_url ) {
			$avatar_pro_url = get_user_meta( $user->data->ID, 'wp_user_avatar', true );
			if ( $avatar_pro_url ) {
				$avatar_url = esc_url( $avatar_pro_url[0]['avatar_url'] );
			}
		}
		if ( $avatar_url ) {
			$avatar = "<img alt='{$alt}' src='{$avatar_url}' class='avatar avatar-{$size} photo' height='{$size}' width='{$size}' />";
		}
	}

	return $avatar;
}

add_action( 'wpfep_after_tab_fields', 'wpr_add_front_end_avatar', 5, 2 );
/**
 * Add upload avatar input
 *
 * @param $tab
 * @param $user_id
 */
function wpr_add_front_end_avatar( $tab, $user_id ) {
	if ( $tab['id'] != 'profile' ) {
		return;
	}

	$user_avatar = get_user_meta( $user_id, 'wpr_user_avatar', true );
	if ( ! $user_avatar ) {
		$user_avatar = get_avatar( $user_id );
	}
	?>
    <div class="wpfep-field wpfep-text-field wpfep-field-9 last wpdealer wpr_edit_offer" id="wpfep-field-wpdealer">
		<?php _e( 'Avatar', 'wpr-user-avatar' ); ?>
        <div>
            <label class="custom-file-upload">
                <input type="file" class="wpr-inputfile" name="wpr_user_avatar"/>
                <span class="wpr_input_file">
                    <i class="fa fa-cloud-upload"></i> <?php _e( 'Choose file', 'wpr-user-avatar' ); ?>
                </span>
            </label>
        </div>
		<?php
		if ( empty( $user_avatar ) ) {
			echo '<span class="description">' . __( 'No local avatar is set. Use the upload field to add a local avatar.', 'wpr-user-avatar' ) . '</span>';
		} else {
			echo get_avatar( $user_id );
			echo '<div>';
			echo '<span class="description">' . __( 'Replace the local avatar by uploading a new avatar.', 'wpr-user-avatar' ) . '</span>';
			echo '</div>';
		}
		?>
    </div>

    <script type="text/javascript">
        var form = document.getElementsByClassName('wpfep-form-profile');
        if (form.length) {
            form[0].encoding = 'multipart/form-data';
            form[0].setAttribute('enctype', 'multipart/form-data');
        }
    </script>
	<?php
}

add_action( 'wpfep_before_tabs', 'wpr_save_user_avatar', 10, 2 );
/**
 * Save avatar in wp-front-end-profile
 *
 * @param $tabs
 * @param $user_id
 */
function wpr_save_user_avatar( $tabs, $user_id ) {
	/* check the nonce */
	if ( ! isset( $_POST['wpfep_nonce_name'] ) || ! wp_verify_nonce( $_POST['wpfep_nonce_name'], 'wpfep_nonce_action' ) ) {
		return;
	}

	/* set an array to store messages in */
	$messages = array();

	/* get the FILE data */
	$profile_avatar = $_FILES['wpr_user_avatar'];

	if ( ! empty( $profile_avatar['name'] ) ) {
		// Allowed file extensions/types
		$mimes = array(
			'jpg|jpeg|jpe' => 'image/jpeg',
			'gif'          => 'image/gif',
			'png'          => 'image/png',
		);
		// Front end support - shortcode, bbPress, etc
		if ( ! function_exists( 'wp_handle_upload' ) ) {
			require_once ABSPATH . 'wp-admin/includes/file.php';
		}
		// Delete old images if successful
		wpr_avatar_delete( $user_id );
		// Need to be more secure since low privelege users can upload
		if ( strstr( $profile_avatar['name'], '.php' ) ) {
			wp_die( 'For security reasons, the extension ".php" cannot be in your file name.' );
		}
		// Make user_id known to unique_filename_callback function
		$upload_dir = wp_upload_dir();
		$ext        = explode( '.', $profile_avatar['name'] );
		$avatar     = wp_handle_upload( $profile_avatar, array(
			'mimes'                    => $mimes,
			'test_form'                => false,
			'unique_filename_callback' => unique_filename_callback( $upload_dir['basedir'], $profile_avatar['name'], end( $ext ) )
		) );
		// Handle failures
		if ( empty( $avatar['file'] ) ) {
			switch ( $avatar['error'] ) {
				case 'File type does not meet security guidelines. Try another.' :
					$messages['update_failed'] = __( "Please upload a valid image file for the avatar.", "wpr-user-avatar" );
					break;
				default :
					$messages['update_failed'] = __( "There was an error uploading the avatar:", "wpr-user-avatar" ) . ': ' . esc_attr( $avatar['error'] );
			}

			return;
		}
		// Save user information (overwriting previous)
		update_user_meta( $user_id, 'wpr_user_avatar', $avatar['url'] );
	} elseif ( ! empty( $_POST['basic-user-avatar-erase'] ) ) {
		// Nuke the current avatar
		wpr_avatar_delete( $user_id );
	}
}

add_action( 'personal_options_update', 'wpr_save_user_avatar_admin' );
add_action( 'edit_user_profile_update', 'wpr_save_user_avatar_admin' );
/**
 * Save avatar in admin
 *
 * @param $tabs
 * @param $user_id
 */
function wpr_save_user_avatar_admin( $user_id ) {
	/* get the FILE data */
	$profile_avatar = $_FILES['wpr_user_avatar'];

	if ( ! empty( $profile_avatar['name'] ) ) {
		// Allowed file extensions/types
		$mimes = array(
			'jpg|jpeg|jpe' => 'image/jpeg',
			'gif'          => 'image/gif',
			'png'          => 'image/png',
		);
		// Front end support - shortcode, bbPress, etc
		if ( ! function_exists( 'wp_handle_upload' ) ) {
			require_once ABSPATH . 'wp-admin/includes/file.php';
		}
		// Delete old images if successful
		wpr_avatar_delete( $user_id );
		// Need to be more secure since low privelege users can upload
		if ( strstr( $profile_avatar['name'], '.php' ) ) {
			wp_die( 'For security reasons, the extension ".php" cannot be in your file name.' );
		}
		// Make user_id known to unique_filename_callback function
		$upload_dir = wp_upload_dir();
		$ext        = explode( '.', $profile_avatar['name'] );
		$avatar     = wp_handle_upload( $profile_avatar, array(
			'mimes'                    => $mimes,
			'test_form'                => false,
			'unique_filename_callback' => unique_filename_callback( $upload_dir['basedir'], $profile_avatar['name'], end( $ext ) )
		) );
		// Handle failures
		if ( empty( $avatar['file'] ) ) {
			switch ( $avatar['error'] ) {
				case 'File type does not meet security guidelines. Try another.' :
					add_action( 'user_profile_update_errors', create_function( '$a', '$a->add("avatar_error",__("Please upload a valid image file for the avatar.","basic-user-avatars"));' ) );
					break;
				default :
					add_action( 'user_profile_update_errors', create_function( '$a', '$a->add("avatar_error","<strong>".__("There was an error uploading the avatar:","basic-user-avatars")."</strong> ' . esc_attr( $avatar['error'] ) . '");' ) );
			}

			return;
		}
		// Save user information (overwriting previous)
		update_user_meta( $user_id, 'wpr_user_avatar', $avatar['url'] );
	} elseif ( ! empty( $_POST['basic-user-avatar-erase'] ) ) {
		// Nuke the current avatar
		wpr_avatar_delete( $user_id );
	}
}

/**
 * Delete avatar
 *
 * @param $user_id
 */
function wpr_avatar_delete( $user_id ) {
	$old_avatar  = get_user_meta( $user_id, 'wpr_user_avatar', true );
	$upload_path = wp_upload_dir();
	if ( $old_avatar ) {
		$old_avatar_path = str_replace( $upload_path['baseurl'], $upload_path['basedir'], $old_avatar );
		@unlink( $old_avatar_path );
	}
	delete_user_meta( $user_id, 'wpr_user_avatar' );
}

/**
 * Generate unique file name
 *
 * @param $dir
 * @param $name
 * @param $ext
 *
 * @return string
 */
function unique_filename_callback( $dir, $name, $ext ) {
	$user   = get_user_by( 'id', get_current_user_id() );
	$name   = $base_name = sanitize_file_name( $user->display_name . '_avatar' );
	$number = 1;
	while ( file_exists( $dir . "/$name.$ext" ) ) {
		$name = $base_name . '_' . $number;
		$number ++;
	}

	return $name . '.' . $ext;
}

add_action( 'show_user_profile', 'wpr_edit_user_profile' );
add_action( 'edit_user_profile', 'wpr_edit_user_profile' );
/**
 * @param $profileuser
 */
function wpr_edit_user_profile( $profileuser ) {
	$user_avatar = get_user_meta( $profileuser->ID, 'wpr_user_avatar', true );
	if ( ! $user_avatar ) {
		$user_avatar = get_avatar( $profileuser->ID );
	}
	?>

    <h3><?php _e( 'Avatar', 'wpr-user-avatar' ); ?></h3>
    <table class="form-table">
        <tr>
            <th><label for="basic-user-avatar"><?php _e( 'Upload Avatar', 'wpr-user-avatar' ); ?></label></th>
            <td style="width: 50px;" valign="top">
				<?php echo get_avatar( $profileuser->ID ); ?>
            </td>
            <td>
				<?php
				if ( current_user_can( 'upload_files' ) ) {
					// Nonce security ftw
					wp_nonce_field( 'basic_user_avatar_nonce', '_basic_user_avatar_nonce', false );

					// File upload input
					echo '<input type="file" class="wpr-inputfile" name="wpr_user_avatar"/><br />';
					if ( empty( $user_avatar ) ) {
						echo '<span class="description">' . __( 'No local avatar is set. Use the upload field to add a local avatar.', 'wpr-user-avatar' ) . '</span>';
					} else {
						echo '<span class="description">' . __( 'Replace the local avatar by uploading a new avatar, or erase the local avatar (falling back to a gravatar) by checking the delete option.', 'wpr-user-avatar' ) . '</span>';
					}
				} else {
					if ( empty( $user_avatar ) ) {
						echo '<span class="description">' . __( 'No local avatar is set. Set up your avatar at Gravatar.com.', 'wpr-user-avatar' ) . '</span>';
					} else {
						echo '<span class="description">' . __( 'You do not have media management permissions. To change your local avatar, contact the site administrator.', 'wpr-user-avatar' ) . '</span>';
					}
				}
				?>
            </td>
        </tr>
    </table>
    <script type="text/javascript">
        var form = document.getElementById('your-profile');
        form.encoding = 'multipart/form-data';
        form.setAttribute('enctype', 'multipart/form-data');
    </script>
	<?php
}

add_action( 'wp_footer', 'wpr_gf_script' );
/**
 * Add GF Terms&Cond URL
 */
function wpr_gf_script() {
	$terms_url = get_the_permalink( get_page_by_path( 'terms-and-conditions' ) );
	?>
    <script type="text/javascript">
        jQuery.noConflict();
        jQuery(document).ready(function ($) {
            $('.gchoice_2_15_1 label, .gchoice_8_10_1 label').html('<?php echo sprintf( '%s <a href="%s" target="_blank">%s</a>', __( 'I have read and accept the' ), esc_url( $terms_url ), __( 'terms and conditions' ) ); ?>');
        });
    </script>
	<?php
}

/**
 * Get brand url
 *
 * @param null $user_id
 *
 * @return mixed|string
 */
function wpr_user_category_url( $user_id = null ) {
	$category_link = '';
	if ( ! $user_id ) {
		return $category_link;
	}

	$get_brand = get_user_meta( $user_id, 'wpr_brand_association', true );
	if ( ! $get_brand ) {
		return $category_link;
	}

	switch_to_blog( 1 );
	$category_link = get_category_link( absint( $get_brand ) ) . wpr_ba_unique_token();
	restore_current_blog();

	return esc_url( $category_link );
}

/**
 * Got token
 *
 * @return mixed|string
 */
function wpr_ba_unique_token() {
	$token      = '';
	$user_id    = get_current_user_id();
	$user_token = get_user_meta( $user_id, 'wpr_ba_unique_token', true );
	if ( $user_token ) {
		$token = "?bakey=$user_token";
	}

	return $token;
}

/**
 * Declare new thumbnail size
 */
function declare_custom_offer_thumbnail() {
	add_image_size( 'offer_thumb', 120, 120, false );
}

add_action( 'init', 'declare_custom_offer_thumbnail' );

add_filter( 'gform_field_input', 'add_avatar_on_fill_account', 10, 5 );
/**
 * Add content before submit button
 *
 * @param $button
 * @param $form
 *
 * @return string
 */
function add_avatar_on_fill_account( $input, $field, $value, $lead_id, $form_id ) {
	if ( $field->cssClass == 'wpr_fill_brand_avatar' ) {
		$show_avatar = $before_button = '';
		$user_id     = get_current_user_id();
		$user_avatar = get_user_meta( $user_id, 'wpr_user_avatar', true );
		if ( $user_avatar ) {
			$show_avatar = '<img src="' . $user_avatar . '" width="200" />';
		}

		if ( ! $user_avatar ) {
			$show_avatar = get_avatar( $user_id );
		}

		if ( $show_avatar ) {
			$before_button = sprintf( '<div>Current Avatar:<br />%s</div>', $show_avatar );
		}

		$before_button = apply_filters( 'wpr_add_element_after_avatar', $before_button );

		$input = $before_button;
	}

	return $input;
}

add_filter( 'gform_pre_render', 'wpr_show_only_empty_fields' );
add_filter( 'gform_pre_validation', 'wpr_show_only_empty_fields' );
#add_filter( 'gform_admin_pre_render', 'wpr_show_only_empty_fields' );
add_filter( 'gform_pre_submission_filter', 'wpr_show_only_empty_fields' );
/**
 * Remove fields
 *
 * @param $form
 *
 * @return mixed
 */
function wpr_show_only_empty_fields( $form ) {
	if ( $form['id'] != 8 ) {
		return $form;
	}
	$i = 0;

	$user_fields = array();
	$user_id     = get_current_user_id();
	$user_info   = get_userdata( $user_id );

	$first_name = get_user_meta( $user_id, 'first_name', true );
	if ( ! $first_name ) {
		array_push( $user_fields, 4 );
	}

	$last_name = get_user_meta( $user_id, 'last_name', true );
	if ( ! $last_name ) {
		array_push( $user_fields, 5 );
	}

	if ( ! $user_info->user_email ) {
		array_push( $user_fields, 7 );
	}

	$phone = get_user_meta( $user_id, 'phone', true );
	if ( ! $phone ) {
		array_push( $user_fields, 9 );
	}

	$dealer = get_user_meta( $user_id, 'wpr_dealer_association', true );
	if ( ! $dealer ) {
		array_push( $user_fields, 2 );
		array_push( $user_fields, 3 );
	}

	$position = get_user_meta( $user_id, 'position', true );
	if ( ! $position ) {
		array_push( $user_fields, 8 );
	}

	$description = get_user_meta( $user_id, 'description', true );
	if ( ! $description ) {
		array_push( $user_fields, 6 );
	}

	$user_avatar = get_user_meta( $user_id, 'wpr_user_avatar', true );
	if ( ! $user_avatar ) {
		$user_avatar = get_avatar_url( $user_id );
	}
	$user_avatar = parse_url( $user_avatar );

	if ( preg_match( "/gravatar.com/", $user_avatar['host'] ) ) {
		$user_avatar = '';
	}
	if ( ! $user_avatar ) {
		array_push( $user_fields, 11 );
		array_push( $user_fields, 12 );
	}

	$social_login = get_user_meta( $user_id, 'oa_social_login_identity_provider', true );
	if ( 'Facebook' == $social_login || 'LinkedIn' == $social_login ) {
		array_push( $user_fields, 10 );
	}

	foreach ( $form['fields'] as &$field ) {
		if ( ! in_array( $field->id, $user_fields ) ) {
			unset ( $form['fields'][ $i ] );
		}
		$i ++;
	}

	return $form;
}

add_filter( 'gform_field_validation_2_6', 'wpr_validate_dealer_selection', 10, 4 );
add_filter( 'gform_field_validation_8_3', 'wpr_validate_dealer_selection', 10, 4 );
/**
 * Validate Dealer availability on BA register
 *
 * @param $result
 * @param $value
 * @param $form
 * @param $field
 *
 * @return mixed
 */
function wpr_validate_dealer_selection( $result, $value, $form, $field ) {
	$selected_dealer = absint( $value );

	if ( ! empty( $selected_dealer ) ) {
		$user_query = new WP_User_Query(
			array( 'meta_key' => '', 'meta_value' => $selected_dealer )
		);
		$users      = $user_query->get_results();

		if ( ! empty( $users ) ) {
			$result['is_valid'] = false;
			$result['message']  = __( 'The current Dealer is not available.' );
		}
	}

	return $result;
}

add_filter( 'oa_social_login_filter_new_user_role', 'oa_social_login_set_new_user_role' );
/**
 * Set default user role
 *
 * @param $user_role
 *
 * @return mixed|string|void
 */
function oa_social_login_set_new_user_role( $user_role ) {
	//This is an example for a custom setting with two roles
	$user_role = 'business_associate';

	//The new user will be assigned this role
	return $user_role;
}

/**
 * Disable email send on account creation & password change
 */
add_filter( 'send_password_change_email', '__return_false' );
add_filter( 'send_email_change_email', '__return_false' );

/**
 * GF scroll to validation message upon submission
 */
add_filter( 'gform_confirmation_anchor', '__return_true' );


//add_action( 'gform_after_submission_9', 'wpr_login_form_after_submission', 50, 2 );
/**
 * Autologin ABC after he finish registration
 *
 * @param $entry
 * @param $form
 */
function wpr_login_form_after_submission( $entry, $form ) {
	// get the username and pass
	$username = $entry[1];
	// create the credentials array
	if ( filter_var( $username, FILTER_VALIDATE_EMAIL ) ) {
		$user = get_user_by( 'email', $username );
	} else {
		$user = get_user_by( 'login', sanitize_user( $username ) );
	}

	if ( ! empty( $user ) ) {
		// First clear session
		clean_user_cache( $user->ID );
		wp_cache_delete( $user->ID, 'users' );
		wp_cache_delete( $user->user_login, 'userlogins' );
//		wp_logout();

		// Login user
		wp_set_current_user( $user->ID );
		wp_set_auth_cookie( $user->ID, true, false );
//		update_user_caches( $user );
	}
}

if ( function_exists( 'oa_social_login_render_login_form' ) ) {
	/**
	 * Setup Social Login Shortcode handler
	 */
	function wpr_oa_social_login_shortcode_handler( $args ) {
		return oa_social_login_render_login_form( 'shortcode' );
	}

	add_shortcode( 'oa_social_login_wpr', 'wpr_oa_social_login_shortcode_handler' );
}

if ( function_exists( 'oa_social_login_callback' ) ) {
	remove_action( 'init', 'oa_social_login_init', 9 );

	add_action( 'init', 'wpr_oa_social_login_init', 9 );
}

function wpr_oa_social_login_init() {
	//Add language file.
	if ( function_exists( 'load_plugin_textdomain' ) ) {
		load_plugin_textdomain( 'oa_social_login', false, OA_SOCIAL_LOGIN_BASE_PATH . '/languages/' );
	}

	//Launch the callback handler.
	wpr_oa_social_login_callback();
}

function wpr_oa_social_login_callback() {
	//Callback Handler
	if ( isset ( $_POST ) AND ! empty ( $_POST ['oa_action'] ) AND $_POST ['oa_action'] == 'social_login' AND ! empty ( $_POST ['connection_token'] ) ) {
		//OneAll Connection token
		$connection_token = trim( $_POST ['connection_token'] );

		//Read settings
		$settings = get_option( 'oa_social_login_settings' );

		//API Settings
		$api_connection_handler   = ( ( ! empty ( $settings ['api_connection_handler'] ) AND $settings ['api_connection_handler'] == 'fsockopen' ) ? 'fsockopen' : 'curl' );
		$api_connection_use_https = ( ( ! isset ( $settings ['api_connection_use_https'] ) OR $settings ['api_connection_use_https'] == '1' ) ? true : false );
		$api_subdomain            = ( ! empty ( $settings ['api_subdomain'] ) ? trim( $settings ['api_subdomain'] ) : '' );

		//We cannot make a connection without a subdomain
		if ( ! empty ( $api_subdomain ) ) {
			//See: http://docs.oneall.com/api/resources/connections/read-connection-details/
			$api_resource_url = ( $api_connection_use_https ? 'https' : 'http' ) . '://' . $api_subdomain . '.api.oneall.com/connections/' . $connection_token . '.json';

			//API Credentials
			$api_opts               = array();
			$api_opts['api_key']    = ( ! empty ( $settings ['api_key'] ) ? $settings ['api_key'] : '' );
			$api_opts['api_secret'] = ( ! empty ( $settings ['api_secret'] ) ? $settings ['api_secret'] : '' );

			//Retrieve connection details
			$result = oa_social_login_do_api_request( $api_connection_handler, $api_resource_url, $api_opts );

			//Check result
			if ( is_object( $result ) AND property_exists( $result, 'http_code' ) AND $result->http_code == 200 AND property_exists( $result, 'http_data' ) ) {
				//Decode result
				$decoded_result = @json_decode( $result->http_data );
				if ( is_object( $decoded_result ) AND isset ( $decoded_result->response->result->data->user ) ) {
					//User data
					$user_data = $decoded_result->response->result->data->user;

					//Social network profile data
					$identity = $user_data->identity;

					//Unique user token provided by OneAll
					$user_token = $user_data->user_token;

					//Identity Provider
					$user_identity_provider = $identity->source->name;

					//Thumbnail
					$user_thumbnail = ( ! empty ( $identity->thumbnailUrl ) ? trim( $identity->thumbnailUrl ) : '' );

					//Picture
					$user_picture = ( ! empty ( $identity->pictureUrl ) ? trim( $identity->pictureUrl ) : '' );

					//About Me
					$user_about_me = ( ! empty ( $identity->aboutMe ) ? trim( $identity->aboutMe ) : '' );

					//Note
					$user_note = ( ! empty ( $identity->note ) ? trim( $identity->note ) : '' );

					//Firstname
					$user_first_name = ( ! empty ( $identity->name->givenName ) ? $identity->name->givenName : '' );

					//Lastname
					$user_last_name = ( ! empty ( $identity->name->familyName ) ? $identity->name->familyName : '' );

					//Fullname
					if ( ! empty ( $identity->name->formatted ) ) {
						$user_full_name = $identity->name->formatted;
					} elseif ( ! empty ( $identity->name->displayName ) ) {
						$user_full_name = $identity->name->displayName;
					} else {
						$user_full_name = trim( $user_first_name . ' ' . $user_last_name );
					}

					// Email Address.
					$user_email = '';
					if ( property_exists( $identity, 'emails' ) AND is_array( $identity->emails ) ) {
						$user_email_is_verified = false;
						while ( $user_email_is_verified !== true AND ( list( , $email ) = each( $identity->emails ) ) ) {
							$user_email             = $email->value;
							$user_email_is_verified = ( $email->is_verified == '1' );
						}
					}

					//User Website
					if ( ! empty ( $identity->profileUrl ) ) {
						$user_website = $identity->profileUrl;
					} elseif ( ! empty ( $identity->urls [0]->value ) ) {
						$user_website = $identity->urls [0]->value;
					} else {
						$user_website = '';
					}

					//Preferred Username
					if ( ! empty ( $identity->preferredUsername ) ) {
						$user_login = $identity->preferredUsername;
					} elseif ( ! empty ( $identity->displayName ) ) {
						$user_login = $identity->displayName;
					} else {
						$user_login = $user_full_name;
					}

					//New user created?
					$new_registration = false;

					//Sanitize Login
					$user_login = str_replace( '.', '-', $user_login );
					$user_login = sanitize_user( $user_login, true );

					// Check if the user is logged in
					if ( is_user_logged_in() ) {
						$user_id = get_current_user_id();

						//Refresh the meta data
						delete_metadata( 'user', null, 'oa_social_login_user_token', $user_token, true );
						update_user_meta( $user_id, 'oa_social_login_user_token', $user_token );
						update_user_meta( $user_id, 'oa_social_login_identity_provider', $user_identity_provider );

					} else {
						// Get user by token
						$user_id = oa_social_login_get_userid_by_token( $user_token );
					}

					//Try to link to existing account
					if ( ! is_numeric( $user_id ) ) {
						//This is a new user
						$new_registration = true;

						//Linking enabled?
						if ( ! isset ( $settings ['plugin_link_verified_accounts'] ) OR $settings ['plugin_link_verified_accounts'] == '1' ) {
							//Only if email is verified
							if ( ! empty ( $user_email ) AND $user_email_is_verified === true ) {
								//Read existing user
								if ( ( $user_id_tmp = email_exists( $user_email ) ) !== false ) {
									$user_data = get_userdata( $user_id_tmp );
									if ( $user_data !== false ) {
										$user_id    = $user_data->ID;
										$user_login = $user_data->user_login;

										//Refresh the meta data
										delete_metadata( 'user', null, 'oa_social_login_user_token', $user_token, true );
										update_user_meta( $user_id, 'oa_social_login_user_token', $user_token );
										update_user_meta( $user_id, 'oa_social_login_identity_provider', $user_identity_provider );

										//Refresh the cache
										wp_cache_delete( $user_id, 'users' );
										wp_cache_delete( $user_login, 'userlogins' );
									}
								}
							}
						}
					}


					//New User
					if ( ! is_numeric( $user_id ) ) {
						//Username is mandatory
						if ( ! isset ( $user_login ) OR strlen( trim( $user_login ) ) == 0 ) {
							$user_login = $user_identity_provider . 'User';
						}

						// BuddyPress : See bp_core_strip_username_spaces()
						if ( function_exists( 'bp_core_strip_username_spaces' ) ) {
							$user_login = str_replace( ' ', '-', $user_login );
						}

						//Username must be unique
						if ( username_exists( $user_login ) ) {
							$i              = 1;
							$user_login_tmp = $user_login;
							do {
								$user_login_tmp = $user_login . ( $i ++ );
							} while ( username_exists( $user_login_tmp ) );
							$user_login = $user_login_tmp;
						}

						//Email Filter
						$user_email = apply_filters( 'oa_social_login_filter_new_user_email', $user_email );

						//Email must be unique
						$placeholder_email_used = false;
						if ( ! isset ( $user_email ) OR ! is_email( $user_email ) OR email_exists( $user_email ) ) {
							$user_email             = oa_social_login_create_rand_email();
							$placeholder_email_used = true;
						}

						//Setup the user's password
						$user_password = wp_generate_password();
						$user_password = apply_filters( 'oa_social_login_filter_new_user_password', $user_password );

						//Setup the user's role
						$user_role = get_option( 'default_role' );
						$user_role = apply_filters( 'oa_social_login_filter_new_user_role', $user_role );

						//Build user data
						$user_fields = array(
							'user_login'   => $user_login,
							'display_name' => ( ! empty ( $user_full_name ) ? $user_full_name : $user_login ),
							'user_email'   => $user_email,
							'first_name'   => $user_first_name,
							'last_name'    => $user_last_name,
							'user_url'     => $user_website,
							'user_pass'    => $user_password,
							'role'         => $user_role
						);

						//Filter for user_data
						$user_fields = apply_filters( 'oa_social_login_filter_new_user_fields', $user_fields );

						//Hook before adding the user
						do_action( 'oa_social_login_action_before_user_insert', $user_fields, $identity );

						// Create a new user
						$user_id = wp_insert_user( $user_fields );
						if ( is_numeric( $user_id ) AND ( $user_data = get_userdata( $user_id ) ) !== false ) {
							//Refresh the meta data
							delete_metadata( 'user', null, 'oa_social_login_user_token', $user_token, true );

							//Save OneAll user meta-data
							update_user_meta( $user_id, 'oa_social_login_user_token', $user_token );
							update_user_meta( $user_id, 'oa_social_login_identity_provider', $user_identity_provider );

							//Save WordPress user meta-data
							if ( ! empty ( $user_about_me ) OR ! empty ( $user_note ) ) {
								$user_description = ( ! empty ( $user_about_me ) ? $user_about_me : $user_note );
								update_user_meta( $user_id, 'description', $user_description );
							}

							//Email is required
							if ( ! empty ( $settings ['plugin_require_email'] ) ) {
								//We don't have the real email
								if ( $placeholder_email_used ) {
									update_user_meta( $user_id, 'oa_social_login_request_email', 1 );
								}
							}

							//Notify Administrator
							if ( ! empty ( $settings ['plugin_notify_admin'] ) ) {
								oa_social_login_user_notification( $user_id, $user_identity_provider );
							}

							//Refresh the cache
							wp_cache_delete( $user_id, 'users' );
							wp_cache_delete( $user_login, 'userlogins' );

							//WordPress hook
							do_action( 'user_register', $user_id );

							//Social Login Hook
							do_action( 'oa_social_login_action_after_user_insert', $user_data, $identity );
						}
					}

					//Sucess
					$user_data = get_userdata( $user_id );
					if ( $user_data !== false ) {
						//Hooks to be used by third parties
						do_action( 'oa_social_login_action_before_user_login', $user_data, $identity, $new_registration );

						//Update user thumbnail
						if ( ! empty ( $user_thumbnail ) ) {
							update_user_meta( $user_id, 'oa_social_login_user_thumbnail', $user_thumbnail );
						}

						//Update user picture
						if ( ! empty ( $user_picture ) ) {
							update_user_meta( $user_id, 'oa_social_login_user_picture', $user_picture );
						}

						//Set the cookie and login
						wp_clear_auth_cookie();
						wp_set_auth_cookie( $user_data->ID, true );
						do_action( 'wp_login', $user_data->user_login, $user_data );

						//Where did the user come from?
						$oa_social_login_source = ( ! empty ( $_REQUEST ['oa_social_login_source'] ) ? strtolower( trim( $_REQUEST ['oa_social_login_source'] ) ) : '' );

						//Use safe redirection?
						$redirect_to_safe = false;

						//Build the url to redirect the user to
						switch ( $oa_social_login_source ) {
							//*************** Registration ***************
							case 'registration':
								//Default redirection
								$redirect_to = admin_url();

								//Redirection in URL
								if ( ! empty ( $_GET ['redirect_to'] ) ) {
									$redirect_to      = $_GET ['redirect_to'];
									$redirect_to_safe = true;
								} else {
									//Redirection customized
									if ( isset ( $settings ['plugin_registration_form_redirect'] ) ) {
										switch ( strtolower( $settings ['plugin_registration_form_redirect'] ) ) {
											//Current
											case 'current':
												$redirect_to = oa_social_login_get_current_url();
												break;

											//Homepage
											case 'homepage':
												$redirect_to = home_url();
												break;

											//Custom
											case 'custom':
												if ( isset ( $settings ['plugin_registration_form_redirect_custom_url'] ) AND strlen( trim( $settings ['plugin_registration_form_redirect_custom_url'] ) ) > 0 ) {
													$redirect_to = trim( $settings ['plugin_registration_form_redirect_custom_url'] );
												}
												break;

											//Default/Dashboard
											default:
											case 'dashboard':
												$redirect_to = admin_url();
												break;
										}
									}
								}
								break;


							//*************** Login ***************
							case 'login':
								//Default redirection
								$redirect_to = home_url();

								//Redirection in URL
								if ( ! empty ( $_GET ['redirect_to'] ) ) {
									$redirect_to      = $_GET ['redirect_to'];
									$redirect_to_safe = true;
								} else {
									//Redirection customized
									if ( isset ( $settings ['plugin_login_form_redirect'] ) ) {
										switch ( strtolower( $settings ['plugin_login_form_redirect'] ) ) {
											//Current
											case 'current':

												global $pagenow;

												//Do not redirect to the login page as this would logout the user.
												if ( empty ( $pagenow ) OR $pagenow <> 'wp-login.php' ) {
													$redirect_to = oa_social_login_get_current_url();
												} //In this case just go to the home page
												else {
													$redirect_to = home_url();
												}
												break;

											//Dashboard
											case 'dashboard':
												$redirect_to = admin_url();
												break;

											//Custom
											case 'custom':
												if ( isset ( $settings ['plugin_login_form_redirect_custom_url'] ) AND strlen( trim( $settings ['plugin_login_form_redirect_custom_url'] ) ) > 0 ) {
													$redirect_to = trim( $settings ['plugin_login_form_redirect_custom_url'] );
												}
												break;

											//Default/Homepage
											default:
											case 'homepage':
												$redirect_to = home_url();
												break;
										}
									}
								}
								break;

							// *************** Comments ***************
							case 'comments':
								$redirect_to = oa_social_login_get_current_url() . '#comments';
								break;

							//*************** Widget/Shortcode ***************
							default:
							case 'widget':
							case 'shortcode':
								// This is a new user
								$opt_key = ( $new_registration === true ? 'register' : 'login' );

								//Default value
								$redirect_to = oa_social_login_get_current_url();

								//Redirection customized
								if ( isset ( $settings [ 'plugin_shortcode_' . $opt_key . '_redirect' ] ) ) {
									switch ( strtolower( $settings [ 'plugin_shortcode_' . $opt_key . '_redirect' ] ) ) {
										//Current
										case 'current':
											$redirect_to = oa_social_login_get_current_url();
											break;

										//Homepage
										case 'homepage':
											$redirect_to = home_url();
											break;

										//Dashboard
										case 'dashboard':
											$redirect_to = admin_url();
											break;

										//Custom
										case 'custom':
											if ( isset ( $settings [ 'plugin_shortcode_' . $opt_key . '_redirect_url' ] ) AND strlen( trim( $settings [ 'plugin_shortcode_' . $opt_key . '_redirect_url' ] ) ) > 0 ) {
												$redirect_to = trim( $settings [ 'plugin_shortcode_' . $opt_key . '_redirect_url' ] );
											}
											break;
									}
								}
								break;
						}

						//Check if url set
						if ( ! isset ( $redirect_to ) OR strlen( trim( $redirect_to ) ) == 0 ) {
							$redirect_to = home_url();
						}

						// New User (Registration)
						if ( $new_registration === true ) {
							// Apply the WordPress filters
							if ( empty ( $settings['plugin_protect_registration_redirect_url'] ) ) {
								$redirect_to = apply_filters( 'registration_redirect', $redirect_to );
							}

							// Apply our filters
							$redirect_to = apply_filters( 'oa_social_login_filter_registration_redirect_url', $redirect_to, $user_data );
						} // Existing User (Login)
						else {
							// Apply the WordPress filters
							if ( empty ( $settings['plugin_protect_login_redirect_url'] ) ) {
								$redirect_to = apply_filters( 'login_redirect', $redirect_to, ( ! empty ( $_GET ['redirect_to'] ) ? $_GET ['redirect_to'] : '' ), $user_data );
							}

							// Apply our filters
							$redirect_to = apply_filters( 'oa_social_login_filter_login_redirect_url', $redirect_to, $user_data );
						}

						//Hooks for other plugins
						do_action( 'oa_social_login_action_before_user_redirect', $user_data, $identity, $redirect_to );

						//Use safe redirection
						if ( $redirect_to_safe === true ) {
							wp_safe_redirect( $redirect_to );
						} else {
							wp_redirect( $redirect_to );
						}
						exit ();
					}
				}
			}
		}
	}
}
