<?php
$offer = get_post( $offer_id );

if ( isset( $_POST['frontend'] ) ) {
	if ( empty( $_POST['post_title'] ) ) {
		echo sprintf( '<div>%s</div>', __( 'Please fill Offer title' ) );
	}

	if ( empty( $_POST['post_content'] ) ) {
		echo sprintf( '<div>%s</div>', __( 'Please fill Offer text' ) );
	}

	if ( ! isset( $_FILES['image'] ) || $_FILES['image']['error'] == UPLOAD_ERR_NO_FILE ) {
		echo sprintf( '<div>%s</div>', __( 'Please upload Offer image' ) );
	}
}
?>
<form method='post' action="" enctype="multipart/form-data">
    <div class="wpr_edit_offer">
        <input type='hidden' name='frontend' value="true"/>
        <input type='hidden' name='ID' value="<?= $offer_id ?>"/>
		<?php wp_nonce_field( 'wpr_offer_edit', 'wpr_offer_nonce' ); ?>

        <div class="wpr_edit_offer">
            <label for="inputTitle"><?php _e( 'Offer title' ); ?> <span>*</span></label>
            <div>
                <input type="text" id="inputTitle" name='post_title' placeholder="Title" value="<?= $offer_id != '' ? $offer->post_title : $_POST['post_title']; ?>"/>
            </div>
        </div>
        <div class="wpr_edit_offer">
            <label for="inputContent"><?php _e( 'Offer text' ); ?> <span>*</span></label>
            <div>
				<?php wp_editor( $offer_id != '' ? htmlspecialchars_decode( $offer->post_content ) : $_POST['post_content'], 'post_content', array(
					'media_buttons' => false,
					'quicktags'     => false,
				) ); ?>
            </div>
        </div>
        <div class="wpr_edit_offer">
            <label for="inputImages"><?php _e( 'Offer image' ); ?> <span>*</span></label>
            <div>
				<?php
				$attachments = get_the_post_thumbnail( $offer_id, 'thumbnail', array( 'class' => 'alignleft' ) );
				if ( $attachments ) {
					echo $attachments;
				} else {
					echo 'No image';
				}
				?>
                <label class="custom-file-upload">
                    <input type="file" class="wpr-inputfile" name="image"/> <span class="wpr_input_file"><i class="fa fa-cloud-upload"></i> <?php _e( 'Choose file' ); ?></span>
                </label>
            </div>
        </div>
    </div>
    <div class="wpr_edit_offer">
        <button type="submit" class="datatable_buttons wpr_save_offer"><?php _e( 'Save changes' ); ?></button>
    </div>
</form>