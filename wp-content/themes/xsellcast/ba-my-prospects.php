<?php
/**
 * Template name: BA prospects
 *
 * @package Novus
 */

get_header(); ?>
<div id="wpr-custom-template" class="wrap">
	<?php
	$user = wp_get_current_user();
	if ( is_user_logged_in() && ( in_array( 'business_associate', (array) $user->roles ) || current_user_can( 'administrator' ) ) ) {
		?>

        <header class="page-header">
            <h1><?php the_title(); ?></h1>
        </header>

		<?php
	}
	?>

    <div id="primary" class="content-area">
        <main id="main" class="site-main" role="main">

			<?php
			$user = wp_get_current_user();
			if ( is_user_logged_in() && ( in_array( 'business_associate', (array) $user->roles ) || current_user_can( 'administrator' ) ) ) {
				?>
                <table id="wprlistingTable" class="table dataTable display" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th><?php esc_attr_e( 'First Name', 'wpr-table' ); ?></th>
                        <th><?php esc_attr_e( 'Last Name', 'wpr-table' ); ?></th>
                        <th><?php esc_attr_e( 'Email', 'wpr-table' ); ?></th>
                        <th><?php esc_attr_e( 'Photo', 'wpr-table' ); ?></th>
                        <th><?php esc_attr_e( 'Phone', 'wpr-table' ); ?></th>
                        <!--                        <th>--><?php //_e( 'URL', 'wpr-table' ); ?><!--</th>-->
                        <th><?php _e( 'Info', 'wpr-table' ); ?></th>
                    </tr>
                    </thead>
                    <tfoot>
                    <tr>
                        <th><?php esc_attr_e( 'First Name', 'wpr-table' ); ?></th>
                        <th><?php esc_attr_e( 'Last Name', 'wpr-table' ); ?></th>
                        <th><?php esc_attr_e( 'Email', 'wpr-table' ); ?></th>
                        <th><?php esc_attr_e( 'Photo', 'wpr-table' ); ?></th>
                        <th><?php esc_attr_e( 'Phone', 'wpr-table' ); ?></th>
                        <!--                        <th>--><?php //_e( 'URL', 'wpr-table' ); ?><!--</th>-->
                        <th><?php esc_attr_e( 'Info', 'wpr-table' ); ?></th>
                    </tr>
                    </tfoot>
					<?php
					$user_id = get_current_user_id();
					if ( ! $user_id ) {
						return;
					}
					$ba_brand    = get_user_meta( $user_id, 'wpr_brand_association', true );
					$ba_dealer   = get_user_meta( $user_id, 'wpr_dealer_association', true );
					$details_url = get_permalink( get_page_by_path( 'prospect-info' ) );
					$prospects   = array();
					if ( $ba_brand ) {
						switch_to_blog( 1 );
						$args  = array(
							'role__in' => array( 'wpr_prospect' ),
							'meta_key' => 'wpr_my_offers_clicked',
						);
						$users = get_users( $args );

						if ( ! empty( $users ) ) {
							echo '<tbody>';
							foreach ( $users as $user ) {
								$user_favs = get_user_meta( $user->ID, 'wpr_my_offers_clicked', true );
								$show_user = false;
								if ( ! empty( $user_favs ) ) {
									foreach ( $user_favs as $key => $val ) {
										if ( in_array( $user_id, array_column( $val, 'abc_id' ) ) && in_array( $ba_dealer, array_column( $val, 'dealer_id' ) ) ) {
											$show_user = true;
										}
									}
								}

								if ( $show_user ) {
									$user_avatar         = get_avatar( $user->ID, 32 );

									$user_phone          = get_user_meta( $user->ID, 'phone', true );
									$prospector_info_url = $details_url . "?prospector=$user->ID";
									echo '<tr>';
									_e( sprintf( '<td>%s</td>', esc_attr( $user->first_name ) ) );
									_e( sprintf( '<td>%s</td>', esc_attr( $user->last_name ) ) );
									_e( sprintf( '<td>%s</td>', esc_attr( $user->user_email ) ) );
									_e( sprintf( '<td>%s</td>', $user_avatar ) );
									_e( sprintf( '<td>%s</td>', esc_attr( $user_phone ) ) );
									//_e( sprintf( '<td>%s</td>', '' ) );
									_e( sprintf( '<td><a href="%s" target="_blank" class="datatable_buttons info_button">%s</a></td>', esc_url( $prospector_info_url ), __( 'Info', 'wpr-table' ) ) );
									echo '</tr>';
								}
							}
							echo '</tbody>';
						}
						restore_current_blog();
					}
					?>
                </table>
			<?php } else {
				$login_url    = get_permalink( get_page_by_path( 'login' ) );
				$register_url = get_permalink( get_page_by_path( 'luxurybuystoday/register' ) );
				?>
                <h1 class="homePageTitle"><?php echo __( 'Welcome to Luxury Buys Today' ); ?></h1>
                <h3 class="homePageSubTitle"><?php echo __( 'Please login or register to continue' ); ?></h3>
                <div class="row">
                    <div class="box50">
                        <div class="row boxContent white">
                            <div class="boxContent30">
                                <img class="homePageIcon" src="<?php echo get_stylesheet_directory_uri() . '/assets/images/login.png'; ?>"
                                     srcset="<?php echo get_stylesheet_directory_uri() . '/assets/images/login_2x.png'; ?>">
                            </div>
                            <div class="boxContent70">
                                <p><?php echo __( 'Already have an account?' ); ?></p>
								<?php

								echo sprintf( ' <a href="%s" class="wpr-url"><p>Login</p></a>', $login_url );
								?>
                            </div>
                        </div>
                    </div>
<!--
                    <div class="box50">
                        <div class="row boxContent yellow">
                            <div class="boxContent30">
                                <img class="homePageIcon" src="<?php echo get_stylesheet_directory_uri() . '/assets/images/note.png'; ?>"
                                     srcset="<?php echo get_stylesheet_directory_uri() . '/assets/images/note_2x.png'; ?>">
                            </div>
                            <div class="boxContent70">
                                <p><?php echo __( 'Don\'t have an account? ' ); ?></p>
								<?php

								echo sprintf( ' <a href="%s" class="wpr-url"><p>Register</p></a>', $register_url );
								?>
                            </div>
                        </div>
                    </div>
-->
                </div>
				<?php
			} ?>

        </main><!-- #main -->
    </div><!-- #primary -->
</div><!-- .wrap -->

<?php get_footer(); ?>
