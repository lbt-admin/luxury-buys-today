<?php
/**
 * Template name: BA prospector info
 *
 * @package Novus
 */

get_header(); ?>
<div id="wpr-custom-template" class="wrap wpr-ba-prospects">
    <header class="page-header">
        <h1><?php single_post_title(); ?></h1>
    </header>

    <div id="primary" class="content-area">
        <main id="main" class="site-main" role="main">

			<?php
			$user    = wp_get_current_user();
			$user_id = $user->ID;
			if ( is_user_logged_in() && ( in_array( 'business_associate', (array) $user->roles ) || current_user_can( 'administrator' ) ) ) {
				switch_to_blog( 1 );
				$prospector_id = absint( $_GET['prospector'] );
				if ( $prospector_id ) {
					$prospector = get_user_by( 'id', $prospector_id );

					if ( $prospector && $user_id && in_array( 'wpr_prospect', $prospector->roles ) ) {

						$ba_brand  = get_user_meta( $user_id, 'wpr_brand_association', true );
						$ba_dealer = get_user_meta( $user_id, 'wpr_dealer_association', true );
						$user_favs = get_user_meta( $prospector->ID, 'wpr_my_offers_clicked', true );

						if ( ! empty( $user_favs ) ) {
							$show_user = false;
							if ( ! empty( $user_favs ) ) {
								foreach ( $user_favs as $key => $val ) {
									if ( in_array( $user_id, array_column( $val, 'abc_id' ) ) && in_array( $ba_dealer, array_column( $val, 'dealer_id' ) ) ) {
										$show_user = true;
									}
								}
							}

							if ( $show_user ) {
								$user_avatar = get_avatar( $prospector->ID, 32 );
								$user_phone  = get_user_meta( $prospector->ID, 'phone', true );
								?>
                                <table id="prospectorInfio" class="table dataTable display" cellspacing="0" width="100%">
                                    <thead>
                                    <tr>
                                        <th><?php esc_attr_e( 'First Name', 'wpr-table' ); ?></th>
                                        <th><?php esc_attr_e( 'Last Name', 'wpr-table' ); ?></th>
                                        <th><?php esc_attr_e( 'Email', 'wpr-table' ); ?></th>
                                        <th><?php esc_attr_e( 'Photo', 'wpr-table' ); ?></th>
                                        <th><?php esc_attr_e( 'Phone', 'wpr-table' ); ?></th>
                                        <th><?php esc_attr_e( 'Intelligence', 'wpr-table' ); ?></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
										<?php
										_e( sprintf( '<td>%s</td>', esc_attr( $prospector->first_name ) ) );
										_e( sprintf( '<td>%s</td>', esc_attr( $prospector->last_name ) ) );
										_e( sprintf( '<td>%s</td>', esc_attr( $prospector->user_email ) ) );
										_e( sprintf( '<td>%s</td>', $user_avatar ) );
										_e( sprintf( '<td>%s</td>', esc_attr( $user_phone ) ) );
										_e( sprintf( '<td><a href="#" id="see_intelligence_button" class="datatable_buttons see_intelligence_button" data-fetch="%s">%s</a></td>', esc_attr( $prospector->user_email ), __('Get Data') ) );
										?>
                                    </tr>
                                    </tbody>
                                </table>

                                <div id="displayProspectorLookbook">
                                    <div class="page-header">
                                        <h3><?php esc_attr_e( 'Prospect Lookbook', 'wpr-table' ); ?></h3>
                                    </div>

                                    <table id="wprlistingTableLook" class="table dataTable display" cellspacing="0" width="100%">
                                        <thead>
                                        <tr>
                                            <th><?php esc_attr_e( 'Offer name', 'wpr-table' ); ?></th>
                                            <th><?php esc_attr_e( 'Offer date', 'wpr-table' ); ?></th>
                                            <th><?php esc_attr_e( 'Link to offer', 'wpr-table' ); ?></th>
                                        </tr>
                                        </thead>
										<?php
										echo '<tbody>';
										$args = array(
											'post__in' => array_keys( $user_favs ),
										);

										$query = new WP_Query( $args );
										$posts = $query->posts;

										if ( $posts ) {
											foreach ( $posts as $lookbooks ) {
												$date_added = strtotime( $lookbooks->post_date );
												echo '<tr>';
												_e( sprintf( '<td>%s</td>', esc_attr( $lookbooks->post_title ) ) );
												_e( sprintf( '<td>%s</td>', esc_attr( date( 'm/d/Y H:i:s', $date_added ) ) ) );
												_e( sprintf( '<td><a href="%s" target="_blank" class="datatable_buttons see_offer_button">%s</a></td>', esc_url( get_permalink( $lookbooks->ID ) ), __( 'See offer', 'wpr-table' ) ) );
												echo '</tr>';
											}
										}
										wp_reset_postdata();
										echo '</tbody>';
										?>
                                    </table>
                                </div>
							<?php }
						} ?>
                        <div id="displayProspectorLookbook">
                            <div class="page-header">
                                <h3><?php esc_attr_e( 'Prospect\'s Interest', 'wpr-table' ); ?></h3>
                            </div>

                            <table id="wprlistingTableLook" class="table dataTable display" cellspacing="0" width="100%">
                                <thead>
                                <tr>
                                    <th><?php esc_attr_e( 'Date/time', 'wpr-table' ); ?></th>
                                    <th><?php esc_attr_e( 'Offer title', 'wpr-table' ); ?></th>
                                    <th><?php esc_attr_e( 'Interested in', 'wpr-table' ); ?></th>
                                    <th><?php esc_attr_e( 'Link to offer', 'wpr-table' ); ?></th>
                                </tr>
                                </thead>
								<?php
								echo '<tbody>';
								global $wpdb;

								$interest = $wpdb->get_results(
									$wpdb->prepare(
										"
                                            SELECT 
                                                *
                                            FROM
                                                {$wpdb->prefix}wpr_cta_buttons
                                            WHERE
                                                user_id = %d
                                            AND
                                                abc_id = %d
                                            ORDER BY 
                                                date
                                            DESC
								         ",
										absint( $prospector_id ),
										absint( $user_id )
									)
								);

								if ( $interest ) {
									foreach ( $interest as $lookbooks ) {
										$date_added = strtotime( $lookbooks->date );

										switch_to_blog( 1 );
										$author_id      = get_post_field( 'post_author', $lookbooks->post_id );
										$post_permalink = get_permalink( $lookbooks->post_id );
										if ( $author_id == $user_id ) {
											$post_permalink .= wpr_ba_unique_token();
										}
										restore_current_blog();

										echo '<tr>';
										_e( sprintf( '<td>%s</td>', esc_attr( date( 'm/d/Y', $date_added ) ) ) );
										_e( sprintf( '<td>%s</td>', esc_attr( get_the_title( $lookbooks->post_id ) ) ) );
										_e( sprintf( '<td>%s</td>', esc_attr( ucfirst( $lookbooks->btn_string ) ) ) );
										_e( sprintf( '<td><a href="%s" target="_blank" class="datatable_buttons see_offer_button">%s</a></td>', esc_url( $post_permalink ), __( 'See offer', 'wpr-table' ) ) );
										echo '</tr>';
									}
								}
								echo '</tbody>';
								?>
                            </table>
                        </div>
						<?php
					} else {
						esc_attr_e( 'You can\'t access this user.', 'wpr-table' );
					}
				}
				restore_current_blog();
			} else {
				$login_url    = get_permalink( get_page_by_path( 'login' ) );
				$register_url = get_permalink( get_page_by_path( 'luxurybuystoday/register' ) );
				echo sprintf( 'Please <a href="%s" class="wpr-url">%s</a> or <a href="%s" class="wpr-url">%s</a> to access this page', $login_url, __( 'Login' ), $register_url, __( 'Register' ) );
			} ?>

        </main><!-- #main -->
    </div><!-- #primary -->
</div><!-- .wrap -->

<?php get_footer(); ?>
