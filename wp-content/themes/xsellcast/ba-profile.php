<?php
/**
 * Template name: Edit Profile
 *
 * @package Novus
 */

get_header(); ?>
<div class="wrap">

    <div id="primary" class="content-area">
        <main id="main" class="site-main" role="main">

			<?php if ( is_user_logged_in() ) : ?>
				<?php wpfep_show_profile(); ?>
                <br><br>
			<?php else : ?>

                <article class="text-center">
                    <div class="entry-content">
						<?php
						$login_url    = get_permalink( get_page_by_path( 'login' ) );
						$register_url = get_permalink( get_page_by_path( 'luxurybuystoday/register' ) );
						echo sprintf( 'Please <a href="%s" class="wpr-url">%s</a> or <a href="%s" class="wpr-url">%s</a> to access this page', $login_url, __( 'Login' ), $register_url, __( 'Register' ) );
						?>
                    </div>
                </article>

			<?php endif; ?>

        </main><!-- #main -->
    </div><!-- #primary -->
</div><!-- .wrap -->

<?php get_footer(); ?>
