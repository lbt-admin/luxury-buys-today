<?php
/**
 * Template name: BA Register
 *
 * @package Novus
 */

get_header(); ?>
	<div class="wrap">
		<header class="page-header">

		</header>

		<div id="primary" class="content-area">
			<main id="main" class="site-main" role="main">
                <div class="wpr-login-register-body wpr-register-page">
                    <h1 class="homePageTitle"><?php echo __( 'Welcome to Luxury Buys Today' ); ?></h1>
                    <h2 class="gsection_title home-page-subtitle"><?php echo __( 'Profesional Profile and Sign-In' ); ?></h2>
                    <div class="wpr-login-register-form">
                        <div class="wpr-login-register-social"><?php echo do_shortcode( '[oa_social_login]' ); ?></div>
                        <div class="wpr-login-register-or"><?php echo __( 'Or' ); ?></div>
	                    <?php echo do_shortcode( '[gravityform id="2" title="false" description="false"]' ); ?>
                    </div>
                    <div class="wpr-login-register-body-footer">
                        <div class="wpr-account">
							<?php
							$login_url = get_permalink( get_page_by_path( 'login' ) );
							_e( sprintf( 'Already have an account? <a href="%s">Sign in</a>', esc_url( $login_url ) ) );
							?>
                        </div>
                    </div>
                </div>
			</main><!-- #main -->
		</div><!-- #primary -->
	</div><!-- .wrap -->

<?php get_footer(); ?>