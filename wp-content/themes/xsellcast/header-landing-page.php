<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
    <header id="finish-registration" class="site-header" role="banner">

        <div class="wrap" id="wpr-header-finish-registration">

            <div class="LBT_branding">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/LBT-logo-black.png"/>
            </div>

            <div class="wpr_header_right">
                <h1><?php the_title(); ?></h1>
            </div>

        </div>

    </header><!-- #masthead -->

    <div class="site-content-contain">
        <div id="content" class="site-content">
