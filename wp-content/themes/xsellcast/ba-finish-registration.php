<?php
/**
 * Template name: BA finish registration
 */

get_header( 'landing-page' );
?>
    <div class="wpr-finish-registration-content">
        <div class="wrap">
			<?php echo do_shortcode( '[wpr_abc_lc_interact]' ); ?>
			<?php
			if ( is_user_logged_in() ) {
				if ( have_posts() ) :
					while ( have_posts() ) : the_post();
						the_content();
					endwhile;
				endif;
			}
			?>
        </div>
    </div>
    <div class="wrap">
        <header class="page-header">

        </header>

        <div id="primary" class="content-area">
            <main id="main" class="site-main" role="main">
                <div class="wpr-login-register-body wpr-register-page">
                    <h1 class="homePageTitle"><?php echo __( 'Welcome to Luxury Buys Today' ); ?></h1>
                    <p class="wpr_h1_under"><?php esc_html_e( 'As an Authorized Brand Consultant register for more referrals', 'xsellcast' ); ?></p>
                    <h2 class="gsection_title home-page-subtitle"><?php echo __( 'Profesional Profile and Sign-In' ); ?></h2>
                    <div class="wpr-login-register-form">
						<?php if ( is_user_logged_in() ) {
							$terms_url = get_the_permalink( get_page_by_path( 'terms-and-conditions' ) );
							?>
                            <div class="wpr-login-register-social wpr_register_social">
                                <div class="wpr_social_login_left">
                                    <form id="wpr_register_social">
                                        <div>
                                            <label for="wpr_register_accept"><?php esc_html_e( 'Terms and conditions *', 'xsellcast' ); ?></label>
                                            <input type="checkbox" name="wpr_register_accept"
                                                   id="wpr_register_accept"/> <?php echo sprintf( '%s <a href="%s" target="_blank">%s</a>', __( 'I have read and accept the' ), esc_url( $terms_url ), __( 'terms and conditions' ) ); ?>
                                        </div>

                                    </form>
                                </div>
                                <div class="wpr_social_login_right">
									<?php
									if ( shortcode_exists( 'oa_social_login_wpr' ) ) {
										echo do_shortcode( '[oa_social_login_wpr]' );
									}
									?>
                                </div>
                            </div>
                            <div class="wpr-login-register-or"><?php echo __( 'Or' ); ?></div>

							<?php echo do_shortcode( '[gravityform id="9" title="false" description="false"]' ); ?>
						<?php } else {
							$login_url = get_the_permalink( get_page_by_path( 'login' ) );
							echo sprintf( 'Please <a href="%s" class="wpr-url">%s</a> in order to access this page', $login_url, __( 'Login' ) );
						} ?>
                    </div>
                </div>
            </main><!-- #main -->
        </div><!-- #primary -->
    </div><!-- .wrap -->

<?php get_footer(); ?>