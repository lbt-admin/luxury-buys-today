<?php
/**
 * Template name: BA Brand&Industry
 *
 * @package Novus
 */

get_header(); ?>
<div class="wrap" id="wpr-custom-template">
    <header class="page-header">
        <h1><?php echo __( 'Finish your registration' ); ?></h1>
    </header>
    <div id="primary" class="content-area">
        <main id="main" class="site-main" role="main">
			<?php if ( is_user_logged_in() ) {
				$ba_id = get_current_user_id();
				if ( wpr_is_profile_not_complete( $ba_id ) ) {
					$avatar = get_user_meta( $ba_id, 'wpr_user_avatar', true );
					if ( ! $avatar ) {
						$avatar = get_avatar( $ba_id, 32 );
					}

					if ( preg_match( "/gravatar.com/", $avatar ) ) {
						echo sprintf( '<span class="wpr-add-avatar">%s</span>', __( 'Please upload your avatar!' ) );
					}
					?>
					<?php
					while ( have_posts() ) : the_post();

						the_content();

					endwhile;
					?>
				<?php }
			} else {
				echo __( 'This content is restricted!' );
			} ?>
        </main><!-- #main -->
    </div><!-- #primary -->
</div><!-- .wrap -->

<?php get_footer(); ?>
