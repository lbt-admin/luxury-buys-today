<?php
/**
 * The front page template file
 *
 * If the user has selected a static page for their homepage, this is what will
 * appear.
 * Learn more: https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>
    <div id="wpr-custom-template" class="wrap">
			<?php if ( is_user_logged_in() ) { ?>
        <header class="page-header wpr-my-offers-header">
                <h1><?php the_title(); ?></h1>
                <div class="wpr-go-to-lbt">
                    <a href="<?php if ( function_exists( 'wpr_user_category_url' ) ) {
		                echo wpr_user_category_url( get_current_user_id() );
	                } ?>" class="datatable_buttons" target="_blank">
		                <?php _e( 'Go to my Homepage', 'wpr-table' ); ?>
                    </a>
                </div>
                <div class="wpr-add-new-offer">
                    <a href="<?php echo esc_url( get_permalink( get_the_ID() ) ); ?>?add_offer" class="datatable_buttons">
						<?php _e( 'Add a new Offer', 'wpr-table' ); ?>
                    </a>
                </div>
        </header>
			<?php } ?>


        <div id="primary" class="content-area">
            <main id="main" class="site-main" role="main">

				<?php
				$user = wp_get_current_user();
				if ( is_user_logged_in() && ( in_array( 'business_associate', (array) $user->roles ) || current_user_can( 'administrator' ) ) ) {
					$user_id        = $user->ID;
					$my_offers_page = get_the_ID();

					if ( isset( $_GET['add_offer'] ) ) {
						//echo do_shortcode( '[gravityform id="3" title="false" description="false"]' );
						get_template_part( 'partials/content', 'edit-offer' );
					} elseif ( isset( $_GET['edit_offer'] ) ) {
						switch_to_blog( 1 );
						$offer_id = absint( $_GET['gform_post_id'] );
						$post     = get_post( $offer_id );

						if ( $post->post_author == $user_id ) {
							set_query_var( 'offer_id', absint( $offer_id ) );
							get_template_part( 'partials/content', 'edit-offer' );
						} else {
							_e( 'You don\'t have access to this offer!', 'wpr-table' );
						}
						restore_current_blog();
					} elseif ( isset( $_GET['delete_offer'] ) ) {
						$offer_id = absint( $_GET['delete_offer'] );
						switch_to_blog( 1 );
						$post = get_post( $offer_id );
						restore_current_blog();
						if ( $post->post_author == $user_id ) {
							wpr_ba_delete_offer( $offer_id, $my_offers_page );
						} else {
							_e( 'You don\'t have access to this offer!', 'wpr-table' );
						}
					} else { ?>
                        <div id="displayProspectorLookbook">
                            <table id="wprlistingTable" class="table dataTable display" cellspacing="0" width="100%">
                                <thead>
                                <tr>
                                    <th><?php _e( 'Offer title', 'wpr-table' ); ?></th>
                                    <th><?php _e( 'Offer image', 'wpr-table' ); ?></th>
                                    <th><?php _e( 'Date added', 'wpr-table' ); ?></th>
                                    <th><?php _e( '', 'wpr-table' ); ?></th>
                                </tr>
                                </thead>

								<?php
								echo '<tbody>';
								switch_to_blog( 1 );
								$args  = array(
									'post_type' => 'post',
									'author'    => $user_id,
								);
								$query = new WP_Query( $args );
								$posts = $query->posts;

								if ( $posts ) {
									foreach ( $posts as $post ) {
										$post_image   = get_the_post_thumbnail(
											$post->ID, 'offer_thumb', array(
												'class' => 'alignleft',
											)
										);
										$offer_url    = esc_url( get_the_permalink( $post->ID ) ) . wpr_ba_unique_token();
										$offer_edit   = esc_url( get_permalink( $my_offers_page ) ) . '?edit_offer&gform_post_id=' . $post->ID;
										$offer_delete = esc_url( get_permalink( $my_offers_page ) ) . '?delete_offer=' . $post->ID;

										echo '<tr>';
											_e( sprintf( '<td><a href="%s" target="_blank">%s</a></td>', $offer_url, $post->post_title ) );
											_e( sprintf( '<td><a href="%s" target="_blank">%s</a></td>', $offer_url, $post_image ) );
											_e( sprintf( '<td><a href="%s" target="_blank">%s</a></td>', $offer_url, $post->post_date ) );
											_e( sprintf( '<td style="text-align: right"><a href="%s" target="_blank" class="datatable_buttons edit_button">%s</a><a href="%s" class="datatable_buttons delete_button" onclick="return confirm(\'%s\');">%s</a></td>', $offer_edit, __( '<i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit' ), $offer_delete, __( 'Are you sure you want to delete this item?' ), __( '<i class="fa fa-trash" aria-hidden="true"></i> Delete' ) ) );
										echo '</tr>';
									}
								}
								wp_reset_postdata();
								restore_current_blog();
								echo '</tbody>';
								?>
                            </table>
                        </div>
					<?php }
				} else {
					$login_url    = get_permalink( get_page_by_path( 'login' ) );
					$register_url = get_permalink( get_page_by_path( 'luxurybuystoday/register' ) );
					?>
                    <h1 class="homePageTitle"><?php echo __( 'Welcome to Luxury Buys Today' ); ?></h1>
                    <h3 class="homePageSubTitle"><?php echo __( 'Please login or register to continue' ); ?></h3>
                    <div class="row">
                        <div class="box50">
                            <div class="row boxContent white">
                                <div class="boxContent30">
                                    <img class="homePageIcon" src="<?php echo get_stylesheet_directory_uri() . '/assets/images/login.png'; ?>"
                                         srcset="<?php echo get_stylesheet_directory_uri() . '/assets/images/login_2x.png'; ?>">
                                </div>
                                <div class="boxContent70">
                                    <p><?php echo __( 'Already have an account?' ); ?></p>
									<?php

									echo sprintf( ' <a href="%s" class="wpr-url"><p>Login</p></a>', $login_url );
									?>
                                </div>
                            </div>
                        </div>
<!--
                        <div class="box50">
                            <div class="row boxContent yellow">
                                <div class="boxContent30">
                                    <img class="homePageIcon" src="<?php echo get_stylesheet_directory_uri() . '/assets/images/note.png'; ?>"
                                         srcset="<?php echo get_stylesheet_directory_uri() . '/assets/images/note_2x.png'; ?>">
                                </div>
                                <div class="boxContent70">
                                    <p><?php echo __( 'Don\'t have an account? ' ); ?></p>
									<?php

									echo sprintf( ' <a href="%s" class="wpr-url"><p>Register</p></a>', $register_url );
									?>
                                </div>
                            </div>
                        </div>
-->
                    </div>
					<?php
				} ?>

            </main><!-- #main -->
        </div><!-- #primary -->
    </div><!-- .wrap -->
<?php get_footer();
