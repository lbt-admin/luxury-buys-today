<?php
/**
 * Template name: BA Login
 *
 * @package Novus
 */

get_header(); ?>
<div class="wrap">
    <header class="page-header wpr-login-register-head">
        <h2><?php echo __( 'Welcome to Luxury Buys Today' ); ?></h2>
    </header>

    <div id="primary" class="content-area">
        <main id="main" class="site-main" role="main">
            <div class="wpr-login-register-body">
                <div class="wpr-login-register-form">
					<?php echo do_shortcode( '[gravityform id="1" title="false" description="false"]' ); ?>
                </div>
                <div class="wpr-login-register-body-footer">
                    <div class="wpr-login-register-or"><?php echo __( 'Or' ); ?></div>
                    <div class="wpr-login-register-social">
						<?php echo do_shortcode( '[oa_social_login]' ); ?>
                    </div>
<!--
                    <div class="wpr-account">
						<?php
						$register_url = get_permalink( get_page_by_path( 'luxurybuystoday/register' ) );
						_e( sprintf( 'Don\'t have an account? <a href="%s">Sign up</a>', esc_url( $register_url ) ) );
						?>
                    </div>
-->
                </div>
            </div>
        </main><!-- #main -->
    </div><!-- #primary -->
</div><!-- .wrap -->

<?php get_footer(); ?>
