<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<?php
$prospect_class = '';
if(!is_user_logged_in()){
    $prospect_class = "yachtBackground";
}
?>

<body <?php body_class($prospect_class); ?>>
<div id="page" class="site">
    <a class="skip-link screen-reader-text" href="#content"><?php _e( 'Skip to content', 'twentyseventeen' ); ?></a>

    <header id="masthead" class="site-header" role="banner">

        <div class="wrap">
			<?php #get_template_part( 'template-parts/header/header', 'image' ); ?>

			<?php
//			if ( is_user_logged_in() ) {
//				$user_id = get_current_user_id();
//				$csr_id  = get_user_meta( $user_id, 'wpr_csr_association', true );
//
//				if ( $csr_id ) {
//					$csr_phone = get_user_meta( $csr_id, 'phone', true );
//					if ( $csr_phone ) {
//						echo '<a href="tel: ' . esc_attr( $csr_phone ) . '">';
//						echo '<div class="wpr-csr-info">';
//
//						$user_avatar = get_avatar( $csr_id );
//						if ( $user_avatar ) {
//							echo sprintf( '<div class="wpr-csr-avatar">%s</div>', $user_avatar );
//						}
//
//						echo '<div class="wpr-csr-phone">';
//						echo sprintf( '<b>%s</b><br />', __( 'Need assistance?' ) );
//						echo sprintf( '<span>%s %s</span>', __( 'Call:' ), esc_attr( $csr_phone ) );
//						echo '</div>';
//						echo '</div>';
//						echo '</a>';
//					}
//				}
//			}
			?>
            <div class="LBT_branding">
            <a href="https://luxurybuystoday.com">
                <img src="<?php echo get_site_url(); ?>/wp-content/themes/xsellcast/assets/images/LBT-logo.png"/>
            </a>
            </div>
			<?php if ( has_nav_menu( 'top' ) ) : ?>
                <div class="navigation-top">
                    <div class="wrap">
						<?php get_template_part( 'template-parts/navigation/navigation', 'top' ); ?>
                    </div><!-- .wrap -->
                </div><!-- .navigation-top -->
			<?php endif; ?>
        </div>

    </header><!-- #masthead -->

	<?php
	// If a regular post or page, and not the front page, show the featured image.
	if ( has_post_thumbnail() && ( is_single() || ( is_page() && ! twentyseventeen_is_frontpage() ) ) ) :
		echo '<div class="single-featured-image-header">';
		the_post_thumbnail( 'twentyseventeen-featured-image' );
		echo '</div><!-- .single-featured-image-header -->';
	endif;
	?>

	<?php

	if(is_user_logged_in()){
		?>
        <style>
        .navigation-top .current-menu-item > a,
        .navigation-top .current_page_item > a {
        background-color: #ba9916;
        color: #FFF;
        }
        </style>
		<?php
	}

	?>

    <div class="site-content-contain">
        <div id="content" class="site-content">
