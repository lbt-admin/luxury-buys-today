<?php
/**
 * Archive hero block template
 *
 * @package Novus
 */

$header_background = false;
$header_title      = false;

if ( is_category() ) {
	$current_object    = get_queried_object();
	$current_object_id = $current_object->term_id;
	$header_title      = get_field( 'archive_page_header_title', 'category_' . $current_object_id );
	$header_background = get_field( 'archive_page_header_image', 'category_' . $current_object_id );
}

?>
<?php if ( $header_title && $header_background ) : ?>
<div class="page_header">
	<div class="header_background" <?php echo $header_background ? 'style="background-image: url(' . esc_url( $header_background ) . ');"' : ''; ?>>
		<div class="header_container">
			<h2 class="page_title">
				<?php echo esc_html( $header_title ); ?>
			</h2>
		</div>
	</div>
</div>
<?php endif; ?>
