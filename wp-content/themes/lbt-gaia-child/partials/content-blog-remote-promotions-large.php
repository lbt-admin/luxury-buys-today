<?php
/**
 * Blog Post Remote Promotions template Large
 *
 * @package LBT
 */

$remote_promotion = $args['remote_promotion'] ?? array();

if ( empty( $remote_promotion ) ) {
	return;
}

?>
<article id="post-<?php echo esc_attr( $remote_promotion['ID'] ); ?>" class="blog-content post type-post status-publish format-standard masonry-brick">
	<div class="entry-wrap">
		<header class="entry-header" data-deal-id="<?php echo esc_attr( $remote_promotion['ID'] ); ?>">
		</header>
		<!-- .entry-header -->
		<h1 class="entry-title">
			<a href="<?php echo esc_attr( $remote_promotion['target_href'] ); ?>?id=<?php echo esc_attr( $remote_promotion['ID'] ); ?>" rel="bookmark">
				<?php echo esc_html( $remote_promotion['listing_item_name'] ); ?>
			</a>
		</h1>
		<?php if ( ! empty( $remote_promotion['listing_image_url'] ) ) { ?>
			<div class="content-post-image">
				<a href="<?php echo esc_url( $remote_promotion['target_href'] . '?id=' . $remote_promotion['ID'] ); ?>">
					<div class="entry-image">
						<div class="promotions-flag">Premiere<br>Promotion</div>
						<img src="<?php echo esc_url( $remote_promotion['listing_image_url'] ); ?>" data-test="1" onerror="this.src='<?php echo esc_url( $remote_promotion['listing_image_url'] ); ?>'; if ( this.getAttribute('retry') ) { if( this.getAttribute('retry') == 1 ) { this.src='<?php echo esc_url( $remote_promotion['listing_image_url'] ); ?>'; this.setAttribute( 'retry', 2 ); } else { this.src='<?php echo esc_url( LBT_THEME_URL ); ?>assets/images/post-thumb-cropped.svg'; } } else { this.setAttribute( 'retry', 1 ); }"/>
					</div>
				</a>
			</div>
		<?php } ?>
		<footer class="entry-footer">
			<div class="test-tset">
				<div class="entry-meta wpr-entry-meta-archive">
					<span class="cat-links">
						<strong><?php echo esc_html( $remote_promotion['category']->name ); ?> promotions</strong>
					</span>
				</div>
				<div class="entry-summary wpr-article-source-archive">
					<a href="<?php echo esc_url( $remote_promotion['branch_location'] ); ?>" rel="dofollow" target="_blank">Source Link</a>
				</div>
			</div>
			<div class="entry-summary mix">
					<strong class="post-date">
						<span class="posted-on">
							<time class="entry-date published updated" datetime="2020-11-27T14:45:33+00:00">November 27, 2020</time>
						</span>
					</strong>
					<?php
					if ( ! empty( $remote_promotion['is_dealership'] ) ) {
						$listing_zip = isset( $listing_city['listing_zip'] ) ? ' ' . $listing_city['listing_zip'] : '';

						$location = $remote_promotion['listing_city'] . ', ' . $remote_promotion['listing_state'] . $listing_zip;
						$price    = number_format( $remote_promotion['listing_price'], 0, '.', ',' );

						echo wp_kses_post( 'Location: ' . $location . '<br>' );
						echo wp_kses_post( 'Price: $' . $price . '<br>' );
					} else {
						echo wp_kses_post( lbt_the_excerpt_truncate( str_replace( 'Source link', '', wp_strip_all_tags( do_shortcode( $remote_promotion['content'] ) ) ), 30 ) );
					}
					?>
					<a href="<?php echo esc_url( $remote_promotion['target_href'] . '?id=' . $remote_promotion['ID'] ); ?>" class="read-more-link">Read more...</a>
				</div>
			<?php
			if ( ! \LBT\Category::is_broyhill_category( $category->term_id ) ) :
				?>
				<div class="archieve-cust-boxes">
					<?php novus_entry_footer( false ); ?>
					<?php
					if ( shortcode_exists( 'apss_share' ) ) {
						echo do_shortcode( "[apss_share theme='3' counter='1']" );
					}
					?>
				</div>
			<?php endif; ?>
		</footer>
	</div>
</article>
