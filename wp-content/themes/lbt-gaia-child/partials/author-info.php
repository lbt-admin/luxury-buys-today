<?php
/**
 * Author info
 *
 * @package Novus
 */
?>
<div class="entry-author media">

	<div class="entry-author-img media-img">
		<?php echo get_avatar( get_the_author_meta('email'), '101' ); ?>
	</div>

	<div class="entry-author-content media-bd">
		<h5 class="entry-author-name"><?php the_author_posts_link(); ?></h5>
		<?php echo wpautop( get_the_author_meta( 'description' ) ); ?>
	</div>

</div>
