<?php
/**
 * Brand filter
 *
 * @package Novus
 */

$main_categories = $args['main_categories'] ?? array();

$brands = \LBT\Category::get_main_categories_children( $main_categories );

?>
<div class="filter-container">
	<select id="brand-filter" class="regular-text" data-link="<?php echo esc_url( get_category_link( get_queried_object() ) ); ?>">
		<option value="0">Filter by Brand</option>
		<?php
		foreach ( $brands as $brand ) :
			?>
			<option value="<?php echo esc_attr( $brand->term_id ); ?>" <?php selected( isset( $_GET['brand_id'] ) && (int) $_GET['brand_id'] === $brand->term_id ); ?>>
				<?php echo esc_html( $brand->name ); ?>
			</option>
			<?php
			endforeach;
		?>
	</select>
</div>
