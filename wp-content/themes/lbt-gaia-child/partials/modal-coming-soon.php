<?php
/**
 * Modal: Coming Soon
 *
 * @package Novus
 */

?>
<div class="lbt-modal default" data-target="coming-soon-modal">
	<div class="lbt-modal__body">
		<div class="lbt-modal__close" data-action="lbt-modal-close"></div>
		<div class="row">
			<div class="column">
				<img src="https://luxurybuystoday-media.s3.amazonaws.com/media/uploads/2020/10/91e21b2177f9253059b0a967966710be2x-png.png" alt="Coming Soon">
			</div>
			<div class="column">
				<h3 class="title">Coming Soon To Your Area</h3>
				<p class="subtitle">A better way to get the latest news and offers from your favorite luxury brands.</p>
				<div class="default-content">
					<p>VIP Special Member Discounts</p>
					<p>Prized Invitations To Local Sales Events</p>
					<p>Daily Brand News, Events & Promotions</p>
					<p>Free Live Concierge Services</p>
				</div>
				<div class="cookie-content">
					<p>These features are not yet available in your area keep an eye on your email for a launch announcement.</p>
					<div class="newsletter-wrapper">
						<p>Sign up to get the latest updates</p>
						<?php echo do_shortcode( '[lbt_newsletter]' ); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
