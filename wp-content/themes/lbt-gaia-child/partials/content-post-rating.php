<div class="rating-order-container">
    <img src="<?php echo site_url(); ?>/wp-content/themes/lbt-gaia-child/assets/images/brohills.png" alt="brohills"/>
    <div class="rating-info">
        <p class="rating-order">RATING of this offer</p>
        <?php
        do_action('wpr_display_post_rating');
        ?>
    </div>
</div>
<div class="know-more">
    <span>Want to know about <a href="<?php echo home_url(); ?>/broyhill-rating-explained/">Broyhill Rating?</a></span>
    <?php //echo sprintf( '<a href="%s">%s</a>', esc_url( home_url() ) . '/broyhill-rating-explained/', __( "Broyhill rating: ", '' ) ); ?>
</div>