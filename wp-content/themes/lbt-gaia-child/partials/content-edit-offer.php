<?php
$offer = get_post( $offer_id );
?>
<form method='post' action="" enctype="multipart/form-data">
    <div class="wpr_edit_offer">
        <input type='hidden' name='frontend' value="true"/>
        <input type='hidden' name='ID' value="<?= $offer_id ?>"/>
		<?php wp_nonce_field( 'wpr_offer_edit', 'wpr_offer_nonce' ); ?>

        <div class="wpr_edit_offer">
            <label for="inputTitle"><?php _e( 'Offer title' ); ?> *</label>
            <div>
                <input type="text" id="inputTitle" name='post_title' placeholder="Title" value="<?= $offer->post_title ?>"/>
            </div>
        </div>
        <div class="wpr_edit_offer">
            <label for="inputContent"><?php _e( 'Offer text' ); ?> *</label>
            <div>
				<?php wp_editor( htmlspecialchars_decode($offer->post_content), 'post_content', array(
					'media_buttons' => false,
					'quicktags'     => false,
				) ); ?>
            </div>
        </div>
        <div class="wpr_edit_offer">
            <label for="inputImages"><?php _e( 'Offer image' ); ?> *</label>
            <div>
				<?php
				$attachments = get_the_post_thumbnail( $offer_id, 'thumbnail', array( 'class' => 'alignleft' ) );
				if ( ! $attachments ) {
					echo "No images";
				} else {
					echo $attachments;
				}
				?>
                <input type="file" name="image"/>
            </div>
        </div>
    </div>
    <div class="wpr_edit_offer">
        <button type="submit" class="datatable_buttons"><?php _e( 'Save changes' ); ?></button>
    </div>
</form>