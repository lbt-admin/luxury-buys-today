<?php
/**
 * Dealer promotions block template
 *
 * @package Novus
 */

$promotions_column_posts = $args['promotions_column_posts'] ?? array();

$promotions_counter = 0;

if ( ! empty( $promotions_column_posts ) ) :
	foreach ( $promotions_column_posts as $remote_key => $remote_promotion ) :
		$promotions_counter++;

		if ( ! is_numeric( $remote_key ) ) {
			$blog_post_type = 'blog-promotions';

			if ( 1 === $promotions_counter ) {
				$blog_post_type = 'blog-promotions-large';
			}
		} else {
			$blog_post_type = 'blog-remote-promotions';

			if ( 1 === $promotions_counter ) {
				$blog_post_type = 'blog-remote-promotions-large';
			}
		}

		get_template_part( 'partials/content', $blog_post_type, array( 'remote_promotion' => $remote_promotion ) );
	endforeach;
else :
	echo 'No active promotions at this time. Please visit back later.';
endif;
