<?php
/**
 * @package Novus
 */
?>

<?php 
	$edgar_cats = array( '36', '37', '38', '39' );
?> 
    <article id="post-<?php the_ID(); ?>" <?php if ( in_category( $edgar_cats, get_the_ID() ) && ! is_category( $edgar_cats ) ) {
		post_class( 'wpr-display-shadow hentry--masonry' );
	} else {
		post_class( 'hentry--masonry' );
	} ?>>
        <div class="entry-wrap">
			<?php
			get_template_part( 'partials/content', 'post-top' );
			get_template_part( 'partials/content', 'post-title' );
			get_template_part( 'partials/content', 'post-image' );
			include( locate_template( 'partials/content-post-content.php', false, false ) );
			get_template_part( 'partials/content', 'post-footer' );
			?>

        </div>
    </article><!-- #post-## --> 