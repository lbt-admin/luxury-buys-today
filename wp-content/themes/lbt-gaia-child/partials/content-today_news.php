<?php
/**
 * Today news template
 *
 * @package Novus
 */

$category    = get_the_category( get_the_ID() );
$category_id = $category[0]->term_id;

?>

<article id="post-<?php the_ID(); ?>" <?php post_class( 'hentry--masonry' ); ?>>
	<div class="entry-wrap">
		<?php
		get_template_part( 'partials/content', 'post-top' );
		get_template_part( 'partials/content', 'post-title' );
		get_template_part( 'partials/content', 'post-image' );
		get_template_part( 'partials/content', 'post-content' );
		?>
		<footer class="entry-footer">
			<div class="test-tset">
				<?php if ( 'post' === get_post_type() ) : ?>
					<div class="entry-meta wpr-entry-meta-archive">
						<?php wpr_entry_meta(); ?>
					</div><!-- .entry-meta -->
				<?php endif; ?>

				<div class="entry-summary wpr-article-source-archive">
					<?php
					$article_url = get_post_meta( get_the_ID(), 'wpr_article_source', true );

					if ( strpos( $article_url, 'chanel' ) !== false ) {
						$str_group = explode( ':', $article_url );

						$article_url = 'https:' . $str_group[1];
					}

					if ( $article_url ) {
						echo '<a href="' . esc_url_raw( $article_url ) . '" rel="dofollow" target="_blank">Source Link</a>';
					} else {
						$brand_catalog = get_term_meta( $category_id, 'wpr_brand_catalog', true );

						if ( $brand_catalog ) {
							echo '<a href="' . esc_url_raw( $brand_catalog ) . '" rel="dofollow" target="_blank">Source Link</a>';
						}
					}
					?>
				</div>
			</div>
			<div class="entry-summary mix">
				<strong class="post-date"><?php novus_posted_on( false ); ?></strong>&nbsp;<?php the_excerpt(); ?>
			</div>
			<?php
			if ( ! \LBT\Category::is_broyhill_category( $category_id ) ) :
				?>
				<div class="archieve-cust-boxes">
					<?php novus_entry_footer( false ); ?>
					<?php get_template_part( 'partials/content', 'post-rating' ); ?>
					<?php
					if ( shortcode_exists( 'apss_share' ) ) {
						echo do_shortcode( "[apss_share theme='3' counter='1']" );
					}
					?>
				</div>
				<?php endif; ?>
		</footer><!-- .entry-footer -->
	</div>
</article><!-- #post-## -->
