<?php
/**
 * Login modal
 *
 * @package LBT
 */

$remove_hide       = $args['remove_hide'] ?? false;
$hide_social_login = $args['hide_social_login'] ?? false;
$show_in_page      = $args['show_in_page'] ?? false;

?>

<section id="lbt-register-form-section" class="<?php echo ! $remove_hide ? 'lbt-login-form-hide' : ''; ?> <?php echo $show_in_page ? 'lbt-register-form-in-page' : ''; ?> lbt-auth-form-section">
	<?php if ( ! $show_in_page ) : ?>
		<div class="lbt-login-form-header">
			<img src="<?php echo esc_url( LBT_THEME_URL . 'assets/images/login-logo.png' ); ?>" alt="Luxury Buys Today">
			<span class="lbt-login-form-close" aria-label="Close">×</span>
		</div>
	<?php endif; ?>
	<div class="lbt-register-form-info">
		<div class="lbt-register-form-left-side">
			<h2>Create your account</h2>
			<div class="signup-benifits">
				<ul>
					<li><img src="https://luxurybuystoday-media.s3.amazonaws.com/media/uploads/2018/04/32845-s-1.jpg" alt="" width="150" height="150" class="alignnone size-full"/> <span>vip special member discount price notices</span></li>
					<li><img src="https://luxurybuystoday-media.s3.amazonaws.com/media/uploads/2018/04/32845-s-2.jpg" alt="" width="150" height="150" class="alignnone size-full"/> <span>prized invitations to local sales events</span></li>
					<li><img src="https://luxurybuystoday-media.s3.amazonaws.com/media/uploads/2018/04/32845-s-3.jpg" alt="" width="150" height="150" class="alignnone size-full"/> <span>daily brand news, events and promotions</span></li>
					<li><img src="https://luxurybuystoday-media.s3.amazonaws.com/media/uploads/2018/04/32845-s-4.jpg" alt="" width="150" height="150" class="alignnone size-full"/> <span>free live concierge services</span></li>
				</ul>
			</div>
			<?php if ( ! $show_in_page ) : ?>
			<div class="lbt-login-form-footer">
				<div><strong><?php esc_html_e( 'Already have an account?', 'lbt' ); ?> <a href="#" class="lbt-form-switch"><?php esc_html_e( 'Sign in', 'lbt' ); ?></a></strong></div>
			</div>
			<?php endif; ?>
		</div>
		<div class="lbt-register-form-right-side">
			<div class="lbt-login-form-social">
				<?php
				if ( shortcode_exists( 'oa_social_login' ) && ! $hide_social_login ) {
					echo do_shortcode( '[oa_social_login]' );
				}
				?>
			</div>
			<?php if ( ! $show_in_page ) : ?>
			<div class="lbt-or-label">
				<?php
				if ( shortcode_exists( 'oa_social_login' ) && ! $hide_social_login ) :
					esc_html_e( 'OR Create Your Account', 'lbt' );
				else :
					esc_html_e( 'Create Your Account', 'lbt' );
				endif;
				?>
			</div>
			<?php endif; ?>
			<div class="lbt-register-form">
				<div class="lbt-register-error-message"></div>
				<form method="POST" id="lbt-register-form">
					<div class="lbt-form-field-group-col-2">
						<div class="lbt-form-field-group">
							<label for="lbt-form-fname">*<?php esc_html_e( 'First Name', 'lbt' ); ?></label>
							<input type="text" name="first_name" class="lbt-form-text-field" id="lbt-form-fname" required>
						</div>
						<div class="lbt-form-field-group">
							<label for="lbt-form-lname">*<?php esc_html_e( 'Last Name', 'lbt' ); ?></label>
							<input type="text" name="last_name" class="lbt-form-text-field" id="lbt-form-lname" required>
						</div>
					</div>
					<div class="lbt-form-field-group">
						<label for="lbt-form-reg-email">*<?php esc_html_e( 'Email', 'lbt' ); ?></label>
						<input type="email" name="email" class="lbt-form-text-field" id="lbt-form-reg-email" required autocomplete="email">
					</div>
					<div class="lbt-form-field-group">
						<label for="lbt-form-reg-phone">*<?php esc_html_e( 'Mobile Phone', 'lbt' ); ?></label>
						<input type="text" name="phone" class="lbt-form-text-field" id="lbt-form-reg-phone" required>
						<div class="lbt-form-field-description"><?php esc_html_e( 'Add your cellphone to qualify for VIP pricing before it\'s too late.', 'lbt' ); ?></div>
					</div>
					<div class="lbt-form-field-group">
						<label for="lbt-form-reg-zip">*<?php esc_html_e( 'Zip Code', 'lbt' ); ?></label>
						<input type="text" name="zip_code" class="lbt-form-text-field" id="lbt-form-reg-zip" required>
					</div>
					<div class="lbt-form-field-group">
						<label for="lbt-form-reg-password">*<?php esc_html_e( 'Create Password', 'lbt' ); ?></label>
						<input type="password" name="password" class="lbt-form-text-field" id="lbt-form-reg-password" autocomplete="new-password" required>
					</div>
					<div class="lbt-password-strength"><?php esc_html_e( 'Strength Indicator', 'lbt' ); ?></div>
					<div class="lbt-form-field-group">
						<label for="lbt-form-reg-conf-password">*<?php esc_html_e( 'Confirm Password', 'lbt' ); ?></label>
						<input type="password" name="confirm_password" class="lbt-form-text-field" autocomplete="new-password" id="lbt-form-reg-conf-password" required>
					</div>
					<div class="lbt-form-field-group">
						<input type="hidden" name="action" value="lbt_register">
						<input type="hidden" name="__nonce" value="<?php echo esc_attr( wp_create_nonce( 'lbt-register-form-nonce' ) ); ?>">
						<?php if ( $show_in_page ) : ?>
							<input type="hidden" name="modal_location" value="in_page">
							<input type="hidden" name="brand" value="<?php echo esc_attr( $_GET['brand'] ?? '' ); ?>">
						<?php endif; ?>
						<button type="submit" class="lbt-form-submit-button" data-loading-text="<?php esc_attr_e( 'Processing...', 'lbt' ); ?>">
							<?php esc_html_e( 'Create Your Account', 'lbt' ); ?>
						</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</section>
