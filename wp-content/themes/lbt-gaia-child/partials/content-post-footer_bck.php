<footer class="entry-footer">
<div class="entry-summary mix">
      <strong class="post-date"><?php novus_posted_on(false); ?></strong>
       <?php the_excerpt(); ?>
    </div>
    <div class="test-tset">
    <?php if (function_exists('the_favorites_button')) : ?>
        <div class="simplefavorite-wrap">
            <?php  $lookbook_img = '';
            if (function_exists('get_favorite_as_image')) {
                echo $lookbook_img = get_favorite_as_image();
            } ?>
        </div>
    <?php endif; ?>
    <?php if ('post' == get_post_type()) : ?>
        <div class="entry-meta wpr-entry-meta-archive">
            <?php wpr_entry_meta(); ?>
        </div><!-- .entry-meta -->
    <?php endif; ?>
    

    <div class="entry-summary wpr-article-source-archive">
        <?php
        $article_url = get_post_meta(get_the_ID(), 'wpr_article_source', true);
        $category = get_the_category(get_the_ID());
        $category_id = $category[0]->term_id;
        if (strpos($article_url, 'chanel') !== false) {
            $strGroup = explode(":", $article_url);

            $article_url = "https:" . $strGroup[1];
        }

        // if (strpos($article_url, 'Fendi') !== false) {
        if (0) {
            $strGroup = explode(":", $article_url);

            $article_url = "https:" . $strGroup[1];
        }


        if ($article_url) {
            echo sprintf('%s <a href="%s" rel="dofollow" target="_blank">%s</a>', __('From:'), esc_url_raw($article_url), esc_url_raw($article_url));
        } else {
            $wpr_brand_catalog = get_term_meta($category_id, 'wpr_brand_catalog', true);

            if ($wpr_brand_catalog) {
                echo sprintf('%s <a href="%s" rel="dofollow" target="_blank">%s</a>', __('From:'), esc_url_raw($wpr_brand_catalog), esc_url_raw($wpr_brand_catalog));
            }
        }

        ?>

    </div>
    </div>

    

<div class="archieve-cust-boxes">
    <a href="<?php echo esc_url(get_the_permalink()); ?>">
        <blockquote>
            <?php wpr_display_broyhill_comment_name($category_id, get_the_ID()); ?>
            <br/>
            <div class="edd_comment_wrapper">
                "<?php if (function_exists('wpr_display_ed_comment')) {
                    echo wpr_display_ed_comment(get_the_ID());
                } else {
                    trim(the_title());
                } ?>
                "
            </div>
        </blockquote>
    </a>

    <?php novus_entry_footer(false); ?>

    <?php get_template_part('partials/content', 'post-rating'); ?>

    <br/>
    <div class="wpr-not-affiliated">
        <?php echo sprintf('%s <a href="%s">%s</a>', __('* Not Affiliated with Brand Featured. See'), esc_url(home_url()) . '/trademark-copyright-policy/', __('Trademark & Copyright Policy')); ?>
    </div>

    <?php
    //		// Share post links
    //		if ( ( get_theme_mod( 'article_share_links', true ) && is_single() ) || ( get_theme_mod( 'archive_share_links', true ) && ! is_single() ) ) {
    //			novus_share_post();
    //		}
    if (shortcode_exists('apss_share')) {
        echo do_shortcode("[apss_share theme='3' counter='1']");
    }
    ?>
    </div>
</footer><!-- .entry-footer -->