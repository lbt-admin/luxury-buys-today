<?php
/**
 * Blog post big article template
 *
 * @package LBT
 */

$category    = get_the_category( get_the_ID() );
$category_id = $category[0]->term_id;

$is_broyhill_category = \LBT\Category::is_broyhill_category( $category_id )

?>

<div class="wpr-post-top">
	<div class="wpr-post-ed-broyhill">
		<img src="<?php echo esc_url( LBT_THEME_URL . 'assets/images/Ed-Broyhill-light-circle.jpg' ); ?>"/>
	</div>
	<div class="wpr-post-meta">
		<?php if ( $is_broyhill_category ) : ?>
			<div class="meta-box">
				<?php echo sprintf( '%s: <a href="%s" target="_blank">%s</a>', esc_html__( 'Authored by' ), 'http://luxurybuystoday.tumblr.com/', esc_html__( '&nbsp;Ed Broyhill' ) ); ?>
			</div>
			<div class="meta-box date">
				<span class="separator">|</span>
				<a href="#"><?php echo get_the_date(); ?></a>
			</div>
		<?php else : ?>
			<div class="meta-box">
				<?php echo sprintf( '%s: <a href="%s" target="_blank">%s</a>', esc_html__( 'Curated by' ), 'http://luxurybuystoday.tumblr.com/', esc_html__( '&nbsp;Ed Broyhill' ) ); ?>
			</div>
			<div class="meta-box">
				<?php echo sprintf( '%s: <a href="%s">%s</a>', esc_html__( 'onto' ), esc_url( home_url() ), esc_html__( '&nbsp;Luxury Buys Today' ) ); ?>
			</div>
			<div class="meta-box date">
				<span>Published by: </span>
				<a href="<?php echo esc_url( get_author_posts_url( '1' ) ); ?>"> Ed Broyhill</a>
				<span class="separator">|</span>
				<a href="#"><?php echo get_the_date(); ?></a>
			</div>
		<?php endif; ?>
	</div>
</div>
