<div id="wpr-filter-offers">
    <div class="container">
        <a href="#" class="wpr-close-filter-panel">X</a>
        <form id="wpr-filter-offers-form">
			<?php
			$categories    = array( 8, 13, 14, 17 );
			$popular_cats  = false;
			$walker        = null;
			$selected_cats = wpr_selected_user_brands();

			foreach ( $categories as $category ) { ?>
                <ul class="wpr-list-brands">
					<?php
					wp_category_checklist( 0, $category, $selected_cats, $popular_cats, $walker, false );
					?>
                </ul>
			<?php } ?>
            <div class="wpr-submit-filter">
                <button><?php echo __( 'Save' ); ?></button><a href="#" class="wpr-clear-brand-selection"><?php echo __('Clear selection'); ?></a>
            </div>
        </form>
    </div>
</div>
