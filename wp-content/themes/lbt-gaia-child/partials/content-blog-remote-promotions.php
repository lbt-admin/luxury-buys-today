<?php
/**
 * Blog Post Remote Promotions template list
 *
 * @package LBT
 */

$remote_promotion = $args['remote_promotion'] ?? array();

if ( empty( $remote_promotion ) ) {
	return;
}

?>

<a href="<?php echo esc_attr( $remote_promotion['target_href'] ); ?>?id=<?php echo esc_attr( $remote_promotion['ID'] ); ?>" class="post-list-item">
	<?php if ( ! empty( $remote_promotion['listing_image_url'] ) ) : ?>
		<figure class="featured-image">
			<img src="<?php echo esc_url( $remote_promotion['listing_image_url'] ); ?>" class="image-cover" data-test="1" onerror="this.src='<?php echo esc_url( $remote_promotion['listing_image_url'] ); ?>'; if ( this.getAttribute('retry') ) { if( this.getAttribute('retry') == 1 ) { this.src='<?php echo esc_url( $remote_promotion['listing_image_url'] ); ?>'; this.setAttribute( 'retry', 2 ); } else { this.src='<?php echo esc_url( LBT_THEME_URL ); ?>assets/images/post-thumb-cropped.svg'; } } else { this.setAttribute( 'retry', 1 ); }">
		</figure>
	<?php endif; ?>
	<div class="content">
		<h4 class="post-title"><?php echo esc_html( $remote_promotion['listing_item_name'] ); ?></h4>
		<?php if ( ! empty( $remote_promotion['is_dealership'] ) ) : ?>
			<p>
				<?php
				$listing_zip = isset( $listing_city['listing_zip'] ) ? ' ' . $listing_city['listing_zip'] : '';

				$location = $remote_promotion['listing_city'] . ', ' . $remote_promotion['listing_state'] . $listing_zip;
				$price    = number_format( $remote_promotion['listing_price'], 0, '.', ',' );

				echo wp_kses_post( 'Location: ' . $location . '<br>' );
				echo wp_kses_post( 'Price: $' . $price . '<br>' );
				?>
				<span class="read-more">Read more...</span>
			</p>
		<?php else : ?>
			<?php echo wp_kses_post( lbt_the_excerpt_truncate( str_replace( 'Source link', '', wp_strip_all_tags( do_shortcode( $remote_promotion['content'] ) ) ), 30 ) ); ?> <span class="read-more">Read more...</span>
		<?php endif; ?>
	</div>
</a>
