<?php
$user_id = get_current_user_id();
$saved_brands = get_user_meta($user_id);
$ids = array_unique( (array) $saved_brands['saved_deals_brands'] );
$selected_cats = get_user_meta(get_current_user_id(), 'wpr_selected_brands', true);

$category_ids = [];

$ontraport_dealership_ids = implode( ',', $ids );

switch_to_blog(1);

global $wpdb;

$table_name = $wpdb->prefix . 'wpr_ontraport_dealerships';

$sql = "SELECT data, brand FROM {$table_name} WHERE ontraport_id IN (" . esc_sql( $ontraport_dealership_ids ) . ") GROUP BY brand";

$my_saved_brands = $wpdb->get_results( $sql );

restore_current_blog();

?>
<div class="content-header">
    <div class="title-wrapper">
        <h1 class="title">My Saved Brands</h1>
    </div>
    <div class="filter-container">
        <h2>SORT BY BRAND</h2>
        <select id="cat-brand-filter" multiple="multiple" class="regular-text">
            <option value="">All Brands</option>
            <?php
            foreach ($my_saved_brands as $brandSingle) {
                if (isset($_GET['brand_cat_id']) && $_GET['brand_cat_id'] != '' && $_GET['brand_cat_id'] != 'null') {
                    $brandsList = explode(',', $_GET['brand_cat_id']);
                    if (in_array($brandSingle->brand, $brandsList)) {
                        $selection = 'selected';
                    } else {
                        $selection = '';
                    }
                } else {
                    $selection = '';
                }
                $brands_cat_tax = get_term_by( 'name', $brandSingle->brand, 'category' );

                if ( ! empty( $brands_cat_tax ) && $brands_cat_tax->term_id == 36 || $brands_cat_tax->term_id == 37 || $brands_cat_tax->term_id == 38 || $brands_cat_tax->term_id == 39) :
                else:
                    echo '<option value="' . $brandSingle->brand . '" ' . $selection . '>' . $brandSingle->brand . '</option>';
                endif;
            }
            ?>
        </select>
    </div>
</div>
<?php

//**************Get and display posts by category id by query string ********************//

if (isset($_GET['brand_cat_id']) && $_GET['brand_cat_id'] != '' && $_GET['brand_cat_id'] != 'null') {
    $brand_cat_ids = explode(",", $_GET['brand_cat_id']);

    switch_to_blog(1);

    global $wpdb;

    $table_name = $wpdb->prefix . 'wpr_ontraport_dealerships';

    foreach ( $brand_cat_ids as $key => $b_cat_ids ) {
        $brand_cat_ids[ $key ] = $wpdb->prepare( '%s', $b_cat_ids );
    }

    $brand_cat_ids = implode( ',', $brand_cat_ids );

    $sql = "SELECT brand, data FROM {$table_name} WHERE brand IN (" . $brand_cat_ids . ") GROUP BY brand";

    $filtered_brands = $wpdb->get_results( $sql );

    restore_current_blog();

    if ( ! empty( $filtered_brands ) ) {
        echo '<div class="post-wrapper">';
       foreach ( $filtered_brands as $f_brand ) {
            $brand_data = maybe_unserialize( $f_brand->data );

            switch_to_blog(1);
            $brands_cat_tax = get_term_by( 'name', $f_brand->brand, 'category' );
            restore_current_blog();

            $cate_id = ! ( empty( $brands_cat_tax ) ) ? $brands_cat_tax->term_id : 0;

            ?>
            <div class="post-box-wrap" id="post-box-<?php echo $brand_data['id']; ?>">
                <div class="post-box">
                    <?php

                    $long = $brand_data['f2824'];
                    $lati = $brand_data['f2823'];
                    $store_address = $brand_data['f1716'];
                    $store_city = $brand_data['f1718'];
                    $store_state = $brand_data['f2107'];
                    $store_number = $brand_data['f2933'];
                    $store_web_adress = $brand_data['f1734'] ?? '';
                    $store_phone_number = $brand_data['f2933'];
                    $store_country = 'US';
                    $store_zip_code = $brand_data['f2108'];

                    $google_map = $store_address . ',' . $store_city . ',' . $store_state;

                    // Ugurcan
                    switch_to_blog( 1 );
                    $brand_image = get_term_meta( $cate_id, 'wpr_brand_image_url', true );
                    $brand_catalog = get_term_meta( $cate_id, 'wpr_brand_catalog', true );
                    $brand_link_text = get_term_meta( $cate_id, 'wpr_brand_link_text', true );
                    restore_current_blog();

                    $category_name = $brands_cat_tax->name;
                    $category_url = get_term_link($brands_cat_tax);

                    // Ugurcan
                    ?>
                    <div id="zm-profile-description"
                        class="zm-dealer-details-single main-brand-page-wrap custom-new saved-brand"
                        data-deal-title="<?php echo $brand_data['f1714']; ?>"
                        data-deal-address="<?php echo $google_map; ?>"
                        data-deal-contact="<?php echo $store_number; ?>"
                        data-deal-state="<?php echo $store_state; ?>"
                        data-deal-city="<?php echo $store_city; ?>">
                        <div class="zm-profile-description main-brand-page">
                            <div class="closest-store">YOUR CLOSEST STORE</div>
                            <div class="zm-profile-media">
                                <div class="profile-image">
                                    <img src="https://luxurybuystoday-media.s3.amazonaws.com/media/uploads/2020/07/img-avatar-png-png.png"
                                    alt="Profile Image">
                                </div>
                                <?php if ( !empty($brand_image) ) : ?>
                                <div class="brand-logo">
                                    <img src="<?php echo $brand_image; ?>" alt="<?php echo $category_name; ?>">
                                </div>
                                <?php endif; ?>
                            </div>
                            <div class="zm-dealer-name">
                                <h2 class="zm-profile-title" data-deal-id="<?php echo $brand_data['id']; ?>">
                                    <?php echo esc_html( $category_name ); ?>
                                </h2>
                            </div>
                            <div class="store-location"><?php echo $google_map; ?></div>
                        </div>
                        <div class="zm-profile-description main-brand-page">
                                <div class="zm-cta-actions">
                                    <div class="lbt-dealer-cta-request-btn zm-cta-btn" data-action="contact">
                                        <i class="fa icon-phone" aria-hidden="true"></i>call me
                                    </div>
                                    <div class="zm-cta-btn lbt-appointment-button" data-action="appointment">
                                        <i class="fa icon-calendar"></i>
                                        <span class="long">make an appt</span>
                                        <span class="short">appt</span>
                                    </div>
                                    <div class="lbt-dealer-cta-request-btn zm-cta-btn" data-action="learn">
                                        <i class="fa icon-info" aria-hidden="true"></i>
                                        <span class="long">request more info</span>
                                        <span class="short">info</span>
                                    </div>
                                    <div class="zm-remove lbt-toggle-brand-btn" remove-brand="active" data-dealership-id="<?php echo $brand_data['id'];?>">
                                        <i class="fa fa-heart red" aria-hidden="true"></i>Remove
                                    </div>
                                    <span class="pumOpen" id="pumOpen" style="display: none;"></span>
                                </div>
                            <div class="zm-dealer-details">
                                <div class="zm-dealer-map">
                                    <?php if (!empty($google_map)) : ?>
                                        <i class="fa fa-map-marker"></i>
                                        <a href="https://www.google.com/maps?q=<?php echo substr($google_map, 0, -2); ?>"
                                        target="_blank" class="wpr-map-location" data-do="map">Directions</a>
                                    <?php endif; ?>
                                </div>
                                <div class="zm-dealer-facebook">
                                    <i class="fa fa-facebook"></i>
                                    <a href="<?php echo esc_url($brand_link_text); ?>">Facebook</a>
                                </div>
                                <div class="zm-dealer-phone zm-cta-btn" data-do="contact">
                                    <i class="fa icon-cell-phone" aria-hidden="true"></i>
                                    <span>Request call back</span>
                                </div>
                                <?php if (!empty($brand_catalog)) : ?>
                                <div class="zm-view-brand-catalog">
                                    <i class="fa icon-share"></i>
                                    <a href="<?php echo esc_url($brand_catalog); ?>"
                                    target="_blank" class=" wpr-brand-catalog">Brand Catalog </a>
                                </div>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>

                    <a href="<?php echo esc_url($category_url); ?>" target="_blank" class="brand-link">VISIT BRAND PAGE</a>
                </div>
            </div>

            <?php
        }//while loop ends

        wp_reset_postdata();
    }
    echo '</div>';


} else {

if(count($ids) !== 0) :

?>
<div class="post-wrapper">
<?php
    if ( ! empty( $my_saved_brands ) ) :
        foreach ( $my_saved_brands as $my_saved_brand ) :
            $brand_data = maybe_unserialize( $my_saved_brand->data );

            switch_to_blog(1);
            $brands_cat_tax = get_term_by( 'name', $my_saved_brand->brand, 'category' );
            restore_current_blog();

            $cate_id = ! ( empty( $brands_cat_tax ) ) ? $brands_cat_tax->term_id : 0;


    ?>
    <div class="post-box-wrap" id="post-box-<?php echo $brand_data['id']; ?>">
        <div class="post-box">
            <?php

            $long = $brand_data['f2824'];
            $lati = $brand_data['f2823'];
            $store_address = $brand_data['f1716'];
            $store_city = $brand_data['f1718'];
            $store_state = $brand_data['f2107'];
            $store_number = $brand_data['f2933'];
            $store_web_adress = $brand_data['f1734'] ?? '';
            $store_phone_number = $brand_data['f2933'];
            $store_country = 'US';
            $store_zip_code = $brand_data['f2108'];

            $google_map = $store_address . ',' . $store_city . ',' . $store_state;

            // Ugurcan
            switch_to_blog( 1 );
            $brand_image = get_term_meta( $cate_id, 'wpr_brand_image_url', true );
            $brand_catalog = get_term_meta( $cate_id, 'wpr_brand_catalog', true );
            $brand_link_text = get_term_meta( $cate_id, 'wpr_brand_link_text', true );
            restore_current_blog();

            $category_name = $brands_cat_tax->name;
            $category_url = get_term_link($brands_cat_tax);
            // Ugurcan
            ?>
            <div id="zm-profile-description"
                class="zm-dealer-details-single main-brand-page-wrap custom-new saved-brand"
                data-deal-title="<?php echo $brand_data['f1714']; ?>"
                data-deal-address="<?php echo $google_map; ?>"
                data-deal-contact="<?php echo $store_number; ?>"
                data-deal-state="<?php echo $store_state; ?>"
                data-deal-city="<?php echo $store_city; ?>">
                <div class="zm-profile-description main-brand-page">
                    <div class="closest-store">YOUR CLOSEST STORE</div>
                    <div class="zm-profile-media">
                        <div class="profile-image">
                            <img src="https://luxurybuystoday-media.s3.amazonaws.com/media/uploads/2020/07/img-avatar-png-png.png"
                            alt="Profile Image">
                        </div>
                        <?php if ( !empty($brand_image) ) : ?>
                        <div class="brand-logo">
                            <img src="<?php echo $brand_image; ?>" alt="<?php echo $category_name; ?>">
                        </div>
                        <?php endif; ?>
                    </div>
                    <div class="zm-dealer-name">
                        <h2 class="zm-profile-title" data-dealership-id="<?php echo $brand_data['id']; ?>">
                            <?php echo esc_html( $category_name ); ?>
                        </h2>
                    </div>
                    <div class="store-location"><?php echo $google_map; ?></div>
                </div>
                <div class="zm-profile-description main-brand-page">
                        <div class="zm-cta-actions">
                            <div class="lbt-dealer-cta-request-btn zm-cta-btn" data-action="contact" data-brand-id="<?php echo esc_attr( $brands_cat_tax->term_id ); ?>" data-dealership-id="<?php echo $brand_data['id'];?>">
                                <i class="fa icon-phone" aria-hidden="true"></i>call me
                            </div>
                            <div class="zm-cta-btn lbt-appointment-button" data-action="appointment" data-brand-id="<?php echo esc_attr( $brands_cat_tax->term_id ); ?>" data-dealership-id="<?php echo $brand_data['id'];?>">
                                <i class="fa icon-calendar"></i>
                                <span class="long">make an appt</span>
                                <span class="short">appt</span>
                            </div>
                            <div class="lbt-dealer-cta-request-btn zm-cta-btn" data-action="learn" data-brand-id="<?php echo esc_attr( $brands_cat_tax->term_id ); ?>" data-dealership-id="<?php echo $brand_data['id'];?>">
                                <i class="fa icon-info" aria-hidden="true"></i>
                                <span class="long">request more info</span>
                                <span class="short">info</span>
                            </div>
                            <div class="zm-remove lbt-toggle-brand-btn" remove-brand="active" data-brand-id="<?php echo esc_attr( $brands_cat_tax->term_id ); ?>" data-dealership-id="<?php echo $brand_data['id'];?>">
                                <i class="fa fa-heart red" aria-hidden="true"></i>Remove
                            </div>
                            <span class="pumOpen" id="pumOpen" style="display: none;"></span>
                        </div>
                    <div class="zm-dealer-details">
                        <div class="zm-dealer-map">
                            <?php if (!empty($google_map)) : ?>
                                <i class="fa fa-map-marker"></i>
                                <a href="https://www.google.com/maps?q=<?php echo substr($google_map, 0, -2); ?>"
                                   target="_blank" class="wpr-map-location" data-do="map">Directions</a>
                            <?php endif; ?>
                        </div>
                        <div class="zm-dealer-facebook">
                            <i class="fa fa-facebook"></i>
                            <a href="<?php echo esc_url($brand_link_text); ?>">Facebook</a>
                        </div>
                        <div class="zm-dealer-phone zm-cta-btn" data-do="contact">
                            <i class="fa icon-cell-phone" aria-hidden="true"></i>
                            <span>Request call back</span>
                        </div>
                        <?php if (!empty($brand_catalog)) : ?>
                        <div class="zm-view-brand-catalog">
                            <i class="fa icon-share"></i>
                            <a href="<?php echo esc_url($brand_catalog); ?>"
                               target="_blank" class=" wpr-brand-catalog">Brand Catalog </a>
                        </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>

            <a href="<?php echo esc_url($category_url); ?>" target="_blank" class="brand-link">VISIT BRAND PAGE</a>
        </div>
    </div>

    <?php
    endforeach;
    endif;

else :
    echo '<h3 style="padding:0 17px;margin-top: 40px;font-family: Lato,sans-serif !important;">No brands saved here.</h3>';
endif;

} ?>
</div>
