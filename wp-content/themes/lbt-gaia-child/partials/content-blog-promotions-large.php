<?php
/**
 * Blog Post Promotions template Large
 *
 * @package LBT
 */

$remote_promotion = $args['remote_promotion'] ?? array();

if ( ! empty( $remote_promotion ) ) {
	if ( is_a( $remote_promotion, '\WP_Post' ) ) {
		global $post;
		$old_post = $post;
		$post     = $remote_promotion;
		setup_postdata( $remote_promotion );
	}
}

$thumbnail = \LBT\Post::get_thumbnail( get_the_ID() );

$category = get_the_category()[0] ?? false;

$catalog_url = get_term_meta( $category->term_id, 'wpr_brand_catalog', true );

$is_favorited = false;

if ( is_user_logged_in() ) {
	$user_lookbook = \LBT\Lookbook::get_user_lookbook();
	$is_favorited  = in_array( get_the_ID(), $user_lookbook );
}

?>
<article id="post-<?php the_ID(); ?>" class="blog-content">
	<div class="entry-wrap">
		<header class="entry-header" data-post-id="<?php the_ID(); ?>">
		</header>
		<h1 class="entry-title">
			<a href="<?php the_permalink(); ?>" rel="bookmark">
				<?php the_title(); ?>
			</a>
		</h1>
		<?php if ( $thumbnail ) : ?>
			<div class="content-post-image">
				<a href="<?php the_permalink(); ?>">
					<div class="entry-image">
						<div class="promotions-flag">Premiere<br>Promotion</div>
						<img src="<?php echo esc_url( $thumbnail ); ?>" />
					</div>
				</a>
				<?php if ( $is_favorited ) : ?>
					<div class="save-to-favorites zm-cta-btn lbt-toggle-lookbook-post lbt-big-article-btn" data-postid="<?php echo esc_attr( get_the_ID() ); ?>">REMOVE FROM LOOKBOOK</div>
					<?php else : ?>
						<div class="save-to-favorites zm-cta-btn lbt-toggle-lookbook-post lbt-big-article-btn" data-postid="<?php echo esc_attr( get_the_ID() ); ?>">SAVE TO LOOKBOOK</div>
				<?php endif; ?>
			</div>
		<?php endif; ?>
		<footer class="entry-footer">
			<div class="test-tset">
				<div class="entry-meta wpr-entry-meta-archive">
					<span class="cat-links">
						<strong><?php echo esc_html( $category ? $category->name : '' ); ?> promotions</strong>
					</span>
				</div>
				<?php if ( $catalog_url ) : ?>
				<div class="entry-summary wpr-article-source-archive">
					<a href="<?php echo esc_url( $catalog_url ); ?>" rel="dofollow" target="_blank">Source Link</a>
				</div>
				<?php endif; ?>
			</div>
			<div class="entry-summary mix">
				<strong class="post-date">
					<span class="posted-on">
						<time class="entry-date published updated" datetime="<?php echo esc_attr( get_the_date() ); ?>"><?php echo esc_html( get_the_date( 'F j, Y' ) ); ?></time>
					</span>
				</strong>
				<a href="<?php the_permalink(); ?>" class="read-more-link"> Read more...</a>
			</div>
			<?php
			if ( ! \LBT\Category::is_broyhill_category( $category->term_id ) ) :
				?>
				<div class="archieve-cust-boxes">
					<?php novus_entry_footer( false ); ?>
					<?php
					if ( shortcode_exists( 'apss_share' ) ) {
						echo do_shortcode( "[apss_share theme='3' counter='1']" );
					}
					?>
				</div>
			<?php endif; ?>
		</footer>
	</div>
</article>
<?php
if ( ! empty( $remote_promotion ) ) {
	if ( is_a( $remote_promotion, '\WP_Post' ) ) {
		global $post;
		$post = $old_post;
		setup_postdata( $post );
	}
}
?>
