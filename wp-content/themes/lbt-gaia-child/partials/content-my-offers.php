<?php
$my_offers = \LBT\Offer::get_user_offers();

$brands_ids = array();

if ( ! empty( $my_offers ) ) {
	$filtered_brands = ! empty( $_GET['brand_id'] ) ? explode(',', $_GET['brand_id'] ) : array();

	$current_page = ( get_query_var('paged') ) ? get_query_var('paged') : 1;

	$args = array(
		'post_type' => 'post',
		'orderby'   => 'date',
		'order'     => 'DESC',
		'paged'     => $current_page,
		'post__in'  => $my_offers,
	);

	if ( ! empty( $filtered_brands ) ) {
		$args['category__in'] = $filtered_brands;
	}

	$display_posts = new WP_Query( $args );

	if ( $display_posts->have_posts() ) {
		foreach ( $display_posts->posts as $p ) {
			$category = get_the_category( $p->ID )[0] ?? false;
			if ( $category ) {
				$brands_ids[ $category->term_id ] = $category->name;
			}
		}
	}
}
?>
<div class="content-header">
    <div class="title-wrapper">
        <h1 class="title">My Offers</h1>
    </div>
    <div class="filter-container">
        <h2>SORT BY BRAND</h2>
        <select id="brand-filter" multiple="multiple" class="regular-text">
            <option value="">All Brands</option>
            <?php
            foreach ($brands_ids as $brand_id => $brand_name ) {
                if ( ! empty( $_GET['brand_id'] ) ) {
                    if (in_array($brand_id, $filtered_brands)) {
                        $selection = 'selected';
                    } else {
                        $selection = '';
                    }
                } else {
                    $selection = '';
                }

                if ( $brand_id == 36 || $brand_id == 37 || $brand_id == 38 || $brand_id == 39) :
                else:
                    echo '<option value="' . $brand_id . '" ' . $selection . '>' . $brand_name . '</option>';
                endif;
            }
            ?>
        </select>
    </div>
</div>
<main class="site-main" id="main">
    <?php
    if (!empty($my_offers)) {

        if ($display_posts->have_posts()) {

            do_action('novus_loop_before');

            while ($display_posts->have_posts()) {
                $display_posts->the_post();
                get_template_part('partials/content', get_post_format());
            }

            do_action('novus_loop_after');
            $GLOBALS['wp_query']->max_num_pages = $display_posts->max_num_pages;
            the_posts_navigation(array(
                'prev_text' => __('&lsaquo; Older posts', 'novus'),
                'next_text' => __('Newer posts &rsaquo;', 'novus'),
            ));
        } else {
            get_template_part('partials/content', 'none');
        }
        wp_reset_postdata();
    } else { ?>
        <header class="page-header">
            <h1 class="page-title"><?php _e('Save posts as favorite to start seeing their offers!', 'novus'); ?></h1>
        </header>
    <?php } ?>
</main>
