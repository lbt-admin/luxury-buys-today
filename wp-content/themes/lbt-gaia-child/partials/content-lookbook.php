<?php
$user_id = get_current_user_id();
$saved_brands = get_user_meta($user_id);
$ids = $saved_brands['simplefavorites'];
$selected_cats = get_user_meta(get_current_user_id(), 'wpr_selected_brands', true);

$category_ids = [];

$brands_args = [
    'post_status' => 'publish',
    'posts_per_page' => -1,
    'post__in' => $ids,
];
$my_saved_dealers = get_posts( $brands_args );

foreach ( $my_saved_dealers as $my_saved_dealer ) {
    $category_id = get_the_category( $my_saved_dealer->ID )[0]->term_id;

    if( !in_array($category_id, $category_ids, true) ){
        array_push( $category_ids, $category_id );
    }
}

$brandsArr = get_terms(array(
    'include' => $category_ids,
));

?>
<div class="content-header">
    <div class="title-wrapper">
        <h1 class="title">My Lookbook</h1>
    </div>
    <div class="filter-container">
        <h2>SORT BY BRAND</h2>
        <select id="brand-filter" multiple="multiple" class="regular-text">
            <option value="">All Brands</option>
            <?php
            foreach ($brandsArr as $brandSingle) {
                if (isset($_GET['brand_id']) && $_GET['brand_id'] != '' && $_GET['brand_id'] != 'null') {
                    $brandsList = explode(',', $_GET['brand_id']);
                    if (in_array($brandSingle->term_id, $brandsList)) {
                        $selection = 'selected';
                    } else {
                        $selection = '';
                    }
                } else {
                    $selection = '';
                }
                if ($brandSingle->term_id == 36 || $brandSingle->term_id == 37 || $brandSingle->term_id == 38 || $brandSingle->term_id == 39) :
                else:
                    echo '<option value="' . $brandSingle->term_id . '" ' . $selection . '>' . $brandSingle->name . '</option>';
                endif;
            }
            ?>
        </select>
    </div>
</div>
<div class="masonry-wrap js-masonry-wrap">
    <?php
    if (isset($_GET['brand_id']) && $_GET['brand_id'] != '' && $_GET['brand_id'] != 'null') {

        $arrr = explode(",", $_GET['brand_id']);

        $args1 = array(
            'post_status' => 'publish',
            'posts_per_page' => -1,
            'post__in' => $ids,
            'cat' => $arrr,
        );

        $query1 = new WP_Query($args1);

        if ($query1->have_posts()) {
            while ($query1->have_posts()) {
                $query1->the_post();
                echo '<div class="post-box-wrap" id="post-box-' . get_the_ID() . '">';
                get_template_part('partials/content', get_post_format());
                echo '</div>';
            }

            wp_reset_postdata();
        }

    } else {

        if ($ids && count($ids) !== 0) {
        $args = array(
            'post_status' => 'publish',
            'posts_per_page' => -1,
            'post__in' => $ids
        );

        $query = new WP_Query($args);
        ?>
        <div class="post-wrapper">
            <?php

            $posts = $query->posts;

            if ($query->have_posts()) {
                while ($query->have_posts()) {
                    $query->the_post();
                    echo '<div class="post-box-wrap" id="post-box-' . get_the_ID() . '">';
                    get_template_part('partials/content', get_post_format());
                    echo '</div>';
                }

                wp_reset_postdata();
            }
        } else {
            echo '<h3 style="padding:0 17px;margin-top: 40px;">You don\'t have any offers saved.</h3>';
        }

    } ?>
    </div>
</div>
