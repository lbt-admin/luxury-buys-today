<?php
/**
 * Template name: Iframe Offer Template
 *
 * @package Novus
 */

?>

<html>
	<head>
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css">
		<link href="<?php echo get_stylesheet_directory_uri(); ?>/assets/css/iframe.css"
			rel="stylesheet" type="text/css" media="all">
		<link href="<?php echo plugins_url(); ?>/wpr-dealers-location/assets/css/sweetalert2.min.css"
			rel="stylesheet" type="text/css" media="all">

		<script
			src="https://code.jquery.com/jquery-3.3.1.min.js"
			integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
			crossorigin="anonymous"></script>
			<style>
				a{text-decoration:none !important;}
				.cta_buttons{
					z-index:99;
				}
				.fa-spin{
					color:#fff;
				}
			</style>
	</head>
	<body>
		<div class="wrap-content 1212" id="wrap-content">
		<?php
		global $post;
		$post = get_post( $_GET['post_id'], OBJECT );
		setup_postdata( $post );

		$category    = get_the_category( get_the_ID() );
		$category_id = $category[0]->term_id;
		$name        = $category[0]->name;

		$is_ba_offer = get_post_meta( get_the_ID(), 'wpr_dealer_id', true );
		if ( $is_ba_offer && has_post_thumbnail() ) {
			printf(
				'<div class="wpr-custom-thumbnail iframe-media">%s</div>',
				get_the_post_thumbnail()
			);
		}

		$twitter_video = get_post_meta( $post->ID, 'wpr_twitter_video', true );
		if ( $twitter_video ) :
			$original_url = get_post_meta( $post->ID, 'original_link', true );
			$twitter_id   = explode( '/', $original_url );
			$twitter_id   = end( $twitter_id );
			?>
			<div class="twitter-video-embed iframe-media">
				<iframe src="https://twitter.com/i/videos/tweet/<?php echo esc_attr( $twitter_id ); ?>?embed_source=clientlib&amp;player_id=0&amp;rpc_init=1&amp;autoplay=1&amp;language_code=en&amp;use_syndication_guest_id=true"
						allowfullscreen="true"
						allow="autoplay; fullscreen"
						id="player_tweet_<?php echo esc_attr( $twitter_id ); ?>"></iframe>
			</div>
			<?php
		endif;

		$twitter_video_gif = get_post_meta( $post->ID, 'wpr_twitter_video_mp4', true );
		if ( $twitter_video_gif ) :
			$twitter_video_thumbnail = get_the_post_thumbnail( $post->ID );
			?>
			<div class="iframe-media">
				<video controls
						width="100%"
						height="360">
					<source src="<?php echo esc_attr( $twitter_video_gif ); ?>"
							type="video/mp4"/>
					<object type="application/x-shockwave-flash"
							data="http://player.longtailvideo.com/player.swf"
							width="640"
							height="360">
						<param name="movie"
								value="http://player.longtailvideo.com/player.swf"/>
						<param name="allowFullScreen"
								value="true"/>
						<param name="wmode"
								value="transparent"/>
					</object>
				</video>
			</div>
			<?php
		endif;
		?>
		<?php
			preg_match_all( '((?:www\.)?(?:youtube(?:-nocookie)?\.com\/(?:v\/|watch\?v=|embed\/)|youtu\.be\/)([a-zA-Z0-9?&=_-]*))', get_the_content(), $urls );
			$urls = array_filter( $urls ); ?>
			<?php if ( false !== strpos( $urls[0][0], 'youtube') ) : ?>
				<style>
				.embed-container { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; margin-bottom: 14px;} .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
				</style>
				<div class='embed-container iframe-media'>
					<iframe src='https://www.youtube.com/embed/<?php echo esc_url( $urls[1][0] ); ?>' frameborder='0' allowfullscreen></iframe>
				</div>

				<div style="margin-bottom: 20px;">
					<?php echo wp_kses( get_the_content(), array() ); ?>
				</div>
				<?php $ylink = '<div><a href="https://www.youtube.com/watch?v=' . esc_url( $urls[1][0] ) . '" target="_blank"><u>Source</u></a></div>'; ?>
				<?php echo wp_kses_post( str_replace( array( '[ad_1]', '[ad_2]', '&nbsp;' ), '', $ylink ) ); ?>
			<?php else : ?>
				<div class="post-content">
				<?php
					$content = str_replace( array( '[ad_1]', '[ad_2]', '&nbsp;' ), '', get_the_content() );
					echo apply_filters( 'lbt_xml_promotions_content', $content );
				?>
				</div>
			<?php endif; ?>
		<script type="text/javascript">
			var nonceVar='<?php echo esc_attr( wp_create_nonce( 'wpr-dealer-location' ) ); ?>';
			var ajaxURLVar='<?php echo esc_url( admin_url( 'admin-ajax.php', 'https://' ) ); ?>';
			<?php $popup_facebook = 'https://luxurybuystoday.com/wp-content/plugins/wpr-dealers-location/assets/images/popup-facebook.png'; ?>
			var facebookURLVar='<?php echo sprintf( '<img src="%s" /> Please like us on Facebook', esc_url( $popup_facebook ) ); ?>';
			var isLoggedIn='<?php echo (bool) is_user_logged_in(); ?>';
			$('#wrap-content a').each(function() {
				var a = new RegExp('/' + window.location.host + '/');
				if (!a.test(this.href)) {
					jQuery(this).attr("target","_blank");
				}
			});
		</script>
		<script src="<?php echo plugins_url(); ?>/wpr_tower_data/assets/js/sweetalert2.min.js" type="application/javascript"></script>
		<script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/datatables.min.js" type="application/javascript"></script>
	</body>
</html>

