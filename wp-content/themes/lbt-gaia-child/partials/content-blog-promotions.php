<?php
/**
 * Blog Post Promotions template Large
 *
 * @package LBT
 */

$remote_promotion = $args['remote_promotion'] ?? array();

if ( ! empty( $remote_promotion ) ) {
	if ( is_a( $remote_promotion, '\WP_Post' ) ) {
		global $post;
		$old_post = $post;
		$post     = $remote_promotion;
		setup_postdata( $remote_promotion );
	}
}

$thumbnail = \LBT\Post::get_thumbnail( get_the_ID() );

?>

<a href="<?php the_permalink(); ?>" class="post-list-item">
	<?php if ( $thumbnail ) : ?>
		<figure class="featured-image">
			<img src="<?php echo esc_url( $thumbnail ); ?>" class="image-cover">
		</figure>
	<?php endif; ?>
	<div class="content">
		<h4 class="post-title"><?php the_title(); ?></h4>
		<?php echo wp_kses_post( substr( wp_strip_all_tags( do_shortcode( $post->post_content ) ), 0, 120 ) ); ?> <span class="read-more">Read more...</span>
	</div>
</a>
<?php
if ( ! empty( $remote_promotion ) ) {
	if ( is_a( $remote_promotion, '\WP_Post' ) ) {
		global $post;
		$post = $old_post;
		setup_postdata( $post );
	}
}
?>
