<?php
/**
 * Blog post list template footer
 *
 * @package LBT
 */

$category    = get_the_category( get_the_ID() );
$category_id = $category[0]->term_id;

?>
<footer class="entry-footer">
	<div>
		<?php if ( 'post' === get_post_type() ) : ?>
			<div class="entry-meta wpr-entry-meta-archive">
				<?php wpr_entry_meta(); ?>
			</div><!-- .entry-meta -->
		<?php endif; ?>
		<div class="entry-summary wpr-article-source-archive">
			<?php
			$article_url = \LBT\Post::get_post_source( get_the_ID() );

			if ( $article_url ) {
				echo '<a href="' . esc_url_raw( $article_url ) . '" rel="dofollow" target="_blank">Source Link</a>';
			} else {
				$wpr_brand_catalog = get_term_meta( $category_id, 'wpr_brand_catalog', true );

				if ( $wpr_brand_catalog ) {
					echo '<a href="' . esc_url_raw( $wpr_brand_catalog ) . '" rel="dofollow" target="_blank">Source Link</a>';
				}
			}
			?>
		</div>
	</div>
	<div class="entry-summary mix">
		<strong class="post-date"><?php novus_posted_on( false ); ?></strong>
		<?php the_excerpt(); ?>
	</div>
	<?php if ( ! \LBT\Category::is_broyhill_category( $category_id ) ) : ?>
	<div class="archieve-cust-boxes">
		<?php novus_entry_footer( false ); ?>
		<?php get_template_part( 'partials/content', 'post-rating' ); ?>
		<?php
		if ( shortcode_exists( 'apss_share' ) ) {
			echo do_shortcode( "[apss_share theme='3' counter='1']" );
		}
		?>
	</div>
	<?php endif; ?>
</footer><!-- .entry-footer -->
