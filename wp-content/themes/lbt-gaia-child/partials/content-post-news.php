<?php
/**
 * Today news template
 *
 * @package Novus
 */

$edgar_cats = \LBT\Constants::BROYHILL_CHILD_CATEGORIES;

?>

<article id="post-<?php the_ID(); ?>"
	<?php
		in_category( $edgar_cats, get_the_ID() ) && ! is_category( $edgar_cats ) ? post_class( 'wpr-display-shadow hentry--masonry entry-box-shadow black-box' ) : post_class( 'hentry--masonry ' . $daily_promo . ' entry-box-shadow black-box' );
	?>
	>
	<div class="entry-wrap">
		<?php
		get_template_part( 'partials/content', 'post-top' );
		get_template_part( 'partials/content', 'post-title' );
		get_template_part( 'partials/content', 'post-image' );
		get_template_part( 'partials/content', 'post-content' );
		get_template_part( 'partials/content', 'post-footer' );
		?>
	</div>
</article><!-- #post-## -->
