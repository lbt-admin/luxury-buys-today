<?php
/**
 * Dealer info box template
 *
 * @package Novus
 */

$dealer       = $args['dealer'] ?? '';
$brand_id     = $args['brand_id'] ?? 0;
$blog_post_id = $args['post_id'] ?? 0;
$is_single    = $args['is_single'] ?? false;

if ( empty( $dealer ) ) {
	return;
}

$store_zip_code = '';
$address        = '';

$brand_fb_url = $dealer['f2817'];
$catalog_url  = get_term_meta( $brand_id, 'wpr_brand_catalog', true );
// $brand_fb_url = get_term_meta( $brand_id, 'wpr_brand_link_text', true );

if ( ! empty( $dealer['f2108'] ) ) {
	$store_zip_code = $dealer['f2108'];
}

if ( ! empty( $dealer['f1716'] ) ) {
	$store_address .= $dealer['f1716'] . ', ';
	$google_map    .= $dealer['f1716'] . ', ';
}

if ( ! empty( $dealer['f1718'] ) ) {
	$store_address .= $dealer['f1718'] . ', ';
	$google_map    .= $dealer['f1718'] . ', ';
	$dealer_city    = $dealer['f1718'];
}

if ( ! empty( $dealer['f2107'] ) ) {
	$store_address .= $dealer['f2107'] . ', ';
	$google_map    .= $dealer['f2107'] . ' ' . $store_zip_code;
	$dealer_state   = $dealer['f2107'];
}

if ( ! empty( $dealer['f2933'] ) ) {
	$phone_number = $dealer['f2933'];
}

if ( ! empty( $store_zip_code ) ) {
	$address = $google_map;
} else {
	$address = substr( $google_map, 0, -2 );
}

$profile_image = ! empty( $dealer['contact_data']['f1739'] ) && false !== filter_var( $dealer['contact_data']['f1739'], FILTER_VALIDATE_URL ) && \LBT\Post::remote_file_exists( $dealer['contact_data']['f1739'] ) ? $dealer['contact_data']['f1739'] : 'https://luxurybuystoday-media.s3.amazonaws.com/media/uploads/2020/07/img-avatar-png-png.png';

$user_brands = array();
$brand_saved = false;

if ( is_user_logged_in() ) {
	$user_brands = \LBT\Brand::get_user_brands();
	$brand_saved = in_array( $dealer['id'], $user_brands, true );
}

?>

<div id="wpr-dealer-location" <?php echo $is_single ? 'data-post-id="' . esc_attr( $blog_post_id ) . '"' : ''; ?> data-dealership-id="<?php echo esc_attr( $dealer['id'] ); ?>" data-brand-id="<?php echo esc_attr( $brand_id ); ?>" class="widget-content wertyuiop">
	<div id="zm-profile-description" class="zm-dealer-details-single main-brand-page-wrap custom-new <?php echo $is_single ? 'lbt-dealer-info-box-single' : ''; ?>"
		data-dealership-title="<?php echo esc_attr( $dealer['f1714'] ); ?>"
		data-dealership-address="<?php echo esc_attr( $address ); ?>"
		data-dealership-contact="<?php echo esc_attr( $phone_number ); ?>"
		data-dealership-state="<?php echo esc_attr( $dealer_state ); ?>"
		data-dealership-city="<?php echo esc_attr( $dealer_city ); ?>">
		<div class="zm-profile-description main-brand-page">
			<div class="closest-store">YOUR CLOSEST STORE</div>
			<div class="zm-profile-media">
				<div class="profile-image">
					<img src="<?php echo esc_url( $profile_image ); ?>" alt="<?php echo esc_attr( $dealer['f1714'] ); ?>">
				</div>
				<?php
				if ( $is_single ) :
					$brand_image = get_term_meta( $brand_id, 'wpr_brand_image_url', true );

					if ( ! empty( $brand_image ) ) :
						?>
						<div class="brand-logo">
							<img src="<?php echo esc_url( $brand_image ); ?>" alt="<?php echo esc_attr( get_cat_name( $brand_id ) ); ?>">
						</div>
						<?php
					endif;
				endif;
				?>
			</div>
			<div class="zm-dealer-name">
				<h2 class="zm-profile-title" data-dealership-id="<?php echo esc_attr( $dealer['id'] ); ?>">
					<?php echo esc_html( $dealer['f1714'] ); ?>
				</h2>
			</div>
			<div class="store-location"><?php echo esc_html( $address ); ?></div>
			<?php if ( ! empty( $dealer['contact_data']['firstname'] ) || ! empty( $dealer['contact_data']['sms_number'] ) || ! empty( $dealer['contact_data']['email'] ) ) : ?>
			<div class="lbt-dealer-contact-info">
				<?php if ( ! empty( $dealer['contact_data']['firstname'] ) ) : ?>
					<div class="lbt-dealer-contact-name"><?php echo esc_html( $dealer['contact_data']['firstname'] . ' ' . $dealer['contact_data']['lastname'] ); ?></div>
				<?php endif; ?>
				<?php if ( ! empty( $dealer['contact_data']['sms_number'] ) ) : ?>
				<div class="lbt-dealer-contact-phone">
					<a href="tel:<?php echo esc_attr( preg_replace( '/ |-/', '', $dealer['contact_data']['sms_number'] ) ); ?>"><?php echo esc_html( $dealer['contact_data']['sms_number'] ); ?></a>
				</div>
				<?php endif; ?>
				<?php if ( ! empty( $dealer['contact_data']['email'] ) ) : ?>
				<div class="lbt-dealer-contact-email">
					<a href="mailto:<?php echo esc_attr( $dealer['contact_data']['email'] ); ?>"><?php echo esc_html( $dealer['contact_data']['email'] ); ?></a>
				</div>
				<?php endif; ?>
			</div>
			<?php endif; ?>
		</div>
		<div class="zm-profile-description main-brand-page">
			<div class="zm-cta-actions">
				<div class="zm-contact-me lbt-dealer-cta-request-btn lbt-cta-request-button" data-action="contact">
					<i class="fa icon-phone" aria-hidden="true"></i>call me
				</div>
				<div class="lbt-appointment-button" data-action="appointment">
					<i class="fa icon-calendar"></i>
					<span class="long">make an appt</span>
					<span class="short">appt</span>
				</div>
				<div class="zm-info lbt-dealer-cta-request-btn lbt-cta-request-button" data-action="learn">
					<i class="fa icon-info" aria-hidden="true"></i>
					<span class="long">request more info</span>
					<span class="short">info</span>
				</div>
				<div class="zm-cta-btn lbt-cta-request-button <?php echo ! $brand_saved ? 'lbt-toggle-brand-btn' : 'lbt-brand-saved'; ?>">
					<i class="fa fa-heart <?php echo $brand_saved ? 'red' : ''; ?>" aria-hidden="true"></i>
					<span class="long">
						<?php
						if ( $brand_saved ) {
							esc_html_e( 'Saved', 'lbt' );
						} else {
							esc_html_e( 'Save this brand', 'lbt' );
						}
						?>
					</span>
					<span class="short">
						<?php
						if ( $brand_saved ) {
							esc_html_e( 'Saved', 'lbt' );
						} else {
							esc_html_e( 'Save', 'lbt' );
						}
						?>
					</span>
				</div>
				<span class="pumOpen" id="pumOpen" style="display: none;"></span>
			</div>
			<div class="zm-dealer-details">
				<?php if ( ! empty( $google_map ) ) : ?>
				<div class="zm-dealer-map">
					<i class="fa fa-map-marker"></i>
					<a href="https://www.google.com/maps?q=<?php echo esc_attr( $google_map ); ?>" target="_blank" class=" wpr-map-location" data-do="map">Directions</a>
				</div>
				<?php endif; ?>
				<?php if ( ! empty( $brand_fb_url ) ) : ?>
				<div class="zm-dealer-facebook">
					<i class="fa fa-facebook"></i>
					<a href="<?php echo esc_attr( $brand_fb_url ); ?>" target="_blank">Facebook</a>
				</div>
				<?php endif; ?>
				<div class="zm-dealer-phone zm-cta-btn lbt-dealer-cta-request-btn" data-action="contact">
					<i class="fa icon-cell-phone" aria-hidden="true"></i>
					<span>Request call back</span>
				</div>
				<?php if ( ! empty( $catalog_url ) ) : ?>
				<div class="zm-view-brand-catalog">
					<i class="fa icon-share"></i>
					<a href="<?php echo esc_url( $catalog_url ); ?>" target="_blank" class="wpr-brand-catalog">Brand Catalog </a>
				</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
</div>
