<?php
/**
 * Blog post big article template
 *
 * @package LBT
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class( 'hentry--masonry' ); ?> data-post-id="<?php echo esc_attr( get_the_ID() ); ?>">
	<div class="entry-wrap">
		<?php
			get_template_part( 'partials/content', 'post-top' );
			the_title( sprintf( '<h1 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h1>' );
			get_template_part( 'partials/content', 'post-image' );
			get_template_part( 'partials/content', 'post-content' );
			get_template_part( 'partials/content', 'post-footer' );
		?>
	</div>
</article>
