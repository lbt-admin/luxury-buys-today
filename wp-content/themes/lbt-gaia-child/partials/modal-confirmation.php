<?php
/**
 * Modal: Confirmation
 *
 * @package Novus
 */

?>
<div class="confirm_remove_pop remove_brand">
	<div class="login_register_wrap">
		<div class="login_register_box">
			<div class="modal_header">
				<img src="https://luxurybuystoday-media.s3.amazonaws.com/media/uploads/2020/01/login-logo-png.png" alt="" />
				<button type="button" class="login-close" aria-label="Close">×</button>
			</div>
			<div class="zm-login-form">
				<p>Please confirm you would like to remove this Brand.</p>
				<button class="confirm-remove">Confirm</button>
				<button class="cancel-remove">Cancel</button>
			</div>
		</div>
	</div>
</div>
