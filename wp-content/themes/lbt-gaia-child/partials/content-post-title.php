<?php
/**
 * Blog post big article template
 *
 * @package LBT
 */

?>

<header class="entry-header" data-deal-id="<?php esc_attr( the_ID() ); ?>">

</header><!-- .entry-header -->
<?php the_title( sprintf( '<h1 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h1>' ); ?>
