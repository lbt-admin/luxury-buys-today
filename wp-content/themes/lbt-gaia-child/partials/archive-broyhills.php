<?php
/**
 * Broyhill Archive template
 *
 * @package Novus
 */

$current_category = $args['current_category'] ?? get_queried_object();

?>
<div class="archive-top lbt-broyhill-category <?php echo ! $current_category->category_parent ? 'parent' : ''; ?>">
	<div class="grid-wrapper grid">
		<div class="gt-box category">
			<div class="item">
				<?php if ( is_user_logged_in() ) : ?>
					<div class="category-desc">
						<h3>Luxury Buys Today - <?php echo esc_html( $current_category->name ); ?> News</h3>
						<?php echo category_description( $current_category->category_parent ); ?>
					</div>
				<?php else : ?>
					<div class="login-box">
						<h2>Looking for more <?php echo esc_html( $current_category->name ); ?> news?</h2>
						<button class="btn btn-black lbt-show-register-form">Create Your account</button>
						<p>Already have an account? <a href="javascript:void(0);" class="login_register
						lbt-show-login-form">Sign in</a></p>
					</div>
				<?php endif; ?>
				<?php get_template_part( 'partials/ed-info' ); ?>
			</div>
		</div>
	</div>
</div>
<h3 class="main-title"><span><?php echo esc_html( $current_category->name ); ?></span> Offers</h3>
<div id="primary" class="content-area <?php echo ! $current_category->category_parent ? 'parent' : ''; ?> ">
	<main id="main" class="site-main" role="main">
		<?php
		do_action( 'novus_loop_before' );
		while ( have_posts() ) :
			the_post();
			get_template_part( 'partials/content', 'todays_news' );
		endwhile;
		do_action( 'novus_loop_after' );
		?>
	</main>
</div>
