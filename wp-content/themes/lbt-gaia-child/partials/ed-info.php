<?php
/**
 * Ed Info box
 *
 * @package Novus
 */

?>

<div class="ed-info content-wrapper" style="padding: 17px;
	-webkit-box-shadow: 0px 2px 5px 5px rgb(0 0 0 / 30%);
	-moz-box-shadow: 0px 2px 5px 5px rgba(0, 0, 0, 0.3);
	box-shadow: 0px 2px 5px 5px rgb(0 0 0 / 30%);
	background: #fff;
	border: #000 solid 2px !important;">
	<div class="text-center">
		<img src="<?php echo esc_url( LBT_THEME_URL ) . 'assets/images/Ed-Broyhill-light-circle.jpg'; ?>"/>
		<h3>About Ed Broyhill</h3>
		<p align="left">
			From a family of luxury furniture icons, Edgar (Ed) Broyhill is the mastermind behind introducing the at-home catalog experience to American shoppers. By introducing the finest home furnishings to the United States, quality and reliable pieces have been fawned over by stars, fashion icons, and even presidents. The Broyhill name has been mentioned in TV interviews - including Oprah Winfrey - and magazines such as Architectural Digest. Now, highlighting the life of quintessential essences through Luxury Buys Today, Ed introduces a unique shopping opportunity to high-style brands and companies in the Automotive, Home Decor, Fashion, and Home Property industries through Luxury Buys Today’s local Authorized Brand Consultants.
		</p>
	</div>
</div>
