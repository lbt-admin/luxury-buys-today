<?php
/**
 * Modal: CTA
 *
 * @package Novus
 */

?>

<div class="openCTApopup" style="width:1px; visibility:hidden; position:absolute; opacity:0;"></div>
<div class="cta_overlay_wrap">
	<div class="cta_pop_wrap">
		<div class="cta_pop_box">
			<div class="cta_pop_header">
				<img src="/wp-content/themes/lbt-gaia-child/assets/images/popover-logo.png" alt="" />
				<h2 style="color:#fff;margin-left:10px;">Thanks!</h2>
				<button type="button" class="CTA-close header-btn" aria-label="Close">×</button>
			</div>
			<p>Thank you for your interest, we will be in touch soon.</p>
			<a href="https://www.facebook.com/pg/LuxuryBuysToday/posts/">
				<button class="swal2-cancel wpr-please-like-us swal2-styled" style="display: inline-block; background-color: transparent;" type="button"><img src="https://luxurybuystoday.com/wp-content/plugins/wpr-dealers-location/assets/images/popup-facebook.png">
					Please like us on Facebook
				</button>
			</a>
			<button class="CTA-close footer-btn" type="button" aria-label="Close">Close</button>
		</div>
	</div>
</div>
