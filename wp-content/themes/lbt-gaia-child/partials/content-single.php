<?php
/**
 * The single blog post template
 *
 * @package Novus
 */

$blog_post_id   = get_the_ID();
$favorite_count = get_post_meta( $blog_post_id, 'simplefavorites_count', true );

$category    = get_the_category( $blog_post_id );
$category_id = $category[0]->term_id;
$slug        = $category[0]->slug;

if ( empty( $favorite_count ) ) {
	$favorite_count = 0;
}

$user_favourites      = \LBT\Lookbook::get_user_lookbook();
$is_saved_to_lookbook = in_array( $blog_post_id, $user_favourites );
$user_offers          = \LBT\Offer::get_user_offers();
$is_saved_to_offers   = in_array( $blog_post_id, $user_offers );
$is_broyhill_category = \LBT\Category::is_broyhill_category( $category_id );

?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header" data-post-id="<?php the_ID(); ?>">
		<div class="wpr-single-brand-border"></div>
		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
		<span id="slugName" style="display:none;" data-slug="<?php echo esc_attr( $slug ); ?>"></span>
		<span class="pumOpen" id="pumOpen" style="display:none;"></span>
		<span class="pumOpen33647" id="pumOpen33647" style="display:none;"></span>
	</header>
	<!-- .entry-header -->

	<div class="wpr_single_content_border">
		<footer class="entry-footer">
			<div class="button-group">
				<div class="wpr-entry-meta">
					<?php
					$single = 'true';
					wpr_entry_meta( $single );
					?>
					<span class="cat-links"><?php novus_posted_on( false ); ?></span>
				</div>
				<div class="like-button">
					<button class="show-likes lbt-favorite-button lbt-toggle-favorite-post"
							data-postid="<?php echo esc_attr( $blog_post_id ); ?>"
							data-data-ctacount="<?php echo esc_attr( $favorite_count ); ?>">
								<?php if ( ! is_user_logged_in() ) : ?>
									<span>Like</span> <i class="fa fa-thumbs-o-up"></i>
								<?php else : ?>
									<?php if ( $is_saved_to_offers ) : ?>
										<span>Favorited</span> <i class="fa fa-star"></i>
									<?php else : ?>
										<span>Favorite</span> <i class="fa fa-star-o"></i>
									<?php endif; ?>
								<?php endif; ?>
					</button>
				</div>
				<div class="print-offer" onclick="window.print()">
					PRINT THIS OFFER <i class="fa fa-print" aria-hidden="true"></i>
				</div>
				<div class="shareLinkLocation">
					<?php
					// Share post links.
					if ( get_theme_mod( 'article_share_links', true ) && function_exists( 'wpr_share_post' ) ) {
						wpr_share_post();
					}
					?>
				</div>
			</div>
			<!-- .entry-meta -->
			<div class="postFromBox">A post from <?php echo esc_html( $category->name ); ?></div>
		</footer>
		<!-- .entry-footer -->

		<div class="entry-content">
			<?php
			$lookbook_img = '';
			if ( function_exists( 'get_favorite_as_image' ) ) {
				$lookbook_img = get_favorite_as_image();
				echo wp_kses_post( $lookbook_img );
			}
			?>
			<div class="iframe_content">
				<?php
				$promotion_param = '';
				$allowed         = array( 'christies-promotions', 'sothebys-promotions' );
				if ( in_array( $post->post_name, $allowed, true ) ) {
					$promotion_param = ! empty( $_GET['id'] ) ? '&promotion=' . sanitize_text_field( wp_unslash( $_GET['id'] ) ) : '';
				}

				if ( in_array( $post->ID, lbt_get_automotive_post_ids(), true ) ) {
					$promotion_param = ! empty( $_GET['id'] ) ? '&post_feed_id=' . sanitize_text_field( wp_unslash( $_GET['id'] ) ) : '';
				}

				?>
				<iframe id="contentFrame" src="/iframe-offer/?post_id=<?php the_ID(); ?><?php echo esc_url( $promotion_param ); ?>" scrolling="no">
				</iframe>
				<div class="wpr-contact-buttons" id="wpr_contact_inner">
					<?php if ( ! $is_broyhill_category ) : ?>
					<div class="zm-cta-actions">
						<div class="zm-price lbt-dealer-cta-request-btn" data-action="price"
								data-ctacount="<?php echo esc_attr( $favorite_count ); ?>">
								<i class="icon-money" aria-hidden="true"></i>Price it
						</div>
						<div class="zm-info lbt-dealer-cta-request-btn" data-action="learn"
								data-ctacount="<?php echo esc_attr( $favorite_count ); ?>"
								data-id="<?php the_ID(); ?>"><i class="icon-mail" aria-hidden="true"></i>Send me<br>info
						</div>
						<div class="zm-contact-me lbt-dealer-cta-request-btn" data-action="contact"
								data-ctacount="<?php echo esc_attr( $favorite_count ); ?>"
								data-id="<?php the_ID(); ?>"><i class="icon-phone" aria-hidden="true"></i>Contact<br>me
						</div>
						<div class="lbt-appointment-button" data-action="appointment"
								data-ctacount="<?php echo esc_attr( $favorite_count ); ?>"
								data-id="<?php the_ID(); ?>"><i class="icon-calendar"></i>Make<br>APPOINTMENT
						</div>
					</div>
					<?php endif; ?>
					<?php
					if ( is_user_logged_in() ) {
						if ( $is_saved_to_lookbook ) {
							$save_brand_btn = '<div class="zm-save-favorites1 zm-fav-btn lbt-toggle-lookbook-post" remove-favorite="active" data-postid="' . $blog_post_id . '" data-siteid="1" data-groupid="1" data-favoritecount="' . $favorite_count . '"><span>REMOVE</span> FROM LOOKBOOK</div>';
						} else {
							$save_brand_btn = '<div class="zm-save-favorites1 zm-fav-btn lbt-toggle-lookbook-post" data-postid="' . $blog_post_id . '" data-siteid="1" data-groupid="1" data-favoritecount="' . $favorite_count . '"><span>SAVE</span> TO LOOKBOOK</div>';
						}

						echo wp_kses_post( $save_brand_btn );
						?>
					<?php } else { ?>
						<a href="#" class="zm-cta-btn wpr-single-offer-buttons wpr-add-to-lookbook1 lbt-toggle-lookbook-post"
							data-postid="<?php the_ID(); ?>"
							data-data-ctacount="<?php echo esc_attr( $favorite_count ); ?>"><span>SAVE</span> TO LOOKBOOK</a>
					<?php } ?>
				</div>
			</div>
			<?php
			wp_link_pages(
				array(
					'before' => '<div class="page-links">' . __( 'Pages:', 'novus' ),
					'after' => '</div>',
				)
			);
			?>
		</div>
		<!-- .entry-content -->


		<footer class="entry-footer" style="padding:10px;">
			<?php if ( ! $is_broyhill_category ) : ?>
				<?php if ( ! has_tag( \LBT\Constants::PROMOTIONS_TAG ) && ! in_array( $blog_post_id, array_values( lbt_get_automotive_post_ids() ), true ) ) : ?>
				<div class="clearfix boxWrap flexWrap">
					<div class="left-image">
						<img src="<?php echo esc_url( LBT_THEME_URL ); ?>assets/images/Ed-Broyhill-light-circle.jpg">
					</div>
					<div class="post-rating-offer">
						<?php get_template_part( 'partials/content', 'post-rating' ); ?>
					</div>
				</div>
				<?php endif; ?>
			<?php endif; ?>
			<div>
				<a href="<?php echo esc_url( home_url() ); ?>">
					<span class="what-i-like"><?php wpr_display_broyhill_comment_name( $category_id, $blog_post_id ); ?></span>
					<span class="what-i-like-text" data-raiting="<?php echo esc_attr( web_pigment_get_offer_raiting( $blog_post_id ) ); ?>">
					<?php
					if ( function_exists( 'wpr_display_ed_comment' ) ) {
						echo wp_kses_post( wpr_display_ed_comment( $blog_post_id ) );
					}
					?>
					</span>
				</a>
				<br/><br/>
			</div>
		</footer>
		<?php if ( get_theme_mod( 'author_info_display', true ) ) : ?>
			<?php get_template_part( 'partials/author-info' ); ?>
		<?php endif; ?>
</article><!-- #post-## -->

<div class="mobile-init-btn">
	<div class="loading-spinner">
		<div class="loading-spinner-inner">
			<div></div>
		</div>
	</div>
</div>
