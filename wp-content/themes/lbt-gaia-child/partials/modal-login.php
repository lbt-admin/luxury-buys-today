<?php
/**
 * Login modal
 *
 * @package LBT
 */

global $wp;

?>

<div id="lbt-login-form-overlay" class="lbt-login-form-hide"></div>
<section id="lbt-login-form-section" class="lbt-login-form-hide lbt-auth-form-section">
	<div class="lbt-login-form-header">
		<img src="<?php echo esc_url( LBT_THEME_URL . 'assets/images/login-logo.png' ); ?>" alt="Luxury Buys Today">
		<span class="lbt-login-form-close" aria-label="Close">×</span>
	</div>
	<div class="lbt-login-form-social">
		<?php
		if ( shortcode_exists( 'oa_social_login' ) ) {
			echo do_shortcode( '[oa_social_login]' );
		}
		?>
	</div>
	<div class="lbt-login-form">
		<div class="lbt-error-message"></div>
		<form method="POST" id="lbt-login-form">
			<div class="lbt-form-field-group">
				<label for="lbt-form-username"><?php esc_html_e( 'Email', 'lbt' ); ?></label>
				<input type="text" name="lbt_username" class="lbt-form-text-field" id="lbt-form-username" autocomplete="username">
			</div>
			<div class="lbt-form-field-group">
				<label for="lbt-form-password"><?php esc_html_e( 'Password', 'lbt' ); ?></label>
				<input type="password" name="lbt_password" class="lbt-form-text-field" id="lbt-form-password" autocomplete="current-password">
			</div>
			<div class="lbt-form-field-group">
				<input type="hidden" name="action" value="lbt_login">
				<input type="hidden" name="__nonce" value="<?php echo esc_attr( wp_create_nonce( 'lbt-login-form-nonce' ) ); ?>">
				<input type="hidden" name="redirect" value="<?php echo esc_url( home_url( $wp->request ) ); ?>">
				<button type="submit" class="lbt-form-submit-button" data-loading-text="<?php esc_attr_e( 'Processing...', 'lbt' ); ?>">
					<?php esc_html_e( 'Submit', 'lbt' ); ?>
				</button>
			</div>
		</form>
	</div>
	<div class="lbt-login-form-footer">
		<?php if ( ! is_page_template( 'template-signup.php' ) ) : ?>
		<div><strong><?php esc_html_e( 'Don’t have an account?', 'lbt' ); ?> <a href="#" class="lbt-form-switch"><?php esc_html_e( 'Sign up', 'lbt' ); ?></a></strong></div>
		<?php endif; ?>
		<div><a href="<?php echo esc_url( wp_lostpassword_url( '/' ) ); ?>"> <?php esc_html_e( 'Lost your password?', 'lbt' ); ?></a></div>
	</div>
</section>
