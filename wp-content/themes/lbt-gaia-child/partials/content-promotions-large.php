<?php
/**
 * Constants data
 *
 * @package LBT
 */

?>
<?php $class = $promotion['first'] ? 'blog-content' : 'hentry--masonry post-' . $promotion['ID'] . ' post type-post status-publish format-standard hentry category-christies-international masonry-brick'; ?>
<article id="post-<?php echo $promotion['ID']; ?>" class="<?php echo $class; ?>">
	<div class="entry-wrap">

		<header class="entry-header" data-deal-id="<?php echo $promotion['ID']; ?>">

		</header>
		<!-- .entry-header -->
		<h1 class="entry-title">
			<a href="<?php echo $promotion['target_href']; ?>?id=<?php echo $promotion['ID']; ?>" rel="bookmark">
				<?php echo $promotion['listing_item_name']; ?>
			</a>
		</h1>
		<?php if ( ! empty( $promotion['listing_image_url'] ) ) { ?>
			<div class="content-post-image">
				<a href="<?php echo $promotion['target_href']; ?>?id=<?php echo $promotion['ID']; ?>">
					<div class="entry-image">
						<img src="<?php echo $promotion['listing_image_url']; ?>" />
					</div>
				</a>
				<div class="save-to-favorites zm-cta-btn lbt-toggle-lookbook-post">SAVE TO LOOKBOOK</div>
			</div>
		<?php } ?>
		<div class="entry-summary custom-entry-summary ">
			<div class="wpr-cta">
				<span class="wrapButtons gridButtons">
					<div class="cta_buttons setCenter">
						<div class="inCenter">
							<div class="wpr-single-offer-buttons wpr-contact-me single_button  popmake-<?php echo $promotion['ID']; ?> " data-do="contact" data-postid="<?php echo $promotion['ID']; ?>">
								<div class="icon_container e">
									<img src="/wp-content/themes/lbt-gaia-child/assets/images/offer_grid_contact_me.svg" class="cta_icon" data-large_image_width="48" data-large_image_height="48">
								</div>
								<p>Contact</p>
							</div>
							<div class="wpr-single-offer-buttons wpr-send-info wpr-learn-more single_button  popmake-<?php echo $promotion['ID']; ?> " data-do="learn" data-postid="<?php echo $promotion['ID']; ?>">
								<div class="icon_container f">
									<img src="/wp-content/themes/lbt-gaia-child/assets/images/offer_grid_email_me.svg" class="cta_icon" data-large_image_width="48" data-large_image_height="48">
								</div>
								<p>Email Me</p>
							</div>
							<div class="wpr-single-offer-buttons wpr-make-appointment wpr-set-appointment single_button  popmake-<?php echo $promotion['ID']; ?> " data-do="appointment" data-postid="<?php echo $promotion['ID']; ?>">
								<div class="icon_container g">
									<img src="/wp-content/themes/lbt-gaia-child/assets/images/offer_grid_make_appointment.svg" class="cta_icon" data-large_image_width="48" data-large_image_height="48">
								</div>
								<p>Make Appt</p>
							</div>
							<div class="wpr-single-offer-buttons wpr-price-it wpr-price-request single_button  popmake-<?php echo $promotion['ID']; ?> " data-do="price" data-postid="<?php echo $promotion['ID']; ?>">
								<div class="icon_container h">
									<img src="/wp-content/themes/lbt-gaia-child/assets/images/offer_grid_price.svg" class="cta_icon" data-large_image_width="48" data-large_image_height="48">
								</div>
								<p>Price It</p>
							</div>
						</div>
					</div>
				</span>
			</div>
		</div>
		<footer class="entry-footer">
			<div class="test-tset">
				<div class="simplefavorite-wrap">
					<img src="/wp-content/themes/lbt-gaia-child/assets/images/add_to_lookbook_1.svg" class="simplefavorite-button bookmark" data-postid="<?php echo $promotion['ID']; ?>" data-siteid="1" data-groupid="1" data-favoritecount="0" title="Add To Lookbook" data-large_image_width="50" data-large_image_height="50">
					<img src="/wp-content/themes/lbt-gaia-child/assets/images/add_to_lookbook_red.svg" class="simplefavorite-button bookmark activated" data-postid="<?php echo $promotion['ID']; ?>" data-siteid="1" data-groupid="1" data-favoritecount="0" data-pagevisited="properties/christies-international" title="Remove From Lookbook" data-large_image_width="50" data-large_image_height="50">
				</div>
				<div class="entry-meta wpr-entry-meta-archive">
					<span class="cat-links">
						<b><?php echo $promotion['category']->name; ?> promotions</b>
					</span>
				</div>
				<div class="entry-summary wpr-article-source-archive">
					<a href="<?php echo $promotion['branch_location']; ?>" rel="dofollow" target="_blank">Source Link</a>
				</div>
			</div>
			<div class="entry-summary mix">
					<strong class="post-date">
						<span class="posted-on">
							<time class="entry-date published updated" datetime="2020-11-27T14:45:33+00:00">November 27, 2020</time>
						</span>
					</strong>
					<?php
					if ( $promotion['is_dealership'] ) {
						$listing_zip = isset( $listing_city['listing_zip'] ) ? ' ' . $listing_city['listing_zip'] : '';

						$location = $promotion['listing_city'] . ', ' . $promotion['listing_state'] . $listing_zip;
						$price    = number_format( $promotion['listing_price'], 0, '.', ',' );

						echo 'Location: ' . $location . '<br>';
						echo 'Price: $' . $price . '<br>';
					} else {
						echo lbt_the_excerpt_truncate( str_replace( 'Source link', '', strip_tags( do_shortcode( $promotion['content'] ) ) ), 30 );
					}
					?>
					<a href="<?php echo $promotion['target_href']; ?>?id=<?php echo $promotion['ID']; ?>" class="read-more-link">Read more...</a>
				</div>
			<?php
			global $is_eds;
			if ( ! $is_eds ) {
				?>
				<div class="archieve-cust-boxes">

					<?php novus_entry_footer( false ); ?>
					<?php get_template_part( 'partials/content', 'post-rating' ); ?>
					<?php
					if ( shortcode_exists( 'apss_share' ) ) {
						echo do_shortcode( "[apss_share theme='3' counter='1']" );
					}
					?>
				</div>
			<?php } ?>
		</footer>
	</div>
</article>
