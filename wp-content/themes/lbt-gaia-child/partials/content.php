<?php
/**
 * Default blog post content template
 *
 * @package Novus
 */

?>

<?php $article_layout = get_theme_mod( 'article_layout', 'classic' ); ?>
<?php if ( $article_layout == 'classic' || $article_layout == 'sidebar-right' || $article_layout == 'full-width' ) : ?>

    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<?php if ( has_post_thumbnail() && false === get_post_format() ) : ?>
			<?php
			$image_url       = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' );
			$post_thumb_size = ( $article_layout == 'full-width' ) ? 'post-thumb-full-width-cropped' : 'post-thumb-cropped';
			?>
            <div class="entry-image"><a href="<?php echo esc_url( $image_url[0] ); ?>"><?php the_post_thumbnail( $post_thumb_size ); ?></a></div>
		<?php endif; ?>

        <header class="entry-header">
			<?php if ( 'post' == get_post_type() ) : ?>
                <div class="entry-meta">
					<?php novus_entry_meta(); ?>
                </div><!-- .entry-meta -->
			<?php endif; ?>
			<?php the_title( sprintf( '<h1 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h1>' ); ?>
        </header><!-- .entry-header -->

        <footer class="entry-footer">
			<?php novus_posted_on(); ?>
			<?php novus_entry_footer(); ?>
        </footer><!-- .entry-footer -->

        <div class="entry-content">
			<?php
			/* translators: %s: Name of current post */
			the_content( sprintf(
				__( 'Continue reading %s <span class="meta-nav">&rarr;</span>', 'novus' ),
				the_title( '<span class="screen-reader-text">"', '"</span>', false )
			) );
			?>

			<?php
			wp_link_pages( array(
				'before' => '<div class="page-links">' . __( 'Pages:', 'novus' ),
				'after'  => '</div>',
			) );
			?>
        </div><!-- .entry-content -->

		<?php
		// Share post links
		if ( ( get_theme_mod( 'article_share_links', true ) && is_single() ) || ( get_theme_mod( 'archive_share_links', true ) && ! is_single() ) ) {
			novus_share_post();
		}
		?>

    </article><!-- #post-## -->

<?php elseif ( $article_layout == 'grid' ) : ?>

    <article id="post-<?php the_ID(); ?>" <?php post_class( 'hentry--grid' ); ?>>
        <div class="entry-wrap">
            <div class="entry-image">
				<?php if ( has_post_thumbnail() ) : ?>
					<?php the_post_thumbnail( 'post-thumb-cropped' ); ?>
				<?php else : ?>
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/post-thumb-cropped.svg" alt="thumbnail">
				<?php endif; ?>
            </div>
            <header class="entry-header">
				<?php if ( 'post' == get_post_type() ) : ?>
                    <div class="entry-meta">
						<?php novus_entry_meta(); ?>
                    </div><!-- .entry-meta -->
				<?php endif; ?>
				<?php the_title( sprintf( '<h1 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h1>' ); ?>
            </header><!-- .entry-header -->
            <footer class="entry-footer">
				<?php novus_posted_on(); ?>
				<?php novus_entry_footer( false ); ?>
            </footer><!-- .entry-footer -->
        </div>
    </article><!-- #post-## -->

<?php elseif ( $article_layout == 'masonry' || $article_layout == 'masonry-full' ) :
	$edgar_cats = array( '36', '37', '38', '39' );

    $post_count = get_query_var('post_count');
    $curr_cat = get_query_var('curr_cat');

    if ( $post_count === 0 && $curr_cat === 'child' ) {
        $daily_promo = 'daily-promotion';
        $promo_title = '';
    } else {
        $daily_promo = '';
        $promo_title = '';
    }
	?>

    <article id="post-<?php the_ID(); ?>" <?php if ( in_category( $edgar_cats, get_the_ID() ) && ! is_category( $edgar_cats ) ) {
		post_class( 'wpr-display-shadow hentry--masonry' );
	} else {
		post_class( 'hentry--masonry ' . $daily_promo );
	} ?>>
        <?php echo $promo_title; ?>
        <div class="entry-wrap">


			<?php
			get_template_part( 'partials/content', 'post-top' );
			get_template_part( 'partials/content', 'post-title' );
			get_template_part( 'partials/content', 'post-image' );
			include( locate_template( 'partials/content-post-content.php', false, false ) );
			get_template_part( 'partials/content', 'post-footer' );
			?>

        </div>
    </article><!-- #post-## -->

<?php else : // List view ?>

    <article id="post-<?php the_ID(); ?>" <?php post_class( 'flag hentry--list' ); ?>>
        <div class="flag-img">
            <div class="entry-image">
				<?php if ( has_post_thumbnail() ) : ?>
					<?php the_post_thumbnail( 'thumbnail' ); ?>
				<?php else : ?>
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/thumb.svg" alt="thumbnail">
				<?php endif; ?>
            </div>
        </div>
        <div class="flag-bd">
            <header class="entry-header">
				<?php if ( 'post' == get_post_type() ) : ?>
                    <div class="entry-meta">
						<?php novus_entry_meta(); ?>
                    </div><!-- .entry-meta -->
				<?php endif; ?>
				<?php the_title( sprintf( '<h1 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h1>' ); ?>
            </header><!-- .entry-header -->
            <footer class="entry-footer">
				<?php novus_posted_on(); ?>
				<?php novus_entry_footer(); ?>
            </footer><!-- .entry-footer -->
        </div>
    </article><!-- #post-## -->

<?php endif; ?>
