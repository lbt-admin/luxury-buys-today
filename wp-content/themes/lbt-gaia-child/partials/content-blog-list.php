<?php
/**
 * Blog post big article template
 *
 * @package LBT
 */

$post_thumbnail = \LBT\Post::get_thumbnail( get_the_ID(), 'full' );

?>
<a href="<?php echo esc_url( get_permalink() ); ?>" class="post-list-item">
	<figure class="featured-image">
		<img src="" data-test="1" onerror="this.src='<?php echo esc_url( $post_thumbnail ); ?>'; if ( this.getAttribute('retry') ) { if( this.getAttribute('retry') == 1 ) { this.src='<?php echo esc_url( $post_thumbnail ); ?>'; this.setAttribute( 'retry', 2 ); } else { this.src='<?php echo esc_url( LBT_THEME_URL ); ?>assets/images/post-thumb-cropped.svg'; } } else { this.setAttribute( 'retry', 1 ); }">
	</figure>
	<div class="content">
		<h4 class="post-title"><?php the_title(); ?></h4>
		<p>
			<?php
			$content   = wpr_display_ed_comment( get_the_ID() );
			$converted = strtr( $content, array_flip( get_html_translation_table( HTML_ENTITIES, ENT_QUOTES ) ) );
			$trimmed   = trim( $converted, chr( 0xC2 ) . chr( 0xA0 ) );

			echo wp_kses_post( wp_trim_words( $trimmed, 13, '' ) );
			?>
			<span class="read-more">Read more...</span>
		</p>
	</div>
</a>
