<a href="<?php echo $promotion['target_href']; ?>?id=<?php echo $promotion['ID']; ?>" class="post-list-item">
	<?php if ( ! empty( $promotion['listing_image_url'] ) ) { ?>
		<figure class="featured-image">
			<img src="<?php echo $promotion['listing_image_url']; ?>" class="image-cover">
		</figure>
	<?php } ?>
	<div class="content">
		<h4 class="post-title"><?php echo $promotion['listing_item_name']; ?></h4>
		<?php if( $promotion['is_dealership'] ) { ?>
			<p>
				<?php
				$listing_zip = isset( $listing_city['listing_zip'] ) ? ' ' . $listing_city['listing_zip'] : '';

				$location = $promotion['listing_city'] . ', ' . $promotion['listing_state'] . $listing_zip;
				$price = number_format( $promotion['listing_price'], 0, '.', ',' );

				echo 'Location: ' . $location . '<br>';
				echo 'Price: $' . $price . '<br>';
				?>
				<span class="read-more">Read more...</span>
			</p>
		<?php } else { ?>
			<?php echo lbt_the_excerpt_truncate( str_replace( 'Source link', '', strip_tags( do_shortcode( $promotion['content'] ) ) ), 30 ); ?> <span class="read-more">Read more...</span>
		<?php } ?>
	</div>
</a>
