<?php
$user = wp_get_current_user();
$user_id = $user->ID;
$user_meta = get_user_meta( $user->ID );

$email = $user->user_email;
$user_name = $user->user_login;
$first_name = get_user_meta( $user_id, 'first_name', true );
$last_name = get_user_meta( $user_id, 'last_name', true );
$phone = get_user_meta( $user_id, 'phone', true );
$avatar = get_user_meta( $user_id, 'lbt_user_avatar', true );
if ( empty( $avatar ) ) {
    $avatar = get_user_meta( $user_id, 'oa_social_login_user_picture', true );
}
$background = get_user_meta( $user_id, 'profile_background', true );

if ( $first_name !== '' && $last_name !== '' ) {
    $display_name = $first_name . ' ' . $last_name;
} else {
    $display_name = $user_name;
}

switch ( $background ) {
    case 'option_1':
        $bg_url = 'https://luxurybuystoday-media.s3.amazonaws.com/media/uploads/2020/10/luxury-goldcard-stripe.jpg';
        break;
    case 'option_2':
        $bg_url = 'https://luxurybuystoday-media.s3.amazonaws.com/media/uploads/2020/10/luxury-white-leather.jpg';
        break;
    case 'option_3':
        $bg_url = 'https://luxurybuystoday-media.s3.amazonaws.com/media/uploads/2020/10/luxury-white-marble.jpg';
        break;
    case 'option_4':
        $bg_url = 'https://luxurybuystoday-media.s3.amazonaws.com/media/uploads/2020/10/luxury-gold-lights.jpg';
        break;
    default :
        $bg_url = 'https://luxurybuystoday-media.s3.amazonaws.com/media/uploads/2020/10/luxury-goldcard-stripe.jpg';
}
?>
<div class="hero-header" style="background-image: url('<?php echo $bg_url; ?>')"></div>
<div class="user-profile">
    <div class="profile-image">
        <?php if ( !empty( $avatar ) ) : ?>
            <img src="<?php echo $avatar; ?>" alt="<?php echo $user_name; ?>">
        <?php else : ?>
            <img src="https://luxurybuystoday-media.s3.amazonaws.com/media/uploads/2020/10/user-image-with-black-background-png-1.png" alt="">
        <?php endif; ?>
        <?php if ( is_page_template('templates/tmp-profile.php') ) : ?>
        <div class="btn-change" data-action="avatar-popup">Change</div>
        <?php endif; ?>
    </div>
    <div class="user-name"><?php echo $display_name; ?></div>
    <?php if ( $email !== '' || $phone !== '' ) : ?>
        <div class="user-contact-info">
            <?php if ( $email !== '' ) : ?>
                <div class="info-box email">
                    <svg xmlns="http://www.w3.org/2000/svg" width="66.24" height="45.858" viewBox="0 0 66.24 45.858">
                        <g id="Icon_ionic-ios-mail" data-name="Icon ionic-ios-mail" transform="translate(-3.375 -7.875)">
                            <path id="Path_1" data-name="Path 1" d="M69.073,10.412,51.94,27.863a.308.308,0,0,0,0,.446l11.99,12.77a2.066,2.066,0,0,1,0,2.93,2.076,2.076,0,0,1-2.93,0L49.058,31.287a.327.327,0,0,0-.462,0l-2.914,2.962a12.82,12.82,0,0,1-9.14,3.853,13.078,13.078,0,0,1-9.331-3.965l-2.8-2.85a.327.327,0,0,0-.462,0L12.005,44.009a2.076,2.076,0,0,1-2.93,0,2.066,2.066,0,0,1,0-2.93l11.99-12.77a.339.339,0,0,0,0-.446L3.916,10.412a.314.314,0,0,0-.541.223V45.554a5.11,5.11,0,0,0,5.1,5.1H64.519a5.11,5.11,0,0,0,5.1-5.1V10.635A.319.319,0,0,0,69.073,10.412Z" transform="translate(0 3.084)"/>
                            <path id="Path_2" data-name="Path 2" d="M35.419,37.03a8.657,8.657,0,0,0,6.226-2.611L66.628,8.99a5,5,0,0,0-3.153-1.115H7.379A4.971,4.971,0,0,0,4.226,8.99L29.209,34.419A8.658,8.658,0,0,0,35.419,37.03Z" transform="translate(1.076 0)"/>
                        </g>
                    </svg>
                    <span><?php echo $email; ?></span>
                </div>
            <?php endif; ?>
            <?php if ( $phone !== '' ) : ?>
                <div class="info-box phone">
                    <svg xmlns="http://www.w3.org/2000/svg" width="38.325" height="64.858" viewBox="0 0 38.325 64.858">
                        <path id="Icon_material-phone-iphone" data-name="Icon material-phone-iphone" d="M38.455,1.5H14.87A7.373,7.373,0,0,0,7.5,8.87V58.988a7.373,7.373,0,0,0,7.37,7.37H38.455a7.373,7.373,0,0,0,7.37-7.37V8.87A7.373,7.373,0,0,0,38.455,1.5ZM26.663,63.41a4.422,4.422,0,1,1,4.422-4.422A4.416,4.416,0,0,1,26.663,63.41ZM39.929,51.618H13.4V10.344H39.929Z" transform="translate(-7.5 -1.5)"/>
                    </svg>
                    <span><?php echo $phone; ?></span>
                </div>
            <?php endif; ?>
        </div>
    <?php endif; ?>
    <div class="user-profile-buttons">
        <div class="btn-wrapper <?php echo is_page_template('templates/tmp-profile.php') ? 'active' : '' ?>">
            <a href="/my-profile" class="btn-profile">My Profile</a>
        </div>
        <div class="btn-wrapper <?php echo is_page_template('templates/tmp-lookbook.php') ? 'active' : '' ?>">
            <a href="/lookbook" class="btn-profile">My Lookbook</a>
        </div>
        <div class="btn-wrapper <?php echo is_page_template('templates/tmp-saved-brands.php') ? 'active' : '' ?>">
            <a href="/saved-brands" class="btn-profile">My Saved Brands</a>
        </div>
        <div class="btn-wrapper <?php echo is_page_template('templates/tmp-my-offers.php') ? 'active' : '' ?>">
            <a href="/my-offers" class="btn-profile">My Offers</a>
        </div>
    </div>
</div>