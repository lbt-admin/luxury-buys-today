<article id="post-<?php the_ID(); ?>" class="blog-content">
        <div class="entry-wrap">


			<?php 			
            get_template_part( 'partials/content', 'post-image' );
            get_template_part( 'partials/content', 'post-title' ); 
			?>
<footer class="entry-footer">
<div class="entry-summary mix">
      <strong class="post-date"><?php novus_posted_on(false); ?></strong>
       <?php the_excerpt(); ?>
    </div>
    
</footer><!-- .entry-footer -->
        </div>
    </article><!-- #post-## -->