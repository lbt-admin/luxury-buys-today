<?php
/**
 * Blog post image template
 *
 * @package LBT
 */

$post_thumbnail = \LBT\Post::get_thumbnail( get_the_ID(), 'full' );

?>
<div class="content-post-image">
	<a href="<?php echo esc_url( get_permalink() ); ?>">
		<?php if ( $post_thumbnail ) : ?>
			<div class="entry-image">
				<img src="" data-test="1" onerror="this.src='<?php echo esc_url( $post_thumbnail ); ?>'; if ( this.getAttribute('retry') ) { if( this.getAttribute('retry') == 1 ) { this.src='<?php echo esc_url( $post_thumbnail ); ?>'; this.setAttribute( 'retry', 2 ); } else { this.src='<?php echo esc_url( LBT_THEME_URL ); ?>assets/images/post-thumb-cropped.svg'; } } else { this.setAttribute( 'retry', 1 ); }">
			</div>
		<?php endif; ?>
	</a>
	<?php
	if ( is_user_logged_in() ) :
		$user_lookbook = \LBT\Lookbook::get_user_lookbook();
		$remove_label  = is_page_template( 'tpl-lookbook.php' ) || is_page_template( 'templates/tmp-lookbook.php' ) ? __( 'REMOVE', 'lbt' ) : __( 'REMOVE FROM LOOKBOOK', 'lbt' );
		?>
		<?php if ( in_array( get_the_ID(), $user_lookbook ) ) : ?>
			<div data-postid="<?php echo esc_attr( get_the_ID() ); ?>" class="zm-save-favorites zm-fav-btn lbt-toggle-lookbook-post lbt-big-article-btn"><?php echo esc_html( $remove_label ); ?></div>
		<?php else : ?>
			<div data-postid="<?php echo esc_attr( get_the_ID() ); ?>" class="zm-save-favorites zm-fav-btn lbt-toggle-lookbook-post lbt-big-article-btn">SAVE TO LOOKBOOK</div>
		<?php endif; ?>
	<?php else : ?>
		<div data-postid="<?php echo esc_attr( get_the_ID() ); ?>" class="zm-save-favorites zm-fav-btn lbt-toggle-lookbook-post lbt-big-article-btn">SAVE TO LOOKBOOK</div>
	<?php endif; ?>
</div>
