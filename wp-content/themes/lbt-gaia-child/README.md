## Requirements for the subsite feed posts (promotions) to display them on automotive archive page

The plugin WP Automatic will parse all remote sources and save the posts 
in the subsite https://staging.luxurybuystoday.com/feeds/wp-admin/edit.php

The parsed posts need this information for the archive template files:

* postmeta:
    * offer_detail_city 
    * offer_detail_state
    * offer_detail_price
* Title will be taken from the post title
* Image will be taken from the feature image

Please note: The API will return all meta fields that starts with `offer_detail_` 
so you can easily add information and use it in the template files.

### Main site posts for dynamic loading of the subsite posts
* On the main site the post with the slug `automotive-promotions-{category}` must exists
as we will use this as a base to dynamically load the posts from the subsite.
  
Example dynamic post slug for Audi is: `automotive-promotions-audi` and so on for every category.

Those posts are locked by post ID and can not be updated after they are created. 
This is done via class: `wp-content/themes/lbt-gaia-child/inc/class-feed-single-post-promotion.php`
  

### Single post dynamic templates
To load the feed posts on the single post page, we use the existing functionality as
we have on the `/properties/christies-international/christies-promotions` and just override 
some data using the `wp-content/themes/lbt-gaia-child/inc/class-feed-single-post-promotion.php`

Also, we set up the `post_feed_id`, the remote post slug, we need to load in the iframe. 
This is done in: `wp-content/themes/lbt-gaia-child/partials/content-single.php`

### Custom REST endpoint plugin is created to serve this data for the feed posts
* The plugin `wp-content/plugins/lbt-data-feed-endpoints/inc/class-posts-tag-category-endpoint.php` is used
to server the data for the automotive category promotions (feed posts).
  
More info in the plugin README file: `wp-content/plugins/lbt-data-feed-endpoints/README.md`

### A transient cache for the endpoint
* There is a transient cache for the endpoint for 60 seconds. So if you change data we need 60 seconds
for the data to appear if you already made an endpoint query.
  
Or use $_GET param `lbt_clear_endpoint_cache` to force clear cache

### Automotive archive functionallity
* The archive dealership column will display 3 last posts from the subsite posts
that have `dealership-{number}` tag assigned, and the same category, example: audi category.
  
* When clicked more button, it will load also the post that contain the same category, but also 
posts which contain tag `promotions`.
  
## How to deploy this on production
1. Create the same posts as on staging for this array items and replce post ids: 
   `wp-content/themes/lbt-gaia-child/inc/class-feed-single-post-promotion.php` change const `DYNAMIC_POSTS`
   
2. On the feed site, setup plugin WC Automatic to pull the data into the right categories: Audi, BMW, and to add 
tag to each post for the dealership: `dealership-{id}`. If needed you can add the tag `promotions` to display this 
   post on category page when clicked on more button.
   
Setup parser plugin to add meta data as described above. 