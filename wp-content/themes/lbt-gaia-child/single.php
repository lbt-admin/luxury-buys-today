<?php
/**
 * The template for displaying all single posts.
 *
 * @package Novus
 */

$blog_post_id         = get_the_ID();
$favorite_count       = get_post_meta( $blog_post_id, 'simplefavorites_count', true );
$blog_category        = get_the_category()[0];
$is_broyhill_category = \LBT\Category::is_broyhill_category( $blog_category->term_id );

if ( empty( $favorite_count ) ) {
	$favorite_count = 0;
}

get_header();
?>
<div class="single-cols">
		<?php
		if ( ! $is_broyhill_category ) :
			?>
			<div class="sale-agent-container all-info" id="dealer_block"
				data-cat-name="<?php echo esc_attr( $blog_category->name ); ?>" data-cat="<?php echo esc_attr( $blog_category->term_id ); ?>"
				data-type="archive">
				<div class="lbt-spinner">
					<div class="closest-store">SEARCHING FOR YOUR CLOSEST STORE</div>
					<div class="lds-spinner"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
				</div>
			</div>
			<?php
		else : ?>
			<div class="lbt-single-post-ed-section">
				<img src="<?php echo esc_url( LBT_THEME_URL . 'assets/images/ed_broyhill.webp' ); ?>" alt="Ed Broyhill" width="220px">
				<h3><?php esc_html_e( 'More on Ed Broyhill', 'lbt' ); ?></h3>
				<p>
					Ed Broyhill's entire life is the embodiment of striving for the finer things in life. Raised in the historic Broyhill Furniture Family - Ed has acquired a knack for finding the best in luxury consumer items, and after years of being the perfect shopper, is working to help you find the better deals now. Luxury Buys Today is a new Broyhill creation where his editorial on quality and value can be publicly shared to a universe of brand conscious consumers. This is truly a quintessential Online Magazine featuring only the finest brands of luxury in Automotive, Fashion, Home Décor, and Home Properties . Additionally, he has carefully recruited across the nation a local concierge service of only Authorized Brand Consultants who provide you the convenience of the finest product offerings closest to you at the very best price. You can share this wonderful experience by subscribing to Luxury Buys Today.
				</p>
				<a href="/about-luxury-buys-today/"><?php esc_html_e( 'Read More', 'lbt' ); ?></a>
			</div>
		<?php
		endif;
		?>
		<?php
			if ( ! $is_broyhill_category ) {
				get_sidebar();
			}
		?>
	<div class="content-wrapper">
		<?php
		$count = get_post_meta( get_the_ID(), 'simplefavorites_count', true );
		if ( empty( $count ) ) {
			$count = 0;
		}

		do_action( 'wpr_homepage_editorial_section' );
		?>
		<div class="content-container" id="single_post_content_container">
			<div class="sticky_container">
				<div class="wpr-contact-buttons" id="wpr_contact">
					<div class="ui sticky">
					<?php
					if ( is_user_logged_in() ) {
						$user_favourites = \LBT\Lookbook::get_user_lookbook();

						if ( in_array( $blog_post_id, $user_favourites ) ) {
							$save_brand_btn = '<div class="zm-save-favorites1 zm-fav-btn lbt-toggle-lookbook-post" remove-favorite="active" data-postid="' . $blog_post_id . '" data-siteid="1" data-groupid="1" data-favoritecount="' . $favorite_count . '"><span>REMOVE</span> FROM LOOKBOOK</div>';
						} else {
							$save_brand_btn = '<div class="zm-save-favorites1 zm-fav-btn lbt-toggle-lookbook-post" data-postid="' . $blog_post_id . '" data-siteid="1" data-groupid="1" data-favoritecount="' . $favorite_count . '"><span>SAVE</span> TO LOOKBOOK</div>';
						}

						echo wp_kses_post( $save_brand_btn );
						?>
					<?php } else { ?>
						<a href="#" class="zm-cta-btn wpr-single-offer-buttons wpr-add-to-lookbook1 lbt-toggle-lookbook-post"
							data-postid="<?php the_ID(); ?>" data-siteid="1"
							data-favoritecount="<?php echo esc_attr( $favorite_count ); ?>"><span>SAVE</span> TO LOOKBOOK</a>
					<?php } ?>
						<?php if ( ! $is_broyhill_category ) : ?>
						<div class="zm-cta-actions">
							<div class="zm-price lbt-dealer-cta-request-btn" data-action="price" data-ctacount="<?php echo esc_attr( $favorite_count ); ?>" data-id="<?php the_ID(); ?>"><i class="icon-money" aria-hidden="true"></i>Price it</div>
							<div class="zm-info lbt-dealer-cta-request-btn" data-action="learn" data-ctacount="<?php echo esc_attr( $favorite_count ); ?>" data-id="<?php the_ID(); ?>"><i class="icon-mail" aria-hidden="true"></i>Send me<br> info</div>
							<div class="zm-contact-me lbt-dealer-cta-request-btn" data-action="contact" data-ctacount="<?php echo esc_attr( $favorite_count ); ?>" data-id="<?php the_ID(); ?>"><i class="icon-phone" aria-hidden="true"></i>Contact<br> me</div>
							<div class="lbt-appointment-button" data-action="appointment" data-ctacount="<?php echo esc_attr( $favorite_count ); ?>" data-id="<?php the_ID(); ?>"><i class="icon-calendar"></i>Make APPOINTMENT</div>
						</div>
						<?php endif; ?>
					</div>
				</div>
				<div id="primary" class="content-area">
					<main id="main" class="site-main" role="main">
						<?php
						while ( have_posts() ) :
							the_post();

							if ( has_post_format( 'gallery' ) ) {
								get_template_part( 'partials/content', 'gallery' );
							} else  {
								get_template_part( 'partials/content', 'single' );
							}

							if ( get_theme_mod( 'related_posts_display', true ) ) {
								novus_related_posts();
							}

							if ( comments_open() || get_comments_number() ) {
								comments_template();
							}
						endwhile; // end of the loop.
						?>
					</main><!-- #main -->
				</div><!-- #primary -->
				<div class="spacer" style="clear: both;"></div>
			</div>
			<div class="spacer" style="clear: both;"></div>
		</div>
	</div>
	<!-- </div> -->
</div>
<?php get_footer(); ?>
