(function ($) {
	$(function () {
		const subscribed = LBT_GetCookie("confirm_subscription");
		const newsletter = $('[data-target="coming-soon-modal"]');

		if (subscribed) {
			newsletter.addClass("cookie").removeClass("default");
		}

		function LBT_SetCookie(cname, cvalue, exdays) {
			var d = new Date();
			d.setTime(d.getTime() + exdays * 24 * 60 * 60 * 1000);
			var expires = "expires=" + d.toUTCString();
			document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
		}

		function LBT_GetCookie(cname) {
			var name = cname + "=";
			var decodedCookie = decodeURIComponent(document.cookie);
			var ca = decodedCookie.split(";");
			for (var i = 0; i < ca.length; i++) {
				var c = ca[i];
				while (c.charAt(0) == " ") {
					c = c.substring(1);
				}
				if (c.indexOf(name) == 0) {
					return c.substring(name.length, c.length);
				}
			}
			return "";
		}

		$(document).on("click", '[data-action="lbt-modal-close"]', function (e) {
			$(this).closest(".lbt-modal").hide();
		});

		$(document).on("click", '[data-action="lbt-coming-soon"]', function (e) {
			$('[data-target="coming-soon-modal"]').show();
		});

		if (modalComingSoon.confirmSubscriptionScreen) {
			LBT_SetCookie("confirm_subscription", true, 365);
		}
	});
})(jQuery);
