(function ($) {
	$(function () {
		const sendCTARequest = function () {
			if (!lbtDealerCTARequest.isUserLoggedIn) {
				$("#lbt-login-form-section .lbt-form-switch").first().trigger("click");
				return;
			}

			const $this = $(this);

			let dealershipID;
			let brandID;
			let postID;

			if (!lbtDealerCTARequest.isSavedBrandsPage) {
				const $dealerInfoBox = $("#wpr-dealer-location");

				dealershipID = $dealerInfoBox.data("dealershipId");
				brandID = $dealerInfoBox.data("brandId");
				postID = $dealerInfoBox.data("postId");
			} else {
				dealershipID = $this.data("dealershipId");
				brandID = $this.data("brandId");
				postID = false;
			}

			const countFav = typeof $this.data("ctacount") != "undefined";

			$.ajax({
				url: lbtDealerCTARequest.ajaxUrl,
				type: "POST",
				data: {
					dealership_id: dealershipID,
					brand_id: brandID,
					post_id: postID,
					cta_action: $this.data("action"),
					count_fav: countFav,
					action: lbtDealerCTARequest.sendCTARequestAction,
					__nonce: lbtDealerCTARequest.sendCTARequestNonce,
				},
				beforeSend: function () {
					$this.prop("disabled", true);
					$this.append(
						'<div class="lbt-cta-btn-spinner"><span class="fa fa-spinner fa-spin"></span></div>'
					);
				},
				success: function (response) {
					if (response.success) {
						$this.find(".lbt-cta-btn-spinner").remove();
						$(".openCTApopup").trigger("click");
					}

					$this.prop("disabled", false);
				},
				error: function (xhr, status, error) {
					console.log(xhr.responseJSON.data);
					if ("coming_soon" === xhr.responseJSON.data) {
						$('[data-target="coming-soon-modal"]').show();
					} else {
						alert(xhr.responseJSON.data);
					}
					$this.find(".lbt-cta-btn-spinner").remove();
					$this.prop("disabled", false);
				},
			});
		};

		$(document).on("click", ".lbt-appointment-button", function () {
			const $cronofyError = $("input[name=lbt_cronofy_error_data]");

			if ($cronofyError.length) {
				return;
			}

			const $this = $(this);

			$.ajax({
				url: lbtDealerCTARequest.ajaxUrl,
				type: "POST",
				data: {
					action: lbtDealerCTARequest.checkAvailabilityAction,
					__nonce: lbtDealerCTARequest.checkAvailabilityNonce,
				},
				beforeSend: function () {
					$this.prop("disabled", true);
					$this.append(
						'<div class="lbt-cta-btn-spinner"><span class="fa fa-spinner fa-spin"></span></div>'
					);
				},
				success: function (response) {
					$(".wpr-cronofy-calendar-overlay").show();
					$(".wpr-cronofy-calendar-modal").show();
					$(".wpr-cronofy-calendar-modal").removeData("request");
					$(".wpr-cronofy-calendar-modal").data("request", {
						post_id: $("#wpr-dealer-location").data("postId"),
						dealer_address: $(".container")
							.find("#wpr-dealer-location #zm-profile-description")
							.data("dealAddress"),
						dealer_state: $(".container")
							.find("#wpr-dealer-location #zm-profile-description")
							.data("dealState"),
					});
					$this.prop("disabled", false);
					$this.find(".lbt-cta-btn-spinner").remove();
				},
				error: function (xhr, status, error) {
					console.log(xhr.responseJSON.data);
					if ("coming_soon" === xhr.responseJSON.data) {
						$('[data-target="coming-soon-modal"]').show();
					} else {
						alert(xhr.responseJSON.data);
					}
					$this.prop("disabled", false);
					$this.find(".lbt-cta-btn-spinner").remove();
				},
			});
		});

		$(".wpr-cronofy-modal-close").on("click", function () {
			$(".wpr-cronofy-calendar-overlay").hide();
			$(".wpr-cronofy-calendar-modal").hide();
		});

		$(document).on("click", ".wpr-dealer-hours", function (event) {
			$(".timeSlots").slideToggle(500);
		});

		$(document).on("click", ".lbt-dealer-cta-request-btn", sendCTARequest);
	});
})(jQuery);
