jQuery.fn.htmlClean = function () {
    this.contents().filter(function () {
        if (this.nodeType != 3) {
            jQuery(this).htmlClean();
            return false;
        }
        else {
            this.textContent = jQuery.trim(this.textContent);
            return !/\S/.test(this.nodeValue);
        }
    }).remove();
    return this;
};

jQuery('.pumClose').on('click' , function(event) {
  jQuery('#popmake-35259').popmake('close');
});

jQuery('#wrap-content').htmlClean();

jQuery('#wrap-content').find("p").contents().filter(function () {
    return this.nodeType === 3 && jQuery.trim(this.nodeValue) !== '';
}).wrap('<div class="padText">');
jQuery('#wrap-content').find("b").contents().filter(function () {
    return this.nodeType === 3 && jQuery.trim(this.nodeValue) !== '';
}).wrap('<div class="padText">');
jQuery('#wrap-content').find("h3").contents().filter(function () {
    return this.nodeType === 3 && jQuery.trim(this.nodeValue) !== '';
}).wrap('<div class="padText">');
jQuery('#wrap-content').find("h2").contents().filter(function () {
    return this.nodeType === 3 && jQuery.trim(this.nodeValue) !== '';
}).wrap('<div class="padText">');
jQuery('#wrap-content').find("i").contents().filter(function () {
    return this.nodeType === 3 && jQuery.trim(this.nodeValue) !== '';
}).wrap('<div class="padText">');
jQuery('#wrap-content').contents().filter(function () {
    return this.nodeType === 3 && jQuery.trim(this.nodeValue) !== '';
}).wrap('<div class="padText source_link" style="display:none;">');

jQuery('#wrap-content').contents().filter(function () {
    return this.nodeType === 3 && jQuery.trim(this.nodeValue) !== '';
}).wrap('<div class="padText">');


jQuery('.padText').each(function () {
    if (jQuery.trim(jQuery(this).text()).length == 0) {
        if (jQuery(this).children().length == 0) {
            jQuery(this).hide();
            jQuery(this).remove(); // remove empty paragraphs
        }
    }
    if (jQuery.trim($(this).text()) == 'Source Link:') {
        var moveLink = " " + jQuery('.wpr_article_source').prop('outerHTML');
        jQuery('.wpr_article_source').remove();
        jQuery(this).append(moveLink)
    }

});


jQuery('.wrap-content').find('td').each(function () {
    if (jQuery.trim(jQuery(this).text()).length == 0) {
        if (jQuery(this).children().length == 0) {
            jQuery(this).hide();
            jQuery(this).remove(); // remove empty paragraphs
        }
    }
});


jQuery('img[src$="//www.saksfifthavenue.com/emailMedia/SF_logo_2016.gif"]').addClass('fixImage');
jQuery('img[src$="//news.saks.com/a/hBbUc7HAdIUrHB9pvplNtimZ-3J/spacer.gif"]').addClass('removeBlock');
jQuery('img[src$="//image.emails.brooksbrothers.com/lib/fe9a13707565017d7d/m/27/2018_fac_logo.gif"]').addClass('removeBlock');
jQuery('img[src$="//t.tco.tiffany.com/r/?id=h1b6b259c,4445ed99,1"]').addClass('removeBlock');


jQuery(window).on('load', function () {
	var chkClass = $("div").hasClass("wpr_christies_a");
	if(chkClass == true){
		jQuery('#wrap-content').find('img.emailImage').wrap('<div class="boxImg">').parent().append(jQuery('.wrapButtons').html());
	}
	else{
		jQuery('#wrap-content').find('img').wrap('<div class="boxImg">').parent().append(jQuery('.wrapButtons').html());
	}
	

    // add timeout Chrome initial blank document for iframe (also fixes ios).
    setTimeout(iResize, 50);


    //rest of the browsers
    height = Math.max(window.document.body.scrollHeight, window.document.body.offsetHeight, window.document.documentElement.clientHeight, window.document.documentElement.scrollHeight, window.document.documentElement.offsetHeight);
    // console.log(height);

    window.parent.document.getElementById('contentFrame').height = height;


    function iResize() {
        height = Math.max(window.document.body.scrollHeight, window.document.body.offsetHeight, window.document.documentElement.clientHeight, window.document.documentElement.scrollHeight, window.document.documentElement.offsetHeight);
        console.log(height);

        window.parent.document.getElementById('contentFrame').height = height;
    }





  requestRunning = false;

  $('.wpr-learn-more, .wpr-price-request, .wpr-set-appointment, .wpr-contact-me, .wpr-single-offer-buttons.wpr-price-it').on('click', function (e) {
    e.preventDefault();
    if (requestRunning) {
      return;
    }

    if ($(this).hasClass('popmake-33647')) {
        jQuery(window.parent.document).find('.pumOpen33647').trigger('click');

    } else {
      var wprInitial = $(this).html();

      $(this).html('<i class="fa fa-spinner fa-spin"></i> ' + wprInitial);

      var wprDeal = $('.container').find('#wpr-dealer-location').attr('data-deal');
      var wprCat = $('.container').find('#wpr-dealer-location').attr('data-loc');
      var wprTODO = $(this).attr('data-do');
      var wprOffer = $(this).closest('.entry-header').attr('data-deal-id');
      //var wprDealerName = $('.container').find('#wpr-dealer-location .wpr-dealer-details-single .wpr-dealer-name').addClass('er3');

      
      var dealTitle    = $('#wpr-dealer-location').find('#zm-profile-description').attr('data-deal-title');
      var dealAddress  = $('#wpr-dealer-location').find('#zm-profile-description').attr('data-deal-address');
      var dealContact  = $('#wpr-dealer-location').find('#zm-profile-description').attr('data-deal-contact');
      var dealState    = $('#wpr-dealer-location').find('#zm-profile-description').attr('data-deal-state');
      var dealCity     = $('#wpr-dealer-location').find('#zm-profile-description').attr('data-deal-city');

      if (!wprOffer) {
        wprOffer = $(this).closest('.single_button').attr('data-postid');
      }

      if ('map' !== wprTODO) {
        // e.preventDefault();
      }

      var data = {
        action: 'save_cta',
        nonce: nonceVar,
        deal: wprDeal,
        categ: wprCat,
        todo: wprTODO,
        current_url: window.location.href,
        current_offer: wprOffer,
        deal_title: dealTitle,  
        deal_address: dealAddress,  
        deal_contact: dealContact,  
        deal_state: dealState,  
        deal_city: dealCity, 
      };

      var fa_icon = 'fa-calendar';

      if ($(this).hasClass('wpr-price-request')) {
        fa_icon = 'fa-money';
      }

      if ($(this).hasClass('wpr-learn-more')) {
        fa_icon = 'fa-envelope-o';
      }

      if ($(this).hasClass('wpr-contact-me')) {
        fa_icon = 'fa-phone';
      }

      if ($(this).hasClass('simplefavorite-button')) {
        fa_icon = 'fa-file-image-o';
      }
      var ajaxOpts = {
        type: 'POST',
        url: ajaxURLVar,
        data: data,
        success: function (data) {

          $('i').remove('.fa-spinner');

          if ('map' !== wprTODO && data) {
            jQuery(window.parent.document).ready(function(){
              jQuery(window.parent.document).find('.pumOpen').trigger('click');
            });

            //var obj = jQuery.parseJSON(data);
           /* swal({
              title: '<i class="fa ' + fa_icon + '" aria-hidden="true"></i>',
              html: 'Thank you for your interest, you will be contacted soon about this product.',
              animation: false,
              showCancelButton: true,
              confirmButtonColor: '#e8be5c',
              cancelButtonColor: 'transparent',
              confirmButtonText: 'Ok',
              cancelButtonText: facebookURLVar,
              confirmButtonClass: 'btn btn-success wpr-popup-button-ok',
              cancelButtonClass: 'wpr-please-like-us',
              buttonsStyling: true
            }).then(function () {

            }, function (dismiss) {
              if (dismiss === 'cancel') {
                wpr_got_to_facebook();
              }
            });
*/
          }
        },
        complete: function () {
          requestRunning = false;
        },
        error: function (request,  status, error) {
          swal({
            title: '<i class="fa ' + fa_icon + '" aria-hidden="true"></i>',
            text: 'There was an error saving your selection, please try again.',
            type: 'warning'
          });
        }
      };

      requestRunning = true;


      $.ajax(ajaxOpts);
    }

    return false;
  });

});
