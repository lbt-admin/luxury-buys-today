(function ($) {
  $(function () {
    let brandDealerPromotionsIntereval;
    let brandDealerPromotionsTries = 0;

    const categoryTopGridLayout = function () {
      var windowWidth = $(window).width(),
        unwrap = $(".gt-box > .gt-box");

      switch (true) {
        case windowWidth > 1025:
          unwrap.unwrap();
          $(".gt-box.dealer, .gt-box.category, .gt-box.promotions").wrapAll(
            '<div class="gt-box middle"></div>'
          );
          break;
        case windowWidth > 576:
          unwrap.unwrap();
          $(".gt-box.offers, .gt-box.promotions").wrapAll(
            '<div class="gt-box left"></div>'
          );
          $(".gt-box.dealer, .gt-box.category, .gt-box.news").wrapAll(
            '<div class="gt-box right"></div>'
          );
          break;
        default:
          unwrap.unwrap();
      }
    };

    const getBrandDealerPromotions = function () {
      brandDealerPromotionsTries++;
      $.ajax({
        url: lbtCategory.ajaxUrl,
        type: "POST",
        data: {
          brand_id: lbtCategory.brandId,
          post_id: lbtCategory.postId,
          is_single_post: lbtCategory.isSinglePost,
          action: lbtCategory.brandDealerPromotionsAction,
          __nonce: lbtCategory.brandDealerPromotionsNonce,
        },
        success: function (response) {
          if (response.success && typeof response.data != "undefined") {
            clearInterval(brandDealerPromotionsIntereval);
            $(".gt-box.promotions .content-wrapper").html(
              response.data.promotions_box
            );
            $("#dealer_block").html(response.data.dealer_box);

            if (lbtCategory.isSinglePost) {
              $("#dealer_block").find(".long").hide();
              $("#dealer_block").find(".short").show();
            }
            return;
          }

          if (brandDealerPromotionsTries > 3) {
            clearInterval(brandDealerPromotionsIntereval);
            $(".gt-box.promotions .content-wrapper").html(
              `<div>Failed to find promotions</div>`
            );
          }
        },
        error: function (xhr, status, error) {
          console.log(xhr.responseJSON.data);

          if (brandDealerPromotionsTries > 3) {
            $(".gt-box.promotions .content-wrapper").html(
              `<div>${xhr.responseJSON.data}</div>`
            );
            clearInterval(brandDealerPromotionsIntereval);
          }
        },
      });
    };

    $(window).on("resize", function () {
      categoryTopGridLayout();
    });

    if ($("#brand-filter").length) {
      $("#brand-filter").select2({
        placeholder: "Filter by brand",
      });

      $("#brand-filter").change(function () {
        if (0 == $(this).val()) {
          window.location.href = $(this).data("link");
        } else {
          window.location.href =
            $(this).data("link") + "?brand_id=" + $(this).val();
        }
      });
    }

    categoryTopGridLayout();

    // if ($(".gt-box.promotions .content-wrapper").length) {
    //   brandDealerPromotionsIntereval = setInterval(
    //     getBrandDealerPromotions,
    //     5000
    //   );
    // }
  });
})(jQuery);
