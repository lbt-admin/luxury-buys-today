(function ($) {
	$(function () {
		$("#owl-demo").owlCarousel({
			navigation: false,
			singleItem: true,
			loop: true,
			autoPlay: true,
			autoPlayTimeout: 3000,
			autoPlayHoverPause: true,
			transitionStyle: "fade",
			lazyLoad: true,
		});
	});
})(jQuery);
