/**
 * navigation.js
 *
 * Handles toggling the navigation menu for small screens and enables tab
 * support for dropdown menus.
 */
( function() {
	var container, containerSecondary, button, menu, links, subMenus;

	container = document.getElementById( 'site-navigation' );
	containerSecondary = document.getElementById( 'secondary-navigation' );
	if ( ! container ) {
		return;
	}

	topbar = document.getElementById( 'masthead' );
	button = topbar.getElementsByTagName( 'button' )[0];
	if ( 'undefined' === typeof button ) {
		return;
	}

	menu = container.getElementsByTagName( 'ul' )[0];

	// Hide menu toggle button if menu is empty and return early.
	if ( 'undefined' === typeof menu ) {
		button.style.display = 'none';
		return;
	}

	menu.setAttribute( 'aria-expanded', 'false' );
	if ( -1 === menu.className.indexOf( 'nav-menu' ) ) {
		menu.className += ' nav-menu';
	}

	button.onclick = function() {
		if ( -1 !== container.className.indexOf( 'toggled' ) ) {
			container.className = container.className.replace( ' toggled', '' );
			button.setAttribute( 'aria-expanded', 'false' );
			menu.setAttribute( 'aria-expanded', 'false' );
		} else {
			if ( -1 !== containerSecondary.className.indexOf( 'toggled' ) ) {
				containerSecondary.className = containerSecondary.className.replace( ' toggled', '' );
			}
			container.className += ' toggled';
			button.setAttribute( 'aria-expanded', 'true' );
			menu.setAttribute( 'aria-expanded', 'true' );
		}
	};

	// Get all the link elements within the menu.
	links    = menu.getElementsByTagName( 'a' );
	subMenus = menu.getElementsByTagName( 'ul' );

	// Set menu items with submenus to aria-haspopup="true".
	for ( var i = 0, len = subMenus.length; i < len; i++ ) {
		subMenus[i].parentNode.setAttribute( 'aria-haspopup', 'true' );
	}

	// Each time a menu link is focused or blurred, toggle focus.
	for ( i = 0, len = links.length; i < len; i++ ) {
		links[i].addEventListener( 'focus', toggleFocus, true );
		links[i].addEventListener( 'blur', toggleFocus, true );
	}

	/**
	 * Sets or removes .focus class on an element.
	 */
	function toggleFocus() {
		var self = this;

		// Move up through the ancestors of the current link until we hit .nav-menu.
		while ( -1 === self.className.indexOf( 'nav-menu' ) ) {

			// On li elements toggle the class .focus.
			if ( 'li' === self.tagName.toLowerCase() ) {
				if ( -1 !== self.className.indexOf( 'focus' ) ) {
					self.className = self.className.replace( ' focus', '' );
				} else {
					self.className += ' focus';
				}
			}

			self = self.parentElement;
		}
	}

	// Fix child menus for touch devices.
	function fixMenuTouchTaps( container ) {
		var touchStartFn,
				parentLink = container.querySelectorAll( '.menu-item-has-children > a, .page_item_has_children > a' );

		if ( 'ontouchstart' in window ) {
			touchStartFn = function( e ) {
				var menuItem = this.parentNode;

				if ( ! menuItem.classList.contains( 'focus' ) ) {
					e.preventDefault();
					for( var i = 0; i < menuItem.parentNode.children.length; ++i ) {
						if ( menuItem === menuItem.parentNode.children[i] ) {
							continue;
						}
						menuItem.parentNode.children[i].classList.remove( 'focus' );
					}
					menuItem.classList.add( 'focus' );
				} else {
					menuItem.classList.remove( 'focus' );
				}
			};

			for ( var i = 0; i < parentLink.length; ++i ) {
				parentLink[i].addEventListener( 'touchstart', touchStartFn, false );
			}
		}
	}

	fixMenuTouchTaps( container );
} )();

( function( $ ) {
	// Hide menu dropdown when clicking/tapping outside.
	var $menuContainer = $('.ubermenu-main');
	var $menuToggle = $('.ubermenu-responsive-toggle-main');

	function wprMenuDropdownHide( event ) {
		if ( $( event.target ).parents('.menu_container').length === 0 ) {
			$(document).off( 'click tap', wprMenuDropdownHide );
			$menuContainer.ubermenu('toggleMenuCollapse');
		}
	}

	$menuToggle
		.on( 'ubermenutoggledopen', function() {
			$(document).on( 'click tap', wprMenuDropdownHide );
		}).on( 'ubermenutoggledclose', function() {
			$(document).off( 'click tap', wprMenuDropdownHide );
		});

	// Fix popup bug on Egde.
	$(document)
		.on('pumAfterOpen', function() {
			var $popup = $('.pum-active');
			$popup.add( $popup.children('.pum-container') ).css( 'opacity', 1 );
		})
		.on('pumAfterClose', function() {
			var $popup = $('.pum');
			$popup.add( $popup.children('.pum-container') ).css( 'opacity', 0 );
		});

	// Prevent "AddThis" redirect.
	$(document).on('click', 'a', function(event) {
		var href = $(this).attr('href');

		if ( null !== String(href).match('addthis') ) {
			event.preventDefault();
			event.stopPropagation();
			return false;
		}
	});

	$(document).ready( function() {
		// Fix popup triggers on tap.
		setTimeout( function() {
			$('li[class*="popmake-"]').on('touchstart tap', function() {
				$(this).click();
			});
		}, 100 );

		// Fix menu toggle on small desktop screens.
		//$('.ubermenu-responsive-toggle').on('click', function() {
		//	$(this).trigger('tap');
		//});
	});
}( jQuery ));
