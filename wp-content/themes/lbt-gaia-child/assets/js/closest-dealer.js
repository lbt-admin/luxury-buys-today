(function ($) {
	$(function () {
		let closestDealerIntereval;
		let closestDealerTries = 0;

		const getClosestBrandDealer = function () {
			closestDealerTries++;
			$.ajax({
				url: lbtClosestDealer.ajaxUrl,
				type: "POST",
				data: {
					brand_id: lbtClosestDealer.brandId,
					post_id: lbtClosestDealer.postId,
					is_single_post: lbtClosestDealer.isSinglePost,
					action: lbtClosestDealer.closestDealerAction,
					__nonce: lbtClosestDealer.closestDealerNonce,
				},
				success: function (response) {
					if (response.success && typeof response.data != "undefined") {
						clearInterval(closestDealerIntereval);
						$("#dealer_block").html(response.data);

						if (lbtClosestDealer.isSinglePost) {
							$("#dealer_block").find(".long").hide();
							$("#dealer_block").find(".short").show();
						}
						return;
					}

					if (closestDealerTries > 3) {
						clearInterval(closestDealerIntereval);
						$("#dealer_block").html(`<div>Failed to find dealer</div>`);
					}
				},
				error: function (xhr, status, error) {
					console.log(xhr.responseJSON.data);

					if (closestDealerTries > 3) {
						$("#dealer_block").html(`<div>${xhr.responseJSON.data}</div>`);
						clearInterval(closestDealerIntereval);
					}
				},
			});
		};

		const toggleSaveBrand = function () {
			if (!lbtClosestDealer.isUserLoggedIn) {
				$("#lbt-login-form-section .lbt-form-switch").first().trigger("click");
				return;
			}

			const $this = $(this);

			let dealershipId;
			let postId = false;

			if (lbtClosestDealer.isSavedBrandsPage) {
				dealershipId = $this.data("dealershipId");
			} else {
				dealershipId = $this
					.parents("#wpr-dealer-location")
					.data("dealershipId");
			}

			if ($this.parents("#wpr-dealer-location").length) {
				postId = $this.parents("#wpr-dealer-location").data("postId");
			}

			$.ajax({
				url: lbtClosestDealer.ajaxUrl,
				type: "POST",
				data: {
					dealership_id: dealershipId,
					post_id: postId,
					action: lbtClosestDealer.saveFavoriteBrandAction,
					__nonce: lbtClosestDealer.saveFavoriteBrandNonce,
				},
				beforeSend: function () {
					$this.prop("disabled", true);
					$this.append(
						'<div class="lbt-cta-btn-spinner"><span class="fa fa-spinner fa-spin"></span></div>'
					);
				},
				success: function (response) {
					if (lbtClosestDealer.isSavedBrandsPage) {
						$this.parents(".post-box-wrap").first().remove();
					} else {
						if (response.success) {
							$this
								.addClass("lbt-brand-saved")
								.removeClass("lbt-toggle-brand-btn");
							$this.find(".fa.fa-heart").addClass("red");
							$this.find(".long").hide();
							$this.find(".short").hide();
							$this.append("<span>Saved</span>");
							$this.find(".lbt-cta-btn-spinner").remove();
						}
					}

					$this.prop("disabled", false);
				},
				error: function (xhr, status, error) {
					console.log(xhr.responseJSON.data);
					alert(xhr.responseJSON.data);
					$this.find(".lbt-cta-btn-spinner").remove();
					$this.prop("disabled", false);
				},
			});
		};

		$(document).on("click", ".lbt-toggle-brand-btn", toggleSaveBrand);
		if (!lbtClosestDealer.isSavedBrandsPage && !lbtClosestDealer.isCategory) {
			getClosestBrandDealer();
		}
	});
})(jQuery);
