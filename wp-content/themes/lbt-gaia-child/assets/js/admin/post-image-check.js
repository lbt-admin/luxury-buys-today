(function ($) {
	$(function () {
		const $manualRunForm = $("#lbt-post-image-check__manual-form");
		const $submitButton = $manualRunForm.find("button");
		const submitButtonText = $submitButton.text();
		const $progressArea = $(".lbt-post-image-check__manual-progress");

		let firstManualRun = false;

		const $restartButton = $("#lbt-post-image-check__restart");

		const runManualAction = function ($form) {
			const formData = new FormData($form[0]);

			$.ajax({
				url: ajaxurl,
				type: "POST",
				data: formData,
				cache: false,
				contentType: false,
				processData: false,
				beforeSend: function () {
					$submitButton.text("Processing...");
					$submitButton.prop("disabled", true);
				},
				success: function (response) {
					let hasNext = false;
					if (response.success) {
						hasNext = parseInt(response.data.total_deleted) > 0;
						if (hasNext) {
							firstManualRun = false;
							$progressArea.html(
								"Total Deleted: " + response.data.total_deleted
							);
							runManualAction($form);
						} else {
							if (firstManualRun) {
								$progressArea.html(
									"Total Deleted: " + response.data.total_deleted
								);
							}
							$submitButton.prop("disabled", false);
							$submitButton.text(submitButtonText);
						}
					}
				},
				error: function (xhr, status, error) {
					alert(xhr.responseJSON.data);
					$progressArea.html(
						'<span style="color: #dc3545;">' + xhr.responseJSON.data + "</span>"
					);
					$submitButton.prop("disabled", false);
					$submitButton.text(submitButtonText);
				},
			});
		};

		const restartScript = function (e) {
			e.preventDefault();

			const $this = $(this);

			$.ajax({
				url: ajaxurl,
				type: "POST",
				data: {
					action: "lbt_post_image_check_restart_script",
					__nonce: $this.data("nonce"),
				},
				beforeSend: function () {
					$progressArea.html("");
					$this.text("Restarting...");
					$this.prop("disabled", true);
				},
				success: function (response) {
					if (response.success) {
						location.reload();
					}
					$this.prop("disabled", false);
					$this.text("Restart Now");
				},
				error: function (xhr, status, error) {
					alert(xhr.responseJSON.data);
					$progressArea.html(
						'<span style="color: #dc3545;">' + xhr.responseJSON.data + "</span>"
					);
					$this.prop("disabled", false);
					$this.text(submitButtonText);
				},
			});
		};

		$manualRunForm.on("submit", function (e) {
			e.preventDefault();

			firstManualRun = true;

			$progressArea.html("");
			// runManualAction($(this));
		});

		$restartButton.on("click", restartScript);
	});
})(jQuery);
