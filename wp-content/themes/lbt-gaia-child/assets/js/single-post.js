(function ($) {
  $(function () {
    let $n = 0;
    let revItem = $(
      ".sticky_container > .wpr-contact-buttons .zm-cta-actions > div"
    );

    function revealItemAnim() {
      if ($n === 0) {
        revItem.css("opacity", 0);
        revItem.addClass("reveal-anim-in");
        revItem.each(function () {
          $(this).css("animation-delay", "" + $n + "s");
          $n = Math.round(($n + 0.1) * 100) / 100;
        });
      } else {
        revItem.css("opacity", 1);
        revItem.removeClass("reveal-anim-in");
        revItem.each(function () {
          $n = Math.round(($n - 0.1) * 100) / 100;
          $(this).css("animation-delay", "" + $n + "s");
        });
        $n = 0;
      }
    }

    $("body")
      .off("click")
      .on("click", ".mobile-init-btn", function () {
        var sticky_container = $(this).closest(".sticky_container");
        sticky_container.toggleClass("active");

        revealItemAnim();
      });

    function btnRadarClick() {
      var contentFrame = $("#contentFrame"),
        post_content = contentFrame.contents().find(".post-content");

      post_content.off("click").on("click", ".post-image", function () {
        var sticky_container = $(window.parent.document).find(
          ".sticky_container"
        );
        sticky_container.toggleClass("active");

        revealItemAnim();
      });
    }

    function iframeContent() {
      var contentFrame = $("#contentFrame"),
        image = contentFrame.contents().find(".post-image img"),
        post_content = contentFrame.contents().find(".post-content"),
        post_image = contentFrame.contents().find(".post-content img"),
        button_radar = contentFrame.contents().find(".post-image"),
        windowWidth = window.innerWidth,
        additionalHeight = 25;

      if (windowWidth < 1025 && windowWidth > 576) {
        additionalHeight = 135;
      } else if (windowWidth < 577) {
        additionalHeight = 180;
      }

      if (contentFrame) {
        function setContentHeight() {
          var iframeHeight = contentFrame
            .contents()
            .find("#wrap-content")
            .height();
          contentFrame.height(iframeHeight + additionalHeight);
        }

        contentFrame.on("load", function () {
          setTimeout(function () {
            setContentHeight();
          }, 250);
        });

        setTimeout(function () {
          setContentHeight();
        }, 250);
      }

      if (windowWidth < 1025) {
        if (button_radar.length === 0) {
          post_image.wrap('<div class="post-image"></div>');
        }
      } else {
        image.unwrap();
      }
    }

    window.addEventListener("resize", function () {
      iframeContent();
      btnRadarClick();
    });

    $("#contentFrame").on("load", function () {
      iframeContent();
      btnRadarClick();
    });

    iframeContent();
    btnRadarClick();
  });
})(jQuery);
