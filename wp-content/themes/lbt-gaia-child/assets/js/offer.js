(function ($) {
	$(function () {
		const toggleOfferPost = function () {
			if (!lbtOffer.isUserLoggedIn) {
				$("#lbt-login-form-section .lbt-form-switch").first().trigger("click");
				return;
			}

			const $this = $(this);

			const postId = $this.data("postid");
			const dealershipId = $("#wpr-dealer-location").data("dealershipId");

			$.ajax({
				url: lbtOffer.ajaxUrl,
				type: "POST",
				data: {
					post_id: postId,
					dealership_id: dealershipId,
					action: lbtOffer.toggleOfferAction,
					__nonce: lbtOffer.toggleOfferNonce,
				},
				beforeSend: function () {
					$this.prop("disabled", true);
					$this.append(
						'<div class="lbt-cta-btn-spinner"><span class="fa fa-spinner fa-spin"></span></div>'
					);
				},
				success: function (response) {
					if (response.success) {
						if (response.data === "save") {
							buttonLabel = '<span>Favorited</span> <i class="fa fa-star"></i>';
						} else {
							buttonLabel =
								'<span>Favorite</span> <i class="fa fa-star-o"></i>';
						}
						$this.html(buttonLabel);
					}

					$this.prop("disabled", false);
				},
				error: function (xhr, status, error) {
					console.log(xhr.responseJSON.data);
					alert(xhr.responseJSON.data);
					$this.find(".lbt-cta-btn-spinner").remove();
					$this.prop("disabled", false);
				},
			});
		};

		$(document).on("click", ".lbt-toggle-favorite-post", toggleOfferPost);
	});
})(jQuery);
