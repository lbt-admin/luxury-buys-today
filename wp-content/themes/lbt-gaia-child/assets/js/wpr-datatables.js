jQuery(document).ready(function ($) {
	if ($(".wpr-login-redirect-field").length > 0) {
		setTimeout(function () {
			$(".wpr-login-redirect-field")
				.find('input[type="text"]')
				.attr("value", window.location.href);
		}, 1000);
	}

	if ($(".wpr-register-redirect-field").length > 0) {
		setTimeout(function () {
			$(".wpr-register-redirect-field")
				.find('input[type="text"]')
				.attr("value", window.location.href);
		}, 1000);
	}

	$(".wpr-brand-catalog").on("click", function (e) {
		e.preventDefault();

		var catalog_url = $(this).attr("href");
		var win_width = (90 / 100) * $(window).width();
		var win_height = (90 / 100) * $(window).height();

		window.open(
			catalog_url,
			"Catalog",
			"height=" + win_height + ",width=" + win_width
		);
	});

	$(".site-main").imagesLoaded(function () {
		$(".js-loader").hide();
	});
	$("body").on("click", "#gform_submit_button_2", function () {
		$("#gform_submit_button_2").val("");
		$("#gform_submit_button_2").css("background", "rgba(79, 79, 79, 0.35)");
	});

	if ($(".wpr_christies").length) {
		$("a").has("img").css("border", "none");
	}

	// $('.ubermenu-responsive-toggle').on('click tap', function (e) {
	// e.preventDefault();
	// $('.ubermenu').ubermenu('toggleMenuCollapse');

	// var deviceAgent = navigator.userAgent.toLowerCase();
	// var agentID = deviceAgent.match(/(iPad|iPhone|iPod)/i);
	// if (agentID) {
	//     $(this).toggleClass('ubermenu-responsive-toggle-open');
	//     $('#ubermenu-main-3-primary-2').toggleClass('ubermenu-responsive-collapse');
	// }
	//});

	$(".container").on("click tap", ".wpr-dealer-hours", function (e) {
		e.preventDefault();

		$(this).find(".wpr-dealer-hours-interval").toggle();
	});

	var isMobile = false; //initiate as false
	// device detection
	if (
		/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(
			navigator.userAgent
		) ||
		/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(
			navigator.userAgent.substr(0, 4)
		)
	) {
		isMobile = true;
	}

	if (isMobile) {
		// $('.mega_menu.center_full_width').find('.ubermenu-submenu').wrapInner('<ul class="submenu_container"></ul>');
	}

	/**
	 * Toggle search
	 */
	// jQuery('.ubermenu-responsive-toggle').on('ubermenutoggledopen', function (e) {
	//     if (!$('.menu_body_overlay').length) {
	//         $('body').append('<div class="menu_body_overlay"></div>');
	//         $('.menu_body_overlay').click(function () {
	//             jQuery('.ubermenu').ubermenu('toggleMenuCollapse');
	//             $('.menu_body_overlay').remove();
	//         });
	//     } else {
	//         $('.menu_body_overlay').remove();
	//     }
	// });

	$(".wpr-header-become-member").on("click tap", function (e) {
		e.preventDefault();
		if ($(window).width() > 600) {
			$("html, body").animate(
				{
					scrollTop: $(".wpr-coming-register").offset().top - 150,
				},
				1100
			);
		} else {
			$("html, body").animate(
				{
					scrollTop: $(".wpr-coming-register").offset().top - 90,
				},
				1100
			);
		}
	});
	$(".wpr-header-why-become-member a").on("click tap", function (e) {
		e.preventDefault();
		$("html, body").animate(
			{
				scrollTop: $(".site-content.comming-soon-page").offset().top - 50,
			},
			330
		);
	});
	$(".entry-content img[src*=pinimg]").css("display", "block");
	$(".entry-content img[src*=ytimg]").css("display", "none");

	// Function to initialize MutationObserver
	function observeTwitterWidget() {
		const target = document.getElementById("twitter-widget-0");
		if (!target) return;

		const observer = new MutationObserver(() => {
			customizeTweetMedia();
		});

		// Start observing
		observer.observe(target, {
			childList: true,
			subtree: true,
			attributes: true,
		});
	}

	// observeTwitterWidget();

	setTimeout(observeTwitterWidget, 1000);

	var customizeTweetMedia = function () {
		jQuery(".entry-content")
			.find("#twitter-widget-0")
			.contents()
			.find(".EmbeddedTweet-tweet")
			.css("display", "none");
		jQuery(".entry-content")
			.find(".EmbeddedTweet-tweet")
			.css("display", "none");
	};

	// Add top menu show/hide on scroll for devices
	function windowSize() {
		windowHeight = window.innerHeight ? window.innerHeight : $(window).height();
		windowWidth = window.innerWidth ? window.innerWidth : $(window).width();

		if (windowWidth < 697) {
			wpr_add_menu_hide_on_scroll();
		} else {
			wpr_sticky_offset();
		}
	}

	//Init Function of init it wherever you like...
	windowSize();

	// For example, get window size on window resize
	$(window).resize(function () {
		windowSize();

		if (windowWidth < 697) {
			wpr_add_menu_hide_on_scroll();
		} else {
			wpr_sticky_offset();
		}
	});
});

function wpr_add_menu_hide_on_scroll() {
	// Hide Header on on scroll down
	var didScroll;
	var lastScrollTop = 0;
	var delta = 5;
	var navbarHeight = $("#main_header").outerHeight();

	$(window).scroll(function (event) {
		didScroll = true;
	});

	setInterval(function () {
		if (didScroll) {
			lastScrollTop = hasScrolled(lastScrollTop, delta, navbarHeight);
			didScroll = false;
		}
	}, 250);
}

function hasScrolled(lastScrollTop, delta, navbarHeight) {
	var st = $(this).scrollTop();

	if (Math.abs(lastScrollTop - st) <= delta) return;

	if (st > lastScrollTop && st > navbarHeight) {
		// Scroll Down
		$("#main_header").addClass("wpr-nav-up");
	} else {
		// Scroll Up
		if (st + $(window).height() < $(document).height() && st < lastScrollTop) {
			$("#main_header").removeClass("wpr-nav-up");
		}
	}

	lastScrollTop = st;

	return lastScrollTop;
}

function wpr_sticky_offset() {
	if ($(".ui.sticky").length) {
		$sticky_offset = 120;
		if ($(window).width() > 696) {
			$(".ui.sticky").sticky({
				context: ".sticky_container",
				offset: $sticky_offset,
			});
		}
	}
}

jQuery(function () {
	/*if (jQuery().sticky_basic) {
        jQuery("#main_header").sticky_basic({topSpacing: 0});
        window.addEventListener("resize", resizeThrottler, false);
    }*/

	var resizeTimeout;

	function resizeThrottler() {
		// ignore resize events as long as an actualResizeHandler execution is in the queue
		if (!resizeTimeout) {
			resizeTimeout = setTimeout(function () {
				resizeTimeout = null;
				actualResizeHandler();

				// The actualResizeHandler will execute at a rate of 15fps
			}, 100);
		}
	}

	/* function actualResizeHandler() {
        if (jQuery("#main_header").length) {
            jQuery("#main_header").sticky_basic('update');
            if ($('.ui.sticky').length) {
                if ($(window).width() > 696) {
                    $('.ui.sticky')
                        .sticky({
                            context: '.sticky_container',
                            offset: $sticky_offset,
                        });
                } else {
                    $('.ui.sticky').sticky('destroy')
                }
            }
        }
    }*/

	jQuery(".single-post")
		.find("article.post")
		.find(".entry-content")
		.find(".fluid-width-video-wrapper")
		.each(function () {
			if (jQuery(".fluid-width-video-wrapper").length > 1) {
				if (jQuery(this).parent().is("div")) {
					jQuery(this).parent().hide();
				}
			}
		});
});

function getUrlParameter(sParam) {
	var sPageURL = decodeURIComponent(window.location.search.substring(1)),
		sURLVariables = sPageURL.split("&"),
		sParameterName,
		i;

	for (i = 0; i < sURLVariables.length; i++) {
		sParameterName = sURLVariables[i].split("=");

		if (sParameterName[0] === sParam) {
			return sParameterName[1] === undefined ? true : sParameterName[1];
		}
	}
}
