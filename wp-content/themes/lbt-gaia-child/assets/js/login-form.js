(function ($) {
	$(function () {
		const $lbtLoginFormOverlay = $("#lbt-login-form-overlay");
		const $lbtLoginFormSection = $(".lbt-auth-form-section");
		const $closeButton = $lbtLoginFormSection.find(".lbt-login-form-close");
		const $lbtLoginFormOpen = $(".lbt-show-login-form");
		const $lbtLoginForm = $lbtLoginFormSection.find("form");
		const $switchForms = $(".lbt-form-switch");
		const $passwordField = $("#lbt-register-form-section input[name=password]");
		const $confirmPasswordFIeld = $(
			"#lbt-register-form-section input[name=confirm_password]"
		);
		const $strengthIndicator = $(
			"#lbt-register-form-section .lbt-password-strength"
		);

		let strength;

		$(document).on(
			"keyup",
			"#lbt-register-form-section input[name=password], #lbt-register-form-section input[name=confirm_password]",
			function () {
				$strengthIndicator.removeClass("short bad good strong");

				let blacklistArray = [];

				blacklistArray = blacklistArray.concat(
					wp.passwordStrength.userInputDisallowedList()
				);

				strength = wp.passwordStrength.meter(
					$passwordField.val(),
					blacklistArray,
					$confirmPasswordFIeld.val()
				);

				switch (strength) {
					case 2:
						$strengthIndicator.addClass("bad").html(pwsL10n.bad);
						break;

					case 3:
						$strengthIndicator.addClass("good").html(pwsL10n.good);
						break;

					case 4:
						$strengthIndicator.addClass("strong").html(pwsL10n.strong);
						break;

					case 5:
						$strengthIndicator.addClass("short").html(pwsL10n.mismatch);
						break;

					default:
						$strengthIndicator.addClass("short").html(pwsL10n.short);
				}

				if ($(this).attr("type") === "password" && $(this).val().length === 0) {
					$strengthIndicator
						.removeClass("short bad good strong")
						.html("Strength Indicator");
				}

				if ($(this).attr("id") === "lbt-form-reg-password") {
					$(document).find(".lbt-passstrength-error-message").remove();
				}

				if (strength !== 4) {
					if ($(this).attr("id") === "lbt-form-reg-password") {
						$("#lbt-register-form input[name=password][type=password]").after(
							`<div class="lbt-passstrength-error-message">Please enter "Strong" password</div>`
						);
					}
					$("#lbt-register-form button[type=submit]").prop("disabled", true);
				} else {
					$("#lbt-register-form button[type=submit]").prop("disabled", false);
				}
			}
		);

		$switchForms.on("click", function (e) {
			e.preventDefault();

			const $section = $(this).parents("section");

			$section.addClass("lbt-login-form-hide");

			if ($section.attr("id") === "lbt-login-form-section") {
				$section.next().removeClass("lbt-login-form-hide");
			} else {
				$section.prev().removeClass("lbt-login-form-hide");
			}
		});

		const closeForm = function () {
			$lbtLoginFormOverlay.addClass("lbt-login-form-hide");
			if (lbtLoginForm.isSignUpTemplate) {
				$("#lbt-login-form-section").addClass("lbt-login-form-hide");
			} else {
				$(".lbt-auth-form-section").addClass("lbt-login-form-hide");
			}
			$lbtLoginForm.trigger("reset");
		};

		const showForm = function (e) {
			e.preventDefault();
			$lbtLoginFormOverlay.removeClass("lbt-login-form-hide");
			$("#lbt-login-form-section").removeClass("lbt-login-form-hide");
			$lbtLoginForm.trigger("reset");
		};

		$("#lbt-login-form").on("submit", function (e) {
			e.preventDefault();

			const $username = $(this).find("input[type=text]");
			const $password = $(this).find("input[type=password]");

			$username.parent().removeClass("lbt-login-form-required-field");
			$password.parent().removeClass("lbt-login-form-required-field");

			if ($username.val() == "") {
				$username.parent().addClass("lbt-login-form-required-field");
			}

			if ($password.val() == "") {
				$password.parent().addClass("lbt-login-form-required-field");
			}

			if ($username.val() == "" || $password.val() == "") {
				return;
			}

			const formData = new FormData($(this)[0]);
			const $errorMessageArea = $(".lbt-error-message");
			const $submitButton = $(this).find("button[type=submit]");
			const originalButtonText = $submitButton.text();
			const loadingButtonText = $submitButton.data("loadingText");

			$.ajax({
				url: lbtLoginForm.ajaxUrl,
				type: "POST",
				data: formData,
				contentType: false,
				processData: false,
				beforeSend: function () {
					$errorMessageArea.hide();
					$errorMessageArea.html("");
					$submitButton.prop("disabled", true);
					$submitButton.text(loadingButtonText);
				},
				success: function (response) {
					if (response.success) {
						$submitButton.text(response.data.message);
						if (response.data.redirect) {
							window.location = response.data.redirect;
						}
					}
					$submitButton.prop("disabled", false);
				},
				error: function (xhr, status, error) {
					const message = xhr.responseJSON.data;
					$errorMessageArea.html(message);
					$errorMessageArea.show();
					$submitButton.prop("disabled", false);
					$submitButton.text(originalButtonText);
				},
			});
		});

		$("#lbt-register-form").on("submit", function (e) {
			e.preventDefault();

			const formData = new FormData($(this)[0]);
			const $errorMessageArea = $(".lbt-register-error-message");
			const $submitButton = $(this).find("button[type=submit]");
			const originalButtonText = $submitButton.text();
			const loadingButtonText = $submitButton.data("loadingText");

			$errorMessageArea.hide();
			$errorMessageArea.html("");

			if (strength !== 4) {
				$errorMessageArea.html('Please enter "Strong" password');
				$errorMessageArea.show();
				return;
			}

			$.ajax({
				url: lbtLoginForm.ajaxUrl,
				type: "POST",
				data: formData,
				contentType: false,
				processData: false,
				beforeSend: function () {
					$errorMessageArea.hide();
					$errorMessageArea.html("");
					$submitButton.prop("disabled", true);
					$submitButton.text(loadingButtonText);
				},
				success: function (response) {
					if (response.success) {
						$errorMessageArea.text(response.data.message);
						$errorMessageArea.css("color", "#28a745");
						$errorMessageArea.css("border-color", "#28a745");
						$errorMessageArea.show();
						if (response.data.redirect) {
							setTimeout(function () {
								window.location = response.data.redirect;
							}, 1000);
						}
					}
					$submitButton.prop("disabled", false);
					$submitButton.text(originalButtonText);
				},
				error: function (xhr, status, error) {
					const message = xhr.responseJSON.data;
					$errorMessageArea.html(message);
					$errorMessageArea.show();
					$submitButton.prop("disabled", false);
					$submitButton.text(originalButtonText);
				},
			});
		});

		$(document).on("click", ".lbt-account-reactivation-email", function (e) {
			e.preventDefault();

			const formData = $(this).data("info");

			const $this = $(this);
			const originalButtonText = $this.text();

			$.ajax({
				url: lbtLoginForm.ajaxUrl,
				type: "POST",
				data: formData,
				beforeSend: function () {
					$errorMessageArea.hide();
					$errorMessageArea.html("");
					$this.prop("disabled", true);
					$this.text($this.data("loadingText"));
				},
				success: function (response) {
					if (response.success) {
						setTimeout(function () {
							$this.text(response.data.message);
							if (response.data.redirect) {
								window.location = response.data.redirect;
							}
						}, 500);
					}
					$this.prop("disabled", false);
					$this.text(originalButtonText);
				},
				error: function (xhr, status, error) {
					const message = xhr.responseJSON.data;
					$errorMessageArea.html(message);
					$errorMessageArea.show();
					$this.prop("disabled", false);
					$this.text(originalButtonText);
				},
			});
		});

		if ($(document).find(".alert").length) {
			setTimeout(function () {
				$(document).find(".alert").remove();
				window.history.replaceState(null, null, window.location.pathname);
			}, 500);
		}

		$(".lbt-show-register-form").on("click", function () {
			$("#lbt-register-form-section").removeClass("lbt-login-form-hide");
		});

		const cacheDealersForNonLoggedInUsers = function (ip) {
			$.ajax({
				url: lbtLoginForm.ajaxUrl,
				type: "POST",
				data: {
					action: "cache_dealers_for_brands",
					ip: ip,
				},
				error: function (xhr, status, error) {
					console.log(xhr.responseJSON.data);
				},
			});
		};

		async function getUserIP() {
			try {
				let response = await fetch("https://api.ipify.org?format=json");
				let data = await response.json();
				const dealersFetchedIP = localStorage.getItem(
					"dealersFetchedIP" + data.ip
				);
				console.log(data.ip);
				if ("1" != dealersFetchedIP) {
					cacheDealersForNonLoggedInUsers(data.ip);
					localStorage.setItem("dealersFetchedIP" + data.ip, "1");
				}
			} catch (error) {
				console.error("Error fetching IP address:", error);
			}
		}

		// getUserIP();

		$closeButton.on("click", closeForm);
		$lbtLoginFormOpen.on("click", showForm);
	});
})(jQuery);
