(function ($) {
	$(function () {
		function ucwords(str, force) {
			str = force ? str.toLowerCase() : str;
			return str.replace(/(\b)([a-zA-Z])/g, function (firstLetter) {
				return firstLetter.toUpperCase();
			});
		}

		$(".title-1 a,.title-3 a,.entry-title a").each(function () {
			var title_value = ucwords($(this).text(), true);
			$(this).text(title_value);
		});

		const $stickyNavigation = $("#main_header");
		let offset = 1;

		if ($("body").hasClass("home")) {
			offset = 81;
		}

		$(window).scroll(function () {
			if ($(this).scrollTop() > offset) {
				$stickyNavigation.addClass("sticky_nav");
			} else {
				$stickyNavigation.removeClass("sticky_nav");
			}
		});

		$(".nav-toggle,.nav-overlay").click(function () {
			$("#main_header").toggleClass("show_nav");
			$("body").toggleClass("nav_active");
		});

		$("iframe").attr("scrolling", "auto");

		$(".openCTApopup").click(function () {
			$(".cta_overlay_wrap").show();
		});
		$(".CTA-close").click(function () {
			$(".cta_overlay_wrap").hide();
		});

		$(".lbt-my-offers-link").on("click", function (e) {
			if (!lbtGlobal.isUserLoggedIn) {
				e.preventDefault();
				$("#lbt-login-form-section .lbt-form-switch").first().trigger("click");
			}
		});
	});
})(jQuery);
