(function () {
	let brandDealerPromotionsInterval;
	let brandDealerPromotionsTries = 0;

	const getBrandDealerPromotions = function () {
		brandDealerPromotionsTries++;
		const data = new URLSearchParams({
			brand_id: lbtCategoryClosestDealer.brandId,
			post_id: lbtCategoryClosestDealer.postId,
			is_single_post: lbtCategoryClosestDealer.isSinglePost,
			action: lbtCategoryClosestDealer.brandDealerPromotionsAction,
			__nonce: lbtCategoryClosestDealer.brandDealerPromotionsNonce,
		});

		fetch(lbtCategoryClosestDealer.ajaxUrl, {
			method: "POST",
			body: data,
			headers: {
				"Content-Type": "application/x-www-form-urlencoded",
			},
		})
			.then((response) => response.json())
			.then((response) => {
				if (response.success && typeof response.data !== "undefined") {
					clearInterval(brandDealerPromotionsInterval);
					document.querySelector(
						".gt-box.promotions .content-wrapper"
					).innerHTML = response.data.promotions_box;
					document.getElementById("dealer_block").innerHTML =
						response.data.dealer_box;

					if (lbtCategoryClosestDealer.isSinglePost) {
						document
							.querySelectorAll("#dealer_block .long")
							.forEach((element) => (element.style.display = "none"));
						document
							.querySelectorAll("#dealer_block .short")
							.forEach((element) => (element.style.display = "block"));
					}
					return;
				}

				if (brandDealerPromotionsTries > 3) {
					clearInterval(brandDealerPromotionsInterval);
					document.querySelector(
						".gt-box.promotions .content-wrapper"
					).innerHTML = `<div>Failed to find promotions</div>`;
				}
			})
			.catch((error) => {
				console.log(error);
				if (brandDealerPromotionsTries > 3) {
					document.querySelector(
						".gt-box.promotions .content-wrapper"
					).innerHTML = `<div>${error}</div>`;
					clearInterval(brandDealerPromotionsInterval);
				}
			});
	};

	document.addEventListener("DOMContentLoaded", function () {
		if (document.querySelector(".gt-box.promotions .content-wrapper")) {
			getBrandDealerPromotions();
		}
	});
})();
