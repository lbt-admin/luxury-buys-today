(function ($) {
	$(function () {
		const toggleLookbookPost = function () {
			if (!lbtLookbook.isUserLoggedIn) {
				$("#lbt-login-form-section .lbt-form-switch").first().trigger("click");
				return;
			}

			const $this = $(this);

			const postId = $this.data("postid");
			let dealershipId = false;

			if ($("#wpr-dealer-location").length) {
				dealershipId = $("#wpr-dealer-location").data("dealershipId");
			}

			$.ajax({
				url: lbtLookbook.ajaxUrl,
				type: "POST",
				data: {
					post_id: postId,
					dealership_id: dealershipId,
					action: lbtLookbook.toggleLookbookAction,
					__nonce: lbtLookbook.toggleLookbookNonce,
				},
				beforeSend: function () {
					$this.prop("disabled", true);
					$this.append(
						'<div class="lbt-cta-btn-spinner"><span class="fa fa-spinner fa-spin"></span></div>'
					);
				},
				success: function (response) {
					if (response.success) {
						if (response.data === "save") {
							if (lbtLookbook.isSinglePost) {
								$(document)
									.find(".lbt-toggle-lookbook-post")
									.each(function () {
										buttonLabel = "<span>REMOVE</span> FROM LOOKBOOK";
										$(this).html(buttonLabel);
									});
							} else {
								buttonLabel = "REMOVE FROM LOOKBOOK";
								$this.html(buttonLabel);
							}
						} else {
							if (lbtLookbook.isSinglePost) {
								$(document)
									.find(".lbt-toggle-lookbook-post")
									.each(function () {
										buttonLabel = "<span>SAVE</span> TO LOOKBOOK";
										$(this).html(buttonLabel);
									});
							} else {
								if (lbtLookbook.isLookbookPage) {
									$this.parents("article").first().remove();
								} else {
									buttonLabel = "SAVE TO LOOKBOOK";
									$this.html(buttonLabel);
								}
							}
						}
					}

					$this.prop("disabled", false);
				},
				error: function (xhr, status, error) {
					console.log(xhr.responseJSON.data);
					alert(xhr.responseJSON.data);
					$this.find(".lbt-cta-btn-spinner").remove();
					$this.prop("disabled", false);
				},
			});
		};

		$(document).on("click", ".lbt-toggle-lookbook-post", toggleLookbookPost);
	});
})(jQuery);
