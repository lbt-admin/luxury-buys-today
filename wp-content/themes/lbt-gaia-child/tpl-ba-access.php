<?php
/**
 * Template name: BA access
 *
 * @package Novus
 */

get_header(); ?>

<div id="primary" class="content-area">
    <main id="main" class="site-main" role="main">

		<?php while ( have_posts() ) : the_post(); ?>

            <h1 class="entry-title"><?php the_title(); ?></h1>

			<?php the_content(); ?>

		<?php endwhile; // end of the loop. ?>


    </main><!-- #main -->
</div><!-- #primary -->
<div id="secondary" class="widget-area" role="complementary">
    <aside id="text-2" class="widget widget_text">
        <div class="textwidget"><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/xsellcast.png"></div>
    </aside>
</div>

<?php get_footer(); ?>
