<?php
/**
 * Template name: BA prospects
 *
 * @package Novus
 */

get_header(); ?>

<div id="primary" class="content-area wpr-ba-prospects">
    <main id="main" class="site-main" role="main">

        <h1 class="entry-title"><?php the_title(); ?></h1>
		<?php
		$user = wp_get_current_user();
		if ( is_user_logged_in() && ( in_array( 'business_associate', (array) $user->roles ) || current_user_can( 'administrator' ) ) ) {
			?>
            <table id="wprlistingTable" class="table dataTable display" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th><?php _e( 'First Name', 'wpr-table' ); ?></th>
                    <th><?php _e( 'Last Name', 'wpr-table' ); ?></th>
                    <th><?php _e( 'Email', 'wpr-table' ); ?></th>
                    <th><?php _e( 'Photo', 'wpr-table' ); ?></th>
                    <th><?php _e( 'Phone', 'wpr-table' ); ?></th>
                    <th><?php _e( 'URL', 'wpr-table' ); ?></th>
                    <th><?php _e( 'Info', 'wpr-table' ); ?></th>
                </tr>
                </thead>
                <tfoot>
                <tr>
                    <th><?php _e( 'First Name', 'wpr-table' ); ?></th>
                    <th><?php _e( 'Last Name', 'wpr-table' ); ?></th>
                    <th><?php _e( 'Email', 'wpr-table' ); ?></th>
                    <th><?php _e( 'Photo', 'wpr-table' ); ?></th>
                    <th><?php _e( 'Phone', 'wpr-table' ); ?></th>
                    <th><?php _e( 'URL', 'wpr-table' ); ?></th>
                    <th><?php _e( 'Info', 'wpr-table' ); ?></th>
                </tr>
                </tfoot>
				<?php
				$user_id = get_current_user_id();
				if ( ! $user_id ) {
					return;
				}
				$ba_brand  = get_user_meta( $user_id, 'wpr_brand_association', true );
				$prospects = array();
				if ( $ba_brand ) {
					$args  = array(
						'role__in' => array( 'wpr_prospect' ),
						'meta_key' => 'simplefavorites',
					);
					$users = get_users( $args );

					if ( ! empty( $users ) ) {
						echo '<tbody>';
						foreach ( $users as $user ) {
							$user_favs = $user->simplefavorites;
							$brands    = wpr_obtain_category_id( $user_favs[0]['posts'] );
							if ( in_array( $ba_brand, $brands ) ) {
								$user_avatar         = get_avatar( $user->ID, 32 );
								$user_phone          = get_user_meta( $user->ID, 'phone', true );
								$prospector_info_url = get_permalink( get_page_by_path( 'prospect-info' ) ) . "?prospector=$user->ID";
								echo '<tr>';
								_e( sprintf( '<td>%s</td>', $user->first_name ) );
								_e( sprintf( '<td>%s</td>', $user->last_name ) );
								_e( sprintf( '<td>%s</td>', $user->user_email ) );
								_e( sprintf( '<td>%s</td>', $user_avatar ) );
								_e( sprintf( '<td>%s</td>', $user_phone ) );
								_e( sprintf( '<td>%s</td>', '' ) );
								_e( sprintf( '<td><a href="%s" target="_blank" class="datatable_buttons">%s</a></td>', esc_url( $prospector_info_url ), __( 'Info', 'wpr-table' ) ) );
								echo '</tr>';
							}
						}
						echo '</tbody>';
					}
				}
				?>
            </table>
		<?php } ?>
    </main><!-- #main -->
</div><!-- #primary -->

<?php get_footer(); ?>
