<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package Novus
 */

?>

</div>
</div><!-- #content -->

<?php get_sidebar( 'footer-full-width' ); ?>
<?php get_sidebar( 'footer' ); ?>
<footer id="colophon" class="site-footer" role="contentinfo">
	<div class="container">
		<div class="wpr-footer-widget-areas" class="footer-sidebar widget-area" role="complementary">
			<?php if ( is_active_sidebar( 'footer_column_1' ) ) : ?>
				<?php dynamic_sidebar( 'footer_column_1' ); ?>
			<?php endif; ?>
			<?php if ( is_active_sidebar( 'footer_column_2' ) ) : ?>
				<?php dynamic_sidebar( 'footer_column_2' ); ?>
			<?php endif; ?>
			<?php if ( is_active_sidebar( 'footer_column_3' ) ) : ?>
				<?php dynamic_sidebar( 'footer_column_3' ); ?>
			<?php endif; ?>
			<?php if ( is_active_sidebar( 'footer_column_4' ) ) : ?>
				<?php dynamic_sidebar( 'footer_column_4' ); ?>
			<?php endif; ?>
		</div>
		<?php get_template_part( 'partials/social-networks' ); ?>
		<div class="wpr-small-type">
			<?php
			printf(
				'&copy; %s %s',
				esc_html( gmdate( 'Y' ) ),
				esc_html__( 'Luxury Buys Today and Xsellcast.  All names, trademarks and images are copyright their respective owners. Luxury Buys Today is the quintessential Online News Magazine featuring the very best in luxury brand News, Events, & Promotions. Our luxury blog features Fashion, Home Décor, Auto, & Residential Properties. Our publisher provides you guidance, advice, and ratings on each product offer in the marketplace right now.', 'lbt' )
			);
			?>
		</div>
	</div><!-- .site-info -->
</footer><!-- #colophon -->
</div><!-- #page -->
<?php
	get_template_part( 'partials/modal', 'coming-soon' );
	get_template_part( 'partials/modal', 'thank-you' );
	get_template_part( 'partials/modal', 'confirmation' );
?>
<?php wp_footer(); ?>
</body>
</html>
