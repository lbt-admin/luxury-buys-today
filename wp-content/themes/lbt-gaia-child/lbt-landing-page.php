<?php
/**
 * Template name: Coming Soon
 *
 * @package Novus
 */
get_header( 'soon' ); ?>
<div class="wpr-vast-selection">
    <div class="wpr-coming-selection-title">
		<?php echo __( 'A vast selection of luxury brands and deals' ); ?>
    </div>
    <div class="wpr-coming-selection-content">
        <div><img src="<?php echo get_stylesheet_directory_uri() . '/assets/images/automobiles-homepage.png'; ?>"/></div>
        <div><img src="<?php echo get_stylesheet_directory_uri() . '/assets/images/fashion-homepage.png'; ?>"/></div>
        <div><img src="<?php echo get_stylesheet_directory_uri() . '/assets/images/home-decor-homepage.png'; ?>"/></div>
        <div><img src="<?php echo get_stylesheet_directory_uri() . '/assets/images/properties-homepage.png'; ?>"/></div>
    </div>
</div>
<div class="wpr-coming-lookbook">
    <div class="wpr-coming-selection-title">
		<?php echo __( 'Save your offers and deals to your Lookbook for later' ); ?>
    </div>
    <div class="wpr-coming-lookbook-content">
        <div class="wpr-coming-lookbook-profile"><img src="<?php echo get_stylesheet_directory_uri() . '/assets/images/home-lookbook-profile.png'; ?>"/></div>
        <div><img src="<?php echo get_stylesheet_directory_uri() . '/assets/images/coming_soon_lookbook@2x.jpg'; ?>"/></div>
    </div>
</div>
<div class="wpr-coming-salesman">
    <div class="wpr-coming-selection-title">
		<?php echo __( 'Get in contact directly with luxury salesman, stylists, and realtors with exclusive offers' ); ?>
    </div>
    <div class="wpr-coming-salesman-content">
        <div><img src="<?php echo get_stylesheet_directory_uri() . '/assets/images/coming_soon_stylist@2x.jpg'; ?>"/></div>
        <div><img src="<?php echo get_stylesheet_directory_uri() . '/assets/images/coming_soon_salesman@2x.jpg'; ?>"/></div>
    </div>
</div>
<?php
if ( ! is_user_logged_in() ) {
	?>
    <div class="wpr-coming-register">
        <div class="wpr-coming-selection-title">
			<?php echo __( 'Be the first to receive exclusive offers and local deals to your email' ); ?>
        </div>
        <div class="wpr-coming-register-content">
            <div class="wpr-coming-register-media"><?php echo __( 'REGISTER WITH SOCIAL MEDIA' ); ?></div>
            <div><?php echo do_shortcode( '[oa_social_login]' ); ?></div>
            <div class="wpr-coming-register-media-or"><?php echo __( 'OR REGISTER WITH EMAIL' ); ?></div>
            <div><?php echo do_shortcode( '[gravityform id="15" title="false" description="false" ajax="true"]' ); ?></div>
        </div>
    </div>
<?php } ?>
<?php get_footer(); ?>
