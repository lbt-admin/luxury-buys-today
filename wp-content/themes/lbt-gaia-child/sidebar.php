<?php
/**
 * The sidebar containing the main widget area.
 *
 * @package Novus
 */
$layout        = get_theme_mod( 'article_layout' );
$layouts       = array( 'grid', 'masonry', 'masonry-full', 'full-width' );
$is_layout     = in_array( $layout, $layouts );
$layout_single = get_theme_mod( 'article_layout_single' );
if ( ( $is_layout && is_home() ) || ( $is_layout && is_archive() ) || ( $is_layout && is_search() ) || ( $layout_single == 'full-width' && is_singular() ) ) {
	return;
}
if ( ! is_active_sidebar( 'sidebar-1' ) ) {
	return;
}
// WooCommerce check
if ( NOVUS_WOOCOMMERCE_ACTIVE ) {
	if ( is_cart() || is_checkout() || is_account_page() ) {
		return;
	}
}
?>

<div id="secondary" class="widget-area" role="complementary">
<!--    <div class="wpr-single-logo"><a href="--><?php //echo home_url(); ?><!--" title="--><?php //echo __('Luxury Buys Today'); ?><!--"><img src="--><?php //echo get_site_url(); ?><!--/wp-content/themes/lbt-gaia-child/assets/images/on-white-logo-verical@2x.png"/></a></div>-->
<?php do_action('display_cta_buttons_single_offer_overlay'); ?>

    <?php dynamic_sidebar( 'sidebar-1' ); ?>
</div><!-- #secondary -->
