<?php
/**
 * Template name: Complete your data template
 *
 * @package Novus
 */

get_header(); ?>

<div class="wpr-login-register">
    <div class="wpr-login-register-header">
        <div class="wpr-login-register-lbt-logo">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/LBT-logo.png"/>
        </div>
        <h2 class="header-2"><?php esc_html_e( 'Your daily source of luxury buys', 'lbt' ); ?></h2>
    </div>
    <div class="wpr-login-register-body">
        <div class="wpr-login-register-body-wrapper">
            <div class="wpr-login-register-body-content">
                <div class="wpr-complete-your-data">
                    <em><?php esc_html_e( 'Please complete your profile', 'lbt' ); ?></em>
					<?php
					if ( isset( $_COOKIE['wpr_user_brand_page'] ) && '' != $_COOKIE['wpr_user_brand_page'] ) {
						_e( '<p>' );
						esc_attr_e( sprintf( 'Thank you for your interest in %s.', esc_attr( $_COOKIE['wpr_user_brand_page'] ) ), 'lbt' );
						_e( '<br />' );
						esc_attr_e( 'You just need to fill in these 2 fields and we’ll process your request.', 'lbt' );
						_e( '</p>' );
					}
					?>
					<?php echo do_shortcode( '[gravityform id="8" title="false" description="false"]' ); ?>
                </div>
            </div>
        </div>
    </div>
</div>

<?php get_footer(); ?>
