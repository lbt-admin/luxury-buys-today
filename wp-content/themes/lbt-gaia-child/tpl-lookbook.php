<?php
/**
 * Template name: Lookbook
 *
 * @package Novus
 */
get_header();
?>
<div id="primary" class="content-area">
    <main id="main" class="site-main" role="main">
     <div class="user-lookbook-brands">
        <?php if (have_posts()) : ?>
         <div class="user-sidebar">
            <header class="page-header lookbook-page-header">
                <?php if (is_user_logged_in()) : ?>
                    <?php $user_id = get_current_user_id(); ?>
                    <a class="edit-profile-link" href="/my-profile/"><span class="genericon genericon-edit"></span> <span class="genericon-text"><?php echo __('My Profile'); ?></span></a>
                    <div class="wpr-lookbook-profile">
                        <div class="user-avatar" style="width: 100px;height: 100px;">
                            <?php
                            if (function_exists('get_wp_user_avatar')) {
                                echo get_wp_user_avatar($user_id, 96);
                            }
                            ?>
                        </div>

                        <div class="wpr-lookbook-profile-right">
                            <?php //the_title('<h1 class="page-title"> ', '</h1>'); ?>
                            <div class="user-details">
                                <ul class="user-info">
                                <?php if ('' !== get_the_author_meta('first_name', $user_id)) : ?>
                                <li>
                                
                                    <span class="genericon genericon-user"></span> <b><?php echo get_the_author_meta('first_name', $user_id); ?>
                                    <?php if ('' !== get_the_author_meta('last_name', $user_id)) : ?>
                                    <?php echo get_the_author_meta('last_name', $user_id); ?> 
                                    </b>
                                <?php endif; ?>
                                </li>
                                <?php endif; ?>
                                
                                <?php if ('' !== get_the_author_meta('user_email', $user_id)) : ?>
                                    <li> <span class="genericon genericon-mail"></span> <?php echo get_the_author_meta('user_email', $user_id); ?></li>
                                <?php endif; ?>
                                <?php if ('' !== get_the_author_meta('zip_code', $user_id)) : ?>
                                    <li><span class="genericon genericon-location"></span> <?php echo get_the_author_meta('zip_code', $user_id); ?> </li>
                                <?php endif; ?>
                                <?php if ('' !== get_the_author_meta('location', $user_id)) : ?>
                                    <li><?php echo get_the_author_meta('location', $user_id); ?></li>
                                <?php endif; ?>
                                <?php if ('' !== get_the_author_meta('phone', $user_id)) : ?>
                                    <li> <span class="genericon genericon-phone"></span> <?php echo get_the_author_meta('phone', $user_id); ?></li>
                                <?php endif; ?>
                                </ul>
                            </div>
                        </div>
                    </div>

                   
                <?php else : ?>
                    <?php the_title('<h1 class="page-title">', '</h1>'); ?>
                <?php endif; ?>
            </header><!-- .page-header -->
      </div><!-- /user-sidebar -->
      <div class="user-content">
           <div class="user-actions-wrap">
               <a <?php if(is_page('saved-brands')){  echo 'href="javascript:void(0);" class="current-action"';}else{echo 'href="'.get_home_url().'/saved-brands"';}?>>My saved brands</a>
               <a <?php if(is_page('lookbook')){echo 'href="javascript:void(0);" class="current-action"';}else{echo 'href="'.get_home_url().'/lookbook"';}?>>My lookbook</a>
            </div><!-- /.user-actions-wrap -->

            <div class="user-content-wrap">

            <?php if (is_user_logged_in()) : ?>

            <?php 
            if(is_page('lookbook'))
            {  
                get_template_part('partials/content', 'lookbook'); 
            ?> 
            <?php } else if(is_page('saved-brands')){
                   get_template_part('partials/content', 'brands');  
               }
            ?>

            <?php else : ?>

                <article class="text-center">
                    <div class="entry-content">
                        <p><?php _e('You need to be registered to add posts to Lookbook.', 'novus'); ?></p>
                        <p><a class="btn" id="wpr-register-lookbook" href="#"><?php echo __('Register'); ?></a>
                        </p>
                    </div>
                </article>

            <?php endif; ?>

        <?php endif; ?>
      <div><!-- /.user-content-wrap -->

      </div><!-- /.user-content -->
    </div><!-- /.user-lookbook-brands -->
    </main><!-- #main -->
</div><!-- #primary -->

<?php // get_sidebar();   ?>
<?php get_footer(); ?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script>
    $("#brand-filter").select2({
        placeholder: "ALL"
    });
    jQuery('#brand-filter').change(function () {
        window.location.href = "<?php get_bloginfo('url'); ?>/lookbook/?brand_id=" + jQuery('#brand-filter').val();
    });

    $("#cat-brand-filter").select2({
        placeholder: "ALL"
    });
    jQuery('#cat-brand-filter').change(function () {
        window.location.href = "<?php get_bloginfo('url'); ?>/saved-brands/?brand_cat_id=" + jQuery('#cat-brand-filter').val();
    });
   
</script>