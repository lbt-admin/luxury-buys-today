<?php
/**
 * Template name: BA prospector info
 *
 * @package Novus
 */

get_header(); ?>

<div id="primary" class="content-area wpr-ba-prospects">
    <main id="main" class="site-main" role="main">

        <h1 class="entry-title"><?php the_title(); ?></h1>
		<?php
		$user = wp_get_current_user();
		if ( is_user_logged_in() && ( in_array( 'business_associate', (array) $user->roles ) || current_user_can( 'administrator' ) ) ) {
			$prospector_id = absint( $_GET['prospector'] );
			if ( $prospector_id ) {
				$prospector = get_user_by( 'id', $prospector_id );
				$user_id    = get_current_user_id();

				if ( $prospector && $user_id && in_array( 'wpr_prospect', $prospector->roles ) ) {

					$ba_brand  = get_user_meta( $user_id, 'wpr_brand_association', true );
					$user_favs = $prospector->simplefavorites;

					if ( ! empty( $user_favs[0]['posts'] ) ) {
						$brands = wpr_obtain_category_id( $user_favs[0]['posts'] );
						if ( in_array( $ba_brand, $brands ) ) {
							$user_avatar = get_avatar( $prospector->ID, 32 );
							$user_phone  = get_user_meta( $prospector->ID, 'phone', true );
							?>
                            <table id="prospectorInfio" class="table dataTable display" cellspacing="0" width="100%">
                                <thead>
                                <tr>
                                    <th><?php _e( 'First Name', 'wpr-table' ); ?></th>
                                    <th><?php _e( 'Last Name', 'wpr-table' ); ?></th>
                                    <th><?php _e( 'Email', 'wpr-table' ); ?></th>
                                    <th><?php _e( 'Photo', 'wpr-table' ); ?></th>
                                    <th><?php _e( 'Phone', 'wpr-table' ); ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
									<?php
									_e( sprintf( '<td>%s</td>', $prospector->first_name ) );
									_e( sprintf( '<td>%s</td>', $prospector->last_name ) );
									_e( sprintf( '<td>%s</td>', $prospector->user_email ) );
									_e( sprintf( '<td>%s</td>', $user_avatar ) );
									_e( sprintf( '<td>%s</td>', $user_phone ) );
									?>
                                </tr>
                                </tbody>
                            </table>

                            <div id="displayProspectorLookbook">
                                <h3><?php _e( 'Prospect Lookbook', 'wpr-table' ); ?></h3>
                                <table id="wprlistingTable" class="table dataTable display" cellspacing="0" width="100%">
                                    <thead>
                                    <tr>
                                        <th><?php _e( 'Offer name', 'wpr-table' ); ?></th>
                                        <th><?php _e( 'Offer date', 'wpr-table' ); ?></th>
                                        <th><?php _e( 'Link to offer', 'wpr-table' ); ?></th>
                                    </tr>
                                    </thead>
                                    <tfoot>
                                    <tr>
                                        <th><?php _e( 'Offer name', 'wpr-table' ); ?></th>
                                        <th><?php _e( 'Offer date', 'wpr-table' ); ?></th>
                                        <th><?php _e( 'Link to offer', 'wpr-table' ); ?></th>
                                    </tr>
                                    </tfoot>
									<?php
									echo '<tbody>';
									$args = array(
										'post__in' => $user_favs[0]['posts']
									);

									$query = new WP_Query( $args );
									$posts = $query->posts;

									if ( $posts ) {
										foreach ( $posts as $lookbooks ) {
											$look_cat = wpr_obtain_category_id( array( $lookbooks->ID ) );
											if ( in_array( $ba_brand, $look_cat ) ) {
												echo '<tr>';
												_e( sprintf( '<td>%s</td>', $lookbooks->post_title ) );
												_e( sprintf( '<td>%s</td>', $lookbooks->post_date ) );
												_e( sprintf( '<td><a href="%s" target="_blank" class="datatable_buttons">%s</a></td>', esc_url( get_permalink( $lookbooks->ID ) ), __( 'See offer', 'wpr-table' ) ) );
												echo '</tr>';
											}
										}
									}
									wp_reset_postdata();
									echo '</tbody>';
									?>
                                </table>
                            </div>
						<?php }
					}
				} else {
					_e( 'You can\'t access this user.', 'wpr-table' );
				}
			}
		} ?>
    </main><!-- #main -->
</div><!-- #primary -->

<?php get_footer(); ?>
