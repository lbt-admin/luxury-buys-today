<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package Novus
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head <?php do_action( 'add_head_attributes' ); ?>>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
     <meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php wp_head(); ?>
	<script>$ = jQuery;</script>
</head>

<body <?php body_class(); ?>>
<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-THG6VNX');
</script>


<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-THG6VNX"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<div class="loader js-loader">
	<div class="loader-status"></div>
	<div class="loader-status-small"></div>
</div>

<div id="page" class="hfeed site">
	<a class="skip-link screen-reader-text" href="#content"><?php _e( 'Skip to content', 'novus' ); ?></a>

	<div id="page" class="hfeed site">
  <a class="skip-link screen-reader-text" href="#content"><?php _e('Skip to content', 'novus'); ?></a>
    <header id="main_header" class="site-header wwwerty" role="banner">
        <div class="header_container container">
           <div class="nav-overlay"></div>
           <div class="nav-toggle"><span></span><span></span><span></span></div>
            <div class="logo_container">
               <a href="/home-new" class="custom-logo-link" rel="home" itemprop="url">
                 <img width="600" height="180" src="https://luxurybuystoday-media.s3.amazonaws.com/media/uploads/2020/04/luxury-buys-today-png.png" class="custom-logo" alt="Luxury Buys Today" itemprop="logo">
              </a>
            </div>
            <div class="menu_container with_userrrr">
               <nav class="header-menu">
                  <ul>
                      <li class="has-submenu current-menu-item has-submenu-images"><a href="/broyhill-news/"><span>NEWS</span></a>
                         <ul class="submenu">
                            <li>
                                 <a href="/automotive/broyhill-auto-news/"><img src="https://luxurybuystoday-media.s3.amazonaws.com/media/uploads/2019/01/general_auto-1-350x200.jpg" alt=""><span>Broyhill Auto News</span></a>
                            </li>
                            <li>
                                 <a href="/fashion/broyhill-fashion-news/"><img src="https://luxurybuystoday-media.s3.amazonaws.com/media/uploads/2019/01/general_fashion-1-350x200.jpg" alt=""><span>Broyhill Fashion News</span></a>
                            </li>
                            <li>
                                 <a href="/home-decor/broyhill-home-decor-news/"><img src="https://luxurybuystoday-media.s3.amazonaws.com/media/uploads/2019/01/general_homedecor-1-350x200.jpg" alt=""><span>Broyhill Home Décor News</span></a>
                            </li>
                            <li>
                                 <a href="/properties/broyhill-properties-news/"><img src="https://luxurybuystoday-media.s3.amazonaws.com/media/uploads/2019/01/general_properties-2-350x200.jpg" alt=""><span>Broyhill Properties News</span></a>
                            </li>
                         </ul>
                     </li>
                      <li class="has-submenu"><a href="/my-offers/"><span>My Offers</span></a>
                          <ul class="submenu">
                              <li><a href="/all-offers/"><span>All Offers</span></a></li>
                              <li><a href="/my-offers/"><span>My Offers</span></a></li>
                          </ul>
                      </li>
                      <li class="has-submenu has-submenu-images"><a href="/automotive/"><span>Automobiles</span></a>
                        <ul class="submenu">
                            <li>
                                 <a href="/automotive/broyhill-auto-news/"><img src="https://luxurybuystoday-media.s3.amazonaws.com/media/uploads/2019/01/general_auto-1-350x200.jpg" alt=""><span>Broyhill Auto News</span></a>
                            </li>
                            <li>
                                 <a href="/automotive/audi/"><img src="https://luxurybuystoday-media.s3.amazonaws.com/media/uploads/2019/01/audi-300x133.jpg" alt=""><span>Audi</span></a>
                            </li>
                            <li>
                                 <a href="/automotive/bmw/"><img src="https://luxurybuystoday-media.s3.amazonaws.com/media/uploads/2019/01/bmw-300x133.jpg" alt=""><span>BMW</span></a>
                            </li>
                            <li>
                                 <a href="/automotive/cadillac/"><img src="https://luxurybuystoday-media.s3.amazonaws.com/media/uploads/2019/01/cadillac-300x133.jpg" alt=""><span>Cadillac</span></a>
                            </li>
                            <li>
                                 <a href="/automotive/ferrari/"><img src="https://luxurybuystoday-media.s3.amazonaws.com/media/uploads/2019/01/ferrari-300x133.jpg" alt=""><span>Ferrari</span></a>
                            </li>
                            <li>
                                 <a href="/automotive/jaguar/"><img src="https://luxurybuystoday-media.s3.amazonaws.com/media/uploads/2019/01/jaguar-300x133.jpg" alt=""><span>Jaguar</span></a>
                            </li>
                            <li>
                                 <a href="/automotive/lexus/"><img src="https://luxurybuystoday-media.s3.amazonaws.com/media/uploads/2019/01/lexus-300x133.jpg" alt=""><span>Lexus</span></a>
                            </li>
                            <li>
                                 <a href="/automotive/mercedes-benz/"><img src="https://luxurybuystoday-media.s3.amazonaws.com/media/uploads/2019/01/mercedes-300x133.jpg" alt=""><span>Mercedes-Benz</span></a>
                            </li>
                       </ul>
                      </li>
                      <li class="has-submenu has-submenu-images"><a href="/fashion/"><span>Fashion</span></a>
                         <ul class="submenu">
                            <li>
                                 <a href="/fashion/broyhill-fashion-news/"><img src="https://luxurybuystoday-media.s3.amazonaws.com/media/uploads/2019/01/general_fashion-1-350x200.jpg" alt=""><span>Broyhill Fashion News</span></a>
                            </li>
                            <li>
                                 <a href="/fashion/bloomingdales/"><img src="https://luxurybuystoday-media.s3.amazonaws.com/media/uploads/2019/01/bloomingdales-300x133.jpg" alt=""><span>Bloomingdales</span></a>
                            </li>
                            <li>
                                 <a href="/fashion/brooks-brothers/"><img src="https://luxurybuystoday-media.s3.amazonaws.com/media/uploads/2019/01/brooks_brothers-300x133.jpg" alt=""><span>Brooks Brothers</span></a>
                            </li>
                            <li>
                                 <a href="/fashion/neiman-marcus/"><img src="https://luxurybuystoday-media.s3.amazonaws.com/media/uploads/2019/01/neiman_marcus-300x133.jpg" alt=""><span>Neiman Marcus</span></a>
                            </li>
                            <li>
                                 <a href="/fashion/nordstrom/"><img src="https://luxurybuystoday-media.s3.amazonaws.com/media/uploads/2019/01/nordstrom-300x133.jpg" alt=""><span>Nordstrom</span></a>
                            </li>
                            <li>
                                 <a href="/fashion/saks-5th-avenue/"><img src="https://luxurybuystoday-media.s3.amazonaws.com/media/uploads/2019/01/saks-300x133.jpg" alt=""><span>Saks 5th Avenue</span></a>
                            </li>
                            <li>
                                 <a href="/fashion/tiffany-co/"><img src="https://luxurybuystoday-media.s3.amazonaws.com/media/uploads/2019/01/tiffany-300x133.jpg" alt=""><span>Tiffany & Co</span></a>
                            </li>
                       </ul>
                      </li>
                      <li class="has-submenu has-submenu-images"><a href="/home-decor/"><span>Home Decor</span></a>
                         <ul class="submenu">
                            <li>
                                 <a href="/home-decor/broyhill-home-decor-news/"><img src="https://luxurybuystoday-media.s3.amazonaws.com/media/uploads/2019/01/general_homedecor-1-350x200.jpg" alt=""><span>Broyhill Home Decor</span></a>
                            </li>
                            <li>
                                 <a href="/home-decor/ethan-allen/"><img src="https://luxurybuystoday-media.s3.amazonaws.com/media/uploads/2019/01/ethan_allen-300x133.jpg" alt=""><span>Ethan Allen</span></a>
                            </li>
                            <li>
                                 <a href="/home-decor/havertys/"><img src="https://luxurybuystoday-media.s3.amazonaws.com/media/uploads/2019/01/havertys-300x133.jpg" alt=""><span>Havertys</span></a>
                            </li>
                       </ul>
                      </li>
                      <li class="has-submenu has-submenu-images"><a href="/properties/"><span>Properties</span></a>
                         <ul class="submenu">
                            <li>
                                 <a href="/properties/broyhill-properties-news/"><img src="https://luxurybuystoday-media.s3.amazonaws.com/media/uploads/2019/01/general_properties-2-350x200.jpg" alt=""><span>Broyhill Properties News</span></a>
                            </li>
                            <li>
                                 <a href="/properties/christies-international/"><img src="https://luxurybuystoday-media.s3.amazonaws.com/media/uploads/2019/01/christies-300x133.jpg" alt=""><span>Christies International</span></a>
                            </li>
                            <li>
                                 <a href="/properties/sothebys-international/"><img src="https://luxurybuystoday-media.s3.amazonaws.com/media/uploads/2019/01/sothebys-300x133.jpg" alt=""><span>Sothebys International</span></a>
                            </li>
                       </ul>
                      </li>

                       <?php if (is_user_logged_in()) { ?>
                        <?php $current_user = wp_get_current_user(); ?>
                      <li class="has-submenu from-right"><a href="/lookbook/"><span><?php echo esc_html( $current_user->user_firstname);?></span></a>
                         <ul class="submenu">
                           <li class="menu_icon user_icon_before"><a href="/my-profile/"><span>Profile</span></a></li>
                           <li class="menu_icon lookbook_icon_before"><a href="/lookbook/"><span>Lookbook</span></a></li>
                           <li class="menu_icon heart_icon_before"><a href="/saved-brands/"><span>Saved Brands</span></a></li>
                           <li class="menu_icon logout_icon_before"><a href="<?php echo wp_logout_url( get_permalink() ); ?>"><i class="fa fa-sign-out"></i><span ><?php echo _e( 'Logout', 'lbtv2' ); ?></span></a>
                           </li>
                         </ul>
                      </li>
                      <!--<li class="lookbook_icon">
                      <a href="/lookbook/" tabindex="0"><img src="<?php /*echo get_stylesheet_directory_uri();*/?>/assets/images/lookbook-icon.svg" height="30" alt="" /><span class="ubermenu-target-title ubermenu-target-text">Lookbook</span></a>
                      </li>-->
					  <?php } else{?>
						<li class="login_register">
                         <a href="#"><span>LOGIN</span></a>
                      </li>
					  <?php } ?>
                  </ul>
               </nav>
            </div>
      </div>
   </header><!-- #masthead -->
	<?php
	//	if ( wpr_is_page_display_filter() ) {
	//		get_template_part( 'partials/content', 'panel' );
	//	}
	?>
	<?php
	$add_background_slider = '';
	if ( is_page( array( 'login', 'register' ) ) ) {
		$add_background_slider = ' wpr-login-register-slider';
	}
	$class = '';
	if ( is_user_logged_in() ) {
		$class = "with_user";
	}
	?>
	<div class="wpr-header-content">
		<div class="wpr-headers-content">
			<div class="wpr-header-full-launch">
				<?php echo __( 'FULL SITE LAUNCHING SOON' ); ?>
			</div>
			<div class="wpr-header-luxury-insp">
				<div><?php echo __( 'LUXURY INSPIRATION.' ); ?></div>
				<div><?php echo __( 'EXCLUSIVE OFFERS.' ); ?></div>
			</div>
			<div class="wpr-header-industries  <?php echo $class; ?>">
				<div><?php echo __( 'AUTOMOBILES' ); ?></div>
				<div><?php echo __( 'FASHION' ); ?></div>
				<div><?php echo __( 'HOME DECOR' ); ?></div>
				<div><?php echo __( 'PROPERTIES' ); ?></div>
			</div>
			<?php
			if ( ! is_user_logged_in() ) {
				?>
				<div class="wpr-header-become-member">
					<a href=""><?php echo __( 'BECOME AN EARLY MEMBER' ); ?></a>
				</div>
				<div class="wpr-header-why-become-member">
					<a href=""><?php echo __( 'Why Become a Member?' ); ?><br/><i class="fa fa-long-arrow-down"></i></a>
				</div>
				<?php
			}
			?>
		</div>
	</div>
	<div id="content" class="site-content comming-soon-page">

		<?php if ( get_theme_mod( 'featured_posts_display_all_pages', false ) || ( get_theme_mod( 'featured_posts_display', true ) && is_home() ) ) : ?>
			<?php novus_featured_posts( get_theme_mod( 'featured_posts_number', 5 ) ); ?>
		<?php endif; ?>

		<div class="container">
			<?php

			//			if ( wpr_is_page_display_filter() ) {
			//				get_template_part( 'partials/content', 'filter-button' );
			//			}

			if ( is_single() ) {
				do_action( 'wpr_homepage_editorial_section' );
			}
			?>
