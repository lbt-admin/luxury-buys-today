<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package Novus
 */

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
	<head <?php do_action( 'add_head_attributes' ); ?>>
		<!-- Pinterest verification -->
		<link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png">
		<link rel="apple-touch-icon" sizes="60x60" href="/apple-icon-60x60.png">
		<link rel="apple-touch-icon" sizes="72x72" href="/apple-icon-72x72.png">
		<link rel="apple-touch-icon" sizes="76x76" href="/apple-icon-76x76.png">
		<link rel="apple-touch-icon" sizes="114x114" href="/apple-icon-114x114.png">
		<link rel="apple-touch-icon" sizes="120x120" href="/apple-icon-120x120.png">
		<link rel="apple-touch-icon" sizes="144x144" href="/apple-icon-144x144.png">
		<link rel="apple-touch-icon" sizes="152x152" href="/apple-icon-152x152.png">
		<link rel="apple-touch-icon" sizes="180x180" href="/apple-icon-180x180.png">
		<link rel="icon" type="image/png" sizes="192x192"  href="/android-icon-192x192.png">
		<link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
		<link rel="icon" type="image/png" sizes="96x96" href="/favicon-96x96.png">
		<link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
		<link rel="manifest" href="/manifest.json">
		<meta name="msapplication-TileColor" content="#ffffff">
		<meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
		<meta name="theme-color" content="#ffffff">
		<meta name="msvalidate.01" content="7F0B9350E76BA9F393356F4D55665D2C" />
		<meta name="p:domain_verify" content="bdab659260d432a3e7b40f561a607920"/>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="facebook-domain-verification" content="at9om9xwcflbl3hte1vwfma6zqt35z" />
		<meta charset="<?php bloginfo( 'charset' ); ?>">
		<meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests">
		<link rel="profile" href="https://gmpg.org/xfn/11">
		<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
		<script>
			(function (w, d, s, o, f, js, fjs) {
				w['FCObject'] = o; w[o] = w[o] || function () { (w[o].q = w[o].q || []).push(arguments) };
				js = d.createElement(s), fjs = d.getElementsByTagName(s)[0];
				js.id = o; js.src = f; js.async = 1; fjs.parentNode.insertBefore(js, fjs);
			}(window, document, 'script', 'fc', 'https://tags.fullcontact.com/anon/fullcontact.js'));
			fc('init',"zkQSZc1CsPeShmdcjaFt6o4XO3vDxKfl");
		</script>
		<script type="text/javascript">
			(function (d) {
				var f = d.getElementsByTagName('SCRIPT')[0], p = d.createElement('SCRIPT');
				p.type = 'text/javascript';
				p.async = true;
				p.src = '//assets.pinterest.com/js/pinit.js';
				f.parentNode.insertBefore(p, f);
			}(document));
		</script>
		<script type="text/javascript">
			(function(c,l,a,r,i,t,y){
				c[a]=c[a]||function(){(c[a].q=c[a].q||[]).push(arguments)};
				t=l.createElement(r);t.async=1;t.src="https://www.clarity.ms/tag/"+i;
				y=l.getElementsByTagName(r)[0];y.parentNode.insertBefore(t,y);
			})(window, document, "clarity", "script", "fcq3w4a9ed");
		</script>
		<!-- Google Tag Manager -->
		<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
					new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
				j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
				'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
			})(window,document,'script','dataLayer','GTM-5ZGF4S3L');</script>
		<!-- End Google Tag Manager -->
		<!-- BEGIN GROOVE WIDGET CODE -->
		<script>
			!function(e,t){if(!e.groove){var i=function(e,t){return Array.prototype.slice.call(e,t)},a={widget:null,loadedWidgets:{},classes:{Shim:null,Embeddable:function(){this._beforeLoadCallQueue=[],this.shim=null,this.finalized=!1;var e=function(e){var t=i(arguments,1);if(this.finalized){if(!this[e])throw new TypeError(e+"() is not a valid widget method");this[e].apply(this,t)}else this._beforeLoadCallQueue.push([e,t])};this.initializeShim=function(){a.classes.Shim&&(this.shim=new a.classes.Shim(this))},this.exec=e,this.init=function(){e.apply(this,["init"].concat(i(arguments,0))),this.initializeShim()},this.onShimScriptLoad=this.initializeShim.bind(this),this.onload=void 0}},scriptLoader:{callbacks:{},states:{},load:function(e,i){if("pending"!==this.states[e]){this.states[e]="pending";var a=t.createElement("script");a.id=e,a.type="text/javascript",a.async=!0,a.src=i;var s=this;a.addEventListener("load",(function(){s.states[e]="completed",(s.callbacks[e]||[]).forEach((function(e){e()}))}),!1);var n=t.getElementsByTagName("script")[0];n.parentNode.insertBefore(a,n)}},addListener:function(e,t){"completed"!==this.states[e]?(this.callbacks[e]||(this.callbacks[e]=[]),this.callbacks[e].push(t)):t()}},createEmbeddable:function(){var t=new a.classes.Embeddable;return e.Proxy?new Proxy(t,{get:function(e,t){return e instanceof a.classes.Embeddable?Object.prototype.hasOwnProperty.call(e,t)||"onload"===t?e[t]:function(){e.exec.apply(e,[t].concat(i(arguments,0)))}:e[t]}}):t},createWidget:function(){var e=a.createEmbeddable();return a.scriptLoader.load("groove-script","https://eaa6db11-8447-477a-b3ce-5f5a5f16863f.widget.cluster.groovehq.com/api/loader"),a.scriptLoader.addListener("groove-iframe-shim-loader",e.onShimScriptLoad),e}};e.groove=a}}(window,document);
			window.groove.widget = window.groove.createWidget();
			window.groove.widget.init('eaa6db11-8447-477a-b3ce-5f5a5f16863f', {});
		</script>
		<!-- END GROOVE WIDGET CODE -->
		<!-- Facebook Pixel Code --><script>!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,document,'script','https://connect.facebook.net/en_US/fbevents.js');fbq('init', '193645979253059'); fbq('track', 'PageView');</script>
		<script async="async" src="//cdn.wishpond.net/connect.js?merchantId=44830&amp;writeKey=086dfb30f299" type="text/javascript"></script>
		<?php wp_head(); ?>
	</head>
	<body <?php body_class(); ?>>
		<!-- Ontraport Pixel -->
		<noscript><img height="1" width="1" src="https://www.facebook.com/tr?id=193645979253059&ev=PageView&noscript=1"/></noscript><!-- End Facebook Pixel Code -->
		<script src='https://optassets.ontraport.com/tracking.js' type='text/javascript' async='true' onload='_mri="137325",_mr_domain="lbttest2.ontraport.com",mrtracking();'></script>
		<!-- Ontraport Pixel -->
		<!-- Google Tag Manager (noscript) -->
		<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5ZGF4S3L"
		                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
		<!-- End Google Tag Manager (noscript) -->
		<?php if ( ! empty( $_GET['act_error'] ) || ! empty( $_GET['act_success'] ) || ( ! empty( $_GET['password'] ) && 'changed' === $_GET['password'] ) ) : //phpcs:ignore ?>
			<div role="alert" class="text-center rounded-0 alert alert-dismissible <?php echo ! empty( $_GET['act_error'] ) ? 'alert-danger' : ''; ?> <?php echo ! empty( $_GET['act_success'] ) || ! empty( $_GET['password'] ) ? 'alert-success' : ''; //phpcs:ignore ?>">
				<?php
				if ( isset( $_GET['password'] ) ) { // phpcs:ignore
					esc_html_e( 'Password is change. You can login', 'ppp' );
				} else {
					echo ! empty( $_GET['act_error'] ) ? esc_html( wp_unslash( $_GET['act_error'] ) ) : esc_html( wp_unslash( $_GET['act_success'] ) ); //phpcs:ignore
				}
				?>
			</div>
		<?php endif; ?>
		<div id="page" class="hfeed site">
			<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'lbt' ); ?></a>
			<header id="main_header" class="site-header wwwerty" role="banner">
				<div class="header_container container">
					<div class="nav-overlay"></div>
					<div class="nav-toggle"><span></span><span></span><span></span></div>
					<div class="logo_container">
						<a href="/" class="custom-logo-link" rel="home" itemprop="url">
							<img width="600" height="180"
								src="https://luxurybuystoday-media.s3.amazonaws.com/media/uploads/2020/04/luxury-buys-today-png.png"
								class="custom-logo" alt="Luxury Buys Today" itemprop="logo">
						</a>
					</div>
					<div class="menu_container with_userrrr">
						<nav class="header-menu">
							<?php
							wp_nav_menu(
								array(
									'theme_location' => 'primary',
									'container'      => false,
									'menu_class'     => false,
								)
							);
							?>
						</nav>
					</div>
				</div>
			</header>
			<?php
			if ( is_archive() ) {
				get_template_part( 'partials/blog-header', 'name' );
			}
			?>
			<div id="content" class="site-content">
				<?php
				if ( ! is_home() && ! is_front_page() ) :
					$container_class = '';

					if ( is_user_logged_in() ) {
						if ( is_page_template( 'templates/tmp-profile.php' ) ) {
							$container_class = 'lbt-my-profile lbt-profile';
						} elseif ( is_page_template( 'templates/tmp-lookbook.php' ) ) {
							$container_class = 'lbt-my-profile lbt-lookbook';
						} elseif ( is_page_template( 'templates/tmp-saved-brands.php' ) ) {
							$container_class = 'lbt-my-profile lbt-saved-brands';
						} elseif ( is_page_template( 'templates/tmp-my-offers.php' ) ) {
							$container_class = 'lbt-my-profile lbt-my-offers';
						}
					}
					?>
				<div class="container <?php echo esc_attr( $container_class ); ?>">
				<?php endif; ?>
