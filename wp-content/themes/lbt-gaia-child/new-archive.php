<?php
/**
 * The template for displaying archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package Novus
 */
get_header();

$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;

$category        = get_category( get_query_var( 'cat' ) );
$cat_id          = $current_category = $category->cat_ID;
$is_parent       = false;
$main_categories = array(
	'8',
	'13',
	'14',
	'17',
);
if ( in_array( $cat_id, $main_categories ) ) {
	$is_parent       = true;
	$childCategories = get_categories(
		array( 'parent' => $cat_id )
	);
	$brandsArr       = $childCategories;
}

$edgar_cat = array(
	'8'  => '36',
	'13' => '37',
	'14' => '38',
	'17' => '39',
);
/* New design updates for brand page top part under cunstructions*/
$thiscat        = get_category( get_query_var( 'cat' ) );
$current_catid  = $thiscat->cat_ID;
$current_parent = $thiscat->category_parent;
//  echo $current_parent;
if ( $current_parent == '8' ) {
	$news_cat_id = 36;
}
if ( $current_parent == '13' ) {
	$news_cat_id = 37;
}
if ( $current_parent == '14' ) {
	$news_cat_id = 38;
}
if ( $current_parent == '17' ) {
	$news_cat_id = 39;
}
if ( $current_parent == '' ) {
	$news_cat_id = $current_catid;
}

// echo 'News:'.$news_cat_id;
/* New design updates for brand page top part under cunstructions*/

if ( $category->parent > 0 ) {
	$cat_id = $category->parent;
}
$wpr_edd_id = $get_post_from = '';
if ( $cat_id ) {
	$get_post_from = $edgar_cat[ $cat_id ];
	if ( $get_post_from ) {
		$args = array(
			'cat'            => absint( $news_cat_id ),
			'posts_per_page' => 1,
		);

		$the_query = new WP_Query( $args );

		if ( $the_query->posts ) {
			$wpr_edd_id = $the_query->posts[0]->ID;
		}
	}
}
?>
<?php
if ( ! ( is_category( '36' ) || is_category( '37' ) || is_category( '8' ) || $is_parent ) ) :
	if ( ! is_user_logged_in() ) { ?>
	    <div class="archive-dealers-header">
			 <?php do_action( 'wpr_homepage_editorial_section2' ); ?>
		    <div class="login-box">
			    <h2>Looking for more <?php print( $category->name ); ?> news?</h2>
			    <button class="btn btn-black show-sign-up-form">Create Your account</button>
			    <p>Already have an account? <a href="#"class="show-login-form">Sign in</a></p>
		    </div>
	    </div>
		<?php
	}
endif;

$queries = apply_filters( 'lbt_get_posts', array() );
extract( $queries );
?>

	<div class="archive-top">
		<div class="container">
			<div class="row">
			     <?php if ( isset( $topQuery ) ) :
					   $count = 0;  ?>
				    <div class="col col-3 author-news">
					    <h2 class="main-title"><?php print( $category->name ); ?><span class="circle"></span> News</h2>
					    <div class="inner-wrap">
					     <?php while ( $topQuery->have_posts() ) :
						     $count++;
						     $topQuery->the_post();
						     if ( 1 === $count ) {
							     get_template_part( 'partials/content', 'today-news' );
						     } else {
							     get_template_part( 'partials/content', 'blog-list' );
						     }
					     endwhile;
					     wp_reset_postdata();
					     ?>
					    </div>
				    </div>
			     <?php
			     endif;
			     if ( $defaultQuery->have_posts() ) :
					$todays_offsers = array_slice( $defaultQuery->posts, 0, 5 );
					$blog_posts     = array_slice( $defaultQuery->posts, 5, 3 );
					$posts          = array_slice( $defaultQuery->posts, 8 );
			          if ( ! empty( $todays_offsers ) ) : ?>
						<div class="col col-3 today-offers">
					          <h2 class="main-title"><?php print( $category->name ); ?><span class="circle"></span> Today's Offers</h2>
					          <?php
							foreach ( $todays_offsers as $index => $post ) :
								setup_postdata( $post );
								if ( 0 === $index ) {
									get_template_part( 'partials/content', get_post_format() );
									echo '<h3 class="main-title small-size">' . $category->name . ' <span class="circle"></span> More Offers</h3>';
								} else {
									get_template_part( 'partials/content', 'blog-list' );
								}
							endforeach;
							wp_reset_postdata();
							?>
				          </div>
				     <?php
				     endif;
				     if ( ! empty( $blog_posts ) ) : ?>
				          <div class="col col-3 blog-posts">
							<?php
							if ( $is_parent || $current_catid == '36' || $current_catid == '37' || $current_catid == '38' || $current_catid == '39' ) {
								echo '<style>.category-desc{margin-top: 50px;}</style>';
							} else {
								$wpr_brand_image = get_term_meta( $current_catid, 'wpr_brand_image_url', true );
								?>
						          <div class="sale-agent-container all-info wpr-load-dealer-data1" data-brand-image="<?php echo $wpr_brand_image; ?>" data-cat-name="<?php echo $category->name; ?>"  data-cat="<?php echo $category->term_id; ?>" data-type="archive"></div>
							<?php } ?>
				               <div class="category-desc">
						          <h3>Luxury Buys Today - <?php echo $category->name; ?> News</h3>
							     <?php echo category_description( $current_parent ); ?>
					          </div>
					          <h2 class="main-title"><?php print( $category->name ); ?> <span class="circle"></span> Blog</h2>
					          <div class="inner-wrap">
				                    <?php
							     foreach ( $blog_posts as $index => $post ) :
							          setup_postdata( $post );
					                    if ( 0 === $index ) {
					                         get_template_part( 'partials/content', 'blog' );
								     } else {
								          get_template_part( 'partials/content', 'blog-list' );
								     }
							     endforeach;
							     wp_reset_postdata();
							     ?>
					          </div>
				          </div>
				     <?php
				     endif;
			     endif;
		          ?>
			</div><!-- .row -->
		</div>
	</div>
	<?php echo '<h3 class="main-title">' . $category->name . ' Offers</h3>'; ?>
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
		     <?php if ( $is_parent ) { ?>
		          <div class="filter-container">
			          <select id="brand-filter"  multiple="multiple" class="regular-text">
				          <option value="">All Brands</option>
							<?php
							foreach ( $brandsArr as $brandSingle ) {
								if ( isset( $_GET['brand_id'] ) && $_GET['brand_id'] != '' && $_GET['brand_id'] != 'null' ) {
									$brandsList = explode( ',', $_GET['brand_id'] );
									if ( in_array( $brandSingle->term_id, $brandsList ) ) {
										$selection = 'selected';
									} else {
										$selection = '';
									}
								} else {
									$selection = '';
								}
								echo '<option value="' . $brandSingle->term_id . '" ' . $selection . '>' . $brandSingle->name . '</option>';
							}
							?>
				     </select>
			     </div>
		     <?php } ?>
		     <?php
               do_action( 'wpr_display_dealer_offers' );
		     do_action( 'novus_loop_before' );
		     while ( have_posts() ) : the_post();
		          if( get_the_ID() ) {
			          get_template_part( 'partials/content', get_post_format() );
			     }
		          // End the loop.
		     endwhile;
		     do_action( 'novus_loop_after' );
		     the_posts_navigation(
			     array(
				     'prev_text' => __( '&lsaquo; Older posts', 'novus' ),
				     'next_text' => __( 'Newer posts &rsaquo;', 'novus' ),
			     )
		     );
		     ?>
		</main><!-- #main -->
	</div><!-- #primary -->
	<div id="remove-cpt-from-list" data-id="post-<?php echo absint( $wpr_edd_id ); ?>"></div>
<?php get_sidebar(); ?>
<?php get_footer(); ?>
<?php if ( $is_parent ) { ?>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
	<script>
		$("#brand-filter").select2({
			placeholder: "Filter by brand"
		});
		jQuery('#brand-filter').change(function () {
			window.location.href = "<?php get_bloginfo( 'url' ); ?>/<?php echo $category->slug; ?>/?brand_id=" + jQuery('#brand-filter').val();
		});
	</script>
	<?php
}
