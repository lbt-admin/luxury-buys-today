<?php
/**
 * Template name: My Offers
 *
 * @package Novus
 */
get_header();
$selected_cats = get_user_meta(get_current_user_id(), 'wpr_selected_brands', true);
$brandsArr = get_terms(array(
    'include' => $selected_cats,
        ));
?>
<div id="primary" class="content-area">
    <main id="main" class="site-main" role="main">
        <header class="page-header lookbook-page-header including-offer-page">
            <?php if (is_user_logged_in()) : ?>
                <?php $user_id = get_current_user_id(); ?>
                <div class="wpr-lookbook-profile">
                    <div class="user-avatar">
                        <?php
                        if (function_exists('get_wp_user_avatar')) {
                            echo get_wp_user_avatar($user_id, 96);
                        }
                        ?>
                    </div>

                    <div class="wpr-lookbook-profile-right">
                        <?php the_title('<h1 class="page-title"> ', '</h1>'); ?>
                        <a class="edit-profile-link btn btn--sm" href="/my-profile/"><span class="genericon genericon-edit"></span> <?php echo __('Edit Profile'); ?></a>
                    </div>
                </div>

                <div class="user-details">
                    <?php if ('' !== get_the_author_meta('first_name', $user_id)) : ?>
                        <span class="genericon genericon-user"></span> <?php echo get_the_author_meta('first_name', $user_id); ?>
                    <?php endif; ?>
                    <?php if ('' !== get_the_author_meta('last_name', $user_id)) : ?>
                        <?php echo get_the_author_meta('last_name', $user_id); ?>
                    <?php endif; ?>
                    <?php if ('' !== get_the_author_meta('user_email', $user_id)) : ?>
                        <span class="genericon genericon-mail"></span> <?php echo get_the_author_meta('user_email', $user_id); ?>
                    <?php endif; ?>
                    <?php if ('' !== get_the_author_meta('zip_code', $user_id)) : ?>
                        <span class="genericon genericon-location"></span> <?php echo get_the_author_meta('zip_code', $user_id); ?>
                    <?php endif; ?>
                    <?php if ('' !== get_the_author_meta('location', $user_id)) : ?>
                        <?php echo get_the_author_meta('location', $user_id); ?>
                    <?php endif; ?>
                    <?php if ('' !== get_the_author_meta('phone', $user_id)) : ?>
                        <span class="genericon genericon-phone"></span> <?php echo get_the_author_meta('phone', $user_id); ?>
                    <?php endif; ?>
                </div>
            <?php else : ?>
                <?php the_title('<h1 class="page-title">', '</h1>'); ?>
            <?php
            endif;
            ?>
        </header>

        <div class="filter-container">                  
            <select id="brand-filter"  multiple="multiple" class="regular-text">
                <option value="">All Brands</option>
                <?php
                foreach ($brandsArr as $brandSingle) {
                    if (isset($_GET['brand_id']) && $_GET['brand_id'] != '' && $_GET['brand_id'] != 'null') {
                        $brandsList = explode(',', $_GET['brand_id']);
                        if (in_array($brandSingle->term_id, $brandsList)) {
                            $selection = 'selected';
                        } else {
                            $selection = '';
                        }
                    } else {
                        $selection = '';
                    }
                    echo '<option value="' . $brandSingle->term_id . '" ' . $selection . '>' . $brandSingle->name . '</option>';
                }
                ?>
            </select></div>
        <?php
        $user_id = get_current_user_id();
        $my_brands = get_user_meta($user_id)['saved_deals_brands'];
        $saved_brands = array();

        foreach ($my_brands as $brand) {
            switch_to_blog(2);
            $category_id = get_post_meta( $brand , 'wpr_category_id', true );
            restore_current_blog();
            $saved_brands[] = $category_id;
        }

        if (!empty(wpr_selected_user_brands())) {
            $paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;

            $args = array(
                'post_type' => 'post',
                'orderby' => 'date',
                'order' => 'DESC',
                'paged' => $paged,
                'category__in' => $saved_brands,
            );

            $display_posts = new WP_Query($args);

            if ($display_posts->have_posts()) {

                do_action('novus_loop_before');

                while ($display_posts->have_posts()) {
                    $display_posts->the_post();
                    get_template_part('partials/content', get_post_format());
                }

                do_action('novus_loop_after');
                $GLOBALS['wp_query']->max_num_pages = $display_posts->max_num_pages;
                the_posts_navigation(array(
                    'prev_text' => __('&lsaquo; Older posts', 'novus'),
                    'next_text' => __('Newer posts &rsaquo;', 'novus'),
                ));
            } else {
                get_template_part('partials/content', 'none');
            }
            wp_reset_postdata();
        } else {
            ?>
            <header class="page-header">
                <h1 class="page-title"><?php _e('Add posts to your lookbook to start seeing their offers!', 'novus'); ?></h1>
            </header>
        <?php } ?>
    </main><!-- #main -->
</div><!-- #primary -->
<?php get_footer(); ?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script>
    $("#brand-filter").select2({
        placeholder: "Filter by brand"
    });
    jQuery('#brand-filter').change(function () {
        window.location.href = "<?php get_bloginfo('url'); ?>/my-offers/?brand_id=" + jQuery('#brand-filter').val();
    });
</script>