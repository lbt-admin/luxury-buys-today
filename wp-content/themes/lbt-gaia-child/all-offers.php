<?php
/**
 * Template name: All Offers
 *
 * @package Novus
 */
get_header();
$categories = array( 1, 8, 13, 14, 17, 7 );
$all_brands = get_terms(
	'category',
	array(
		'get'     => 'all',
		'exclude' => $categories,
	)
);
$brandsArr  = $all_brands
?>

<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">
		<div class="ed-container">
			<div class="row">
				<div class="col-lg-12 text-center">
					<?php get_template_part( 'partials/ed-info' ); ?>
				</div>
			</div>
		</div>
		<div class="filter-container all-offers-tpl">
			<h2>
			     <?php
				if ( is_user_logged_in() ) {
					  echo 'Filter saved offers by brand';
				} else {
					echo 'Filter offers by brand';
				}
				?>
			</h2>
			<select id="brand-filter"  multiple="multiple" class="regular-text">
				<option value="">All Brands</option>
				<?php
				foreach ( $brandsArr as $brandSingle ) {
					if ( isset( $_GET['brand_id'] ) && $_GET['brand_id'] != '' && $_GET['brand_id'] != 'null' ) {
						$brandsList = explode( ',', $_GET['brand_id'] );
						if ( in_array( $brandSingle->term_id, $brandsList ) ) {
							$selection = 'selected';
						} else {
							$selection = '';
						}
					} else {
						$selection = '';
					}
					echo '<option value="' . $brandSingle->term_id . '" ' . $selection . '>' . $brandSingle->name . '</option>';
				}
				?>
			</select>
		</div>
		<div class="replace-container">
			<div class="posts-wrapper-area">
				<?php do_action( 'novus_loop_before' ); ?>

				<?php
				$edgar_cat = array( '36', '37', '38', '39' );

				$wpr_edd_id = '';
				if ( ! empty( $paged ) ) {
					$paged = $paged;
				} elseif ( get_query_var( 'paged' ) ) {
					$paged = get_query_var( 'paged' );
				} elseif ( get_query_var( 'page' ) ) {
					$paged = get_query_var( 'page' );
				} else {
					$paged = 1;
				}
				if ( isset( $_GET['brand_id'] ) && $_GET['brand_id'] != '' && $_GET['brand_id'] != 'null' ) {
					$brandsList = explode( ',', $_GET['brand_id'] );


					$args = array(
						'post_type'    => 'post',
						'paged'        => $paged,
						'category__in' => $brandsList,
						'post__not_in' => array( $wpr_edd_id ),
					);
				} else {
					$args = array(
						'post_type'    => 'post',
						'paged'        => $paged,
						'category__not_in' => $edgar_cat,
						'post__not_in' => array( $wpr_edd_id ),
					);
				}
				$the_edd_query = new WP_Query( $args );
				$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

				$temp_args  = array(
					'posts_per_page' => 2,
					'paged' => $paged,
					'cat' => $edgar_cat
				);
				$black_ids = array();
				$temp_query = new WP_Query( $temp_args );
				if ( $temp_query->have_posts() ) :
					while ( $temp_query->have_posts() ) :
						$temp_query->the_post();
						$black_ids[] = get_the_ID();
					endwhile;
				endif;
				wp_reset_postdata();
				if ( $the_edd_query->have_posts() ) : /* Start the Loop */
					?>
					<?php
					global $post;
					$counter = 1;
					$black_ids_counter = 0;
					while ( $the_edd_query->have_posts() ) :
						$the_edd_query->the_post();
						if ( get_the_ID() != $wpr_edd_id ) {
							?>

							<?php
							/*
							Include the Post-Format-specific template for the content.
							* If you want to override this in a child theme, then include a file
							* called content-___.php (where ___ is the Post Format name) and that will be used instead.
							*/
							get_template_part( 'partials/content', get_post_format() );
							if ( $counter % 6 === 0 && $counter < 14 ) {
								$post = get_post( $black_ids[ $black_ids_counter ] );
								setup_postdata( $post );
								get_template_part( 'partials/content', 'post-news' );
								$black_ids_counter++;
							}
							$counter++;
						}
						?>

					<?php endwhile; ?>
				</div>
				<?php do_action( 'novus_loop_after' ); ?>

				<?php
				$GLOBALS['wp_query']->max_num_pages = $the_edd_query->max_num_pages;
				the_posts_navigation(
					array(
						'prev_text' => __( '&lsaquo; Prev Offers', 'novus' ),
						'next_text' => __( 'Newer offers &rsaquo;', 'novus' ),
					)
				);
				?>

				<?php
				wp_reset_postdata();
			else :
				?>

				<?php get_template_part( 'partials/content', 'none' ); ?>

			<?php endif; ?>
		</div>
	</main><!-- #main -->
</div><!-- #primary -->

<?php get_footer(); ?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script>
	$("#brand-filter").select2({
		placeholder: "Filter by brand"
	});
	jQuery('#brand-filter').change(function () {
		window.location.href = "<?php get_bloginfo( 'url' ); ?>/all-offers/?brand_id=" + jQuery('#brand-filter').val();
	});
</script>
