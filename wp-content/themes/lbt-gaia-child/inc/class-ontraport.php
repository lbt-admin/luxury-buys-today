<?php
/**
 * Handle all Ontraport requests
 *
 * Maybe at somepoint switch to Ontraport SDK.
 *
 * @package LBT
 */

namespace LBT;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Ontraport class
 */
class Ontraport {
	use \LBT\Traits\Singleton;

	/**
	 * Ontraport API Base
	 *
	 * @var string
	 */
	private $api_base = 'https://api.ontraport.com/1/';

	/**
	 * Ontraport api key
	 *
	 * @var string
	 */
	private $api_key = 'a03Nn6WLAYTugEy';

	/**
	 * Ontraport app id
	 *
	 * @var string
	 */
	private $app_id = '2_137325_S0z9TDx5I';

	/**
	 * Brand names with corresponding ontraport subscribe tag id
	 *
	 * @var array
	 */
	private $ontraport_brand_subscribe_tags_mapping = array(
		'Audi'                    => 4688,
		'Bloomingdales'           => 4694,
		'BMW'                     => 813,
		'Brooks Brothers'         => 4695,
		'Cadillac'                => 4689,
		'Christies International' => 4701,
		'Ethan Allen'             => 4699,
		'Ferrari'                 => 4690,
		'Havertys'                => 4700,
		'Jaguar'                  => 4691,
		'Lexus'                   => 4692,
		'Mercedes-Benz'           => 4693,
		'Neiman Marcus'           => 4696,
		'Nordstrom'               => 4697,
		'Saks 5th Avenue'         => 4698,
		'Sothebys International'  => 4702,
		'Tiffany'                 => 3782,
	);

	/**
	 * Brand names with corresponding ontraport unsubscribe tag id
	 *
	 * @var array
	 */
	private $ontraport_brand_unsubscribe_tags_mapping = array(
		'Audi'                    => 4706,
		'Bloomingdales'           => 4707,
		'BMW'                     => 4703,
		'Brooks Brothers'         => 4704,
		'Cadillac'                => 4705,
		'Christies International' => 4716,
		'Ethan Allen'             => 4708,
		'Ferrari'                 => 4709,
		'Havertys'                => 4710,
		'Jaguar'                  => 4711,
		'Lexus'                   => 4712,
		'Mercedes-Benz'           => 4713,
		'Neiman Marcus'           => 4714,
		'Nordstrom'               => 4715,
		'Saks 5th Avenue'         => 4717,
		'Sothebys International'  => 4718,
		'Tiffany'                 => 4719,
	);

	/**
	 * Ontraport request
	 *
	 * @param  string $endpoint The endpoint to access on Ontraport.
	 * @param  array  $args     List of args to send to Ontraport.
	 *
	 * @return WP_Error|array
	 */
	public function request( $endpoint, $args = array() ) {
		$default_request_args = array(
			'method'  => 'POST',
			'timeout' => '45',
			'headers' => array(
				'Accept'    => 'application/json',
				'Api-Key'   => $this->api_key,
				'Api-Appid' => $this->app_id,
			),
		);

		$request_args = wp_parse_args( $args, $default_request_args );

		$response = wp_remote_post( $this->api_base . $endpoint, $request_args );

		if ( is_wp_error( $response ) ) {
			return $response;
		}

		if ( $response['response']['code'] > 299 || $response['response']['code'] < 200 ) {
			return new \WP_Error(
				$response['response']['code'],
				$response['response']['message']
			);
		}

		$response = json_decode( wp_remote_retrieve_body( $response ), true );

		return $response['data'];
	}

	/**
	 * Save Contact in Ontraport
	 *
	 * @param  WP_User|int $user User object or user id.
	 *
	 * @return array|WP_Error
	 */
	public function save_contact( $user ) {
		if ( is_numeric( $user ) ) {
			$user = get_user_by( 'id', $user );
		}

		$contact_data = array(
			'firstname'  => $user->first_name,
			'lastname'   => $user->last_name,
			'email'      => $user->user_email,
			'home_phone' => get_user_meta( $user->ID, 'phone', true ),
			'sms_number' => get_user_meta( $user->ID, 'phone', true ),
			'zip'        => get_user_meta( $user->ID, 'zip_code', true ),
			'f2882'      => '183',
		);

		$ontraport_id = get_user_meta( $user->ID, 'wpr_ontraport_id', true );

		if ( $ontraport_id ) {
			$contact_data['id'] = $ontraport_id;
		}

		$method = $ontraport_id ? 'PUT' : 'POST';

		$args = array(
			'method' => $method,
			'body'   => $contact_data,
		);

		$response = $this->request( 'Contacts', $args );

		if ( ! $ontraport_id && ! is_wp_error( $response ) ) {
			update_user_meta( $user->ID, 'wpr_ontraport_id', $response['id'] );
		}

		return $response;
	}

	/**
	 * Subscribe to brand in ontraport by adding tags
	 *
	 * @param  int    $dealership_id The ontraport dealearship id.
	 * @param  string $action Subscribe or unsubscribe.
	 * @param  bool   $from_cta If request is from CTA Buttons.
	 * @param  int    $user_id false for current user.
	 *
	 * @return bool|WP_Error
	 */
	public function toggle_brand_subscription( $dealership_id, $action = 'subscribe', $from_cta = false, $user_id = false ) {
		$dealership_id = absint( $dealership_id );

		switch_to_blog( 1 );

		global $wpdb;

		$table_name = $wpdb->prefix . 'wpr_ontraport_dealerships';

		$sql = $wpdb->prepare( "SELECT brand FROM {$table_name} WHERE ontraport_id = %d", $dealership_id ); //phpcs:ignore

		$brand = $wpdb->get_var( $sql ); //phpcs:ignore

		restore_current_blog();

		if ( empty( $brand ) ) {
			return new \WP_Error(
				404,
				__( 'No brand found associated with the dealership', 'lbt' )
			);
		}

		$tag_mapping = $this->ontraport_brand_subscribe_tags_mapping;
		$remove_tag  = $this->ontraport_brand_unsubscribe_tags_mapping[ $brand ] ?? '';

		if ( 'subscribe' !== $action ) {
			$remove_tag  = $tag_mapping[ $brand ] ?? '';
			$tag_mapping = $this->ontraport_brand_unsubscribe_tags_mapping;
		}

		if ( ! isset( $tag_mapping[ $brand ] ) ) {
			return new \WP_Error(
				404,
				__( 'No match found for brand - tag combination', 'lbt' )
			);
		}

		$ontraport_tag_id = $tag_mapping[ $brand ];

		if ( ! $user_id ) {
			$userdata = wp_get_current_user();
		} else {
			$userdata = get_userdata( $user_id );
		}

		$user_email = $userdata->user_email;

		$conditions = array();

		$conditions[] = array(
			'field' => array( 'field' => 'email' ),
			'op'    => '=',
			'value' => array( 'value' => $user_email ),
		);

		$query_string = 'range=1&condition=' . rawurlencode( wp_json_encode( $conditions ) );

		$contacts = $this->request(
			'Contacts?' . $query_string,
			array(
				'method' => 'GET',
			)
		);

		if ( is_wp_error( $contacts ) ) {
			return $contacts;
		}

		if ( ! empty( $contacts ) ) {
			$contact_id = $contacts[0]['id'];

			$contact_tags = $contacts[0]['contact_cat'] ?? '';

			if ( empty( $contact_tags ) ) {
				$contact_tags = '*/*';
			} elseif ( ! empty( $remove_tag ) ) {
				$contact_tags = explode( '*/*' . $remove_tag . '*/*', $contact_tags );
				$contact_tags = implode( '*/*', $contact_tags );
			}

			if ( $from_cta && 'subscribe' === $action && false !== strpos( $contact_tags, '*/* ' . $ontraport_tag_id . '*/*' ) ) {
				return true;
			}

			$contact_tags .= $ontraport_tag_id . '*/*';

			$response = $this->request(
				'Contacts',
				array(
					'method' => 'PUT',
					'body'   => array(
						'id'          => $contact_id,
						'contact_cat' => $contact_tags,
					),
				)
			);

			if ( is_wp_error( $response ) ) {
				return $response;
			}

			return true;
		}

		return new \WP_Error(
			404,
			__( 'Contact not found for dealership', 'lbt' )
		);
	}

	/**
	 * Get dealership contact by email
	 *
	 * @param  string $contact_email Email of the dealership contact.
	 *
	 * @return object The ontraport dealership object with contact data
	 */
	public function get_dealership_by_contact_email( $contact_email ) {
		$condition[] = array(
			'field' => array( 'field' => 'email' ),
			'op'    => '=',
			'value' => array( 'value' => $contact_email ),
		);

		try {
			$user = $this->request(
				'Contacts?range=1&condition=' . rawurlencode( wp_json_encode( $condition ) ),
				array(
					'method' => 'GET',
				)
			);

			if ( ! is_array( $user ) || empty( $user[0] ) ) {
				return new \WP_Error(
					404,
					__( 'Contact not found' )
				);
			}

			$user = $user[0];

			$dealership_id = $user['f2963'];

			if ( empty( $dealership_id ) ) {
				return new \WP_Error(
					404,
					__( 'This Contact is not associated with any dealership' )
				);
			}

			$dealership = $this->request(
				'Dealership-ABCs?id=' . $dealership_id,
				array(
					'method' => 'GET',
				)
			);

			return $dealership;
		} catch ( \Exception $e ) {
			return new \WP_Error(
				$e->getCode(),
				$e->getMessage()
			);
		}
	}

	/**
	 * Save match between dealer and prospect
	 *
	 * @param  int $dealership_id The dealership ontraport id.
	 * @param  int $prospect_id   The prospect ontraport id.
	 *
	 * @return array|WP_Error
	 */
	public function save_match( $dealership_id, $prospect_id ) {
		return $this->request(
			'Matches',
			array(
				'body' => array(
					'f2257' => $dealership_id,
					'f2260' => $prospect_id,
				),
			)
		);
	}

	/**
	 * Get match between dealership and prospect
	 *
	 * @param  int $dealership_id Dealership ontraport id.
	 * @param  int $prospect_id Prospect ontraport id.
	 *
	 * @return array|WP_Error
	 */
	public function get_match_between_prospect_and_dealership( $dealership_id, $prospect_id ) {
		$condition[] = array(
			'field' => array( 'field' => 'f2257' ),
			'op'    => '=',
			'value' => array( 'value' => $dealership_id ),
		);

		$condition[] = 'AND';

		$condition[] = array(
			'field' => array( 'field' => 'f2260' ),
			'op'    => '=',
			'value' => array( 'value' => $prospect_id ),
		);

		$match = $this->request(
			'Matches?range=1&condition=' . rawurlencode( wp_json_encode( $condition ) ),
			array(
				'method' => 'GET',
			)
		);

		if ( is_wp_error( $match ) ) {
			return $match;
		}

		return $match[0];
	}

	/**
	 * Save activity
	 *
	 * @param  array $activity_data List of activity params.
	 *
	 * @return array|WP_Error
	 */
	public function save_activity( $activity_data ) {
		$activity_data = array_filter( $activity_data );

		return $this->request(
			'Activities',
			array(
				'body' => $activity_data,
			)
		);
	}

	/**
	 * Add Tag
	 *
	 * @param  array $tag_data List of tag params.
	 *
	 * @return array
	 */
	public function add_tag( $tag_data ) {
		$client = new \OntraportAPI\Ontraport( $this->app_id, $this->api_key );
		$object = new \OntraportAPI\Objects( $client );

		$tag_data['objectID'] = \OntraportAPI\ObjectType::CONTACT;

		return $object->addTag( $tag_data );
	}

	/**
	 * Sanitize options
	 *
	 * @param  array $option The option to be sanitized.
	 *
	 * @return array
	 */
	private function sanitize_options( $option ) {
		if ( is_array( $option ) ) {
			foreach ( $option as $field => $value ) {
				if ( is_numeric( $value ) ) {
					$option[ $field ] = (int) $value;
				} elseif ( is_email( $value ) ) {
					$option[ $field ] = sanitize_email( $value );
				} else {
					if ( is_array( $value ) ) {
						$option[ $field ] = $this->sanitize_options( $value );
					} else {
						$option[ $field ] = sanitize_text_field( wp_unslash( $value ) );
					}
				}
			}
		}

		return $option;
	}
}
