<?php
/**
 * Handle Post Image Check functionality
 *
 * @package LBT
 */

namespace LBT;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Post Validation
 */
class Post_Image_Check {
	use \LBT\Traits\Singleton;

	/**
	 * Private constructor
	 *
	 * @return  void
	 */
	private function initialize() {
		add_filter( 'cron_schedules', array( $this, 'cron_schedules' ) );
		add_action( 'lbt_validate_posts', array( $this, 'validate_posts' ) );

		$this->run_cron();
	}

	/**
	 * Add custom cron schedules
	 *
	 * @param array $schedules The existing schedules.
	 *
	 * @return  array
	 */
	public function cron_schedules( $schedules ) {
		$schedules['15_minutes'] = array(
			'interval' => 900,
			'display'  => __( 'Every 15 minutes' ),
		);

		return $schedules;
	}

	/**
	 * Run cron
	 *
	 * @return  void
	 */
	public function run_cron() {
		$next = wp_next_scheduled( 'lbt_validate_posts' );

		if ( ! $next ) {
			wp_schedule_event( time(), '15_minutes', 'lbt_validate_posts' );
		}
	}

	/**
	 * Clear cron schedules
	 *
	 * @return  void
	 */
	public function clear() {
		wp_clear_scheduled_hook( 'lbt_validate_posts' );
	}

	/**
	 * Validate posts
	 *
	 * @return  int
	 */
	public function validate_posts() {
		$total_posts         = wp_count_posts();
		$total_publish_posts = (int) $total_posts->publish;
		$posts_per_cron      = $total_publish_posts / 96;
		$posts_per_cron      = round( $posts_per_cron );

		$validated_posts = get_option( 'lbt_validated_posts', array() );

		$query_args = array(
			'post_type'      => 'post',
			'post_status'    => 'publish',
			'posts_per_page' => $posts_per_cron,
			'tag__not_in'    => array( 81 ),
		);

		if ( ! empty( $validated_posts ) ) {
			$query_args['post__not_in'] = $validated_posts;
		}

		$query = new \WP_Query( $query_args );

		if ( ! $query->have_posts() ) {
			delete_option( 'lbt_validated_posts' );
			return 0;
		}

		$total_deleted = 0;

		$skip            = array( 'christies-promotions', 'sothebys-promotions' );
		$skip_categories = Constants::BROYHILL_CHILD_CATEGORIES;

		foreach ( $query->posts as $p ) {
			if ( in_array( $p->post_name, $skip, true ) ) {
				$validated_posts[] = $p->ID;
				continue;
			}

			$post_categories = get_the_category( $p->ID );

			if ( ! empty( $post_categories ) ) {
				$post_categories_ids = wp_list_pluck( $post_categories, 'term_id' );

				if ( ! empty( array_intersect( $skip_categories, $post_categories_ids ) ) ) {
					$validated_posts[] = $p->ID;
					continue;
				}
			}

			$post_thumbnail = Post::get_thumbnail( $p->ID );

			if ( false !== $post_thumbnail ) {
				$post_thumbnail = Post::remote_file_exists( $post_thumbnail );

				if ( ! $post_thumbnail ) {
					$total_deleted++;
					wp_delete_post( $p->ID, true );
					continue;
				}
			} else {
				$total_deleted++;
				wp_delete_post( $p->ID, true );
				continue;
			}

			$validated_posts[] = $p->ID;
		}

		update_option( 'lbt_validated_posts', $validated_posts );
		update_option( 'lbt_validated_posts_last_run', time() );

		if ( ! $total_deleted ) {
			delete_option( 'lbt_validated_posts' );
		}

		return $total_deleted;
	}
}
