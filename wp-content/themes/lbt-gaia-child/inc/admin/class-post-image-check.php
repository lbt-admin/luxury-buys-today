<?php
/**
 * Post Image Check Dashboard
 *
 * @package PaceflexDashboards
 */

namespace LBT\Admin;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Dashboard class
 */
class Post_Image_Check {
	use \LBT\Traits\Singleton;

	/**
	 * Initialize class
	 *
	 * @return  void
	 */
	private function initialize() {
		add_action( 'admin_menu', array( $this, 'menu' ) );
		add_action( 'admin_enqueue_scripts', array( $this, 'load_assets' ) );
	}

	/**
	 * Load assets
	 *
	 * @param  string $hook The page where the script is run in admin.
	 *
	 * @return void
	 */
	public function load_assets( $hook ) {
		if ( 'posts_page_lbt-post-image-check' !== $hook ) {
			return;
		}

		wp_enqueue_script(
			'lbt-post-image-check',
			LBT_THEME_URL . 'assets/js/admin/post-image-check.js',
			array( 'jquery' ),
			LBT_THEME_VERSION,
			true
		);
	}

	/**
	 * Register menu page
	 *
	 * @return  void
	 */
	public function menu() {
		add_submenu_page(
			'edit.php',
			__( 'Post Image Check', 'lbt' ),
			__( 'Post Image Check', 'lbt' ),
			'administrator',
			'lbt-post-image-check',
			array( $this, 'dashboard_page' ),
			'dashicons-share-alt',
			3
		);
	}

	/**
	 * Show page settings
	 *
	 * @return  void
	 */
	public function dashboard_page() {
		require_once LBT_THEME_DIR . 'templates/admin/tmp-post-image-check.php';
	}

	/**
	 * Sanitize the fields before saving
	 *
	 * @param mixed<string|array> $option The option to be sanitized.
	 *
	 * @return  array
	 */
	private function sanitize_options( $option ) {
		if ( is_array( $option ) ) {
			foreach ( $option as $field => $value ) {
				if ( empty( $value ) ) {
					unset( $option[ $field ] );
					continue;
				}

				if ( is_numeric( $value ) ) {
					$option[ $field ] = $value;
				} else {
					if ( is_array( $value ) ) {
						$option[ $field ] = $this->sanitize_options( $value );
					} else {
						$option[ $field ] = sanitize_text_field( $value );
					}
				}
			}

			return array_filter( $option );
		}

		return $option;
	}
}
