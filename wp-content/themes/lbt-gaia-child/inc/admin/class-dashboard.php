<?php
/**
 * Admin dashboard
 *
 * @package LBT
 */

namespace LBT\Admin;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Dashboard class
 */
class Dashboard {
	use \LBT\Traits\Singleton;

	/**
	 * Initialize class
	 *
	 * @return  void
	 */
	private function initialize() {
		add_action( 'admin_init', array( $this, 'register_settings' ) );
		add_action( 'admin_menu', array( $this, 'menu' ) );
		add_action( 'admin_enqueue_scripts', array( $this, 'load_scripts' ) );
	}

	/**
     * Load scripts
     *
     * @param  string $location Slug where the action is executed.
     *
     * @return void
     */
    public function load_scripts( $location ) {
		if ( 'settings_page_lbt-offer-ratings' !== $location ) {
			return;
		}

        wp_enqueue_style(
            'lbt-offer-rating-dashboard',
            LBT_THEME_URL . 'assets/css/admin/dashboard.css',
			array(),
            LBT_THEME_VERSION
        );
    }

	/**
	 * Register menu page
	 *
	 * @return  void
	 */
	public function menu() {
		add_submenu_page(
			'options-general.php',
			__( 'Offer Rating', 'fso-stays' ),
			__( 'Offer Rating', 'fso-stays' ),
			'administrator',
			'lbt-offer-ratings',
			array( $this, 'dashboard_page' ),
		);
	}

	/**
	 * Show page settings
	 *
	 * @return  void
	 */
	public function dashboard_page() {
		$dashboard_settings = get_option( '_lbt_offer_rating', array() );

		require_once LBT_THEME_DIR . 'templates/admin/dashboard.php';
	}

	/**
	 * Register plugin settings
	 *
	 * @return  void
	 */
	public function register_settings() {
		register_setting(
			'lbt-offer-rating-settings-group',
			'_lbt_offer_rating',
			array(
				'sanitize_callback' => array( $this, 'sanitize_options' ),
			)
		);
	}

	/**
	 * Sanitize the fields before saving
	 *
	 * @param mixed<string|array> $option The option to be sanitized.
	 *
	 * @return  array
	 */
	public function sanitize_options( $option ) {
		if ( is_array( $option ) ) {
			foreach ( $option as $field => $value ) {
				if ( empty( $value ) ) {
					unset( $option[ $field ] );
					continue;
				}

				if ( is_numeric( $value ) ) {
					$option[ $field ] = $value;
				} else {
					if ( is_array( $value ) ) {
						$option[ $field ] = $this->sanitize_options( $value );
					} else {
						$option[ $field ] = sanitize_text_field( $value );
					}
				}
			}

			return array_filter( $option );
		}

		return $option;
	}
}
