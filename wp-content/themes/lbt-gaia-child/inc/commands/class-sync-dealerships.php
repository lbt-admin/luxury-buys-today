<?php
/**
 * Old posts removal command Class
 *
 * @package LBT
 */

namespace LBT\Commands;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * CLI Command
 */
class Sync_Dealerships {
    /**
     * Remove old posts command
     *
     * @param  array $args       Command args.
     * @param  array $assoc_args Some other command args.
     *
     * @return void               
     */
    public function sync_dealerships( $args, $assoc_args ) {
        if ( defined( 'WP_CLI' ) && WP_CLI ) {
            \WP_CLI::success( 'Sync dealerships started. Please wait while the process run...' );
            try {
                $dealer = \LBT\Dealer::get_instance();
                $dealer->sync_dealerships();
            } catch ( \Exception $e ) {
                \WP_CLI::warning( $e->getMessage() );
            }

            \WP_CLI::success( 'Dealerships synched.' );
        }
    }

}