<?php
/**
 * WP CLI Commands Class
 *
 * @package LBT
 */

namespace LBT\Commands;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * CLI Command
 */
class WP_CLI_Commands {
	use \LBT\Traits\Singleton;

    /**
     * Initialize class
     *
     * @return void 
     */
    private function initialize() {
        if ( defined( 'WP_CLI' ) && WP_CLI ) {
            \WP_CLI::add_command( 'old-post-removal', Old_Posts_Removal_Command::class );
            \WP_CLI::add_command( 'sync-dealerships', Sync_Dealerships::class );
        }
    }
}
