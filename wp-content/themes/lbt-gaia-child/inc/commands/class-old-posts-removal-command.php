<?php
/**
 * Old posts removal command Class
 *
 * @package LBT
 */

namespace LBT\Commands;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * CLI Command
 */
class Old_Posts_Removal_Command {
    /**
     * Remove old posts command
     *
     * @param  array $args       Command args.
     * @param  array $assoc_args Some other command args.
     *
     * @return void               
     */
    public function remove_old_posts( $args, $assoc_args ) {
        if ( defined( 'WP_CLI' ) && WP_CLI ) {
            \WP_CLI::success( 'Old posts removal started. Please wait while the process run...' );
            $old_post_removal = \LBT\Old_Post_Removal::get_instance();
            $old_post_removal->remove_old_posts();

            \WP_CLI::success( 'Old posts successfully removed.' );
        }
    }

}