<?php
/**
 * Remove old posts CRON
 *
 * @package LBT
 */

namespace LBT;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Post Validation
 */
class Old_Post_Removal {
	use \LBT\Traits\Singleton;

	/**
	 * Period for which the posts to be removed.
	 *
	 * @var string
	 */
	protected $before_period = '3 months ago';

	/**
	 * When the script starts running
	 *
	 * @var integer
	 */
	protected $run_script_from_date = 20230201;

	/**
	 * Private constructor
	 *
	 * @return  void
	 */
	private function initialize() {
		add_filter( 'cron_schedules', array( $this, 'cron_schedules' ) );
		add_action( 'lbt_remove_old_posts_script', array( $this, 'remove_old_posts' ) );

		if ( $this->run_script() ) {
			$this->run_cron();
		}
	}

	/**
	 * Add custom cron schedules
	 *
	 * @param array $schedules The existing schedules.
	 *
	 * @return  array
	 */
	public function cron_schedules( $schedules ) {
		$schedules['monthly'] = array(
			'interval' => MONTH_IN_SECONDS,
			'display'  => __( 'Monthly' ),
		);

		$schedules['1_minutes'] = array(
			'interval' => 60,
			'display'  => __( 'Every Minute' ),
		);

		return $schedules;
	}

	/**
	 * Check if the script should run
	 *
	 * @return boolean
	 */
	protected function run_script() {
		if ( (int) gmdate( 'Ymd' ) < $this->run_script_from_date ) {
			return false;
		}

		return true;
	}

	/**
	 * Run cron
	 *
	 * @return  void
	 */
	public function run_cron() {
		$next = wp_next_scheduled( 'lbt_remove_old_posts_script' );

		if ( isset( $_GET['clear_old_posts_cron'] ) || ( $next && ! $this->run_script() ) ) {
			$this->clear();
			return;
		}

		if ( ! $next ) {
			wp_schedule_event( time(), 'hourly', 'lbt_remove_old_posts_script' );
		}
	}

	/**
	 * Clear cron schedules
	 *
	 * @return  void
	 */
	public function clear() {
		wp_clear_scheduled_hook( 'lbt_remove_old_posts_script' );
	}

	/**
	 * Remove old posts
	 *
	 * @return  int
	 */
	public function remove_old_posts() {
		if ( ! $this->run_script() ) {
			return;
		}

		$args = array(
			'post_type'        => 'post',
			'posts_per_page'   => -1,
			'post_status'      => 'publish',
			'category__not_in' => Constants::BROYHILL_CHILD_CATEGORIES,
			'fields'           => 'ids',
			'meta_query'       => array(
				array(
					'key'     => 'simplefavorites',
					'compare' => 'NOT EXISTS',
				),
				array(
					'key'     => 'lbt_saved_offers',
					'compare' => 'NOT EXISTS',
				),
				array(
					'key'     => 'cta_clicked',
					'compare' => 'NOT EXISTS',
				),
			),
			'date_query' => array(
				array(
					'before' => '3 months ago',
				),
			),
		);

		$posts_query = new \WP_Query( $args );

		if ( ! $posts_query->have_posts() ) {
			return;
		}

		foreach ( $posts_query->posts as $post_id ) {
			if ( has_post_thumbnail( $post_id ) ) {
				wp_delete_attachment( get_post_thumbnail_id( $post_id ), true );
			}
			wp_delete_post( $post_id, true );
		}
	}
}
