<?php
/**
 * Handles category logic and wrappers
 *
 * @package LBT
 */

namespace LBT;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Category class
 */
class Category {
	use \LBT\Traits\Singleton;

	/**
	 * Fetch main categories, that is, categories with no parents.
	 *
	 * @param int   $parent Categories with given parent id or 0 main.
	 * @param bool  $return_ids Return only list of ids or object.
	 * @param array $exclude List of categories to be excluded form the return result.
	 *
	 * @return array
	 */
	public static function get_categories( $parent = 0, $return_ids = false, $exclude = array() ) {
		$parent = absint( $parent );

		$key = md5( 'lbt-cats-' . $parent . $return_ids . implode( '', $exclude ) );

		$categories = wp_cache_get( $key, 'lbt-categories' );

		if ( $categories ) {
			return $categories;
		}

		$args = array(
			'orderby' => 'name',
			'order'   => 'ASC',
			'parent'  => $parent,
		);

		if ( ! empty( $exclude ) ) {
			$args['exclude'] = $exclude;
		}

		$categories = get_categories( $args );

		if ( $return_ids ) {
			$categories = wp_list_pluck( $categories, 'term_id' );
		}

		wp_cache_set( $key, $categories, 'lbt-categories' );

		return $categories;
	}

	/**
	 * Get all main categories children
	 *
	 * @param  array $main_categories List of main categories or empty to get the default.
	 *
	 * @return array
	 */
	public static function get_main_categories_children( $main_categories = array() ) {
		if ( empty( $main_categories ) ) {
			$main_categories = self::get_categories( 0, true, array_merge( Constants::EXCULDE_CATEGORIES, array( 1 ) ) );
		}

		$children = array();

		foreach ( $main_categories as $main_cat ) {
			$main_cat_children = self::get_categories( $main_cat );

			$children = array_merge( $children, $main_cat_children );
		}

		return $children;
	}

	/**
	 * Get broyhill child categories
	 *
	 * @param bool  $return_ids Return only list of ids or object.
	 * @param array $exclude List of categories to be excluded form the return result.
	 *
	 * @return array
	 */
	public static function get_broyhill_categories( $return_ids = false, $exclude = array() ) {
		return self::get_categories( Constants::BROYHILL_PARENT_CATEGORY, $return_ids, $exclude );
	}

	/**
	 * Check if given category is is broyhill category
	 *
	 * @param  int $category_id The category id.
	 *
	 * @return bool
	 */
	public static function is_broyhill_category( $category_id ) {
		$broyhill_categories = self::get_broyhill_categories( true );

		return Constants::BROYHILL_PARENT_CATEGORY === $category_id || in_array( $category_id, $broyhill_categories, true );
	}

	/**
	 * Category page left column posts query
	 *
	 * @param  WP_Term $current_category The category term.
	 * @param  int     $current_page     Page number.
	 *
	 * @return WP_Query
	 */
	public static function get_left_column_posts( $current_category, $current_page ) {
		$key = 'lbt-category-left' . $current_category->term_id . $current_category->category_parent . $current_page;

		$query = get_transient( $key );

		if ( $query ) {
			return $query;
		}

		$query = new \WP_Query(
			array(
				'post_type'      => 'post',
				'cat'            => $current_category->term_id,
				'posts_per_page' => 5,
				'paged'          => $current_page,
				'post_status'    => 'publish',
				'tax_query'      => array( //phpcs:ignore
					array(
						'taxonomy' => 'post_tag',
						'field'    => 'slug',
						'operator' => 'NOT IN',
						'terms'    => array( Constants::PROMOTIONS_TAG ),
					),
				),
			)
		);

		set_transient( $key, $query, DAY_IN_SECONDS );

		return $query;
	}

	/**
	 * Category page promotions column posts query
	 *
	 * @param  WP_Term $current_category The category term.
	 * @param  int     $current_page     Page number.
	 *
	 * @return WP_Query
	 */
	public static function get_promotions_column_posts( $current_category, $current_page ) {
		$dealership = Dealer_Locator::get_nearby_dealer( $current_category->term_id );

		$remote_promotions = array();

		if ( ! is_wp_error( $dealership ) && function_exists( 'lbt_get_automotive_post_ids' ) ) {
			$automotive_dynamic_post_ids = lbt_get_automotive_post_ids();
			$automotive_target_href      = get_permalink( $automotive_dynamic_post_ids[ $current_category->slug ] );

			$remote_promotions = lbt_feed_archive_promotion_posts(
				$current_category,
				'dealership-' . $dealership['id'],
				$current_page,
				5,
				$automotive_target_href
			);

		}

		$total_remote_promotions = count( $remote_promotions );

		if ( $total_remote_promotions < 5 ) {
			$query = new \WP_Query(
				array(
					'post_type'      => 'post',
					'cat'            => $current_category->term_id,
					'tag'            => Constants::PROMOTIONS_TAG,
					'posts_per_page' => ( 5 - $total_remote_promotions ),
					'paged'          => $current_page,
					'post_status'    => 'publish',
				)
			);

			if ( $query->have_posts() ) {
				foreach ( $query->posts as $promotion_post ) {
					$remote_promotions[ 'internal-' . $promotion_post->ID ] = $promotion_post;
				}
			}
		}

		return $remote_promotions;
	}

	/**
	 * Category page broyhill column posts query
	 *
	 * @param  WP_Term $broyhill_corresponding_category The category term.
	 * @param  int     $current_page Page number.
	 *
	 * @return WP_Query
	 */
	public static function get_broyhill_column_posts( $broyhill_corresponding_category, $current_page ) {
		$key = 'lbt-category-broyhill' . $broyhill_corresponding_category . $current_page;

		$query = get_transient( $key );

		if ( $query ) {
			return $query;
		}

		$query_args = array(
			'post_type'      => 'post',
			'paged'          => $current_page,
			'order'          => 'DESC',
			'order_by'       => 'date',
			'posts_per_page' => 5,
			'post_status'    => 'publish',
			'tax_query'      => array(), //phpcs:ignore
		);

		$query_args['cat']         = $broyhill_corresponding_category;
		$query_args['tax_query'][] = array(
			'taxonomy' => 'post_tag',
			'field'    => 'slug',
			'operator' => 'NOT IN',
			'terms'    => array( Constants::PROMOTIONS_TAG ),
		);

		$query = new \WP_Query( $query_args );

		set_transient( $key, $query, DAY_IN_SECONDS );

		return $query;
	}

	/**
	 * Category page broyhill column posts query
	 *
	 * @param  int   $broyhill_corresponding_category Broyhill category id.
	 * @param  array $args Query arguments.
	 *
	 * @return WP_Query
	 */
	public static function get_broyhill_posts( $broyhill_corresponding_category, $args = array() ) {
		$default_args = array(
			'post_type'      => 'post',
			'order'          => 'DESC',
			'order_by'       => 'date',
			'posts_per_page' => 5,
			'post_status'    => 'publish',
			'tax_query'      => array(), //phpcs:ignore
		);

		$query_args = wp_parse_args( $args, $default_args );

		$query_args['cat']         = $broyhill_corresponding_category;
		$query_args['tax_query'][] = array(
			'taxonomy' => 'post_tag',
			'field'    => 'slug',
			'operator' => 'NOT IN',
			'terms'    => array( Constants::PROMOTIONS_TAG ),
		);

		return new \WP_Query( $query_args );
	}
}
