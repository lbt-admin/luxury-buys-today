<?php
if ( ! defined( 'ABSPATH' ) ) {
	header( 'Status: 403 Forbidden' );
	header( 'HTTP/1.1 403 Forbidden' );
	exit;
}

if ( ! class_exists( 'WPR_Run_Text_Spin' ) ) {
	class WPR_Run_Text_Spin extends WP_Background_Process {
		/**
		 * @var string
		 */
		protected $action = 'wpr_run_text_spin_post';

		/**
		 * Cron_hook_identifier
		 *
		 * @var mixed
		 * @access protected
		 */
		protected $cron_hook_identifier;

		/**
		 * Cron_interval_identifier
		 *
		 * @var mixed
		 * @access protected
		 */
		protected $cron_interval_identifier;

		/**
		 * Initiate new background process
		 */
		public function __construct() {
			parent::__construct();

			$this->cron_hook_identifier     = $this->identifier . '_cron';
			$this->cron_interval_identifier = $this->identifier . '_cron_interval';

			add_action( $this->cron_hook_identifier, array( $this, 'handle_cron_healthcheck' ) );
			add_filter( 'cron_schedules', array( $this, 'schedule_cron_healthcheck' ) );
		}

		/**
		 * Cron Init
		 *
		 * @param $add_data_to_cron
		 */
		public function wpr_init_cron( $add_data_to_cron ) {
			if ( ! empty( $add_data_to_cron ) ) {
				$this->data( $add_data_to_cron )->save()->dispatch();
			}
		}

		/**
		 * Schedule cron healthcheck
		 *
		 * @access public
		 *
		 * @param mixed $schedules Schedules.
		 *
		 * @return mixed
		 */
		public function schedule_cron_healthcheck( $schedules ) {
//			$interval = apply_filters( $this->identifier . '_cron_interval', 1 );
			$interval = 20;

			if ( property_exists( $this, 'cron_interval' ) ) {
				$interval = apply_filters( $this->identifier . '_cron_interval', $this->cron_interval_identifier );
			}

			// Adds every 20 seconds to the existing schedules.
			$schedules[ $this->identifier . '_cron_interval' ] = array(
				'interval' => $interval,
				'display'  => sprintf( __( 'Every %d Seconds' ), $interval ),
			);

			return $schedules;
		}

		/**
		 * Dispatch
		 *
		 * @access public
		 * @return void
		 */
		public function dispatch() {
			// Schedule the cron healthcheck.
			$this->schedule_event();
		}

		/**
		 * Handle
		 *
		 * Pass each queue item to the task handler, while remaining
		 * within server memory and time limit constraints.
		 */
		protected function handle() {
			$this->lock_process();

			do {
				$batch = $this->get_batch();

				// Process tasks
				foreach ( $batch->data as $key => $value ) {

					if ( 'spin_content' == $key && ! empty( $value ) && sizeof( $value ) > 0 ) {
						$task = $this->content_task( $value );
					} elseif ( 'fetch_wordai_string' == $key && ! empty( $value ) && sizeof( $value ) > 0 ) {
						$task = $this->fetch_wordai_string( $value );
					} elseif ( 'ed_random_comment' == $key && ! empty( $value ) && sizeof( $value ) > 0 ) {
						$task = $this->ed_random_comment( $value );
					} else {
						$task = $this->task( $value );
					}

					if ( false !== $task ) {
						$batch->data[ $key ] = $value;
					} else {
						unset( $batch->data[ $key ] );
					}

					if ( $this->time_exceeded() || $this->memory_exceeded() ) {
						// Batch limits reached.
						break;
					}
				}

				// Update or delete current batch.
				if ( ! empty( $batch->data ) ) {
					$this->update( $batch->key, $batch->data );
				} else {
					$this->delete( $batch->key );
				}
			} while ( ! $this->time_exceeded() && ! $this->memory_exceeded() && ! $this->is_queue_empty() );

			$this->unlock_process();

			// Start next batch or complete process.
			if ( ! $this->is_queue_empty() ) {
				$this->dispatch();
			} else {
				$this->complete();
			}

			wp_die();
		}

		/**
		 * Task to add random spin comment
		 *
		 * @param mixed $item Queue item to iterate over
		 *
		 * @return mixed
		 */
		protected function content_task( $item ) {
			$response = true;

			try {
				if ( empty( $item ) ) {
					$response = false;
				} else {
					$post_content = $item['post_content'];
					// Use the Spintax
					$spintax          = new Spintax();
					$spin_the_content = $spintax->process( $post_content );

					// Used for Ed comments
					if ( ! empty( $spin_the_content ) ) {
						$spin_the_content = $this->wpr_remove_hash_tags( $spin_the_content );
						update_post_meta( absint( $item['ID'] ), 'wpr_spin_the_content', $spin_the_content );

						// Clean cache
						clean_post_cache( $item['ID'] );
						$post              = get_post( $item['ID'] );
						$old_status        = $post->post_status;
						$post->post_status = 'publish';
						wp_transition_post_status( 'publish', $old_status, $post );

						$response = false;
					}
				}

				return $response;
			} catch ( Exception $e ) {
				error_log( 'Cron task encountered an exception: ' );
				error_log( $e );

				// Remove from queue
				return $response;
			}
		}

		/**
		 * Fetch WordAi string content together with variations
		 *
		 * @param $item
		 *
		 * @return bool
		 */
		protected function fetch_wordai_string( $item ) {
			$response = true;

			try {
				if ( empty( $item ) ) {
					$response = false;
				} else {
					$post_content = strip_shortcodes( $item['post_content'] );

					// Used for Ed comments
					if ( ! empty( $post_content ) ) {
						$spin_the_content = $this->wpr_remove_hash_tags( $post_content );

						// Use the Spintax
						$spintax = new Spintax();

						// Save spin comment
						update_post_meta( absint( $item['ID'] ), 'wpr_spin_the_content', $spintax->process( $spin_the_content ) );

						// Save spin comment for Edd Article if there is no spinned content
						if ( array_key_exists( 'edd_post_id', $item ) ) {
							update_post_meta( absint( $item['edd_post_id'] ), 'wpr_spin_the_content', $spintax->process( $spin_the_content ) );
						}

						$response = false;
					}
				}

				return $response;
			} catch ( Exception $e ) {
				error_log( 'Cron task encountered an exception: ' );
				error_log( $e );

				// Remove from queue
				return $response;
			}
		}

		/**
		 * Generate Edd Comment
		 *
		 * @param $item
		 *
		 * @return bool
		 */
		protected function ed_random_comment( $item ) {
			$response = true;

			try {
				if ( empty( $item ) ) {
					$response = false;
				} else {
					$post_content = strip_shortcodes( $item['post_content'] );

					// Used for Ed comments
					if ( ! empty( $post_content ) ) {
						// Use the Spintax
						$spintax      = new Spintax();
						$post_content = $spintax->process( $post_content );
						$post_content = $this->wpr_remove_hash_tags( $post_content );

						// Save spin comment
						update_post_meta( absint( $item['ID'] ), 'wpr_spin_the_content', $post_content );

						// Save spin comment id
						if ( array_key_exists( 'edd_post_id', $item ) ) {
							update_post_meta( absint( $item['ID'] ), 'wpr_spinned_edd_article', absint( $item['edd_post_id'] ) );
						}

						$response = false;
					}
				}

				return $response;
			} catch ( Exception $e ) {
				error_log( 'Cron task encountered an exception: ' );
				error_log( $e );

				// Remove from queue
				return $response;
			}
		}

		/**
		 * Get batch
		 *
		 * @return stdClass Return the first batch from the queue
		 */
		protected function get_batch() {
			global $wpdb;

			$table        = $wpdb->options;
			$column       = 'option_name';
			$key_column   = 'option_id';
			$value_column = 'option_value';

			if ( is_multisite() ) {
				$table        = $wpdb->sitemeta;
				$column       = 'meta_key';
				$key_column   = 'meta_id';
				$value_column = 'meta_value';
			}

			$key = $this->identifier . '_batch_%';

			$query = $wpdb->get_row( $wpdb->prepare( "
				SELECT *
				FROM {$table}
				WHERE {$column} LIKE %s
				ORDER BY {$key_column} ASC
				LIMIT 50
			", $key ) );

			$batch       = new stdClass();
			$batch->key  = $query->$column;
			$batch->data = maybe_unserialize( $query->$value_column );

			return $batch;
		}

		/**
		 * Remove hash tags from string
		 *
		 * @param $string
		 *
		 * @return mixed
		 */
		public function wpr_remove_hash_tags( $string ) {
			$result_title = str_replace( '#', '', $string );

			if ( '' != $result_title ) {
				return $result_title;
			} else {
				return $string;
			}
		}

		/**
		 * Needs to be here
		 *
		 * @param mixed $item
		 */
		protected function task( $item ) {
			return false;
		}

		/**
		 * Handle cron healthcheck
		 *
		 * Restart the background process if not already running
		 * and data exists in the queue.
		 */
		public function handle_cron_healthcheck() {
			if ( $this->is_process_running() ) {
				// Background process already running.
				exit;
			}

			if ( $this->is_queue_empty() ) {
				// No data to process.
				$this->clear_scheduled_event();
				exit;
			}

			$this->handle();

			exit;
		}

		/**
		 * Is queue empty
		 *
		 * @return bool
		 */
		protected function is_queue_empty() {
			global $wpdb;

			$table  = $wpdb->options;
			$column = 'option_name';

			if ( is_multisite() ) {
				$table  = $wpdb->sitemeta;
				$column = 'meta_key';
			}

			$key = $this->identifier . '_batch_%';

			$count = $wpdb->get_var( $wpdb->prepare( "
			SELECT COUNT(*)
			FROM {$table}
			WHERE {$column} LIKE %s
		", $key ) );

			return ( $count > 0 ) ? false : true;
		}

		/**
		 * Schedule event
		 */
		protected function schedule_event() {
			if ( ! wp_next_scheduled( $this->cron_hook_identifier ) ) {
				$time = time() + 20;
				wp_schedule_event( $time, $this->cron_interval_identifier, $this->cron_hook_identifier );
			}
		}

		/**
		 * Complete
		 *
		 * Override if applicable, but ensure that the below actions are
		 * performed, or, call parent::complete().
		 */
		protected function complete() {
			parent::complete();
			// Show notice to user or perform some other arbitrary task...
		}
	}
}

if ( ! class_exists( 'Spintax' ) ) {
	/**
	 * Class Spintax
	 *
	 * Usage:
	 *   $spintax = new Spintax();
	 *   echo $spintax->process($str);
	 */
	class Spintax {
		/**
		 * @param $text
		 *
		 * @return null|string|string[]
		 */
		public function process( $text ) {
			return preg_replace_callback(
				'/\{(((?>[^\{\}]+)|(?R))*)\}/x',
				array( $this, 'replace' ),
				$text
			);
		}

		/**
		 * @param $text
		 *
		 * @return mixed
		 */
		public function replace( $text ) {
			$text  = $this->process( $text[1] );
			$parts = explode( '|', $text );

			return $parts[ array_rand( $parts ) ];
		}
	}
}

$request_text_spin = new WPR_Run_Text_Spin();
