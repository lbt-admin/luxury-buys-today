<?php
add_action( 'plugins_loaded', 'webpigment_remove_plugin_data', 99 );
function webpigment_remove_plugin_data() {
	remove_action( 'template_redirect', 'wp_automatic_eb_redirect_end' );
}
// add_action( 'wp_print_styles', 'webpigment_remove_unnecessary_styles', 99 );

function webpigment_remove_unnecessary_styles() {
	global $wp_styles;
	$disabled = array( 'ubermenu-font-awesome', 'datatables-style', 'datatables-rowreorder-style', 'datatables-responsive-style', 'wpr-tower-data', 'wp-block-library', 'RozhaOne', 'Barlow', '' );

	foreach ( $wp_styles->queue as $key => $value ) {
		if ( ! in_array( $value, $disabled, true ) || false !== strpos( $value, 'payment' ) ) {
			continue;
		}
		unset( $wp_styles->queue[ $key ] );
	}
	// echo '<pre>' . print_r( $wp_styles, true ) . '</pre>';
	// exit();
}
if ( ! function_exists('switch_to_blog' ) ) {
	function switch_to_blog( $blog ) {}
}
if ( ! function_exists('restore_current_blog' ) ) {
	function restore_current_blog() {}
}