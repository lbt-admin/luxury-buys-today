<?php
/**
 * Post Class handling custom post methods
 *
 * @package LBT
 */

namespace LBT;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Post Class
 */
class Post {
	use \LBT\Traits\Singleton;

	/**
	 * Initialize class
	 *
	 * @return void
	 */
	private function initialize() {
		add_filter( 'the_excerpt', array( $this, 'get_excerpt' ), 100 );
		add_filter( 'the_content', array( $this, 'filter_the_content_in_the_main_loop' ) );
		add_filter( 'the_content', array( $this, 'add_source_link' ), 20 );
		add_action( 'save_post', array( $this, 'clear_cache_if_promotion_post' ), 10, 2 );
	}

	/**
	 * Clear promotion cache if the created / updated post is promotion post
	 *
	 * @param  int     $post_id The post id.
	 * @param  WP_Post $post    The post object.
	 *
	 * @return void
	 */
	public function clear_cache_if_promotion_post( $post_id, $post ) {
		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
			return;
		}

		if ( ! has_tag( Constants::PROMOTIONS_TAG, $post ) ) {
			return;
		}

		$brands = get_the_category( $post_id );

		$brands_names = wp_list_pluck( $brands, 'name' );

		$placeholders = array_fill( 0, count( $brands_names ), '%s' );

		$in_clause = implode( ', ', $placeholders );

		global $wpdb;

		$table_name = $wpdb->prefix . 'wpr_ontraport_dealerships';

		$dealership_ids = $wpdb->get_col( $wpdb->prepare( "SELECT DISTINCT(ontraport_id) FROM {$table_name} WHERE brand IN ($in_clause)", $brands_names ) );

		if ( empty( $dealership_ids ) ) {
			return;
		}

		foreach ( $dealership_ids as $dealer_id ) {
			$transient_promotions_key = 'lbtpromotions' . $dealer_id;
			delete_transient( $transient_promotions_key );
		}
	}

	/**
	 * Undocumented function
	 *
	 * @return void Description
	 */
	public function set_offer_rating() {
		$args = array(
			'post_type'        => 'post',
			'posts_per_page'   => -1,
			'post_status'      => 'publish',
			'fields'           => 'ids',
			'date_query' => array(
				array(
					'after' => '1 week ago',
				),
			),
		);

		$query = new \WP_Query( $args );

		if ( ! $query->have_posts() ) {
			return;
		}

		foreach ( $query->posts as $offer_id ) {
			$this->generate_and_save_offer_rating( $offer_id );
		}
	}

	/**
	 * Generate and save offer rating
	 *
	 * @param  int $post_id The post this is for.
	 *
	 * @return void
	 */
	public static function generate_and_save_offer_rating( $post_id ) {
		$rating_settings = get_option( '_lbt_offer_rating', array() );

		if ( empty( $rating_settings ) ) {
			$rating_weights = array(
				1 => 0.1,   // 10%
				2 => 0.1,   // 10%
				3 => 0.4,   // 40%
				4 => 0.3,   // 30%
				5 => 0.1    // 10%
			);
		} else {
			$rating_weights = array(
				1 => ! empty( $rating_settings[1] ) ? (float) $rating_settings[1] / 100 : 0.1,
				2 => ! empty( $rating_settings[2] ) ? (float) $rating_settings[2] / 100 : 0.1,
				3 => ! empty( $rating_settings[3] ) ? (float) $rating_settings[3] / 100 : 0.4,
				4 => ! empty( $rating_settings[4] ) ? (float) $rating_settings[4] / 100 : 0.3,
				5 => ! empty( $rating_settings[5] ) ? (float) $rating_settings[5] / 100 : 0.1,
			);
		}

		// Generate a random number between 0 and 1.
		$random = mt_rand() / mt_getrandmax();

		$rating = 0;

		$accumulated_weight = 0;
		foreach ( $rating_weights as $value => $weight ) {
			$accumulated_weight += $weight;
			if ( $random <= $accumulated_weight ) {
				$rating = $value;
				break;
			}
		}

		update_post_meta( $post_id, 'offer_raiting', $rating );
		delete_post_meta( $post_id, 'lbt_broyhill_comment' );
	}

	/**
	 * Check if post has thumbnail or custom remote image
	 *
	 * @param  int    $post_id The post ID.
	 * @param  string $size The image size.
	 *
	 * @return boolean
	 */
	public static function get_thumbnail( $post_id, $size = 'thumbnail' ) {
		$post_id = absint( $post_id );

		if ( ! $post_id ) {
			return false;
		}

		if ( has_post_thumbnail( $post_id ) ) {
			return get_the_post_thumbnail_url( $post_id, $size );
		}

		$img_remote_url = get_post_meta( $post_id, 'wpr_remote_featured_img', true );

		if ( filter_var( $img_remote_url, FILTER_VALIDATE_URL ) ) {
			return $img_remote_url;
		}

		$lbt_img = get_post_meta( $post_id, 'lbt_img', true );

		if ( filter_var( $lbt_img, FILTER_VALIDATE_URL ) ) {
			return $lbt_img;
		}

		return false;
	}

	/**
	 * If the post contains youtube embedd, get the youtube thumbnail
	 *
	 * @param  int $post_id The post id.
	 *
	 * @return boolean|string
	 */
	public static function maybe_get_thumbnail_from_youtube_embed( $post_id ) {
		$post_content  = get_the_content( null, false, $post_id );
		$video_find    = preg_match_all( '((?:www\.)?(?:youtube(?:-nocookie)?\.com\/(?:v\/|watch\?v=|embed\/)|youtu\.be\/)([a-zA-Z0-9?&=_-]*))', $post_content, $yt_urls );
		$yt_vides_urls = array_filter(
			$yt_urls,
			function ( $v ) {
				return ! empty( $v );
			}
		);

		if ( is_array( $yt_urls ) && ! empty( $yt_urls ) && ! empty( $yt_vides_urls ) ) {
			$youtube_url = $urls[0][0];
			$youtube_id  = '';
			if ( strlen( trim( $youtube_url ) ) > 0 ) {
				$youtube_id = getYoutubeIdFromUrl( $youtube_url );
			}

			$img_url     = 'https://img.youtube.com/vi/' . $youtube_id . '/hqdefault.jpg';
			$sd_img_url  = 'https://img.youtube.com/vi/' . trim( $youtube_id ) . '/sddefault.jpg';
			$response_sd = wp_remote_head( $sd_img_url );

			if ( $response_sd ) {
				return $sd_img_url;
			}

			return $img_url;
		}

		return false;
	}

	/**
	 * Get the first image from post content
	 *
	 * @param  WP_Post|null $post The WP_Post or null to get current post.
	 *
	 * @return string
	 */
	public static function get_first_image_from_content( $post = null ) {
		$first_img = '';

		if ( ! $post ) {
			global $post;
		}

		$post_html = str_get_html( $post->post_content );

		if ( false === $post_html ) {
			$first_img = $post_html->find( 'img', 0 );
		}

		if ( null !== $first_img ) {
			return $first_img->src;
		} else {
			return LBT_THEME_URL . 'assets/images/coming_soon_bg@2x.jpg';
		}
	}

	/**
	 * Update exceprt for post
	 *
	 * @param  string $excerpt The post excerpt.
	 *
	 * @return string
	 */
	public function get_excerpt( $excerpt ) {
		global $post;

		if ( empty( $post ) ) {
			return '';
		}

		$excerpt   = get_the_content( '', false, $post );
		$excerpt   = preg_replace( ' ([.*?])', '', $excerpt );
		$excerpt   = strip_shortcodes( $excerpt );
		$excerpt   = wp_strip_all_tags( $excerpt );
		$excerpt   = substr( $excerpt, 0, 150 );
		$excerpt   = substr( $excerpt, 0, strripos( $excerpt, ' ' ) );
		$excerpt   = trim( preg_replace( '/\s+/', ' ', $excerpt ) );
		$excerpt   = str_replace( array( 'BROYHILL COMMENTS', 'BROYHILL COMMENT' ), 'BROYHILL COMMENT ', $excerpt );
		$permalink = get_permalink( $post->ID );

		return $excerpt . '<a href="' . esc_url( $permalink ) . '" class="read-more-link">' . __( 'Read more', 'lbt' ) . '...</a>';
	}

	/**
	 * Add youtube image to content
	 *
	 * @param string $content The post content.
	 *
	 * @return string
	 */
	public function filter_the_content_in_the_main_loop( $content ) {
		if ( is_single() && is_main_query() ) {
			preg_match_all( '((?:www\.)?(?:youtube(?:-nocookie)?\.com\/(?:v\/|watch\?v=|embed\/)|youtu\.be\/)([a-zA-Z0-9?&=_-]*))', $content, $urls );
			$urls = array_filter( $urls );

			if ( ! empty( $urls ) ) {
				$youtube_url = $urls[0][0];

				if ( strlen( trim( $youtube_url ) ) > 0 ) {
					$youtube_id = getYoutubeIdFromUrl( $youtube_url );

					$content = preg_replace( '/<img[^>]+\>/i', '', $content );
					$pos     = strrpos( $content, 'Source ' );

					return substr( $content, 0, $pos ) . wp_oembed_get( 'https://www.youtube.com/watch?v=' . $youtube_id ) . substr( $content, $pos );
				}
			}
		}

		return $content;
	}

	/**
	 * Add source url
	 *
	 * @param string $content The post content.
	 *
	 * @return mixed
	 */
	public function add_source_link( $content ) {
		global $post, $wpdb;

		if ( get_post_meta( $post->ID, 'wpr_broyhill_article', true ) ) {
			$content = preg_replace( '/<\\/?br(.|\\s)*?>/', '', $content );
		}

		if ( is_singular( 'post' ) ) {
			$insert_source       = '';
			$is_email_parser     = get_post_meta( $post->ID, 'wpr_email_source', true );
			$is_custom_offer     = get_post_meta( $post->ID, 'wpr_dealer_id', true );
			$is_automatic_parser = get_post_meta( $post->ID, 'wp_automatic_camp', true );

			if ( $is_email_parser ) {
				$categories       = get_the_category( $post->ID );
				$parse_source_url = get_post_meta( $post->ID, 'source_url', true );
				$source_changed   = get_post_meta( $post->ID, 'wpr_url_changed', true );

				if ( $source_changed ) {
					$categories_id = wp_list_pluck( $categories, 'term_id' );
					$get_catalog   = $wpdb->get_row( $wpdb->prepare( "SELECT wpr_fallback_url FROM {$wpdb->prefix}parser_options WHERE wpr_category IN (%s)", implode( ',', $categories_id ) ) ); //phpcs:ignore

					if ( $get_catalog ) {
						$parse_source_url = $get_catalog->wpr_fallback_url;
					}
				}

				if ( $parse_source_url ) {
					$insert_source = sprintf( '<a href="%s" target="_bank" rel="dofollow">%s</a>', esc_url( $parse_source_url ), esc_html( $categories[0]->name ) );
				} else {
					$insert_source = $categories[0]->name;
				}
			} elseif ( $is_custom_offer ) {
				$author = get_user_by( 'id', $post->post_author );
				if ( empty( $author->first_name ) ) {
					$insert_source = $author->display_name;
				} else {
					$insert_source = sprintf( '%s %s', esc_html( $author->first_name ), esc_html( $author->last_name ) );
				}
			} elseif ( ! $is_automatic_parser ) {
				$insert_source = __( 'Ed Broyhill' );
			}

			$content = closetags( $content );

			if ( $insert_source ) {
				$content .= sprintf( '<div class="wpr-display-source">%s: %s</div>', __( 'Source', 'lbt' ), $insert_source );
			}

			libxml_use_internal_errors( true );
			if ( $is_automatic_parser ) {
				$wpr_html_dom           = new \DOMDocument();
				$wpr_html_dom->encoding = 'utf-8';
				$wpr_html_dom->loadHTML( mb_convert_encoding( $content, 'HTML-ENTITIES', 'UTF-8' ) );
				if ( $wpr_html_dom ) {
					$wpr_xpath      = new \DOMXPath( $wpr_html_dom );
					$source_url     = '';
					$get_source_url = $wpr_xpath->query( "//a[@class='wpr_article_source']/@href" );

					if ( ! empty( $get_source_url ) ) {
						foreach ( $get_source_url as $href_source ) {
							$source_url = $href_source->nodeValue; //phpcs:ignore
						}
					}

					$all_links_list = $wpr_xpath->query( '//a' );

					if ( ! empty( $all_links_list ) ) {
						foreach ( $all_links_list as $link_element ) {
							$link_element->setAttribute( 'rel', 'dofollow' );
							$link_element->removeAttribute( 'href' );
							$link_element->setAttribute( 'href', ! empty( $source_url ) ? $source_url : '' );
							$link_element->setAttribute( 'target', '_blank' );
						}

						$content = $wpr_html_dom->saveHTML();
					}

					$all_images_list = $wpr_xpath->query( '//img' );
					if ( ! empty( $all_images_list ) ) {
						$new_a = $wpr_html_dom->createElement( 'a' );
						$new_a->setAttribute( 'href', ! empty( $source_url ) ? $source_url : '' );
						$new_a->setAttribute( 'target', '_blank' );

						foreach ( $all_images_list as $image ) {
							$anchor = $image->parentNode; //phpcs:ignore
							$anchor->parentNode->insertBefore( $image, $anchor );//phpcs:ignore
							$new_a_clone = $new_a->cloneNode(); //phpcs:ignore
							$image->parentNode->replaceChild( $new_a_clone, $image ); //phpcs:ignore
							$new_a_clone->appendChild( $image ); //phpcs:ignore
						}

						$content = $wpr_html_dom->saveHTML();
					}
				}
			}

			// TODO: What is this thing.
			$dom           = new \DOMDocument();
			$dom->encoding = 'utf-8';
			$dom->loadHTML( mb_convert_encoding( $content, 'HTML-ENTITIES', 'UTF-8' ) );
			foreach ( $dom->getElementsByTagName('img') as $image ) { //phpcs:ignore
				if ( $is_email_parser ) {
					$image->removeAttribute( 'srcset' );
				}

				$image->setAttribute( 'data-original', preg_replace( '/^https?:/', '', $image->getAttribute( 'data-original' ) ) );
			}

			$content = $dom->saveHTML();
		}

		return $content;
	}

	/**
	 * Get the post source
	 *
	 * TODO: CHeck the backup source link here
	 *
	 * @param  int $post_id The post id.
	 *
	 * @return string
	 */
	public static function get_post_source( $post_id ) {
		$article_url = get_post_meta( $post_id, 'wpr_article_source', true );

		if ( empty( $article_url ) ) {
			$article_url = get_post_meta( $post_id, 'backup_source_link', true );

			if ( ! empty( $article_url ) ) {
				update_post_meta( $post_id, 'wpr_article_source', $article_url );
				delete_post_meta( $post_id, 'backup_source_link' );
			}
		}

		if ( strpos( $article_url, 'chanel' ) !== false ) {
			$article_url_parts = explode( ':', $article_url );
			$article_url       = 'https:' . $article_url_parts[1];
		}

		return $article_url;
	}


	/**
	 * Get user's lookbook count
	 *
	 * @param  int $blog_post_id The post id.
	 *
	 * @return int
	 */
	public static function get_count( $blog_post_id ) {
		return (int) get_post_meta( $blog_post_id, 'simplefavorites_count' );
	}

	/**
	 * Update lookbook count
	 *
	 * @param  int     $blog_post_id The post id.
	 * @param  boolean $add If false countdown else countup.
	 *
	 * @return boolean
	 */
	public static function update_count( $blog_post_id, $add = true ) {
		$count = self::get_count( $blog_post_id );

		if ( $add ) {
			$count++;
		} else {
			$count--;

			if ( $count < 0 ) {
				$count = 0;
			}
		}

		update_user_meta( $blog_post_id, 'simplefavorites_count', $count );

		return true;
	}

	/**
	 * Check if a remote file exists.
	 *
	 * @param  string $url The url to the remote file.
	 * @return bool        Whether the remote file exists.
	 */
	public static function remote_file_exists( $url ) {
		$response = wp_remote_head( $url );

		return 200 === wp_remote_retrieve_response_code( $response );
	}
}
