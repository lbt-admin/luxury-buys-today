<?php
/**
 * User Class
 *
 * @package LBT
 */

namespace LBT;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

use LBT\Forms\User_Save_Form;

/**
 * User
 */
class User {
	use \LBT\Traits\Singleton;

	/**
	 * Save a new user into database
	 *
	 * @param array $user_data User fields values.
	 *
	 * @return array
	 */
	public function save( $user_data ) {
		try {
			$user_form = new User_Save_Form( $user_data );
			$user_data = $user_form->validate_and_sanitize_data();

			$first_name = $user_data['first_name'];
			$last_name  = $user_data['last_name'];
			$email      = $user_data['email'];
			$phone      = $user_data['phone'];
			$zip_code   = $user_data['zip_code'];

			$update = ! empty( $user_data['user_id'] );

			if ( ! $update && email_exists( $email ) ) {
				return new \WP_Error(
					422,
					__( 'User with this email already exists', 'lbt' )
				);
			}

			$user_args = array(
				'user_login'   => $email,
				'user_email'   => $email,
				'display_name' => $first_name . ' ' . $last_name,
				'first_name'   => $first_name,
				'last_name'    => $last_name,
			);

			if ( $update ) {
				$user_id = $user_data['user_id'];
				$user    = get_user_by( 'id', $user_id );

				if ( ! $user ) {
					return new \WP_Error(
						404,
						__( 'The user does not exist', 'lbt' )
					);
				}

				$user_args['ID']         = $user->ID;
				$user_args['user_login'] = $user->user_login;
				$user_args['user_email'] = $user->user_email;

				if ( ! empty( $user_data['password'] ) ) {
					if ( $user_data['password'] !== $user_data['password_confirm'] ) {
						return new \WP_Error(
							400,
							__( 'Passwords do not match', 'lbt' )
						);
					}

					$user_args['user_pass'] = wp_hash_password( $user_data['password'] );
				}
			} else {
				$user_args['user_pass']       = $user_data['password'];
				$user_args['user_registered'] = gmdate( 'Y-m-d H:i:s' );
				$user_args['role']            = 'subscriber';
			}

			$user_id = wp_insert_user( $user_args );

			if ( is_wp_error( $user_id ) ) {
				return $user_id;
			}

			if ( ! $update ) {
				$activation_code = wp_generate_password( 32, false );
				add_user_meta( $user_id, 'user_status', 'pending_verification' );
				add_user_meta( $user_id, 'user_verification_code', $activation_code );
			}

			update_user_meta( $user_id, 'phone', $phone );
			update_user_meta( $user_id, 'zip_code', $zip_code );

			if ( ! $update && ! isset( $user_data['from_ontraport'] ) ) {
				Ontraport::get_instance()->save_contact( $user_id );
			}

			return $user_id;
		} catch ( \Exception $e ) {
			return new \WP_Error(
				$e->getCode(),
				$e->getMessage()
			);
		}
	}

	/**
	 * Check if user account is active
	 *
	 * @param   int $user_id  The user id to be checked.
	 *
	 * @return  bool
	 */
	public function is_active( $user_id ) {
		return true;
	}

	/**
	 * Resend account activation email link
	 *
	 * @param  string $email The email to be send to.
	 *
	 * @return bool|\WP_Error
	 */
	public function resend_account_activation_email( $email ) {
		$email = sanitize_email( $email );

		if ( ! is_email( $email ) ) {
			return new \WP_Error(
				400,
				__( 'Invalid email', 'lbt' )
			);
		}

		$user = get_user_by( 'email', $email );

		if ( ! $user ) {
			return new \WP_Error(
				404,
				__( 'User with that email is not found', 'lbt' )
			);
		}

		$is_active = $this->is_active( $user->ID );

		if ( $is_active ) {
			return new \WP_Error(
				400,
				__( 'The user account is already active', 'lbt' )
			);
		}

		$activation_code = wp_generate_password( 32, false );
		add_user_meta( $user->ID, 'user_status', 'pending_verification' );
		add_user_meta( $user->ID, 'user_verification_code', $activation_code );

		$email_data = array(
			'email'           => $email,
			'activation_code' => $activation_code,
		);

		return Email::get_instance()->send_new_user_account_activation_code( $email_data );
	}

	/**
	 * Get current user IP
	 *
	 * @return string
	 */
	public static function get_ip() {
		if ( ! empty( $_SERVER['HTTP_CLIENT_IP'] ) ) {
			return sanitize_text_field( wp_unslash( $_SERVER['HTTP_CLIENT_IP'] ?? '' ) );
		} elseif ( ! empty( $_SERVER['HTTP_X_FORWARDED_FOR'] ) ) {
			return sanitize_text_field( wp_unslash( $_SERVER['HTTP_X_FORWARDED_FOR'] ) );
		} else {
			return sanitize_text_field( wp_unslash( $_SERVER['REMOTE_ADDR'] ?? '' ) );
		}
	}

	/**
	 * Fetch visitor location
	 *
	 * @param string              $type country_code | ip | zip.
	 * @param mixed<string|array> $value State code ( Texas = TX ), IP | ZIP CODE.
	 *
	 * @return  bool If its in the region code or not
	 */
	public static function get_user_location( $type = 'country_code', $value = '' ) {
		$allowed_zip_codes = Constants::DALAS_ZIPCODES;

		$ip      = self::get_ip();
		$user    = is_user_logged_in() ? wp_get_current_user() : null;
		$user_id = null;

		if ( $user ) {
			$user_id = $user->ID;
		}

		if ( false !== strpos( $user->user_email, '@hokuapps.com') ) {
			return true;
		}

		switch ( $type ) {
			case 'ip':
				if ( $value === $user_ip ) {
					return true;
				}

				return false;
			case 'country_code':
				$user_location = false;
				if ( $user_id > 0 ) {
					$user_zip = get_user_meta( $user_id, 'zip_code', true );

					if ( ! empty( $user_zip ) ) {
						$geocode_response = wp_remote_get( 'https://maps.googleapis.com/maps/api/geocode/json?address=' . esc_attr( $user_zip ) . '&key=AIzaSyDkE0osaaC14WeHZ75j54HnXqDjz9Z5fg8' );
						if ( ! is_wp_error( $geocode_response ) ) {
							$output = json_decode( wp_remote_retrieve_body( $geocode_response ) );
							if ( $output->results ) {
								$user_location = $output->results[0]->formatted_address;
							}
						}
					}
				}

				if ( ! $user_location ) {
					$user_ip = explode( ',', $ip );
					if ( ! empty( $user_ip ) ) {
						$ip = $user_ip[0];
					}

					$geocode = @unserialize( file_get_contents( 'http://ip-api.com/php/' . $ip ) ); //phpcs:ignore

					if ( 'fail' !== $geocode['status'] ) {
						$user_location = $geocode['regionName'] . ', ' . $geocode['region'] . ', ' . $geocode['countryCode'];

						if ( is_string( $value ) && strpos( $user_location, $value ) !== false ) {
							return true;
						}
					}

					if ( is_array( $value ) ) {
						$allowed = array_filter(
							$value,
							function( $rc ) use ( $user_location ) {
								return strpos( $user_location, $rc ) !== false;
							}
						);

						if ( ! empty( $allowed ) ) {
							return true;
						}
					}
				}

				return false;
			case 'zip':
				$zip = false;

				if ( ! empty( $value ) && in_array( (int) $value, $allowed_zip_codes, true ) ) {
					return true;
				} elseif ( ! empty( $value ) && is_array( $value ) && ! empty( array_intersect( (int) $value, $allowed_zip_codes ) ) ) {
					return true;
				} else {
					$zip = Dealer_Locator::get_user_zip( $user_id );

					if ( ! empty( $zip ) && in_array( (int) $zip, $allowed_zip_codes, true ) ) {
						return true;
					}
				}

				return false;
		}
	}

	/**
	 * Get user name
	 *
	 * @param  int $user_id The user id.
	 *
	 * @return string
	 */
	public static function get_user_name( $user_id ) {
		$userdata = get_userdata( $user_id );

		$name = $userdata->first_name . ' ' . $usedata->last_name;

		if ( empty( trim( $name ) ) ) {
			$name = $usedata->display_name;
		}

		if ( empty( trim( $name ) ) ) {
			$name = $usedata->user_nicename;
		}

		return trim( $name );
	}

	/**
	 * Get user zip code
	 *
	 * @param  boolean|int    $user_id The user zip or false for current user if logged in.
	 * @param  boolean|string $user_ip The user ip address.
	 *
	 * @return string
	 */
	public static function get_user_zip( $user_id = null, $ip = false ) {
		if ( false === $user_id && is_user_logged_in() ) {
			$user_id = get_current_user_id();
		}

		return Dealer_Locator::get_user_zip( $user_id );
	}
}
