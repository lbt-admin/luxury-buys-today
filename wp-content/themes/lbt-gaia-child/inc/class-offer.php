<?php
/**
 * Offer logic
 *
 * @package LBT
 */

namespace LBT;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Offer class
 */
class Offer {
	use \LBT\Traits\Singleton;

	/**
	 * Fetch user's offers
	 *
	 * @return array
	 */
	public static function get_user_offers() {
		$offers = get_user_meta( get_current_user_id(), 'lbt_saved_offers' );

		if ( empty( $offers ) ) {
			return array();
		}

		return $offers;
	}

	/**
	 * Save post to user's offers and save brand
	 *
	 * @param int $post_id The post to be saved to offers.
	 * @param int $ontraport_dealership_id The Ontraport dealership id.
	 *
	 * @return bool
	 */
	public static function save( $post_id, $ontraport_dealership_id = 0 ) {
		$post_id = (int) $post_id;

		if ( ! $post_id ) {
			return false;
		}

		add_user_meta( get_current_user_id(), 'lbt_saved_offers', $post_id );
		add_post_meta( $post_id, 'lbt_saved_offers', get_current_user_id() );

		if ( ! empty( $ontraport_dealership_id ) && is_numeric( $ontraport_dealership_id ) ) {
			Brand::get_instance()->save( $ontraport_dealership_id );
		}

		return true;
	}

	/**
	 * Remove post from user's offers
	 *
	 * @param int $post_id THe post to be removed from offers.
	 *
	 * @return bool
	 */
	public static function remove( $post_id ) {
		$post_id = (int) $post_id;

		if ( ! $post_id ) {
			return false;
		}

		delete_user_meta( get_current_user_id(), 'lbt_saved_offers', $post_id );
		delete_post_meta( $post_id, 'lbt_saved_offers', get_current_user_id() );

		return true;
	}
}
