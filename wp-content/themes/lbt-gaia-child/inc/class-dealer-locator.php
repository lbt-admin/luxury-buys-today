<?php

namespace LBT;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

use \GeoIp2\Database\Reader;

class Dealer_Locator {
	use \LBT\Traits\Singleton;

    private static $cache_group = 'dealer_locator';
    private static $redis_available = false;

    private function initialize() {
        global $wp_object_cache;
		self::$redis_available = wp_using_ext_object_cache();

		// if (!wp_next_scheduled('dealer_cache_warmup')) {
		// 	wp_schedule_event(time(), 'daily', 'dealer_cache_warmup');
		// }
		// add_action('dealer_cache_warmup', ['Dealer_Locator', 'preload_dealers_per_zip']);

		add_action('admin_menu', function() {
			add_menu_page('Dealer Cache Control', 'Dealer Cache', 'manage_options', 'dealer-cache-control', function() {
				echo '<div class="wrap"><h1>Dealer Cache Control</h1>';
				if (isset($_POST['preload_dealers'])) {
					self::preload_dealers_per_zip();
					echo '<p>Cache preloaded successfully.</p>';
				}
				echo '<form method="POST"><input type="submit" name="preload_dealers" class="button-primary" value="Preload Dealer Cache"></form>';
				echo '</div>';
			});
		});
    }

    public static function get_user_zip($user_id = null) {
        if ($user_id) {
            $zip = get_user_meta($user_id, 'zip_code', true);
            if ($zip) {
                return $zip;
            }
        }

        $ip = self::get_visitor_ip();
        return self::lookup_zip_by_ip($ip);
    }

    public static function get_visitor_ip() {
        if (!empty($_SERVER['HTTP_CF_CONNECTING_IP'])) {
            return $_SERVER['HTTP_CF_CONNECTING_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            return explode(',', $_SERVER['HTTP_X_FORWARDED_FOR'])[0];
        } else {
            return $_SERVER['REMOTE_ADDR'];
        }
    }

    public static function lookup_zip_by_ip($ip) {
        try {
            $cache_key = "user_zip_{$ip}";
			$cached_zip = self::get_cache($cache_key);

			if ($cached_zip) {
				return $cached_zip;
			}

			$reader = new Reader(WP_CONTENT_DIR . '/GeoLite2-City.mmdb');
			$record = $reader->city($ip);
			$zip = $record->postal->code ?? '';

			if ($zip) {
				self::set_cache($cache_key, $zip, DAY_IN_SECONDS);
			}

			return $zip;
        } catch (\Exception $e) {
            error_log("GeoIP Lookup Failed: " . $e->getMessage());
            return '';
        }
    }

	public static function get_nearby_dealer_manual( $brand, $brand_id, $zip ) {
		if ( ! in_array( (int) $zip, Constants::TEST_ZIPCODES, true ) ) {
			return array();
		}

		if ( in_array( $brand, array( 'Brook Brothers', 'Bloomingdales' ), true ) ) {
			return array();
		}

		if ( strpos( $brand, 'Tiffany' ) !== false ) {
			$brand = 'Tiffany';
		}

		if ( strpos( $brand, 'Christie' ) !== false ) {
			$brand = 'Christie';
		}

		if (
			stripos( $brand, 'Audi' ) !== false ||
			stripos( $brand, 'BMW' ) !== false ||
			stripos( $brand, 'Jaguar' ) !== false ||
			stripos( $brand, 'Ferrari' ) !== false ||
			stripos( $brand, 'Cadillac' ) !== false ||
			stripos( $brand, 'Mercedes' ) !== false ||
			stripos( $brand, 'Lexus' ) !== false ||
			stripos( $brand, 'Sotheby' ) !== false
		) {
			$zip = 75209;
		} elseif (
			stripos( $brand, 'Nordstrom' ) !== false ||
			stripos( $brand, 'Tiffany' ) !== false
		) {
			$zip = 75225;
		} elseif (
			stripos( $brand, 'Neiman' ) !== false
		) {
			$zip = 75201;
		} elseif (
			stripos( $brand, 'Saks' ) !== false
		) {
			$zip = 77056;
		} elseif (
			stripos( $brand, 'Havetry' ) !== false
		) {
			$zip = 75150;
		} elseif (
			stripos( $brand, 'Ethan' ) !== false
		) {
			$zip = 75240;
		} elseif (
			stripos( $brand, 'Christie' ) !== false
		) {
			$zip = 75205;
		} else {
			$zip = 0;
		}

		if ( empty( $zip ) ) {
			return array();
		}

		global $wpdb;

		$table_name = $wpdb->prefix . 'wpr_ontraport_dealerships';

		$sql = $wpdb->prepare( "SELECT * FROM {$table_name} WHERE brand = %s AND data LIKE %s LIMIT 1", $brand, '%' . $zip . '%' );

		$dealerships = $wpdb->get_results( $sql );

		$dealer = array();

		if ( $dealerships ) {
			$cache_key = "dealers_{$brand_id}_{$zip}";
			$dealer = maybe_unserialize( $dealerships[0]->data );
			self::set_cache($cache_key, $dealer, DAY_IN_SECONDS);
		}

		return $dealer;
	}

    public static function get_nearby_dealer($brand_id, $zip = false, $user_id = false) {
		if ( ! $zip ) {
			if ( ! $user_id ) {
				$user_id = is_user_logged_in() ? get_current_user_id() : false;
			}
			$zip = self::get_user_zip( $user_id );
		}

		if ( empty( $zip ) ) {
			return array();
		}

        global $wpdb;

        $cache_key = "dealers_{$brand_id}_{$zip}";
        $dealer = self::get_cache($cache_key);

        if ($dealer !== false) {
			return $dealer;
        }

		$brand = get_cat_name( $brand_id );

		$dealer = self::get_nearby_dealer_manual( $brand, $brand_id, $zip );

		if ( ! empty( $dealer ) ) {
			return $dealer;
		}

        $coords = self::get_lat_lng_for_zip($zip);
        if (!$coords) {
			$coords['lat'] = '40.748194';
			$coords['lng'] = '-73.984925';
        }

		$table = $wpdb->prefix . 'wpr_ontraport_dealerships';

		if ( $user_id ) {
			$current_user = get_user_by( 'ID', $user_id );
		} else {
			$current_user = is_user_logged_in() ? wp_get_current_user() : false;
		}

		$current_user_email = $current_user ? $current_user->user_email : false;

		if ( $current_user_email ) {
			$dealer_email = Dealer::dealer_hokuapps_email( $current_user_email, $brand );
		}

		$dealers = array();

		if ( $dealer_email ) {
			$sql = $wpdb->prepare("
				SELECT *, ROUND(3959 * acos(
					cos(radians(%f)) * cos(radians(latitude)) *
					cos(radians(longitude) - radians(%f)) +
					sin(radians(%f)) * sin(radians(latitude))
				), 1) AS distance
				FROM {$table}
				WHERE data LIKE %s
				ORDER BY distance ASC
				LIMIT 1
			", $coords['lat'], $coords['lng'], $coords['lat'], '%' . $dealer_email . '%');

			$dealers = $wpdb->get_results($sql, ARRAY_A);

			if ( empty( $dealers ) ) {
				$sql = $wpdb->prepare("
					SELECT *, ROUND(3959 * acos(
						cos(radians(%f)) * cos(radians(latitude)) *
						cos(radians(longitude) - radians(%f)) +
						sin(radians(%f)) * sin(radians(latitude))
					), 1) AS distance
					FROM {$table}
					WHERE brand LIKE %s
					ORDER BY distance ASC
					LIMIT 1
				", $coords['lat'], $coords['lng'], $coords['lat'], '%' . $brand . '%');

				$dealers = $wpdb->get_results($sql, ARRAY_A);
			}
		} else {
			$sql = $wpdb->prepare("
				SELECT *, ROUND(3959 * acos(
					cos(radians(%f)) * cos(radians(latitude)) *
					cos(radians(longitude) - radians(%f)) +
					sin(radians(%f)) * sin(radians(latitude))
				), 1) AS distance
				FROM {$table}
				WHERE brand LIKE %s
				ORDER BY distance ASC
				LIMIT 1
			", $coords['lat'], $coords['lng'], $coords['lat'], '%' . $brand . '%');

			$dealers = $wpdb->get_results($sql, ARRAY_A);
		}

		if ( ! empty( $dealers ) ) {
			$dealer = maybe_unserialize( $dealers[0]['data'] );
        	self::set_cache($cache_key, $dealer, DAY_IN_SECONDS);
			return $dealer;
		}

        return array();
    }

    public static function get_lat_lng_for_zip($zip) {
        global $wpdb;
        $table = $wpdb->prefix . 'zip_coordinates';

        $row = $wpdb->get_row($wpdb->prepare(
            "SELECT latitude, longitude FROM {$table} WHERE zip_code = %s",
            $zip
        ), ARRAY_A);

        return $row ? $row : false;
    }

    private static function get_redis_client() {
		static $client = null;

		if (!$client) {
			$client = new \Redis();
			$client->connect('127.0.0.1', 6379);
			$client->select(1);
		}

		return $client;
	}

	public static function get_cache($key) {
		$redis = self::get_redis_client();
		return maybe_unserialize( $redis->get($key) );
	}

	public static function set_cache($key, $value, $ttl = DAY_IN_SECONDS) {
		$redis = self::get_redis_client();
		$redis->setex($key, $ttl, serialize($value));
	}

    public static function preload_dealers_per_zip() {
        global $wpdb;

        $zips = $wpdb->get_col("SELECT zip_code FROM {$wpdb->prefix}zip_coordinates WHERE zip_code LIKE '7%'");
        $brands = Category::get_main_categories_children();

        foreach ($zips as $zip) {
            foreach ($brands as $brand) {
                $dealers = self::get_nearby_dealer($brand->term_id, $zip);
                self::set_cache("dealers_{$brand->term_id}_{$zip}", $dealers, DAY_IN_SECONDS);
            }
        }
    }
}
