<?php
namespace LBT;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
class Action_Scheduler_Cleaner {
	use \LBT\Traits\Singleton;

    const CLEANUP_INTERVAL_DAYS = 30;
    const CRON_HOOK = 'asc_daily_cleanup_event';

    private function initialize() {
        add_action('wp', [__CLASS__, 'schedule_daily_cleanup']);
        add_action(self::CRON_HOOK, [__CLASS__, 'cleanup_old_entries']);
        add_action('admin_menu', [__CLASS__, 'register_admin_menu']);
    }

    /**
     * Schedule daily cron job if not already scheduled.
     */
    public static function schedule_daily_cleanup() {
        if (!wp_next_scheduled(self::CRON_HOOK)) {
            wp_schedule_event(time(), 'daily', self::CRON_HOOK);
        }
    }

    /**
     * Cleanup old ActionScheduler entries (older than 30 days).
     */
    public static function cleanup_old_entries() {
        global $wpdb;

        $days = self::CLEANUP_INTERVAL_DAYS;

        // Delete logs for completed, canceled, or failed actions older than X days.
        $log_sql = "
            DELETE l
            FROM {$wpdb->prefix}actionscheduler_logs l
            INNER JOIN {$wpdb->prefix}actionscheduler_actions a
            ON l.action_id = a.action_id
            WHERE a.status IN ('canceled', 'failed', 'complete')
            AND a.last_attempt_gmt < (NOW() - INTERVAL %d DAY)
        ";
        $wpdb->query($wpdb->prepare($log_sql, $days));

        // Delete the actual actions themselves.
        $action_sql = "
            DELETE FROM {$wpdb->prefix}actionscheduler_actions
            WHERE status IN ('canceled', 'failed', 'complete')
            AND last_attempt_gmt < (NOW() - INTERVAL %d DAY)
        ";
        $wpdb->query($wpdb->prepare($action_sql, $days));
    }

    /**
     * Add menu page for manual cleanup.
     */
    public static function register_admin_menu() {
        add_submenu_page(
            'tools.php',
            'ActionScheduler Cleanup',
            'ActionScheduler Cleanup',
            'manage_options',
            'asc-cleanup',
            [__CLASS__, 'manual_cleanup_page']
        );
    }

    /**
     * Manual cleanup page HTML.
     */
    public static function manual_cleanup_page() {
        echo '<div class="wrap"><h1>ActionScheduler Cleanup</h1>';
        if (isset($_POST['run_cleanup'])) {
            self::cleanup_old_entries();
            echo '<p>Old ActionScheduler entries cleaned up.</p>';
        }
        echo '<form method="POST">';
        echo '<input type="submit" name="run_cleanup" class="button-primary" value="Run Cleanup Now">';
        echo '</form>';
        echo '</div>';
    }
}
