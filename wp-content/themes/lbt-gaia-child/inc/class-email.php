<?php
/**
 * Email
 *
 * @package LBT
 */

namespace LBT;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Email
 */
class Email {
	use \LBT\Traits\Singleton;

	/**
	 * Initialize class
	 *
	 * @return  void
	 */
	private function initalize() {
		add_filter( 'wp_mail_from_name', array( $this, 'set_from_name' ), 9999 );
		add_filter( 'wp_mail_from', array( $this, 'set_from_email' ), 9999 );
	}

	/**
	 * Set email from name
	 *
	 * @param   string $name  The original name.
	 *
	 * @return  string
	 */
	public function set_from_name( $name ) {
		return __( 'LuxuryBuysToday', 'ppp' );
	}

	/**
	 * Set from email
	 *
	 * @param   string $email  The original email.
	 *
	 * @return  string
	 */
	public function set_from_email( $email ) {
		return 'support@luxurybuystoday.com';
	}

	/**
	 * Send new user account activation email
	 *
	 * @param   array $data Email data.
	 *
	 * @return  bool
	 */
	public function send_new_user_account_activation_code( $data ) {
		$headers = array(
			'Content-Type: text/html; charset=UTF-8',
			'From: LuxuryBuysToday <support@luxurybuystoday.com>',
		);

		ob_start();
		include LBT_THEME_DIR . 'templates/emails/new-user-account-activation.php';
		$message = ob_get_clean();

		return wp_mail( $data['email'], 'Welcome to LuxuryBuysToday', $message, $headers );
	}
}
