<?php
/**
 * Google Analytics logic
 *
 * @package LBT
 */

namespace LBT;

use Google_Client;
use Google_Service_AnalyticsData;
use Google_Service_AnalyticsData_RunReportRequest;
use Google_Service_AnalyticsData_Dimension;
use Google_Service_AnalyticsData_Metric;
use Google_Service_AnalyticsData_DateRange;
use Google_Service_AnalyticsData_FilterExpression;
use Google_Service_AnalyticsData_Filter;
use Google_Service_AnalyticsData_StringFilter;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Google Analytics class.
 */
class Google_Analytics {
	use \LBT\Traits\Singleton;

	/**
	 * GA4 property ID.
	 *
	 * @var string
	 */
	private $ga4_property_id = 'YOUR_GA4_PROPERTY_ID';

	/**
	 * Path to your Google service account JSON key.
	 *
	 * @var string
	 */
	private $service_account_path = '/path/to/your/service-account-key.json';

	/**
	 * Get page views for a post (last X days).
	 *
	 * @param int $post_id Post ID.
	 * @param int $start_days_ago Days ago (default 30 days).
	 * @return int Total page views.
	 */
	public function get_offer_views( $post_id, $start_days_ago = 30 ) {
		$page_path = wp_parse_url( get_permalink( $post_id ), PHP_URL_PATH );

		if ( empty( $page_path ) ) {
			return 0;
		}

		try {
			$client = new Google_Client();
			$client->setAuthConfig( $this->service_account_path );
			$client->setScopes( [ 'https://www.googleapis.com/auth/analytics.readonly' ] );

			$service = new Google_Service_AnalyticsData( $client );

			$request = new Google_Service_AnalyticsData_RunReportRequest();

			$request->setDateRanges( [
				new Google_Service_AnalyticsData_DateRange( [
					'startDate' => $start_days_ago . 'daysAgo',
					'endDate'   => 'today',
				] )
			] );

			$request->setDimensions( [
				new Google_Service_AnalyticsData_Dimension( [ 'name' => 'pagePath' ] )
			] );

			$request->setMetrics( [
				new Google_Service_AnalyticsData_Metric( [ 'name' => 'screenPageViews' ] )
			] );

			$filter = new Google_Service_AnalyticsData_Filter();
			$filter->setFieldName( 'pagePath' );
			$filter->setStringFilter( new Google_Service_AnalyticsData_StringFilter( [
				'matchType' => 'EXACT',
				'value'     => $page_path,
			] ) );

			$filter_expression = new Google_Service_AnalyticsData_FilterExpression();
			$filter_expression->setFilter( $filter );

			$request->setDimensionFilter( $filter_expression );

			$response = $service->properties->runReport( "properties/{$this->ga4_property_id}", $request );

			$total_views = 0;

			foreach ( $response->getRows() as $row ) {
				$views = isset( $row->getMetricValues()[0] ) ? (int) $row->getMetricValues()[0]->getValue() : 0;
				$total_views += $views;
			}

			return $total_views;
		} catch ( \Exception $e ) {
			error_log( 'GA4 Fetch Error: ' . $e->getMessage() );
			return 3;
		}

		return 3;
	}
}
