<?php
/**
 * Brand logic
 *
 * @package LBT
 */

namespace LBT;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Brand class
 */
class Brand {
	use \LBT\Traits\Singleton;

	/**
	 * Fetch user's saved brands
	 *
	 * @param int|bool $user_id The brands for given user id or false for current.
	 *
	 * @return array
	 */
	public static function get_user_brands( $user_id = false ) {
		if ( ! $user_id ) {
			$user_id = get_current_user_id();
		}

		$saved_brands = get_user_meta( $user_id, 'saved_deals_brands' );

		if ( ! $saved_brands ) {
			return array();
		}

		return array_unique( $saved_brands );
	}

	/**
	 * Save brand to user profile
	 *
	 * @param int      $ontraport_dealership_id The ontraport dealership id.
	 * @param int|bool $post_id The post id if the save brand is clicked from a single post.
	 * @param int|bool $user_id The user id or false for current user.
	 *
	 * @return bool|WP_Error
	 */
	public function save( $ontraport_dealership_id, $post_id = false, $user_id = false ) {
		if ( ! $user_id ) {
			$user_id = get_current_user_id();
		}

		if ( $post_id ) {
			Lookbook::get_instance()->save( $post_id );
		}

		$saved_brands = self::get_user_brands();

		if ( in_array( $ontraport_dealership_id, $saved_brands ) ) {
			return true;
		}

		$response = Ontraport::get_instance()->toggle_brand_subscription( $ontraport_dealership_id, 'subscribe', false, $user_id );

		if ( is_wp_error( $response ) ) {
			return $response;
		}

		add_user_meta( $user_id, 'saved_deals_brands', $ontraport_dealership_id );

		return true;
	}

	/**
	 * Remove brand from user profile
	 *
	 * @param int      $ontraport_dealership_id The ontraport dealership id.
	 * @param int|bool $user_id The user id or false for current user.
	 *
	 * @return bool|WP_Error
	 */
	public function remove( $ontraport_dealership_id, $user_id = false ) {
		if ( ! $user_id ) {
			$user_id = get_current_user_id();
		}

		$response = Ontraport::get_instance()->toggle_brand_subscription( $ontraport_dealership_id, 'unsubscribe', false, $user_id );

		if ( is_wp_error( $response ) ) {
			return $response;
		}

		delete_user_meta( $user_id, 'saved_deals_brands', $ontraport_dealership_id );

		return true;
	}
}
