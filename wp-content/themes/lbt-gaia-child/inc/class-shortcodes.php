<?php
/**
 * Shortcodes Class
 *
 * @package LBT
 */

namespace LBT;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Shortcodes Class
 */
class Shortcodes {
	use \LBT\Traits\Singleton;

	/**
	 * Initialize class
	 *
	 * @return void
	 */
	private function initialize() {
		add_action( 'wp_enqueue_scripts', array( $this, 'load_assets' ) );

		add_shortcode( 'lbt_newsletter', array( $this, 'render_ontraport_newsletter' ) );
		add_shortcode( 'categoria', array( $this, 'render_category' ) );
		add_shortcode( 'home_posts', array( $this, 'render_home_posts' ) );
	}

	/**
	 * Load shortcode assets
	 *
	 * @return void
	 */
	public function load_assets() {
		global $post;

		if ( isset( $post ) && ( has_shortcode( $post->post_content, 'categoria' ) || is_front_page() ) ) {
			wp_enqueue_style(
				'lbt-category-shortcode',
				LBT_THEME_URL . 'assets/css/category-shortcode.css',
				array(),
				LBT_THEME_VERSION
			);
		}
	}

	/**
	 * Show ontraport newsletter form
	 *
	 * @param  array $atts List of shortcode attributes.
	 *
	 * @return void
	 */
	public function render_ontraport_newsletter( $atts ) {
		wp_enqueue_script('lbt-newsletter', get_stylesheet_directory_uri() . '/assets/js/newsletter.js', array('jquery'), '1.0.0', true);

		include LBT_THEME_DIR . 'shortcodes/newsletter.php';
	}

	/**
	 * Show category template box
	 *
	 * @param  array $atts List of shortcode attributes.
	 *
	 * @return void
	 */
	public function render_category( $atts ) {
		$category_slug = sanitize_text_field( $atts['type'] );
		$category      = get_category_by_slug( $category_slug );
		$category_link = get_category_link( $category->term_id );

		$header_background = get_field( 'archive_page_header_image', 'category_' . $category->term_id );

		include LBT_THEME_DIR . 'shortcodes/category-box.php';
	}

	/**
	 * Show posts in widget. Used on home page
	 *
	 * @param  array $atts List of shortcode attributes.
	 *
	 * @return void
	 */
	public function render_home_posts( $atts ) {
		$post_type = $atts['type'];
		$amount    = $atts['amount'] + 0;
		$offset    = $atts['offset'] + 0;
		$column    = $atts['column'] + 0;
		$counter   = 1;

		$args = array(
			'post_type'      => 'post',
			'posts_per_page' => $amount,
			'category_name'  => $post_type,
		);

		$append_class = '';

		global $is_eds;

		if ( 'broyhill' === $atts['layout'] ) {
			$append_class = 'entry-box-shadow black-box';
			$is_eds       = 1;
		}

		$query = new \WP_Query( $args );

		include LBT_THEME_DIR . 'shortcodes/home-posts.php';
	}
}
