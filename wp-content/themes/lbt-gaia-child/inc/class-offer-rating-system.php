<?php
/**
 * Offer Rating System logic
 *
 * @package LBT
 */

namespace LBT;

use LBT\Google_Analytics;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Offer Rating System class.
 */
class Offer_Rating_System {
	use \LBT\Traits\Singleton;

	/**
	 * Cache group.
	 *
	 * @var string
	 */
	private $cache_group = 'offer_rating_system';

	/**
	 * Cache expiration time (1 day).
	 *
	 * @var int
	 */
	private $cache_expiration = DAY_IN_SECONDS;

	/**
	 * Get star rating for a post.
	 *
	 * @param int $post_id Post ID.
	 * @return float Star rating (0 to 5).
	 */
	public function get_star_rating( $post_id ) {
		$cache_key = 'star_rating_' . $post_id;

		$cached_rating = wp_cache_get( $cache_key, $this->cache_group );

		if ( false !== $cached_rating ) {
			return (float) $cached_rating;
		}

		$rating = $this->calculate_star_rating( $post_id );

		wp_cache_set( $cache_key, $rating, $this->cache_group, $this->cache_expiration );

		return $rating;
	}

	/**
	 * Calculate star rating based on views and favorites.
	 *
	 * @param int $post_id Post ID.
	 * @return float Star rating (0 to 5).
	 */
	private function calculate_star_rating( $post_id ) {
		$ga = Google_Analytics::get_instance();

		$total_views     = $ga->get_offer_views( $post_id, 30 );
		$favorites_count = $this->get_favorites_count( $post_id );

		$rating_score = ( $total_views * 0.7 ) + ( $favorites_count * 0.3 );

		$max_possible_score = 10000;
		$rating             = ( $rating_score / $max_possible_score ) * 5;

		return max( 0, min( 5, round( $rating, 1 ) ) );
	}

	/**
	 * Count number of times a post is favorited (lbt_saved_offers meta).
	 *
	 * @param int $post_id Post ID.
	 * @return int Number of favorites.
	 */
	private function get_favorites_count( $post_id ) {
		$meta_values = get_post_meta( $post_id, 'lbt_saved_offers' );

		if ( ! is_array( $meta_values ) ) {
			return 0;
		}

		return count( $meta_values );
	}

	/**
	 * Clear the cache for a post's rating.
	 *
	 * @param int $post_id Post ID.
	 */
	public function clear_rating_cache( $post_id ) {
		$cache_key = 'star_rating_' . $post_id;
		wp_cache_delete( $cache_key, $this->cache_group );
	}
}
