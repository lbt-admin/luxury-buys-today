<?php
/**
 * Include php scrips and classes
 *
 * @package LBT
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
require_once LBT_THEME_DIR . 'vendor/autoload.php';
require_once LBT_THEME_DIR . 'inc/core/autoloader.php';

if ( ! class_exists( '\ACF' ) ) {
	add_action(
		'wp',
		function() {
			// Fallback function so we don't need to check every time if it exists.
			if ( ! function_exists( 'get_field' ) ) {
				/**
				 * Return meta field
				 *
				 * @param   string $selector      The selector.
				 * @param   int    $post_id       The post id.
				 * @param   bool   $format_value  Return array or format it to single value.
				 *
				 * @return  string
				 */
				function get_field( $selector, $post_id = false, $format_value = true ) {
					return '';
				}

				/**
				 * Print meta field
				 *
				 * @param   string $selector      The selector.
				 * @param   int    $post_id       The post id.
				 * @param   bool   $format_value  Return array or format it to single value.
				 *
				 * @return  void
				 */
				function the_field( $selector, $post_id = false, $format_value = true ) {
					echo '';
				}

				/**
				 * Return sub meta field
				 *
				 * @param   string $selector      The selector.
				 * @param   int    $post_id       The post id.
				 * @param   bool   $format_value  Return array or format it to single value.
				 *
				 * @return  string
				 */
				function get_sub_field( $selector, $post_id = false, $format_value = true ) {
					return '';
				}

				/**
				 * Print sub meta field
				 *
				 * @param   string $selector      The selector.
				 * @param   int    $post_id       The post id.
				 * @param   bool   $format_value  Return array or format it to single value.
				 *
				 * @return  void
				 */
				function the_sub_field( $selector, $post_id = false, $format_value = true ) {
					echo '';
				}
			}
		}
	);
}

LBT\CPT\Broyhill_Comment::get_instance();

LBT\Core\Database::get_instance()->create_tables();
LBT\Core\Menu::get_instance();
LBT\Core\Theme::get_instance();
LBT\Core\Cron::get_instance();

LBT\Shortcodes::get_instance();
LBT\Profile::get_instance();
LBT\Post_Image_Check::get_instance();
LBT\Old_Post_Removal::get_instance();
LBT\Post::get_instance();
LBT\Dealer_Locator::get_instance();
LBT\Action_Scheduler_Cleaner::get_instance();

if ( ! is_user_logged_in() ) {
	LBT\Login_Register::get_instance();
}

LBT\AJAX\Brand::get_instance();
LBT\AJAX\Dealer::get_instance();
LBT\AJAX\Lookbook::get_instance();
LBT\AJAX\Offer::get_instance();
LBT\AJAX\Post_Image_Check::get_instance();
LBT\AJAX\Home_Feed::get_instance();
LBT\AJAX\Archive_Feed::get_instance();

LBT\API\Brand::get_instance();
LBT\API\User::get_instance();
LBT\API\Dealer::get_instance();

if ( is_admin() ) {
	LBT\Admin\Post_Image_Check::get_instance();
	LBT\Admin\Dashboard::get_instance();
}

if ( defined( 'WP_CLI' ) && WP_CLI ) {
	LBT\Commands\WP_CLI_Commands::get_instance();
}

/**
 * Retrieve posts from a REST API and fill in the template data
 *
 * @param WP_Term|null $category_slug The term object.
 * @param string       $tag_slug The slug.
 * @param int          $paged Page number.
 * @param int          $posts_per_page Number of posts per page.
 * @param string       $target_href The target link.
 * @param string       $post_name The post name.
 *
 * @return array
 */
function lbt_feed_archive_promotion_posts( $category_slug, $tag_slug, $paged, $posts_per_page, $target_href, $post_name = null ) {
	return \LBT\Feed_Archive_Promotions::get_instance()->get_posts_with_template_data(
		$category_slug,
		$tag_slug,
		$paged,
		$posts_per_page,
		$target_href,
		$post_name
	);
}

LBT\Feed_Single_Post_Promotion::get_instance();

/**
 * Get Dybamic posts
 *
 * @return array
 */
function lbt_get_automotive_post_ids() {
	return \LBT\Feed_Single_Post_Promotion::DYNAMIC_POSTS;
}
