<?php

/**
 * Novus Posts List widget.
 */
class Novus_Posts_List_Category_Widget extends WP_Widget {

	/**
	 * Register widget with WordPress.
	 */
	function __construct() {
		parent::__construct(
			'posts_list_category', // Base ID
			__( 'Novus / Posts List Category', 'novus' ), // Name
			array( 'description' => __( 'List of posts from category.', 'novus' ), ) // Args
		);
	}

	/**
	 * Front-end display of widget.
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance ) {

		// Get current category
		$categories = get_the_category();
		if ( empty( $categories ) ) {
			return;
		}
		$category_id = $categories[0]->cat_ID;

		echo $args['before_widget'];

		$title = ( ! empty( $instance['title'] ) ) ? $instance['title'] : __( 'Posts from same brand', 'novus' );
		$title = apply_filters( 'widget_title', $title, $instance );
		echo $args['before_title'] . $title . $args['after_title'];

		// Create WP Query object
		$posts_list = new WP_Query();

		global $post;

		// Number of posts
		$number = ( ! empty( $instance['number'] ) ) ? absint( $instance['number'] ) : 5;

		// Query arguments
		$query_args = array(
			'posts_per_page' => $number,
			'cat'            => $category_id,
			'post__not_in'   => array( $post->ID ),
			'meta_key' => '_thumbnail_id',
			'orderby' => 'date',
	        'order'   => 'DESC', 
		);

		$query_args['meta_query'] = array(
			array(
				'key'     => 'wpr_dealer_id',
				'compare' => 'NOT EXISTS',
			),
		);

		$posts_list->query( $query_args );

		// List style
		$list_style = empty( $instance['list_style'] ) ? 'rounded' : $instance['list_style'];

		if ( $posts_list->have_posts() ) : ?>
            <ul class="aaa posts_list--<?php echo esc_attr( $list_style ); ?>">
				<?php while ( $posts_list->have_posts() ) : $posts_list->the_post(); ?>
                    <li class="flag">
                        <div class="flag-img">
                            <a href="<?php the_permalink(); ?>">
                                <?php
                                $featured_img = get_post_meta( $post->ID, 'lbt_img', true );
                                if( empty( $featured_img ) ) {
                                    $featured_img = get_post_meta( $post->ID, 'backup_image', true );
                                    if ( ! empty( $featured_img ) ) {
                                        update_post_meta( $post->ID, 'lbt_img', $featured_img );
                                    }
                                }
                                if ( ! empty( $featured_img ) ) {
                                    delete_post_meta( $post->ID, 'backup_image' );
                                }
                                $remote_featured_img = get_post_meta( $post->ID, 'wpr_remote_featured_img', true );
                                if ( empty( $remote_featured_img ) ) {
                                    $remote_featured_img = get_post_meta( $post->ID, 'backup_remote_image', true );
                                    if ( ! empty( $remote_featured_img ) ) {
                                        update_post_meta( $post->ID, 'wpr_remote_featured_img', $remote_featured_img );
                                    }
                                }
                                if ( ! empty( $remote_featured_img ) ) {
                                    delete_post_meta( $post->ID, 'backup_remote_image' );
                                }
                                ?>
                                <img src="" data-test="1" onerror="this.src='<?php echo $remote_featured_img; ?>'; if ( this.getAttribute('retry') ) { if( this.getAttribute('retry') == 1 ) { this.src='<?php echo $featured_img; ?>'; this.setAttribute( 'retry', 2 ); } else { this.src='<?php echo get_stylesheet_directory_uri(); ?>/assets/images/post-thumb-cropped.svg'; } } else { this.setAttribute( 'retry', 1 ); }">
                            </a>
                        </div>
                        <div class="flag-bd">
                            <h3 class="post-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                            <p class="custom-post-excert"><?php wpr_custom_length_excerpt(4); ?></p>
                        </div>
                    </li>
				<?php endwhile; ?>
            </ul>
<!--            <a href="--><?php //echo get_category_link( $category_id ); ?><!--">View all offers from this brand &raquo;</a>-->
			<?php
			wp_reset_postdata();
		else : ?>
            <p><?php _e( 'Oh...there are no posts.', 'novus' ); ?></p>
			<?php
		endif;

		echo $args['after_widget'];
	}

	/**
	 * Back-end widget form.
	 *
	 * @see WP_Widget::form()
	 *
	 * @param array $instance Previously saved values from database.
	 */
	public function form( $instance ) {
		$instance = wp_parse_args( (array) $instance, array( 'list_style' => 'rounded' ) );
		$title    = ! empty( $instance['title'] ) ? $instance['title'] : '';
		$number   = isset( $instance['number'] ) ? absint( $instance['number'] ) : 5;
		?>
        <p>
            <label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php _e( 'Title:', 'novus' ); ?></label>
            <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text"
                   value="<?php echo esc_attr( $title ); ?>">
        </p>
        <p>
            <label for="<?php echo esc_attr( $this->get_field_id( 'list_style' ) ); ?>"><?php _e( 'Style:', 'novus' ); ?></label>
            <select name="<?php echo esc_attr( $this->get_field_name( 'list_style' ) ); ?>" id="<?php echo esc_attr( $this->get_field_id( 'list_style' ) ); ?>" class="widefat">
                <option value="rounded"<?php selected( $instance['list_style'], 'rounded' ); ?>><?php _e( 'Rounded', 'novus' ); ?></option>
                <option value="rounded-big"<?php selected( $instance['list_style'], 'rounded-big' ); ?>><?php _e( 'Rounded Big', 'novus' ); ?></option>
                <option value="squared"<?php selected( $instance['list_style'], 'squared' ); ?>><?php _e( 'Squared', 'novus' ); ?></option>
                <option value="squared-big"<?php selected( $instance['list_style'], 'squared-big' ); ?>><?php _e( 'Squared Big', 'novus' ); ?></option>
            </select>
        </p>
        <p>
            <label for="<?php echo esc_attr( $this->get_field_id( 'number' ) ); ?>"><?php _e( 'Number of posts to show:', 'novus' ); ?></label>
            <input id="<?php echo esc_attr( $this->get_field_id( 'number' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'number' ) ); ?>" type="text"
                   value="<?php echo esc_attr( $number ); ?>" size="3"/>
        </p>
		<?php
	}

	/**
	 * Sanitize widget form values as they are saved.
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance Values just sent to be saved.
	 * @param array $old_instance Previously saved values from database.
	 *
	 * @return array Updated safe values to be saved.
	 */
	public function update( $new_instance, $old_instance ) {
		$instance           = $old_instance;
		$instance['title']  = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		$instance['number'] = (int) $new_instance['number'];
		if ( in_array( $new_instance['list_style'], array( 'rounded', 'rounded-big', 'squared', 'squared-big' ) ) ) {
			$instance['list_style'] = $new_instance['list_style'];
		} else {
			$instance['list_style'] = 'rounded';
		}

		return $instance;
	}

} // class Novus_Posts_List_Category_Widget

add_action( 'widgets_init', function () {
	register_widget( 'Novus_Posts_List_Category_Widget' );
} );
