<?php
/**
 * Class LBT_Feed_Archive_Promotions
 *
 * List posts from a subsite via REST API to display promotions on the archive pages
 *
 * Currently only used for the automotive promotions
 *
 * This class uses the REST API endpoint added by: plugins/lbt-data-feed-endpoints/inc/class-posts-tag-category-endpoint.php
 *
 * @package LBT
 */

namespace LBT;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Feed Archive Promotions
 */
class Feed_Archive_Promotions {
	use \LBT\Traits\Singleton;

	public const FEED_SITE_URL = 'https://luxurybuystoday.com/feeds';

	/**
	 * Retrieve posts from a REST API and fill in the template data
	 *
	 * @param WP_Term|null $category The term object.
	 * @param string       $tag_slug The slug.
	 * @param int          $paged Page number.
	 * @param int          $posts_per_page Number of posts per page.
	 * @param string       $target_href The target link.
	 * @param string       $post_name The post name.
	 *
	 * @return array
	 */
	public function get_posts_with_template_data( $category, $tag_slug, $paged, $posts_per_page, $target_href, $post_name = null ) {
		$remote_posts = array();

		$args = array(
			'lbt_user'       => 'site',
			'lbt_secret'     => 'jcF42BshvDUhjXWG4m5ugBb',
			'cat'            => is_a( $category, '\WP_Term' ) ? $category->slug : '',
			'tag'            => $tag_slug,
			'paged'          => $paged,
			'posts_per_page' => $posts_per_page,
		);

		if ( $post_name ) {
			$args['name'] = $post_name;
		}

		$result = wp_remote_get(
			self::FEED_SITE_URL . '/wp-json/datafeed/v1/posts',
			array(
				'body' => $args,
			)
		);

		if ( is_wp_error( $result ) ) {
			$this->log( 'Error fetching remote posts from feed data site!' );
			return $remote_posts;
		}

		if ( 200 !== wp_remote_retrieve_response_code( $result ) ) {
			$this->log( 'Error fetching remote posts from feed data site! Response code not 200!' );
			return $remote_posts;
		}

		$result_body = json_decode( wp_remote_retrieve_body( $result ), true );
		if ( empty( $result_body ) || ! isset( $result_body['posts'] ) ) {
			return $remote_posts;
		}

		$remote_posts = $result_body['posts'];

		$promotions = array();
		foreach ( $remote_posts as  $remote_post ) {
			$promotions[ $remote_post['ID'] ] = array(
				'target_href'       => $target_href,
				'ID'                => $remote_post['slug'],
				'content'           => $remote_post['content'],
				'category'          => $category,
				'listing_item_name' => $remote_post['title'],
				'listing_image_url' => isset( $remote_post['image_src'] ) && isset( $remote_post['image_src']['full'] ) && ! empty( $remote_post['image_src']['full'] ) ? $remote_post['image_src']['full'] : '',
				'listing_city'      => isset( $remote_post['offer_detail_city'] ) ? $remote_post['offer_detail_city'] : '',
				'listing_state'     => isset( $remote_post['offer_detail_state'] ) ? $remote_post['offer_detail_state'] : '',
				'listing_price'     => isset( $remote_post['offer_detail_price'] ) ? $remote_post['offer_detail_price'] : '',
			);
		}

		return $promotions;
	}

	/**
	 * Log messages
	 *
	 * @param string $message Message to be logged.
	 */
	private function log( $message ) {
		if ( ! defined( 'WP_DEBUG_LOG' ) || ! WP_DEBUG_LOG ) {
			return;
		}

		error_log( $message ); //phpcs:ignore
	}
}
