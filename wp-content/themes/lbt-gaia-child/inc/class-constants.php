<?php
/**
 * Constants data
 *
 * @package LBT
 */

namespace LBT;

// phpcs:disable

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Constants class
 */
class Constants {
	const BROYHILL_PARENT_CATEGORY  = 75;
	const BROYHILL_CHILD_CATEGORIES = array(
		36, 37, 38, 39
	);

	const EXCULDE_CATEGORIES = array(
		75, 77, 78
	);

	const HOME_DECOR_CATEGORY = 14;

	const PROMOTIONS_TAG = 'promotions';

	const TEST_ZIPCODES = array(
		75150,
		75201,
		75205,
		75209,
		75225,
		75231,
		75240,
	);

	const DALAS_ZIPCODES = self::TEST_ZIPCODES;

	const DALAS_ZIPCODES_2 = array(
		75201,
		75202,
		75203,
		75204,
		75205,
		75206,
		75207,
		75208,
		75209,
		75210,
		75211,
		75212,
		75214,
		75215,
		75216,
		75217,
		75218,
		75219,
		75220,
		75221,
		75222,
		75223,
		75224,
		75225,
		75226,
		75227,
		75228,
		75229,
		75230,
		75231,
		75232,
		75233,
		75234,
		75235,
		75236,
		75237,
		75238,
		75239,
		75240,
		75241,
		75242,
		75243,
		75244,
		75245,
		75246,
		75247,
		75248,
		75249,
		75250,
		75251,
		75252,
		75253,
		75258,
		75260,
		75261,
		75262,
		75263,
		75264,
		75265,
		75266,
		75267,
		75270,
		75275,
		75277,
		75283,
		75284,
		75285,
		75286,
		75287,
		75294,
		75295,
		75301,
		75303,
		75310,
		75312,
		75313,
		75315,
		75320,
		75323,
		75326,
		75336,
		75339,
		75342,
		75346,
		75350,
		75353,
		75354,
		75355,
		75356,
		75357,
		75359,
		75360,
		75363,
		75364,
		75367,
		75368,
		75370,
		75371,
		75372,
		75373,
		75374,
		75376,
		75378,
		75379,
		75380,
		75381,
		75382,
		75386,
		75387,
		75388,
		75389,
		75390,
		75391,
		75392,
		75393,
		75394,
		75395,
		75396,
		75397,
		75398,
	);

	/**
	 * Private constructor
	 *
	 * @return  void
	 */
	private function __construct() {

	}
}
