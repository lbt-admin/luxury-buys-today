<?php
/**
 * Handles Dealer's API requests
 *
 * @package LBT
 */

namespace LBT\API;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Ajax Dealer Class
 */
class Dealer {
	use \LBT\Traits\Singleton, \LBT\Traits\Routes;

	/**
	 * Initialize class
	 *
	 * @return void
	 */
	private function initialize() {
		$this->init_routes();
	}

	/**
	 * Load routes
	 *
	 * Format: endpoint@callback => http_method
	 */
	private function load_routes() {
		$this->namespace       = 'lbt/v1';
		$this->endpoint_prefix = 'dealers';

		return array(
			'sync@sync_dealer' => 'post',
		);
	}

	/**
	 * Sync dealer from ontraport
	 *
	 * @param  \WP_REST_Request $request The request from the API.
	 *
	 * @return \WP_REST_Response|\WP_Error
	 */
	public function sync_dealer( \WP_REST_Request $request ) {
		$params = $request->get_json_params();

		$params = wp_parse_args( $params, $request->get_params() );

		if ( empty( $params['id'] ) ) {
			return new \WP_Error(
				400,
				__( 'Param "id" is required' )
			);
		}

		$ontraport_dealer_id = sanitize_text_field( wp_unslash( $params['id'] ) );

		$response = \LBT\Dealer::sync_dealership( $ontraport_dealer_id );

		if ( is_wp_error( $response ) ) {
			return $response;
		}

		return new \WP_REST_Response(
			array(
				'code' => 200,
				'data' => $response,
			)
		);
	}
}
