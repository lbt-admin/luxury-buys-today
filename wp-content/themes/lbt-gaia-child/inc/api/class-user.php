<?php
/**
 * Handles User's API requests
 *
 * @package LBT
 */

namespace LBT\API;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

use \LBT\User as MainUser;

/**
 * Ajax User Class
 */
class User {
	use \LBT\Traits\Singleton, \LBT\Traits\Routes;

	/**
	 * Initialize class
	 *
	 * @return void
	 */
	private function initialize() {
		$this->init_routes();
	}

	/**
	 * Load routes
	 *
	 * Format: endpoint@callback => http_method
	 */
	private function load_routes() {
		$this->namespace       = 'lbt/v1';
		$this->endpoint_prefix = 'users';

		return array(
			'save@save_user' => 'post',
		);
	}

    /**
	 * Toggle user brands
	 *
	 * @param  \WP_REST_Request $request The request from the API.
	 *
	 * @return \WP_REST_Response|\WP_Error
	 */
	public function save_user( \WP_REST_Request $request ) {
		$params = $request->get_json_params();

		$params = wp_parse_args( $params, $request->get_params() );

        $params['from_ontraport'] = true;

        // $user = get_user_by( 'email', sanitize_email( $params['email'] ) );
        
        // if ( $user ) {
        //     $params['user_id'] = $user->ID;
        // }

        $response = MainUser::get_instance()->save( $params );

        if ( is_wp_error( $response ) ) {
            return $response;
        }

        return new \WP_REST_Response(
            array(
                'code' => 200,
                'data' => array(
                    'user_id' => $response,
                )
            )
        );
    }
}