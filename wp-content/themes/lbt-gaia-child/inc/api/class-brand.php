<?php
/**
 * Handles Brand's API requests
 *
 * @package LBT
 */

namespace LBT\API;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Ajax Brand Class
 */
class Brand {
	use \LBT\Traits\Singleton, \LBT\Traits\Routes;

	/**
	 * Initialize class
	 *
	 * @return void
	 */
	private function initialize() {
		$this->init_routes();
	}

	/**
	 * Load routes
	 *
	 * Format: endpoint@callback => http_method
	 */
	private function load_routes() {
		$this->namespace       = 'lbt/v1';
		$this->endpoint_prefix = 'brands';

		return array(
			'toggle-saved@toggle_user_brands' => 'post',
			'get@get_user_brands' => 'get',
			'clear-promotions-cache@clear_promotions_cache' => 'post',
		);
	}

	/**
	 * Clear promotions cache
	 *
	 * @param  \WP_REST_Request $request The request from the API.
	 *
	 * @return \WP_REST_Response
	 */
	public function clear_promotions_cache( \WP_REST_Request $request ) {
		$params = $request->get_json_params();
		$params = wp_parse_args( $params, $request->get_params() );

		global $wpdb;

		$table_name = $wpdb->prefix . 'wpr_ontraport_dealerships';

		$dealership_ids = $params['ids'] ?? '';

		if ( empty( $dealership_ids ) ) {
			$dealership_ids = $wpdb->get_col( "SELECT DISTINCT(ontraport_id) FROM {$table_name}" );
		} else {
			$dealership_ids = explode( ',', $dealership_ids );
		}

		if ( ! empty( $dealership_ids ) ) {
			foreach ( $dealership_ids as $dealer_id ) {
				$transient_promotions_key = 'lbtpromotions' . $dealer_id;

				delete_transient( $transient_promotions_key );
			}
		}

		return new \WP_REST_Response(
			array(
				'code' => 200,
			)
		);
	}

	/**
	 * Toggle user brands
	 *
	 * @param  \WP_REST_Request $request The request from the API.
	 *
	 * @return \WP_REST_Response|\WP_Error
	 */
	public function toggle_user_brands( \WP_REST_Request $request ) {
		$params = $request->get_json_params();

		$params = wp_parse_args( $params, $request->get_params() );

		$email    = sanitize_email( wp_unslash( $params['useremail'] ?? '' ) );
		$ip       = sanitize_text_field( wp_unslash( $params['user_ip'] ?? '' ) );
		$s_brands = $params['subscribed_brands'];

		if ( empty( $email ) || ! is_email( $email ) ) {
			return new \WP_Error(
				400,
				'Valid email is required'
			);
		}

		if ( empty( $ip ) ) {
			return new \WP_Error(
				400,
				'Valid IP is required'
			);
		}

		if ( empty( $s_brands ) ) {
			return new \WP_Error(
				400,
				'No brands were sent'
			);
		}

		$user = get_user_by( 'email', $email );

		if ( empty( $user ) ) {
			return new \WP_Error(
				400,
				'There is no registered user with the given email'
			);
		}

		$brand_obj = \LBT\Brand::get_instance();

		$saved_brands = \LBT\Brand::get_user_brands( $user->ID );

		$brands = \LBT\Dealer::get_brand( $saved_brands );

		$saved_brands_slugs = array();

		foreach ( $brands as $brand_data ) {
			$category = get_term_by( 'name', $brand_data->brand, 'category' );

			if ( $category ) {
				$saved_brands_slugs[] = $category->slug;
			}
		}

		$to_be_removed = array_diff( $saved_brands_slugs, $s_brands );
		$to_be_added   = array_diff( $s_brands, $saved_brands_slugs );

		$errors = array();
		$dh     = array();

		foreach ( $to_be_removed as $brand_slug ) {
			$brand_slug = sanitize_text_field( wp_unslash( $brand_slug ) );

			$brand = get_term_by( 'slug', $brand_slug, 'category' );

			if ( empty( $brand ) ) {
				$errors[] = 'Brand not found: ' . $brand_slug;
				continue;
			}

			$dealership = \LBT\Dealer_Locator::get_nearby_dealer( $brand->term_id, false, $user->ID );

			$dh[] = $dealership;

			if ( empty( $dealership ) ) {
				$errors[] = 'Dealership not found in database for: ' . $brand_slug;
				continue;
			}

			try {
				$status = $brand_obj->remove( $dealership['id'], $user->ID );

				if ( is_wp_error( $status ) ) {
					$errors[] = $status->get_error_message() . ' for: ' . $brand_slug;
					continue;
				}
			} catch ( \Exception $e ) {
				$errors[] = $e->getMessage() . ' for: ' . $brand_slug;
				continue;
			}
		}

		foreach ( $to_be_added as $brand_slug ) {
			$brand_slug = sanitize_text_field( wp_unslash( $brand_slug ) );

			$brand = get_term_by( 'slug', $brand_slug, 'category' );

			if ( empty( $brand ) ) {
				$errors[] = 'Brand not found: ' . $brand_slug;
				continue;
			}

			$dealership = \LBT\Dealer_Locator::get_nearby_dealer( $brand->term_id, false, $user->ID );
			$dh[] = $dealership;
			if ( empty( $dealership ) ) {
				$errors[] = 'Dealership not found in database for: ' . $brand_slug;
				continue;
			}

			try {
				$status = $brand_obj->save( $dealership['id'], false, $user->ID );

				if ( is_wp_error( $status ) ) {
					$errors[] = $status->get_error_message() . ' for: ' . $brand_slug;
					continue;
				}
			} catch ( \Exception $e ) {
				$errors[] = $e->getMessage() . ' for: ' . $brand_slug;
				continue;
			}
		}

		if ( ! empty( $errors ) ) {
			return new \WP_Error(
				500,
				'Some errors occured while saving some of the brands.',
				$errors
			);
		}

		$saved_brands = \LBT\Brand::get_user_brands( $user->ID );

		$brands = \LBT\Dealer::get_brand( $saved_brands );

		$brands_list = array();

		if ( ! empty( $brands ) ) {
			foreach ( $brands as $brand_data ) {
				$category = get_term_by( 'name', $brand_data->brand, 'category' );

				if ( $category ) {
					$brands_list[ $category->slug ] = $brand_data->ontraport_id;
				}
			}
		}

		return new \WP_REST_Response(
			array(
				'code'    => 200,
				'message' => 'Success',
				'data'    => $brands_list,
			)
		);
	}

	/**
	 * Get user brands
	 *
	 * @param  \WP_REST_Request $request The request from the API.
	 *
	 * @return \WP_REST_Response|\WP_Error
	 */
	public function get_user_brands( \WP_REST_Request $request ) {
		$params = $request->get_json_params();

		$params = wp_parse_args( $params, $request->get_params() );

		$email = sanitize_email( $params['email'] ?? '' );

		if ( empty( $email ) || ! is_email( $email ) ) {
			return new \WP_Error(
				400,
				'Valid email is required'
			);
		}

		$user = get_user_by( 'email', $email );

		if ( empty( $user ) ) {
			return new \WP_Error(
				400,
				'There is no registered user with the given email'
			);
		}

		$saved_brands = \LBT\Brand::get_user_brands( $user->ID );

		if ( empty( $saved_brands ) ) {
			return new \WP_Error(
				404,
				'This user has no saved brands'
			);
		}

		$brands = \LBT\Dealer::get_brand( $saved_brands );

		$brands_list = array();

		if ( ! empty( $brands ) ) {
			foreach ( $brands as $brand_data ) {
				$category = get_term_by( 'name', $brand_data->brand, 'category' );

				if ( $category ) {
					$brands_list[ $category->slug ] = $brand_data->ontraport_id;
				}
			}
		}

		return new \WP_REST_Response(
			array(
				'code'    => 200,
				'message' => 'Success',
				'data'    => $brands_list,
			)
		);
	}
}
