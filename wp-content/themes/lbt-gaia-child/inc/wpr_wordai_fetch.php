<?php
/**
 * Created by PhpStorm.
 * User: ovidiugeorgeirodiu
 * Date: 3/21/18
 * Time: 10:31 AM
 */

/**
 * Custom add meta to avoid added_meta hook execution
 *
 * @param $meta_type
 * @param $object_id
 * @param $meta_key
 * @param $meta_value
 * @param bool $unique
 *
 * @return bool|int|mixed|void
 */
function wpr_add_post_meta( $meta_type, $object_id, $meta_key, $meta_value, $unique = false ) {
	global $wpdb;

	if ( ! $meta_type || ! $meta_key || ! is_numeric( $object_id ) ) {
		return false;
	}

	$object_id = absint( $object_id );
	if ( ! $object_id ) {
		return false;
	}

	$table = _get_meta_table( $meta_type );
	if ( ! $table ) {
		return false;
	}

	$column = sanitize_key( $meta_type . '_id' );

	// expected_slashed ($meta_key)
	$meta_key   = wp_unslash( $meta_key );
	$meta_value = wp_unslash( $meta_value );
	$meta_value = sanitize_meta( $meta_key, $meta_value, $meta_type );

	/**
	 * Filters whether to add metadata of a specific type.
	 *
	 * The dynamic portion of the hook, `$meta_type`, refers to the meta
	 * object type (comment, post, or user). Returning a non-null value
	 * will effectively short-circuit the function.
	 *
	 * @since 3.1.0
	 *
	 * @param null|bool $check Whether to allow adding metadata for the given type.
	 * @param int $object_id Object ID.
	 * @param string $meta_key Meta key.
	 * @param mixed $meta_value Meta value. Must be serializable if non-scalar.
	 * @param bool $unique Whether the specified meta key should be unique
	 *                              for the object. Optional. Default false.
	 */
	$check = apply_filters( "add_{$meta_type}_metadata", null, $object_id, $meta_key, $meta_value, $unique );
	if ( null !== $check ) {
		return $check;
	}

	if ( $unique && $wpdb->get_var( $wpdb->prepare(
			"SELECT COUNT(*) FROM $table WHERE meta_key = %s AND $column = %d",
			$meta_key, $object_id ) ) ) {
		return false;
	}

	$meta_value = maybe_serialize( $meta_value );

	$result = $wpdb->insert( $table, array(
		$column      => $object_id,
		'meta_key'   => $meta_key,
		'meta_value' => $meta_value
	) );

	if ( ! $result ) {
		return false;
	}

	$mid = (int) $wpdb->insert_id;

	wp_cache_delete( $object_id, $meta_type . '_meta' );

	return $mid;
}

/**
 * Generate Edgar Spin Content
 *
 * @param $post_id
 */
function wpr_generate_edgar_wordai_spin( $post_id ) {
	$content = apply_filters( 'the_content', get_post_field( 'post_content', $post_id ) );
	$content = str_replace( array( "BROYHILL COMMENTS", "BROYHILL COMMENT" ), "", $content );
	preg_match( '/<p>(.*):/', $content, $thetitle );
	if ( ! empty( $thetitle ) && strpos( $thetitle[0], 'http' ) === false ) {
		$content = preg_replace( '/<p>(.*):/', '', $content, 1 );
	}

	$content = preg_replace( '/Source Link:.*/', '', $content );
	$content = preg_replace( '/\s+/', ' ', trim( $content ) );
	$content = wp_strip_all_tags( $content );

	if ( ! empty( $content ) ) {
		// Try to generate mixed content straight away
		$post_content = Wpr_Wordai_api::wpr_wordai_api_call( $content, 'Unique', array( 'returnspin' => 'false', 'perfect_tense' => 'correct' ) );

		// Used for Ed comments
		if ( ! empty( $post_content ) ) {
			$spin_the_content = str_replace( '#', '', $post_content );

			wpr_add_post_meta( 'post', absint( $post_id ), 'wpr_wordai_string', $spin_the_content );
		} else {
			// If there is no API Response, add this in wp_cron queue
			$register_data = array(
				'fetch_wordai_string' => array(
					'ID'           => $post_id,
					'post_content' => wp_strip_all_tags( $content ),
					'edd_do_spin'  => $post_id,
				),
			);

			$request_spin = new WPR_Run_Process();
			$request_spin->wpr_init_cron( $register_data );
		}
	}

	// Generate custom comment
	$categories = get_the_category( $post_id );
	$category   = get_category( $categories[0]->term_id );
	$cat_id     = $category->term_id;

	$args = array(
		'post_type'      => 'post',
		'post_status'    => 'published',
		'posts_per_page' => 1,
		'date_query'     => array( 'after' => '-7 days' ),
		'orderby'        => 'rand',
		'meta_key'       => 'wpr_wordai_string',
		'cat'            => absint( $cat_id ),
		'post__not_in'   => array( $post_id ),
	);

	$edd_posts    = new WP_Query( $args );
	$post_content = '';
	$edd_post_id  = '';
	if ( $edd_posts->have_posts() ) {
		// The Loop
		while ( $edd_posts->have_posts() ) {
			$edd_posts->the_post();
			$post_content = get_post_meta( $edd_posts->post->ID, 'wpr_wordai_string', true );
			$edd_post_id  = $edd_posts->post->ID;
		}
		wp_reset_postdata();
	} else {
		$args = array(
			'post_type'      => 'post',
			'post_status'    => 'published',
			'posts_per_page' => 1,
			'orderby'        => 'rand',
			'meta_key'       => 'wpr_wordai_string',
			'cat'            => absint( $cat_id ),
			'post__not_in'   => array( $post_id ),
		);

		$edd_posts = new WP_Query( $args );

		if ( $edd_posts->have_posts() ) {
			// The 2nd Loop
			while ( $edd_posts->have_posts() ) {
				$edd_posts->the_post();
				$post_content = get_post_meta( $edd_posts->post->ID, 'wpr_wordai_string', true );
				$edd_post_id  = $edd_posts->post->ID;
			}
			wp_reset_postdata();
		}
	}

	// Use the Spintax
	$spintax = new Spintax();
	wpr_add_post_meta( 'post', absint( $post_id ), 'wpr_spin_the_content', $spintax->process( wp_strip_all_tags( $post_content ) ) );
	wpr_add_post_meta( 'post', absint( $post_id ), 'wpr_spinned_edd_article', absint( $edd_post_id ) );
}

/**
 * Generate random comment
 *
 * @param $post_id
 */
function wpr_generate_random_article_content( $post_id ) {
	$edgar_cat  = array(
		'8'  => '36',
		'13' => '37',
		'14' => '38',
		'17' => '39',
	);
	$categories = get_the_category( $post_id );
	$category   = get_category( $categories[0]->term_id );
	$cat_id     = $category->term_id;

	if ( $category->parent > 0 ) {
		$cat_id = $category->parent;

	}
	$get_post_from = $edgar_cat[ $cat_id ];

	$args = array(
		'post_type'      => 'post',
		'post_status'    => 'published',
		'posts_per_page' => 1,
		'date_query'     => array( 'after' => '-7 days' ),
		'orderby'        => 'rand',
		'meta_key'       => 'wpr_wordai_string',
		'cat'            => absint( $get_post_from ),
	);

	$edd_posts    = new WP_Query( $args );
	$post_content = '';
	$edd_post_id  = '';
	if ( $edd_posts->have_posts() ) {
		// The Loop
		while ( $edd_posts->have_posts() ) {
			$edd_posts->the_post();
			$post_content = get_post_meta( $edd_posts->post->ID, 'wpr_wordai_string', true );
			$edd_post_id  = $edd_posts->post->ID;
		}
		wp_reset_postdata();
	} else {
		$args = array(
			'post_type'      => 'post',
			'post_status'    => 'published',
			'posts_per_page' => 1,
			'orderby'        => 'rand',
			'meta_key'       => 'wpr_wordai_string',
			'cat'            => absint( $get_post_from ),
		);

		$edd_posts = new WP_Query( $args );

		if ( $edd_posts->have_posts() ) {
			// The 2nd Loop
			while ( $edd_posts->have_posts() ) {
				$edd_posts->the_post();
				$post_content = get_post_meta( $edd_posts->post->ID, 'wpr_wordai_string', true );
				$edd_post_id  = $edd_posts->post->ID;
			}
			wp_reset_postdata();
		}
	}

	if ( ! empty( $post_content ) ) {
		// Use the Spintax
		$spintax = new Spintax();
		wpr_add_post_meta( 'post', absint( $post_id ), 'wpr_spin_the_content', $spintax->process( wp_strip_all_tags( $post_content ) ) );
		wpr_add_post_meta( 'post', absint( $post_id ), 'wpr_spinned_edd_article', absint( $edd_post_id ) );
	} else {
		$fetch_random_edd = wpr_fetch_random_edd_content( $get_post_from );
		if ( $fetch_random_edd ) {
			$register_data = array(
				'fetch_wordai_string' => array(
					'ID'           => $post_id,
					'edd_post_id'  => $fetch_random_edd['post_id'],
					'post_content' => wp_strip_all_tags( $fetch_random_edd['post_content'] ),
				),
			);

			$request_spin = new WPR_Run_Process();
			$request_spin->wpr_init_cron( $register_data );

			update_post_meta( absint( $post_id ), 'wpr_spinned_edd_article', absint( $fetch_random_edd['post_id'] ) );
		}
	}
}

/**
 * Fetch random Edd Article content
 *
 * @param $get_post_from
 * @param int $exclude A post ID to exclude from the query (optional).
 *
 * @return array
 */
function wpr_fetch_random_edd_content( $get_post_from, $exclude = null ) {
	$args = array(
		'post_type'      => 'post',
		'post_status'    => 'published',
		'posts_per_page' => 1,
		'orderby'        => 'rand',
		'cat'            => absint( $get_post_from ),
	);

	if ( $exclude ) {
		$args['post__not_in'] = array( absint( $exclude ) );
	}

	$edd_posts = new WP_Query( $args );

	$response = array();
	if ( $edd_posts->have_posts() ) {
		// The 2nd Loop
		while ( $edd_posts->have_posts() ) {
			$edd_posts->the_post();

			$content = apply_filters( 'the_content', get_post_field( 'post_content', $edd_posts->post->ID ) );
			$content = str_replace( "BROYHILL COMMENTS", "", $content );

			$response['post_id']      = $edd_posts->post->ID;
			$response['post_content'] = $content;
		}
		wp_reset_postdata();
	}

	return $response;
}
