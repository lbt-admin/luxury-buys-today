<?php
/**
 * Profile related stuff
 *
 * @package LBT
 */

namespace LBT;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Profile Class
 */
class Profile {
	use \LBT\Traits\Singleton;

	/**
	 * Initialize class
	 *
	 * @return void
	 */
	private function initialize() {
		add_filter( 'wpfep_fields_profile', array( $this, 'add_tab_fields' ) );
		add_filter( 'wpfep_fields_profile', array( $this, 'remove_fields' ) );
		add_action( 'update_user_meta', array( $this, 'before_zip_update' ), 10, 4 );
		add_action( 'updated_user_meta', array( $this, 'after_zip_update' ), 10, 4 );
	}

	/**
	 * Add tab fields for the profile.
	 *
	 * Used in WP Front End Profile Plugin
	 *
	 * @param  array $fields List of existing fields.
	 *
	 * @return array
	 */
	public function add_tab_fields( $fields ) {
		$fields[] = array(
			'id'      => 'street',
			'label'   => 'Street',
			'desc'    => '',
			'type'    => 'text',
			'classes' => 'street',
		);

		$fields[] = array(
			'id'      => 'city',
			'label'   => 'City',
			'desc'    => '',
			'type'    => 'text',
			'classes' => 'city',
		);

		$fields[] = array(
			'id'      => 'state',
			'label'   => 'State',
			'desc'    => '',
			'type'    => 'text',
			'classes' => 'state',
		);

		$fields[] = array(
			'id'      => 'zip_code',
			'label'   => 'Zip Code',
			'desc'    => '',
			'type'    => 'text',
			'classes' => 'zip',
		);

		$fields[] = array(
			'id'      => 'phone',
			'label'   => 'Cell Phone',
			'desc'    => '',
			'type'    => 'text',
			'classes' => 'phone',
		);

		return $fields;
	}

	/**
	 * Remove fields from profile
	 *
	 * @param  array $fields List of existing fields.
	 *
	 * @return array
	 */
	public function remove_fields( $fields ) {
		$desc_unset = false;
		$url_unset  = false;

		foreach ( $fields as $key => $val ) {
			if ( 'description' === $val['id'] ) {
				unset( $fields[ $key ] );
				$desc_unset = true;
			} elseif ( 'user_url' === $val['id'] ) {
				unset( $fields[ $key ] );
				$url_unset = true;
			}

			if ( $desc_unset && $url_unset ) {
				break;
			}
		}

		return $fields;
	}

	/**
	 * On zip code change
	 *
	 * @param  [type] $meta_id     Description
	 * @param  [type] $object_id   Description
	 * @param  [type] $meta_key    Description
	 * @param  [type] $_meta_value Description
	 *
	 * @return void                Description
	 */
	public function before_zip_update( $meta_id, $object_id, $meta_key, $_meta_value ) {
		if ( 'zip_code' !== $meta_key ) {
			return;
		}

		if ( empty( $_POST['zip_code'] ) && empty( $_POST['zip']) ) {
			return;
		}

		$old_zip = get_user_meta( $object_id, 'zip_code', true );

		$new_zip = ! empty( $_POST['zip'] ) ? $_POST['zip'] : $_POST['zip_code'];

		if ( $old_zip === $new_zip ) {
			return;
		}

		set_transient( 'zipupdate' . $object_id, 1, 5 );
	}

	/**
	 * Undocumented function
	 *
	 * @param  [type] $meta_id     Description
	 * @param  [type] $object_id   Description
	 * @param  [type] $meta_key    Description
	 * @param  [type] $_meta_value Description
	 *
	 * @return void                Description
	 */
	public function after_zip_update( $meta_id, $object_id, $meta_key, $_meta_value ) {
		if ( 'zip_code' !== $meta_key ) {
			return;
		}

		if ( ! get_transient( 'zipupdate' . $object_id ) ) {
			return;
		}

		$brand_instance = Brand::get_instance();

		$current_brands = $brand_instance->get_user_brands( $object_id );

		$brands_data = Dealer::get_brand( $current_brands );

		$saved_brands_id = array();

		foreach ( $brands_data as $brand_data ) {
			$category = get_term_by( 'name', $brand_data->brand, 'category' );

			if ( $category ) {
				$saved_brands_id[ $category->slug . '-' .  $category->term_id ] = $category->term_id;
			}
		}

		if ( ! empty( $current_brands ) ) {
			foreach ( $current_brands as $dealership_id ) {
				$response = $brand_instance->remove( $dealership_id, $object_id );

				if ( is_wp_error( $response ) ) {
					$errors[] = $response->get_error_message();
				}
			}
		}

		$dealerships_list = array();

		foreach ( $saved_brands_id as $brand_name => $brand_id ) {
			$dealership = Dealer_Locator::get_nearby_dealer( $brand_id );

			if ( empty( $dealership ) ) {
				$errors[] = 'Dealership not found in database for: ' . $brand_name;
				continue;
			}

			try {
				$status = $brand_instance->save( $dealership['id'], false, $object_id );

				if ( is_wp_error( $status ) ) {
					$errors[] = $status->get_error_message() . ' for: ' . $brand_name;
					continue;
				} else {
					$slug = explode( '-', $brand_name )[0] ?? $brand_id;

					$dealerships_list[ $slug ] = $dealership['id'];
				}
			} catch ( \Exception $e ) {
				$errors[] = $e->getMessage() . ' for: ' . $brand_name;
			}
		}

		delete_transient( 'zipupdate' . $object_id );

		Dealer::send_dealerships_to_news_site( $dealerships_list, $object_id );
	}
}
