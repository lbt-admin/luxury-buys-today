<?php
/**
 * Novus Subscribe Form widget.
 */
class Novus_Subscribe_Form_Widget extends WP_Widget {

  /**
   * Register widget with WordPress.
   */
  function __construct() {
    parent::__construct(
      'subscribe_form', // Base ID
      __( 'Novus / Subscribe Form', 'novus' ), // Name
      array( 'description' => __( 'Displays MailChimp subscribe form for current category.', 'novus' ), ) // Args
    );
  }

  /**
   * Front-end display of widget.
   *
   * @see WP_Widget::widget()
   *
   * @param array $args     Widget arguments.
   * @param array $instance Saved values from database.
   */
  public function widget( $args, $instance ) {

    // Bail out if user is not signed in
    if ( !function_exists( 'get_terms_meta' ) ) return;

    $categories = get_the_category();
    if ( empty( $categories ) ) return;
    $category_id = $categories[0]->cat_ID;
    $category_name = $categories[0]->name;
    $category_meta = get_terms_meta( $category_id, 'mailchimp_form_id' );
    // Bail out if there is no category id
    if ( empty( $category_meta ) ) return;

    echo $args['before_widget'];

    $title = ( ! empty( $instance['title'] ) ) ? $instance['title'] : __( 'Newsletter', 'novus' );
    $title = apply_filters( 'widget_title', $title, $instance );
    echo $args['before_title'] . $title . $args['after_title'];

    // Text before form
    $content = ( ! empty( $instance['content'] ) ) ? $instance['content'] : '';
    echo wpautop( $content );
    echo "<p>Receive exclusive offers on your email from $category_name!</p>";
    echo do_shortcode( '[mc4wp_form id="' . $category_meta[0] . '"]' );

    echo $args['after_widget'];
  }

  /**
   * Back-end widget form.
   *
   * @see WP_Widget::form()
   *
   * @param array $instance Previously saved values from database.
   */
  public function form( $instance ) {
    $title = ! empty( $instance['title'] ) ? $instance['title'] : '';
    $content = ! empty( $instance['content'] ) ? $instance['content'] : '';
    ?>
    <p>
      <label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php _e( 'Title:', 'novus' ); ?></label>
      <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
    </p>
    <p>
      <label for="<?php echo esc_attr( $this->get_field_id( 'content' ) ); ?>"><?php _e( 'Text before form:', 'novus' ); ?></label>
      <textarea class="widefat" rows="5" id="<?php echo esc_attr( $this->get_field_id( 'content' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'content' ) ); ?>"><?php echo esc_textarea( $content ); ?></textarea>
    </p>
    <?php
  }

  /**
   * Sanitize widget form values as they are saved.
   *
   * @see WP_Widget::update()
   *
   * @param array $new_instance Values just sent to be saved.
   * @param array $old_instance Previously saved values from database.
   *
   * @return array Updated safe values to be saved.
   */
  public function update( $new_instance, $old_instance ) {
    $instance = $old_instance;
    $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
    $instance['content'] = ( ! empty( $new_instance['content'] ) ) ? wp_kses_post( $new_instance['content'] ) : '';

    return $instance;
  }

} // class Novus_Subscribe_Form_Widget

add_action( 'widgets_init', function(){
  register_widget( 'Novus_Subscribe_Form_Widget' );
});
