<?php
/**
 * Handles Offer's AJAX calls
 *
 * @package LBT
 */

namespace LBT\AJAX;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Ajax Offer Class
 */
class Offer {
	use \LBT\Traits\Singleton;

	/**
	 * Initialize class
	 *
	 * @return void
	 */
	private function initialize() {
		add_action( 'wp_ajax_lbt_offer_toggle', array( $this, 'toggle_offer_post' ) );
	}

	/**
	 * AJAX - Toggle offer post
	 *
	 * Action: lbt_offer_toggle
	 *
	 * @return void
	 */
	public function toggle_offer_post() {
		$nonce_check = check_ajax_referer( 'lbt-offer-toggle-nonce', '__nonce', false );

		// if ( ! $nonce_check ) {
		// 	wp_send_json_error(
		// 		__( 'Security check failed. Refresh page and try again', 'lbt' ),
		// 		403
		// 	);
		// }

		$post_id                 = absint( $_POST['post_id'] ?? 0 );
		$ontraport_dealership_id = absint( $_POST['dealership_id'] ?? 0 );

		if ( ! $post_id ) {
			wp_send_json_error(
				__( 'Post ID is missing. Refresh page and try again', 'lbt' ),
				400
			);
		}

		$user_offer = \LBT\Offer::get_user_offers();

		if ( in_array( (int) $post_id, $user_offer ) ) { //phpcs:ignore
			\LBT\Offer::remove( $post_id );
			$action = 'remove';
		} else {
			$action = 'save';
			\LBT\Offer::save( $post_id, $ontraport_dealership_id );
		}

		wp_send_json_success( $action );
	}
}
