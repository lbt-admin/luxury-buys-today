<?php

/**
 * Handles Brand's AJAX calls
 *
 * @package LBT
 */

namespace LBT\AJAX;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Ajax Brand Class
 */
class Archive_Feed {
	use \LBT\Traits\Singleton;

	/**
	 * Initialize class
	 *
	 * @return void
	 */
	private function initialize() {
		add_action('wp_ajax_fetch_archive_posts', array( $this, 'display_page' ) );
		add_action('wp_ajax_nopriv_fetch_archive_posts', array( $this, 'display_page' ) );
	}

	public function display_page() {
		?>
			<?php
            echo '<div class="posts-wrapper-area">';
			$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
			if ( $paged === 1 ) {
				$paged = isset( $_GET[ 'paged' ] ) ? $_GET[ 'paged' ] : 1;
			}

            $type    = $_GET['type'] ?? 'category';
            $type_id = $_GET['type_id'] ?? 0;
            $brand_id = $_GET['brand_id'] ?? 0;

            $posts_query_args = array(
                'post_type'      => 'post',
                'posts_per_page' => 13,
                'paged'          => $paged,
            );

            if ( ! empty( $type ) && ! empty( $type_id ) ) {
                if ( 'author' !== $type ) {
                    if ( ! empty( $brand_id ) ) {
                        $type_id = $brand_id;
                    }
                    $posts_query_args['tax_query'] = array(
                        array(
                            'taxonomy' => $type,
                            'terms'    => array( (int) $type_id )
                        )
                    );
                } else {
                    $posts_query_args['author'] = (int) $type_id;
                }
            }

			$posts_query = new \WP_Query( $posts_query_args );

			if ( $posts_query->have_posts() ) :
				while ( $posts_query->have_posts() ) :
					$posts_query->the_post();
					get_template_part( 'partials/content', 'blog-big-article' );
				endwhile;
                wp_reset_postdata();
			endif;
            echo '</div>';
			?>
			<div class="post-navigation archive">
                <?php if ( $paged > 1 ) { ?>
                    <a class="post-navigation-prev" data-page="<?php echo $paged - 1; ?>" style="margin-right: 15px;" href="javascript:void(0)">Previous</a>
                <?php } ?>
                <a class="post-navigation-next" data-page="<?php echo $paged + 1; ?>" href="javascript:void(0)">Next</a>
            </div>
		<?php
		exit();
	}
}