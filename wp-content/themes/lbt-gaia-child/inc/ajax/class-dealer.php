<?php
/**
 * Handles Dealer's AJAX calls
 *
 * @package LBT
 */

namespace LBT\AJAX;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Ajax Dealer Class
 */
class Dealer {
	use \LBT\Traits\Singleton;

	/**
	 * Initialize class
	 *
	 * @return void
	 */
	private function initialize() {
		add_action( 'wp_ajax_lbt_get_closest_brand_dealer', array( $this, 'get_closest_brand_dealer' ) );
		add_action( 'wp_ajax_nopriv_lbt_get_closest_brand_dealer', array( $this, 'get_closest_brand_dealer' ) );
		add_action( 'wp_ajax_lbt_get_brand_dealer_promotions', array( $this, 'get_brand_dealer_promotions' ) );
		add_action( 'wp_ajax_nopriv_lbt_get_brand_dealer_promotions', array( $this, 'get_brand_dealer_promotions' ) );
		add_action( 'wp_ajax_lbt_send_dealer_cta_request', array( $this, 'send_cta_request' ) );
		add_action( 'wp_ajax_lbt_check_availability', array( $this, 'check_availability' ) );
		// add_action( 'wp_login', array( $this, 'cache_dealers_for_brands' ), 10, 2 );
		// add_action( 'lbt_fetch_dealers_for_brands_per_user', array( $this, 'fetch_dealers_for_brands' ), 10, 2 );
		// add_action( 'wp_ajax_nopriv_cache_dealers_for_brands', array( $this, 'cache_dealers_for_brands_for_not_logged_in' ) );
	}

	/**
	 * On user login fetch dealships for each brand for current user and cache it
	 *
	 * @param  string  $user_login The userlogin.
	 * @param  WP_User $user       The user object.
	 *
	 * @return void
	 */
	public function cache_dealers_for_brands( $user_login, $user ) {
		$this->cache_dealers_for_brands_2( false, $user );
	}

	/**
	 * AJAX - Cache dealers for not logged in users in browser cache
	 *
	 * @return void
	 */
	public function cache_dealers_for_brands_for_not_logged_in() {
		$ip = sanitize_text_field( wp_unslash( $_POST['ip'] ?? '' ) );

		$this->cache_dealers_for_brands_2( $ip );

		wp_send_json_success();
	}

	/**
	 * Cache users for brands
	 *
	 * @param  boolean|string $ip   The ip if request for non logged in users.
	 * @param  boolean|WP_User $user User object if request for logged in users.
	 *
	 * @return void|array
	 */
	public function cache_dealers_for_brands_2( $ip = false, $user = false ) {
		$brands = \LBT\Category::get_instance()->get_main_categories_children( array( 8, 13, 14, 17 ) );

		$counter = 0;

		$last_part_of_key = \LBT\User::get_user_zip( false, $ip );

		if ( empty( $last_part_of_key ) && is_user_logged_in() ) {
			$last_part_of_key = $user->ID;
		}

		foreach ( $brands as $brand ) {
			$brand_id = $brand->term_id;
			$key = 'lbtdealer' . $brand_id . $last_part_of_key;

			if ( ! empty( get_transient( $key ) ) ) {
				continue;
			}

			wp_schedule_single_event( time() + $counter, 'lbt_fetch_dealers_for_brands_per_user', array( $key, $brand_id ) );

			$counter += random_int( 1, 30 );
		}
	}

	/**
	 * Fetch dealers for brands for logged in user
	 *
	 * @param string $key The cache key.
	 * @param int    $brand_id The brand id.
	 *
	 * @return array
	 */
	public function fetch_dealers_for_brands( $key, $brand_id ) {
		$dealer = \LBT\Dealer_Locator::get_nearby_dealer( $brand_id );

		if ( ! empty( $dealer ) ) {
			set_transient( $key, $dealer );
		}

		return $dealer;
	}

	/**
	 * AJAX - Get closest brand dealer
	 *
	 * Action: lbt_get_closest_brand_dealer
	 *
	 * @return void
	 */
	public function get_closest_brand_dealer() {
		$nonce_check = check_ajax_referer( 'lbt-get-closes-brand-dealer-nonce', '__nonce', false );

		// if ( ! $nonce_check ) {
		// 	wp_send_json_error(
		// 		__( 'Security check failed. Refresh page and try again', 'lbt' ),
		// 		403
		// 	);
		// }

		$brand_id       = absint( $_POST['brand_id'] ?? 0 );
		$post_id        = absint( $_POST['post_id'] ?? 0 );
		$is_single_post = (bool) $_POST['is_single_post'] ?? false;

		if ( ! $brand_id ) {
			wp_send_json_error(
				__( 'Brand ID missing', 'lbt' ),
				400
			);
		}

		$dealer = \LBT\Dealer_Locator::get_nearby_dealer( $brand_id );

		if ( $dealer ) {
			ob_start();
			get_template_part(
				'partials/dealer',
				'info-box',
				array(
					'dealer'    => $dealer,
					'brand_id'  => $brand_id,
					'post_id'   => $post_id,
					'is_single' => $is_single_post,
				)
			);
			$dealer_box = ob_get_clean();
			wp_send_json_success( $dealer_box );
		}

		wp_send_json_success();
	}

	/**
	 * AJAX - Get brand dealer promotions
	 *
	 * Action: lbt_get_brand_dealer_promotions
	 *
	 * @return void
	 */
	public function get_brand_dealer_promotions() {
		$nonce_check = check_ajax_referer( 'lbt-get-brand-dealer-promotions-nonce', '__nonce', false );

		$brand_id       = absint( $_POST['brand_id'] ?? 0 );
		$current_page   = absint( $_POST['current_page'] ?? 1 );
		$post_id        = absint( $_POST['post_id'] ?? 0 );
		$is_single_post = (bool) $_POST['is_single_post'] ?? false;

		if ( ! $brand_id ) {
			wp_send_json_error(
				__( 'Brand ID missing', 'lbt' ),
				400
			);
		}

		$dealer = \LBT\Dealer_Locator::get_nearby_dealer( $brand_id );

		$dealer_box = '';

		if ( $dealer ) {
			ob_start();
			get_template_part(
				'partials/dealer',
				'info-box',
				array(
					'dealer'    => $dealer,
					'brand_id'  => $brand_id,
					'post_id'   => $post_id,
					'is_single' => $is_single_post,
				)
			);
			$dealer_box = ob_get_clean();

			$transient_promotions_key = 'lbtpromotions' . $dealer['id'];
		}

		$promotions_box = false;

		if ( $dealer ) {
			$promotions_box = get_transient( $transient_promotions_key );
		}

		if ( ! $promotions_box ) {
			$brand = get_category( $brand_id );

			$promotions_column_posts = \LBT\Category::get_promotions_column_posts( $brand, $current_page );

			ob_start();
			get_template_part(
				'partials/dealer',
				'promotions-block',
				array(
					'promotions_column_posts' => $promotions_column_posts,
				)
			);
			$promotions_box = ob_get_clean();

			if ( $dealer ) {
				set_transient( $transient_promotions_key, $promotions_box );
			}
		}

		wp_send_json_success(
			array(
				'promotions_box' => $promotions_box,
				'dealer_box'     => $dealer_box,
			)
		);

	}

	/**
	 * AJAX - Get closest brand dealer
	 *
	 * Action: lbt_send_dealer_cta_request
	 *
	 * @return void
	 */
	public function send_cta_request() {
		$nonce_check = check_ajax_referer( 'lbt-send-dealer-cta-request-nonce', '__nonce', false );

		// if ( ! $nonce_check ) {
		// 	wp_send_json_error(
		// 		__( 'Security check failed. Refresh page and try again', 'lbt' ),
		// 		403
		// 	);
		// }

		$can_user_access = \LBT\User::get_user_location( 'zip' );

		if ( ! $can_user_access ) {
			wp_send_json_error(
				'coming_soon',
				403
			);
		}

		$brand_id      = absint( $_POST['brand_id'] ?? 0 );
		$dealership_id = absint( $_POST['dealership_id'] ?? 0 );
		$action        = sanitize_text_field( wp_unslash( $_POST['cta_action'] ?? '' ) );
		$post_id       = absint( $_POST['post_id'] ?? 0 );
		$count_fav     = (bool) $_POST['count_fav'] ?? false;

		if ( ! $brand_id ) {
			wp_send_json_error(
				__( 'Brand ID missing', 'lbt' ),
				400
			);
		}

		if ( ! $dealership_id ) {
			wp_send_json_error(
				__( 'Dealership ID missing', 'lbt' ),
				400
			);
		}

		$response = \LBT\Dealer::get_instance()->send_cta_request( $dealership_id, $brand_id, $action, $post_id );
		$save_brand_response = \LBT\Brand::get_instance()->save( $dealership_id, $post_id, true );

		if ( is_wp_error( $response ) ) {
			wp_send_json_error(
				$response->get_error_message(),
				$response->get_error_code()
			);
		}

		if ( $count_fav && $post_id ) {
			\LBT\Post::update_count( $post_id );
		}

		wp_send_json_success( $response );
	}

	/**
	 * AJAX - Check if action available for current user region
	 *
	 * Action: lbt_check_availability
	 *
	 * @return void
	 */
	public function check_availability() {
		$nonce_check = check_ajax_referer( 'lbt-check-availability-nonce', '__nonce', false );

		// if ( ! $nonce_check ) {
		// 	wp_send_json_error(
		// 		__( 'Security check failed. Refresh page and try again', 'lbt' ),
		// 		403
		// 	);
		// }

		$can_user_access = \LBT\User::get_user_location( 'zip' );

		if ( ! $can_user_access ) {
			wp_send_json_error(
				'coming_soon',
				403
			);
		}

		wp_send_json_success();
	}
}
