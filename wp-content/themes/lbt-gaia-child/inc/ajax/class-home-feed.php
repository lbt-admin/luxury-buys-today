<?php

/**
 * Handles Brand's AJAX calls
 *
 * @package LBT
 */

namespace LBT\AJAX;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Ajax Brand Class
 */
class Home_Feed {
	use \LBT\Traits\Singleton;

	/**
	 * Initialize class
	 *
	 * @return void
	 */
	private function initialize() {
		add_action('wp_ajax_fetch_home_page_steps', array( $this, 'display_page' ) );
		add_action('wp_ajax_nopriv_fetch_home_page_steps', array( $this, 'display_page' ) );
	}

	public function display_page() {
		?>
		<div class="inner-container">
			<?php
			$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
			if ( $paged === 1 ) {
				$paged = isset( $_GET[ 'paged' ] ) ? $_GET[ 'paged' ] : 1;
			}

			if( $paged > 1 ) {
				$posts_query_args = array(
					'post_type'      => 'post',
					'posts_per_page' => 16,
					'paged'          => $paged
				);
			} else {
				$posts_query_args = array(
					'post_type'      => 'post',
					'posts_per_page' => 16,
					'offset'         => 5,
				);
			}

			$posts_query = new \WP_Query( $posts_query_args );

			if ( $posts_query->have_posts() ) :
				while ( $posts_query->have_posts() ) :
					$posts_query->the_post();
					$post_thumbnail = \LBT\Post::get_thumbnail( get_the_ID(), 'wpr_listing' );
					?>
					<article class="home-post-big offers">
						<?php if ( $post_thumbnail ) : ?>
							<a href="<?php echo esc_url( get_permalink() ); ?>" class="featured-image">
								<img
									class="image-cover"
									src=""
									data-test="1"
									onerror="this.src='<?php echo esc_url( $post_thumbnail ); ?>'; if ( this.getAttribute('retry') ) { if( this.getAttribute('retry') == 1 ) { this.src='<?php echo esc_url( $post_thumbnail ); ?>'; this.setAttribute( 'retry', 2 ); } else { this.src='<?php echo esc_url( LBT_THEME_URL ); ?>assets/images/post-thumb-cropped.svg'; } } else { this.setAttribute( 'retry', 1 ); }">
							</a>
						<?php endif; ?>
						<div class="post-content">
							<h4 class="title">
								<?php $post_title = ucwords( strtolower( get_the_title() ) ); ?>
								<a href="<?php echo esc_url( get_permalink() ); ?>" class="title">
									<?php echo esc_html( mb_strimwidth( $post_title, 0, 49, '...' ) ); ?>
								</a>
							</h4>
							<div class="post-meta">
								<span>Published by: </span>
								<a href="http://luxurybuystoday.tumblr.com/" target="_blank">Ed Broyhill</a> |
								<a href="javascript:void(0);"><?php echo get_the_date(); ?></a>
							</div>
							<p class="description">
								<?php
								$content   = wpr_display_ed_comment( get_the_ID() );
								$converted = strtr( $content, array_flip( get_html_translation_table( HTML_ENTITIES, ENT_QUOTES ) ) );
								$trimmed   = trim( $converted, chr( 0xC2 ) . chr( 0xA0 ) );

								echo wp_kses_post( wp_trim_words( $trimmed, 30, '' ) );
								?>
								<a href="<?php echo esc_url( get_permalink() ); ?>" class="read-more">Read more...</a>
							</p>
						</div>
					</article>
				<?php
				endwhile;
			endif;
			?>
			<div class="post-navigation homefeed">
				<?php if ( $paged > 1 ) { ?>
					<a class="post-navigation-prev" data-page="<?php echo $paged - 1; ?>" style="margin-right: 15px;" href="<?php echo get_site_url(); ?>?paged=<?php echo $paged - 1; ?>">Previous</a>
				<?php } ?>
				<a class="post-navigation-next" data-page="<?php echo $paged + 1; ?>" href="<?php echo get_site_url(); ?>?paged=<?php echo $paged + 1; ?>">Next</a>
			</div>
		</div>
		<?php
		exit();
	}
}