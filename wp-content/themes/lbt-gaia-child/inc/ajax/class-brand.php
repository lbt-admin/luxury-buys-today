<?php
/**
 * Handles Brand's AJAX calls
 *
 * @package LBT
 */

namespace LBT\AJAX;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Ajax Brand Class
 */
class Brand {
	use \LBT\Traits\Singleton;

	/**
	 * Initialize class
	 *
	 * @return void
	 */
	private function initialize() {
		add_action( 'wp_ajax_lbt_toggle_favorite_brand', array( $this, 'toggle_favourite' ) );
	}

	/**
	 * AJAX - Save favourite brand
	 *
	 * Action: lbt_toggle_favorite_brand
	 *
	 * @return void
	 */
	public function toggle_favourite() {
		$nonce_check = check_ajax_referer( 'lbt-toggle-fav-brand-nonce', '__nonce', false );

		// if ( ! $nonce_check ) {
		// 	wp_send_json_error(
		// 		__( 'Security check failed. Refresh page and try again', 'lbt' ),
		// 		403
		// 	);
		// }

		$ontraport_dealership_id = absint( $_POST['dealership_id'] ?? 0 );
		$post_id                 = absint( $_POST['post_id'] ?? 0 );

		if ( ! $ontraport_dealership_id ) {
			wp_send_json_error(
				__( 'Dealership ID is missing', 'lbt' ),
				400
			);
		}

		$user_brands = \LBT\Brand::get_user_brands();

		if ( ! in_array( $ontraport_dealership_id, $user_brands ) ) {
			$response = \LBT\Brand::get_instance()->save( $ontraport_dealership_id, $post_id );
		} else {
			$response = \LBT\Brand::get_instance()->remove( $ontraport_dealership_id );
		}

		if ( is_wp_error( $response ) ) {
			wp_send_json_error(
				$response->get_error_message(),
				$response->get_error_code()
			);
		}

		wp_send_json_success();
	}
}
