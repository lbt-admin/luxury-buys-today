<?php
/**
 * Handles Post Image Check's AJAX calls
 *
 * @package LBT
 */

namespace LBT\AJAX;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Ajax Post Image Check Class
 */
class Post_Image_Check {
	use \LBT\Traits\Singleton;

	/**
	 * Initialize class
	 *
	 * @return void
	 */
	private function initialize() {
		add_action( 'wp_ajax_lbt_post_image_check_manual_run', array( $this, 'run_manual_post_image_check' ) );
		add_action( 'wp_ajax_lbt_post_image_check_restart_script', array( $this, 'run_manual_post_image_check_restart' ) );
	}

	/**
	 * AJAX - Run manual post image check
	 *
	 * Action: lbt_post_image_check_manual_run
	 *
	 * @return void
	 */
	public function run_manual_post_image_check() {
		$nonce_check = check_ajax_referer( 'lbt-ajax-validate-post-image-manual', '__nonce', false );

		// if ( ! $nonce_check ) {
		// 	wp_send_json_error(
		// 		__( 'Security check failed. Refresh page and try again', 'lbt' ),
		// 		403
		// 	);
		// }

		$total_deleted = \LBT\Post_Image_Check::get_instance()->validate_posts();

		wp_send_json_success(
			array(
				'total_deleted' => $total_deleted,
			)
		);
	}

	/**
	 * AJAX - Run manual post image script restart
	 *
	 * Action: lbt_post_image_check_restart_script
	 *
	 * @return void
	 */
	public function run_manual_post_image_check_restart() {
		$nonce_check = check_ajax_referer( 'lbt-ajax-validate-post-image-restart', '__nonce', false );

		// if ( ! $nonce_check ) {
		// 	wp_send_json_error(
		// 		__( 'Security check failed. Refresh page and try again', 'lbt' ),
		// 		403
		// 	);
		// }

		$post_image_check = \LBT\Post_Image_Check::get_instance();

		$post_image_check->clear();
		$post_image_check->run_cron();

		wp_send_json_success();
	}
}
