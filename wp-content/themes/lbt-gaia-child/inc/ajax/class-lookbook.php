<?php
/**
 * Handles Lookbook's AJAX calls
 *
 * @package LBT
 */

namespace LBT\AJAX;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Ajax Lookbook Class
 */
class Lookbook {
	use \LBT\Traits\Singleton;

	/**
	 * Initialize class
	 *
	 * @return void
	 */
	private function initialize() {
		add_action( 'wp_ajax_lbt_lookbook_toggle', array( $this, 'toggle_lookbook_post' ) );
	}

	/**
	 * AJAX - Toggle lookbook post
	 *
	 * Action: lbt_lookbook_toggle
	 *
	 * @return void
	 */
	public function toggle_lookbook_post() {
		$nonce_check = check_ajax_referer( 'lbt-look-book-toggle-nonce', '__nonce', false );

		// if ( ! $nonce_check ) {
		// 	wp_send_json_error(
		// 		__( 'Security check failed. Refresh page and try again', 'lbt' ),
		// 		403
		// 	);
		// }

		$post_id                 = absint( $_POST['post_id'] ?? 0 );
		$ontraport_dealership_id = absint( $_POST['dealership_id'] ?? 0 );

		if ( ! $post_id ) {
			wp_send_json_error(
				__( 'Post ID is missing. Refresh page and try again', 'lbt' ),
				400
			);
		}

		$user_lookbook = \LBT\Lookbook::get_user_lookbook();

		if ( in_array( (int) $post_id, $user_lookbook ) ) { //phpcs:ignore
			\LBT\Lookbook::remove( $post_id );
			$action = 'remove';
		} else {
			$action = 'save';
			\LBT\Lookbook::save( $post_id, $ontraport_dealership_id );
		}

		wp_send_json_success( $action );
	}
}
