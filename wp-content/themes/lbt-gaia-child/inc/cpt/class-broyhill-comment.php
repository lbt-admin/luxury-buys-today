<?php
/**
 * Registers Broyhill Comment CPT
 *
 * @package LBT
 */

namespace LBT\CPT;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Broyhill Comment CPT Class
 */
class Broyhill_Comment {
	use \LBT\Traits\Singleton;

	/**
	 * Initialize class
	 *
	 * @return void
	 */
	private function initialize() {
		add_action( 'init', array( $this, 'register_custom_post_types' ), 1 );
	}

	/**
	 * Register custom post types
	 *
	 * @return  void
	 */
	public function register_custom_post_types() {
		$labels = array(
			'name'               => _x( 'Broyhill Comment', 'post type general name', 'lbt' ),
			'singular_name'      => _x( 'Broyhill Comment', 'post type singular name', 'lbt' ),
			'menu_name'          => _x( 'Broyhill Comments', 'admin menu', 'lbt' ),
			'name_admin_bar'     => _x( 'Broyhill Comments', 'add new on admin bar', 'lbt' ),
			'add_new'            => _x( 'Add New Broyhill Comment', 'add new', 'lbt' ),
			'add_new_item'       => __( 'Add New Broyhill Comment', 'lbt' ),
			'new_item'           => __( 'New Broyhill Comment', 'lbt' ),
			'edit_item'          => __( 'Edit Broyhill Comment', 'lbt' ),
			'view_item'          => __( 'View Broyhill Comment', 'lbt' ),
			'all_items'          => __( 'Broyhill Comments', 'lbt' ),
			'search_items'       => __( 'Search Broyhill Comment', 'lbt' ),
			'parent_item_colon'  => __( 'Parent Broyhill Comment:', 'lbt' ),
			'not_found'          => __( 'No Broyhill Comments found.', 'lbt' ),
			'not_found_in_trash' => __( 'No Broyhill Comments found in Trash.', 'lbt' ),
		);

		$args = array(
			'labels'             => $labels,
			'description'        => __( 'Broyhill Comments', 'lbt' ),
			'public'             => false,
			'publicly_queryable' => true,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'query_var'          => true,
			'has_archive'        => false,
			'hierarchical'       => false,
			'menu_position'      => null,
			'supports'           => array( 'title', 'editor' ),
			'rewrite'            => array(
				'slug'       => 'broyhill-comment',
				'with_front' => false,
			),
			'taxonomies'         => array( 'category' ),
		);

		register_post_type( 'broyhill_comment', $args );
	}
}
