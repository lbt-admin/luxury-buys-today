<?php
/**
 * Users Save Form Class
 *
 * @package LBT
 */

namespace LBT\Forms;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Users
 */
class User_Save_Form extends \LBT\Core\Form {
	/**
	 * Class constructor
	 *
	 * @param   array $fields  The company fields.
	 *
	 * @return  void
	 */
	public function __construct( &$fields = array() ) {
		$this->fields = $fields;

		$this->rules = array(
			'first_name'       => 'required',
			'last_name'        => 'required',
			'email'            => 'required|email',
			'phone'            => 'required|number',
			'zip_code'         => 'required',
			'password'         => 'required_if:!user_id|min:6',
			'confirm_password' => 'same_as:password',
		);
	}
}
