<?php

/**
 * Class Shuffle_Broyhill_Import_Feed
 *
 * Import or update posts from feed: https://luxurypropertiesnews.tumblr.com/api/read/
 *
 * IMPORTANT: If we can not parse the post title from the photo caption, the content will not be updated or the
 * new post will not be created.
 */
class Shuffle_Broyhill_Import_Feed {

	public function import_auto_news() {
		$this->import( 'https://luxuryautomotivenews.tumblr.com/api/read/' );
	}

	public function import_property_news() {
		$this->import( 'https://luxurypropertiesnews.tumblr.com/api/read/' );
	}

	public function import_fashion_news() {
		$this->import( 'https://luxuryfashionnews.tumblr.com/api/read/' );
	}

	public function import_home_decor_news() {
		$this->import( 'https://luxuryhomedecornews.tumblr.com/api/read/' );
	}

	/**
	 * Import or update posts from feed.
	 */
	private function import( $feed_url ) {
		$data = wp_remote_get( $feed_url );

		if ( is_wp_error( $data ) ) {
			$this->log( 'Error fetching rss feed data to import!' );
			return;
		}

		if ( 200 !== wp_remote_retrieve_response_code( $data ) ) {
			$this->log( 'Error fetching rss feed data to import! Response code not 200!' );
			return;
		}

		$body = wp_remote_retrieve_body( $data );
		if ( empty( $body ) ) {
			return;
		}

		$xml_object = simplexml_load_string( $body );
		if ( false === $xml_object ) {
			$this->log( 'Error decoding XML from fetched rss feed for the import!' );
			return;
		}
		$json  = json_encode( $xml_object );
		$array = json_decode( $json, true );

		if ( ! is_array( $array ) || empty( $array ) || ! isset( $array['posts'] ) || ! isset( $array['posts']['post'] ) ) {
			$this->log( 'Error parsing array from fetched rss feed for the import!' );
			return;
		}

		// Collect data we need only.
		$rss_posts = array();
		foreach ( $array['posts']['post'] as $post ) {
			$rss_posts[ $post['@attributes']['url'] ] = array(
				'id'         => $post['@attributes']['id'],
				'source_url' => $post['@attributes']['url'],
				'blog_url'   => $post['tumblelog']['@attributes']['url'],
				'content'    => $post['photo-caption'],
				'src'        => $post['photo-url'],
			);
		}

		$meta_lookup = array_map(
			function ( $post ) {
				return $post['source_url'];
			},
			$rss_posts
		);

		// Get all posts by source url from the thumblr blog url.
		$args  = array(
			'post_type'              => 'post',
			'cache_results'          => true,
			'update_post_meta_cache' => true,
			'update_post_term_cache' => false,
			'meta_query'             => array(
				array(
					'key'     => 'wpr_article_source_url',
					'value'   => array_values( $meta_lookup ),
					'compare' => 'IN',
				),
			),
			'nopaging'               => true,
			'posts_per_page'         => -1,
		);
		$query = new WP_Query( $args );

		$passed_rss_posts = array();
		if ( $query->have_posts() ) {
			while ( $query->have_posts() ) {
				$query->the_post();
				$post_id         = get_the_ID();
				$meta_source_url = get_post_meta( $post_id, 'wpr_article_source_url', true );

				// Compare thumblr post url as this will never change.
				if ( ! isset( $rss_posts[ $meta_source_url ] ) ) {
					continue;
				}

				// Save as passed and imported as we can have multiple posts with the same meta key already in the database as old data.
				$passed_rss_posts[] = $meta_source_url;
				$this->update_post( $post_id, $rss_posts[ $meta_source_url ] );
			}
			wp_reset_postdata();
		}

		// Check if we have any new posts to import.
		$new_posts = array_diff( array_keys( $rss_posts ), $passed_rss_posts );
		if ( empty( $new_posts ) ) {
			return;
		}

		// Create new posts that we don't have in database.
		foreach ( $new_posts as $new_post_source_url ) {
			$this->update_post( null, $rss_posts[ $new_post_source_url ] );
		}
	}

	/**
	 * Update post data from rss data
	 *
	 * @param int $post_id
	 * @param array $current_rss_post
	 */
	private function update_post( $post_id, $current_rss_post ) {
		// Fill the content.
		$post_data    = $this->parse_content( $current_rss_post['content'], $current_rss_post );
		$remote_image = $current_rss_post['src'][0];

		if ( $post_id ) {
			// If not empty post title that means we could parse the content, so do update it or leave it as is.
			if ( ! empty( $post_data['title'] ) ) {
				wp_update_post(
					array(
						'ID'           => $post_id,
						'post_content' => $post_data['content'],
					)
				);
			}
		} else {

			// Bail if empty title as no pattern can match it. There are too many variations and also the ones without the title.
			// We must check this only for the new posts as the old ones has the title.
			if ( empty( $post_data['title'] ) ) {
				$this->log( 'Error fetching rss feed data to import! Could not parse title for the post: ' . $current_rss_post['source_url'] );
				return;
			}

			$category_id = $this->get_post_category( $current_rss_post['blog_url'] );

			$args = array(
				'post_title'   => $post_data['title'],
				'post_content' => $post_data['content'],
				'post_status'  => 'publish',
			);

			if ( $category_id ) {
				$args['post_category'] = array( $category_id );
			}

			$post_id = wp_insert_post( $args );
		}

		// Anyway update the images as we always have data for this.

		if ( ! wp_doing_cron() ) {
			echo 'Imported data for post id: ' . $post_id . ' for blog: ' . $current_rss_post['source_url'] . '<br />';
		}

		// Check if the old remote image is the same.
		if ( $remote_image !== get_post_meta( $post_id, 'wpr_remote_featured_img', true ) ) {
			// Change post meta
			update_post_meta( $post_id, 'wpr_remote_featured_img', $remote_image );

			// Delete old image.
			$old_image_id = get_post_thumbnail_id();
			if ( $old_image_id ) {
				wp_delete_attachment( $old_image_id );
			}

			// Import new image.
			require_once ABSPATH . 'wp-admin/includes/media.php';
			require_once ABSPATH . 'wp-admin/includes/file.php';
			require_once ABSPATH . 'wp-admin/includes/image.php';

			// It can happen that we can not parse the title, so use the post title.
			$image_title = ! empty( $post_data['title'] ) ? $post_data['title'] : get_the_title( $post_id );

			$new_image_id = media_sideload_image( $remote_image, $post_id, $image_title, 'id' );
			set_post_thumbnail( $post_id, $new_image_id );

			$new_image_metadata = wp_get_attachment_metadata( $new_image_id );
			update_post_meta(
				$post_id,
				'wpr_remote_image_atts',
				array(
					'height' => $new_image_metadata['height'],
					'width'  => $new_image_metadata['width'],
				)
			);
			update_post_meta( $post_id, 'wpr_social_image', wp_get_attachment_image_url( $new_image_id, 'full' ) );
		}

		// Additional required meta.
		update_post_meta( $post_id, 'wpr_article_source_url', $current_rss_post['source_url'] );
		update_post_meta( $post_id, 'wpr_article_source', $current_rss_post['blog_url'] );
	}

	/**
	 * Return post category based on thumblr blog url
	 *
	 * @param string $blog_url
	 *
	 * @return int|string
	 */
	private function get_post_category( $blog_url ) {
		$cat_id = '';

		switch ( true ) {
			case false !== strpos( $blog_url, 'luxuryautomotivenews' ):
				$cat_id = 36;
				break;

			case false !== strpos( $blog_url, 'luxuryfashionnews' ):
				$cat_id = 37;
				break;

			case false !== strpos( $blog_url, 'luxuryhomedecornews' ):
				$cat_id = 38;
				break;

			case false !== strpos( $blog_url, 'luxurypropertiesnews' ):
				$cat_id = 39;
				break;
		}

		return $cat_id;
	}


	/**
	 * Parse title and the content from photo credits
	 *
	 * @param string $content
	 * @param array  $current_rss_post
	 *
	 * @return array
	 */
	private function parse_content( $content, $current_rss_post ) {
		$content = preg_replace( '/\s+/', ' ', trim( str_replace( array( '<p>', '</p>', '<i>', '</i>' ), '', $content ) ) );

		preg_match(
			'/^(.*)(?:COMMENTS|COMMENT)(.*):(.*)/',
			$content,
			$matches,
			PREG_UNMATCHED_AS_NULL
		);

		// Trim all white space including &nbsp
		$title = trim( str_replace( ':', '', wp_strip_all_tags( $matches[2] ) ), " \t\n\r\0\x0B\xC2\xA0" );

		$post_content = sprintf(
			'<br><img src="%1$s" alt="%3$s"/>
			<br>
			<b>%2$s</b>
			<b>%3$s</b>
			<i>%4$s</i>
			<br>Source Link: <a href="%5$s" target="_blank">%6$s</a>',
			sanitize_text_field( $current_rss_post['src'][1] ), // Get width image 500.
			trim( wp_strip_all_tags( $matches[1] ) ) . ' COMMENTS', // BROYHILL COMMENTS.
			$title . ':', // Title.
			trim( wp_strip_all_tags( trim( $matches[3], ':' ) ), " \t\n\r\0\x0B\xC2\xA0" ), // Content.
			sanitize_text_field( $current_rss_post['source_url'] ), // Src.
			sanitize_text_field( str_replace( array( 'https:', '/' ), '', $current_rss_post['blog_url'] ) ) // Thumblr post url.
		);

		return array(
			'title'   => $title,
			'content' => $post_content,
		);
	}

	/**
	 * Log messages
	 * @param string $message
	 */
	private function log($message) {
		if ( ! defined( 'WP_DEBUG_LOG' ) || ! WP_DEBUG_LOG ) {
			return;
		}

		error_log( $message );
	}
}
