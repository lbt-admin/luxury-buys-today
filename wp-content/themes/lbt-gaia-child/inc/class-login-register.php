<?php
/**
 * Adds Login/Register modal and handles the process
 *
 * @package LBT
 */

namespace LBT;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Login Class
 */
class Login_Register {
	use \LBT\Traits\Singleton;

	/**
	 * Initialize class
	 *
	 * @return void
	 */
	private function initialize() {
		add_action( 'init', array( $this, 'activate_account' ) );
		add_action( 'wp_footer', array( $this, 'login_form' ) );
		add_action( 'wp_enqueue_scripts', array( $this, 'load_assets' ) );
		add_action( 'wp_ajax_nopriv_lbt_login', array( $this, 'login' ) );
		add_action( 'wp_ajax_nopriv_lbt_register', array( $this, 'register' ) );
		add_action( 'wp_ajax_nopriv_lbt_resend_account_activation_email', array( $this, 'resend_account_activation_email' ) );
		add_action( 'oa_social_login_action_after_user_insert', array( $this, 'social_register_create_ontraport_contact_and_tag' ), 10, 2 );
	}

	/**
	 * Load assets
	 *
	 * @return void
	 */
	public function load_assets() {
		if ( is_user_logged_in() ) {
			return;
		}

		wp_enqueue_style(
			'lbt-login-form',
			LBT_THEME_URL . 'assets/css/login-form.css',
			array(),
			LBT_THEME_VERSION
		);

		wp_enqueue_script(
			'lbt-login-form',
			LBT_THEME_URL . 'assets/js/login-form.js',
			array( 'jquery', 'password-strength-meter' ),
			LBT_THEME_VERSION,
			true
		);

		wp_localize_script(
			'lbt-login-form',
			'lbtLoginForm',
			array(
				'ajaxUrl'          => esc_url( admin_url( 'admin-ajax.php' ) ),
				'isSignUpTemplate' => is_page_template( 'template-signup.php' ),
			)
		);
	}

	/**
	 * Load login form in the footer
	 *
	 * @return void
	 */
	public function login_form() {
		get_template_part( 'partials/modal', 'login' );
		if ( ! is_page_template( 'template-signup.php' ) ) {
			get_template_part( 'partials/modal', 'register' );
		}
	}

	/**
	 * AJAX - Login user
	 *
	 * Action: lbt_login
	 *
	 * @return void
	 */
	public function login() {
		$nonce_check = check_ajax_referer( 'lbt-login-form-nonce', '__nonce', false );

		// if ( ! $nonce_check ) {
		// 	wp_send_json_error(
		// 		__( 'Security check failed. Refresh page and try again', 'lbt' ),
		// 		403
		// 	);
		// }

		$args = $_POST;

		$redirect = $args['redirect'] ?? home_url( '/' );

		unset( $args['__nonce'] );
		unset( $args['action'] );
		unset( $args['redirect'] );

		$redirect = sanitize_text_field( $redirect );

		if ( empty( $args['lbt_username'] ) ) {
			wp_send_json_error(
				__( 'Email is required', 'lbt' ),
				400
			);
		}

		if ( empty( $args['lbt_password'] ) ) {
			wp_send_json_error(
				__( 'Email is password', 'lbt' ),
				400
			);
		}

		$username = sanitize_text_field( $args['lbt_username'] );
		$password = sanitize_text_field( $args['lbt_password'] );

		if ( is_email( $username ) ) {
			$user = get_user_by( 'email', $username );
		} else {
			$user = get_user_by( 'login', $username );
		}

		if ( ! $user ) {
			wp_send_json_error(
				__( 'User with given email does not exists', 'lbt' ),
				404
			);
		}

		$is_active = User::get_instance()->is_active( $user->ID );

		if ( ! $is_active ) {
			$info = array(
				'email'   => $user->user_email,
				'action'  => 'lbt_resend_account_activation_email',
				'__nonce' => wp_create_nonce( 'ajax-lbt-account-activation-email' . $user->user_email ),
			);

			$message = sprintf(
				'%s<br> <a href="javascript:void(0)" data-info="%s" data-loading-text="%s", class="lbt-account-reactivation-email">%s</a>',
				__( 'Your account is not activated. Check your email for verification message.', 'lbt' ),
				esc_attr( wp_json_encode( $info ) ),
				__( 'Sending...', 'lbt' ),
				__( 'Resend account verification email', 'lbt' )
			);

			wp_send_json_error(
				$message,
				403
			);
		}

		$auth = wp_signon(
			array(
				'user_login'    => $username,
				'user_password' => $password,
				'remember'      => true,
			),
			is_ssl()
		);

		if ( is_wp_error( $auth ) ) {
			wp_send_json_error(
				$auth->get_error_message(),
				400
			);
		}

		global $wp;

		wp_send_json_success(
			array(
				'id'       => $auth->ID,
				'message'  => __( 'Login successful, redirecting...', 'lbt' ),
				'redirect' => $redirect,
			)
		);
	}

	/**
	 * AJAX - Register user
	 *
	 * Action: lbt_register
	 *
	 * @return void
	 */
	public function register() {
		$nonce_check = check_ajax_referer( 'lbt-register-form-nonce', '__nonce', false );

		// if ( ! $nonce_check ) {
		// 	wp_send_json_error(
		// 		__( 'Security check failed. Refresh page and try again', 'lbt' ),
		// 		403
		// 	);
		// }

		$args = $_POST;

		$modal_location = $args['modal_location'] ?? false;
		$brand          = sanitize_text_field( $args['brand'] ?? '' );

		unset( $args['__nonce'] );
		unset( $args['action'] );
		unset( $args['modal_location'] );
		unset( $args['brand'] );

		$user_id = User::get_instance()->save( $args );

		if ( is_wp_error( $user_id ) ) {
			wp_send_json_error(
				$user_id->get_error_message(),
				400
			);
		}

		$redirect = home_url( '/' );

		if ( ! empty( $brand ) ) {
			$category      = get_category_by_slug( $brand );
			$category_link = get_category_link( $category );

			if ( ! empty( $category_link ) ) {
				$redirect = str_replace( '/category', '', $category_link );
			}
		}

		wp_send_json_success(
			array(
				'message'        => __( 'Registration successfull. Check your email for account verification. Redirecting...', 'lbt' ),
				'redirect'       => esc_url( $redirect ),
				'modal_location' => $modal_location,
			)
		);
	}

	/**
	 * Activate user account
	 *
	 * @return  void
	 */
	public function activate_account() {
		$error = false;

		if ( empty( $_GET['act_code'] ) ) { // phpcs:ignore
			return;
		}

		$code = sanitize_text_field( wp_unslash( $_GET['act_code'] ) ); // phpcs:ignore

		$users = get_users(
			array(
				'meta_query' => array( // phpcs:ignore
					array(
						'key'   => 'user_verification_code',
						'value' => $code,
					),
				),
			)
		);

		if ( ! $users || empty( $users ) ) {
			$message = __( 'Invalid activation code', 'lbt' );
			wp_safe_redirect( '/?act_error=' . $message );
			exit;
		}

		$user = $users[0];

		if ( User::get_instance()->is_active( $user->ID ) ) {
			$message = __( 'Your account is already active', 'lbt' );
			wp_safe_redirect( '/?act_error=' . $message );
			exit;
		}

		update_user_meta( $user->ID, 'user_status', 'active' );
		delete_user_meta( $user->ID, 'user_verification_code' );

		Ontraport::get_instance()->save_contact( $user );

		$message = __( 'Your account is activated successfully. You can now login', 'lbt' );
		wp_safe_redirect( '/?act_success=' . $message );
		exit;
	}

	/**
	 * AJAX - Resend email for account activation
	 *
	 * Action: lbt_resend_account_activation_email
	 *
	 * @return  void
	 */
	public function resend_account_activation_email() {
		$nonce_check = check_ajax_referer( 'ajax-lbt-account-activation-email' . ( sanitize_text_field( wp_unslash( $_POST['email'] ?? '' ) ) ), '__nonce', false );

		if ( ! $nonce_check ) {
			wp_send_json_error(
				__( 'Security check failed', 'lbt' ),
				403
			);
		}

		$args = $_POST;

		unset( $args['__nonce'] );
		unset( $args['action'] );

		$user_id = User::get_instance()->resend_account_activation_email( $args['email'] );

		if ( is_wp_error( $user_id ) ) {
			wp_send_json_error(
				$user_id->get_error_message(),
				$user_id->get_error_code()
			);
		}

		wp_send_json_success(
			array(
				'message' => __( 'Email sent', 'lbt' ),
			)
		);
	}

	/**
	 * Create ontraport contact and tag after social network login on subscription
	 *
	 * @param  object $user_data The user data object.
	 * @param  object $identity Social user data.
	 *
	 * @return  void
	 */
	public function social_register_create_ontraport_contact_and_tag( $user_data, $identity ) {
		$response = Ontraport::get_instance()->save_contact( $user_data );

		if ( ! is_wp_error( $response ) ) {
			$contact_id = $response['id'];

			$tag_data = array(
				'objectID' => 0,
				'add_list' => array( 642, 536 ), // GeoGate and Newsletter tag id.
				'ids'      => array( $contact_id ),
			);

			Ontraport::get_instance()->add_tag( $tag_data );
		}
	}
}
