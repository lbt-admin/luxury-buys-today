<?php
/**
 * Form Class
 *
 * @package LBT
 */

namespace LBT\Core;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Form class
 */
abstract class Form {
	/**
	 * Form data
	 *
	 * @var array
	 */
	protected $fields = array();

	/**
	 * Field rules
	 *
	 * @var array
	 */
	protected $rules = array();

	/**
	 * Validate company fields
	 *
	 * @throws \Exception If there are invalid fields.
	 *
	 * @return  void
	 */
	public function validate_and_sanitize_data() {
		if ( empty( $this->fields ) ) {
			throw new \Exception(
				__( 'Empty data sent', 'lbt' ),
				400
			);
		}


		if ( empty( $this->rules ) ) {
			return;
		}

		foreach ( $this->fields as $field_name => &$value ) {
			if ( ! is_array( $value ) && 0 === strpos( $value, '[' ) ) {
				$value = json_decode( $value, true );
			}

			if ( isset( $this->rules[ $field_name ] ) ) {
				$rules = $this->rules[ $field_name ];

				if ( strpos( $rules, '|' ) ) {
					$rules = explode( '|', $rules );
				} else {
					$rules = array( $rules );
				}

				$errors = array();

				foreach ( $rules as $rule ) {
					switch ( $rule ) {
						case 'required':
							if ( 0 === $value || '0' === $value ) {
								break;
							}

							if ( empty( $value ) ) {
								// translators: %s: Field Name.
								$errors[] = sprintf( __( 'Field %s is required', 'lbt' ), $field_name );
							}
							break;
						case 'number':
							if ( ! is_numeric( $value ) ) {
								// translators: %s: Field Name.
								$errors[] = sprintf( __( 'Only numbers can be entered for %s', 'lbt' ), $field_name );
							}
							break;
						case 'email':
							if ( empty( $value ) ) {
								break;
							}

							if ( ! is_email( $value ) ) {
								// translators: %s: Field Name.
								$errors[] = sprintf( __( 'Invalid email address for %s', 'lbt' ), $field_name );
							}
							break;
						case 'phone':
							if ( empty( $value ) ) {
								break;
							}

							if ( ! preg_match( '/[0-9]/', $value ) ) {
								// translators: %s: Field Name.
								$errors[] = sprintf( __( 'Invalid phone number %s', 'lbt' ), $field_name );
							}
							break;
						case 'url':
							if ( empty( $value ) ) {
								break;
							}

							if ( ! filter_var( $value, FILTER_VALIDATE_URL ) ) {
								// translators: %s: Field Name.
								$errors[] = sprintf( __( 'Invalid web address %s', 'lbt' ), $field_name );
							}
							break;
						case 0 === strpos( $rule, 'term:' ):
							if ( empty( $value ) ) {
								break;
							}

							$term_type = explode( ':', $rule );

							if ( empty( $term_type[1] ) ) {
								// translators: %s: Field Name.
								$errors[] = sprintf( __( 'Invalid rule for %s', 'lbt' ), $field_name );
							}
							if ( is_array( $value ) ) {
								foreach ( $value as $term_id ) {
									if ( is_numeric( $term_id ) ) {
										$term_field = 'id';
									} elseif ( is_string( $term_id ) ) {
										$term_field = 'slug';
									}

									$term = get_term_by( $term_field, $term_id, $term_type[1] );
									if ( ! $term ) {
										// translators: %1$d : Term id - %2$s : Field Name.
										$errors[] = sprintf( __( '%3$s with id %1$d does not exists for %2$s', 'lbt' ), $term_id, $field_name, ucfirst( $term_type[1] ) );
									}
								}
							} else {
								if ( is_numeric( $value ) ) {
									$term_field = 'id';
								} elseif ( is_string( $value ) ) {
									$term_field = 'slug';
								}

								$term = get_term_by( $term_field, $value, $term_type[1] );
								if ( ! $term ) {
									// translators: %1$d : Term id - %2$s : Field Name.
									$errors[] = sprintf( __( '%3$s with id %1$d does not exists for %2$s', 'lbt' ), $value, $field_name, ucfirst( $term_type[1] ) );
								}
							}
							break;
						case 0 === strpos( $rule, 'in:' ):
							if ( empty( $value ) ) {
								break;
							}

							$rule_parts   = explode( ':', $rule );
							$valid_values = explode( ',', $rule_parts[1] );

							if ( empty( $valid_values ) ) {
								// translators: %s: Field Name.
								$errors[] = sprintf( __( 'Invalid IN rule for %s', 'lbt' ), $field_name );
							}

							if ( ! in_array( $value, $valid_values, true ) ) {
								// translators: %1$s: Field Name. %2$s: Allowed values.
								$errors[] = sprintf( __( 'Invalid set of values for %1$s. Allowed are only %2$s', 'lbt' ), $field_name, $rule_parts[1] );
							}
							break;
						case 0 === strpos( $rule, 'min:' ):
							if ( empty( $value ) ) {
								break;
							}

							$rule_parts = explode( ':', $rule );
							$min_value  = absint( $rule_parts[1] );

							if ( ! $min_value ) {
								break;
							}

							if ( $min_value > strlen( $value ) ) {
								// translators: %1$s: Field Name - %2$d: Minumum length.
								$errors[] = sprintf( __( 'Minimum number of characters for %1$s is %2$d', 'lbt' ), $field_name, $min_value );
							}

							break;
						case 0 === strpos( $rule, 'range:' ):
							if ( empty( $value ) ) {
								break;
							}

							$rule_parts   = explode( ':', $rule );
							$range_values = $rule_parts[1];

							$range_values = explode( '..', $range_values );

							if ( (float) $range_values[0] > (float) $value || (float) $value > (float) $range_values[1] ) {
								// translators: %1$s: Field Name - %2$f: Min value - %3$f: Max value.
								$errors[] = sprintf( __( 'The value for %1$s must be between %2$f and %3$f', 'lbt' ), $field_name, $range_values[0], $range_values[1] );
							}

							break;
						case 0 === strpos( $rule, 'user=' ):
							if ( empty( $value ) ) {
								break;
							}

							$rule_parts        = explode( '=', $rule );
							$validation_object = json_decode( $rule_parts[1] );

							if ( 'meta' === $validation_object->field_type ) {
								$users = get_users(
									array(
										'field'      => 'ID',
										'meta_query' => array(
											array(
												'key'   => $validation_object->field_name,
												'value' => $value,
											),
										),
									)
								);

								if ( empty( $users ) ) {
									// translators: %s: Field Name.
									$errors[] = sprintf( __( 'The user is not found %s', 'lbt' ), $field_name );
								}
							}

							break;
						case 0 === strpos( $rule, 'post=' ):
							if ( empty( $value ) ) {
								break;
							}

							$rule_parts        = explode( '=', $rule );
							$validation_object = json_decode( $rule_parts[1] );

							if ( 'meta' === $validation_object->field_type ) {
								$c_posts = get_posts(
									array(
										'post_type'  => $validation_object->type,
										'fields'     => 'ids',
										'meta_query' => array(
											array(
												'key'   => $validation_object->field_name,
												'value' => $value,
											),
										),
									)
								);

								if ( empty( $c_posts ) ) {
									// translators: %1$s: Post type name - %2$s: Field Name.
									$errors[] = sprintf( __( '%1$s is not found for %2$s', 'lbt' ), ucfirst( $validation_object->type ), $field_name );
								}
							}

							break;
						case 0 === strpos( $rule, 'comment=' ):
							if ( empty( $value ) ) {
								break;
							}

							$rule_parts        = explode( '=', $rule );
							$validation_object = json_decode( $rule_parts[1] );

							if ( 'meta' === $validation_object->field_type ) {
								$c_posts = get_comments(
									array(
										'fields'     => 'ids',
										'meta_query' => array(
											array(
												'key'   => $validation_object->field_name,
												'value' => $value,
											),
										),
									)
								);

								if ( empty( $c_posts ) ) {
									// translators: %1$s: Post type name - %2$s: Field Name.
									$errors[] = sprintf( __( '%1$s is not for for %2$s', 'lbt' ), ucfirst( $validation_object->type ), $field_name );
								}
							}

							break;
						case 0 === strpos( $rule, 'same_as:' ):
							if ( empty( $value ) ) {
								break;
							}

							$rule_parts         = explode( ':', $rule );
							$same_as_field_name = $rule_parts[1];

							if ( empty( $this->fields[ $same_as_field_name ] ) ) {
								break;
							}

							if ( strval( $value ) !== strval( $this->fields[ $same_as_field_name ] ) ) {
								if ( false !== strpos( $same_as_field_name, 'password' ) ) {
									$errors[] = __( 'Passwords do not match', 'lbt' );
								} else {
									// translators: %1$s: Field name - %2$s: Field two name.
									$errors[] = sprintf( __( 'Field %1$s must be same with %2$s', 'lbt' ), $field_name, $same_as_field_name );
								}
							}

							break;
						case 0 === strpos( $rule, 'required_if:' ):
							if ( 0 === $value || '0' === $value ) {
								break;
							}

							$rule_parts = explode( ':', $rule );
							$field      = $rule_parts[1];

							$should_field_exists = false === strpos( '!', $field );
							$field_name          = str_replace( '!', '', $field );
							
							if ( $should_field_exists && ! isset( $this->fields[ $field_name ] ) ) {
								if ( empty( $value ) ) {
									// translators: %s: Field Name.
									$errors[] = sprintf( __( 'Field %s is required', 'lbt' ), $field_name );
								}
							}

							break;
					}
				}
			}

			if ( ! empty( $errors ) ) {
				$errors = implode( '<br>', $errors );

				throw new \Exception( $errors, 400 );
			}

			if ( is_numeric( $value ) ) {
				$value = (int) $value;
			} elseif ( is_email( $value ) ) {
				$value = sanitize_email( $value );
			} elseif ( ! is_array( $value ) ) {
				$value = sanitize_text_field( $value );
			}
		}

		return $this->fields;
	}
}
