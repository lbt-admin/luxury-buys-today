<?php
/**
 * Menu Class
 *
 * @package LBT
 */

namespace LBT\Core;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Menu
 */
class Menu {
	use \LBT\Traits\Singleton;

	/**
	 * Initialize class
	 *
	 * @return  void
	 */
	private function initialize() {
		add_action( 'wp_update_nav_menu_item', array( $this, 'save_custom_menu_fields' ), 10, 2 );
		add_filter( 'wp_nav_menu_objects', array( $this, 'change_last_menu_link' ), 10, 2 );
		add_filter( 'walker_nav_menu_start_el', array( $this, 'menus_updates' ), 10, 4 );
		add_filter( 'wp_nav_menu_items', array( $this, 'about_menus_updates' ), 10, 2 );

		add_filter(
			'wp_edit_nav_menu_walker',
			function( $class, $menu_id ) {
				$theme_locations = get_nav_menu_locations();

				$menu = get_term( $theme_locations['primary'], 'nav_menu' );

				if ( $menu && $menu_id === $menu->term_id ) {
					add_action( 'wp_nav_menu_item_custom_fields', array( $this, 'add_menu_fields' ), 10, 5 );
				}

				return $class;
			},
			10,
			2
		);

	}

	/**
	 * Add images for submenus
	 *
	 * @param   string $items  Menu items.
	 * @param   object $args   Menu arguments.
	 *
	 * @return  string
	 */
	public function menus_updates( $item_output, $menu_item, $depth, $args  ) {
		if ( 'primary' !== $args->theme_location ) {
			return $item_output;
		}

		if ( $depth == 1 ) {
			if ( in_array( 'no-image', $menu_item->classes, true ) ) {
				return '<a href="' . esc_url( $menu_item->url ) . '"><span>' . esc_html( $menu_item->title ) . '</span></a>';
			} else {
				$image_url = get_post_meta( $menu_item->ID, '_lbt_menu_item_image_url', true );
				return'<a href="' . esc_url( $menu_item->url ) . '">
			<img
			src="' . esc_url( $image_url ) . '"
			alt="' . esc_attr( $menu_item->title ) . '"><span>' . esc_html( $menu_item->title ) . '</span></a>';
			}
		} elseif ( 0 === $depth ) {
			return '<a href="' . esc_url( $menu_item->url ) . '"><span>' . esc_html( $menu_item->title ) . '</span></a>';
		}

		return $item_output;
	}

	/**
	 * Change last menu item to login if user not logged in
	 *
	 * @param  array $sorted_menu_objects List of menu items.
	 * @param  array $args                Menu arguments.
	 *
	 * @return array
	 */
	public function change_last_menu_link( $sorted_menu_objects, $args ) {
		if ( 'primary' !== $args->theme_location ) {
			return $sorted_menu_objects;
		}

		$total_menu_items = count( $sorted_menu_objects );

		if ( is_user_logged_in() ) {
			global $wp;
			$sorted_menu_objects[ $total_menu_items ]->url = wp_logout_url() . '&redirect=' . esc_url( home_url( $wp->request ) );
			return $sorted_menu_objects;
		}

		$last_item = $sorted_menu_objects[ $total_menu_items ];
		$last_item_parent = (int) $last_item->menu_item_parent;

		foreach ( $sorted_menu_objects as $key => $menu_obj ) {
			if ( $last_item_parent === (int) $menu_obj->menu_item_parent || $last_item_parent === (int) $menu_obj->db_id ) {
				unset( $sorted_menu_objects[ $key ] );
			}
		}

		$sorted_menu_objects[] = (object) array(
			'url'     => '#',
			'title'   => __( 'Login', 'lbt' ),
			'classes' => array( 'lbt-show-login-form' ),
		);

		return $sorted_menu_objects;
	}

	/**
	 * Add custom menu fields for submenu items only
	 *
	 * @param  int     $item_id The item id.
	 * @param  object  $item    The menu item object.
	 * @param  integer $dept    The depth of the menu item.
	 * @param  object  $args    The menu item arguments object.
	 * @param  int     $id      The navigation menu id.
	 *
	 * @return void
	 */
	public function add_menu_fields( $item_id, $item, $dept, $args, $id ) {
		if ( 0 === (int) $item->menu_item_parent ) {
			return;
		}

		wp_nonce_field( 'lbt-menu-items', '_lbt_menu_item_nonce' );
		$image_url = get_post_meta( $item_id, '_lbt_menu_item_image_url', true );
		?>
		<div class="field-icon-class description-wide" style="margin: 5px 0;">
			<input type="hidden" class="nav-menu-id" value="<?php echo esc_attr( $item_id ); ?>" />

			<div class="icon-class-holder">
				<label for="lbt-icon-class<?php echo esc_attr( $item_id ); ?>">
					<?php esc_html_e( 'Image Url', 'lbt' ); ?>
				</label>
				<br/>
				<input type="text" name="_lbt_menu_item_image_url[<?php echo esc_attr( $item_id ); ?>]" id="lbt-icon-class-<?php echo esc_attr( $item_id ); ?>" value="<?php echo esc_attr( $image_url ); ?>" />
			</div>
		</div>
		<?php
	}

	/**
	 * Save the menu item meta
	 *
	 * @param int $menu_id The Menu ID.
	 * @param int $menu_item_db_id The Menu item db id.
	 *
	 * @return void|int
	 */
	public function save_custom_menu_fields( $menu_id, $menu_item_db_id ) {
		if ( ! isset( $_POST['_lbt_menu_item_nonce'] ) || ! wp_verify_nonce( sanitize_text_field( wp_unslash( $_POST['_lbt_menu_item_nonce'] ) ), 'lbt-menu-items' ) ) {
			return $menu_id;
		}

		if ( isset( $_POST['_lbt_menu_item_image_url'][ $menu_item_db_id ] ) ) {
			$sanitized_data = sanitize_text_field( wp_unslash( $_POST['_lbt_menu_item_image_url'][ $menu_item_db_id ] ) );
			update_post_meta( $menu_item_db_id, '_lbt_menu_item_image_url', $sanitized_data );
		} else {
			delete_post_meta( $menu_item_db_id, '_lbt_menu_item_image_url' );
		}
	}

	/**
	 * Update the About menu in the footer with Sign in / Sign out links
	 *
	 * @param   string $items  Menu items.
	 * @param   object $args   Menu arguments.
	 *
	 * @return  string
	 */
	public function about_menus_updates( $items, $args ) {
		if ( 51 !== $args->menu->term_id ) {
			return $items;
		}

		if ( is_user_logged_in() ) {
			global $wp;
			$items .= '<li class="log-out">
				<a href="' . esc_url( wp_logout_url() . '&redirect=' . esc_url( home_url( $wp->request ) ) ) . '"><span>' . __( 'Sign out', 'ppp' ) . '</span></a>
			</li>';
		} else {
			$items .= '<li class="lbt-show-login-form">
				<a href="#"><span>' . __( 'Sign in', 'ppp' ) . '</span></a>
			</li>';
		}

		return $items;
	}
}
