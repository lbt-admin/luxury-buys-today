<?php
/**
 * Cron handling
 *
 * @package CurrencyComparisonTool
 */

namespace LBT\Core;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Cron
 */
class Cron {
	use \LBT\Traits\Singleton;

	/**
	 * Initialize class
	 *
	 * @return  void
	 */
	private function initialize() {
		add_action( 'wpr_ontarport_dealerships_sync', array( \LBT\Dealer::get_instance(), 'sync_dealerships' ) );
		add_action( 'wpr_set_offer_ratings', array( \LBT\Post::get_instance(), 'set_offer_rating' ) );

		if ( isset( $_GET['lbtsyncdealerships'] ) ) {
			try {
				\LBT\Dealer::get_instance()->sync_dealerships();
			} catch ( \Exception $e ) {
				echo esc_html( $e->getMessage() );
			}
		}

		$this->run_cron();
	}

	/**
	 * Run cron
	 *
	 * @return  void
	 */
	public function run_cron() {
		wp_clear_scheduled_hook( 'wpr_ontarport_dealerships_sync' );
		// $next_run = wp_next_scheduled( 'wpr_ontarport_dealerships_sync' );

		// if ( ! $next_run ) {
		// 	wp_schedule_event( time(), 'hourly', 'wpr_ontarport_dealerships_sync' );
		// }

		$next_run = wp_next_scheduled( 'wpr_set_offer_ratings' );

		if ( ! $next_run ) {
			wp_schedule_event( time(), 'daily', 'wpr_set_offer_ratings' );
		}
	}

	/**
	 * Clear cron schedules
	 *
	 * @return  void
	 */
	public function clear() {
		wp_clear_scheduled_hook( 'wpr_ontarport_dealerships_sync' );
		wp_clear_scheduled_hook( 'wpr_set_offer_ratings' );
	}
}
