<?php
/**
 * Database handling
 *
 * @package CurrencyComparisonTool
 */

namespace LBT\Core;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Database
 */
class Database {
	use \LBT\Traits\Singleton;

	/**
	 * Create database tables
	 *
	 * @return  void
	 */
	public function create_tables() {
		$current_db_version = 2;

		$db_version = get_option( 'lbt_db_version', 0 );

		if ( $current_db_version === (int) $db_version ) {
			return;
		}

		global $wpdb, $charset_collate;

		$wp_track_table = $wpdb->prefix . 'wpr_ontraport_log';
		if ( $wpdb->get_var( "show tables like '$wp_track_table'" ) !== $wp_track_table ) {

			$sql = 'CREATE TABLE `' . $wp_track_table . '` ( ';
			$sql .= '  `id` bigint(20) NOT NULL auto_increment, ';
			$sql .= '  `api_url` longtext NOT NULL, ';
			$sql .= '  `api_response` longtext, ';
			$sql .= '  `response_code` int(5) NOT NULL, ';
			$sql .= "  `date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',";
			$sql .= '  PRIMARY KEY (`id`) ';
			$sql .= ") $charset_collate;";
			require_once( ABSPATH . '/wp-admin/includes/upgrade.php' );
			dbDelta( $sql );
		}

		$pwr_cta_buttons = $wpdb->prefix . 'wpr_cta_buttons';
		if ( $wpdb->get_var( "show tables like '$pwr_cta_buttons'" ) !== $pwr_cta_buttons ) {
			$sql = 'CREATE TABLE `' . $pwr_cta_buttons . '` ( ';
			$sql .= '  `id` bigint(20) NOT NULL auto_increment, ';
			$sql .= '  `user_id` bigint(20) NOT NULL, ';
			$sql .= '  `post_id` bigint(20) NOT NULL, ';
			$sql .= '  `abc_id` bigint(20) NOT NULL, ';
			$sql .= '  `dealer_id` bigint(20) NOT NULL, ';
			$sql .= '  `brand_id` bigint(20) NOT NULL, ';
			$sql .= '  `btn_type` varchar(10) NOT NULL, ';
			$sql .= '  `btn_string` varchar(30) NOT NULL, ';
			$sql .= '  `page_type` int(1) NOT NULL, ';
			$sql .= "  `date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',";
			$sql .= '  PRIMARY KEY (`id`) ';
			$sql .= ") $charset_collate;";
			require_once( ABSPATH . '/wp-admin/includes/upgrade.php' );
			dbDelta( $sql );
		}

		$wpr_waiting_abc_list = $wpdb->prefix . 'wpr_waiting_abc_list';
		if ( $wpdb->get_var( "show tables like '$wpr_waiting_abc_list'" ) !== $wpr_waiting_abc_list ) {
			$sql = 'CREATE TABLE `' . $wpr_waiting_abc_list . '` ( ';
			$sql .= '  `id` bigint(20) NOT NULL auto_increment, ';
			$sql .= '  `brand_id` bigint(20) NOT NULL, ';
			$sql .= '  `dealer_id` bigint(20) NOT NULL, ';
			$sql .= '  `lc_id` bigint(20) NOT NULL, ';
			$sql .= '  `post_id` bigint(20) NOT NULL, ';
			$sql .= '  `lc_order` int(1) NOT NULL, ';
			$sql .= '  `btn_type` varchar(10) NOT NULL, ';
			$sql .= '  `api_call` varchar(10) NOT NULL, ';
			$sql .= "  `date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',";
			$sql .= '  PRIMARY KEY (`id`) ';
			$sql .= ") $charset_collate;";
			require_once( ABSPATH . '/wp-admin/includes/upgrade.php' );
			dbDelta( $sql );
		}

		$wp_track_table = $wpdb->prefix . 'wpr_hubspot_log';
		if ( $wpdb->get_var( "show tables like '$wp_track_table'" ) !== $wp_track_table ) {

			$sql = 'CREATE TABLE `' . $wp_track_table . '` ( ';
			$sql .= '  `id` bigint(20) NOT NULL auto_increment, ';
			$sql .= '  `api_url` longtext NOT NULL, ';
			$sql .= '  `api_response` longtext NOT NULL, ';
			$sql .= '  `response_code` int(5) NOT NULL, ';
			$sql .= "  `date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',";
			$sql .= '  PRIMARY KEY (`id`) ';
			$sql .= ") $charset_collate;";
			require_once( ABSPATH . '/wp-admin/includes/upgrade.php' );
			dbDelta( $sql );
		}

		$wp_track_table = $wpdb->prefix . 'wpr_ontraport_dealerships';
		if ( $wpdb->get_var( "show tables like '$wp_track_table'" ) !== $wp_track_table ) {
			$sql = 'CREATE TABLE `' . $wp_track_table . '` ( ';
			$sql .= '  `id` bigint(20) NOT NULL auto_increment, ';
			$sql .= '  `brand` text NOT NULL, ';
			$sql .= '  `latitude` varchar(255) NOT NULL, ';
			$sql .= '  `longitude` varchar(255) NOT NULL, ';
			$sql .= "  `ontraport_id` varchar(255) NOT NULL,";
			$sql .= "  `data` longtext NOT NULL,";
			$sql .= '  PRIMARY KEY (`id`), ';
			$sql .= '  INDEX inx_brnd (brand), ';
			$sql .= '  INDEX inx_oid (ontraport_id), ';
			$sql .= '  INDEX inx_ll (latitude, longitude), ';
			$sql .= '  INDEX inx_llb (latitude, longitude, brand) ';
			$sql .= ") $charset_collate;";
			require_once( ABSPATH . '/wp-admin/includes/upgrade.php' );
			dbDelta( $sql );
		}

		$wp_track_table = $wpdb->prefix . 'zip_coordinates';
		if ( $wpdb->get_var( "show tables like '$wp_track_table'" ) !== $wp_track_table ) {
			$sql = 'CREATE TABLE `' . $wp_track_table . '` ( ';
			$sql .= '  `id` bigint(20) NOT NULL auto_increment, ';
			$sql .= '  `zip_code` VARCHAR(255) NOT NULL, ';
			$sql .= '  `latitude` VARCHAR(255) NOT NULL, ';
			$sql .= '  `longitude` VARCHAR(255) NOT NULL, ';
			$sql .= '  PRIMARY KEY (`id`), ';
			$sql .= '  INDEX inx_zc (zip_code) ';
			$sql .= ") $charset_collate;";
			require_once( ABSPATH . '/wp-admin/includes/upgrade.php' );
			dbDelta( $sql );
		}

		update_option( 'lbt_db_version', $current_db_version );
	}
}
