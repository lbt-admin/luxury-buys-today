<?php
/**
 * General theme functionality goes here
 *
 * @package LBT
 */

namespace LBT\Core;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Theme Class
 */
class Theme {
	use \LBT\Traits\Singleton;

	/**
	 * Initialize class
	 *
	 * @return  void
	 */
	private function initialize() {
		add_filter( 'auto_update_plugin', '__return_false' );
		add_filter( 'auto_update_theme', '__return_false' );
		add_filter( 'pre_option_link_manager_enabled', '__return_true' );
		add_filter( 'login_headerurl', array( $this, 'login_logo_url' ) );
		add_filter( 'login_headertitle', array( $this, 'login_logo_url_title' ) );
		add_action( 'login_footer', array( $this, 'login_background_image' ) );
		add_action( 'wp_logout', array( $this, 'on_logout' ) );
		add_action( 'after_setup_theme', array( $this, 'remove_admin_bar' ) );
		add_action( 'admin_init', array( $this, 'redirect_non_admin_user' ) );
		add_action( 'pre_get_posts', array( $this, 'alter_main_query' ) );
		add_action( 'save_post', array( $this, 'clear_transients' ), 10, 3 );
	}

	/**
	 * Set the logo url label on the login page
	 *
	 * @return string
	 */
	public function login_logo_url_title() {
		return get_bloginfo( 'name' );
	}

	/**
	 * Set the login logo url
	 *
	 * @return string
	 */
	public function login_logo_url() {
		return home_url();
	}

	/**
	 * Set the login screen background image
	 *
	 * @return void
	 */
	public function login_background_image() {
		echo '<div class="bg"><img src="' . esc_url( LBT_THEME_URL ) . 'assets/images/login-bg-6.jpg"></div>';
	}

	/**
	 * On logout actions
	 *
	 * @return void
	 */
	public function on_logout() {
		$redirect_to = ! empty( $_GET['redirect'] ) ? $_GET['redirect'] : home_url(); //phpcs:ignore
		$redirect_to = sanitize_text_field( $redirect_to );
		wp_safe_redirect( $redirect_to, 302 );
		exit;
	}

	/**
	 * Remove admin bar for given users
	 *
	 * @return void
	 */
	public function remove_admin_bar() {
		if ( ! current_user_can( 'administrator' ) && ! is_admin() ) {
			show_admin_bar( false );
		}
	}

	/**
	 * Redirect non-admin users to home page if they try to access admin dashboard
	 *
	 * @return void
	 */
	public function redirect_non_admin_user() {
		if ( is_user_logged_in() && ! defined( 'DOING_AJAX' ) && ! current_user_can( 'administrator' ) ) {
			wp_safe_redirect( home_url(), 302 );
			exit;
		}
	}

	/**
	 * Alter main loop quer where needed
	 *
	 * @param  WP_Query $query The query.
	 *
	 * @return void
	 */
	public function alter_main_query( $query ) {
		if ( ! is_admin() && $query->is_main_query() && $query->is_category() ) {
			$current_category = get_queried_object();

			if ( ! \LBT\Category::is_broyhill_category( $current_category->term_id ) ) {
				$curr_page = (int) $query->query_vars['paged'];
				if ( 0 === $curr_page ) {
					$paged = 1;
				}

				$query->set( 'offset', ( 13 * ( $curr_page - 1 ) ) + 5 );
			}
		}
	}

	/**
	 * Clear any post transients after post save
	 *
	 * @param  int     $post_id The post id.
	 * @param  WP_Post $post    The Post object.
	 * @param  bool    $update  If post is update or new post.
	 *
	 * @return void
	 */
	public function clear_transients( $post_id, $post, $update ) {
		$pages = array( '', 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 );

		$current_category = get_the_category( $post_id )[0];

		foreach ( $pages as $page ) {
			$left_key       = 'lbt-category-left' . $current_category->term_id . $current_category->category_parent . $page;
			$promotions_key = 'lbt-category-promotions' . $current_category->term_id . $current_category->category_parent . $page;
			$broyhill_key   = 'lbt-category-broyhill' . $current_category->term_id . $page;

			delete_transient( $left_key );
			delete_transient( $promotions_key );
			delete_transient( $broyhill_key );
		}
	}
}
