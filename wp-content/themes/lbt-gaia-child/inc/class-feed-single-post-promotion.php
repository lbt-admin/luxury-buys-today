<?php
/**
 * Class LBT_Feed_Archive_Promotions
 *
 * @package LBT
 */

namespace LBT;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Feed Single Post Promotion
 */
class Feed_Single_Post_Promotion {
	use \LBT\Traits\Singleton;

	/**
	 * Dynamic post ids per category for the automotive category
	 */
	public const DYNAMIC_POSTS = array(
		'audi'                    => 499774,
		'bmw'                     => 499768,
		'cadillac'                => 499775,
		'ferrari'                 => 499776,
		'jaguar'                  => 499777,
		'lexus'                   => 499778,
		'mercedes-benz'           => 499779,
		'bloomingdales'           => 499780,
		'brooks-brothers'         => 499781,
		'neiman-marcus'           => 499782,
		'nordstrom'               => 499783,
		'saks-5th-avenue'         => 499784,
		'tiffany-co'              => 499785,
		'ethan-allen'             => 499786,
		'havertys'                => 499787,
		'christies-international' => 415996,
		'sothebys-international'  => 459317,
	);

	public const DYNAMIC_POSTS_STAG = array(
		'audi'                    => 499774,
		'bmw'                     => 499768,
		'cadillac'                => 499775,
		'ferrari'                 => 499776,
		'jaguar'                  => 499777,
		'lexus'                   => 499778,
		'mercedes-benz'           => 499779,
		'bloomingdales'           => 499780,
		'brooks-brothers'         => 499781,
		'neiman-marcus'           => 499782,
		'nordstrom'               => 499783,
		'saks-5th-avenue'         => 499784,
		'tiffany-co'              => 499785,
		'ethan-allen'             => 499786,
		'havertys'                => 499787,
		'christies-international' => 415996,
		'sothebys-international'  => 459317,
	);


	/**
	 * Initialize class
	 */
	private function initialize() {
		add_action( 'template_redirect', array( $this, 'single_post_overrides' ) );
		add_filter( 'lbt_xml_promotions_content', array( $this, 'single_post_iframe_content_overrides' ), 9999 );
		add_filter( 'wp_insert_post_data', array( $this, 'restrict_editing' ), 9999, 3 );
	}

	/**
	 * Prevent changes to our posts that we use for dynamic loading
	 *
	 * @param array $data The post data.
	 * @param array $postarr The post array data.
	 * @param array $unsanitized_postarr Unsanitize post data.
	 *
	 * @return array
	 */
	public function restrict_editing( $data, $postarr, $unsanitized_postarr ) {
		if ( empty( $postarr['ID'] ) || ! in_array( $postarr['ID'], self::DYNAMIC_POSTS, true ) ) {
			return $data;
		}

		$old = get_post( $postarr['ID'] );

		return get_object_vars( $old );
	}

	/**
	 * Overwrite single post data with remote post data
	 *
	 * @return void
	 */
	public function single_post_overrides() {
		global $post;

		if ( ! is_singular( 'post' ) || wp_doing_cron() || ! is_main_site() || ! in_array( $post->ID, self::DYNAMIC_POSTS, true ) ) {
			return;
		}

		if ( empty( $_GET['id'] ) ) {
			return;
		}

		$post_slug = sanitize_text_field( wp_unslash( $_GET['id'] ) ); //phpcs:ignore

		$remote_promotions = lbt_feed_archive_promotion_posts(
			'',
			'',
			1,
			1,
			'',
			$post_slug
		);

		if ( empty( $remote_promotions ) ) {
			return;
		}

		$post_content = array_values( $remote_promotions );
		$post->post_title = $post_content[0]['listing_item_name'];
	}

	/**
	 * Override content used in the single post iframe to match remote post content
	 *
	 * @param string $content The content.
	 *
	 * @return mixed|void
	 */
	public function single_post_iframe_content_overrides( $content ) {
		global $post;

		$post_thumbnail = Post::get_thumbnail( $post->ID, 'full' );

		if ( $post_thumbnail ) {
			$new_src = "'" . esc_url( $post_thumbnail ) . "'";
			$content = str_replace( '<img', '<img onerror="this.src=' . $new_src . '"', $content );
		}

		if ( ! in_array( $post->ID, self::DYNAMIC_POSTS, true ) ) {
			return $content;
		}

		if ( empty( $_GET['post_feed_id'] ) ) {
			return $content;
		}

		$post_feed_id = sanitize_text_field( wp_unslash( $_GET['post_feed_id'] ) );

		$remote_promotions = lbt_feed_archive_promotion_posts(
			'',
			'',
			1,
			1,
			'',
			$post_feed_id
		);

		if ( empty( $remote_promotions ) ) {
			return $content;
		}

		$post_content = array_values( $remote_promotions );

		if ( ! empty( $post_content[0]['listing_image_url'] ) && Post::remote_file_exists( $post_content[0]['listing_image_url'] ) && false === strpos( $post_content[0]['content'], '<img' ) && isset( $_GET['post_feed_id'] ) ) {
			$content = '<img src="' . esc_url( $post_content[0]['listing_image_url'] ) . '" style="width: 100%">' . $post_content[0]['content'];
		} else {
			$content .= $post_content[0]['content'];
		}

		return strip_tags( do_shortcode( $content ), array( 'img', 'a', 'p', 'div' ) );
	}
}
