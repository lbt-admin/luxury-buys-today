<?php
/**
 * Handles dealer's logic
 *
 * @package LBT
 */

namespace LBT;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Dealer
 */
class Dealer {
	use \LBT\Traits\Singleton;

	/**
	 * Get dealer hokuapps email
	 *
	 * @param  string $email The dealer email.
	 * @param  string $brand The brand (category) name.
	 *
	 * @return bool|string
	 */
	public static function dealer_hokuapps_email( $email, $brand ) {
		$dealer_email = false;
		$email_domain = explode( '@', $email )[1];

		if ( 'hokuapps.com' === $email_domain ) {
			switch ( $brand ) {
				case 'BMW':
					$dealer_email = 'ed@broyhill.net';
					break;
				case 'Audi':
					$dealer_email = 'careertowntest1@' . $email_domain;
					break;
				case 'Mercedes-Benz':
					$dealer_email = 'alohaankurj@gmail.com';
					break;
				case 'Ferrari':
					$dealer_email = 'oyster93@yahoo.com';
					break;
				case 'Jaguar':
					$dealer_email = 'mitko.kockovski@gmail.com';
					break;
				case 'Lexus':
					$dealer_email = 'adam.l.humburg@gmail.com';
					break;
				case 'Bloomingdales':
					$dealer_email = 'careertowntest3@' . $email_domain;
					break;
				case 'Nordstrom':
					$dealer_email = 'billfutreal@gmail.com';
					break;
				case 'Brooks Brothers':
					$dealer_email = 'hello@ugurcan.me';
					break;
				case 'Ethan Allen':
					$dealer_email = 'msdolphin@lethadavis.com';
					break;
			}
		}

		return $dealer_email;
	}

	/**
	 * Get brand by dealership id
	 *
	 * @param  string|array $dealership_id The ontraport dealership id or list of ids.
	 *
	 * @return string
	 */
	public static function get_brand( $dealership_id ) {
		switch_to_blog( 1 );

		global $wpdb;

		$table_name = $wpdb->prefix . 'wpr_ontraport_dealerships';

		$dealership_ids = array();

		if ( ! is_array( $dealership_id ) ) {
			$dealership_id = (array) $dealership_id;
		}

		$dealership_ids = array_map(
			function( $did ) {
				return "'" . esc_sql( $did ) . "'";
			},
			$dealership_id
		);

		$dealership_ids = implode( ',', $dealership_ids );

		$brands = $wpdb->get_results( "SELECT brand, ontraport_id FROM {$table_name} WHERE ontraport_id IN (" . $dealership_ids .  ')' );

		restore_current_blog();

		return $brands;
	}

	/**
	 * Get closes dealer by brand, but manually search.
	 *
	 * @param  int      $brand_id The brand id.
	 * @param  string   $ip The ip or false for current user ip.
	 * @param  int|bool $user_id The user uid.
	 * @param  string   $zip Post code.
	 *
	 * @return array
	 */
	public static function get_closest_dealer_by_brand_manual( $brand_id, $ip = false, $user_id = false, $zip = '' ) {
		switch_to_blog( 1 );

		if ( ! $user_id ) {
			$user_id = get_current_user_id();
		}

		if ( ! $ip ) {
			$ip = User::get_ip();
		}

		$key = 'lbt-closest-dealer-manual-' . $brand_id . $ip;

		$dealer = wp_cache_get( $key, 'lbt-dealer' );

		if ( $dealer ) {
			restore_current_blog();
			return $dealer;
		}

		global $wpdb;

		$table_name = $wpdb->prefix . 'wpr_ontraport_dealerships';

		$brand = get_cat_name( $brand_id );

		if ( in_array( $brand, array( 'Brook Brothers', 'Bloomingdales' ), true ) ) {
			return array();
		}

		if ( strpos( $brand, 'Tiffany' ) !== false ) {
			$brand = 'Tiffany';
		}

		if ( strpos( $brand, 'Christie' ) !== false ) {
			$brand = 'Christie';
		}

		if ( empty( $zip ) ) {
			$zip = User::get_user_zip( $user_id );
		}

		if ( empty( $zip ) ) {
			return array();
		}

		if ( ! in_array( (int) $zip, Constants::TEST_ZIPCODES, true ) ) {
			return array();
		}

		if (
			stripos( $brand, 'Audi' ) !== false ||
			stripos( $brand, 'BMW' ) !== false ||
			stripos( $brand, 'Jaguar' ) !== false ||
			stripos( $brand, 'Ferrari' ) !== false ||
			stripos( $brand, 'Cadillac' ) !== false ||
			stripos( $brand, 'Mercedes' ) !== false ||
			stripos( $brand, 'Lexus' ) !== false ||
			stripos( $brand, 'Sotheby' ) !== false
		) {
			$zip_code = 75209;
		} elseif (
			stripos( $brand, 'Nordstrom' ) !== false ||
			stripos( $brand, 'Tiffany' ) !== false
		) {
			$zip_code = 75225;
		} elseif (
			stripos( $brand, 'Neiman' ) !== false
		) {
			$zip_code = 75201;
		} elseif (
			stripos( $brand, 'Saks' ) !== false
		) {
			$zip_code = 77056;
		} elseif (
			stripos( $brand, 'Havetry' ) !== false
		) {
			$zip_code = 75150;
		} elseif (
			stripos( $brand, 'Ethan' ) !== false
		) {
			$zip_code = 75240;
		} elseif (
			stripos( $brand, 'Christie' ) !== false
		) {
			$zip_code = 75205;
		} else {
			$zip_code = 0;
		}

		if ( empty( $zip_code ) ) {
			return array();
		}

		$sql = $wpdb->prepare( "SELECT * FROM {$table_name} WHERE brand = %s AND data LIKE %s LIMIT 1", $brand, '%' . $zip_code . '%' );

		$dealerships = $wpdb->get_results( $sql );

		$dealer = array();

		if ( $dealerships ) {
			$dealer = maybe_unserialize( $dealerships[0]->data );
		}

		wp_cache_set( $key, $dealer, 'lbt-dealer' );

		restore_current_blog();

		return $dealer;
	}

	/**
	 * Get the closest dealer to a given brand
	 *
	 * @param  int         $brand_id The brand (category) id.
	 * @param  string|bool $ip The user ip.
	 * @param  int|bool    $user_id The user id.
	 *
	 * @return array|WP_Error
	 */
	public static function get_closest_dealer_by_brand( $brand_id, $ip = false, $user_id = false ) {
		if ( ! $user_id ) {
			$user_id = get_current_user_id();
		}

		$zip = User::get_user_zip( $user_id );

		$last_part_of_key = $zip;

		if ( empty( $last_part_of_key ) && is_user_logged_in() ) {
			$last_part_of_key = $user_id;
		}

		$key = 'lbtdealer' . $brand_id . $last_part_of_key;

		$dealer = get_transient( $key );

		if ( ! empty( $dealer ) ) {
			return $dealer;
		}

		$dealer = self::get_closest_dealer_by_brand_manual( $brand_id, $ip, $user_id, $zip );

		if ( ! empty( $dealer ) ) {
			set_transient( $key, $dealer );
			return $dealer;
		}

		switch_to_blog( 1 );

		if ( ! $ip ) {
			$ip = User::get_ip();
		}

		$dealer = wp_cache_get( $key, 'lbt-dealer' );

		if ( $dealer ) {
			set_transient( $key, $dealer );
			restore_current_blog();
			return $dealer;
		}

		global $wpdb;

		$table_name = $wpdb->prefix . 'wpr_ontraport_dealerships';

		$brand = get_cat_name( $brand_id );

		if ( strpos( $brand, 'Tiffany' ) !== false ) {
			$brand = 'Tiffany';
		}

		if ( strpos( $brand, 'Christie' ) !== false ) {
			$brand = 'Christie';
		}

		if ( empty( $user_id ) ) {
			$current_user = wp_get_current_user();
		} else {
			$current_user = get_userdata( $user_id );
		}

		$current_user_email = $current_user ? $current_user->user_email : false;

		$dealerships = false;

		if ( $current_user_email ) {
			$dealer_email = self::dealer_hokuapps_email( $current_user_email, $brand );

			if ( $dealer_email ) {
				$sql         = $wpdb->prepare( "SELECT data{fields} FROM {$table_name} WHERE data LIKE %s", '%' . $dealer_email . '%' );//phpcs:ignore
				$sql         = self::prepare_closes_dealer_sql( $sql );
				$dealerships = $wpdb->get_results( $sql ); //phpcs:ignore
			}
		}

		if ( empty( $dealerships ) ) {
			$sql         = $wpdb->prepare( "SELECT data{fields} FROM {$table_name} WHERE brand LIKE %s", '%' . $brand . '%' ); //phpcs:ignore
			$sql         = self::prepare_closes_dealer_sql( $sql, $user_id );
			$dealerships = $wpdb->get_results( $sql ); //phpcs:ignore
		}

		restore_current_blog();

		$dealer = array();

		if ( $dealerships ) {
			$dealer = maybe_unserialize( $dealerships[0]->data );
		}

		wp_cache_set( $key, $dealer, 'lbt-dealer' );

		set_transient( $key, $dealer );

		return $dealer;
	}

	/**
	 * Preparea the closes dealer sql
	 *
	 * @param string   $sql The initial sql string.
	 * @param int|bool $user_id The user id.
	 *
	 * @return mixed
	 */
	private static function prepare_closes_dealer_sql( $sql, $user_id = false ) {
		global $wpdb;

		$user_lat = false;
		$user_lng = false;

		if ( ! $user_id ) {
			$user_id = get_current_user_id();
		}

		if ( $user_id ) {
			$zip = get_user_meta( $user_id, 'zip_code', true );

			if ( ! empty( $zip ) ) {
				$geocode_response = wp_remote_get( 'https://maps.googleapis.com/maps/api/geocode/json?address=' . esc_attr( $zip ) . '&key=AIzaSyDkE0osaaC14WeHZ75j54HnXqDjz9Z5fg8' );

				if ( ! is_wp_error( $geocode_response ) ) {
					$output = json_decode( wp_remote_retrieve_body( $geocode_response ) );
					if ( $output->results ) {
						$user_lat = $output->results[0]->geometry->location->lat;
						$user_lng = $output->results[0]->geometry->location->lng;
					}
				}
			}
		}

		if ( false === $user_lat ) {
			$ip = User::get_ip();

			$user_ip = explode( ',', $ip );

			if ( ! empty( $user_ip ) ) {
				$ip = $user_ip[0];
			}

			$geocode = @unserialize( file_get_contents( 'http://ip-api.com/php/' . $ip ) );

			if ( 'fail' !== $geocode['status'] ) {
				$user_lat = $geocode['lat'];
				$user_lng = $geocode['lon'];
			}
		}

		if ( false === $user_lat || false === $user_lng ) {
			$user_lat = '40.748194';
			$user_lng = '-73.984925';
		}

		// 3959 for miles or 6371 for kilometers.
		$units = 3959;

		$sql = str_replace(
			'{fields}',
			$wpdb->prepare(
				', ROUND( %d * acos( cos( radians( %s ) ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians( %s ) ) + sin( radians( %s ) ) * sin( radians( latitude ) ) ),1 ) AS distance',
				$units,
				$user_lat,
				$user_lng,
				$user_lat
			),
			$sql
		);

		$sql .= ' GROUP BY id ORDER BY distance ASC LIMIT 1';

		return $sql;
	}

	/**
	 * Send CTA Request to dealer
	 *
	 * @param  int    $dealership_id The Ontraport dealership id.
	 * @param  int    $brand_id      The brand (category) id.
	 * @param  string $action        The action to be send.
	 * @param  int    $post_id       The post id if the request is coming form single post page.
	 *
	 * @return bool|WP_Error
	 */
	public function send_cta_request( $dealership_id, $brand_id, $action, $post_id = 0 ) {
		if ( ! in_array( $action, array( 'price', 'learn', 'contact', 'appointment' ), true ) ) {
			return new \WP_Error(
				400,
				__( 'Invalid action requestion. Please request for "price", "learn", "appointment" or "contact"', 'lbt' )
			);
		}

		$current_user = wp_get_current_user();

		$prospect_ontraport_id = get_user_meta( $current_user->ID, 'wpr_ontraport_id', true );

		if ( ! $prospect_ontraport_id ) {
			return new \WP_Error(
				404,
				__( 'Contact not found for current user in Ontraport', 'lbt' )
			);
		}

		$brand = get_category( $brand_id );

		$dealer_email = $this->dealer_hokuapps_email( $current_user->user_email, $brand );

		$ontraport = Ontraport::get_instance();

		if ( $dealer_email ) {
			$dealership = $ontraport->get_dealership_by_contact_email( $dealer_email );
			if ( ! is_wp_error( $dealership ) && false !== $dealership ) {
				$dealership_id = $dealership['id'];
			}
		}

		$match = $ontraport->get_match_between_prospect_and_dealership( $dealership_id, $prospect_ontraport_id );

		if ( is_wp_error( $match ) ) {
			return $match;
		}

		if ( ! $match ) {
			$match = $ontraport->save_match( $dealership_id, $prospect_ontraport_id );

			if ( is_wp_error( $match ) ) {
				return $match;
			}
		}

		$post_id   = absint( $post_id );
		$thumbnail = $post_id ? Post::get_thumbnail( $post_id ) : '';
		$url       = $post_id ? get_permalink( $post_id ) : get_category_link( $brand_id );
		$url       = $post_id ? $url : str_replace( '/category/', '/', $url );

		switch ( $action ) {
			case 'price':
				$todo_type = 'Request for Price';
				break;
			case 'learn':
				$todo_type = 'Request for Info';
				break;
			case 'contact':
				$todo_type = 'Request for Call';
				break;
			case 'appointment':
				$todo_type = 'Request for Appointment';
				break;
			default:
				$todo_type = '';
		}

		$activity_data = array(
			'f2761' => $post_id ? get_the_title( $post_id ) : '',
			'f2762' => $match['id'],
			'f2763' => $thumbnail,
			'f2764' => $url,
			'f2765' => 6,
			'f2766' => time(),
			'f2767' => gmdate( 'H:i:s' ),
			'f2768' => $todo_type,
			'f2800' => true,
		);

		if ( $post_id > 0 ) {
			update_post_meta( $post_id, 'cta_clicked', true );
		}

		return $ontraport->save_activity( $activity_data );
	}

	/**
	 * Sync dealership from ontraport
	 *
	 * @param  string $ontraport_id The ontraport dealership id.
	 *
	 * @return boolean|WP_Error
	 */
	public static function sync_dealership( $ontraport_id ) {
		global $wpdb;

		$table_name = $wpdb->prefix . 'wpr_ontraport_dealerships';

		$ontraport = Ontraport::get_instance();

		$dealership = $ontraport->request(
			'Dealership-ABCs?id=' . $ontraport_id,
			array( 'method' => 'GET' )
		);

		if ( is_wp_error( $dealership ) ) {
			return $dealership;
		}

		if ( empty( $dealership ) ) {
			return new \WP_Error(
				404,
				__( 'Dealer with id ' . $ontraport_id . ' not found' )
			);
		}

		$dealership = $dealership[0];

		$condition = array();

		$condition[] = array(
			'field' => array( 'field' => 'f2963' ),
			'op'    => '=',
			'value' => array( 'value' => $dealership['id'] ),
		);
		$condition[] = 'AND';
		$condition[] = array(
			'field' => array( 'field' => 'f2882' ),
			'op'    => '=',
			'value' => array( 'value' => 182 ),
		);
		$condition[] = 'AND';
		$condition[] = array(
			'field' => array( 'field' => 'f2964' ),
			'op'    => '=',
			'value' => array( 'value' => 1 ),
		);

		$dealership_contact = $ontraport->request(
			'Contacts?range=1&condition=' . rawurlencode( wp_json_encode( $condition ) ),
			array( 'method' => 'GET' )
		);

		if ( is_wp_error( $dealership_contact ) ) {
			return $dealership_contact;
		}

		$dealership['contact_data'] = array();

		if ( is_array( $dealership_contact ) && ! empty( $dealership_contact ) ) {
			$dealership['contact_data'] = $dealership_contact[0];
		}

		$dealership_exists = (int) $wpdb->get_var( $wpdb->prepare( "SELECT COUNT(*) FROM {$table_name} WHERE ontraport_id = %s", $ontraport_id ) );

		if ( $dealership_exists ) {
			$brand_name = sanitize_text_field( $dealership['f1732'] );

			$transient_prefix = '_transient_lbtdealer%';
			if ( ! empty( $brand_name ) ) {
				$brand = get_term_by( 'name', $brand_name, 'category' );

				if ( $brand ) {
					$transient_prefix = '_transient_lbtdealer' . $brand->term_id . '%';
				}
			}

			$wpdb->query(
				$wpdb->prepare(
					"DELETE FROM {$wpdb->options} WHERE option_name LIKE %s OR option_name LIKE %s",
					$transient_prefix,
					str_replace( '_transient_', '_transient_timeout_', $transient_prefix )
				)
			);

			$wpdb->update(
				$table_name,
				array(
					'brand'     => $brand_name,
					'latitude'  => sanitize_text_field( $dealership['f2823'] ),
					'longitude' => sanitize_text_field( $dealership['f2824'] ),
					'data'      => maybe_serialize( $dealership ),
				),
				array(
					'ontraport_id' => $ontraport_id,
				),
				array(
					'%s',
					'%s',
					'%s',
					'%s',
				),
				array(
					'%s',
				)
			);
		} else {
			$wpdb->insert(
				$table_name,
				array(
					'brand'        => sanitize_text_field( $dealership['f1732'] ?? '' ),
					'latitude'     => sanitize_text_field( $dealership['f2823'] ?? '' ),
					'longitude'    => sanitize_text_field( $dealership['f2824'] ?? '' ),
					'ontraport_id' => $ontraport_id,
					'data'         => maybe_serialize( $dealership ),
				),
				array(
					'%s',
					'%s',
					'%s',
					'%s',
					'%s',
				)
			);
		}

		if ( is_array( $dealership_contact ) && ! empty( $dealership_contact ) ) {
			return 'For Dealership ID: ' . $dealership['id'] . ' Found Contact with ID: ' . $dealership_contact[0]['id'];
		} else {
			return 'For Dealership ID: ' . $dealership['id'] . ' NO contact found defaulting to info from dealership';
		}
	}

	/**
	 * Sync dealerships from ontraport to database
	 *
	 * @return void
	 */
	public function sync_dealerships() {
		$start = 0;
		$range = 50;

		global $wpdb;

		$table_name = $wpdb->prefix . 'wpr_ontraport_dealerships';

		$sql = "SELECT ontraport_id FROM $table_name";

		$results = $wpdb->get_results( $sql ); //phpcs:ignore

		$ontraport_ids = array();

		if ( $results ) {
			foreach ( $results as $res ) {
				$ontraport_ids[] = $res->ontraport_id;
			}
		}

		$ontraport = Ontraport::get_instance();

		while ( true ) {
			$dealerships = $ontraport->request(
				'Dealership-ABCs?start=' . $start . '&range=' . $range,
				array( 'method' => 'GET' )
			);

			if ( empty( $dealerships ) || ! is_array( $dealerships ) || ! isset( $dealerships[0]['id'] ) || count( $ontraport_ids ) === count( $dealerships ) ) {
				break;
			}

			foreach ( $dealerships as $dealership ) {
				$condition = array();

				$condition[] = array(
					'field' => array( 'field' => 'f2963' ),
					'op'    => '=',
					'value' => array( 'value' => $dealership['id'] ),
				);
				$condition[] = 'AND';
				$condition[] = array(
					'field' => array( 'field' => 'f2882' ),
					'op'    => '=',
					'value' => array( 'value' => 182 ),
				);
				$condition[] = 'AND';
				$condition[] = array(
					'field' => array( 'field' => 'f2964' ),
					'op'    => '=',
					'value' => array( 'value' => 1 ),
				);

				$dealership_contact = $ontraport->request(
					'Contacts?range=1&condition=' . rawurlencode( wp_json_encode( $condition ) ),
					array( 'method' => 'GET' )
				);

				$dealership['contact_data'] = array();

				if ( is_array( $dealership_contact ) && ! empty( $dealership_contact ) ) {
					$dealership['contact_data'] = $dealership_contact[0];
				}

				if ( in_array( $dealership['id'], $ontraport_ids ) ) {
					$wpdb->update(
						$table_name,
						array(
							'brand'     => sanitize_text_field( $dealership['f1732'] ),
							'latitude'  => sanitize_text_field( $dealership['f2823'] ),
							'longitude' => sanitize_text_field( $dealership['f2824'] ),
							'data'      => maybe_serialize( $dealership ),
						),
						array(
							'ontraport_id' => sanitize_text_field( $dealership['id'] ),
						),
						array(
							'%s',
							'%s',
							'%s',
							'%s',
						),
						array(
							'%s',
						)
					);
				} else {
					$wpdb->insert(
						$table_name,
						array(
							'brand'        => sanitize_text_field( $dealership['f1732'] ?? '' ),
							'latitude'     => sanitize_text_field( $dealership['f2823'] ?? '' ),
							'longitude'    => sanitize_text_field( $dealership['f2824'] ?? '' ),
							'ontraport_id' => sanitize_text_field( $dealership['id'] ),
							'data'         => maybe_serialize( $dealership ),
						),
						array(
							'%s',
							'%s',
							'%s',
							'%s',
							'%s',
						)
					);
				}
			}

			$start += $range;
		}
	}

	/**
	 * Send dealership list after update of profile to news site
	 *
	 * @param  array    $dealerships_list List of dealerships.
	 * @param  int|bool $user_id The user id that is updating dealerships or false for current user.
	 *
	 * @return void
	 */
	public static function send_dealerships_to_news_site(  $dealerships_list, $user_id = false ) {
		if ( ! $user_id ) {
			$user_id = get_current_user_id();
		}

		$userdata = get_userdata( $user_id );

		$response = wp_remote_post(
			'https://news.luxurybuystoday.com/wp-json/newsletter-lbt/v1/subscriber-location-updated/',
			array(
				'timeout' => 45,
				'headers' => array(
					'Content-Type' => 'application/json',
				),
				'body' => wp_json_encode(
					array(
						'email'       => $userdata->user_email,
						'dealerships' => $dealerships_list,
					)
				)
			)
		);
	}
}
