<?php
/**
 * Template name: BA My offers
 *
 * @package Novus
 */

get_header(); ?>

<div id="primary" class="content-area wpr-ba-prospects">
    <main id="main" class="site-main" role="main">

        <h1 class="entry-title"><?php the_title(); ?></h1>
		<?php
		$user = wp_get_current_user();
		if ( is_user_logged_in() && ( in_array( 'business_associate', (array) $user->roles ) || current_user_can( 'administrator' ) ) ) {
		$user_id        = $user->ID;
		$my_offers_page = get_the_ID();

		if ( isset( $_GET['add_offer'] ) ) {
			echo do_shortcode( '[gravityform id="5" title="false" description="false"]' );
		} elseif ( isset( $_GET['edit_offer'] ) ) {
			$offer_id = absint( $_GET['gform_post_id'] );
			$post     = get_post( $offer_id );

			if ( $post->post_author == $user_id ) {
				set_query_var( 'offer_id', absint( $offer_id ) );
				get_template_part( 'partials/content', 'edit-offer' );
			} else {
				_e( 'You don\'t have access to this offer!', 'wpr-table' );
			}

		} elseif ( $_GET['delete_offer'] ) {
			$offer_id = absint( $_GET['delete_offer'] );
			$post     = get_post( $offer_id );
			if ( $post->post_author == $user_id ) {
				wpr_ba_delete_offer( $offer_id, $my_offers_page );
			} else {
				_e( 'You don\'t have access to this offer!', 'wpr-table' );
			}
		}
		else { ?>
        <div id="displayProspectorLookbook">
            <a href="<?php echo esc_url( get_permalink( get_the_ID() ) ); ?>?add_offer" class="datatable_buttons"><?php _e( 'Add a new Offer', 'wpr-table' ); ?></a>
            <table id="wprlistingTable" class="table dataTable display" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th><?php _e( 'Offer title', 'wpr-table' ); ?></th>
                    <th><?php _e( 'Offer image', 'wpr-table' ); ?></th>
                    <th><?php _e( 'Date added', 'wpr-table' ); ?></th>
                    <th><?php _e( '', 'wpr-table' ); ?></th>
                </tr>
                </thead>
                <tfoot>
                <tr>
                    <th><?php _e( 'Offer title', 'wpr-table' ); ?></th>
                    <th><?php _e( 'Offer image', 'wpr-table' ); ?></th>
                    <th><?php _e( 'Date added', 'wpr-table' ); ?></th>
                    <th><?php _e( '', 'wpr-table' ); ?></th>
                </tr>
                </tfoot>
				<?php
				echo '<tbody>';
				$args  = array(
					'post_type' => 'post',
					'author'    => $user_id,
				);
				$query = new WP_Query( $args );
				$posts = $query->posts;

				if ( $posts ) {
					foreach ( $posts as $post ) {
						$post_image   = get_the_post_thumbnail( $post->ID, array( 80, 80 ), array( 'class' => 'alignleft' ) );
						$offer_url    = get_the_permalink( $post->ID );
						$offer_edit   = esc_url( get_permalink( $my_offers_page ) ) . "?edit_offer&gform_post_id=" . $post->ID;
						$offer_delete = esc_url( get_permalink( $my_offers_page ) ) . "?delete_offer=" . $post->ID;

						echo '<tr>';
						_e( sprintf( '<td><a href="%s" target="_blank">%s</a></td>', $offer_url, $post->post_title ) );
						_e( sprintf( '<td><a href="%s" target="_blank">%s</a></td>', $offer_url, $post_image ) );
						_e( sprintf( '<td><a href="%s" target="_blank">%s</a></td>', $offer_url, $post->post_date ) );
						_e( sprintf( '<td><a href="%s" target="_blank" class="datatable_buttons edit_button">%s</a><a href="%s" class="datatable_buttons delete_button">%s</a></td>', $offer_edit, __( '<i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit' ), $offer_delete, __( '<i class="fa fa-trash" aria-hidden="true"></i> Delete' ) ) );
						echo '</tr>';
					}
				}
				wp_reset_postdata();
				echo '</tbody>';
				?>
            </table>
			<?php }
			} ?>
    </main><!-- #main -->
</div><!-- #primary -->

<?php get_footer(); ?>
