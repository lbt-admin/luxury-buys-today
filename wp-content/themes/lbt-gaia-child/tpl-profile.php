<?php
/**
 * Template name: Edit Profile
 *
 * @package Novus
 */
get_header();
?>

<div id="primary" class="content-area wpr-edit-profile">
    <main id="main" class="site-main" role="main">

        <?php if (is_user_logged_in()) : ?>
            <div class="wpr-user-avatar-profile">
                <?php echo do_shortcode('[avatar_upload]'); ?>
                <div><h2><?php echo __('Edit Profile'); ?></h2></div>
            </div>
            <?php wpfep_show_profile(); ?>
        <?php else : ?>

            <article class="text-center">
                <div class="entry-content">
                    <p><?php _e('You need to be registered to edit your profile.', 'novus'); ?></p>
                    <p><a class="btn" href="#popmake-33647" title="Login">Login</a> or <a class="btn" href="#popmake-32845">Register</a></p>
                </div>
            </article>

        <?php endif; ?>


    </main><!-- #main -->
</div><!-- #primary -->

<?php #get_sidebar(); ?>
<?php get_footer(); ?>
<script>
    jQuery(document).ready(
            function () {
                $('.brand-button').click(function () {
                    var brand_id = $(this).data('id');
                    if ($(this).hasClass('selected-brand')) {
                        var todo = 'remove';
                    } else {
                        var todo = 'add';
                    }
                    var elem = $(this);
                    jQuery.ajax({
                        url: '<?php echo admin_url("admin-ajax.php"); ?>',
                        dataType: 'json',
                        type: 'post',
                        data: {
                            action: 'add_remove_brands_offers',
                            brand_id: brand_id,
                            todo: todo
                        },
                        success: function (json) {
                            if (json.response) {
                                if (json.todo == 'add') {
                                    elem.addClass('selected-brand');
                                    elem.attr("src", "/wp-content/themes/lbt-gaia-child/assets/images/add_to_lookbook_red.svg");
                                } else {
                                    elem.removeClass('selected-brand');
                                    elem.attr("src", "/wp-content/themes/lbt-gaia-child/assets/images/add_to_lookbook_1.svg");
                                }
                            }
                        },
                        complete: function () {
                        }
                    });
                });
            });

</script>
