<?php
/**
 * Categories List Shortcode Template
 *
 * @package Novus
 */

?>
<a href="<?php echo esc_url( $category_link ); ?>" class="item-1">
	<div class="thumbnail">
		<img data-original="<?php echo esc_url( $header_background ); ?>" alt="" class="lazyload post-size2">
	</div>
	<div>
		<h4 class="title-1"><?php echo esc_html( $category->name ); ?></h4>
		<p><?php echo wp_kses_post( mb_strimwidth( $category->category_description, 0, 110, '...' ) ); ?></p>
	</div>
</a>
