<?php
/**
 * Home Posts List Shortcode Template
 *
 * @package Novus
 */

while ( $query->have_posts() ) :
	$query->the_post();
	if ( $counter === $offset ) {
		$class    = 'home-post-big ' . $append_class;
		$img_size = 'wpr_listing';

		if ( 'broyhill' === $atts['layout'] ) {
			?>
			<div class="entry-box-shadow black-box">
				<?php get_template_part( 'partials/content', 'today_news' ); ?>
			</div>
			<?php
			$counter++;
			continue;
		}
	} else {
		$class    = 'home-post-small';
		$img_size = 'offer_thumb';
	}

	if ( 2 === $column ) {
		echo ( $counter === $offset + 1 ) ? '<div class="half">' : '';

		if ( 1 === $counter ) {
			$class .= ' half';
		}
	}

	$post_thumbnail = \LBT\Post::get_thumbnail( get_the_ID(), $img_size );
	?>
	<article class="<?php echo esc_attr( $class ); ?>">
		<?php if ( $post_thumbnail ) : ?>
		<a href="<?php echo esc_url( get_permalink() ); ?>" class="featured-image">
			<img
				class="image-cover"
				src=""
				data-test="1"
				onerror="this.src='<?php echo esc_url( $post_thumbnail ); ?>'; if ( this.getAttribute('retry') ) { if( this.getAttribute('retry') == 1 ) { this.src='<?php echo esc_url( $post_thumbnail ); ?>'; this.setAttribute( 'retry', 2 ); } else { this.src='<?php echo esc_url( LBT_THEME_URL ); ?>assets/images/post-thumb-cropped.svg'; } } else { this.setAttribute( 'retry', 1 ); }">
		</a>
		<?php endif; ?>
		<div class="post-content">
			<h4 class="title">
				<?php $post_title = ucwords( strtolower( get_the_title() ) ); ?>
				<a href="<?php echo esc_url( get_permalink() ); ?>" class="title">
					<?php echo esc_html( mb_strimwidth( $post_title, 0, 49, '...' ) ); ?>
				</a>
			</h4>
			<?php if ( $counter === $offset ) : ?>
				<div class="post-meta">
					<span>Published by: </span>
					<a href="http://luxurybuystoday.tumblr.com/" target="_blank">Ed Broyhill</a> |
					<a href="javascript:void(0);"><?php echo get_the_date(); ?></a>
				</div>
			<?php endif; ?>
			<p class="description">
				<?php
				$word      = ( $counter === $offset ) ? 30 : 14;
				$content   = wpr_display_ed_comment( get_the_ID() );
				$converted = strtr( $content, array_flip( get_html_translation_table( HTML_ENTITIES, ENT_QUOTES ) ) );
				$trimmed   = trim( $converted, chr( 0xC2 ) . chr( 0xA0 ) );

				echo wp_kses_post( wp_trim_words( $trimmed, $word, '' ) );
				?>
				<a href="<?php echo esc_url( get_permalink() ); ?>" class="read-more">Read more...</a>
			</p>
		</div>
	</article>

	<?php
	$is_eds = 0;
	if ( 2 === $column ) {
		echo ( $counter === $amount ) ? '</div>' : '';
	}

	$counter++;
endwhile;
wp_reset_postdata();
