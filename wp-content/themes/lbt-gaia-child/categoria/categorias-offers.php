<?php
/**
 * Posts Template
 *
 * @package Novus
 */

?>
<div class="item-big item-2cols">
	<a href="<?php echo esc_url( get_permalink() ); ?>">
		<div class="entry-image-1 ">
			<?php
			if ( false !== strpos( $urls[0][0], 'youtube' ) || false !== strpos( $instagram_urls[0][0], 'instagram' ) || '' !== $twitter_video || '' !== $twitter_video_gif ) :
				?>
				<span class="video_post_icon"></span>
				<?php
			endif;

			$post_thumbnail = \LBT\Post::get_thumbnail( get_the_ID(), 'wpr_listing' );
			if ( $post_thumbnail ) :
				?>
				<div class="image_wrap" style="background-image:url(<?php echo esc_url( $post_thumbnail ); ?>)">
					<img
						class="image-cover"
						src=""
						data-test="1"
						onerror="this.src='<?php echo esc_url( $post_thumbnail ); ?>'; if ( this.getAttribute('retry') ) { if( this.getAttribute('retry') == 1 ) { this.src='<?php echo esc_url( $post_thumbnail ); ?>'; this.setAttribute( 'retry', 2 ); } else { this.src='<?php echo esc_url( LBT_THEME_URL ); ?>assets/images/post-thumb-cropped.svg'; } } else { this.setAttribute( 'retry', 1 ); }">
				</div>
				<?php
			else :
				$wpr_img_url = \LBT\Post::maybe_get_thumbnail_from_youtube_embed( get_the_ID() );

				if ( ! $wpr_img_url ) {
					$wpr_img_url = LBT_THEME_URL . 'assets/images/post-thumb-cropped.svg';
				}

				if ( wpr_remote_check_file( $wpr_img_url ) ) :
					?>
					<div class="image_wrap" style="background-image:url(<?php echo esc_url( $wpr_img_url ); ?>)">
						<img src="<?php echo esc_url( $wpr_img_url ); ?>" data-original="<?php echo esc_url( $wpr_img_url ); ?>" alt="thumbnail">
					</div>
					<?php
				else :
					$first_content_image = \LBT\Post::get_first_image_from_content( $post );
					?>
					<div class="image_wrap" style="background-image:url(<?php echo esc_url( $first_content_image ); ?>);">
						<img class="image-here-2" src="<?php echo esc_url( $first_content_image ); ?>"
							onerror="this.src='<?php echo esc_url( LBT_THEME_URL . 'assets/images/post-thumb-cropped.svg' ); ?>">
					</div>
					<?php
				endif;
				?>
			<?php endif; ?>
		</div>
	</a>
	<div class="art-3">
		<h4 class="title-3">
			<a href="<?php echo esc_url( get_permalink() ); ?>"><?php echo esc_html( mb_strimwidth( get_the_title(), 0, 49, '...' ) ); ?></a>
		</h4>
		<p>
			<span>Published by: </span>
			<a href="<?php echo esc_url( get_author_posts_url( 1 ) ); ?>"> Ed Broyhill</a> |
			<a href="#"><?php echo esc_html( get_the_date() ); ?></a>
		</p>
		<p class="descripcion"><?php the_excerpt(); ?></p>
	</div>
</div>
