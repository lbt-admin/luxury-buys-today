<?php
/**
 * Template name: Register template
 *
 * @package Novus
 */

get_header(); ?>

<div class="wpr-login-register">
    <div class="wpr-login-register-header">
        <div class="wpr-login-register-lbt-logo">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/LBT-logo.png"/>
        </div>
        <h2 class="header-2"><?php esc_html_e( 'Your daily source of luxury buys', 'lbt' ); ?></h2>
    </div>
    <div class="wpr-login-register-body">
        <div class="wpr-login-register-body-wrapper">
            <div class="wpr-login-register-body-content">
                <div class="wpr-login-register-body-divider-wide"></div>
                <div class="wpr-login-register-body-content-inner">
                    <div class="wpr-login-register-form">
						<?php echo do_shortcode( '[gravityform id="2" title="false" description="false" ajax="true"]' ); ?>
                    </div>
                    <div class="wpr-login-register-social">
						<?php echo do_shortcode( '[oa_social_login]' ); ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="wpr-login-register-body-footer">
			<?php
			$login_url = get_permalink( get_page_by_path( 'login' ) );
			_e( sprintf( 'Already have an account? <a href="%s">%s</a>', esc_url( $login_url ), __( 'Sign in' ) ) );
			?>
        </div>
    </div>
</div>

<?php get_footer(); ?>
