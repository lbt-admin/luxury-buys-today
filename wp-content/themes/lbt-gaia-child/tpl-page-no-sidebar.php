<?php
/**
 * Template name: Page Without Sidebar
 *
 * @package Novus
 */

get_header(); ?>

<div id="primary" class="content-area full-width">
	<main id="main" class="site-main" role="main">

		<?php while ( have_posts() ) : the_post(); ?>

			<?php get_template_part( 'partials/content', 'page' ); ?>

		<?php endwhile; ?>

	</main><!-- #main -->
</div><!-- #primary -->

<?php get_footer(); ?>
