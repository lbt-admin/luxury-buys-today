<?php
// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! defined( 'LBT_THEME_DIR' ) ) {
	define( 'LBT_THEME_DIR', trailingslashit( get_stylesheet_directory() ) );
}

if ( ! defined( 'LBT_THEME_URL' ) ) {
	define( 'LBT_THEME_URL', trailingslashit( get_stylesheet_directory_uri() ) );
}

if ( ! defined( 'LBT_THEME_VERSION' ) ) {
	define( 'LBT_THEME_VERSION', 6 );
}

if ( ! defined( 'IMAGES_DIR_PATH' ) ) {
	define( 'IMAGES_DIR_PATH', '/assets/images/' );
}

if ( ! function_exists( 'wpdd' ) ) {
	/**
	 * Debug function
	 *
	 * @param  mixed $data Data to be shown.
	 *
	 * @return void
	 */
	function wpdd( $data ) {
		echo '<pre>' . print_r( $data, true ) . '</pre>';
		die;
	}
}

// Require modules.
require_once LBT_THEME_DIR . 'inc/init.php';

// Load widgets
require get_stylesheet_directory() . '/inc/webpigment-optimisation.php';
require get_stylesheet_directory() . '/inc/widget-posts-list-lookbook.php';
require get_stylesheet_directory() . '/inc/widget-posts-list-category.php';
require get_stylesheet_directory() . '/inc/widget-subscribe-form.php';
require get_stylesheet_directory() . '/inc/wp-async-request.php';
require get_stylesheet_directory() . '/inc/wp-background-process.php';
require get_stylesheet_directory() . '/inc/wpr_run_process.php';
require get_stylesheet_directory() . '/inc/wpr_run_text_spin.php';
require get_stylesheet_directory() . '/inc/wpr_wordai_fetch.php';

/**
 * Load login/register screen assets
 *
 * @return void
 */
function lbt_login_enqueue_styles() {
	wp_enqueue_style( 'login-style', LBT_THEME_URL . 'style-login.css');
}
add_action( 'login_enqueue_scripts', 'lbt_login_enqueue_styles' );

// Override parent theme scripts and styles
function lbt_scripts_override() {
	wp_enqueue_style( 'child-style', LBT_THEME_URL . 'style.css', array('parent-style') );
	wp_enqueue_style( 'semantic_sticky', LBT_THEME_URL . 'assets/css/semantic_sticky.min.css' );
	wp_enqueue_style( 'pop-up-maker', LBT_THEME_URL . 'assets/css/pop_up_maker.css' );
	wp_enqueue_style( 'ios8_styles', LBT_THEME_URL . 'assets/css/iOS_8.css' );

	wp_deregister_style('dashicons');
	wp_deregister_script('jquery');

	wp_enqueue_script( 'jquery', '//ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js', array(), 1, false );
	wp_enqueue_script('jquery-confirm', get_stylesheet_directory_uri() . '/assets/js/jquery-confirm.min.js', array('jquery'), '', false);
	// wp_enqueue_script('jquery-masonry');

	wp_register_script(
		'lbt-select2',
		LBT_THEME_URL . 'assets/libs/select2/js/select2.min.js',
		array(),
		LBT_THEME_VERSION,
		true
	);

	wp_register_style(
		'lbt-select2',
		LBT_THEME_URL . 'assets/libs/select2/css/select2.min.css',
		array(),
		LBT_THEME_VERSION,
	);

	wp_enqueue_script(
		'lbt-global',
		LBT_THEME_URL . 'assets/js/global.js',
		array( 'jquery' ),
		LBT_THEME_VERSION,
		true
	);

	wp_localize_script(
		'lbt-global',
		'lbtGlobal',
		array(
			'isUserLoggedIn' => is_user_logged_in(),
		)
	);

	wp_enqueue_script(
		'lbt-modal-coming-soon',
		LBT_THEME_URL . 'assets/js/modal-coming-soon.js',
		array( 'jquery' ),
		LBT_THEME_VERSION,
		true
	);

	wp_localize_script(
		'lbt-modal-coming-soon',
		'modalComingSoon',
		array(
			'confirmSubscriptionScreen' => is_page_template( 'templates/tmp-confirm-subscription.php' ),
		)
	);

	wp_register_script(
		'lbt-lookbook',
		LBT_THEME_URL . 'assets/js/lookbook.js',
		array( 'jquery' ),
		LBT_THEME_VERSION,
		true
	);

	wp_localize_script(
		'lbt-lookbook',
		'lbtLookbook',
		array(
			'ajaxUrl'              => esc_url( admin_url( 'admin-ajax.php' ) ),
			'toggleLookbookAction' => 'lbt_lookbook_toggle',
			'toggleLookbookNonce'  => wp_create_nonce( 'lbt-look-book-toggle-nonce' ),
			'isUserLoggedIn'       => is_user_logged_in(),
			'isSinglePost'         => is_singular( 'post' ),
			'isLookbookPage'       => is_page_template( 'templates/tmp-lookbook.php' ),
		)
	);

	wp_register_script(
		'lbt-offer',
		LBT_THEME_URL . 'assets/js/offer.js',
		array( 'jquery' ),
		LBT_THEME_VERSION,
		true
	);

	wp_localize_script(
		'lbt-offer',
		'lbtOffer',
		array(
			'ajaxUrl'              => esc_url( admin_url( 'admin-ajax.php' ) ),
			'toggleOfferAction'    => 'lbt_offer_toggle',
			'toggleOfferNonce'     => wp_create_nonce( 'lbt-offer-toggle-nonce' ),
			'isUserLoggedIn'       => is_user_logged_in(),
			'isSinglePost'         => is_singular( 'post' ),
			'isOfferPage'          => is_page_template( 'templates/tmp-my-offers.php' ),
		)
	);

	wp_register_script(
		'lbt-dealer-cta-request',
		LBT_THEME_URL . 'assets/js/dealer-cta-request.js',
		array( 'jquery' ),
		LBT_THEME_VERSION,
		true
	);

	wp_localize_script(
		'lbt-dealer-cta-request',
		'lbtDealerCTARequest',
		array(
			'ajaxUrl'                 => esc_url( admin_url( 'admin-ajax.php' ) ),
			'sendCTARequestAction'    => 'lbt_send_dealer_cta_request',
			'sendCTARequestNonce'     => wp_create_nonce( 'lbt-send-dealer-cta-request-nonce' ),
			'checkAvailabilityAction' => 'lbt_check_availability',
			'checkAvailabilityNonce'  => wp_create_nonce( 'lbt-check-availability-nonce' ),
			'isUserLoggedIn'          => is_user_logged_in(),
			'isSavedBrandsPage'       => is_page_template( 'templates/tmp-saved-brands.php'),
		)
	);

	wp_register_script(
		'lbt-closest-dealer',
		LBT_THEME_URL . 'assets/js/closest-dealer.js',
		array( 'jquery' ),
		LBT_THEME_VERSION,
		true
	);

	if ( is_category() ) {
		$brand_id = ! empty( $_GET['brand_id'] ) && absint( $_GET['brand_id'] ) ? (int) $_GET['brand_id'] : get_query_var( 'cat' );
	} elseif ( is_singular( 'post' ) ) {
		$brand_id = get_the_category()[0]->term_id;
	}

	wp_localize_script(
		'lbt-closest-dealer',
		'lbtClosestDealer',
		array(
			'ajaxUrl'                 => esc_url( admin_url( 'admin-ajax.php' ) ),
			'brandId'                 => $brand_id,
			'postId'                  => is_singular( 'post' ) ? get_the_ID() : false,
			'closestDealerAction'     => 'lbt_get_closest_brand_dealer',
			'closestDealerNonce'      => wp_create_nonce( 'lbt-get-closes-brand-dealer-nonce' ),
			'saveFavoriteBrandAction' => 'lbt_toggle_favorite_brand',
			'saveFavoriteBrandNonce'  => wp_create_nonce( 'lbt-toggle-fav-brand-nonce' ),
			'isUserLoggedIn'          => is_user_logged_in(),
			'isSinglePost'            => is_singular( 'post' ),
			'isSavedBrandsPage'       => is_page_template( 'templates/tmp-saved-brands.php'),
			'isCategory'              => is_category(),
		)
	);

	if ( is_category() ) {
		wp_enqueue_style( 'lbt-select2' );
		wp_enqueue_script(
			'lbt-category',
			LBT_THEME_URL . 'assets/js/category.js',
			array( 'jquery', 'lbt-select2' ),
			LBT_THEME_VERSION,
			true
		);

		wp_localize_script(
			'lbt-category',
			'lbtCategory',
			array(
				'ajaxUrl'                     => esc_url( admin_url( 'admin-ajax.php' ) ),
				'brandId'                     => $brand_id,
				'postId'                      => is_singular( 'post' ) ? get_the_ID() : false,
				'isSinglePost'                => is_singular( 'post' ),
				'brandDealerPromotionsAction' => 'lbt_get_brand_dealer_promotions',
				'brandDealerPromotionsNonce'  => wp_create_nonce( 'lbt-get-brand-dealer-promotions-nonce' ),
			)
		);

		wp_enqueue_script(
			'lbt-category-closest-dealer',
			LBT_THEME_URL . 'assets/js/category-closest-dealer.js',
			array(),
			LBT_THEME_VERSION,
			false
		);

		wp_localize_script(
			'lbt-category-closest-dealer',
			'lbtCategoryClosestDealer',
			array(
				'ajaxUrl'                     => esc_url( admin_url( 'admin-ajax.php' ) ),
				'brandId'                     => $brand_id,
				'postId'                      => is_singular( 'post' ) ? get_the_ID() : false,
				'isSinglePost'                => is_singular( 'post' ),
				'brandDealerPromotionsAction' => 'lbt_get_brand_dealer_promotions',
				'brandDealerPromotionsNonce'  => wp_create_nonce( 'lbt-get-brand-dealer-promotions-nonce' ),
			)
		);

		wp_enqueue_script( 'lbt-dealer-cta-request' );
	}

	if ( is_archive() || is_front_page() || is_singular( 'post' ) || is_page_template( 'templates/tmp-lookbook.php' ) || is_page_template( 'templates/tmp-my-offers.php' ) ) {
		wp_enqueue_script( 'lbt-lookbook' );
	}

	if ( is_page_template( 'templates/tmp-saved-brands.php' ) || is_singular( 'post' ) || is_category() ) {
		wp_enqueue_script( 'lbt-closest-dealer' );
		wp_enqueue_script( 'lbt-dealer-cta-request' );
	}

	if ( is_singular( 'post' ) ) {
		wp_enqueue_script( 'lbt-offer' );

		wp_enqueue_script(
			'lbt-single-post',
			LBT_THEME_URL . 'assets/js/single-post.js',
			array( 'jquery' ),
			LBT_THEME_VERSION,
			true
		);
	}

	remove_action('wp_enqueue_scripts', 'novus_scripts', 10);

	wp_enqueue_style('Barlow-style', 'https://fonts.googleapis.com/css2?family=Barlow:wght@300;400;500;600;700;800;900&display=swap');
	wp_enqueue_style('Merriweather-style', 'https://fonts.googleapis.com/css2?family=Merriweather:wght@300;400;700;900&display=swap');
	wp_enqueue_style('novus-style', get_template_directory_uri() . '/assets/css/main.min.css');
	wp_enqueue_style('novus-google-font', get_stylesheet_directory_uri() . '/assets/css/load-fonts.css');
	if ( is_singular( 'post' ) ) {
		wp_enqueue_script('semantic_sticky', get_stylesheet_directory_uri() . '/assets/js/semantic_sticky.min.js', array('jquery'), false, true);
		wp_enqueue_script('datatables-cont', get_stylesheet_directory_uri() . '/assets/js/wpr-datatables.js', array(), time(), true);
	}
	if (is_user_logged_in() && !is_admin()) {
		wp_enqueue_style('dashicons-bar', get_site_url() . '/wp-includes/css/dashicons.min.css');
		wp_enqueue_style('admin-bar-bar', get_site_url() . '/wp-includes/css/admin-bar.min.css');
		wp_enqueue_script('admin-bar-bar', get_site_url() . '/wp-includes/js/admin-bar.min.js', array(), '4.8.3', true);
	}

	if ( is_front_page() ) {
		wp_enqueue_style('owl.carousel-style', get_stylesheet_directory_uri() . '/assets/css/owl.carousel.css');
		wp_enqueue_style('owl.theme-style', get_stylesheet_directory_uri() . '/assets/css/owl.theme.css');
		wp_enqueue_script('owl.carousel-js', get_stylesheet_directory_uri() . '/assets/js/owl.carousel.js', array(), '20120206', true);
		wp_enqueue_script('lbt-homepage', get_stylesheet_directory_uri() . '/assets/js/homepage.js', array(), LBT_THEME_VERSION, true);
	}

	wp_enqueue_script('novus-navigation', get_stylesheet_directory_uri() . '/assets/js/navigation.js', array(), '20120206', true);
	wp_enqueue_script('novus-navigation-secondary', get_stylesheet_directory_uri() . '/assets/js/navigation-secondary.js', array(), '20120206', true);

	wp_enqueue_script('novus-skip-link-focus-fix', get_template_directory_uri() . '/assets/js/skip-link-focus-fix.js', array(), '20130115', true);
	wp_enqueue_script('jquery-cookie-theme', get_stylesheet_directory_uri() . '/assets/js/js.cookie.js', array(), '2.1.4', true);

	if ( ! is_single() ) {
		wp_enqueue_script('novus-script-main', get_stylesheet_directory_uri() . '/assets/js/all.min.js', array('jquery'), false, true);
	}

	if ( is_singular() && comments_open() && get_option('thread_comments') ) {
		wp_enqueue_script('comment-reply');
	}

	wp_deregister_script('rfwbs-superslides');
	wp_enqueue_script('wpr-rfwbs-superslides', get_stylesheet_directory_uri() . '/assets/js/jquery.superslides.js', array(), '0.6.3', true);

	if (!wp_style_is('font-awesome', 'enqueued')) {
		wp_enqueue_style('font-awesome', get_stylesheet_directory_uri() . '/assets/css/font-awesome.min.css', array(), '4.7.0');
	}

	wp_enqueue_script( 'lbt-ontraport', 'https://forms.ontraport.com/v2.4/include/formEditor/genjs-v3.php?html=false&uid=p2c137325f51' );
}

add_action('wp_enqueue_scripts', 'lbt_scripts_override');

add_action('widgets_init', 'wpr_register_widgets_area');

/**
 * Register Widget areas
 */
function wpr_register_widgets_area() {
	register_sidebar(array(
		'name' => 'Footer 1 column',
		'id' => 'footer_column_1',
		'before_widget' => '<div class="wpr-footer-widget">',
		'after_widget' => '</div>',
		'before_title' => '<h2 class="wpr-footer-widget-title">',
		'after_title' => '</h2>',
	));

	register_sidebar(array(
		'name' => 'Footer 2 column',
		'id' => 'footer_column_2',
		'before_widget' => '<div class="wpr-footer-widget">',
		'after_widget' => '</div>',
		'before_title' => '<h2 class="wpr-footer-widget-title">',
		'after_title' => '</h2>',
	));

	register_sidebar(array(
		'name' => 'Footer 3 column',
		'id' => 'footer_column_3',
		'before_widget' => '<div class="wpr-footer-widget">',
		'after_widget' => '</div>',
		'before_title' => '<h2 class="wpr-footer-widget-title">',
		'after_title' => '</h2>',
	));

	register_sidebar(array(
		'name' => 'Footer 4 column',
		'id' => 'footer_column_4',
		'before_widget' => '<div class="wpr-footer-widget">',
		'after_widget' => '</div>',
		'before_title' => '<h2 class="wpr-footer-widget-title">',
		'after_title' => '</h2>',
	));
}

/**
 * Sets up theme defaults and registers support for various WordPress features.
 */
function novus_child_setup() {
	register_nav_menus(array(
		'secondary' => __( 'Secondary Menu', 'lbt' ),
	));
}

add_action('after_setup_theme', 'novus_child_setup');

/**
 * Add masonry class to Lookbook page
 */
function lbt_lookbook() {
	if (is_page_template('tpl-lookbook.php')) {
		remove_filter('body_class', 'novus_body_class');
		add_filter('body_class', 'lbt_body_class');
	}
}
add_action('template_redirect', 'lbt_lookbook');

function lbt_body_class($classes) {
	$classes[] = 'layout-masonry';
	return $classes;
}

/**
 * Add additional contact methods
 */
function lbt_contactmethods($contactmethods) {
	$contactmethods['street'] = __('Street', 'novus');
	$contactmethods['city'] = __('City', 'novus');
	$contactmethods['state'] = __('State', 'novus');
	$contactmethods['zip_code'] = __('Zip code', 'novus');
	$contactmethods['phone'] = __('Phone', 'novus');

	return $contactmethods;
}

add_filter('user_contactmethods', 'lbt_contactmethods', 10, 1);

/**
 * Prints HTML with meta information for the categories, tags and comments.
 */
function novus_entry_footer($show_tags = true) {

	// Hide category and tag text for pages.
	if ('post' == get_post_type() && true === $show_tags) {
		/* translators: used between list items, there is a space after the comma */
		// $categories_list = get_the_category_list( __( ', ', 'novus' ) );
		// if ( $categories_list && novus_categorized_blog() ) {
		//  printf( '<span class="cat-links">' . __( 'Posted in %1$s', 'novus' ) . '</span>', $categories_list );
		// }

		/* translators: used between list items, there is a space after the comma */
		$tags_list = get_the_tag_list('', __(', ', 'novus'));
		if ($tags_list) {
			printf('<span class="tags-links">%1$s</span>', $tags_list);
		}
	}

	if (!is_single() && !post_password_required() && (comments_open() || get_comments_number())) {
		echo '<span class="comments-link">';
		comments_popup_link(__('Comment', 'novus'), __('1 Comment', 'novus'), __('% Comments', 'novus'));
		echo '</span>';
	}
}

/**
 * Get movie ID from Youtube url
 *
 * @param $url
 *
 * @return bool
 */
function getYoutubeIdFromUrl($url) {
	$parts = parse_url($url);
	if (isset($parts['query'])) {
		parse_str($parts['query'], $qs);
		if (isset($qs['v'])) {
			return $qs['v'];
		} else if (isset($qs['vi'])) {
			return $qs['vi'];
		}
	}
	if (isset($parts['path'])) {
		$path = explode('/', trim($parts['path'], '/'));

		return $path[count($path) - 1];
	}

	return false;
}


/**
 * Get the post's external featured image.
 *
 * @TODO: Remove this
 *
 * @param  int $post_id
 *
 * @return string
 */
function wpr_get_external_image($post_id) {
	return '';
}

/**
 * Remote check file existence
 *
 * Checking the file remotely without downloading it,
 * the HEAD request is faster in this case.
 *
 * @param string $remote_file_path remote file address, an valid URL
 * @return bool true means the file has been found
 */

function wpr_remote_check_file($remote_file_path) {
	if (!filter_var($remote_file_path, FILTER_VALIDATE_URL) === false) {
		stream_context_set_default(
			array(
				'http' => array(
					'method' => 'HEAD',
				),
			)
		);
		$headers = get_headers($remote_file_path, 1);
		$file_found = stristr($headers[0], '200'); // Returns all of haystack starting from and including the first occurrence of needle to the end. In our case "200 OK" If everything is ok.
		if ($file_found) {
			return true;
		}

		return false;
	} else {
		return false;
	}
}

/**
 * Return Category ID's
 *
 * TODO: This may or may not be needed.
 *
 * @param $posts
 *
 * @return array
 */
function wpr_obtain_category_id($posts) {
	$category_id = array();
	if (is_array($posts) && !empty($posts)) {
		foreach ($posts as $post) {
			$post_categories = get_the_category($post);

			if (!empty($post_categories)) {
				foreach ($post_categories as $term) {
					array_push($category_id, $term->term_id);
				}
			}
		}
	}

	return $category_id;
}

/**
 * Add hotjar script
 *
 * TODO: Check what those brands things is
 */
function wpr_hotjar_script() {
	?>
	<!-- Hotjar Tracking Code for www.luxurybuystoday.com -->
	<script>
		(function (h, o, t, j, a, r) {
			h.hj = h.hj || function () {
				(h.hj.q = h.hj.q || []).push(arguments)
			};
			h._hjSettings = {
				hjid: 506996,
				hjsv: 5
			};
			a = o.getElementsByTagName('head')[0];
			r = o.createElement('script');
			r.async = 1;
			r.src = t + h._hjSettings.hjid + j + h._hjSettings.hjsv;
			a.appendChild(r);
		})(window, document, '//static.hotjar.com/c/hotjar-', '.js?sv=');
	</script>
	<?php
}
add_action('wp_head', 'wpr_hotjar_script');

/**
 * Fetch random custom title
 *
 * @param int $category_id The category id.
 * @param int $post_id The Post id.
 *
 * @return string
 */
function wpr_fetch_random_title( $category_id = 0, $post_id = 0 ) {
	$post_title = '';
	$tax_query  = array();
	$meta_query = array();

	if ( ! empty( $category_id ) ) {
		$tax_query = array(
			array(
				'taxonomy' => 'category',
				'field'    => 'term_id',
				'terms'    => $category_id,
			),
		);
	}

	if ( $post_id && has_tag( \LBT\Constants::PROMOTIONS_TAG, $post_id ) ) {
		$meta_query = array(
			array(
				'key'     => 'is_promotion_title',
				'compare' => 'EXISTS',
 			)
		);
	}

	$args = array(
		'post_type'      => 'wpr_custom_title',
		'posts_per_page' => 1,
		'orderby'        => 'rand',
		'tax_query'      => $tax_query,
		'meta_query'     => $meta_query,
	);

	$query = new WP_Query( $args );

	if ( $query->have_posts() ) {
		$post_title = $query->posts[0]->post_title;
	}

	return $post_title;
}

/**
 * Return parent categories
 *
 * @param $post_id
 *
 * @return array
 */
function wpr_fetch_parent_category($post_id) {
	$categories = array();
	if (!empty($post_id)) {
		$term_list = wp_get_post_terms($post_id, 'category');

		if (!empty($term_list)) {
			foreach ($term_list as $term) {
				if ($term->parent == 0) {
					array_push($categories, $term->term_id);
				} else {
					array_push($categories, $term->parent);
				}
			}
		}
	}

	return $categories;
}

add_action('added_post_meta', 'wpr_twitter_video_gif_get', 20, 4);
#add_action( 'updated_post_meta', 'wpr_twitter_video_gif_get', 20, 4 );
/**
 * Get Twitter embed video & add featured image to Automatic posts
 *
 * @param $meta_id
 * @param $object_id
 * @param $meta_key
 * @param $_meta_value
 */

function wpr_twitter_video_gif_get($meta_id, $object_id, $meta_key, $_meta_value) {

	// Set WP-Automatic & E-mail Parsers articles on Draft status till the title & comments are generated
	if ('original_link' == $meta_key || 'wpr_email_source' == $meta_key) {
		wpr_set_post_status($object_id, 'draft');
	}

	// ****************** Start generating *************************
	// Edgar cats
	$edgar_cats = array('36', '37', '38', '39');
	// If comes from third party
	if ('original_link' == $meta_key) {
		// Get post object
		$post = get_post($object_id);
		$content = $post->post_content;

		// Extract Youtube Title & Description
		preg_match_all('((?:www\.)?(?:youtube(?:-nocookie)?\.com\/(?:v\/|watch\?v=|embed\/)|youtu\.be\/)([a-zA-Z0-9?&=_-]*))', $_meta_value, $urls);
		$urls = array_filter($urls);
		if (!empty($urls)) {
			$doc = new DOMDocument();
			$doc->preserveWhiteSpace = false;
			$doc->loadHTMLFile($_meta_value);

			$title_div = $doc->getElementById('eow-title');
			$title = $title_div->nodeValue;

			if (!empty($title)) {
				// Save video title to meta.
				update_post_meta($object_id, 'wpr_remote_video_title', utf8_decode($title));
			}

			$description_div = $doc->getElementById('eow-description');
			$description = $description_div->nodeValue;

			if (!empty($description)) {
				// Save video description to meta.
				update_post_meta($object_id, 'wpr_remote_video_description', utf8_decode($description));
			}
		}

		preg_match('/(<img .*?>)/', $content, $img_tag);
		preg_match('/<a[^>]+class="wpr_article_source"[^>]+href=([\'"])(?<href>.+?)\1[^>]*>(.*?)/i', $content, $result);
		preg_match('/(twitter.com)/', $_meta_value, $twitter_url);

		// Update instagram thumbnail URL
		preg_match('/(instagram.com)/', $_meta_value, $instagram_url);
		if ($instagram_url) {
			$get_instagram = 'https://api.instagram.com/oembed/?url=' . $_meta_value;
			$remote_instagram = wp_remote_get($get_instagram);
			$remote_instagram_response_code = wp_remote_retrieve_response_code($remote_instagram);
			if (200 === (int) $remote_instagram_response_code) {
				$instagram_body = wp_remote_retrieve_body($remote_instagram);
				$parse_data = json_decode($instagram_body, true);

				if (!empty($parse_data)) {
					$thumbnail_url = $parse_data['thumbnail_url'];
					update_post_meta($object_id, 'wpr_remote_featured_img', esc_url($thumbnail_url));

					libxml_use_internal_errors(true);
					$wpr_html_dom = new DOMDocument();
					$wpr_html_dom->encoding = 'utf-8';
					$wpr_html_dom->loadHTML(mb_convert_encoding($content, 'HTML-ENTITIES', 'UTF-8'));
					if ($wpr_html_dom) {
						$wpr_xpath = new DOMXPath($wpr_html_dom);
						$all_links_list = $wpr_xpath->query("//img");
						if (!empty($all_links_list)) {

							foreach ($all_links_list as $link_element) {
								$link_element->removeAttribute('src');
								$link_element->setAttribute("src", (!empty(esc_url($thumbnail_url))) ? esc_url($thumbnail_url) : '');
							}

							$content = $wpr_html_dom->saveHTML();

							global $wpdb;
							$wpdb->update($wpdb->posts, array(
								'post_content' => $content,
							), array('ID' => $object_id));
						}
					}
				}
			}
		}

		// Fetch Google Plus OG Image
		preg_match('/(plus.google.com)/', $_meta_value, $google_plus_url);
		if ($google_plus_url) {
			$remote_google_plus = wp_remote_get($_meta_value);
			$google_plus_body = wp_remote_retrieve_body($remote_google_plus);
			libxml_use_internal_errors(true);
			$doc = new DomDocument();
			$doc->loadHTML($google_plus_body);
			$xpath = new DOMXPath($doc);
			$query = '//*/meta[@property="og:image"]';
			$metas = $xpath->query($query);
			if (!empty($metas)) {
				foreach ($metas as $meta) {
					$content = $meta->getAttribute('content');
					update_post_meta($object_id, 'wpr_remote_featured_img', esc_url($content));
					update_post_meta($object_id, 'wpr_social_image', esc_url($content));
				}
			}
		}

		$external_image = get_post_meta($object_id, 'wpr_remote_featured_img', true);

		// This is for other posts with content images but with no feature image set
		if (!empty($img_tag)) {
			if (has_post_thumbnail($object_id)) {

			} else {
				preg_match('/< *img[^>]*src *= *["\']?([^"\']*)/i', $content, $img_src);
				if (!empty($img_src)) {
					$image_url = esc_url_raw($img_src[1]);

					update_post_meta($post->ID, 'wpr_remote_featured_img', $image_url);
				}
			}
		}
		// External image
		if ($external_image) {
			if (has_post_thumbnail($object_id)) {

			} else {
				$image_url = esc_url_raw($external_image);
				update_post_meta($post->ID, 'wpr_remote_featured_img', $image_url);
			}
		}

		// This is for Twitter posts
		if (empty($img_tag) && !empty($twitter_url)) {
			$twitter_id = explode('/', $_meta_value);
			$twitter_id = end($twitter_id);

			$remote_twitter = wp_remote_get($_meta_value);
			$remote_twitter_response_code = wp_remote_retrieve_response_code($remote_twitter);
			// Response type = 200
			if (200 === (int) $remote_twitter_response_code) {
				$twitter_body = wp_remote_retrieve_body($remote_twitter);
				libxml_use_internal_errors(true);
				$wpr_html_dom = new DOMDocument();
				$wpr_html_dom->loadHTML($twitter_body);

				if ($wpr_html_dom) {
					$wpr_xpath = new DOMXPath($wpr_html_dom);
					$wpr_xpath->registerNamespace("php", "http://php.net/xpath");
					$wpr_xpath->registerPHPFunctions("strtolower");

					$is_movie = $wpr_xpath->query('/html/body//div[contains((php:functionString("strtolower",@class)),"playablemedia playablemedia--video")]');
					if (!is_null($is_movie) && $is_movie->length > 0) {
						//Add embed code if not exists
						$twitter_video = get_post_meta($post->ID, 'wpr_twitter_video', true);
						if (empty($twitter_video)) {
							$get_remote = wp_remote_get('https://publish.twitter.com/oembed?widget_type=video&maxwidth=800&url=https%3A%2F%2Ftwitter.com%2FInterior%2Fstatus%2F' . $twitter_id);
							$response_code = wp_remote_retrieve_response_code($get_remote);
							// Response type = 200
							if (200 === (int) $response_code) {
								$twitter_body = wp_remote_retrieve_body($get_remote);
								$parse_json = json_decode($twitter_body, true);
								if (!empty($parse_json) && $parse_json['html']) {
									update_post_meta($post->ID, 'wpr_twitter_video', $parse_json['html']);
								}
							} else {
								update_post_meta($post->ID, 'wpr_twitter_request_code', esc_attr($response_code));
							}
						}

						// Get thumbnail
						$twitter_video_thumbnail = get_the_post_thumbnail($post->ID);
						if (empty($twitter_video_thumbnail)) {
							$backgroun_image = $wpr_xpath->query('/html/body//div[contains(@class,"PlayableMedia-player")]');
							if (!is_null($backgroun_image)) {
								foreach ($backgroun_image as $iframe) {
									$current_classes = $iframe->getAttribute('style');
									if ($current_classes) {
										preg_match('/background-image.*?url\((.*?)\)/mi', $current_classes, $matches);
										if (!empty($matches[1])) {
											$image_url = esc_url_raw(str_replace("'", "", $matches[1]));
											update_post_meta($post->ID, 'wpr_remote_featured_img', $image_url);
										}
									}
								}
							}
						}
					}

					$is_gif = $wpr_xpath->query('/html/body//div[contains((php:functionString("strtolower",@class)),"playablemedia playablemedia--gif")]');
					if (!is_null($is_gif) && $is_gif->length > 0) {
						// Get thumbnail
						$backgroun_image = $wpr_xpath->query('/html/body//div[contains(@class,"PlayableMedia-player")]');
						if (!is_null($backgroun_image)) {
							foreach ($backgroun_image as $iframe) {
								$current_classes = $iframe->getAttribute('style');
								if ($current_classes) {
									preg_match('/background-image.*?url\((.*?)\)/mi', $current_classes, $matches);
									if (!empty($matches[1])) {
										$image_url = esc_url_raw(str_replace("'", "", $matches[1]));
										update_post_meta($post->ID, 'wpr_remote_featured_img', $image_url);

										// Save video URL
										$movie_code = explode('/', $image_url);
										$movie_code = end($movie_code);
										$movie_code = str_replace('.jpg', '.mp4', $movie_code);
										$movie_url = "https://video.twimg.com/tweet_video/{$movie_code}";

										update_post_meta($post->ID, 'wpr_twitter_video_mp4', esc_url_raw($movie_url));
									}
								}
							}
						}
					}

					$is_card = $wpr_xpath->query('/html/body//div[contains((php:functionString("strtolower",@class)),"card2 js-media-container")]');
					if (!is_null($is_card) && $is_card->length > 0) {
						$tweet_site = wp_remote_get(esc_url_raw('https://twitter.com/i/cards/tfw/v1/' . $twitter_id));
						$tweet_site_code = wp_remote_retrieve_response_code($tweet_site);
						if (200 === (int) $tweet_site_code) {
							// Retrieve full post page HTML.
							$tweet_site_body = wp_remote_retrieve_body($tweet_site);

							// Fetch image Open Graph meta tag.
							preg_match('/< *img[^>]*src *= *["\']?([^"\']*)/i', $tweet_site_body, $tweet_card_image);

							if (!empty($tweet_card_image[1])) {
								$url = esc_url($tweet_card_image[1]);
								update_post_meta($post->ID, 'wpr_remote_featured_img', $url);
								update_post_meta($post->ID, 'wpr_remote_twitter_cards', 1);
								update_post_meta($post->ID, 'wpr_social_image', $url);
							}
						}
					}
				}
			} else {
				update_post_meta($post->ID, 'wpr_twitter_image_request_code', esc_attr($remote_twitter_response_code));
			}

			// Extract tweet image from 3rd party site.
			preg_match('/(https:\/\/(t\.co|twitter\.com\/[^\/]*\/status)\/[^"\s<]*)/', $content, $retweet_link);
			preg_match('/(https:\/\/w{3}?\.youtube\.com\/watch\?v=([^\s<"]*))/', $content, $youtube_link);
			if (empty(get_post_meta($object_id, 'wpr_remote_featured_img'))) {
				if (!empty($retweet_link[1])) {
					// Check if site contains og image.
					$tweet_site = wp_remote_get(esc_url_raw($retweet_link[1]));
					$tweet_site_code = wp_remote_retrieve_response_code($tweet_site);
					if (200 === (int) $tweet_site_code) {
						// Retrieve full post page HTML.
						$tweet_site_body = wp_remote_retrieve_body($tweet_site);

						// Fetch image Open Graph meta tag.
						preg_match('/<[^>]*meta[^>]*property="og:image"[^>]*content="([^"]*)"[^>]*>/i', $tweet_site_body, $tweet_site_og_image);

						if (!empty($tweet_site_og_image[1])) {
							$tweet_media_url = wpr_save_media_from_url($tweet_site_og_image[1]);

							if ($tweet_media_url) {
								// Set featured image.
								update_post_meta($object_id, 'wpr_remote_featured_img', $tweet_media_url);
							}
						}
					}
				}

				if (!empty($youtube_link[1]) && !empty($youtube_link[2])) {
					// Extract YouTube video thumbnail.
					$youtube_thumb_url = 'http://img.youtube.com/vi/' . $youtube_link[2] . '/maxresdefault.jpg';
					$youtube_media_url = wpr_save_media_from_url($youtube_thumb_url);

					if ($youtube_media_url) {
						// Set featured image.
						update_post_meta($object_id, 'wpr_remote_featured_img', $youtube_media_url);
					}
				}
			}
		}

		// Extract & save original article source
		if (in_category($edgar_cats, $object_id)) {

			// Get Original Article Source
//			wpr_get_edgar_article_source( $object_id );
//
			// Generate spin content
			wpr_generate_edgar_wordai_spin($object_id);
		}
	}

	$is_broyhill = get_post_meta($object_id, 'wpr_broyhill_article', true);

	if ('wpr_non_ed_article' == $meta_key && !$is_broyhill) {
		$term_list = wpr_fetch_parent_category($object_id);
		$random_title = wpr_fetch_random_title($term_list);
		$current_title = get_the_title($object_id);

		update_post_meta($object_id, 'original_title', esc_attr($current_title));

		$current_content = apply_filters('the_content', get_post_field('post_content', $object_id));

		// Remove URL's from title
		$regex = "@(https?://([-\w\.]+[-\w])+(:\d+)?(/([\w/_\.#-]*(\?\S+)?[^\.\s])?).*$)@";
		$current_title = preg_replace($regex, ' ', $current_title);

		$the_content = "<b>$current_title</b><br />$current_content";

		if (!empty($random_title)) {
			global $wpdb;
			$wpdb->update($wpdb->posts, array(
				'post_title' => esc_attr($random_title),
				'post_content' => $the_content,
			), array('ID' => $object_id));

			$categories = get_the_category($object_id);
			$category = $categories[0];
			$cat_id = $category->term_id;

			if ($category->parent > 0) {
				$cat_id = $category->parent;
			}

			update_post_meta($object_id, 'wpr_title_changed', sprintf('Random title from our custom title database for %s', get_cat_name($cat_id)));
		}
	}

	// Spin the content
	if (!in_category($edgar_cats, $object_id) && ('original_link' == $meta_key || 'wpr_email_source' == $meta_key)) {
		// Generate random comment
		wpr_generate_random_article_content($object_id);
	}

	// Revers Broyhill article title
	if ('wpr_broyhill_article' == $meta_key) {
		$current_content = apply_filters('the_content', get_post_field('post_content', $object_id));
		$current_content = str_replace(array("\n", "\r"), ' ', $current_content);
		// Extract the title from content
		preg_match('/<b>(.*?):/', $current_content, $thetitle);
		if (!empty($thetitle) && strpos($thetitle[0], 'http') === false) {
			if ($thetitle[1]) {
				$original_title = $thetitle[1];
			} else {
				$original_title = $thetitle[0];
			}
		}
		$original_title = str_replace(array("BROYHILL COMMENTS", "BROYHILL COMMENT"), "", $original_title);
		$original_title = wp_strip_all_tags($original_title);

		global $wpdb;

		if (!empty($original_title)) {
			$wpdb->update($wpdb->posts, array(
				'post_title' => esc_attr($original_title),
			), array('ID' => $object_id));

			update_post_meta($object_id, 'wpr_title_changed', 'Original title from the Broyhill News');
		} else {
			$term_list = wpr_fetch_parent_category($object_id);
			$random_title = wpr_fetch_random_title($term_list);

			if (!empty($random_title)) {
				$wpdb->update($wpdb->posts, array(
					'post_title' => esc_attr($random_title),
				), array('ID' => $object_id));
				update_post_meta($object_id, 'wpr_title_changed', 'Custom title from the database because the lookup for the Original title in the Broyhill News failed');
			}
		}

		update_post_meta($object_id, 'wpr_original_title', esc_attr(get_the_title($object_id)));
	}

	// *********************** Stop generation *****************************
	// Set WP-Automatic & E-mail Parsers articles on Publish status after the title & comments are generated
	if ('draft' === get_post_status($object_id) && ('wpr_non_ed_article' == $meta_key || 'wpr_broyhill_article' == $meta_key)) {
//		wpr_set_post_status( $object_id, 'publish' );
	}
}

/**
 * Set post status by wpdb
 *
 * @param $post_id
 * @param $status
 */
function wpr_set_post_status($post_id, $status) {
	$post = get_post($post_id);

	if (!$post) {
		return;
	}

	$wp_post_statuses = get_post_statuses();
	if (!array_key_exists($status, $wp_post_statuses)) {
		return;
	}

	if ('publish' === $status) {
		wpr_add_post_meta('post', absint($post_id), 'wpr_triggered_as_draft', 1);
	}

	if (wpr_offer_title_exists($post) && 'publish' === $status) {
		return;
	}

	global $wpdb;
	$wpdb->update($wpdb->posts, array(
		'post_status' => $status,
	), array('ID' => $post_id));

	if ('publish' === $status) {
		if (function_exists('wpr_add_post_meta')) {
			wpr_add_post_meta('post', absint($post_id), 'wpr_status_changed', 1);
		}
	}

	// Clean cache
	clean_post_cache($post_id);

	$old_status = $post->post_status;
	$post->post_status = $status;
	wp_transition_post_status($status, $old_status, $post);
}

/**
 * Add schedule to Publish posts
 */
if (!wp_next_scheduled('wpr_publish_draft_posts')) {
	wp_schedule_event(time(), 'hourly', 'wpr_publish_draft_posts');
}

add_action('wpr_publish_draft_posts', 'wpr_execute_publish_draft_posts');

/**
 * Change post status from Draft to Publish
 */
function wpr_execute_publish_draft_posts() {
	global $wpdb;
	$search_draft = $wpdb->get_results(
		"
         SELECT ID
         FROM {$wpdb->prefix}posts
         INNER JOIN {$wpdb->prefix}postmeta m1
         ON ( {$wpdb->prefix}posts.ID = m1.post_id )
         WHERE
              {$wpdb->prefix}posts.post_type = 'post'
         AND {$wpdb->prefix}posts.post_status = 'draft'
         AND  $wpdb->posts.ID NOT IN
              ( SELECT DISTINCT post_id FROM $wpdb->postmeta WHERE meta_key = 'wpr_status_changed' )
         AND  $wpdb->posts.ID NOT IN
              ( SELECT DISTINCT post_id FROM $wpdb->postmeta WHERE meta_key = 'wpr_triggered_as_draft' )
         GROUP BY {$wpdb->prefix}posts.ID
         ORDER BY {$wpdb->prefix}posts.post_date
         DESC
         LIMIT 40;
         "
	);

	if ($search_draft) {
		foreach ($search_draft as $post) {
			wpr_set_post_status($post->ID, 'publish');
		}
	}
}

/**
 * Save media from a URL to the media library.
 *
 * @param  string $url The URL from which to download the media.
 *
 * @return mixed False on error, or the new media URL string.
 */
function wpr_save_media_from_url($url) {
	$url = preg_replace(array(
		'/\?.*$/', // Remove query args.
		'/:[^\/]*$/', // Remove :<property> values, such as ":large".
	), '', $url);

	// Sanitize & validate URL.
	$url = esc_url_raw($url, array(
		'http',
		'https',
	));

	if (empty($url)) {
		return false;
	}

	require_once ABSPATH . 'wp-admin/includes/file.php';

	$temp_file = download_url($url, 5);

	if (is_wp_error($temp_file)) {
		return false;
	}

	$filetype = wp_check_filetype(basename($temp_file));

	$file = array(
		'name' => basename($url),
		'type' => $filetype['type'],
		'tmp_name' => $temp_file,
		'error' => 0,
		'size' => filesize($temp_file),
	);

	$overrides = array(
		'test_form' => false,
		'test_size' => true,
	);

	// Move the temporary file into the uploads directory.
	$results = wp_handle_sideload($file, $overrides);

	// Remove the temporary file.
	unlink($temp_file);

	if (empty($results['error'])) {
		return $results['url'];
	} else {
		return false;
	}
}

/**
 * Grab video media from page at given URL.
 *
 * @param string $url URL of the page to grab video from
 *
 * @return string Video shortcode.
 */
function wpr_grab_video_from_url($url) {
	$remote_page = wp_remote_get($url);
	$remote_code = wp_remote_retrieve_response_code($remote_page);

	if (200 === (int) $remote_code) {
		// Retrieve full post page HTML.
		$remote_body = wp_remote_retrieve_body($remote_page);

		// Fetch relevant Open Graph meta tags.
		preg_match_all('~<\s*meta\s*property="og:(image|video|video:[^"]+)"\s*content="([^"]*)~i', $remote_body, $og_meta);

		if (!empty($og_meta[1]) && (in_array('video', $og_meta[1]) || in_array('video:url', $og_meta[1]))) {
			// Post contains video meta tag.
			$og_meta_data = array_combine($og_meta[1], $og_meta[2]);

			$og_meta_video_url = !empty($og_meta_data['video']) ? $og_meta_data['video'] : $og_meta_data['video:url'];

			// Remove query args from URL.
			$og_meta_video_url = preg_replace('/\?.*$/', '', $og_meta_video_url);

			// Save video and poster to media library.
			$video_url = wpr_save_media_from_url($og_meta_video_url);
			$poster_url = wpr_save_media_from_url($og_meta_data['image']);

			if (!$video_url || !$poster_url) {
				return;
			}

			return "[video width='{$og_meta_data['video:width']}' height='{$og_meta_data['video:height']}' poster='{$poster_url}' src='{$video_url}']";
		}
	}
}

/**
 * Add YouTube video description inside the content, if available.
 *
 * @param  string $content
 *
 * @return string
 */
function wpr_display_youtube_description($content) {
	if (!is_single()) {
		return $content;
	}

	$post_id = get_the_id();
	$description = get_post_meta($post_id, 'wpr_remote_video_description', true);

	if (empty($description)) {
		return $content;
	}

	// Add description in content after the video iframe.
	return preg_replace('/<\/iframe>/', '</iframe><p>' . $description . '</p>', $content, 1);
}

add_filter('the_content', 'wpr_display_youtube_description', 20);

/**
 * Handle Instagram post imports.
 *
 * @param $meta_id
 * @param $object_id
 * @param $meta_key
 * @param $_meta_value
 */
function wpr_handle_automatic_video_post($meta_id, $object_id, $meta_key, $_meta_value) {
	if ('original_link' !== $meta_key) {
		return;
	}

	$is_twitter = false; // preg_match( '/(twitter\.com)/', $_meta_value );
	$is_instagram = preg_match('/(instagram\.com)/', $_meta_value);

	$has_video = !empty(get_post_meta($object_id, 'wpr_remote_video_html', true));

	if ($has_video || (!$is_instagram && !$is_twitter)) {
		return;
	}

	if ($is_twitter) {
		$content = get_post_field('post_content', $object_id, 'display');
		preg_match('/(https:\/\/(t\.co|twitter\.com\/[^\/]*\/status)\/[^\s<]*)/', $content, $retweet_link);

		if (empty($retweet_link[1])) {
			return;
		}

		$page_url = $retweet_link[1];
	} else {
		$page_url = $_meta_value;
	}

	$video_sc = wpr_grab_video_from_url($page_url);

	if (!empty($video_sc)) {
		update_post_meta($object_id, 'wpr_remote_video_html', $video_sc);
	}
}

add_action('added_post_meta', 'wpr_handle_automatic_video_post', 30, 4);

/**
 * Replace the first image in a post with a video element, if available.
 *
 * @param  string $content
 *
 * @return string
 */
function wpr_replace_offer_image_with_video($content) {
	if (!is_single()) {
		return $content;
	}

	$post_id = get_the_ID();
	$video_sc = get_post_meta($post_id, 'wpr_remote_video_html', true);

	if (empty($video_sc)) {
		return $content;
	}

	// Replace first image with video in content.
	return preg_replace('/(<a[^>]*>)?<img([^>]*)>(<\/a>)?/i', do_shortcode($video_sc), $content, 1);
}

add_filter('the_content', 'wpr_replace_offer_image_with_video', 99);

/**
 * Replace retweeted link with image from respective tweet or 3rd party site.
 *
 * @param  string $content
 *
 * @return string
 */
function wpr_replace_retweet_link_with_image($content) {
	if (!is_single()) {
		return $content;
	}

	$post_id = get_the_ID();
	$image_url = get_post_meta($post_id, 'wpr_remote_featured_img', true);

	if (strpos($content, $image_url) !== false) {
		// Image is already in content.
		return $content;
	}

	preg_match('/(https:\/\/(t\.co|twitter\.com\/[^\/]*\/status)\/[^"\s<]*)(<\/a>)?/', $content, $retweet_link);
	if (empty($image_url) || empty($retweet_link[1])) {
		return $content;
	}

	return '<div class="wpr-offer-image"><a href="' . $retweet_link[1] . '" target="_blank"><img src="' . $image_url . '"  alt="Featured image"></a></div>' . $content;

	// Add image inside content.
//	return str_replace( $retweet_link[1], '<div class="wpr-offer-image"><a href="' . $retweet_link[1] . '" target="_blank"><img src="' . $image_url . '"  alt="Featured image"></a></div>', $content );
}

add_filter('the_content', 'wpr_replace_retweet_link_with_image', 99);

/**
 * Check if an offer with a similar title exists.
 *
 * @param $post
 *
 * @return bool
 */
function wpr_offer_title_exists($post) {
	// If original title is set, fetch it.
	$original_title = get_post_meta($post->ID, 'original_title', true);

	$category = get_the_category($post->ID);
	$category_id = $category[0]->term_id;

	$title = empty($original_title) ? $post->post_title : $original_title;
	$regex = "@(https?://([-\w\.]+[-\w])+(:\d+)?(/([\w/_\.#-]*(\?\S+)?[^\.\s])?).*$)@";
	$title = preg_replace($regex, ' ', $title);

	// Remove blog name and ellipsis from title.
	$title = preg_replace(array(
		'/^(\S*:)\s?/',
		'/(.{3})$/',
	), '', $title);

	global $wpdb;
	$duplicate_query = $wpdb->get_results(
		$wpdb->prepare("
                SELECT *
                FROM {$wpdb->prefix}posts
                INNER JOIN {$wpdb->prefix}postmeta m1
                 ON ( {$wpdb->prefix}posts.ID = m1.post_id )
                LEFT JOIN  {$wpdb->term_relationships}  as t
                  ON {$wpdb->prefix}posts.ID = t.object_id
                WHERE
                {$wpdb->prefix}posts.post_type = 'post'
                AND {$wpdb->prefix}posts.post_status = 'publish'
                AND (( m1.meta_key = 'original_title' AND m1.meta_value LIKE %s )
                OR ( m1.meta_key = 'original_title' AND m1.meta_value LIKE %s ))
                AND {$wpdb->prefix}posts.ID NOT IN (%d)
                AND {$wpdb->prefix}posts.post_content = '%s'
                AND t.term_taxonomy_id = %d
                GROUP BY {$wpdb->prefix}posts.ID
                ORDER BY {$wpdb->prefix}posts.post_date
                DESC;
            ",
			$wpdb->esc_like(substr($title, 0, 100)) . '%',
			$wpdb->esc_like(preg_replace('/(\s{2,})/', ' ', esc_attr(substr($title, 0, 100)))) . '%',
			absint($post->ID),
			esc_attr($post->post_content),
			absint($category_id)
		)
	);

	if ($duplicate_query) {
		return true;
	} else {
		return false;
	}
}

/**
 * Extract & save original article source
 *
 * @param $post_id
 */
function wpr_get_edgar_article_source($post_id) {
	$edgar_url = get_post_meta($post_id, 'original_link', true);
	if ($edgar_url) {
		$remote_edgar_url = wp_remote_get($edgar_url);
		$remote_edgar_url_response_code = wp_remote_retrieve_response_code($remote_edgar_url);
		// Response type = 200
		if (200 === (int) $remote_edgar_url_response_code) {
			$edgar_url_body = wp_remote_retrieve_body($remote_edgar_url);
			libxml_use_internal_errors(true);
			$wpr_html_dom = new DOMDocument();
			$wpr_html_dom->loadHTML($edgar_url_body);

			preg_match('!\d+!', $edgar_url, $matches);
			$the_post_id = end($matches);

			if ($wpr_html_dom && $the_post_id) {
				$wpr_xpath = new DOMXPath($wpr_html_dom);
				$wpr_xpath->registerNamespace("php", "http://php.net/xpath");

				$get_original_url = $wpr_xpath->query('/html/body//article[@data-post-id=' . $the_post_id . ']/div/header/a[contains(@class,"reblog-link")]');

				if (!is_null($get_original_url) && $get_original_url->length > 0) {
					$this_url = '';
					foreach ($get_original_url as $get_url) {
						$this_url = $get_url->getAttribute('href');
						if (!empty($this_url)) {
							break;
						}
					}

					if (!empty($this_url)) {
						$post_content = apply_filters('the_content', get_post_field('post_content', $post_id));
						wpr_update_edgar_articles_content($post_content, $this_url, $post_id);
//						update_post_meta( $post_id, 'wpr_original_source', esc_url( $this_url ) );
					}
				}
			}
		}
	}
}

/**
 * Update Edgar articles content URL
 *
 * @param $the_content
 * @param $the_url
 * @param $post_id
 */
function wpr_update_edgar_articles_content($the_content, $the_url, $post_id) {
	$wpr_html_dom = new DOMDocument();

	libxml_use_internal_errors(true);
	$wpr_html_dom->loadHTML($the_content);
	libxml_clear_errors();

	if ($wpr_html_dom && $post_id) {
		$wpr_xpath = new DOMXPath($wpr_html_dom);
		$wpr_xpath->registerNamespace("php", "http://php.net/xpath");

		$all_links_list = $wpr_xpath->query("//a");
		if (!empty($all_links_list)) {
			foreach ($all_links_list as $link_element) {
				$link_element->removeAttribute('href');
				$link_element->setAttribute("href", $the_url);
			}

			$content = $wpr_html_dom->saveHTML();

			$parse_url = parse_url($the_url);
			$replace_with = '<a class="wpr_article_source" target="_blank" rel="dofollow" href="' . $the_url . '">' . $parse_url['host'] . '</a>';
			$content = preg_replace('#<a[^>]+class="wpr_article_source"[^>]*>(.*)</a>#is', $replace_with, $content);

			$edgar_post = array(
				'ID' => $post_id,
				'post_content' => $content,
			);

			wp_update_post($edgar_post);
		}
	}
}

/**
 * Close all html tags
 *
 * @param $html
 *
 * @return string
 */
function closetags($html) {
	preg_match_all('#<([a-z]+)(?: .*)?(?<![/|/ ])>#iU', $html, $result);
	$openedtags = $result[1];
	preg_match_all('#</([a-z]+)>#iU', $html, $result);
	$closedtags = $result[1];
	$len_opened = count($openedtags);
	if (count($closedtags) == $len_opened) {
		return $html;
	}
	$openedtags = array_reverse($openedtags);
	for ($i = 0; $i < $len_opened; $i++) {
		if (!in_array($openedtags[$i], $closedtags)) {
			$html .= '</' . $openedtags[$i] . '>';
		} else {
			unset($closedtags[array_search($openedtags[$i], $closedtags)]);
		}
	}

	return $html;
}

add_filter('wp_footer', 'wpr_add_slider_class');

/**
 * Add background slider
 */
function wpr_add_slider_class() {
	if (is_page(array('login', 'register', 'complete-your-profile'))) {
		global $rfwbsgobj;
		$rfwbsgobj->rfwbs_display();
	}
	?>
	<?php if (isset($_POST['password'])) { ?>
		<script>
			jQuery(document).ready(function ($) {
				if ($('#password').length) {
					setTimeout(function () {
						$('.password-tab a').click();
					}, 400)
				}
			});
		</script>
		<?php
	}
}

add_filter('the_content', 'wpr_ferrari_content_fix', 9999, 1);

/**
 * Fix Ferrari content
 *
 * @param $content
 *
 * @return mixed
 */
function wpr_ferrari_content_fix($content) {
	$post_id = get_the_ID();
	$categories = get_the_terms($post_id, 'category');

	if (is_single() && 5 == $categories[0]->term_id) {
		$content = preg_replace('#<p>\s*+(<br\s*/*>)?\s*</p>#i', '', $content);
	}

//	$content = preg_replace( '/<p>(.*):/', '', $content, 1 );
	$is_parser = get_post_meta($post_id, 'wpr_email_source', true);
	if (!$is_parser) {
		$content = str_replace('http://', 'https://', $content);
	} else {
		$content = str_replace('https://', 'http://', $content);
	}

	return $content;
}

if (!function_exists('wpr_share_post')) {

	/**
	 * Displays post's share links
	 * @return string
	 */
	function wpr_share_post() {
		global $post;
		$url = urlencode(esc_url(get_permalink($post->ID)));
		$title = esc_attr($post->post_title);

		$thumbnail_src = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'medium');
		$external_image = get_post_meta($post->ID, 'wpr_remote_featured_img', true);

		$content = apply_filters('the_content', get_post_field('post_content', $post->ID));
		preg_match_all('((?:www\.)?(?:youtube(?:-nocookie)?\.com\/(?:v\/|watch\?v=|embed\/)|youtu\.be\/)([a-zA-Z0-9?&=_-]*))', $content, $urls);
		$urls = array_filter($urls);

		$wpr_img_url = '';
		if (!empty($urls)) {
			$youtube_url = $urls[0][0];

			$youtube_id = '';
			if (strlen(trim($youtube_url)) > 0) {
				$youtube_id = getYoutubeIdFromUrl($youtube_url);
			}

			if ($youtube_id) {
				$wpr_img_url = 'https://img.youtube.com/vi/' . $youtube_id . '/hqdefault.jpg';
				// Get the bigger image
				$wpr_sd_img_url = 'https://img.youtube.com/vi/' . trim($youtube_id) . '/sddefault.jpg';
				$response_sd = wp_remote_head($wpr_sd_img_url);
				if ($response_sd) {
					$wpr_img_url = $wpr_sd_img_url;
				}
			}
		}

		if (has_post_thumbnail($post->ID) && !empty($thumbnail_src[0])) {
			$thumb_url = esc_attr($thumbnail_src[0]);
		} elseif (!empty($external_image)) {
			$thumb_url = esc_attr($external_image);
		} elseif (!empty($wpr_img_url)) {
			$thumb_url = esc_attr($wpr_img_url);
		} else {
			$thumb_url = get_stylesheet_directory_uri() . '/assets/images/LBT-logo.png';
		}
		?>
		<div class="entry-share wpr-share-offer">
			<a class="genericon genericon-facebook-alt"
			   href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $url; ?>"
			   target="_blank"></a>
			<a class="genericon genericon-twitter"
			   href="https://twitter.com/intent/tweet/?text=<?php echo urlencode($title); ?>&url=<?php echo $url; ?>"
			   target="_blank"></a>
			<a class="genericon genericon-linkedin"
			   href="https://www.linkedin.com/sharing/share-offsite/?url=<?php echo $url; ?>&summary=<?php echo urlencode($title); ?>"
			   target="_blank"></a>
			<a class="genericon genericon-pinterest"
			   data-pin-do="buttonPin"
			   href="https://www.pinterest.com/pin/create/button/?url=<?php echo $url; ?>&media=<?php echo $thumb_url[0]; ?>&description=<?php echo $title; ?>"
			   data-pin-custom="true"></a>
		</div>
		<?php
	}

}

if (!function_exists('wpr_social_icons')) {

	/**
	 * Displays socia links
	 * @return string
	 */
	function wpr_social_icons() {
		?>
		<div class="entry-share wpr-share-offer">
			<a class="genericon genericon-facebook-alt"
			   href="https://www.facebook.com/LuxuryBuysToday/"
			   target="_blank"></a>
			<a class="genericon genericon-twitter"
			   href="https://twitter.com/luxurybuystoday"
			   target="_blank"></a>
			<a class="genericon genericon-linkedin"
			   href="https://www.linkedin.com/organization/22345830"
			   target="_blank"></a>
			<a class="genericon genericon-pinterest"
			   href="https://www.pinterest.com/LuxuryBuysToday/"
			   target="_blank"></a>
			<a class="genericon genericon-googleplus"
			   href="https://plus.google.com/109427800814304071215"
			   target="_blank"></a>
		</div>
		<?php
	}

}

add_filter('wsl_render_auth_widget_alter_provider_name', 'wpr_linkedin_icon', 10, 1);

/**
 * Change html elements to social login
 *
 * @param $provider_name
 *
 * @return string
 */
function wpr_linkedin_icon($provider_name) {
	if ('Facebook' == $provider_name) {
		$provider_name = sprintf('<span class="fa fa-facebook"></span> %s', __('Log in with Facebook'));
	}

	if ('LinkedIn' == $provider_name) {
		$provider_name = sprintf('<span class="fa fa-linkedin"></span> %s', __('Log in with LinkedIn'));
	}

	return $provider_name;
}

add_filter('wpseo_opengraph_title', 'wpr_update_ed_og_title', 99, 1);
add_filter('wpseo_twitter_title', 'wpr_update_ed_og_title', 99, 1);

/**
 * Update OG Title for Ed articles
 *
 * @param $title
 *
 * @return string
 */
function wpr_update_ed_og_title($title) {
	if (!is_single()) {
		return $title;
	}

	global $post;

	// Remove site title.
	$title = str_replace(' - Luxury Buys Today', '', $title);

	$edgar_cats = array('36', '37', '38', '39');
	if (in_category($edgar_cats, $post->ID)) {
		// Try to fetch the original title from the content.
		$content = apply_filters('the_content', get_post_field('post_content', $post->ID));
		preg_match('/<b>(.*):/', $content, $thetitle);
		if (!empty($thetitle) && strpos($thetitle[0], 'http') === false) {
			if ($thetitle[1]) {
				$original_title = $thetitle[1];
			} else {
				$original_title = $thetitle[0];
			}
		}

		if (!empty($original_title)) {
			$title = str_replace(array('BROYHILL COMMENTS', 'BROYHILL COMMENT'), '', $original_title);
			$title = strip_tags(html_entity_decode($title));

			$title = esc_attr($title);
		}

		// Convert uppercase to title case.
		if (ctype_upper(preg_replace('/[^A-Za-z]/', '', $title))) {
			$title = ucwords(strtolower($title));
		}

		// Limit to 60 - 7 = 52 chars.
		if (strlen($title) > 60) {
			$title = substr($title, 0, strrpos(substr($title, 0, 52), ' ')) . '...';
		}
	} else {
		$og_title = array();
		$categories = get_the_category();
		if (!empty($categories)) {
			array_push($og_title, esc_html($categories[0]->name));
		}

		$post_title = strip_tags($post->post_title);

		// Convert uppercase to title case.
		if (ctype_upper(preg_replace('/[^A-Za-z]/', '', $post_title))) {
			$post_title = ucwords(strtolower($post_title));
		}

		#array_push( $og_title, esc_attr( $post_title ) );

		$title = implode(' - ', $og_title);

		// Limit to 60 - 21 - 3 = 36 chars to accommodate " - Broyhill Rating: X".
		if (strlen($title) > 39) {
			$title = substr($title, 0, strrpos(substr($title, 0, 36), ' ')) . '...';
		}

		if (class_exists('WPR_Posts_Rating')) {
			global $wpr_posts_rating;
			$title .= ' - ' . sprintf('Broyhill Rating: %d', absint($wpr_posts_rating->wpr_seo_broy_rating_no()));
		}
	}
	if ( false !== strpos( $title, '-' ) ) {
		$alt_title = explode( '-', $title );
		return $alt_title[0];
	}
	return $title;
}

add_filter( 'oembed_response_data', 'wpr_update_ed_og_title_linkedin', 99, 4 );


/**
 * Remove the title from the Status Update oembed.
 *
 * @param string $title Post title.
 * @param int    $id Post ID.
 *
 * @return string
 */
function wpr_update_ed_og_title_linkedin( $data, $post_obj, $width, $height ) {
	global $post;
	$post = $post_obj;
	$title = str_replace( ' - Luxury Buys Today', '', $data['title'] );
	$edgar_cats = array( '36', '37', '38', '39' );
	if ( in_category( $edgar_cats, $post->ID ) ) {
		// Try to fetch the original title from the content.
		$content = apply_filters( 'the_content', get_post_field( 'post_content', $post->ID ) );
		preg_match( '/<b>(.*):/', $content, $thetitle );
		if ( ! empty( $thetitle ) && strpos( $thetitle[0], 'http' ) === false ) {
			if ( $thetitle[1] ) {
				$original_title = $thetitle[1];
			} else {
				$original_title = $thetitle[0];
			}
		}

		if ( ! empty( $original_title ) ) {
			$title = str_replace( array( 'BROYHILL COMMENTS', 'BROYHILL COMMENT' ), '', $original_title );
			$title = strip_tags( html_entity_decode( $title ) );

			$title = esc_attr( $title );
		}

		// Convert uppercase to title case.
		if ( ctype_upper( preg_replace( '/[^A-Za-z]/', '', $title ) ) ) {
			$title = ucwords( strtolower( $title ) );
		}

		// Limit to 60 - 7 = 52 chars.
		if ( strlen( $title ) > 60 ) {
			$title = substr( $title, 0, strrpos( substr( $title, 0, 52 ), ' ' ) ) . '...';
		}
	} else {
		$og_title   = array();
		$categories = get_the_category( $post->ID );
		if ( ! empty( $categories ) ) {
			array_push( $og_title, esc_html( $categories[0]->name ) );
		}

		$post_title = strip_tags( $post->post_title );

		// Convert uppercase to title case.
		if ( ctype_upper( preg_replace( '/[^A-Za-z]/', '', $post_title ) ) ) {
			$post_title = ucwords( strtolower( $post_title ) );
		}

		#array_push( $og_title, esc_attr( $post_title ) );

		$title = implode( ' - ', $og_title );

		// Limit to 60 - 21 - 3 = 36 chars to accommodate " - Broyhill Rating: X".
		if ( strlen( $title ) > 39 ) {
			$title = substr( $title, 0, strrpos( substr( $title, 0, 36 ), ' ' ) ) . '...';
		}

		if ( class_exists( 'WPR_Posts_Rating' ) ) {
			global $wpr_posts_rating;
			$title .= ' - ' . sprintf( 'Broyhill Rating: %d', absint( $wpr_posts_rating->wpr_seo_broy_rating_no() ) );
		}
	}
	if ( false !== strpos( $title, '-') && $width !== -1 ) {
		$alt_title = explode( '-', $title);
		$data['title'] = $alt_title[0];
	} else {
		$data['title'] = $title;
	}
	return $data;
}

/*
add_filter('wpseo_opengraph_desc', 'wpr_update_ed_og_description', 99, 1);
add_filter('wpseo_twitter_description', 'wpr_update_ed_og_description', 99, 1);*/

/**
 * Update OG Description for Ed articles
 *
 * @param $description
 *
 * @return null|string|string[]
 */
function wpr_update_ed_og_description($description) {
	if (is_single()) {
		global $post;
		$edgar_cats = array('36', '37', '38', '39');
		if (in_category($edgar_cats, $post->ID)) {
			$content = apply_filters('the_content', get_post_field('post_content', $post->ID));
			$content = preg_replace('/\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|$!:,.;]*[A-Z0-9+&@#\/%=~_|$]/i', '', trim(strip_tags(html_entity_decode($content))));
			$content = str_replace(array("BROYHILL COMMENTS", "BROYHILL COMMENT"), "", $content);
			$content = preg_replace('/Source.*/', '', $content);
			$content = str_replace(array("\n", "\r"), ' ', $content);
			$content = trim($content);
			$description = preg_replace('/(.*):/', '', $content, 1);
			$description = str_replace('&nbsp;', '', $description);
			$description = "BROYHILL COMMENTS:" . esc_attr($description);
		} else {
			$description = sprintf('Broyhill Comments: %s', strip_tags(trim(wpr_display_ed_comment($post->ID))));
			$description = str_replace(array("\n", "\r"), ' ', $description);
			$description = trim(strip_tags(html_entity_decode($description)));
		}
	}

	// Limit to 55 words.
	$description = wp_trim_words($description, 55);

	// Remove uppercase letters in the middle of sentences.
	$description = implode(' ', array_map(
		function ($sentence) {
			return ucfirst(strtolower($sentence));
		},
		preg_split('/(?<=[.?!])\s+(?=[a-z])/i', $description)
	));

	// Re-capitalize specific words.
	$uc_words = array(
		'Ed',
		'Broyhill',
		'BMW',
		'Luxury Buys Today',
	);
	foreach ($uc_words as $uc_word) {
		$description = preg_replace("/(?<=\W)+($uc_word){1}(?=\W)+/i", $uc_word, $description);
	}

	// Remove multiple consecutive spaces.
	$description = preg_replace('/\s+/mu', ' ', $description);

	return $description;
}

add_action('wpseo_opengraph', 'wpr_add_article_meta');

/**
 * Add article author and publisher meta tags.
 */
function wpr_add_article_meta() {
	echo '<meta property="article:author" content="Ed Broyhill" />' . PHP_EOL;
	echo '<meta property="article:publisher" content="Luxury Buys Today" />' . PHP_EOL;
}

/**
 * Check if Prospect have all data info filled
 *
 * @param $user_id
 *
 * @return bool
 */
function wpr_is_profile_not_complete($user_id) {
	$incomplete = false;

	$user_info = get_user_by('id', $user_id);
	$phone = get_user_meta($user_id, 'phone', true);
	$zip_code = get_user_meta($user_id, 'zip_code', true);

	if (in_array('wpr_prospect', $user_info->roles)) {
		if (
			empty($phone) ||
			empty($zip_code)
		) {
			$incomplete = true;
		}
	}

	return $incomplete;
}

add_filter('get_avatar', 'wpr_custom_user_avatar', 10, 5);

/**
 * User avatar
 *
 * @param $avatar
 * @param $id_or_email
 * @param $size
 * @param $default
 * @param $alt
 *
 * @return string
 */
function wpr_custom_user_avatar($avatar, $id_or_email, $size, $default, $alt) {
	$user = false;

	if (is_numeric($id_or_email)) {

		$id = (int) $id_or_email;
		$user = get_user_by('id', $id);
	} elseif (is_object($id_or_email)) {

		if (!empty($id_or_email->user_id)) {
			$id = (int) $id_or_email->user_id;
			$user = get_user_by('id', $id);
		}
	} else {
		$user = get_user_by('email', $id_or_email);
	}

	if ($user && is_object($user)) {
		if (is_admin() && function_exists('get_current_screen') && 'user-edit' != get_current_screen()) {
			$size = 32;
		} else {
			$size = 200;
		}
		$avatar_url = get_user_meta($user->data->ID, 'wpr_user_avatar', true);
		if (!$avatar_url) {
			$avatar_pro_url = get_user_meta($user->data->ID, 'wp_user_avatar', true);
			if ($avatar_pro_url) {
				$avatar_url = esc_url($avatar_pro_url['avatar_url']);
			}
		}
		if ($avatar_url) {
			$avatar = "<img alt='{$alt}' src='{$avatar_url}' class='avatar avatar-{$size} photo' height='{$size}' width='{$size}' />";
		}
	}

	return $avatar;
}

/**
 * Declare new thumbnail size
 */
function declare_custom_offer_thumbnail() {
	add_image_size( 'offer_thumb', 120, 120, false);
	add_image_size( 'wpr_listing', 450, 9999, false);
}

add_action('init', 'declare_custom_offer_thumbnail');

#add_filter( 'gform_submit_button_3', 'add_paragraph_below_submit', 10, 2 );
/**
 * Add content before submit button
 *
 * @param $button
 * @param $form
 *
 * @return string
 */

function add_paragraph_below_submit($button, $form) {

	$before_button = '<a href="' . esc_url(get_permalink(get_page_by_path('password-reset'))) . '" class="wpr-forgot-password">' . __('Forgot password?') . '</a>';
	$before_button .= $button;

	return $before_button;
}

add_filter('wpseo_next_rel_link', 'mtb_canonical_ssl');
add_filter('wpseo_prev_rel_link', 'mtb_canonical_ssl');

/**
 * @param $head
 *
 * @return mixed
 */
function mtb_canonical_ssl($head) {
	$head = str_replace("http:", "https:", $head);

	return $head;
}

add_action('pre_user_query', 'wpr_pre_user_query');

/**
 * @param $user_search
 */
function wpr_pre_user_query($user_search) {
	$screen = get_current_screen();

	if (is_admin() && 'users' == $screen->id) {
		global $wpdb;

		$user_search->query_where = str_replace(
			'WHERE 1=1',
			"WHERE 1=1 AND {$wpdb->users}.ID NOT IN (
              SELECT {$wpdb->usermeta}.user_id FROM $wpdb->usermeta
              WHERE {$wpdb->usermeta}.meta_key = 'wpr_dealer_association')",
			$user_search->query_where
		);
	}
}

add_filter('pre_get_posts', 'exclude_custom_posts_from_feed');

/**
 * Exclude custom posts from feed
 *
 * @param $query
 *
 * @return mixed
 */
function exclude_custom_posts_from_feed($query) {
	if ($query->is_feed || 0 === get_current_user_id()) {
		$meta_query = array(
			array(
				'key' => 'wpr_dealer_id',
				'compare' => 'NOT EXISTS',
			),
		);
		$query->set('meta_query', $meta_query);
	}

	if ( ! is_admin() && $query->is_main_query() ) {
		// Not a query for an admin page.
		// It's the main query for a front end page of your site.

		if ( is_category() && ! isset( $_GET['tag'] ) ) {
			// Remove promotions from category page.
			$args = array('56', '57');
			$query->set('tag__not_in', $args );
		}
	}

	return $query;
}

/**
 * Remove Emoji loading
 */
remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('wp_print_styles', 'print_emoji_styles');

add_filter('body_class', 'wpr_add_body_class');

/**
 * Add body class
 *
 * @param $classes
 *
 * @return mixed
 */
function wpr_add_body_class($classes) {
	if (is_page('my-offers') || is_category() || is_front_page() || is_home()) {
		$classes[] = 'wpr_filter_class';
	}

	if (is_user_logged_in()) {
		$classes[] = 'wpr_logged_in';
	} else {
		$classes[] = 'wpr_not_logged_in';
	}

	return $classes;
}

if (!function_exists('wpr_selected_user_brands')) {

	/**
	 * Brands ids used for filtering
	 *
	 * @param $categories
	 *
	 * @return array|int|mixed|WP_Error
	 */
	function wpr_selected_user_brands() {
		$categories = array(8, 13, 14, 17);
		if (isset($_COOKIE['wpr_selected_brands'])) {
			$data = json_decode(stripslashes($_COOKIE['wpr_selected_brands']), true);

			$selected_cats = $data['brands'];
		} else {
			if (!is_user_logged_in() && is_single()) {
				$selected_cats = wp_get_post_terms(get_the_ID(), 'category', array(
					'fields' => 'ids',
					'get' => 'all',
					'exclude' => $categories,
				));
			} elseif (!is_user_logged_in() && is_category()) {
				$selected_cats = array(get_query_var('cat'));
			} else {
//				$selected_cats = get_terms( 'category', array( 'fields' => 'ids', 'get' => 'all', 'exclude' => $categories ) );
				$selected_cats = array();
			}
		}

		if (is_user_logged_in()) {
			$selected_cats = get_user_meta(get_current_user_id(), 'wpr_selected_brands', true);
		}

		return $selected_cats;
	}

}

if (!function_exists('wpr_is_page_display_filter')) {

	function wpr_is_page_display_filter() {
		$is_page = false;
		if (is_page('my-offers') || is_category() || is_front_page() || is_home()) {
			$is_page = true;
		}

		return $is_page;
	}

}

if (!function_exists('wpr_entry_meta')) :

	/**
	 * Prints HTML with meta information.
	 */
	function wpr_entry_meta($single = NULL) {
		// Hide category and tag text for pages.
		if ('post' == get_post_type()) {
			/* translators: used between list items, there is a space after the comma */
			$category = get_the_category();
			if ($single) {
				echo '<span class="cat-links "> <span class="subtle">Post From: </span>' . $category[0]->name . ' &nbsp;&nbsp;&nbsp;|</span>';
			} else {
				if ($category && novus_categorized_blog()) {
					// printf('<span class="cat-links">' . __('%2$s: %1$s', 'novus') . '</span>', $category[0]->name, __('Brand'));
					// echo '<br />';
					printf('<span class="cat-links"><b>' . __('%1$s latest news', 'novus') . '</b></span>', $category[0]->name);
				}
			}
		}
	}

endif;

/**
 * Extract first paragraph string
 *
 * @param $content
 *
 * @return string
 */
function wpr_extract_string_from_first_paragraph($content) {
	$text = substr($content, 0, strpos($content, '</p>') + 4);

	return strip_tags($text);
}

if (!function_exists('wpr_display_ed_comment')) {

	/**
	 * Prints Ed spin comment
	 *
	 * @param $post_id
	 */
	function wpr_display_ed_comment($post_id) {
		$comment = lbt_get_broyhill_comment( $post_id );

		if ( ! empty( $comment['content'] ) ) {
			return strtoupper( $comment['title'] ?? '' ) . ': ' . $comment['content'];
		}

		$display_content = get_the_title($post_id);
		$broyhill_id = get_post_meta($post_id, 'wpr_spinned_edd_article', true);

		$get_spin_content = get_post_meta($post_id, 'wpr_spin_the_content', true);
		$is_empty = preg_replace('/\s+/', '', trim($get_spin_content));
		if ($get_spin_content && strlen($is_empty) > 2) {
			$get_spin_content = html_entity_decode($get_spin_content);
			$display_content = esc_attr(strip_tags($get_spin_content));
		}

		$get_spin_title = get_post_meta($post_id, 'wpr_spin_the_title', true);
		if ($get_spin_title && !$get_spin_content) {
			$get_spin_title = html_entity_decode($get_spin_title);
			$display_content = esc_attr(strip_tags($get_spin_title));
		}

		// Clean text
		$display_content = wpr_cleanse_string($display_content);
		$display_content = str_replace(array(
			"d &Atilde; &copy; cor",
			"d Ã © cor",
			"Ã © cor",
			"dÃ © cor",
			"& Acirc",
			"-& Acirc",
		), "decor", $display_content);
		$display_content = html_entity_decode($display_content, ENT_QUOTES | ENT_IGNORE, "UTF-8");
		$display_content = preg_replace("/&#?[a-z0-9]{2,8};/i", "", $display_content);
		$display_content = preg_replace('/Source.*/', '', $display_content);
//		$display_content = preg_replace( '/(.*):/', '', $display_content );
		$display_content = str_replace(array("BROYHILL COMMENTS", "BROYHILL COMMENT"), "", $display_content);
		$display_content = str_replace("â", "", $display_content);
		$display_content = str_replace("Â", "", $display_content);
		$display_content = str_replace("& Atilde;", "", $display_content);
		$display_content = delete_all_between('{', '|', $display_content);

		preg_match('/(.*):/', $display_content, $thetitle);
		if (!empty($thetitle) && strpos($thetitle[0], 'http') === false) {
			$display_content = preg_replace('/(.*):/', '', $display_content, 1);
		}



		if (empty(trim($display_content)) || !wpr_validate_broyhill_comment($display_content)) {
			$display_content = wpr_generate_new_spinned_content($post_id);
		}

		if (!$broyhill_id || !$get_spin_content) {
			$display_content = wpr_generate_new_spinned_content($post_id);
		}

		if ($broyhill_id) {
//			$cleared_title = get_post_meta( $broyhill_id, 'wpr_original_title', true );
//			$cleared_title = htmlspecialchars_decode( wp_strip_all_tags( $cleared_title ) );
			$cleared_title = get_the_title($broyhill_id);
			$add_edd_title = '';
			if ($cleared_title) {
				$add_edd_title = esc_attr($cleared_title);
			} else {
				$ed_original_title = get_post_meta($broyhill_id, 'original_title', true);
				preg_match('/(.*):/', $ed_original_title, $thetitle);
				if (!empty($thetitle) && strpos($thetitle[0], 'http') === false) {
					$thetitle_content = str_replace(array("BROYHILL COMMENTS", "BROYHILL COMMENT"), "", $thetitle[0]);
					$add_edd_title = esc_attr($thetitle_content);
				}
			}

			// $display_content = $add_edd_title . ': ' . $display_content;
		}

		$display_content = htmlspecialchars_decode($display_content);



		return trim(wp_strip_all_tags($display_content));
	}

}

/**
 * Run spin Ed Content
 *
 * @param $post_id
 *
 * @return null|string|string[]
 */
function wpr_generate_new_spinned_content($post_id) {
	$edgar_cat = array(
		'8' => '36',
		'13' => '37',
		'14' => '38',
		'17' => '39',
	);
	$categories = get_the_category($post_id);
	$category = $categories[0];
	$cat_id = $category->term_id;

	if ($category->parent > 0) {
		$cat_id = $category->parent;
	}
	$get_post_from = $edgar_cat[$cat_id];

	$args = array(
		'post_type' => 'post',
		'post_status' => 'published',
		'posts_per_page' => 1,
		'post__not_in' => array($post_id),
		'date_query' => array('after' => '-7 days'),
		'orderby' => 'rand',
		'category__in' => $get_post_from,
	);

	$edd_posts = new WP_Query($args);
	$post_content = $fetch_content = '';
	$edd_post_id = '';
	if ($edd_posts->have_posts()) {
		// The Loop
		while ($edd_posts->have_posts()) {
			$edd_posts->the_post();
			$post_content = get_post_meta($edd_posts->post->ID, 'wpr_wordai_string', true);
			$edd_post_id = $edd_posts->post->ID;
		}
		wp_reset_postdata();
	} else {
		unset($args['date_query']);
		$args['meta_key'] = 'wpr_wordai_string';

		$edd_posts = new WP_Query($args);

		if ($edd_posts->have_posts()) {
			// The 2nd Loop
			while ($edd_posts->have_posts()) {
				$edd_posts->the_post();
				$post_content = get_post_meta($edd_posts->post->ID, 'wpr_wordai_string', true);
				$edd_post_id = $edd_posts->post->ID;
			}
			wp_reset_postdata();
		}
	}

	$spintax = new Spintax();
	$spin_content = $spintax->process($post_content);

	$display_content = wpr_cleanse_string($spin_content);
	$display_content = str_replace(array("d &Atilde; &copy; cor", "d Ã © cor"), "decor", $display_content);
	$display_content = html_entity_decode($display_content, ENT_QUOTES | ENT_IGNORE, "UTF-8");
	$display_content = preg_replace("/&#?[a-z0-9]{2,8};/i", "", $display_content);
	$display_content = preg_replace('/Source.*/', '', $display_content);
	$display_content = preg_replace('/(.*):/', '', $display_content);
	$display_content = str_replace("â", "", $display_content);
	$display_content = str_replace("Â", "", $display_content);
	$display_content = str_replace("& Atilde;", "", $display_content);

	if (!empty($display_content)) {
		$fetch_content = $post_content;
		update_post_meta(absint($post_id), 'wpr_spinned_edd_article', absint($edd_post_id));
	} else {
		$fetch_random_edd = wpr_fetch_random_edd_content($get_post_from, $post_id);
		if ($fetch_random_edd) {
			$fetch_content = $fetch_random_edd['post_content'];
			update_post_meta(absint($post_id), 'wpr_spinned_edd_article', absint($fetch_random_edd['post_id']));
		}
	}

	$spinned_content = $spintax->process($fetch_content);
	$spinned_content = wp_strip_all_tags($spinned_content);

	update_post_meta(absint($post_id), 'wpr_spin_the_content', $spinned_content);

	return $spinned_content;
}

/**
 * Validate Broyhill comment text to ensure it doesn't contain server error messages.
 *
 * @param  string $text The comment text to check.
 *
 * @return bool         True if the comment is valid, false otherwise.
 */
function wpr_validate_broyhill_comment($text) {
	return (strpos($text, 'Please try again') === false);
}

/**
 * Display Broyhill comment title
 *
 * @param $category_id
 * @param $post_id
 */
function wpr_display_broyhill_comment_name($category_id, $post_id) {
	$edgar_cats = array('36', '37', '38', '39');
	if (in_category($edgar_cats, $post_id)) {
		//echo esc_attr(get_cat_name($category_id));
		echo __('What I like about this post: ');
	} else {
//		echo sprintf( '%s %s %s', __( 'Broyhill' ), esc_attr( get_cat_name( $category_id ) ), __( 'News' ) );
		echo __('What I like about this post: ');
	}
}

/**
 * @return bool
 */
function wpr_is_subcategory() {
	$cat = get_query_var('cat');
	$category = get_category($cat);
	$category->parent;

	if ($category->parent == '0' || in_array($category->term_id, array(36, 37, 38, 39))) {
		return false;
	} else {
		return true;
	}
}

add_action('wp_head', 'wpr_add_head_tags');

/**
 * Add custom header code
 */
function wpr_add_head_tags() {
	echo '<meta name="google-site-verification" content="gx5cnt8p9BhpKbBgw9CVmm-bztHBqWuT-Z_x2IQ3OZs" />';
}

add_action('wpr_display_post_rating', 'wpr_display_post_rating');

/**
 * Display Post Rating
 */
function wpr_display_post_rating() {
	$raiting = get_post_meta( get_the_ID(), 'offer_raiting', true );
	$raiting = intval( $raiting ) ? intval( $raiting ) : 3;
	?>
	<div class="star-ratings-css">
		<div class="star-ratings-css-bottom">
			<?php for ( $i = 1; $i <= 5; $i++ ) {
				if ( $i <= $raiting ) {
					?>
					<span class="filled">★</span>
					<?php
				} else { ?>
					<span>★</span>
				<?php }
			} ?>
		</div>
	</div>
	<?php
}

add_action('wpr_homepage_editorial_section', 'wpr_homepage_editorial_section');

/**
 * Display homepage editorial section
 */
function wpr_homepage_editorial_section() {
	$current_post_id = get_the_ID();
	$edgar_cats = array(36, 37, 38, 39);
	$current_cat = get_the_category($current_post_id);
	$industry = $current_cat[0]->term_id;
	$parents = array(
		8  => 36,
		13 => 37,
		14 => 38,
		17 => 39,
	);
	if (!in_array($industry, $edgar_cats)) {
		$industry_cats = get_term_children($current_cat[0]->parent, 'category');

		// Find the Broyhill category from current industry.
		foreach ($industry_cats as $industry_cat) {
			if (in_array($industry_cat, $edgar_cats)) {
				$industry = $industry_cat;
				break;
			}
		}
		if( isset( $current_cat[0]->parent, $parents ) ) {
			$industry = $parents[ $current_cat[0]->parent ];
		}
	}
	$args = array(
		'post_type' => 'post',
		'category__in' => array($industry),
		'posts_per_page' => 1,
		'post__not_in' => array($current_post_id),
		'orderby' => 'rand',
	);
	if( isset( $_GET['mitko123123'])){
		echo '<pre>';
		print_r( $current_cat );
		print_r( $industry_cats );
		print_r( $args );
		echo '</pre>';
	}

	$result = new WP_Query($args);

	if ($result->have_posts()) {
		while ($result->have_posts()) {
			$result->the_post();
			$post_id = get_the_ID();
			$categories = get_the_category(get_the_ID());
			$output = '';
			$brand_id = '';
			if (!empty($categories)) {
				foreach ($categories as $category) {
//						$output = '<a href="' . esc_url( get_category_link( $category->parent ) ) . '" title="' . get_cat_name( $category->parent ) . '">' . get_cat_name( $category->parent ) . '</a>';
					$output = get_cat_name($category->parent);
					$brand_id = $category->term_id;
					break;
				}
			}

			// Get the offer picture. First look for the post thumbnail, then the remote featured image, then fall back to default LBT logo image.
			$current_category = end($current_cat);
			$category_id = $current_category->term_id;
			$category_slug = $current_category->slug;
			$category_name = $current_category->name;
			$parent = get_term($current_category->parent);
			$parent_slug = $parent->slug;

			$post_thumb_url = get_post_meta( $post_id, 'wpr_remote_featured_img', true );
			$post_thumb = sprintf(' style="background-image:url(%s);background-size: cover;background-repeat: no-repeat;background-position: center;min-height: 200px;"', $post_thumb_url);
			?>
			<div class="wpr_display_editorial_section fff ">
				<div class="wpr_display_editorial_section_left">
					<div class="wpr_display_editorial_section_left_content">
						<div class="wpr_editorial_category">
							<div class="wpr_editorial_editor_brand"><?php echo $output; ?></div>
							<div class="wpr_editorial_editor_pick"><?php echo __('Broyhill\'s Pick'); ?></div>
						</div>
						<?php the_title(sprintf('<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url(get_permalink())), '</a></h2>'); ?>
						<div class="wpr_editorial_post_date">
							<div><?php echo sprintf('%s: <a href="%s" target="_blank">%s</a>', __('Published by'), 'http://luxurybuystoday.tumblr.com/', __('Ed Broyhill')); ?>
								|
							</div>
							<div><?php echo get_the_date(); ?></div>
						</div>
						<div class="wpr_editorial_excerpt"><strong>BROYHILL COMMENT </strong><?php wpr_custom_length_excerpt(30); ?><a style="margin-left: -9px !IMPORTANT;background: #fff !important;" class="link-on-excerpt" href="<?php the_permalink(); ?>"><?php echo __('...'); ?></a></div>

						<div class="wpr_editorial_action_buttons">
							<a href="<?php the_permalink(); ?>"
							   title="<?php the_title_attribute(); ?>"><?php echo __('Read more'); ?></a>
							<a href="<?php echo esc_url(get_category_link($brand_id)); ?>"
							   title="<?php echo __('See All Broyhill News'); ?>"><?php echo __('See All Broyhill News'); ?></a>
						</div>
					</div>
				</div>
				<div class="wpr_display_editorial_section_right"<?php echo $post_thumb; ?>>
					<a href="<?php the_permalink(); ?>"
					   title="<?php the_title_attribute(); ?>"
					   class="wpr_homepage_image_url"></a>
				</div>
			</div>

			<?php
		}
		wp_reset_postdata();
	}
}

add_action('wpr_homepage_editorial_section2', 'wpr_homepage_editorial_section2');

function wpr_homepage_editorial_section2() {
$current_post_id = get_the_ID();
$edgar_cats = array(36, 37, 38, 39);
$current_cat = get_the_category($current_post_id);
$industry = $current_cat[0]->term_id;


if (!in_array($industry, $edgar_cats)) {
	$industry_cats = get_term_children($current_cat[0]->parent, 'category');

	// Find the Broyhill category from current industry.
	foreach ($industry_cats as $industry_cat) {
		if (in_array($industry_cat, $edgar_cats)) {
			$industry = $industry_cat;
			break;
		}
	}
}

$args = array(
	'post_type' => 'post',
	'category__in' => array($industry),
	'posts_per_page' => 1,
	'post__not_in' => array($current_post_id),
	'orderby' => 'rand',
);

$result = new WP_Query($args);

if ($result->have_posts()) {
while ($result->have_posts()) {
$result->the_post();
$post_id = get_the_ID();
$categories = get_the_category(get_the_ID());
$output = '';
$brand_id = '';
if (!empty($categories)) {
	foreach ($categories as $category) {
//                      $output = '<a href="' . esc_url( get_category_link( $category->parent ) ) . '" title="' . get_cat_name( $category->parent ) . '">' . get_cat_name( $category->parent ) . '</a>';
		$output = get_cat_name($category->parent);
		$brand_id = $category->term_id;
		break;
	}
}

// Get the offer picture. First look for the post thumbnail, then the remote featured image, then fall back to default LBT logo image.
$current_category = end($current_cat);
$category_id = $current_category->term_id;
$category_slug = $current_category->slug;
$category_name = $current_category->name;
$parent = get_term($current_category->parent);
$parent_slug = $parent->slug;
$post_thumb_url = get_post_meta($post_id, 'wpr_remote_featured_img', true);
$post_thumb = sprintf(' style="background-image:url(%s);background-size: cover;background-repeat: no-repeat;background-position: center;min-height: 200px;"', $post_thumb_url);
?>
<?php if (is_user_logged_in()) { ?>
<div class="editorial-content">

	<?php } else { ?>
	<div class="editorial-content editorial-content-no-log log-out">

		<?php } ?>
		<div class="profile-section wpr-load-dealer-data1" data-cat="<?php echo $category_id; ?>" data-type="single">

			<div class="profile-description f">

				<?php if (is_user_logged_in()) { ?>
					<img class="profile-img" src="/wp-content/uploads/2019/03/img_avatar.png" />
					<h1 class="profile-title">John Brussen</h1>
					<h6 class="profile-info"><a href="#"> Salesman In, Dealership &amp; Brand</a></h6>
					<p class="profile-icons">
						<a href="<?php echo get_bloginfo('url').'/'.$parent_slug.'/'.$category_slug ?>"><i class="fa fa-list" title="<?php echo $category_name; ?> Offers"></i></a>
						<a href="tel:" data-rel="external"><i class="fa fa-phone" aria-hidden="true"></i></a>
						<a href="mailto:"><i class="fa fa-envelope-o"></i></a>
					</p>
				<?php } else { ?>
					<!-- <p class="looking-for">Looking to buy BMW? Here is the closest location.</p> -->

				<?php } ?>
			</div>
			<div class="profile-car-description">
				<?php if (is_user_logged_in()) { ?>
					<img class="car-logo" src="/wp-content/uploads/2019/03/BMW_logo-512.png">
				<?php } ?>
				<p class="car-place">BMW of Manhattan</p>
				<p class="profile address">816 North Route 1, Edison, Nj</p>
				<p class="profile fb-address"><a href="#">https://www.facebook.com...</a></p>
				<p class="profile brand-catalog">View Brand Catalog</p>
				<p class="profile store-hours">Store Hours<i class="fa fa-angle-down"></i></p>


			</div>


		</div>
		<?php do_action('display_cta_buttons_single_offer_overlay'); ?>
	</div>



	<?php
	}
	wp_reset_postdata();
	}
	}
	/**
	 * Excerpt custom length
	 *
	 * @param $word_count_limit
	 */
	function wpr_custom_length_excerpt($word_count_limit) {
		$content = wp_strip_all_tags(strip_shortcodes(get_the_content()), true);
		$content = str_replace(array("BROYHILL COMMENTS", "BROYHILL COMMENT"), '', $content);
		echo wp_trim_words($content, $word_count_limit);
	}

	add_filter('gform_username', 'auto_username', 10, 4);

	/**
	 * Generate username on user registration
	 *
	 * @param $username
	 * @param $feed
	 * @param $form
	 * @param $entry
	 *
	 * @return string
	 */
	function auto_username($username, $feed, $form, $entry) {
		$username = strtolower(rgar($entry, '1'));
		$username = explode('@', $username);
		$username = reset($username);

		if (empty($username)) {
			return $username;
		}

		$username = sanitize_user($username);

		if (!function_exists('username_exists')) {
			require_once(ABSPATH . WPINC . '/registration.php');
		}

		if (username_exists($username)) {
			$i = 2;
			while (username_exists($username . $i)) {
				$i++;
			}
			$username = $username . $i;
		}

		return $username;
	}

	/**
	 * Allow use email as username
	 */
	add_filter('wpmu_validate_user_signup', function ($result) {
		$error_name = $result['errors']->get_error_message('user_name');
		if (!empty($error_name) && $error_name === __('Usernames can only contain lowercase letters (a-z) and numbers.')) {
			$user_name = preg_replace('/\s+/', '', sanitize_user($result['user_name'], true));
			// set clean user_name and clear the user_name error
			$result['user_name'] = $user_name;
			unset($result['errors']->errors['user_name']);
		}

		return $result;
	});

	/**
	 * Return User lookbook
	 *
	 * @param $user_id
	 *
	 * @return array|bool
	 */
	function wpr_fetch_lookbook_for_user($user_id) {
		global $wpdb, $paged, $max_num_pages, $current_date;

		$paged = (get_query_var('page')) ? get_query_var('page') : 1;
		if ($paged === 1) {
			$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
		}
		$post_per_page = intval(get_query_var('posts_per_page'));
		$offset = ($paged - 1) * $post_per_page;

		$table_brand = $wpdb->prefix . 'wpr_cta_buttons';
		if (isset($_GET['brand_id']) && $_GET['brand_id'] != '' && $_GET['brand_id'] != 'null') {
			$brandsList = '('.$_GET['brand_id'].')';
			$result = $wpdb->get_results(
				$wpdb->prepare("SELECT post_id FROM $table_brand WHERE user_id = %d AND btn_type = 'lookbook' AND brand_id in $brandsList GROUP BY post_id ORDER BY date DESC LIMIT $offset, $post_per_page", absint($user_id))
				, OBJECT);

			$sql_posts_total = $wpdb->get_var($wpdb->prepare("SELECT post_id FROM $table_brand WHERE user_id = %d AND btn_type = 'lookbook' AND brand_id in $brandsList GROUP BY post_id ORDER BY date DESC", absint($user_id)));
		} else {
			$result = $wpdb->get_results(
				$wpdb->prepare("SELECT post_id FROM $table_brand WHERE user_id = %d AND btn_type = 'lookbook' GROUP BY post_id ORDER BY date DESC LIMIT $offset, $post_per_page", absint($user_id))
				, OBJECT);

			$sql_posts_total = $wpdb->get_var($wpdb->prepare("SELECT post_id FROM $table_brand WHERE user_id = %d AND btn_type = 'lookbook' GROUP BY post_id ORDER BY date DESC", absint($user_id)));
		}
		$max_num_pages = ceil($sql_posts_total / $post_per_page);

		$lookbook_posts = array();
		$favorites = get_user_meta($user_id, 'simplefavorites', true);
		if ($favorites) {
			$lookbook_posts = $favorites[0]['posts'];
		}

		$posts = array();
		if ($result) {
			foreach ($result as $key => $val) {
				if (in_array($val->post_id, $lookbook_posts) && get_post_status($val->post_id)) {
					array_push($posts, $val->post_id);
				}
			}
		}

		if (!empty($posts)) {
			return $posts;
		} else {
			return false;
		}
	}

	add_action('wp_footer', 'my_custom_popup_scripts', 500);

	/**
	 * Trigger pop-up close / open
	 */
	function my_custom_popup_scripts() {
		?>
		<script type="text/javascript">
			jQuery(document).ready(function ($) {
				$(document).bind("gform_confirmation_loaded", function (e, form_id) {
					if (form_id == 11) {
						$("#popmake-32845 p, #popmake-32845 .oneall_social_login").remove();
						if (window.history.pushState) {
							window.history.pushState('', '/', window.location.pathname)
						} else {
							window.location.hash = '';
						}
					}
				});

				$('#popmake-32845').on('click', '.popmake-close', function () {
					// window.location.href = $(location).attr('href');
				});
			});
		</script><?php
	}

	add_action('wp_head', 'itsg_add_ie_edge_wp_headers');

	/**
	 * Add IE Compatible tag
	 */
	function itsg_add_ie_edge_wp_headers() {
		echo '<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">';
	}

	/**
	 * Replace social login callback
	 */
	if (function_exists('oa_social_login_callback')) {
		remove_action('init', 'oa_social_login_init', 9);

		add_action('init', 'wpr_oa_social_login_init', 9);
	}

	/**
	 * Social login Init
	 */
	function wpr_oa_social_login_init() {
		//Add language file.
		if (function_exists('load_plugin_textdomain')) {
			load_plugin_textdomain('oa_social_login', false, OA_SOCIAL_LOGIN_BASE_PATH . '/languages/');
		}

		//Launch the callback handler.
		wpr_oa_social_login_callback();
	}

	/**
	 * Social login callback
	 */
	function wpr_oa_social_login_callback() {
		//Callback Handler
		if (isset($_POST) AND ! empty($_POST ['oa_action']) AND $_POST ['oa_action'] == 'social_login' AND ! empty($_POST ['connection_token'])) {
			//OneAll Connection token
			$connection_token = trim($_POST ['connection_token']);

			//Read settings
			$settings = get_option('oa_social_login_settings');

			//API Settings
			$api_connection_handler = ((!empty($settings ['api_connection_handler']) AND $settings ['api_connection_handler'] == 'fsockopen') ? 'fsockopen' : 'curl');
			$api_connection_use_https = ((!isset($settings ['api_connection_use_https']) OR $settings ['api_connection_use_https'] == '1') ? true : false);
			$api_subdomain = (!empty($settings ['api_subdomain']) ? trim($settings ['api_subdomain']) : '');

			//We cannot make a connection without a subdomain
			if (!empty($api_subdomain)) {
				//See: http://docs.oneall.com/api/resources/connections/read-connection-details/
				$api_resource_url = ($api_connection_use_https ? 'https' : 'http') . '://' . $api_subdomain . '.api.oneall.com/connections/' . $connection_token . '.json';

				//API Credentials
				$api_opts = array();
				$api_opts['api_key'] = (!empty($settings ['api_key']) ? $settings ['api_key'] : '');
				$api_opts['api_secret'] = (!empty($settings ['api_secret']) ? $settings ['api_secret'] : '');

				//Retrieve connection details
				$result = oa_social_login_do_api_request($api_connection_handler, $api_resource_url, $api_opts);

				//Check result
				if (is_object($result) AND property_exists($result, 'http_code') AND $result->http_code == 200 AND property_exists($result, 'http_data')) {
					//Decode result
					$decoded_result = @json_decode($result->http_data);
					if (is_object($decoded_result) AND isset($decoded_result->response->result->data->user)) {
						//User data
						$user_data = $decoded_result->response->result->data->user;

						//Social network profile data
						$identity = $user_data->identity;

						//Unique user token provided by OneAll
						$user_token = $user_data->user_token;

						//Identity Provider
						$user_identity_provider = $identity->source->name;

						//Thumbnail
						$user_thumbnail = (!empty($identity->thumbnailUrl) ? trim($identity->thumbnailUrl) : '');

						//Picture
						$user_picture = (!empty($identity->pictureUrl) ? trim($identity->pictureUrl) : '');

						//About Me
						$user_about_me = (!empty($identity->aboutMe) ? trim($identity->aboutMe) : '');

						//Note
						$user_note = (!empty($identity->note) ? trim($identity->note) : '');

						//Firstname
						$user_first_name = (!empty($identity->name->givenName) ? $identity->name->givenName : '');

						//Lastname
						$user_last_name = (!empty($identity->name->familyName) ? $identity->name->familyName : '');

						//Fullname
						if (!empty($identity->name->formatted)) {
							$user_full_name = $identity->name->formatted;
						} elseif (!empty($identity->name->displayName)) {
							$user_full_name = $identity->name->displayName;
						} else {
							$user_full_name = trim($user_first_name . ' ' . $user_last_name);
						}

						// Email Address.
						$user_email = '';
						if (property_exists($identity, 'emails') AND is_array($identity->emails)) {
							$user_email_is_verified = false;
							foreach( $identity->emails as $email ) {
								$user_email = $email->value;
								$user_email_is_verified = ($email->is_verified == '1');
								if( $user_email_is_verified ) {
									break;
								}
							}
						}

						//User Website
						if (!empty($identity->profileUrl)) {
							$user_website = $identity->profileUrl;
						} elseif (!empty($identity->urls [0]->value)) {
							$user_website = $identity->urls [0]->value;
						} else {
							$user_website = '';
						}

						//Preferred Username
						if (!empty($identity->preferredUsername)) {
							$user_login = $identity->preferredUsername;
						} elseif (!empty($identity->displayName)) {
							$user_login = $identity->displayName;
						} else {
							$user_login = $user_full_name;
						}

						//New user created?
						$new_registration = false;

						//Sanitize Login
						$user_login = str_replace('.', '-', $user_login);
						$user_login = sanitize_user($user_login, true);

						// Check if the user is logged in
						if (is_user_logged_in()) {
							$user_id = get_current_user_id();

							//Refresh the meta data
							delete_metadata('user', null, 'oa_social_login_user_token', $user_token, true);
							update_user_meta($user_id, 'oa_social_login_user_token', $user_token);
							update_user_meta($user_id, 'oa_social_login_identity_provider', $user_identity_provider);
						} else {
							// Get user by token
							$user_id = oa_social_login_get_userid_by_token($user_token);
						}

						if (!is_numeric($user_id)) {
							$user_id = email_exists($user_email);
						}

						//Try to link to existing account
						if (!is_numeric($user_id)) {
							//This is a new user
							$new_registration = true;

							//Linking enabled?
							if (!isset($settings ['plugin_link_verified_accounts']) OR $settings ['plugin_link_verified_accounts'] == '1') {
								//Only if email is verified
								if (!empty($user_email) AND $user_email_is_verified === true) {
									//Read existing user
									if (($user_id_tmp = email_exists($user_email)) !== false) {
										$user_data = get_userdata($user_id_tmp);
										if ($user_data !== false) {
											$user_id = $user_data->ID;
											$user_login = $user_data->user_login;

											//Refresh the meta data
											delete_metadata('user', null, 'oa_social_login_user_token', $user_token, true);
											update_user_meta($user_id, 'oa_social_login_user_token', $user_token);
											update_user_meta($user_id, 'oa_social_login_identity_provider', $user_identity_provider);

											//Refresh the cache
											wp_cache_delete($user_id, 'users');
											wp_cache_delete($user_login, 'userlogins');
										}
									}
								}
							}
						}


						//New User
						if (!is_numeric($user_id)) {
							//Username is mandatory
							if (!isset($user_login) OR strlen(trim($user_login)) == 0) {
								$user_login = $user_identity_provider . 'User';
							}

							// BuddyPress : See bp_core_strip_username_spaces()
							if (function_exists('bp_core_strip_username_spaces')) {
								$user_login = str_replace(' ', '-', $user_login);
							}

							//Username must be unique
							if (username_exists($user_login)) {
								$i = 1;
								$user_login_tmp = $user_login;
								do {
									$user_login_tmp = $user_login . ($i++);
								} while (username_exists($user_login_tmp));
								$user_login = $user_login_tmp;
							}

							//Email Filter
							$user_email = apply_filters('oa_social_login_filter_new_user_email', $user_email);

							//Email must be unique
							$placeholder_email_used = false;
							if (!isset($user_email) OR ! is_email($user_email) OR email_exists($user_email)) {
								$user_email = oa_social_login_create_rand_email();
								$placeholder_email_used = true;
							}

							//Setup the user's password
							$user_password = wp_generate_password();
							$user_password = apply_filters('oa_social_login_filter_new_user_password', $user_password);

							//Setup the user's role
							$user_role = get_option('default_role');
							$user_role = apply_filters('oa_social_login_filter_new_user_role', $user_role);

							//Build user data
							$user_fields = array(
								'user_login' => $user_login,
								'display_name' => (!empty($user_full_name) ? $user_full_name : $user_login),
								'user_email' => $user_email,
								'first_name' => $user_first_name,
								'last_name' => $user_last_name,
								'user_url' => $user_website,
								'user_pass' => $user_password,
								'role' => $user_role,
							);

							//Filter for user_data
							$user_fields = apply_filters('oa_social_login_filter_new_user_fields', $user_fields);

							//Hook before adding the user
							do_action('oa_social_login_action_before_user_insert', $user_fields, $identity);

							// Create a new user
							$user_id = wp_insert_user($user_fields);
							if (is_numeric($user_id) AND ( $user_data = get_userdata($user_id)) !== false) {
								//Refresh the meta data
								delete_metadata('user', null, 'oa_social_login_user_token', $user_token, true);

								//Save OneAll user meta-data
								update_user_meta($user_id, 'oa_social_login_user_token', $user_token);
								update_user_meta($user_id, 'oa_social_login_identity_provider', $user_identity_provider);

								//Save WordPress user meta-data
								if (!empty($user_about_me) OR ! empty($user_note)) {
									$user_description = (!empty($user_about_me) ? $user_about_me : $user_note);
									update_user_meta($user_id, 'description', $user_description);
								}

								//Email is required
								if (!empty($settings ['plugin_require_email'])) {
									//We don't have the real email
									if ($placeholder_email_used) {
										update_user_meta($user_id, 'oa_social_login_request_email', 1);
									}
								}

								//Notify Administrator
								if (!empty($settings ['plugin_notify_admin'])) {
									oa_social_login_user_notification($user_id, $user_identity_provider);
								}

								//Refresh the cache
								wp_cache_delete($user_id, 'users');
								wp_cache_delete($user_login, 'userlogins');

								//WordPress hook
								do_action('user_register', $user_id);

								//Social Login Hook
								do_action('oa_social_login_action_after_user_insert', $user_data, $identity);
							}
						}

						//Sucess
						$user_data = get_userdata($user_id);
						if ($user_data !== false) {
							//Hooks to be used by third parties
							do_action('oa_social_login_action_before_user_login', $user_data, $identity, $new_registration);

							//Update user thumbnail
							if (!empty($user_thumbnail)) {
								update_user_meta($user_id, 'oa_social_login_user_thumbnail', $user_thumbnail);
							}

							//Update user picture
							if (!empty($user_picture)) {
								update_user_meta($user_id, 'oa_social_login_user_picture', $user_picture);
							}

							//Set the cookie and login
							wp_clear_auth_cookie();
							wp_set_auth_cookie($user_data->ID, true);
							do_action('wp_login', $user_data->user_login, $user_data);

							//Where did the user come from?
							$oa_social_login_source = (!empty($_REQUEST ['oa_social_login_source']) ? strtolower(trim($_REQUEST ['oa_social_login_source'])) : '');

							//Use safe redirection?
							$redirect_to_safe = false;

							//Build the url to redirect the user to
							switch ($oa_social_login_source) {
								//*************** Registration ***************
								case 'registration':
									//Default redirection
									$redirect_to = admin_url();

									//Redirection in URL
									if (!empty($_GET ['redirect_to'])) {
										$redirect_to = $_GET ['redirect_to'];
										$redirect_to_safe = true;
									} else {
										//Redirection customized
										if (isset($settings ['plugin_registration_form_redirect'])) {
											switch (strtolower($settings ['plugin_registration_form_redirect'])) {
												//Current
												case 'current':
													$redirect_to = oa_social_login_get_current_url();
													break;

												//Homepage
												case 'homepage':
													$redirect_to = home_url();
													break;

												//Custom
												case 'custom':
													if (isset($settings ['plugin_registration_form_redirect_custom_url']) AND strlen(trim($settings ['plugin_registration_form_redirect_custom_url'])) > 0) {
														$redirect_to = trim($settings ['plugin_registration_form_redirect_custom_url']);
													}
													break;

												//Default/Dashboard
												default:
												case 'dashboard':
													$redirect_to = admin_url();
													break;
											}
										}
									}
									break;


								//*************** Login ***************
								case 'login':
									//Default redirection
									$redirect_to = home_url();

									//Redirection in URL
									if (!empty($_GET ['redirect_to'])) {
										$redirect_to = $_GET ['redirect_to'];
										$redirect_to_safe = true;
									} else {
										//Redirection customized
										if (isset($settings ['plugin_login_form_redirect'])) {
											switch (strtolower($settings ['plugin_login_form_redirect'])) {
												//Current
												case 'current':

													global $pagenow;

													//Do not redirect to the login page as this would logout the user.
													if (empty($pagenow) OR $pagenow <> 'wp-login.php') {
														$redirect_to = oa_social_login_get_current_url();
													} //In this case just go to the home page
													else {
														$redirect_to = home_url();
													}
													break;

												//Dashboard
												case 'dashboard':
													$redirect_to = admin_url();
													break;

												//Custom
												case 'custom':
													if (isset($settings ['plugin_login_form_redirect_custom_url']) AND strlen(trim($settings ['plugin_login_form_redirect_custom_url'])) > 0) {
														$redirect_to = trim($settings ['plugin_login_form_redirect_custom_url']);
													}
													break;

												//Default/Homepage
												default:
												case 'homepage':
													$redirect_to = home_url();
													break;
											}
										}
									}
									break;

								// *************** Comments ***************
								case 'comments':
									$redirect_to = oa_social_login_get_current_url() . '#comments';
									break;

								//*************** Widget/Shortcode ***************
								default:
								case 'widget':
								case 'shortcode':
									// This is a new user
									$opt_key = ($new_registration === true ? 'register' : 'login');

									//Default value
									$redirect_to = oa_social_login_get_current_url();

									//Redirection customized
									if (isset($settings ['plugin_shortcode_' . $opt_key . '_redirect'])) {
										switch (strtolower($settings ['plugin_shortcode_' . $opt_key . '_redirect'])) {
											//Current
											case 'current':
												$redirect_to = oa_social_login_get_current_url();
												break;

											//Homepage
											case 'homepage':
												$redirect_to = home_url();
												break;

											//Dashboard
											case 'dashboard':
												$redirect_to = admin_url();
												break;

											//Custom
											case 'custom':
												if (isset($settings ['plugin_shortcode_' . $opt_key . '_redirect_url']) AND strlen(trim($settings ['plugin_shortcode_' . $opt_key . '_redirect_url'])) > 0) {
													$redirect_to = trim($settings ['plugin_shortcode_' . $opt_key . '_redirect_url']);
												}
												break;
										}
									}
									break;
							}

							//Check if url set
							if (!isset($redirect_to) OR strlen(trim($redirect_to)) == 0) {
								$redirect_to = home_url();
							}

							// New User (Registration)
							if ($new_registration === true) {
								// Apply the WordPress filters
								if (empty($settings['plugin_protect_registration_redirect_url'])) {
									$redirect_to = apply_filters('registration_redirect', $redirect_to);
								}

								// Apply our filters
								$redirect_to = apply_filters('oa_social_login_filter_registration_redirect_url', $redirect_to, $user_data);
							} // Existing User (Login)
							else {
								// Apply the WordPress filters
								if (empty($settings['plugin_protect_login_redirect_url'])) {
									$redirect_to = apply_filters('login_redirect', $redirect_to, (!empty($_GET ['redirect_to']) ? $_GET ['redirect_to'] : ''), $user_data);
								}

								// Apply our filters
								$redirect_to = apply_filters('oa_social_login_filter_login_redirect_url', $redirect_to, $user_data);
							}

							//Hooks for other plugins
							do_action('oa_social_login_action_before_user_redirect', $user_data, $identity, $redirect_to);

							//Use safe redirection
							if ($redirect_to_safe === true) {
								wp_safe_redirect($redirect_to);
							} else {
								wp_redirect($redirect_to);
							}
							exit();
						}
					}
				}
			}
		}
	}

	if (function_exists('oa_social_login_request_email')) {
		remove_action('init', 'oa_social_login_request_email', 9);

		add_action('init', 'wpr_oa_social_login_request_email', 9);
	}

	/**
	 * Replace Add email Social login form
	 */
	function wpr_oa_social_login_request_email() {
//Get the current user
		$current_user = wp_get_current_user();

		//Check if logged in
		if (!empty($current_user->ID) AND is_numeric($current_user->ID)) {
			//Current user
			$user_id = $current_user->ID;

			//Check if email has to be requested
			$oa_social_login_request_email = get_user_meta($user_id, 'oa_social_login_request_email', true);
			if (!empty($oa_social_login_request_email)) {
				//Display modal dialog?
				$display_modal = true;

				//Messaging
				$message = '';

				//Read settings
				$settings = get_option('oa_social_login_settings');

				//Make sure that the email is still required
				if (empty($settings ['plugin_require_email'])) {
					//Do not display the modal dialog
					$display_modal = false;

					//Stop asking for the email
					delete_user_meta($user_id, 'oa_social_login_request_email');
				}

				//Form submitted
				if (isset($_POST) AND ! empty($_POST ['oa_social_login_action'])) {
					if ($_POST ['oa_social_login_action'] == 'confirm_email') {
						$user_email = (empty($_POST ['oa_social_login_email']) ? '' : trim($_POST ['oa_social_login_email']));
						$user_email = apply_filters('oa_social_login_filter_user_request_email', $user_email, $user_id);

						if (empty($user_email)) {
							$message = __('Please enter your email address', 'oa_social_login');
						} else {
							if (!is_email($user_email)) {
								$message = __('This email is not valid', 'oa_social_login');
							} else {
								//Read user
								$user_data = get_userdata($user_id);
								if ($user_data !== false) {
									//Store old email
									$old_user_email = $user_data->user_email;

									//Update user
									wp_update_user(array('ID' => $user_data->ID, 'user_email' => $user_email));
									delete_user_meta($user_data->ID, 'oa_social_login_request_email');

									//Set new email for hook
									$user_data->user_email = $user_email;

									//Hook after having updated the email
									do_action('oa_social_login_action_on_user_enter_email', $user_id, $user_data, $old_user_email);

									//No longer needed
									$display_modal = false;
								}
							}
						}
					}
				}

				//Display modal dialog?
				if ($display_modal === true) {
					//Read Settings
					$oa_social_login_settings = get_option('oa_social_login_settings');

					//Read the social network
					$oa_social_login_identity_provider = get_user_meta($user_id, 'oa_social_login_identity_provider', true);

					//Caption
					$caption = (isset($oa_social_login_settings ['plugin_require_email_text']) ? $oa_social_login_settings ['plugin_require_email_text'] : __('<strong>We unfortunately could not retrieve your email address from %s.</strong> Please enter your email address in the form below in order to continue.', 'oa_social_login'));

					// Create Nonce
					$oa_nonce = wp_create_nonce('request_email_cancel-' . $user_id);

					// Compute logout url
					$logout_url = wp_logout_url(oa_social_login_get_current_url());
					$logout_url .= ((parse_url($logout_url, PHP_URL_QUERY) ? '&' : '?') . 'oa_action=request_email_cancel&oa_nonce=' . $oa_nonce);

					//Add CSS
					oa_social_login_add_site_css();

					//Show email request form
					?>
					<div id="oa_social_login_overlay"></div>
					<div id="oa_social_login_modal">
						<div class="oa_social_login_modal_outer">
							<div class="oa_social_login_modal_inner">
								<div class="oa_social_login_modal_title">
									<?php
									printf(__('You have successfully connected with %s!', 'oa_social_login'), '<strong>' . $oa_social_login_identity_provider . '</strong>');
									?>
								</div>
								<?php
								if (strlen(trim($caption)) > 0) {
									?>
									<div class="oa_social_login_modal_notice"><?php echo str_replace('%s', $oa_social_login_identity_provider, $caption); ?></div>
									<?php
								}
								?>
								<div class="oa_social_login_modal_body">
									<div class="oa_social_login_modal_subtitle">
										<?php _e('Please enter your email address', 'oa_social_login'); ?>
										:
									</div>
									<form method="post"
									      action="">
										<fieldset>
											<div class="oa_social_login_input">
												<input type="text"
												       name="oa_social_login_email"
												       class="oa_social_login_confirm_text"
												       value="<?php echo(!empty($_POST ['oa_social_login_email']) ? oa_social_login_esc_attr($_POST ['oa_social_login_email']) : ''); ?>"/>
												<input type="hidden"
												       name="oa_social_login_action"
												       value="confirm_email"
												       size="30"/>
											</div>
											<div class="oa_social_login_modal_error">
												<?php echo $message; ?>
											</div>
											<div class="oa_social_login_buttons">
												<input class="oa_social_login_button"
												       id="oa_social_login_button_confirm"
												       name="oa_social_login_confirm_btn"
												       type="submit"
												       value="<?php _e('Confirm', 'oa_social_login'); ?>"/>
												<a href="<?php echo esc_url($logout_url); ?>"
												   class="oa_social_login_button"
												   id="oa_social_login_button_cancel"><?php _e('Cancel', 'oa_social_login'); ?></a>
											</div>
										</fieldset>
									</form>
								</div>
							</div>
						</div>
					</div>
					<?php
				}
			}
		}
	}

	/**
	 * Fix hanging user roles & caps.
	 * This is for users who are removed from a site but remain registered on the network.
	 * This scenario will cause users to be unable to log in.
	 *
	 * @param  WP_User $user_data
	 * @param  $identity
	 * @param  $new_registration
	 */
	function wpr_user_assign_default_role($user_data, $identity, $new_registration) {
		if (empty($user_data->roles)) {
			$user_data->set_role(get_option('default_role'));
		}
	}

	add_action('oa_social_login_action_before_user_login', 'wpr_user_assign_default_role', 10, 3);

	add_filter('the_title', 'wpr_fix_wp_automatic_title', 10, 2);

	/**
	 * Fix wp automatic title
	 *
	 * @param      $title
	 * @param null $id
	 *
	 * @return string
	 */
	function wpr_fix_wp_automatic_title($title, $id = null) {
		$is_broyhill = get_post_meta($id, 'wpr_broyhill_article', true);
		if ($is_broyhill) {
			#$title = preg_replace( '/^[^:]*:\s*/', '', $title, 1 );
		}

		$title = str_replace('BROYHILL COMMENTS', '', $title);

		// Fix double-encoding issue.
		preg_match_all('/&amp;([^;]+;)/', $title, $entities);
		if (!empty($entities)) {
			foreach ($entities[0] as $i => $entity) {
				$replace = html_entity_decode($entity);
				$title = str_replace($entity, $replace, $title);
			}
		}

		return $title;
	}

	function logo_setup() {
		$defaults = array(
			'height' => 128,
			'width' => 91,
			'flex-height' => true,
			'flex-width' => true,
			'header-text' => array('site-title', 'site-description'),
		);
		add_theme_support('custom-logo', $defaults);
	}

	add_action('after_setup_theme', 'logo_setup');

	add_filter('oa_social_login_default_css', 'oa_social_login_set_custom_css');
	add_filter('oa_social_login_widget_css', 'oa_social_login_set_custom_css');
	add_filter('oa_social_login_link_css', 'oa_social_login_set_custom_css');

	function oa_social_login_set_custom_css($css_theme_uri) {
		//Replace this URL by an URL to a CSS file on your own server
		$css_theme_uri = get_stylesheet_directory_uri() . '/assets/css/social_login_modal.css';

		// Done
		return $css_theme_uri;
	}

	add_filter('gform_field_value_refurl', 'wpr_referral_url');

	/**
	 * Add referral URL
	 *
	 * @param $form
	 *
	 * @return string
	 */
	function wpr_referral_url($form) {
		$refurl = wpr_fetch_current_Page_URL();

		// Return that value to the form
		return esc_url_raw($refurl);
	}

	/**
	 * Fetch current URL
	 *
	 * @return string
	 */
	function wpr_fetch_current_Page_URL() {
		$pageURL = 'http';
		if ($_SERVER["HTTPS"] == "on") {
			$pageURL .= "s";
		}
		$pageURL .= "://";
		$pageURL .= $_SERVER["SERVER_NAME"] . $_SERVER["REQUEST_URI"];

		return $pageURL;
	}

	/**
	 * Clean string text
	 *
	 * @param       $string
	 * @param array $allowedTags
	 *
	 * @return mixed|string
	 */
	function wpr_cleanse_string($string, $allowedTags = array()) {
		$string = stripslashes($string);

		$string = wp_kses($string, $allowedTags);

		// ============
		// Remove MS Word Special Characters
		// ============

		$search = array(
			'&acirc;€“',
			'&acirc;€œ',
			'&acirc;€˜',
			'&acirc;€™',
			'&Acirc;&pound;',
			'&Acirc;&not;',
			'&acirc;„&cent;',
		);
		$replace = array('-', '&ldquo;', '&lsquo;', '&rsquo;', '&pound;', '&not;', '&#8482;');

		$string = str_replace($search, $replace, $string);
		$string = str_replace('&acirc;€', '&rdquo;', $string);

		$search = array(
			"&#39;",
			"\xc3\xa2\xc2\x80\xc2\x99",
			"\xc3\xa2\xc2\x80\xc2\x93",
			"\xc3\xa2\xc2\x80\xc2\x9d",
			"\xc3\xa2\x3f\x3f",
		);
		$resplace = array("'", "'", ' - ', '"', "'");

		$string = str_replace($search, $replace, $string);

		$quotes = array(
			"\xC2\xAB" => '"',
			"\xC2\xBB" => '"',
			"\xE2\x80\x98" => "'",
			"\xE2\x80\x99" => "'",
			"\xE2\x80\x9A" => "'",
			"\xE2\x80\x9B" => "'",
			"\xE2\x80\x9C" => '"',
			"\xE2\x80\x9D" => '"',
			"\xE2\x80\x9E" => '"',
			"\xE2\x80\x9F" => '"',
			"\xE2\x80\xB9" => "'",
			"\xE2\x80\xBA" => "'",
			"\xe2\x80\x93" => "-",
			"\xc2\xb0" => "°",
			"\xc2\xba" => "°",
			"\xc3\xb1" => "&#241;",
			"\x96" => "&#241;",
			"\xe2\x81\x83" => '&bull;',
		);
		$string = strtr($string, $quotes);
		/*
 // Use the below to get the byte of the special char and put it in the array above + the replacement.

 if (strpos($string, "Live Wave Buoy Data") !== false)
 {
 for ($i=strpos($string, "Live Wave Buoy Data") ; $i<strlen($string) ; $i++) {
 $byte = $string[$i];
 $char = ord($byte);
 printf('%s:0x%02x ', $byte, $char);
 }
 }
 exit;
*/
		// ============
		// END
		// ============

		return $string;
	}

	add_action('wp_head', 'wpr_add_facebook_pixel');

	/**
	 * Add Facebook pixel in header
	 */
	function wpr_add_facebook_pixel() {
	if (is_category('bmw')) {
		?>
		<!-- Facebook Pixel Code -->
		<script>
			!function (f, b, e, v, n, t, s) {
				if (f.fbq)
					return;
				n = f.fbq = function () {
					n.callMethod ?
						n.callMethod.apply(n, arguments) : n.queue.push(arguments)
				};
				if (!f._fbq)
					f._fbq = n;
				n.push = n;
				n.loaded = !0;
				n.version = '2.0';
				n.queue = [];
				t = b.createElement(e);
				t.async = !0;
				t.src = v;
				s = b.getElementsByTagName(e)[0];
				s.parentNode.insertBefore(t, s)
			}(window, document, 'script',
				'https://connect.facebook.net/en_US/fbevents.js');
			fbq('init', '1278599232285193');
			fbq('track', 'PageView');
			fbq('track', 'ViewContent');
		</script>
		<noscript>
			<img height="1"
			     width="1"
			     style="display:none"
			     src="https://www.facebook.com/tr?id=1278599232285193&ev=PageView&noscript=1"
			/>
		</noscript>
		<!-- End Facebook Pixel Code -->
	<?php
	}

	if (is_category('neiman-marcus')) {
	?>
		<!-- Facebook Pixel Code -->
		<script>
			!function (f, b, e, v, n, t, s) {
				if (f.fbq)
					return;
				n = f.fbq = function () {
					n.callMethod ?
						n.callMethod.apply(n, arguments) : n.queue.push(arguments)
				};
				if (!f._fbq)
					f._fbq = n;
				n.push = n;
				n.loaded = !0;
				n.version = '2.0';
				n.queue = [];
				t = b.createElement(e);
				t.async = !0;
				t.src = v;
				s = b.getElementsByTagName(e)[0];
				s.parentNode.insertBefore(t, s)
			}(window, document, 'script',
				'https://connect.facebook.net/en_US/fbevents.js');
			fbq('init', '1948376428826124');
			fbq('track', 'PageView');
			fbq('track', 'ViewContent');
		</script>
		<noscript>
			<img height="1"
			     width="1"
			     style="display:none"
			     src="https://www.facebook.com/tr?id=1948376428826124&ev=PageView&noscript=1"
			/>
		</noscript>
		<!-- End Facebook Pixel Code -->
		<?php
	}
	}

	/**
	 * Add metabox
	 */
	if (is_admin()) {
		add_action('load-post.php', 'wpr_init_spin_metabox');
	}

	function wpr_init_spin_metabox() {
		add_action('add_meta_boxes', 'wpr_register_spin_metabox');
	}

	/**
	 * Define metabox settings
	 */
	function wpr_register_spin_metabox() {
		add_meta_box('wpr_spun_to', __('The current article has been spun and added as a comment on the following articles:'), 'wpr_spun_to_metabox', 'post', 'advanced', 'default', array('taxonomy' => 'category'));

		add_meta_box('wpr_spun_from', __('The comment on this article comes from a spun version of:'), 'wpr_spun_from_metabox', 'post', 'advanced', 'default', array('taxonomy' => 'category'));

		add_meta_box('wpr_title_changed', __('Title of the post came from:'), 'wpr_title_changed_metabox', 'post', 'advanced', 'default', array('taxonomy' => 'category'));
	}

	/**
	 * Display metabox for comments spun from an article.
	 *
	 * @param $post
	 */
	function wpr_spun_to_metabox($post) {
		$args = array(
			'meta_key' => 'wpr_spinned_edd_article',
			'meta_value' => $post->ID,
		);

		$query = new WP_Query($args);
		if ($query->have_posts()) {
			echo '<ul>';
			while ($query->have_posts()) {
				$query->the_post();

				echo sprintf('<li><a href="%s" target="_blank">%s</a></li>', get_edit_post_link(get_the_ID()), get_the_title());
			}
			echo '</ul>';
		} else {
			echo '<p>' . __('None') . '</p>';
		}
	}

	/**
	 * Display metabox for the article from which the current article comment was spun.
	 *
	 * @param $post
	 */
	function wpr_spun_from_metabox($post) {
		$spun_from_id = absint(get_post_meta($post->ID, 'wpr_spinned_edd_article', true));

		if ($spun_from_id && get_post($spun_from_id)) {
			echo '<ul>';
			echo sprintf('<li><a href="%s" target="_blank">%s</a></li>', get_edit_post_link($spun_from_id), get_the_title($spun_from_id));
			echo '</ul>';
		} else {
			echo '<p>' . __('None') . '</p>';
		}
	}

	/**
	 * Title was take from
	 *
	 * @param $post
	 */
	function wpr_title_changed_metabox($post) {
		$title_source = get_post_meta($post->ID, 'wpr_title_changed', true);
		if ($title_source && strlen($title_source) > 2) {
			echo '<p>' . esc_attr($title_source) . '</p>';
		} else {
			echo '<p>' . __('None') . '</p>';
		}
	}

	/**
	 * @param $beginning
	 * @param $end
	 * @param $string
	 *
	 * @return mixed
	 */
	function delete_all_between($beginning, $end, $string) {
		$beginningPos = strpos($string, $beginning);
		$endPos = strripos($string, $end);
		if ($beginningPos === false || $endPos === false) {
			return $string;
		}

		$textToDelete = substr($string, $beginningPos, ($endPos + strlen($end)) - $beginningPos);

		return str_replace($textToDelete, '', $string);
	}

	// Disable the email change notification.
	add_filter('send_email_change_email', '__return_false');

	/**
	 * Fix social profile images for WP User Avatar Pro.
	 *
	 * @param  string $avatar
	 * @param  mixed $id_or_email
	 * @param  mixed $size
	 * @param  string $align
	 * @param  string $alt
	 *
	 * @return string
	 */
	function wpr_fix_wpuap_social_avatars($avatar, $id_or_email, $size, $align, $alt) {
		if (strpos($avatar, 'licdn') !== false) {
			// Revert modifications to the URL of LinkedIn profile images.
			$alignclass = !empty($align) && ('left' === $align || 'right' === $align || 'center' === $align) ? ' align' . $align : ' alignnone';
			$avatar = str_replace('wp-user-avatar wp-user-avatar-' . $size . $alignclass . ' photo', 'photo', $avatar);
		} elseif (strpos($avatar, 'graph.facebook') !== false) {
			// Grab larger profile images from Facebook.
			$avatar = str_replace('type=square', 'type=large', $avatar);
		}

		return $avatar;
	}

	add_filter('get_wp_user_avatar', 'wpr_fix_wpuap_social_avatars', 10, 5);

	/**
	 * Add media regenerate link in post publish metabox.
	 *
	 * @param object $post
	 */
	function wpr_add_post_media_regen_link($post) {
		?>
		<div class="misc-pub-section">
			<a href="<?php echo add_query_arg('wpr_regen_media', $post->ID, get_permalink($post->ID)); ?>"
			   target="_blank"><?php esc_html_e('Regenerate offer media', 'wpr'); ?></a>
		</div>
		<?php
	}

	add_action('post_submitbox_misc_actions', 'wpr_add_post_media_regen_link', 999);

	/**
	 * Regenerate post media (featured image and video).
	 */
	function wpr_post_media_regen() {
		if (empty($_GET['wpr_regen_media'])) {
			return;
		}

		$post_id = absint($_GET['wpr_regen_media']);

		// Delete featured image.
		delete_post_meta($post_id, '_thumbnail_id');

		// Delete remote featured image.
		delete_post_meta($post_id, 'wpr_remote_featured_img');

		// Delete image saved for use on social media.
		delete_post_meta($post_id, 'wpr_social_image');

		$original_link = get_post_meta($post_id, 'original_link', true);

		if (!empty($original_link)) {
			delete_post_meta($post_id, 'original_link');
			delete_post_meta($post_id, 'wpr_remote_video_html');
			add_post_meta($post_id, 'original_link', $original_link);
		}

		wp_redirect(get_permalink($post_id), 302);
	}

	add_action('template_redirect', 'wpr_post_media_regen', 10, 1);

	add_filter('posts_where', 'publish_later_on_feed');

	/**
	 * Display new articles in feed with delay in order to finish the spin title/content
	 *
	 * @param $where
	 *
	 * @return string
	 */
	function publish_later_on_feed($where) {
		global $wpdb;

		if (is_feed()) {
			// timestamp in WP-format
			$now = gmdate('Y-m-d H:i:s');

			// value for wait; + device
			$wait = '240'; // integer
			$device = 'MINUTE';

			// add SQL-sytax to default $where
			$where .= " AND TIMESTAMPDIFF($device, $wpdb->posts.post_date_gmt, '$now') > $wait ";
		}

		return $where;
	}

	#add_filter( 'nav_menu_link_attributes', 'add_popup_link', 10, 3 );

	function add_popup_link($atts, $item, $args) {
		if ($args->theme_location == 'primary' && in_array('wpr_open_login_popup', (array) $item->classes)) {
			$atts['onclick'] = "jQuery('#popmake-33647').popmake('open', function () {jQuery('#pum-33647').css('opacity', '1');})";
		}

		return $atts;
	}

	add_action('display_cta_buttons_single_offer_overlay', 'create_cta_buttons');

	function create_cta_buttons() {

		if (is_user_logged_in()) {
			$contact_icon = get_image_path('offer_grid_contact_me_white.png');
		} else {
			$contact_icon = get_image_path('offer_grid_contact_me.png');
		}

		if (is_user_logged_in()) {
			$email_icon = get_image_path('offer_grid_email_me_white.png');
		} else {
			$email_icon = get_image_path('offer_grid_email_me.png');
		}

		if (is_user_logged_in()) {
			$make_app_icon = get_image_path('offer_grid_make_appointment_white.png');
		} else {
			$make_app_icon = get_image_path('offer_grid_make_appointment.png');
		}

		if (is_user_logged_in()) {
			$price_icon = get_image_path('offer_grid_price_white.png');
		} else {
			$price_icon = get_image_path('offer_grid_price.png');
		}

		/* $contact_icon = get_image_path('offer_grid_contact_me.svg');
 $email_icon = get_image_path('offer_grid_email_me.svg');
 $make_app_icon = get_image_path('offer_grid_make_appointment.svg');
 $price_icon = get_image_path('offer_grid_price.svg'); */
		$output = '';
		if (!is_user_logged_in()) {
			$loginClass = ' popmake-33647 white-bg';
		} else {
			$loginClass = '';
		}

		$image_gif = get_image_path('circle.gif');
		?>

		<span class="wrapButtons" style="display:none;">
        <div class="cta_buttons setCenter">
            <div class="inCenter" id="inCenter">
    			<button class="inCenter-button" id="inCenterButton">x</button>
                <div class="wpr-single-offer-buttons wpr-contact-me single_button iframe-below-icons <?php echo $loginClass; ?>"
                     data-do="contact" data-postid="<?php echo get_the_id(); ?>">
                    <div class="icon_container a">
                        <img src="<?php echo $contact_icon; ?>"
                             class="cta_icon">
                    </div>
                    <p>Contact Me</p>
                </div>
                <div class="wpr-single-offer-buttons wpr-send-info wpr-learn-more single_button iframe-below-icons <?php echo $loginClass; ?>"
                     data-do="learn" data-postid="<?php echo get_the_id(); ?>">
                    <div class="icon_container b">
                        <img src="<?php echo $email_icon; ?>"
                             class="cta_icon">
                    </div>
                    <p>Send me info</p>
                </div>
                <div class="wpr-single-offer-buttons wpr-make-appointment wpr-set-appointment single_button iframe-below-icons <?php echo $loginClass; ?>"
                     data-do="appointment" data-postid="<?php echo get_the_id(); ?>">
                    <div class="icon_container c">
                        <img src="<?php echo $make_app_icon; ?>"
                             class="cta_icon">
                    </div>
                    <p class="make-a">Make Appointment</p>
                </div>
                <div class="wpr-single-offer-buttons wpr-price-it wpr-price-request single_button iframe-below-icons <?php echo $loginClass; ?>"
                     data-do="price" data-postid="<?php echo get_the_id(); ?>">
                    <div class="icon_container d">
                        <img src="<?php echo $price_icon; ?>"
                             class="cta_icon">
                    </div>
                    <p>Price It</p>
                </div>
            </div>
        </div>
        <?php
        if ( wp_is_mobile() ) {
	        ?>

	        <div class="gif" id="gif">

            <div id="gifimg" class="pulse"></div>
        </div>

	        <?php
        }
        ?>

    </span>
		<?php
	}

	add_action('display_cta_buttons_grid_overlay', 'create_cta_grid_buttons');

	function create_cta_grid_buttons() {
		$contact_icon = get_image_path('offer_grid_contact_me.svg');
		$email_icon = get_image_path('offer_grid_email_me.svg');
		$make_app_icon = get_image_path('offer_grid_make_appointment.svg');
		$price_icon = get_image_path('offer_grid_price.svg');


		if (!is_user_logged_in()) {
			$loginClass = ' popmake-33647 ';
		} else {
			$loginClass = '';
		}
		$dispalyCTA = true;
		$categories = get_the_category(get_the_id());
		foreach ($categories as $cat) {
			if (in_array($cat->term_id, [36, 37, 38, 39])) {
				$dispalyCTA = false;
			}
		}
		if ($dispalyCTA) {
			?>
			<span class="wrapButtons gridButtons">
            <div class="cta_buttons setCenter">
                <div class="inCenter">
                    <div class="wpr-single-offer-buttons wpr-contact-me single_button <?php echo $loginClass; ?>"
                         data-do="contact" data-postid="<?php echo get_the_id(); ?>">
                        <div class="icon_container e">
                            <img src="<?php echo $contact_icon; ?>"
                                 class="cta_icon">
                        </div>
                        <p>Contact</p>
                    </div>
                    <div class="wpr-single-offer-buttons wpr-send-info wpr-learn-more single_button <?php echo $loginClass; ?>"
                         data-do="learn" data-postid="<?php echo get_the_id(); ?>">
                        <div class="icon_container f">
                            <img src="<?php echo $email_icon; ?>"
                                 class="cta_icon">
                        </div>
                        <p>Email Me</p>
                    </div>
                    <div class="wpr-single-offer-buttons wpr-make-appointment wpr-set-appointment single_button <?php echo $loginClass; ?>"
                         data-do="appointment" data-postid="<?php echo get_the_id(); ?>">
                        <div class="icon_container g">
                            <img src="<?php echo $make_app_icon; ?>"
                                 class="cta_icon">
                        </div>
                        <p>Make Appt</p>
                    </div>
                    <div class="wpr-single-offer-buttons wpr-price-it wpr-price-request single_button <?php echo $loginClass; ?>"
                         data-do="price" data-postid="<?php echo get_the_id(); ?>">
                        <div class="icon_container h">
                            <img src="<?php echo $price_icon; ?>"
                                 class="cta_icon">
                        </div>
                        <p>Price It</p>
                    </div>
                </div>
            </div>
        </span>
			<?php
		}
	}

	function get_image_path($filename = '') {
		if (empty($filename)) {
			return __return_empty_string();
		}

		if (filter_var($filename, FILTER_VALIDATE_URL) === false) {
			return get_stylesheet_directory_uri() . '' . IMAGES_DIR_PATH . $filename;
		}

		return $filename;
	}

	add_filter('wp_nav_menu_objects', 'my_dynamic_menu_items');

	function my_dynamic_menu_items($menu_items) {
		foreach ($menu_items as $menu_item) {
			if ('#first_name#' == $menu_item->title) {
				if (is_user_logged_in()) {
					$user = wp_get_current_user();
					$name = $user->user_firstname;
					if ($name == '') {
						$name = $user->firstname;
						print_r($name);
					}
					if ($name == '') {
						$name = ucwords(explode(" ", $user->display_name)[0]);
					}
					if ($name == $user->display_name) {
						$name = ucwords(explode(".", $user->display_name)[0]);
					}
					if ($name == $user->display_name) {
						$name = $user->user_login;
					}
					$menu_item->title = $name;
				}
			} else if ('#user_avatar_and_name#' == $menu_item->title) {
				if (is_user_logged_in()) {
					$menu_item->classes[] = 'about_user_section';
					$user = wp_get_current_user();
					$name = $user->first_name . ' ' . $user->last_name;
					$link = '<a href="' . get_permalink(get_page_by_path('my-profile')) . '" class="full_name_link">';
					$name_with_p = '<p class="full_name_text_menu">' . $name . '</p>';
					$html = '<div class="about_user_section_content">';

					$html .= $link . '<div class="about_user_section_avatar">';
					if (function_exists('get_wp_user_avatar')) {
						$html .= get_wp_user_avatar($user->ID, 60);
					}
					$html .= '</div>';
					$html .= '<div class="about_user_section_name">';
					$html .= $name_with_p;
					$html .= '</div>';
					$html .= '</div></a>';
					$menu_item->title = $html;
				}
			}
		}

		return $menu_items;
	}

	function register_mobile_menu() {
		register_nav_menu('mobile', __('Mobile Menu'));
	}

	add_action('init', 'register_mobile_menu');

	add_action('wp_ajax_add_remove_brands_offers', 'add_remove_brands_offers');

	function add_remove_brands_offers() {
		$selected_brands_raw = get_user_meta(get_current_user_id(), 'wpr_selected_brands');
		$selected_brands = $selected_brands_raw[0];
		$brand_id = $_REQUEST['brand_id'];
		$todo = $_REQUEST['todo'];
		if ($todo == 'add') {
			array_push($selected_brands, $brand_id);
		} else {
			if (($key = array_search($brand_id, $selected_brands)) !== false) {
				unset($selected_brands[$key]);
			}
		}
		$res = update_user_meta(get_current_user_id(), 'wpr_selected_brands', $selected_brands);
		echo json_encode(['todo' => $todo, 'response' => $res]);
		die;
	}

	function filter_post_data($post_id ) {
		if ( 'wpr_custom_title' === get_post_type( $post_id ) ) {
			return;
		}

		$cat=wp_get_post_categories( $post_id );
		$child= get_category($cat[0]);
		$parent = $child->parent;
		$ntitle = wpr_fetch_random_title( $parent, $post_id );

		$iftitle= get_post_meta($post_id, 'wpr_spin_the_title', 1);

		if($ntitle && !$iftitle) {

			global $wpdb;
			$wpdb->update( $wpdb->posts, array( 'post_title' =>  $ntitle ), array( 'ID' => $post_id ) );

			update_post_meta( $post_id, 'wpr_spin_the_title', $ntitle );

		}

	}

	add_action( 'save_post' , 'filter_post_data');

	add_action( 'init', 'luxury_get_custom_rrs_feed');
	function luxury_get_custom_rrs_feed() {
		global $post;
		if ( isset( $_GET['luxury_custom_feed'] ) ) {
			?>
			<div class="list-items">
				<?php
				$args = array(
					'post_type' => 'post',
					'post_status' => 'publish',
					'posts_per_page' => 20,
				);
				if ( isset( $_GET['cat'] ) ) {
					$args = array(
						'post_type' => 'post',
						'post_status' => 'publish',
						'tax_query' => array(
							array(
								'taxonomy' => 'category',
								'field'    => 'slug',
								'terms'    => array( $_GET['cat'] ),
							),
						)
					);
				}
				$add_space = false;
				if( isset( $_GET['fb_source'] ) ) {
					$add_space = true;
				}
				$post_query = new WP_Query($args);
				// print_r( $post_query );
				if( $post_query->have_posts() ) {
					while( $post_query->have_posts() ) {
						$post_query->the_post();
						setup_postdata( get_the_ID() );
						$post_thumnail = \LBT\Post::get_thumbnail( get_the_ID() );
						if ( ! $post_thumnail ) {
							continue;
						}

						$temp_data = wpr_update_ed_og_title_linkedin( array(), $post, -1, 0 );
						$image_attributes = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' );
						if ( $image_attributes[1] < 500 && ! empty( $image_attributes[1] ) ) {
							continue;
						}
						ob_start();
						the_excerpt();
						$excerpt = ob_get_clean();
						$details = explode( ' - ', $temp_data['title'] );
						$desc = wpr_display_ed_comment(get_the_ID());
						if( $add_space ) {
							$temp_desc = explode( ' ', $desc );
							$n_desc = '<br><br>';
							foreach ( $temp_desc as $k => $t_d ) {
								if( $k > 20 ) {
									break;
								}
								$n_desc .= $t_d . ' ';
							}
							$n_desc .= '... read more';
							$desc = $n_desc;
						}
						?>

						<div class="post-item-rss">
							<div class="item-permalink"><a href="<?php the_permalink(); ?>">Link</a></div>
							<?php if ( 0 && get_post_meta( get_the_ID(), 'wpr_remote_featured_img', true ) ) { ?>
								<div class="item-thumbnail"><img src="<?php echo get_post_meta( get_the_ID(), 'wpr_remote_featured_img', true ); ?>"/></div>
							<?php } else { ?>
								<div class="item-thumbnail"><img src="<?php echo get_post_meta( get_the_ID(), 'lbt_img', true ); ?>"/></div>
							<?php } ?>
							<div class="item-title"><strong>Broyhill Comments:</strong> "<?php the_title(); ?>"</div>
							<div class="item-description">
								<?php echo $desc; ?>
							</div>
						</div>
						<?php
					}
					wp_reset_postdata();
				}
				?>
			</div>
			<?php
			exit();
		} else if( isset( $_GET['luxury_custom_rss_feed'] ) ) {
			?>
			<?xml version="1.0" encoding="UTF-8" ?>
			<rss version="2.0">
				<channel>
					<title>LBT custom feed</title>
					<link>https://luxurybuystoday.com</link>
					<description></description>
					<?php
					$args = array(
						'post_type' => 'post',
						'post_status' => 'publish',
						'posts_per_page' => 20,
					);
					if ( isset( $_GET['cat'] ) ) {
						$args = array(
							'post_type' => 'post',
							'post_status' => 'publish',
							'tax_query' => array(
								array(
									'taxonomy' => 'category',
									'field'    => 'slug',
									'terms'    => array( $_GET['cat'] ),
								),
							)
						);
					}
					$add_space = false;
					if( isset( $_GET['fb_source'] ) ) {
						$add_space = true;
					}
					$post_query = new WP_Query($args);
					// print_r( $post_query );
					if( $post_query->have_posts() ) {
						while( $post_query->have_posts() ) {
							$post_query->the_post();
							setup_postdata( get_the_ID() );
							$post_thumnail = \LBT\Post::get_thumbnail( get_the_ID() );
							if ( ! $post_thumnail ) {
								continue;
							}

							$temp_data = wpr_update_ed_og_title_linkedin( array(), $post, -1, 0 );
							$image_attributes = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' );
							if ( $image_attributes[1] < 500 && ! empty( $image_attributes[1] ) ) {
								continue;
							}
							ob_start();
							the_excerpt();
							$excerpt = ob_get_clean();
							$details = explode( ' - ', $temp_data['title'] );
							$desc = wpr_display_ed_comment(get_the_ID());
							if( $add_space ) {
								$temp_desc = explode( ' ', $desc );
								$n_desc = '<br><br>';
								foreach ( $temp_desc as $k => $t_d ) {
									if( $k > 20 ) {
										break;
									}
									$n_desc .= $t_d . ' ';
								}
								$n_desc .= '... read more';
								$desc = $n_desc;
							}
							?>
							<item>
								<title><?php echo $details[0]; ?> - <?php echo str_replace( 'Read more...', '', strip_tags( $excerpt ) ); ?></title>
								<link><?php the_permalink(); ?></link>
								<?php if ( 0 && get_post_meta( get_the_ID(), 'wpr_remote_featured_img', true ) ) { ?>
									<image>
										<url><?php echo get_post_meta( get_the_ID(), 'wpr_remote_featured_img', true ); ?></url>
									</image>
									<div class="item-thumbnail"><img src=""/></div>
								<?php } else { ?>
									<image>
										<url><?php echo get_post_meta( get_the_ID(), 'lbt_img', true ); ?></url>
									</image>
								<?php } ?>
								<description>
									"<?php the_title(); ?>"<br/>BROYHILL Comments & Offer Rating: <?php echo str_replace( 'Broyhill Rating:', '' , $details[1] ); ?><br/><?php echo $desc; ?>
								</description>
							</item>
							<?php
						}
						wp_reset_postdata();
					}
					?>
				</channel>

			</rss>
			<?php
				exit();
		}
	}
	add_action( 'before_delete_post', 'luxury_remove_attachments_on_delete', 10 );
	/**
	 * Remove media when post is being deleted.
	 *
	 * @param int $post_id The post that's been removed.
	 */
	function luxury_remove_attachments_on_delete( $post_id ) {
		if ( $post_id ) {
			if ( has_post_thumbnail( $post_id ) ) {
				$attachment_id = get_post_thumbnail_id( $post_id );
				wp_delete_attachment( $attachment_id, true );
			}
			$media = get_attached_media( '', $post_id );
			foreach( $media as $post_id => $post ) {
				wp_delete_attachment( $post_id, true );
			}
		}
	}

	function luxury_fix_media_file_names($filename) {

		if ( isset( $_REQUEST['post_id'] ) ) {
			$post_id =  (int)$_REQUEST['post_id'];
		} else {
			$post_id = 0;
		}

		$info = pathinfo( $filename );
		$ext  = empty( $info['extension'] ) ? '.jpg' : '.' . $info['extension'];
		if ( strlen( $ext ) > 7 ) {
			$ext = '.jpg';
		}
		if ( false !== strpos( $filename, '.jpg' ) ) {
			$filename = str_replace( '.jpg', '', $filename );
		}
		$name = basename( $filename );

		if ( $post_id > 0 ) {
			$return = $post_id . '-' . sanitize_title( $name ) . $ext;
			// $return = $post_id . '-' . sanitize_title( $name );
		} else {
			$return = sanitize_title( $name ) . $ext;
			// $return = sanitize_title( $name );
		}
		if ( false !== strpos( $return, '.jpg.jpg' ) ) {
			$return = str_replace( '.jpg.jpg', '.jpg', $return );
		}
		return $return;
	}
	add_filter( 'sanitize_file_name', 'luxury_fix_media_file_names', 10 );


	function luxury_media_add_attached_post_dropdown() {
		$scr = get_current_screen();

		if ( $scr->base !== 'upload' ) {
			return;
		}

		$attached_post   = filter_input(INPUT_GET, 'attached_post', FILTER_SANITIZE_STRING );
		$selected = (int)$attached_post;
		?>
		<select name="attached_post" id="attached_post" class="">
			<option value="0" <?php if( $selected === 0 ) { ?> selected="selected" <?php } ?>>All Media</option>
			<option value="1"  <?php if( $selected === 1 ) { ?> selected="selected" <?php } ?>>Detached Media</option>
		</select>
		<?php
	}
	add_action('restrict_manage_posts', 'luxury_media_add_attached_post_dropdown');

	function luxury_attached_post_filter( $query ) {
		if ( is_admin() && $query->is_main_query() ) {
			if ( isset ( $_GET['attached_post'] ) && $_GET['attached_post'] == 1 ) {
				$query->set( 'post_parent', 0 );
			}
		}
	}
	add_action('pre_get_posts','luxury_attached_post_filter');

	// Force post slug to be auto generated from title on save
	function myplugin_update_slug( $data, $postarr ) {
		if ( ! in_array( $data['post_status'], array('pending', 'auto-draft' ) ) ) {
			$data['post_name'] = wp_unique_post_slug( sanitize_title( $data['post_title'] ), $postarr['ID'], $data['post_status'], $data['post_type'], $data['post_parent'] );
		}

		return $data;
	}
	add_filter( 'wp_insert_post_data', 'myplugin_update_slug', 99, 2 );


	function slug_save_post_callback( $post_ID, $post, $update ) {
		// allow 'publish', 'draft', 'future'
		if ($post->post_type != 'post' || $post->post_status == 'auto-draft')
			return;

		// only change slug when the post is created (both dates are equal)
		if ($post->post_date_gmt != $post->post_modified_gmt)
			return;

		// use title, since $post->post_name might have unique numbers added
		$new_slug = sanitize_title( $post->post_title, $post_ID );

		if ($new_slug == $post->post_name)
			return; // already set

		// unhook this function to prevent infinite looping
		remove_action( 'save_post', 'slug_save_post_callback', 10, 3 );
		// update the post slug (WP handles unique post slug)
		wp_update_post( array(
			'ID' => $post_ID,
			'post_name' => $new_slug
		));
		// re-hook this function
		add_action( 'save_post', 'slug_save_post_callback', 10, 3 );
	}
	add_action( 'save_post', 'slug_save_post_callback', 10, 3 );

	//Handle data retrieved from a social network profile
	function oa_social_login_store_extended_data ($user_data, $identity)
	{
		// $user_data is an object that represents the newly added user
		// The format is similar to the data returned by $user_data = get_userdata ($user_id);

		// $identity is an object that contains the full social network profile


		update_user_meta ($user_data->ID, 'first_name', "$identity->givenName");
		update_user_meta ($user_data->ID, 'last_name', $identity->familyName);
		update_user_meta ($user_data->ID, 'street', $identity->street);
		update_user_meta ($user_data->ID, 'city', $identity->city);
		update_user_meta ($user_data->ID, 'state', $identity->state);
		update_user_meta ($user_data->ID, 'zip_code', $identity->zip_code);
		update_user_meta ($user_data->ID, 'phone', $identity->phone);
	}

	//This action is called whenever Social Login adds a new user
	add_action ('oa_social_login_action_after_user_insert', 'oa_social_login_store_extended_data', 10, 2);

		/*********Custom mobile check function*********/
		function custom_wp_is_mobile() {
			static $is_mobile;

			if ( isset($is_mobile) )
				return $is_mobile;

			if ( empty($_SERVER['HTTP_USER_AGENT']) ) {
				$is_mobile = false;
			} elseif (
				strpos($_SERVER['HTTP_USER_AGENT'], 'Android') !== false
				|| strpos($_SERVER['HTTP_USER_AGENT'], 'Silk/') !== false
				|| strpos($_SERVER['HTTP_USER_AGENT'], 'Kindle') !== false
				|| strpos($_SERVER['HTTP_USER_AGENT'], 'BlackBerry') !== false
				|| strpos($_SERVER['HTTP_USER_AGENT'], 'Opera Mini') !== false ) {
				$is_mobile = true;
			} elseif (strpos($_SERVER['HTTP_USER_AGENT'], 'Mobile') !== false && strpos($_SERVER['HTTP_USER_AGENT'], 'iPad') == false) {
				$is_mobile = true;
			} elseif (strpos($_SERVER['HTTP_USER_AGENT'], 'iPad') !== false) {
				$is_mobile = false;
			} else {
				$is_mobile = false;
			}

			return $is_mobile;
		}
		//************ Dealers address from acf field  **************//
		add_action('admin_footer', function () {
			?>
			<script type="text/javascript">
				var addressField   = jQuery('#acf-field_582dc688e98ce').val();
				var stateField     = jQuery('#acf-field_582dc6bbe98d0').val();
				var cityField      = jQuery('#acf-field_582dc6a6e98cf').val();
				var zipField       = jQuery('#acf-field_582dc6cae98d1').val();
				var countryField   = jQuery('#acf-field_582dc75fe98d7').val();
				var latitueField   = jQuery('#acf-field_582dc708e98d4').val();
				var longitudeField = jQuery('#acf-field_582dc71ce98d5').val();

				// jQuery('#wppl-addresspicker').val(addressField);
				// jQuery('#_wppl_street').val(addressField);
				// jQuery('#_wppl_city').val(cityField);
				// jQuery('#_wppl_state').val(stateField);
				// jQuery('#_wppl_zipcode').val(zipField);
				// jQuery('#_wppl_country').val(countryField);
				// jQuery('#_wppl_lat').val(latitueField);
				// jQuery('#_wppl_long').val(longitudeField);

				window.onload = function(){
					// jQuery("#gmw-admin-getaddress-btn").trigger('click');
				}
			</script>
			<?php
		});

/** Log in redirect to previous page by portalpacific.net **/
// start global session for saving the referer url
function start_session() {
	if(!session_id()) {
		session_start();
	}
}
add_action('init', 'start_session', 1);

// get the referer url and save it to the session
function redirect_url() {
	if (! is_user_logged_in()) {
		$_SESSION['referer_url'] = wp_get_referer();
	} else {
		session_destroy();
	}
}
add_action( 'template_redirect', 'redirect_url' );

//login redirect to referer url
function zm_login_redirect() {
	if (isset($_SESSION['referer_url'])) {
		wp_redirect($_SESSION['referer_url']);
	} else {
		wp_redirect(home_url());
	}
}
// add_filter('login_redirect', 'zm_login_redirect', 10, 3);
// add_filter('woocommerce_login_redirect', 'zm_login_redirect', 1100, 2);

add_action( 'wp_automatic_post_added', function( $data ) {
	global $webp_img;
	global $webp_remote_img;
	if ( isset( $data['post_id'] ) && ! empty( $webp_img ) ) {
		update_post_meta( $data['post_id'], 'backup_source_link', $data['img']['source_link'] );
		update_post_meta( $data['post_id'], 'backup_image', $webp_img );
		update_post_meta( $data['post_id'], 'backup_remote_image', $webp_remote_img );
	}
});

add_filter('wpseo_opengraph_image', 'luxury_fix_image_open_graph', 999 );

function luxury_fix_image_open_graph( $img )  {
	if ( is_singular('post') ) {
		if ( ! empty( get_post_meta( get_the_ID(), 'lbt_img', true ) ) ) {
			return get_post_meta( get_the_ID(), 'lbt_img', true );
		}
	}
	return $img;
}


/**
 * Create image size on the fly
 *
 * @param   string  $image_url
 * @param   array   $size
 *
 * @return  string
 */
function lbt_get_image_by_size( string $image_url, array $size  ) {
	if ( empty( $image_url ) || count( $size ) !== 2 ) {
		return $image_url;
	}

	$uploads = wp_upload_dir();

	$file_path = str_replace( $uploads['baseurl'], $uploads['basedir'], $image_url );

	$image_name_parts = explode( '/', $image_url );
	$image_name = $image_name_parts[ count( $image_name_parts ) - 1 ];

	$max_width = absint( $size[0] );
	$max_height = absint( $size[1] );

	$new_image_name = str_replace( '.', '-' . $max_width . 'x' . $max_height . '.', $image_name );

	if ( file_exists( $uploads['path'] . '/' . $new_image_name  ) ) {
		return $uploads['url'] . '/' . $new_image_name ;
	}

	$image = wp_get_image_editor( $file_path );

	if ( ! is_wp_error( $image ) ) {
		$crop = false;

		if ( $max_height > 0 ) {
			$crop = true;
		}

		$size = [ 'width' => $max_width, 'height' => $max_height, $crop ];

		$image->resize( $max_width, $max_height, $crop );
		$new_image = $image->save( $uploads['path'] . '/' . $new_image_name );

		return  $uploads['url'] . '/' . $new_image['file'];
	}

	error_log( 'RESIZE IMAGE ON THE FLY ERROR: ' . $image->get_error_message() );
	return $image_url;
}

/*
 * Shuffle Broyhill News posts by categories
 */
add_action('init', function() {
	add_action( 'cron_broyhill_news', 'shuffle_broyhill_news_posts' );

	if (! wp_next_scheduled ( 'cron_broyhill_news' )) {
		wp_schedule_event( time() + 10, 'daily', 'cron_broyhill_news' );
	}
});

if( isset( $_GET['mitko_qw123123'])) {
	shuffle_broyhill_news_posts();
}
function shuffle_broyhill_news_posts() {
	$cats = [ 36, 37, 38, 39 ];

	$end_date   = new DateTime();
	$start_date = ( new DateTime() )->modify( '-2 months' );

	foreach ( $cats as $cat ) {
		$cat_posts = get_posts([
			'category'   => $cat,
			'orderby'    => 'date',
			'order'      => 'asc',
			'posts_per_page' => 4,
		]);

		$times = 1;
		foreach ( $cat_posts as $cat_post ) {
			$post_date = lbt_random_date_in_range( $start_date, $end_date );

			//  echo 'Yesterday ' . ($times * 3 ) . 'pm' . date( 'Y-m-d H:i:s', strtotime( 'Yesterday ' . ($times * 3 ) . ':' . $cat . 'pm' ) ) . '<br>';
			wp_update_post([
				'ID'            => $cat_post->ID, // ID of the post to update
				'post_date'     => $post_date,
				'post_date_gmt' => gmdate( 'Y-m-d H:i:s', strtotime( $post_date ) ),
			]);
			$times++;
		}
	}
}


function lbt_random_date_in_range( DateTime $start, DateTime $end ) {
    $random_timestamp = mt_rand($start->getTimestamp(), $end->getTimestamp());
    $random_date = new DateTime();
    $random_date->setTimestamp( $random_timestamp );
    return $random_date->format( 'Y-m-d H:i:s' );
}

/**
 * Import feed for Broyhill news
 */
add_action('init', function() {
	add_action( 'cron_broyhill_auto_news_feed_import', 'broyhill_import_auto_news_feed' );
	add_action( 'cron_broyhill_property_news_feed_import', 'broyhill_import_property_news_feed' );
	add_action( 'cron_broyhill_fashion_news_feed_import', 'broyhill_import_fashion_news_feed' );
	add_action( 'cron_broyhill_decor_news_feed_import', 'broyhill_import_decor_news_feed' );

	if (! wp_next_scheduled ( 'cron_broyhill_auto_news_feed_import' )) {
		wp_schedule_event( time() + MINUTE_IN_SECONDS, 'daily', 'cron_broyhill_auto_news_feed_import' );
	}

	if (! wp_next_scheduled ( 'cron_broyhill_property_news_feed_import' )) {
		wp_schedule_event( time() + ( MINUTE_IN_SECONDS * 5 ), 'daily', 'cron_broyhill_property_news_feed_import' );
	}

	if (! wp_next_scheduled ( 'cron_broyhill_fashion_news_feed_import' )) {
		wp_schedule_event( time() + ( MINUTE_IN_SECONDS * 10 ), 'daily', 'cron_broyhill_fashion_news_feed_import' );
	}

	if (! wp_next_scheduled ( 'cron_broyhill_decor_news_feed_import' )) {
		wp_schedule_event( time() + ( MINUTE_IN_SECONDS * 15 ), 'daily', 'cron_broyhill_decor_news_feed_import' );
	}
});

if( isset( $_GET['mitko_qw123123_import'])) {
	broyhill_import_auto_news_feed();
	broyhill_import_property_news_feed();
	broyhill_import_fashion_news_feed();
	broyhill_import_decor_news_feed();
}

function broyhill_import_auto_news_feed() {
	include_once get_stylesheet_directory() . '/inc/class-tumblr-import-feed.php';
	$feed_import = new Shuffle_Broyhill_Import_Feed();
	$feed_import->import_auto_news();
}

function broyhill_import_property_news_feed() {
	include_once get_stylesheet_directory() . '/inc/class-tumblr-import-feed.php';
	$feed_import = new Shuffle_Broyhill_Import_Feed();
	$feed_import->import_property_news();
}

function broyhill_import_fashion_news_feed() {
	include_once get_stylesheet_directory() . '/inc/class-tumblr-import-feed.php';
	$feed_import = new Shuffle_Broyhill_Import_Feed();
	$feed_import->import_fashion_news();
}

function broyhill_import_decor_news_feed() {
	include_once get_stylesheet_directory() . '/inc/class-tumblr-import-feed.php';
	$feed_import = new Shuffle_Broyhill_Import_Feed();
	$feed_import->import_home_decor_news();
}

add_action( 'oa_social_login_action_before_user_redirect', 'lbt_update_social_data', 10, 3 );
function lbt_update_social_data( $user_data, $identity, $redirect ) {
	$user_id = $user_data->data->ID;
	//Thumbnail
	$user_thumbnail = (!empty ($identity->thumbnailUrl) ? trim ($identity->thumbnailUrl) : '');
	//Picture
	$user_picture = (!empty ($identity->pictureUrl) ? trim ($identity->pictureUrl) : '');
	//Update user thumbnail
	if (!empty ($user_thumbnail))
	{
		update_user_meta ($user_id, 'oa_social_login_user_thumbnail', $user_thumbnail);
	}

	//Update user picture
	if (!empty ($user_picture))
	{
		update_user_meta ($user_id, 'oa_social_login_user_picture', $user_picture);
		update_user_meta ($user_id, 'lbt_user_avatar', $user_picture);
	}
	// die;
	if ( ! empty( $identity->loginLocation->country->short ) ) {
		$country = $identity->loginLocation->country->short;
		update_field( 'country', $country, 'user_' . $user_id );
	}

	if ( ! empty( $identity->birthday ) ) {
		$birthdate = new DateTime( $identity->birthday );
		$today = new Datetime(date('m/d/y'));

		$diff = $today->diff( $birthdate );

		update_field( 'age', $diff->y, 'user_' . $user_id );
		update_field( 'birthdate', $identity->birthday, 'user_' . $user_id );
	}
}

add_filter( 'get_avatar', 'lbt_change_avatar', 10, 5 );
function lbt_change_avatar( $avatar, $id_or_email, $size, $default, $alt ) {
	if ( ! empty( $_GET['user_id'] ) && is_numeric( $_GET['user_id' ] ) ) {
		$id_or_email = $_GET['user_id'];
	} else {
		//If is email, try and find user ID
		if( ! is_numeric( $id_or_email ) && is_email( $id_or_email ) ){
			$user  =  get_user_by( 'email', $id_or_email );
			if( $user ){
				$id_or_email = $user->ID;
			}
		}

		//if not user ID, return
		if( ! is_numeric( $id_or_email ) ){
			return $avatar;
		}
	}

	//Find URL of saved avatar in user meta
	$saved = get_user_meta( $id_or_email, 'oa_social_login_user_picture', true );
	//check if it is a URL
	if( filter_var( $saved, FILTER_VALIDATE_URL ) ) {
		//return saved image
		return sprintf( '<img src="%s" alt="%s" />', esc_url( $saved ), esc_attr( $alt ) );
	}

	//return normal
	return $avatar;
}

function lbt_get_broyhill_comment( $post_id ) {
	$comment_data = get_post_meta( $post_id, 'lbt_broyhill_comment', true );

	if ( $comment_data ) {
		if ( function_exists('web_pigment_get_offer_raiting') ) {
			$raiting = web_pigment_get_offer_raiting( $rating );
		} else {
			$raiting = get_post_meta( $post_id, 'offer_raiting', true );
			$raiting = intval( $raiting ) ? intval( $raiting ) : 3;
		}

		$comment_data['content'] = preg_replace( '/(\[|\]|\];)/', '', $comment_data['content'] );

		switch ( $raiting ) {
			case 1:
				break;
			case 2:
			case 3:
				$comment_data['content'] = str_replace( '{2,3}', $raiting, $comment_data['content'] );
				break;
			case 4:
			case 5:
				$comment_data['content'] = str_replace( '{4,5}', $raiting, $comment_data['content'] );
				break;
		}
		return $comment_data;
	}

	$categories = get_the_category( $post_id );

	if ( empty( $categories ) ) {
		return '';
	}

	$category_slug = $categories[0]->slug;

	$comments_query = new WP_Query(
		array(
			'post_type'      => 'broyhill_comment',
			'posts_per_page' => 20,
			'post_status'    => 'publish',
			'category_name'  => $category_slug,
			'orderby'        => 'rand',
		)
	);

	if ( ! $comments_query->have_posts() ) {
		return '';
	}

	if ( function_exists('web_pigment_get_offer_raiting') ) {
		$raiting = web_pigment_get_offer_raiting( $rating );
	} else {
		$raiting = get_post_meta( $post_id, 'offer_raiting', true );
		$raiting = intval( $raiting ) ? intval( $raiting ) : 3;
	}

	$comment = $comments_query->posts[0];

	$comment_data = array(
		'title' => $comment->post_title,
		'content' => $comment->post_content,
	);

	switch ( $raiting ) {
		case 1:
			break;
		case 2:
		case 3:
			$comment_data['content'] .= '<br><br>' . get_field( 'comment_two_three_stars', $comment->ID );
			update_post_meta( $post_id, 'lbt_broyhill_comment', $comment_data );
			$comment_data['content'] = str_replace( '{2,3}', $raiting, $comment_data['content'] );
			break;
		case 4:
		case 5:
			$comment_data['content'] .= '<br><br>' . get_field( 'comment_four_five_stars', $comment->ID );
			update_post_meta( $post_id, 'lbt_broyhill_comment', $comment_data );
			$comment_data['content'] = str_replace( '{4,5}', $raiting, $comment_data['content'] );
			break;
	}


	return $comment_data;
}


function lbt_the_excerpt_truncate($str, $charlength) {
	if ( strlen( $str ) <= $charlength )  {
		return $str;
	}

	return substr( strip_tags( $str ), 0, $charlength) . '...';
}

add_action( 'wpseo_title', 'lbt_set_seo_title' );

/**
 * Filters YOAST SEO Title based on Operation Term
 *
 * @param string $title Default SEO Title.
 *
 * @return string
 */
function lbt_set_seo_title( $title ) {
	if ( is_category() ) {
		global $wp_query;

		$object = get_queried_object();

		if ( strpos( $object->slug, 'news' ) ) {
			$title = str_replace( 'Archives', 'News', $title );
		} else {
			$title = str_replace( 'Archives', 'Offers', $title );
		}
	}

	return $tfitle;
}

function web_pigment_get_offer_raiting() {
	$raiting = get_post_meta( get_the_ID(), 'offer_raiting', true );
	return intval( $raiting ) ? intval( $raiting ) : 3;
}


// function sms_saved_brand() {
	// get all prospect useres from wp database
	// $args = array(
	// 	'role'    => 'wpr_prospect',
	// 	'orderby' => 'user_nicename',
	// 	'order'   => 'ASC'
	// );
	// $users = get_users( $args );

	// $prospect = '';
	// foreach ( $users as $user ) {
	// 	$prospect .= esc_html( $user->display_name ) . '[' . esc_html( $user->user_email ) . ']';
	// }
	// wp_mail( 'tech@superiortech.ws', 'Testing a cron event', $ontraport);
// }
add_action( 'brand_newsflash', 'sms_saved_brand' );


// add_action('wp_footer', 'ontraport_check_func');

// function ontraport_check_func() {
function sms_saved_brand() {
	$start = 0;
	$range = 50;

	/**
	 * Brand names with corresponding fetchrss rss_url
	 *
	 * @var array
	 */
	$ontraport_brand_rss_uris_mapping = array(
		'Audi'                    => 'http://fetchrss.com/rss/60b02afefee357413c48117260b0c187c9146968fd5d83e2.xml',
		'Bloomingdales'           => 'http://fetchrss.com/rss/60b02afefee357413c481172630748a78774c7254a152882.xml',
		'BMW'                     => 'http://fetchrss.com/rss/60b02afefee357413c48117260b0ce1043aca32b475ec2a2.xml',
		'Brooks Brothers'         => 'http://fetchrss.com/rss/60b02afefee357413c4811726307491332d94e23cd13ea72.xml',
		'Cadillac'                => 'http://fetchrss.com/rss/60b02afefee357413c48117260b0cf64b0316e04537e94e2.xml',
		'Christies International' => 'http://fetchrss.com/rss/60b02afefee357413c4811726307515530a27a2485688042.xml',
		'Ethan Allen'             => 'http://fetchrss.com/rss/60b02afefee357413c481172630773fdcbfdd5302f6c1783.xml',
		'Ferrari'                 => 'http://fetchrss.com/rss/60b02afefee357413c48117260b0d256627af9586b5563c2.xml',
		'Havertys'                => 'http://fetchrss.com/rss/60b02afefee357413c481172630750e32950557317404d42.xml',
		'Jaguar'                  => 'http://fetchrss.com/rss/60b02afefee357413c4811726307365bf807571dc34a1482.xml',
		'Lexus'                   => 'http://fetchrss.com/rss/60b02afefee357413c481172630736ca34872c6f513e2582.xml',
		'Mercedes-Benz'           => 'http://fetchrss.com/rss/60b02afefee357413c481172630773446ec42c27c2128913.xml',
		'Neiman Marcus'           => 'http://fetchrss.com/rss/60b02afefee357413c481172630749b008044d66205eccd2.xml',
		'Nordstrom'               => 'http://fetchrss.com/rss/60b02afefee357413c481172630749fd5836935d39788bb2.xml',
		'Saks 5th Avenue'         => 'http://fetchrss.com/rss/60b02afefee357413c48117263074a41334bfd64b148adb2.xml',
		'Sothebys International'  => 'http://fetchrss.com/rss/60b02afefee357413c481172630751b8fe8e190a4d550fe2.xml',
		'Tiffany'                 => 'http://fetchrss.com/rss/60b02afefee357413c48117263074aabeccf3572e00588d2.xml',
	);

	/**
	 * Brand names with corresponding ontraport subscribe tag id
	 *
	 * @var array
	 */
	$ontraport_brand_subscribe_tags_mapping = array(
		'Audi'                    => 4688,
		'Bloomingdales'           => 4694,
		'BMW'                     => 813,
		'Brooks Brothers'         => 4695,
		'Cadillac'                => 4689,
		'Christies International' => 4701,
		'Ethan Allen'             => 4699,
		'Ferrari'                 => 4690,
		'Havertys'                => 4700,
		'Jaguar'                  => 4691,
		'Lexus'                   => 4692,
		'Mercedes-Benz'           => 4693,
		'Neiman Marcus'           => 4696,
		'Nordstrom'               => 4697,
		'Saks 5th Avenue'         => 4698,
		'Sothebys International'  => 4702,
		'Tiffany'                 => 3782,
	);

	/**
	 * Brand names with corresponding ontraport unsubscribe tag id
	 *
	 * @var array
	 */
	$ontraport_brand_unsubscribe_tags_mapping = array(
		'Audi'                    => 4706,
		'Bloomingdales'           => 4707,
		'BMW'                     => 4703,
		'Brooks Brothers'         => 4704,
		'Cadillac'                => 4705,
		'Christies International' => 4716,
		'Ethan Allen'             => 4708,
		'Ferrari'                 => 4709,
		'Havertys'                => 4710,
		'Jaguar'                  => 4711,
		'Lexus'                   => 4712,
		'Mercedes-Benz'           => 4713,
		'Neiman Marcus'           => 4714,
		'Nordstrom'               => 4715,
		'Saks 5th Avenue'         => 4717,
		'Sothebys International'  => 4718,
		'Tiffany'                 => 4719,
	);
	$subscribe_values   = array_values($ontraport_brand_subscribe_tags_mapping);
	$unsubscribe_values = array_values($ontraport_brand_unsubscribe_tags_mapping);

	$saved_tags = array();

	while ( true ) {

		$condition = array();

		$condition[] = array(
			'field' => array( 'field' => 'f2882' ),
			'op'    => '=',
			'value' => array( 'value' => '183' ),
		);

		$prospect_contacts = request(
			'Contacts?start=' . $start . '&range=' . $range . '&condition=' . rawurlencode( wp_json_encode( $condition ) ),
			array( 'method' => 'GET' )
		);

		if ( empty( $prospect_contacts ) || ! is_array( $prospect_contacts ) || ! isset( $prospect_contacts[0]['id'] ) ) {
			break;
		}

		// Get the brands list from Ontraport for prospects
		foreach($prospect_contacts as $prospect_contact) {
			$contact_cat = $prospect_contact['contact_cat'];

			if( empty($prospect_contact) || ! isset($prospect_contact['email']) ) {
				continue;
			}

			if( empty($contact_cat) || $contact_cat === "*/*" ) {
				continue;
			}

			$tag_ids = explode("*/*", $contact_cat);

			$temp              = array();
			$temp['email']     = $prospect_contact['email'];
			$temp['tag_names'] = [];

			foreach($tag_ids as $tag_id) {
				if( !empty( $tag_id ) && ( in_array((int)$tag_id, $subscribe_values) ||  in_array((int)$tag_id, $unsubscribe_values)) ) {
					$tag_data = request(
						'Tag?id=' . $tag_id,
						array( 'method' => 'GET' )
					);
					if( !is_wp_error( $tag_data ) )
						array_push($temp['tag_names'], $tag_data['tag_name']);
					else
						var_dump($tag_data->get_error_message());
				}
			}

			array_push($saved_tags, $temp);
		}

		$start += $range;
	}

	$subscribe_brands = array();
	// Remove the unsubscribe brands from the list
	foreach($saved_tags as $saved_tag) {
		$temp          = array();
		$temp['email'] = $saved_tag['email'];

		$brand_names = $saved_tag['tag_names'];
		foreach($brand_names as $index => $brand_name ) {
			if(strpos($brand_name, 'Unsubscribe') !== false) {
				$subscribe = str_replace(" Unsubscribe", "", $brand_name);
				if (($key = array_search($subscribe, $brand_names)) !== false) {
					unset($brand_names[$key]);
				}
				unset($brand_names[$index]);
			}
		}
		$temp['brand_names'] = $brand_names;
		array_push($subscribe_brands, $temp);
	}

	// fetch the latest articles from RSS.app based on the brands
	foreach($subscribe_brands as $subscribe_brand) {

		$flag = false;
		$emailTemp = '<html lang="en" xmlns="http://www.w3.org/1999/xhtml" xmlns:o="urn:schemas-microsoft-com:office:office"><head>
						<meta charset="UTF-8">
						<meta name="viewport" content="width=device-width,initial-scale=1">
						<meta name="x-apple-disable-message-reformatting">
						<title></title>
						<!--[if mso]>
						<noscript>
						<xml>
							<o:OfficeDocumentSettings>
							<o:PixelsPerInch>96</o:PixelsPerInch>
							</o:OfficeDocumentSettings>
						</xml>
						</noscript>
						<![endif]-->
						<style>
						table, td, div, h1, p {font-family: Arial, sans-serif;}
						img {width: 400px;height: 300px; object-fit: cover; margin:0 auto; display: block;}
						</style>
					</head>
					<body style="margin:0;padding:0;">
					<table role="presentation" style="width:100%;border-collapse:collapse;border:0;border-spacing:0;background:#ffffff;">
					<tbody><tr>
						<td align="center" style="padding:0;">
						<table role="presentation" style="width:802px;border-collapse:collapse;border:1px solid #cccccc;border-spacing:0;text-align:left;">
							<tbody>
								<tr>
									<td align="center" style="padding:40px 0 30px 0;background: #000000;">
										<img src="https://luxurybuystoday-media.s3.amazonaws.com/media/uploads/2020/04/luxury-buys-today-png.png" alt="" width="300" style="height:auto;display:block;">
									</td>
								</tr>';

		if(count($subscribe_brand['brand_names']) > 0) {
			foreach($subscribe_brand['brand_names'] as $sub_brand) {

				$ori_brand_name = str_replace('Newsletter ', '', $sub_brand);
				$rss            = $ontraport_brand_rss_uris_mapping[$ori_brand_name];
				$xmlfile        = file_get_contents($rss);
				$ob             = simplexml_load_string($xmlfile, 'SimpleXMLElement', LIBXML_NOCDATA);
				$json           = json_encode($ob);
				$configData     = json_decode($json, true);

				$xml = new SimpleXMLElement(file_get_contents($rss));
				$emailTemp .= '<tr>
								<td style="padding: 36px 30px 0px 30px;">
								<table role="presentation" style="width:100%;border-collapse:collapse;border:0;border-spacing:0;">
									<tbody><tr>
									<td style="padding:0 0 15px 0;color:#153643;">
										<h1 style="font-size:24px;margin:0 0 20px 0;font-family:Arial,sans-serif;">Latest from '.$ori_brand_name.':</h1>
									</td>
									</tr>
								</tbody></table>
								</td>
							</tr>';
				if ( array_key_exists('channel', $configData) ) {
					if ( array_key_exists('item', $configData['channel'] ) ) {
						for ( $i=0; $i <2 ; $i++ ) {
							if( array_key_exists($i, $configData['channel']['item']) ) {
								$pub_date           = $configData['channel']['item'][$i]['pubDate'];
								$pub_date_timestamp = strtotime($pub_date);
								$new_pub_date       = date('Y-m-d', $pub_date_timestamp);
								$now_today          = date_create(date("Y-m-d"));
								$new_pub_date       = date_create($new_pub_date);
								$diff               = date_diff($now_today,$new_pub_date);

								if ( $diff->format("%R%a") == '0') {

									$flag = true;
									$title       = $configData['channel']['item'][$i]['title'];
									$link        = $configData['channel']['item'][$i]['link'];
									$description = $configData['channel']['item'][$i]['description'];
									$emailTemp .= '<tr>
													<td style="padding: 0px 30px 0px 30px;">
													<table role="presentation" style="width:100%;border-collapse:collapse;border:0;border-spacing:0;">
														<tbody><tr>
														<td style="padding:0 0 36px 0;color:#153643;">
															<h2 style="font-size:18px;margin:0 0 20px 0;font-family:Arial,sans-serif;">'.$title.'</h2>'.str_replace('<img ', '<img width="100%" ', $description).'
															<p style="margin:0;font-size:16px;line-height:24px;font-family:Arial,sans-serif;"><a href="'.$link.'" style="color: #ad0202;text-decoration:underline;">Read more</a></p>
														</td>
														</tr>
													</tbody></table>
													</td>
												</tr>';

								}
							}
						}
					}
				}
			}
		}

		// send the email
		$emailTemp .= '<tr>
						<td style="padding:30px;background: #000000;">
						<table role="presentation" style="width:100%;border-collapse:collapse;border:0;border-spacing:0;font-size:9px;font-family:Arial,sans-serif;">
							<tbody><tr>
							<td style="padding:0;width: 100%;" align="center">
								<p style="margin:0;font-size:14px;line-height:16px;font-family:Arial,sans-serif;color: #70737c;">
						© 2023 Luxury Buys Today and Xsellcast.  All names, trademarks and images are copyright their respective owners. Luxury Buys Today is the quintessential Online News Magazine featuring the very best in luxury brand News, Events, &amp; Promotions. Our luxury blog features Fashion, Home Décor, Auto, &amp; Residential Properties. Our publisher provides you guidance, advice, and ratings on each product offer in the marketplace right now.		<br>
								</p>
							</td>

							</tr>
						</tbody></table>
						</td>
					</tr>
				</tbody></table></td></tr></tbody></table></body></html>';

		if ( $flag ) {
			// $headers = array('Content-Type: text/html; charset=UTF-8');
			$headers  = 'MIME-Version: 1.0' . "\r\n";
			$headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
			// $result = wp_mail( 'tech@superiortech.ws', 'Luxury Buys Today Newsletter', $emailTemp, $headers);

			// uncomment this code
			// $result = mail( $subscribe_brand['email'], 'Newsflash from Luxury Buys Today', $emailTemp, $headers);
		}
	}

}

function request( $endpoint, $args = array() ) {

	$api_base = 'https://api.ontraport.com/1/';
	$api_key = 'a03Nn6WLAYTugEy';
	$app_id = '2_137325_S0z9TDx5I';

	$default_request_args = array(
		'method'  => 'POST',
		'timeout' => '45',
		'headers' => array(
			'Accept'    => 'application/json',
			'Api-Key'   => $api_key,
			'Api-Appid' => $app_id,
		),
	);

	$request_args = wp_parse_args( $args, $default_request_args );

	$response = wp_remote_post( $api_base . $endpoint, $request_args );

	if ( is_wp_error( $response ) ) {
		return $response;
	}

	if ( $response['response']['code'] > 299 || $response['response']['code'] < 200 ) {
		return new \WP_Error(
			$response['response']['code'],
			$response['response']['message']
		);
	}

	$response = json_decode( wp_remote_retrieve_body( $response ), true );

	return $response['data'];
}

if ( is_admin() && isset( $_GET['update_promotions_posts_titles'] ) ) {
	$query = new \WP_Query(
		array(
			'post_type'      => 'post',
			'posts_per_page' => -1,
			'post_status'    => 'publish',
			'tax_query'      => array( //phpcs:ignore
				array(
					'taxonomy' => 'post_tag',
					'field'    => 'slug',
					'operator' => 'IN',
					'terms'    => array( \LBT\Constants::PROMOTIONS_TAG ),
				),
			),
		)
	);

	if ( $query->have_posts() ) {
		foreach ( $query->posts as $p ) {
			do_action( 'save_post', $p->ID, $p, true );
		}
	}
}

remove_action( 'novus_loop_before', 'novus_masonry_wrap_loop_before', 10 );
remove_action( 'novus_loop_after', 'novus_masonry_wrap_loop_after', 10 );


// show wp_mail() errors
// add_action( 'wp_mail_failed', 'onMailError', 10, 1 );
// function onMailError( $wp_error ) {
// 	var_dump('wp email error showing... ');
// 	echo "<pre>";
// 	print_r($wp_error);
// 	echo "</pre>";
// }
