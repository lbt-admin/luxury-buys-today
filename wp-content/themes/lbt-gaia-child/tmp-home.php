<?php
/**
 * Template Name: Homepage Template
 *
 * @package Novus
 */

get_header(); ?>

<div id="owl-demo" class="owl-carousel owl-theme homepage-slider">
	<div class="item"><img class="lazyOwl" data-src="<?php echo esc_url( LBT_THEME_URL ); ?>assets/images/home-slides/lbt_hp-vid-layers-r1-01.jpg" alt=""></div>
	<div class="item"><img class="lazyOwl" data-src="<?php echo esc_url( LBT_THEME_URL ); ?>assets/images/home-slides/lbt_hp-vid-layers-r1-02.jpg" alt=""></div>
	<div class="item"><img class="lazyOwl" data-src="<?php echo esc_url( LBT_THEME_URL ); ?>assets/images/home-slides/lbt_hp-vid-layers-r1-03.jpg" alt=""></div>
	<div class="item"><img class="lazyOwl" data-src="<?php echo esc_url( LBT_THEME_URL ); ?>assets/images/home-slides/lbt_hp-vid-layers-r1-04.jpg" alt=""></div>
	<div class="item"><img class="lazyOwl" data-src="<?php echo esc_url( LBT_THEME_URL ); ?>assets/images/home-slides/lbt_hp-vid-layers-r1-05.jpg" alt=""></div>
	<div class="item"><img class="lazyOwl" data-src="<?php echo esc_url( LBT_THEME_URL ); ?>assets/images/home-slides/lbt_hp-vid-layers-r1-06.jpg" alt=""></div>
	<div class="item"><img class="lazyOwl" data-src="<?php echo esc_url( LBT_THEME_URL ); ?>assets/images/home-slides/lbt_hp-vid-layers-r1-07.jpg" alt=""></div>
	<div class="item"><img class="lazyOwl" data-src="<?php echo esc_url( LBT_THEME_URL ); ?>assets/images/home-slides/lbt_hp-vid-layers-r1-08.jpg" alt=""></div>
	<div class="item"><img class="lazyOwl" data-src="<?php echo esc_url( LBT_THEME_URL ); ?>assets/images/home-slides/lbt_hp-vid-layers-r1-09.jpg" alt=""></div>
	<div class="item"><img class="lazyOwl" data-src="<?php echo esc_url( LBT_THEME_URL ); ?>assets/images/home-slides/lbt_hp-vid-layers-r1-10.jpg" alt=""></div>
	<div class="item"><img class="lazyOwl" data-src="<?php echo esc_url( LBT_THEME_URL ); ?>assets/images/home-slides/lbt_hp-vid-layers-r1-11.jpg" alt=""></div>
</div>

<div class="container">
	<div id="primary" class="content-area full-width">
		<main id="main" class="site-main home-main-content" role="main">
			<div class="home-top-categoria">
				<div class="type-automotive"><?php echo do_shortcode( '[categoria type="automotive"]' ); ?></div>
				<div class="type-fashion"><?php echo do_shortcode( '[categoria type="fashion" offset="1"]' ); ?></div>
				<div class="type-home-decor"><?php echo do_shortcode( '[categoria type="home-decor"]' ); ?></div>
				<div class="type-properties"><?php echo do_shortcode( '[categoria type="properties"]' ); ?></div>
			</div><!-- home-top-categoria -->

			<!-- Home Left Content [BEGINS] -->
			<div class="home-left-content">
				<div class="todays-news">
					<h2 class="main-title">TODAY'S NEWS</h2>
					<div class="flex-row">
						<?php echo do_shortcode( '[home_posts type="" amount="5" offset="1" column="2"]' ); ?>
					</div>
				</div>
				<?php /*
				<div class="full-width newsletter-box">
					<h2>Subscribe To Our Newsletter</h2>
					<p>Subscribe to our newsletter to receive the latest news and more!</p>
					<div class="newsletter-wrapper">
						<?php echo do_shortcode( '[lbt_newsletter]' ); ?>
					</div>
				</div><!-- /.newsletter-box -->
				*/ ?>
				<div class="all-offers for-spinner">
					<h2 class="main-title">All Offers</h2>
					<div class="inner-container replace-container">
						<?php
						$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
						if ( $paged === 1 ) {
							$paged = isset( $_GET[ 'paged' ] ) ? $_GET[ 'paged' ] : 1;
						}

						if( $paged > 1 ) {
							$posts_query_args = array(
								'post_type'      => 'post',
								'posts_per_page' => 16,
								'paged'          => $paged
							);
						} else {
							$posts_query_args = array(
								'post_type'      => 'post',
								'posts_per_page' => 16,
								'offset'         => 5,
							);
						}

						$posts_query = new WP_Query( $posts_query_args );

						if ( $posts_query->have_posts() ) :
							while ( $posts_query->have_posts() ) :
								$posts_query->the_post();
								$post_thumbnail = \LBT\Post::get_thumbnail( get_the_ID(), 'wpr_listing' );
								?>
								<article class="home-post-big offers">
									<?php if ( $post_thumbnail ) : ?>
										<a href="<?php echo esc_url( get_permalink() ); ?>" class="featured-image">
											<img
													class="image-cover"
													src=""
													data-test="1"
													onerror="this.src='<?php echo esc_url( $post_thumbnail ); ?>'; if ( this.getAttribute('retry') ) { if( this.getAttribute('retry') == 1 ) { this.src='<?php echo esc_url( $post_thumbnail ); ?>'; this.setAttribute( 'retry', 2 ); } else { this.src='<?php echo esc_url( LBT_THEME_URL ); ?>assets/images/post-thumb-cropped.svg'; } } else { this.setAttribute( 'retry', 1 ); }">
										</a>
									<?php endif; ?>
									<div class="post-content">
										<h4 class="title">
											<?php $post_title = ucwords( strtolower( get_the_title() ) ); ?>
											<a href="<?php echo esc_url( get_permalink() ); ?>" class="title">
												<?php echo esc_html( mb_strimwidth( $post_title, 0, 49, '...' ) ); ?>
											</a>
										</h4>
										<div class="post-meta">
											<span>Published by: </span>
											<a href="http://luxurybuystoday.tumblr.com/" target="_blank">Ed Broyhill</a> |
											<a href="javascript:void(0);"><?php echo get_the_date(); ?></a>
										</div>
										<p class="description">
											<?php
											$content   = wpr_display_ed_comment( get_the_ID() );
											$converted = strtr( $content, array_flip( get_html_translation_table( HTML_ENTITIES, ENT_QUOTES ) ) );
											$trimmed   = trim( $converted, chr( 0xC2 ) . chr( 0xA0 ) );

											echo wp_kses_post( wp_trim_words( $trimmed, 30, '' ) );
											?>
											<a href="<?php echo esc_url( get_permalink() ); ?>" class="read-more">Read more...</a>
										</p>
									</div>
								</article>
							<?php
							endwhile;
						endif;
						?>
						<div class="post-navigation homefeed">
							<?php if ( $paged > 1 ) { ?>
								<a class="post-navigation-prev" data-page="<?php echo $paged - 1; ?>" style="margin-right: 15px;" href="<?php echo get_site_url(); ?>?paged=<?php echo $paged - 1; ?>">Previous</a>
							<?php } ?>
							<a class="post-navigation-next" data-page="<?php echo $paged + 1; ?>" href="<?php echo get_site_url(); ?>?paged=<?php echo $paged + 1; ?>">Next</a>
						</div>
					</div>
					<div class="spinner-container">
						<div class="lbt-spinner">
							<div class="lds-spinner"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
						</div>
					</div>
				</div>
			</div>
			<script>
				jQuery(document).on( 'click','.post-navigation.homefeed a', function(e){
					e.preventDefault();
					var url = '<?php echo admin_url( 'admin-ajax.php' ); ?>?action=fetch_home_page_steps&paged=' + jQuery(this).data('page');
					jQuery('.all-offers.for-spinner').addClass('active');
					jQuery.get(url, function(data, status) {
						// This function is called when the response is received
						jQuery('.replace-container').html(data);
						jQuery('.all-offers.for-spinner').removeClass('active');
						console.log("Status: " + status); // Log the status of the response
					}).fail(function(xhr, status, error) {
						// This function is called if the request fails
						console.error("Error: " + error);
						console.log("Status: " + status);
					});
				});
			</script>
			<style>
			.post-navigation.homefeed{
				text-align: center;
			}
			.all-offers.for-spinner{
				position: relative;
			}
			.all-offers.for-spinner .spinner-container{
				display: none;
				background: rgba(255,255,255,.4);
				position: absolute;
				width: 100%;
				height: 100%;
				left: 0;
				top: 0;
			}
			.all-offers.for-spinner .spinner-container .lbt-spinner{
				position: fixed;
				top:50%;
				left: 50%;
				transform: translate(-50%,-50%);
			}
			.all-offers.for-spinner.active .spinner-container{
				display: block;
			}
			.post-navigation-prev,
			.post-navigation-next{
				position: relative;
				background: #000;
				color: #fff!important;
				padding: 10px 35px;
				border-radius: 4px;
				display: inline-block;
				line-height: 1;
				 min-width: 132px;
			}
			.post-navigation-next:after {
				position: absolute;
				font-size: 30px;
				right: 5px;
				margin-top: 0;
				content: "\f105";
				display: inline-block;
				font-family: FontAwesome;
				font-style: normal;
				font-weight: 400;
				line-height: 1;
				color: inherit;
				-webkit-font-smoothing: antialiased;
				-moz-osx-font-smoothing: grayscale;
				top: 3px;
			}
			.post-navigation-prev:after {
				position: absolute;
				font-size: 30px;
				left: 5px;
				margin-top: 0;
				content: "\f104";
				display: inline-block;
				font-family: FontAwesome;
				font-style: normal;
				font-weight: 400;
				line-height: 1;
				color: inherit;
				-webkit-font-smoothing: antialiased;
				-moz-osx-font-smoothing: grayscale;
				top: 3px;
			}

			</style>
			<!-- Home Left Content [ENDS] -->
			<!-- Home Right Content [BEGINS] -->
			<div class="home-right-content">
				<div class="automative-news">
					<h2 class="main-title">AUTOMOTIVE NEWS</h2>
					<div class="articles">
						<?php echo do_shortcode( '[home_posts type="broyhill-auto-news" layout="broyhill" amount="4" offset="1" column="1"]' ); ?>
					</div>
				</div>
				<div class="fashion-news">
					<h2 class="main-title">FASHION NEWS</h2>
					<div class="articles">
						<?php echo do_shortcode( '[home_posts type="broyhill-fashion-news" layout="broyhill" amount="4" offset="1" column="1"]' ); ?>
					</div>
				</div>
				<div class="home-decor-news">
					<h2 class="main-title">HOME DECOR NEWS</h2>
					<div class="articles">
						<?php echo do_shortcode( '[home_posts type="broyhill-home-decor-news" layout="broyhill" amount="4" offset="1" column="1"]' ); ?>
					</div>
				</div>
				<div class="properties-news">
					<h2 class="main-title">PROPERTIES NEWS</h2>
					<div class="articles">
						<?php echo do_shortcode( '[home_posts type="broyhill-properties-news" layout="broyhill" amount="4" offset="1" column="1"]' ); ?>
					</div>
				</div>
			</div>
			<!-- Home Right Content [ENDS] -->
		</main>
	</div>
	<?php get_footer(); ?>
