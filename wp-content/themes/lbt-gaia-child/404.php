<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @package Novus
 */

get_header(); ?>

	<div id="primary" class="full-width">
		<main id="main" class="site-main" role="main">

			<section class="error-404 not-found">
				<header class="page-header">
					<h1 class="page-title">404</h1>
					<h2><?php esc_html_e( 'Page not found', 'wpr' ); ?></h2>
				</header><!-- .page-header -->

				<div class="page-content">
					<p><?php esc_html_e( 'The page you were looking for doesn\'t exist. You may have mistyped the address or the page may have moved.', 'wpr' ); ?></p>
				</div><!-- .page-content -->
			</section><!-- .error-404 -->

		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_footer(); ?>
