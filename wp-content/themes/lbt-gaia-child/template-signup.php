<?php
/**
 * Template name: SignUp
 *
 * @package Novus
 */

if ( is_user_logged_in() ) {
	wp_safe_redirect( '/', 302 );
	exit;
}

get_header(); ?>

<?php
$can_user_see_page = \LBT\User::get_user_location( 'zip' );

?>

<?php if ( ! $can_user_see_page && ! isset( $_GET['skip_location_check'] ) ) : ?>
	<div class="lbt-modal default" style="display: block;">
		<div class="lbt-modal__body">
			<div class="row">
				<div class="column">
					<img src="https://luxurybuystoday-media.s3.amazonaws.com/media/uploads/2020/10/91e21b2177f9253059b0a967966710be2x-png.png" alt="Coming Soon">
				</div>
				<div class="column">
					<h3 class="title">Coming Soon To Your Area</h3>
					<p class="subtitle">A better way to get the latest news and offers from your favorite luxury brands.</p>
					<div class="default-content">
						<p>VIP Special Member Discounts</p>
						<p>Prized Invitations To Local Sales Events</p>
						<p>Daily Brand News, Events & Promotions</p>
						<p>Free Live Concierge Services</p>
					</div>
					<div class="cookie-content" style="display: block;">
						<p>These features are not yet available in your area keep an eye on your email for a launch announcement.</p>
						<div class="newsletter-wrapper">
							<p>Sign up to get the latest updates</p>
							<?php echo do_shortcode( '[lbt_newsletter]' ); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php else : ?>
	<div class="container lbt-tmp-signup">
	<div id="primary" class="content-area full-width">
		<article class="hentry">
			<header class="entry-header">
				<h1 class="entry-title"><?php the_title(); ?></h1>
			</header>
			<main id="main" class="site-main" role="main">
				<?php
					get_template_part(
						'partials/modal',
						'register',
						array(
							'remove_hide'       => true,
							'hide_social_login' => true,
							'show_in_page'      => true,
						)
					);
				?>
			</main>
		</article>
	</div>
</div>
<?php endif; ?>
<?php get_footer(); ?>
