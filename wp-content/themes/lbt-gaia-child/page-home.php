<?php
/**
 * Template name: Page Without Sidebar
 *
 * @package Novus
 */

get_header(); ?>

<div id="primary" class="content-area full-width">
	<main id="main" class="site-main" role="main">

		<article class="page type-page status-publish hentry">
			<?php the_content(); ?>
		</article>

	</main><!-- #main -->
</div><!-- #primary -->

<?php get_footer(); ?>
