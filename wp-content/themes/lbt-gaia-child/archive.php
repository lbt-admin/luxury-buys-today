<?php
/**
 * Archive template
 *
 * @package Novus
 */

get_header();

$current_page     = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1;
$current_category = get_queried_object();

if ( ! $current_category->category_parent && ! empty( $_GET['brand_id'] ) ) {
	$brand_id = absint( $_GET['brand_id'] );

	if ( ! empty( $brand_id ) ) {
		$current_category = get_category( $brand_id );
	}
}

$is_broyhill_category = \LBT\Category::is_broyhill_category( $current_category->term_id );

if ( $is_broyhill_category ) {
	get_template_part( 'partials/archive', 'broyhills', array( 'current_category' => $current_category ) );
} else {
	$main_categories = \LBT\Category::get_categories( 0, true, array_merge( \LBT\Constants::EXCULDE_CATEGORIES, array( 1 ) ) );

	if ( $current_category->category_parent ) {
		$broyhill_corresponding_category = get_field( 'broyhill_corresponding_category', 'term_' . $current_category->category_parent );
	} else {
		$broyhill_corresponding_category = get_field( 'broyhill_corresponding_category', $current_category );
	}

	$left_column_posts_query     = \LBT\Category::get_left_column_posts( $current_category, $current_page );
	// $promotions_column_posts     = \LBT\Category::get_promotions_column_posts( $current_category, $current_page );
	$broyhill_column_posts_query = \LBT\Category::get_broyhill_column_posts( $broyhill_corresponding_category, $current_page )
	?>

	<?php if ( is_category() ) : ?>
	<div class="archive-top <?php echo ! $current_category->category_parent ? 'parent' : ''; ?>">
		<div class="grid-wrapper grid">
			<div class="gt-box offers t3">
				<div class="item">
					<div class="title-wrapper">
						<h2 class="main-title">
							<span><?php echo esc_html( $current_category->name ); ?></span>
							<?php esc_html_e( ' Today\'s Offers', 'lbt' ); ?>
						</h2>
					</div>
					<div class="content-wrapper">
						<?php
						$blog_post_count = 0;
						while ( $left_column_posts_query->have_posts() ) :
							$left_column_posts_query->the_post();

							$blog_post_count++;

							$blog_post_type = 1 === $blog_post_count ? 'blog-big-article' : 'blog-list';

							if ( 2 === $blog_post_count ) {
								echo '<div class="entry-box-shadow">';
							}

							get_template_part(
								'partials/content',
								$blog_post_type,
							);

						endwhile;

						if ( $blog_post_count > 1 ) {
							echo '</div>';
						}

						wp_reset_postdata();
						?>
					</div>
				</div>
			</div>
			<?php if ( $current_category->category_parent ) : ?>
			<div class="gt-box dealer">
				<div class="item">
					<?php $wpr_brand_image = get_term_meta( $current_category->term_id, 'wpr_brand_image_url', true ); ?>
					<div class="sale-agent-container all-info" id="dealer_block"
							data-brand-image="<?php echo esc_url( $wpr_brand_image ); ?>"
							data-cat-name="<?php echo esc_attr( $current_category->name ); ?>" data-cat="<?php echo esc_attr( $current_category->term_id ); ?>"
							data-type="archive">
							<div class="lbt-spinner">
								<div class="closest-store">SEARCHING FOR YOUR CLOSEST STORE</div>
								<div class="lds-spinner"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
							</div>
					</div>
				</div>
			</div>
			<?php endif; ?>
			<div class="gt-box category">
				<div class="item">
					<?php if ( is_user_logged_in() ) : ?>
						<div class="category-desc">
							<h3>Luxury Buys Today - <?php echo esc_html( $current_category->name ); ?> News</h3>
							<?php echo category_description( $current_category->category_parent ); ?>
						</div>
					<?php else : ?>
						<div class="login-box">
							<h2>Looking for more <?php echo esc_html( $current_category->name ); ?> news?</h2>
							<button class="btn btn-black lbt-show-register-form">Create Your account</button>
							<p>Already have an account? <a href="javascript:void(0);" class="login_register
							lbt-show-login-form">Sign in</a></p>
						</div>
					<?php endif; ?>
				</div>
			</div>
			<div class="gt-box promotions">
				<div class="item">
					<?php
					if ( $is_broyhill_category ) :
						get_template_part( 'partials/ed-info' );
					else :
						?>
						<div class="title-wrapper">
							<h2 class="main-title">
								<?php
								if ( \LBT\Constants::HOME_DECOR_CATEGORY === $current_category->category_parent ) :
									?>
									<span><?php echo esc_html( $category->name ); ?></span> Promotions
								<?php else : ?>
									Promotions
									<?php
								endif;
								?>
							</h2>
							<div class="content-wrapper">
								<div class="lbt-spinner">
									<div class="closest-store">FETCHING PROMOTIONS</div>
									<div class="lds-spinner"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
								</div>
							</div>
						</div>
						<?php
						endif;
					?>
				</div>
			</div>
			<div class="gt-box news t2">
				<div class="item">
					<div class="title-wrapper">
						<h2 class="main-title">
							BROYHILL NEWS FEATURE
						</h2>
					</div>
					<div>
					<?php
					$broyhill_news_counter = 0;
					if ( $broyhill_column_posts_query->have_posts() ) :
						while ( $broyhill_column_posts_query->have_posts() ) :
							$broyhill_column_posts_query->the_post();

							$blog_post_type = 'blog-list';

							$broyhill_news_counter++;

							if ( 1 === $broyhill_news_counter ) {
								$blog_post_type = 'today_news';
								echo '<div class="entry-box-shadow black-box">';
							}

							if ( 2 === $broyhill_news_counter ) {
								echo '</div><div class="entry-box-shadow">';
							}

							get_template_part( 'partials/content', $blog_post_type );
						endwhile;

						wp_reset_postdata();

						if ( $broyhill_column_posts_query->found_posts > 5 ) : //phpcs:ignore
							?>
							<a href="<?php echo esc_url( get_category_link( $broyhill_corresponding_category ) ); ?>" class="lbt-category-cols-load-more-btn">
								More...
							</a>
							<?php
						endif;

						if ( $broyhill_news_counter > 1 ) {
							echo '</div>';
						}
					endif;
					?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php endif; // End for is_category checkup. ?>

	<?php if ( ! is_author() ) : ?>
	<h3 class="main-title"><span><?php echo esc_html( $current_category->name ); ?></span> News</h3>
	<?php else : ?>
		<h3 class="main-title"><span><?php echo esc_html( \LBT\User::get_user_name( $current_category->data->ID ) ); ?>'s</span> News</h3>
	<?php endif; ?>
	<div id="primary" class="content-area <?php echo ! $current_category->category_parent ? 'parent' : ''; ?> ">
		<main id="main" class="site-main" role="main">
			<?php
			if ( is_category() && ( ! $current_category->category_parent || ! empty( $_GET['brand_id'] ) ) ) :
				get_template_part( 'partials/brand', 'filter', array( 'main_categories' => $main_categories ) );
			endif;
			?>
			<?php
			if ( is_category() ) {
				$broyhill_posts_query = \LBT\Category::get_broyhill_posts(
					$broyhill_corresponding_category,
					array(
						'offset'         => 5,
						'posts_per_page' => 3,
					)
				);

				$cat_posts_counter     = 0;
				$broyhill_post_counter = 0;
				$have_broyhill_posts   = $broyhill_posts_query->have_posts();
			}

			do_action( 'wpr_display_dealer_offers' );
			do_action( 'novus_loop_before' );
			global $wp_query;
			$total_pages = $wp_query->max_num_pages;
			$qo = get_queried_object();

			if ( is_a( $qo, 'WP_Term' ) ) {
				$queried_tax = $qo->taxonomy;
				$qo_id = $qo->term_id;
			} elseif ( is_a( $qo, 'WP_User' ) ) {
				$queried_tax = 'author';
				$qo_id = $qo->ID;
			}

			echo '<div class="replace-container">';
			echo '<div class="posts-wrapper-area">';
			if ( is_category() ) :

				while ( have_posts() ) :
					the_post();
					$cat_posts_counter++;

					if ( $have_broyhill_posts && 0 === $cat_posts_counter % 4 ) {
						global $post;
						$old_post = $post;
						$post     = $broyhill_posts_query->posts[ $broyhill_post_counter ];
						setup_postdata( $post );
						get_template_part( 'partials/content', 'post-news' );
						$post = $old_post;
						wp_reset_postdata();
						$broyhill_post_counter++;
						$cat_posts_counter = 0;
					}

					get_template_part( 'partials/content', 'blog-big-article' );
				endwhile;
			else :
				if ( have_posts() ) :
					while ( have_posts() ) :
						the_post();
						get_template_part( 'partials/content', 'blog-big-article' );
					endwhile;
				else :
					esc_html_e( 'No posts found', 'lbt' );
				endif;
			endif;
			echo '</div>';
			if ( $total_pages > 1 ) :
				$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
				?>
				<div class="post-navigation archive">
					<?php if ( $paged > 1 ) { ?>
						<a class="post-navigation-prev" data-page="<?php echo $paged - 1; ?>" style="margin-right: 15px;" href="javascript:void(0)">Previous</a>
					<?php } ?>
					<a class="post-navigation-next" data-page="<?php echo $paged + 1; ?>" href="javascript:void(0)">Next</a>
				</div>
				<?php
			endif;
			echo '</div>';
			?>
			<div class="spinner-container">
						<div class="lbt-spinner">
							<div class="lds-spinner"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
						</div>
					</div>
					<script>
						jQuery(document).on( 'click','.post-navigation.archive a', function(e){
							e.preventDefault();
							var url = '<?php echo esc_url( admin_url( 'admin-ajax.php' ) ); ?>?action=fetch_archive_posts&type=<?php echo $queried_tax; ?>&type_id=<?php echo $qo_id; ?>&<?php echo isset( $brand_id ) ? ( (int) $brand_id ) . '&' : '' ?>paged=' + jQuery(this).data('page');
							jQuery('#main').addClass('active');
							jQuery.get(url, function(data, status) {
								// This function is called when the response is received
								jQuery('.replace-container').html(data);
								jQuery('#main').removeClass('active');
								console.log("Status: " + status); // Log the status of the response
							}).fail(function(xhr, status, error) {
								// This function is called if the request fails
								console.error("Error: " + error);
								console.log("Status: " + status);
							});
						});
					</script>
					<style>
					.posts-wrapper-area {
							display: flex;
							flex-wrap: wrap;
						}
			.post-navigation.archive{
				text-align: center;
			}
			#main{
				position: relative;
			}
			#main .spinner-container{
				display: none;
				background: rgba(255,255,255,.4);
				position: absolute;
				width: 100%;
				height: 100%;
				left: 0;
				top: 0;
			}
			#main .spinner-container .lbt-spinner{
				position: fixed;
				top:50%;
				left: 50%;
				transform: translate(-50%,-50%);
			}
			#main.active .spinner-container{
				display: block;
			}
			.post-navigation-prev,
			.post-navigation-next{
				position: relative;
				background: #000;
				color: #fff!important;
				padding: 10px 35px;
				border-radius: 4px;
				display: inline-block;
				line-height: 1;
				 min-width: 132px;
			}
			.post-navigation-next:after {
				position: absolute;
				font-size: 30px;
				right: 5px;
				margin-top: 0;
				content: "\f105";
				display: inline-block;
				font-family: FontAwesome;
				font-style: normal;
				font-weight: 400;
				line-height: 1;
				color: inherit;
				-webkit-font-smoothing: antialiased;
				-moz-osx-font-smoothing: grayscale;
				top: 3px;
			}
			.post-navigation-prev:after {
				position: absolute;
				font-size: 30px;
				left: 5px;
				margin-top: 0;
				content: "\f104";
				display: inline-block;
				font-family: FontAwesome;
				font-style: normal;
				font-weight: 400;
				line-height: 1;
				color: inherit;
				-webkit-font-smoothing: antialiased;
				-moz-osx-font-smoothing: grayscale;
				top: 3px;
			}
			</style>
			<?php
			do_action( 'novus_loop_after' );
			the_posts_navigation(
				array(
					'prev_text' => __( '&lsaquo; Older posts', 'novus' ),
					'next_text' => __( 'Newer posts &rsaquo;', 'novus' ),
				)
			);
			?>
		</main>
	</div>
	<?php
} // end broyhill check condition.
get_footer();
