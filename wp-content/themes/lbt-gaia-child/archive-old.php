<?php
// phpcs:disable
/**
 * The template for displaying archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package Novus
 */

get_header();
$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
global $is_eds;

$main_categories = \LBT\Category::get_main_categories();
wpdd( $main_categories );
$category        = get_category( get_query_var( 'cat' ) );
$cat_id          = $current_category = $category->cat_ID;
$is_parent       = false;
$thirdPosts  = array();
$main_categories = array(
	'8', // automotive
	'13', // fashion
	'14', // home-decor
	'17', // properties
);
if ( in_array( $cat_id, $main_categories ) ) {
	$is_parent       = true;
	$childCategories = get_categories(
		array( 'parent' => $cat_id )
	);
	$brandsArr       = $childCategories;
}
$edgar_cat = array(
	'8'  => '36', // broyhill-auto-news
	'13' => '37', // broyhill-fashion-news
	'14' => '38', // broyhill-home-decor-news
	'17' => '39', // broyhill-properties-news
);

/* New design updates for brand page top part under cunstructions*/
$thiscat = get_category( get_query_var( 'cat' ) );

$current_catid  = $thiscat->cat_ID;
$current_parent = $thiscat->category_parent;
// echo $current_parent;
if ( $current_parent == '8' ) {
	$news_cat_id = 36;
}
if ( $current_parent == '13' ) {
	$news_cat_id = 37;
}
if ( $current_parent == '14' ) {
	$news_cat_id = 38;
}
if ( $current_parent == '17' ) {
	$news_cat_id = 39;
}
if ( $current_parent == '' ) {
	$news_cat_id = $current_catid;
}
if( $current_parent == 75 ) {
	if ( $current_catid == 36) {
		$news_cat_id = '8' ;
	}
	if ( $current_catid == 37 ) {
		$news_cat_id = '13';
	}
	if ( $current_catid == 38 ) {
		$news_cat_id = '14';
	}
	if ( $current_catid == 39 ) {
		$news_cat_id = '17';
	}
}
// echo 'News:'.$news_cat_id;
/* New design updates for brand page top part under cunstructions*/

if ( $category->parent > 0 ) {
	$cat_id = $category->parent;
}
$wpr_edd_id = $get_post_from = '';
if ( $cat_id ) {
	$get_post_from = $edgar_cat[ $cat_id ];
	if ( $get_post_from ) {
		$args = array(
			'cat'            => absint( $news_cat_id ),
			'posts_per_page' => 1,
			'order'          => 'DESC',
			'order_by'       => 'date',
			/*'meta_key' => '_thumbnail_id',*/
		);

		$the_query = new WP_Query( $args );

		if ( $the_query->posts ) {
			$wpr_edd_id = $the_query->posts[0]->ID;
		}
	}
}

?>
<?php if( $current_catid !== 75 ) { ?>
<div class="archive-top <?php if ( $is_parent ) { echo 'parent'; } ?>">
	<div class="grid-wrapper grid">
		<?php if ( $current_catid != '36' && $current_catid != '37' && $current_catid != '38' &&
				  $current_catid != '39' && $current_catid != '75' ) { ?>
			<div class="gt-box offers t3">
			<div class="item">
				<div class="title-wrapper">
					<h2 class="main-title">
						<?php if( $category->term_id === 75 ) { ?>
							<span><?php echo $category->name; ?></span> Today's News
						<?php } else { ?>
							<span><?php echo $category->name; ?></span> Today's Offers
						<?php } ?>
					</h2>
				</div>
				<div class="content-wrapper">
					<?php
					if ( isset( $is_parent ) && isset( $_GET['brand_id'] ) &&
						 $_GET['brand_id'] != '' && $_GET['brand_id'] != 'null' ) {
						$offer_cats = explode( ',', $_GET['brand_id'] );
					} else {
						$offer_cats = array( absint( $current_category ) );
					}

					$offers_args = array(
						'post_type'      => 'post',
						'paged'          => $paged,
						'cat'            => $category->term_id,
						'order'          => 'DESC',
						'order_by'       => 'date',
						'posts_per_page' => 5,
						/*'post__not_in'   => $firstPosts,*/
						/*'meta_key' => '_thumbnail_id',*/
						'tax_query' => array(
							array(
								'taxonomy' => 'post_tag',
								'field'    => 'slug',
								'operator' => 'NOT IN',
								'terms'    => array( 'promotions' ),
							),
						),
						'meta_query'     => array(
							array(
								'key'     => 'wpr_dealer_id',
								'compare' => 'NOT EXISTS',
							),
						),
					);

					$offers_query = new WP_Query( $offers_args );

					$thirdPosts  = array();
					if ( $offers_query->have_posts() ) :

						$offers_count = 0;

						while ( $offers_query->have_posts() ) :
							$offers_query->the_post();

							$offers_count++;
							$type = 'blog-list';

							if ( $offers_count === 1 ) {
								$type = get_post_format();
							}

							if ( $offers_count === 2 ) {
								?>
								<div class="entry-box-shadow">
								<?php
							}

							$thirdPosts[] = get_the_ID(); // add post id to array

							get_template_part( 'partials/content', $type );

						endwhile;
							if ( $offers_count > 1 ) {
							?>
								</div>
							<?php
							}

					endif;

					wp_reset_postdata();

					// $combine_1_2 = array_merge( $firstPosts, $thirdPosts );
					?>
				</div>
			</div>
		</div>
		<?php } ?>

		<?php
		if ( $is_parent || $current_catid == '36' || $current_catid == '37' || $current_catid == '38' ||
				  $current_catid == '39' || $current_catid == '75' ) :
			echo ''; else :
				?>
			<div class="gt-box dealer">
				<div class="item">
					<?php $wpr_brand_image = get_term_meta( $current_catid, 'wpr_brand_image_url', true ); ?>
					<div class="sale-agent-container all-info wpr-load-dealer-data1" id="dealer_block"
						 data-brand-image="<?php echo $wpr_brand_image; ?>"
						 data-cat-name="<?php echo $category->name; ?>" data-cat="<?php echo $category->term_id; ?>"
						 data-type="archive"></div>
				</div>
			</div>
		<?php endif; ?>
		<div class="gt-box category">
			<div class="item">
				<?php if ( get_current_user_id() ) : ?>
					<div class="category-desc">
						<h3>Luxury Buys Today - <?php echo $category->name; ?> News</h3>
						<?php echo category_description( $current_parent ); ?>
					</div>
				<?php endif; ?>
				<?php if ( ! is_user_logged_in() ) : ?>
					<div class="login-box">
						<h2>Looking for more <?php print( $category->name ); ?> news?</h2>
						<button class="btn btn-black zm-signup">Create Your account</button>
						<p>Already have an account? <a href="javascript:void(0);" class="login_register
						show-login-form">Sign in</a></p>
					</div>
				<?php endif; ?>
			</div>
		</div>
        <?php
        $christine_xml_promotions = array();
        $sothersby_xml_promotions = array();

        if ( is_plugin_active('lbt-xml-promotions-parser/lblt-xml-promotions-parser.php') && 'christies-international' === $category->slug ) {
	        $christine_xml_promotions = lbt_xml_promotion()->fetch_promotions( 'christiesFeed', 3, false, true );
        }

        if ( is_plugin_active('lbt-xml-promotions-parser/lblt-xml-promotions-parser.php') && 'sothebys-international' === $category->slug ) {
	        $sothersby_xml_promotions = lbt_xml_promotion()->fetch_promotions( 'SotherbysFeed', 3, false, true );
        }

        // Authomotive parent category.
        $remote_promotions = array();
        if( 8 === $current_parent ) {
	        $dealership_id     = apply_filters( 'webpigment_get_user_category_dealer', $category->term_id );
	        $dealership_tag    = 'dealership-' . $dealership_id;

	        $automotive_dynamic_post_ids = lbt_get_automotive_post_ids();
	        $automotive_target_href      = get_permalink( $automotive_dynamic_post_ids[ $category->slug ] );

	        $remote_promotions = lbt_feed_archive_promotion_posts(
		        $category,
		        $dealership_tag,
		        1,
		        3,
		        $automotive_target_href
	        );
        } else {
        	     if( ( 'sothebys-international' === $category->slug && empty( $sothersby_xml_promotions ) ) ||  ( 'christies-international' === $category->slug && empty( $christine_xml_promotions ) ) || ( 'sothebys-international' !== $category->slug && 'christies-international' !== $category->slug ) ) {
	               $dealership_id     = apply_filters( 'webpigment_get_user_category_dealer', $category->term_id );
	               $dealership_tag    = 'dealership-' . $dealership_id;

	               $automotive_dynamic_post_ids = lbt_get_automotive_post_ids();
	               $automotive_target_href      = get_permalink( $automotive_dynamic_post_ids[ $category->slug ] );
		        $remote_promotions = lbt_feed_archive_promotion_posts(
			        $category,
			        $dealership_tag,
			        1,
			        3,
			        $automotive_target_href
		        );
               }
        }

        $the_top_query_2 = array();
        ?>
		<div class="gt-box promotions">
			<div class="item">
				<?php if( $current_catid == '36' || $current_catid == '37' || $current_catid == '38' || $current_catid == '39' || $current_catid == '75' ) {
					?>
					<style>
						body .archive-top .grid-wrapper .gt-box.category,
						body .archive-top .grid-wrapper .gt-box.promotions,
						body .archive-top .grid-wrapper .gt-box.middle{
							flex: 0 0 100%;
							max-width: 100%;
						}
						body .gt-box.category .category-desc h3{
							text-align: center;
						}
					</style>
					<?php
					get_template_part( 'partials/ed-info' );

				} else { ?>
					<div class="title-wrapper">
						<h2 class="main-title">
							<?php
							$per_page = 4;
							if( $category->term_id === 75 ) { $per_page = 7; ?>
								<span><?php echo $category->name; ?></span> My Daily Favorites
							<?php } else if( $category->parent === 14 ) { ?>
								<span><?php echo $category->name; ?></span> Promotions
							<?php } else { ?>
								Promotions
							<?php } ?>
						</h2>
					</div>
					<div class="content-wrapper">
					<?php
					$skip_promotions = array();
					if ( 'christies-international' === $category->slug ) {
						$first                    = 1;
						if ( ! empty( $christine_xml_promotions ) ) {
							foreach ( $christine_xml_promotions as $promotion ) {
								$skip_promotions[] = $promotion['ID'];
								$promotion['target_href'] = '/properties/christies-international/christies-promotions/';
								if ( $first ) {
									// echo '<pre>' . print_r( $promotion, true ) . '</pre>';
									$promotion['category'] = $category;
									$promotion['first']    = 1;
									include get_stylesheet_directory() . '/partials/content-promotions-large.php';
									$first = 0;
								} else {

									include get_stylesheet_directory() . '/partials/content-promotions.php';
								}
							}
						} else {
							echo 'No active promotions at this time. Please visit back later.';
						}
						// Authomotive parent category.
					} else if ( 'sothebys-international' === $category->slug ) {
						$first                    = 1;
						if ( ! empty( $sothersby_xml_promotions ) ) {
							foreach ( $sothersby_xml_promotions as $promotion ) {
								$skip_promotions[] = $promotion['ID'];
								$promotion['target_href'] = '/properties/sothebys-international/sothebys-promotions/';
								if ( $first ) {
									// echo '<pre>' . print_r( $promotion, true ) . '</pre>';
									$promotion['category'] = $category;
									$promotion['first']    = 1;
									include get_stylesheet_directory() . '/partials/content-promotions-large.php';
									$first = 0;
								} else {

									include get_stylesheet_directory() . '/partials/content-promotions.php';
								}
							}
						} else {
							echo 'No active promotions at this time. Please visit back later.';
						}
					} else if( ! empty( $remote_promotions ) ) {
						$first = 1;
						if ( ! empty( $remote_promotions ) ) {
							foreach ( $remote_promotions as $promotion ) {
								$skip_promotions[] = $promotion['ID'];
								if ( $first ) {
									$promotion['first'] = 1;
									include get_stylesheet_directory() . '/partials/content-promotions-large.php';
									$first = 0;
								} else {
									include get_stylesheet_directory() . '/partials/content-promotions.php';
								}
							}
						} else {
						    echo 'No active promotions at this time. Please visit back later.';
						}

					} else {
						$promotion_tag = 'promotions';
						if ( isset( $is_parent ) && isset( $_GET['brand_id'] ) && $_GET['brand_id'] != '' && $_GET['brand_id'] != 'null' ) {
							$brandsList = explode( ',', $_GET['brand_id'] );
							$top_args_2 = array(
								'post_type'      => 'post',
								'posts_per_page' => $per_page,
								// Filter for promotions.
								'tag'            => $promotion_tag,
								'cat'            => array( absint( $current_category ) ),
								'paged'          => $paged,
								'post__not_in'   => $combine_1_2,
								'meta_query'     => array(
									array(
										'key'     => 'wpr_dealer_id',
										'compare' => 'NOT EXISTS',
									),
								),
							);
						} else {
							$top_args_2 = array(
								'post_type'      => 'post',
								'cat'            => array( absint( $current_category ) ),
								'paged'          => $paged,
								'posts_per_page' => 4,
								// Filter for promotions.
								'tag'            => array( $promotion_tag ),
								'order'          => 'DESC',
								'order_by'       => 'date',
								'post__not_in'   => $combine_1_2,
							);
						}
						$first_promotion_post = 0;
						$the_top_query_2      = new WP_Query( $top_args_2 );
						// $thirdPosts = array();
						if ( $the_top_query_2->have_posts() ) :
							$count_3    = 0;

							while ( $the_top_query_2->have_posts() ) :
								$the_top_query_2->the_post();

								$count_3++;
								if ( $count_3 > 3 ) {
									$first_promotion_post = get_the_ID();
									continue;
								}
								$type = 'blog-list';
								if ( $count_3 === 1 ) {
									$type = 'blog';
								}
								$thirdPosts[] = get_the_ID(); // add post id to array
								get_template_part( 'partials/content', $type );
							endwhile;
						endif;
						wp_reset_postdata();
					// $combine_1_2_3 = array_merge( $combine_1_2, $thirdPosts );
					}

					?>
					<?php if( $category->parent !== 14 && ! isset( $_GET['tag'] ) ) {
					    if ( ! empty( $remote_promotions )
                             || ! empty( $christine_xml_promotions )
                             || ! empty( $sothersby_xml_promotions )
                             || ( is_a( $the_top_query_2, 'WP_Query' ) && $the_top_query_2->have_posts() && $the_top_query_2->post_count > 3 ) ) {
						    ?>
                            <a href="<?php echo rtrim( get_category_link( $category->term_id ), '/' ); ?>/?tag=promotions" style="display: flex;position: relative;color: #fff;font-size: 14px;width: 100%;padding: 5px 10px;margin: 0 0 15px;height: 42px;cursor: pointer;overflow: hidden;white-space: nowrap;justify-content:center;align-items: center;text-transform: uppercase; background-color: #000;">More...</a>
						    <?php
					    }
                    } ?>
				</div>
				<?php } ?>
			</div>
		</div>
		<?php if( $current_catid != '36' && $current_catid != '37' && $current_catid != '38' && $current_catid != '39' && $current_catid != '75' ) { ?>
			<div class="gt-box news t2">
			<div class="item">
				<div class="title-wrapper">
					<?php if( $category->term_id === 75 ) { ?>
						<h2 class="main-title">
							BROYHILL NEWS FEATURE
						</h2>
					<?php } else { ?>
						<?php if ( $is_parent ) : ?>
							<h2 class="main-title">
								BROYHILL NEWS FEATURE
							</h2>
						<?php else : ?>
							<h2 class="main-title">
								BROYHILL NEWS FEATURE
							</h2>
						<?php endif; ?>
					<?php } ?>
				</div>
				<div class="">
					<?php
					if ( isset( $is_parent ) && isset( $_GET['brand_id'] ) &&
						 $_GET['brand_id'] != '' && $_GET['brand_id'] != 'null' ) {
						$offer_cats = explode( ',', $_GET['brand_id'] );
					} else {
						$offer_cats = array( absint( $news_cat_id ) );
					}

					$news_args = array(
						'post_type'      => 'post',
						'paged'          => $paged,
						'cat'            => $offer_cats,
						'order'          => 'DESC',
						'order_by'       => 'date',
						'posts_per_page' => 5,
						/*'meta_key' => '_thumbnail_id',*/
						'meta_query'     => array(
							array(
								'key'     => 'wpr_dealer_id',
								'compare' => 'NOT EXISTS',
							),
						),
					);
					if( $category->term_id === 75 ) {
						$news_args['cat'] = 77;
					}


					$news_query = new WP_Query( $news_args );
					// $thirdPosts = array();
					if ( $news_query->have_posts() ) :
						$count_1 = 0;

						while ( $news_query->have_posts() ) :
							$news_query->the_post();
							$count_1++;
							$type = 'blog-list';
							if ( $count_1 === 2 ) {
								?>
								</div>
								<div class="entry-box-shadow">
								<?php
							}
							if ( $count_1 === 1 ) {
								$type = 'today_news';
								$is_eds = 1;
								?>
									<div class="entry-box-shadow black-box">
								<?php
							}
							$thirdPosts[] = get_the_ID(); // add post id to array
							get_template_part( 'partials/content', $type );
							$is_eds = 0;
						endwhile;
						if ( $count_1 > 1 ) {
							?>
								<a href="<?php echo esc_url( get_category_link( $news_cat_id ) ); ?>" style="display: flex;position: relative;color: #fff;font-size: 14px;width: 100%;padding: 5px 10px;margin: 0 0 15px;height: 42px;cursor: pointer;overflow: hidden;white-space: nowrap;justify-content:center;align-items: center;text-transform: uppercase; background-color: #000;">More...</a>
							</div>
							<?php
						} else {
							?>
								<a href="<?php echo esc_url( get_category_link( $news_cat_id ) ); ?>" style="display: flex;position: relative;color: #fff;font-size: 14px;width: 100%;padding: 5px 10px;margin: 0 0 15px;height: 42px;cursor: pointer;overflow: hidden;white-space: nowrap;justify-content:center;align-items: center;text-transform: uppercase; background-color: #000;">More...</a>
							<?php
						}
					endif;
					wp_reset_postdata();
					?>

				</div>
			</div>
		</div>
		<?php } ?>

	</div>
</div>
<?php } else { ?>
	<div class="archive-top <?php if ( $is_parent ) { echo 'parent'; } ?>">
		<div class="grid-wrapper grid">
			<div class="gt-box offers t4">
				<div class="item">
					<div class="title-wrapper">
						<h2 class="main-title">
							<?php if( $category->term_id === 75 ) { ?>
								<span>Broyhill</span> Auto News
							<?php } else { ?>
								<span><?php echo $category->name; ?></span> Today's Offers
							<?php } ?>
						</h2>
					</div>
					<div class="content-wrapper">
						<?php
						if ( isset( $is_parent ) && isset( $_GET['brand_id'] ) &&
						     $_GET['brand_id'] != '' && $_GET['brand_id'] != 'null' ) {
							$offer_cats = explode( ',', $_GET['brand_id'] );
						} else {
							$offer_cats = array( absint( $current_category ) );
						}

						$offers_args = array(
							'post_type'      => 'post',
							'paged'          => $paged,
							'cat'            => 36,
							'order'          => 'DESC',
							'order_by'       => 'date',
							'posts_per_page' => 5,
							/*'post__not_in'   => $firstPosts,*/
							/*'meta_key' => '_thumbnail_id',*/
							'tax_query' => array(
								array(
									'taxonomy' => 'post_tag',
									'field'    => 'slug',
									'operator' => 'NOT IN',
									'terms'    => array( 'promotions' ),
								),
							),
							'meta_query'     => array(
								array(
									'key'     => 'wpr_dealer_id',
									'compare' => 'NOT EXISTS',
								),
							),
						);

						$offers_query = new WP_Query( $offers_args );

						$thirdPosts  = array();
						if ( $offers_query->have_posts() ) :

							$offers_count = 0;

							while ( $offers_query->have_posts() ) :
								$offers_query->the_post();

								$offers_count++;
								$type = 'blog-list';

								if ( $offers_count === 1 ) {
									$type = get_post_format();
								}
								if ( $offers_count === 2 ) {
									?>
									<div class="entry-box-shadow">
									<?php
								}

								$thirdPosts[] = get_the_ID(); // add post id to array

								get_template_part( 'partials/content', $type );

							endwhile;
							if ( $offers_count > 1 ) {
								?>
								</div>
								<?php
							}
						endif;

						wp_reset_postdata();

						// $combine_1_2 = array_merge( $firstPosts, $thirdPosts );
						?>
					</div>
				</div>
			</div>
			<?php
			if ( $is_parent || $current_catid == '36' || $current_catid == '37' || $current_catid == '38' ||
			     $current_catid == '39' || $current_catid == '75' ) :
				echo ''; else :
				?>
				<div class="gt-box dealer">
					<div class="item">
						<?php $wpr_brand_image = get_term_meta( $current_catid, 'wpr_brand_image_url', true ); ?>
						<div class="sale-agent-container all-info wpr-load-dealer-data1" id="dealer_block"
						     data-brand-image="<?php echo $wpr_brand_image; ?>"
						     data-cat-name="<?php echo $category->name; ?>" data-cat="<?php echo $category->term_id; ?>"
						     data-type="archive"></div>
					</div>
				</div>
			<?php endif; ?>
			<div class="gt-box promotions cat-<?php echo $current_catid; ?>">
				<div class="item">
					<?php if( $current_catid == '36' || $current_catid == '37' || $current_catid == '38' || $current_catid == '39' || $current_catid == '75' ) {
						get_template_part( 'partials/ed-info' );
					} else { ?>
						<div class="title-wrapper">
							<h2 class="main-title">
								<?php if( $category->term_id === 75 || $category->parent === 75 ) { ?>
									<span><?php echo $category->name; ?></span> My Daily Favorites
								<?php } else if( $category->parent === 14 ) { ?>
									<span><?php echo $category->name; ?></span> Promotions
								<?php } else { ?>
									Promotions
								<?php } ?>
							</h2>
						</div>
						<div class="content-wrapper">
						<?php
						$skip_promotions = array();
						if ( is_plugin_active('lbt-xml-promotions-parser/lblt-xml-promotions-parser.php') && 'christies-international' === $category->slug ) {
							$christine_xml_promotions = lbt_xml_promotion()->fetch_promotions( 'christiesFeed', 3, false, true );
							$first                    = 1;
							if ( ! empty( $christine_xml_promotions ) ) {
								foreach ( $christine_xml_promotions as $promotion ) {
									$skip_promotions[] = $promotion['ID'];
									if ( $first ) {
										// echo '<pre>' . print_r( $promotion, true ) . '</pre>';
										$promotion['category'] = $category;
										$promotion['first']    = 1;
										include get_stylesheet_directory() . '/partials/content-promotions-large.php';
										$first = 0;
									} else {

										include get_stylesheet_directory() . '/partials/content-promotions.php';
									}
								}
							}
						} else if ( is_plugin_active('lbt-xml-promotions-parser/lblt-xml-promotions-parser.php') && 'sothebys-international' === $category->slug ) {
							$sothebys_xml_promotions = lbt_xml_promotion()->fetch_promotions( 'SotherbysFeed', 3, false, true );
							$first                    = 1;
							if ( ! empty( $sothebys_xml_promotions ) ) {
								foreach ( $sothebys_xml_promotions as $promotion ) {
									$skip_promotions[] = $promotion['ID'];
									if ( $first ) {
										// echo '<pre>' . print_r( $promotion, true ) . '</pre>';
										$promotion['category'] = $category;
										$promotion['first']    = 1;
										include get_stylesheet_directory() . '/partials/content-promotions-large.php';
										$first = 0;
									} else {

										include get_stylesheet_directory() . '/partials/content-promotions.php';
									}
								}
							}
						} else if( 0 ) {
							$dealership_id = apply_filters('webpigment_get_user_category_dealer', $category->term_id );
							$posts = wp_remote_get( '',  array('body'=> array('cat_id' => $category->term_id, 'tag_id' => $dealership_id )));
						} else {
							$promotion_tag = 'promotions';
							$top_args_2 = array(
								'post_type'      => 'post',
								'posts_per_page' => 4,
								// Filter for promotions.
								'tag'            => $promotion_tag,
								'cat'            => array( absint( 75 ) ),
								'paged'          => $paged,
								'post__not_in'   => $thirdPosts,
								'meta_query'     => array(
									array(
										'key'     => 'wpr_dealer_id',
										'compare' => 'NOT EXISTS',
									),
								),
							);
							$first_promotion_post = 0;
							$the_top_query_2      = new WP_Query( $top_args_2 );
							// $thirdPosts = array();
							if ( $the_top_query_2->have_posts() ) :
								$count_3    = 0;

								while ( $the_top_query_2->have_posts() ) :
									$the_top_query_2->the_post();

									$count_3++;
									if ( $count_3 > 3 ) {
										$first_promotion_post = get_the_ID();
										continue;
									}
									$type = 'blog-list';
									if ( $count_3 === 1 ) {
										$type = 'blog';
									}
									$thirdPosts[] = get_the_ID(); // add post id to array
									get_template_part( 'partials/content', $type );
								endwhile;
							endif;
							wp_reset_postdata();
							// $combine_1_2_3 = array_merge( $combine_1_2, $thirdPosts );
						}

						?>
						<?php if( $category->parent !== 14 && ! isset( $_GET['tag'] ) ) { ?>
							<a href="<?php echo get_category_link( $category->term_id ); ?>/?tag=promotions" style="display: flex;position: relative;color: #fff;font-size: 14px;width: 100%;padding: 5px 10px;margin: 0 0 15px;height: 42px;cursor: pointer;overflow: hidden;white-space: nowrap;justify-content:center;align-items: center;text-transform: uppercase; background-color: #000;">More...</a>
						<?php } ?>
					</div>
					<?php } ?>
				</div>
			</div>
			<div class="gt-box news t1">
				<div class="item">
					<div class="title-wrapper">
						<?php if( $category->term_id === 75 ) { ?>
							<h2 class="main-title">
								BROYHILL NEWS FEATURE
							</h2>
						<?php } else { ?>
							<?php if ( $is_parent ) : ?>
								<h2 class="main-title">
									BROYHILL NEWS FEATURE
								</h2>
							<?php else : ?>
								<h2 class="main-title">
									BROYHILL NEWS FEATURE
								</h2>
							<?php endif; ?>
						<?php } ?>
					</div>
					<div class="">
						<?php
						if ( isset( $is_parent ) && isset( $_GET['brand_id'] ) &&
						     $_GET['brand_id'] != '' && $_GET['brand_id'] != 'null' ) {
							$offer_cats = explode( ',', $_GET['brand_id'] );
						} else {
							$offer_cats = array( absint( $news_cat_id ) );
						}

						$news_args = array(
							'post_type'      => 'post',
							'paged'          => $paged,
							'cat'            => $offer_cats,
							'order'          => 'DESC',
							'order_by'       => 'date',
							'posts_per_page' => 5,
							/*'meta_key' => '_thumbnail_id',*/
							'meta_query'     => array(
								array(
									'key'     => 'wpr_dealer_id',
									'compare' => 'NOT EXISTS',
								),
							),
						);
						if( $category->term_id === 75 ) {
							$news_args['cat'] = 77;
						}


						$news_query = new WP_Query( $news_args );
						// $thirdPosts = array();
						if ( $news_query->have_posts() ) :
							$count_1 = 0;

							while ( $news_query->have_posts() ) :
								$news_query->the_post();
								$count_1++;
								$type = 'blog-list';
								if ( $count_1 === 2 ) {
								?>
									</div>
									<div class="entry-box-shadow">
								<?php
								}
								if ( $count_1 === 1 ) {
									$type = 'today_news';
									$is_eds = 1;
									?>
										<div class="entry-box-shadow black-box">
									<?php
								}
								$thirdPosts[] = get_the_ID(); // add post id to array
								get_template_part( 'partials/content', $type );
								$is_eds = 0;
							endwhile;
							if ( $count_1 > 1 ) {
								?>
									<a href="<?php echo esc_url( get_category_link( $news_cat_id ) ); ?>" style="display: flex;position: relative;color: #fff;font-size: 14px;width: 100%;padding: 5px 10px;margin: 0 0 15px;height: 42px;cursor: pointer;overflow: hidden;white-space: nowrap;justify-content:center;align-items: center;text-transform: uppercase; background-color: #000;">More...</a>
								</div>
								<?php
							} else {
								?>
								<a href="<?php echo esc_url( get_category_link( $news_cat_id ) ); ?>" style="display: flex;position: relative;color: #fff;font-size: 14px;width: 100%;padding: 5px 10px;margin: 0 0 15px;height: 42px;cursor: pointer;overflow: hidden;white-space: nowrap;justify-content:center;align-items: center;text-transform: uppercase; background-color: #000;">More...</a>
								<?php
							}
						endif;
						wp_reset_postdata();
						?>

					</div>
				</div>
			</div>
		</div>
	</div>
<?php } ?>

<!-- New design updates for brand page top part under cunstructions ends here -->
<?php echo $current_catid !== 75 ? '<h3 class="main-title"><span>' . $category->name . '</span>  Offers </h3>' : '<h3 class="main-title"><span>' . $category->name . '</span>  News </h3>'; ?>

<div id="primary" class="content-area <?php if ( $is_parent ) { 	echo 'parent';} ?> ">
	<main id="main" class="site-main" role="main">
		<?php if ( $is_parent ) { ?>
			<div class="filter-container">
				<select id="brand-filter" multiple="multiple" class="regular-text">
					<option value="">All Brands</option>
					<?php
					foreach ( $brandsArr as $brandSingle ) {
						if ( isset( $_GET['brand_id'] ) && $_GET['brand_id'] != '' && $_GET['brand_id'] != 'null' ) {
							$brandsList = explode( ',', $_GET['brand_id'] );
							if ( in_array( $brandSingle->term_id, $brandsList ) ) {
								$selection = 'selected';
							} else {
								$selection = '';
							}
						} else {
							$selection = '';
						}
						echo '<option value="' . $brandSingle->term_id . '" ' . $selection . '>' . $brandSingle->name . '</option>';
					}
					?>
				</select></div>
		<?php } ?>
		<?php
		do_action( 'wpr_display_dealer_offers' );
		do_action( 'novus_loop_before' );
		if ( is_plugin_active('lbt-xml-promotions-parser/lblt-xml-promotions-parser.php') && 'christies-international' === $category->slug && isset( $_GET['tag'] ) ) {
			$christine_xml_promotions = lbt_xml_promotion()->fetch_promotions( 'christiesFeed', 3, false, true );
			if ( ! empty( $christine_xml_promotions ) ) {
				foreach ( $christine_xml_promotions as $promotion ) {
					if ( ! in_array( $skip_promotions, $promotion['ID'], true ) ) {
						$promotion['category'] = $category;
						$promotion['target_href'] = '/properties/christies-international/christies-promotions/';
						include get_stylesheet_directory() . '/partials/content-promotions-large.php';
					}
				}
			}
			// Authomotive parent category.
        } else if ( is_plugin_active('lbt-xml-promotions-parser/lblt-xml-promotions-parser.php') && 'sothebys-international' === $category->slug && isset( $_GET['tag'] ) ) {
			$sothebys_xml_promotions = lbt_xml_promotion()->fetch_promotions( 'SotherbysFeed', 3, false, true );
			if ( ! empty( $sothebys_xml_promotions ) ) {
				foreach ( $sothebys_xml_promotions as $promotion ) {
					if ( ! in_array( $skip_promotions, $promotion['ID'], true ) ) {
						$promotion['category'] = $category;
						$promotion['target_href'] = '/properties/sothebys-international/sothebys-promotions/';
						include get_stylesheet_directory() . '/partials/content-promotions-large.php';
					}
				}
			}
			// Authomotive parent category.
        } else if( 8 === $current_parent && isset( $_GET['tag'] ) ) {
            $remote_promotions = lbt_feed_archive_promotion_posts(
                $category,
	            $automotive_target_href . ',' . sanitize_text_field( $_GET['tag'] ),
                false,
                false,
	            $automotive_target_href
            );

            if ( ! empty( $remote_promotions ) ) {
                foreach ( $remote_promotions as $promotion ) {
	                if ( in_array( $promotion['ID'], $skip_promotions, true ) ) {
	                    continue;
	                }
                    include get_stylesheet_directory() . '/partials/content-promotions-large.php';
                }
            }

		} else {
			$paged_ed = (get_query_var('paged')) ? get_query_var('paged') : 1;
			$paged_ed += 2;
			$paged_ed = $paged_ed % 6;
			$fix_paged_ed += 2;
			$temp_args  = array(
				'posts_per_page' => 2,
				'paged' => $paged_ed,
				'cat' => $edgar_cat[ $cat_id ],
				'order' => 'desc',
				'order_by' => 'date'
			);
			$skip_first = true;
			$black_ids = array();
			$temp_query = new WP_Query( $temp_args );
			if ( $temp_query->have_posts() ) :
				while ( $temp_query->have_posts() ) :
					$temp_query->the_post();
					if ( $skip_first && $fix_paged_ed === 3 ) {
						$skip_first = false;
						continue;
					}
					$black_ids[] = get_the_ID();
				endwhile;
			endif;
			echo '<!-- ' . print_r( $black_ids, true ) . print_r( $temp_args, true ) . '-->';
			wp_reset_postdata();
			$post_count = 0;
			$curr_cat   = ( $is_parent ? 'parent' : 'child' );
			if ( $first_promotion_post && $current_catid !== 75 ) {
				global $post;
				$old_post = $post;
				$post     = get_post( $first_promotion_post );
				setup_postdata( $post );
				set_query_var( 'post_count', $post_count );
				set_query_var( 'curr_cat', $curr_cat );

				get_template_part( 'partials/content', get_post_format() );
				wp_reset_postdata();
				$post       = $old_post;
				$post_count = 1;
			}
			if( $current_catid === 75 ) {
				$post_count = 1;
			}
			// echo '<pre>';
			// print_r( $thirdPosts );
			// exit();
			global $post;
			$counter = 1;
			while ( have_posts() ) :
				the_post();
				if ( get_the_ID()  && ! in_array( get_the_ID(), $thirdPosts ) ) {
					set_query_var( 'post_count', $post_count );
					set_query_var( 'curr_cat', $curr_cat );
					// echo $current_category . ' <pre>' . print_r( $edgar_cat,true ) ."</pre>";
					if ( ! in_array( $current_category, $edgar_cat ) ) {
						get_template_part( 'partials/content', get_post_format() );
						if ( $counter % 6 === 0 && $counter < 14 ) {
							$old = $post;
							$post = get_post( $black_ids[intval( $counter/6 ) - 1 ] );
							setup_postdata( $post );
							get_template_part( 'partials/content', 'post-news' );
							$post = $old;
							wp_reset_postdata();
						}
						$counter++;
					} else {
						get_template_part( 'partials/content', 'post-news' );
					}
				}
				$post_count++;
				// End the loop.
			endwhile;
			do_action( 'novus_loop_after' );
			the_posts_navigation(
				array(
					'prev_text' => __( '&lsaquo; Older posts', 'novus' ),
					'next_text' => __( 'Newer posts &rsaquo;', 'novus' ),
				)
			);
		}

		?>

	</main><!-- #main -->
</div><!-- #primary -->

<div id="remove-cpt-from-list" data-id="post-<?php echo absint( $wpr_edd_id ); ?>"></div>
<?php get_sidebar(); ?>
<?php get_footer(); ?>
<?php if ( $is_parent ) { ?>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
	<script>
		$("#brand-filter").select2({
			placeholder: "Filter by brand"
		});
		jQuery('#brand-filter').change(function () {
			window.location.href = "<?php get_bloginfo( 'url' ); ?>/<?php echo $category->slug; ?>/?brand_id=" + jQuery('#brand-filter').val();
		});
	</script>
<?php } ?>

<script>
	function ArhiveTopGridLayout() {
		var windowWidth = $(window).width(),
			unwrap = $('.gt-box > .gt-box');

		switch (true) {
			case (windowWidth > 1025):
				unwrap.unwrap();
				$('.gt-box.dealer, .gt-box.category, .gt-box.promotions').wrapAll('<div class="gt-box middle"></div>');
				break;
			case (windowWidth > 576):
				unwrap.unwrap();
				$('.gt-box.offers, .gt-box.promotions').wrapAll('<div class="gt-box left"></div>');
				$('.gt-box.dealer, .gt-box.category, .gt-box.news').wrapAll('<div class="gt-box right"></div>');
				break;
			default:
				unwrap.unwrap();
		}
	}

	ArhiveTopGridLayout();

	$(window).on('resize', function () {
		ArhiveTopGridLayout();
	});
</script>
