<?php
/**
 * Template name: LBT My Offers
 *
 * @package Novus
 */

if ( ! is_user_logged_in() ) {
	wp_safe_redirect( home_url(), 302 );
	exit;
}

get_header();

?>

<?php get_template_part('partials/content', 'profile-header'); ?>
<div class="content-wrapper">
	<div class="container">
		<?php get_template_part('partials/content', 'my-offers'); ?>
	</div>
</div>
<?php get_footer(); ?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script>
    var currentURL = location.protocol + '//' + location.host + location.pathname;

    $("#brand-filter").select2({
        placeholder: "Filter by brand"
    });
    jQuery('#brand-filter').change(function () {
        window.location.href = currentURL + "/?brand_id=" + jQuery('#brand-filter').val();
    });
</script>
