<?php
/**
 * Template name: LBT Saved Brands
 *
 * @package Novus
 */
get_header();

?>

<?php if (is_user_logged_in()) : ?>

    <?php get_template_part('partials/content', 'profile-header'); ?>

    <div class="content-wrapper">
        <div class="container">
            <?php get_template_part('partials/content', 'brands'); ?>
        </div>
    </div>

<?php else : ?>


<?php endif; ?>

<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script>
    var currentURL = location.protocol + '//' + location.host + location.pathname;

    $("#brand-filter").select2({
        placeholder: "ALL"
    });
    jQuery('#brand-filter').change(function () {
        window.location.href = currentURL + "/?brand_id=" + jQuery('#brand-filter').val();
    });

    $("#cat-brand-filter").select2({
        placeholder: "ALL"
    });
    jQuery('#cat-brand-filter').change(function () {
        window.location.href = currentURL + "/?brand_cat_id=" + jQuery('#cat-brand-filter').val();
    });

</script>
<?php get_footer(); ?>
