<?php
/**
 * Template name: LBT Profile
 *
 * @package Novus
 */
get_header();
?>

<?php if (is_user_logged_in()) : ?>

    <?php get_template_part('partials/content', 'profile-header'); ?>

    <div class="content-wrapper">
        <div class="container narrow">
            <?php
            $profile_form = new LBTUP_ProfileForm();
            $profile_form->profile_form_html();
            ?>
        </div>
    </div>

    <?php
    $avatar_upload = new LBTUP_AvatarUpload();
    $avatar_upload->avatar_upload_html();
    ?>

<?php else : ?>


<?php endif; ?>

<?php get_footer(); ?>