<?php
/**
 * Admin Dashboard for Post Image Check
 *
 * @package Novus
 */

$next_run = wp_next_scheduled( 'lbt_validate_posts' );
$last_run = get_option( 'lbt_validated_posts_last_run', 0 );

?>

<section class="lbt-post-image-check">
	<?php if ( $next_run ) : ?>
	<p>Next scheduled run is: <strong><?php echo esc_html( gmdate( 'F j, Y h:i A', $next_run ) ); ?></strong></p>
	<?php else : ?>
		<p>The script is not running. <a href="javascript:void(0)" id="lbt-post-image-check__restart" data-nonce="<?php echo esc_attr( wp_create_nonce( 'lbt-ajax-validate-post-image-restart' ) ); ?>">Restart Now</a></p>
	<?php endif; ?>
	<?php if ( $last_run ) : ?>
		<p>Last run was: <strong><?php echo esc_html( gmdate( 'F j, Y h:i A', $last_run ) ); ?></strong></p>
	<?php else : ?>
		<p>Last run was: -/-</p>
	<?php endif; ?>
	<hr>
	<div class="lbt-post-image-check__cta">
		<form method="POST" id="lbt-post-image-check__manual-form">
			<input type="hidden" name="action" value="lbt_post_image_check_manual_run">
			<input type="hidden" name="__nonce" value="<?php echo esc_attr( wp_create_nonce( 'lbt-ajax-validate-post-image-manual' ) ); ?>">
			<button type="submit" class="button button-primary" disabled title="The will not cancel the scheduled run">Run Script Manually</button>
		</form>
		<div class="lbt-post-image-check__manual-progress"></div>
	</div>
</section>
