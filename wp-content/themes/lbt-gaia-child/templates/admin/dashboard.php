<?php
/**
 * Admin Dashboard template
 *
 * @package LBT
 */

?>
<div class="lbt-wrap">
	<h2><?php esc_html_e( 'Offer Rating Weight Percentages' ); ?></h2>

	<form method="post" action="options.php">
		<?php settings_fields( 'lbt-offer-rating-settings-group' ); ?>
		<?php do_settings_sections( 'lbt-offer-rating-settings-group' ); ?>
		<div class="form-inner">
			<div class="form-row">
				<span><?php esc_html_e( 'Percantage for Rating 1' ); ?>:</span>
				<input type="number" min="1" max="100" step="0.01" name="_lbt_offer_rating[1]" value="<?php echo esc_attr( $dashboard_settings[1] ?? 10 ); ?>"/>
			</div>
			<div class="form-row">
				<span><?php esc_html_e( 'Percantage for Rating 2' ); ?>:</span>
				<input type="number" min="1" max="100" step="0.01" name="_lbt_offer_rating[2]" value="<?php echo esc_attr( $dashboard_settings[2] ?? 10 ); ?>"/>
			</div>
			<div class="form-row">
				<span><?php esc_html_e( 'Percantage for Rating 3' ); ?>:</span>
				<input type="number" min="1" max="100" step="0.01" name="_lbt_offer_rating[3]" value="<?php echo esc_attr( $dashboard_settings[3] ?? 40 ); ?>"/>
			</div>
			<div class="form-row">
				<span><?php esc_html_e( 'Percantage for Rating 4' ); ?>:</span>
				<input type="number" min="1" max="100" step="0.01" name="_lbt_offer_rating[4]" value="<?php echo esc_attr( $dashboard_settings[4] ?? 30 ); ?>"/>
			</div>
			<div class="form-row">
				<span><?php esc_html_e( 'Percantage for Rating 5' ); ?>:</span>
				<input type="number" min="1" max="100" step="0.01" name="_lbt_offer_rating[5]" value="<?php echo esc_attr( $dashboard_settings[5] ?? 10 ); ?>"/>
			</div>
		</div>
		<?php submit_button(); ?>
	</form>
</div>
