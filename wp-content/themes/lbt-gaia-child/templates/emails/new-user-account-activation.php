<?php
/**
 * New User Account Activation Email
 *
 * @package LBT
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
?>

Thank you for creating your account with Luxury Buys Today. We look forward to serving your shopping needs.<br><br>
