<?php
/**
 * Template name: LBT Confirm Subscription
 *
 * @package Novus
 */
get_header();
?>

<div class="single-cols">
    <?php
    if ( !in_category( $edgar_cats ) ) {
        if (is_single()) {
            do_action('wpr_single_page_sidebar');
        }
    }
    ?>
    <?php get_sidebar(); ?>

    <div class="content-wrapper">
        <div class="content-container" id="single_post_content_container">
            <div id="primary" class="content-area">
                <main id="main" class="site-main" role="main">

                    <div class="lbt-confirm">
                        <h3 class="title">Subscription Confirmed</h3>
                        <p class="subtitle">Welcome to the Luxury Buys Today Newsletter!</p>
                        <p class="subtitle">Make sure you’re receiving our emails:</p>
                        <div class="default-content">
                            <p>1. Add support@luxurybuystoday.com to your address book.</p>
                            <p>2. Check your SPAM/Junk folder to see if you have missed a message from us recently.</p>
                            <p class="indent">If yes:</p>
                            <p class="indent x2">Mark the message as NOT SPAM/Junk.</p>
                            <p class="indent x2">Move the message to your inbox.</p>
                        </div>
                    </div>

                </main><!-- #main -->
            </div><!-- #primary -->
        </div>
    </div>
    <!-- </div> -->
</div>

<?php get_footer(); ?>
