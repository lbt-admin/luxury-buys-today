<?php
/**
 * Ontraport request wrapper
 */
class WP_LBT_Ontraport_Request {
	private static $instance = null;
	private $_api_base       = 'https://api.ontraport.com/1/';

	public static function get_instance() {
		if ( is_null( self::$instance ) ) {
			self::$instance = new static();
		}

		return self::$instance;
	}

	private function __construct() {

	}

	/**
	 * Ontraport Request wrapper
	 *
	 * @param   string  $endpoint
	 * @param   array  $args      Request args
	 *
	 * @return  array             Parsed response
	 */
	public function request( $endpoint, $args = array() ) {
		if ( ! is_array( $args ) ) {
			error_log( 'Data Hub Ontraport Request Error: Arguments must be an array' );
			throw new \Exception(
				__( 'Data Hub Ontraport Request Error: Arguments must be an array' ),
				400
			);
		}

		$default_request_args = array(
			'method'  => 'POST',
			'timeout' => '45',
			'headers' => array(
				'Accept'    => 'application/json',
				'Api-Key'   => 'a03Nn6WLAYTugEy',
				'Api-Appid' => '2_137325_S0z9TDx5I',
			),
		);

		$request_args = wp_parse_args( $args, $default_request_args );

		$response = wp_remote_post( $this->_api_base . $endpoint, $request_args );
		
		if ( is_wp_error( $response ) ) {
			error_log( 'Data Hub Ontraport Request Error: ' . $response->get_error_message() );
			throw new \Exception(
				$response->get_error_message()
			);
		}

		if ( $response['response']['code'] > 299 || $response['response']['code'] < 200 ) {
			error_log( 'Data Hub Ontraport Request Error: ' . $response['response']['message'] );
				throw new \Exception(
				$response['response']['message'],
				$response['response']['code']
			);
		}

		$response = json_decode( wp_remote_retrieve_body( $response ), true );

		return $response['data'];
	}

	 /**
         * Get dealership
         *
         * @param   array  $args  Params for searching dealership by
         * @param   string  $relation  Relation to connect the condition by
         *
         * @return  array
         */
        public function get_dealership( array $args = [], string $relation = 'AND' ) {
            if ( empty( $args ) ) {
                return new \WP_Error(
                    __( 'Empty args' )
                );
            }

            $conditions = [];

            foreach ( $args as $key => $value ) {
                $conditions[] = [
                    'field' => [ 'field' => $key ],
                    'op' => '=',
                    'value' => [ 'value' => $value ]
                ];

                $conditions[] = $relation;
            }

            unset( $conditions[ count( $conditions ) - 1 ] );

            $query_string = 'range=1&condition=' . urlencode( json_encode( $conditions ) );

            try {
                $dealership = $this->request( 'Contacts?' . $query_string, [
                    'method' => 'GET'
                ]);

                if ( ! empty( $dealership ) ) {
                    return $dealership[0];
                }

                return false;
            } catch ( \Exception $e ) {
                error_log( 'Get Dealership by params error: ' . $e->getMessage() );
                return new \WP_Error(
                    $e->getCode(),
                    $e->getMessage()
                );
            }
        }

	/**
	 * Get dealership by contact email
	 *
	 * @param   string $contact_email Contact emial.
	 *
	 * @return  array
	 */
	public function get_dealership_by_contact_email( $contact_email ) {
		$condition[] = array(
			'field' => array( 'field' => 'email' ),
			'op' => '=',
			'value' => array( 'value' => $contact_email ),
		);

		try {
			$user = $this->request(
				'Contacts?range=1&condition=' . urlencode( json_encode( $condition ) ),
				array(
					'method' => 'GET',
				)
			);

			if ( ! is_array( $user ) || empty( $user[0] ) ) {
				return new \WP_Error(
					404,
					__( 'Contact not found' )
				);
			}

			$user = $users[0];

			$dealership_id = $user['f2963'];

			if ( empty( $dealership_id ) ) {
				return new \WP_Error(
					404,
					__( 'This Contact is not associated with any dealership' )
				);
			}

			$dealership = $this->request(
				'Dealership-ABCs?id=' . $dealership_id,
				array(
					'method' => 'GET',
				)
			);

			return $dealership;
		} catch ( \Exception $e ) {
			return new \WP_Error(
				$e->getCode(),
				$e->getMessage()
			);
		}
	}
}

/**
 * Load Ontraport Request Wrapper
 */
function wp_lbt_ontraport_request( $endpoint, $args = array() ) {
	try {
		return WP_LBT_Ontraport_Request::get_instance()->request( $endpoint, $args );
	} catch ( \Exception $e ) {
		return new WP_Error(
			$e->getCode(),
			$e->getMessage()
		);
	}
}

/**
 * Get dealership from Ontraport
 */
function wp_lbt_ontraport_get_dealership( $args = array(), $relation = 'AND' ) {
	return WP_LBT_Ontraport_Request::get_instance()->get_dealership( $args, $relation );
}

/**
 * Get dealership from Ontraport by contact email
 *
 * @param string $contact_email The contact email.
 */
function wp_lbt_ontraport_get_dealership_by_contact_email( $contact_email ) {
	return WP_LBT_Ontraport_Request::get_instance()->get_dealership_by_contact_email( $contact_email );
}
