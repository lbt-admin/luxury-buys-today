(function($){
	$(function() {
		const $clearPromotionsButton = $('.lbt-xml-parser-clear-xml-promotions');

		const clearPromotions = function() {
			$.ajax({
				url: ajaxurl,
				type: 'POST',
				data: {
					'action': lbtXMLParser.wpClearXmlPromotionsAction,
					'__nonce': lbtXMLParser.wpClearXmlPromotionsNonce
				},
				success: function() {
					alert('Done!');
				},
				error: function(xhr, status, error) {
                    alert(xhr.statusText);
                }
			})
		}

		$clearPromotionsButton.on('click', clearPromotions);
	})
})(jQuery)
