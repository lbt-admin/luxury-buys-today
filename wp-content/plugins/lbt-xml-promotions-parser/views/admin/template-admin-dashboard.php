<div class="wrap">
	<h1><?php _e( 'XML Promotions Parser' ); ?></h1>
    <hr>
	<form method="POST" action="<?php echo esc_url( admin_url( 'admin-post.php' ) ); ?>" enctype="multipart/form-data">
        <div class="row">
            <label for="lbt-xml-promotions-parse__select_file"><?php _e( 'Choose XML File' ); ?></label><br>
            <input type="file" name="xml_promotions_file" id="lbt-xml-promotions-parse__select_file" accept=".xml" requred>
        </div>
        <input type="hidden" name="action" value="lbt_xml_parse_promotions">
        <input type="hidden" name="__nonce" value="<?php echo wp_create_nonce( 'lbt-xml-parse-promotions-nonce' ); ?>">
	    <?php submit_button( 'Parse Promotions' ); ?>
		<button type="button" class="button button-default lbt-xml-parser-clear-xml-promotions"><?php _e( 'Clear XML File Promotions' ); ?></button>
	</form>
</div>
