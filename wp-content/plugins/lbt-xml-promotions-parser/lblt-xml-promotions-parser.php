<?php
/**
 * Plugin Name: LBT XML Promotions Parser
 * Description: Parse promotions from XML file and show on site
 * Author:      Webpigment
 * Author URI:  https://www.webpigment.com
 * Version:     1.0.0
 *
 * @package    LBT_XML_Promotions_Parser
 * @author     Webpigment
 * @since      1.0.0
 * @license    GPL-3.0+
 * @copyright  Copyright (c) 2020, Webpigment
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

final class LBT_XML_Promotions_Parser {
    /**
     * Instance of the plugin
     *
     * @var LBT_XML_Promotions_Parser
     */
	private static $_instance;

    /**
     * Plugin version
     *
     * @var string
     */
	private $_version = '1.0.0';

	public static function instance() {
		if ( ! isset( self::$_instance ) && ! ( self::$_instance instanceof LBT_XML_Promotions_Parser ) ) {
			self::$_instance = new LBT_XML_Promotions_Parser;
            self::$_instance->constants();
			self::$_instance->includes();

            add_action( 'plugins_loaded', [ self::$_instance, 'objects' ] );
        }

		return self::$_instance;
	}

    /**
     * 3rd party includes
     *
     * @return  void
     */
	private function includes() {
		require_once LBT_XML_PARSER_PLUGIN_DIR . 'inc/core/autoloader.php';
	}

    /**
     * Define plugin constants
     *
     * @return  void
     */
	private function constants() {
		// Plugin version
		if ( ! defined( 'LBT_XML_PARSER_VERSION' ) ) {
			define( 'LBT_XML_PARSER_VERSION', $this->_version );
		}

		// Plugin Folder Path
		if ( ! defined( 'LBT_XML_PARSER_PLUGIN_DIR' ) ) {
			define( 'LBT_XML_PARSER_PLUGIN_DIR', trailingslashit( plugin_dir_path( __FILE__ ) ) );
		}

		// Plugin Folder URL
		if ( ! defined( 'LBT_XML_PARSER_PLUGIN_URL' ) ) {
			define( 'LBT_XML_PARSER_PLUGIN_URL', trailingslashit( plugin_dir_url( __FILE__ ) ) );
		}
	}

    /**
     * Initialize classes / objects here
     *
     * @return  void
     */
	public function objects() {
		// Global objects
        \Webpigment\API\AJAX_Handler::get_instance();

		// Init classes if is Admin/Dashboard
		if ( is_admin() ) {
			\Webpigment\Admin\Admin_Dashboard::get_instance();
			\Webpigment\Admin\Parse_XML_Promotions_File::get_instance();
		}
	}
}

/**
 * Initiate XML Promotions Parser plugin
 *
 * @return  \LBT_XML_Promotions_Parser
 */
function lbt_xml_promotions_parser() {
	return LBT_XML_Promotions_Parser::instance();
}
lbt_xml_promotions_parser();

/**
 * Fetch promotions
 *
 * @return  \Promotion
 */
function lbt_xml_promotion()  {
	return \Webpigment\Promotion::get_instance();
}
lbt_xml_promotion();

/**
 * User location class
 *
 * @return  \User_Location
 */
function lbt_xml_user_location() {
	return \Webpigment\Core\User_Location::get_instance();
}
