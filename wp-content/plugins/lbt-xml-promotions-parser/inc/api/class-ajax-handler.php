<?php
namespace Webpigment\API;

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class AJAX_Handler {
    use \Webpigment\Traits\Singleton;

    /**
     * Initialize class
     *
     * @return  void
     */
    public function _initialize() {
        add_action( 'wp_ajax_lbt_clear_file_promotions', [ $this, 'clear_promotions' ] );
    }

    /**
     * Clear promotions
     *
     * @return  \JSON
     */
    public function clear_promotions() {
        if ( ! current_user_can( 'administrator' ) ) {
            wp_send_json_error(
                __( 'You don\'t have permission to do this action' ),
                403
            );
        }

        $args = $_POST;

        if ( ! wp_verify_nonce( $args['__nonce'], 'lbt-xml-promotions-clear-nonce' ) ) {
            wp_send_json_error(
                __( 'Invalid nonce' ),
                403
            );
        }

		delete_option( '_xml_christies_propmotions' );

        wp_send_json_success();
    }
}
