<?php
//phpcs:disable
namespace Webpigment;

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class Promotion {
    use \Webpigment\Traits\Singleton;

    /**
     * If exits, invoked in Singleton traint private constructor
     *
     * @return  void
     */
    private function _initialize() {
		add_filter( 'the_title', function( $title ) {
			return $this->_validate_christies_promotion_title( $title );
		} );

		add_filter( 'lbt_xml_promotions_content', function( $content ) {
			return $this->_validate_christies_promotion_content( $content );
		} );
	}

	/**
	 * Fetch christies property promotions
	 *
	 * @param   string $type
	 * @param   int $number_of_posts
	 * @param   string $id
	 * @param   bool $skip_options
	 *
	 * @return  array
	 */
	public function fetch_promotions( $type, $number_of_posts = 5, $id = false, $skip_options = false ) {
		$key = '_xml_' . $type . $number_of_posts . $id;

		$promotions = wp_cache_get( $key, 'lbt' );

		if ( $promotions ) {
			return $promotions;
		}

		if ( ! $skip_options ) {
			$all_promotions = get_option( '_xml_' . $type . '_propmotions' );
		} else {
			$file = ABSPATH . 'wp-content/feeds/' . $type . '.xml';
			$all_promotions = \Webpigment\Admin\Parse_XML_Promotions_File::get_instance()->_parse_xml( $file, true );
		}

		$promotions = [];

		if ( ! $all_promotions ) {
			return $promotions;
		} else if ( false !== $id ) {
			$promotions = $all_promotions[ $id ] ?? [];
		} else if ( $number_of_posts === -1 ) {
			$promotions = $all_promotions;
		} else {
			$promotions = array_chunk( $all_promotions, $number_of_posts )[0];
		}

		wp_cache_set( $key, $promotions, 'lbt' );

		return $promotions;
	}

	/**
	 * Validate crhristins promotions blank post
	 *
	 * @return  void
	 */
	private function _validate_promotions() {
		if ( ! is_singular('post') ) {
			return false;
		}

		global $post;

		$allowed = array( 'christies-promotions', 'sothebys-promotions' );

		if ( ! in_array( $post->post_name, $allowed ) && $post->post_name !== 'iframe-offer' ) {
			return false;
		}

		$redirect = 0 === strpos( $post->post_name, 'chriesties' ) ? 'christies-international' : 'sothebys-international';
		$feed = 0 === strpos( $post->post_name, 'chriesties' ) ? 'christiesFeed' : 'SotherbysFeed';

		if (
			( in_array( $post->post_name, $allowed ) && empty( $_GET['id'] ) ) &&
			( $post->post_name === 'iframe-offer' && empty( $_GET['promotion'] ) )
		) {
			wp_safe_redirect( '/properties/' . $redirect );
			exit;
		}

		$promotion = $this->fetch_promotions( $feed, 1, $_GET['id'] ?? $_GET['promotion'], true );

		if ( empty( $promotion ) ) {
			wp_safe_redirect( '/properties/' . $redirect );
			exit;
		}

		return $promotion;
	}

	/**
	 * Validate promotion title
	 *
	 * @param   string  $title
	 *
	 * @return  string
	 */
	private function _validate_christies_promotion_title( $title ) {
		$promotion = $this->_validate_promotions();

		if ( false === $promotion ) {
			return $title;
		}

		return $promotion['listing_item_name'];
	}

	/**
	 * Validate promotion content
	 *
	 * @param   string  $content
	 *
	 * @return  void
	 */
	private function _validate_christies_promotion_content( $content ) {
		$promotion = $this->_validate_promotions();

		if ( false === $promotion ) {
			return $content;
		}


		$location = $promotion['listing_city'] . ', ' . $promotion['listing_state'] . ' ' . $listing_city['listing_zip'];
		$price = $promotion['listing_price'];

		$content = '<div class="wpr-custom-thumbnail iframe-media"><img src="' . $promotion['listing_image_url'] . '" /></div>';
		$content .= '<p>Location: ' . $location . '</p>';
		$content .= '<p>Price: $' . number_format( $price, 0, '.', ',' ) . '</p>';
		$content .= '<a href="'.$promotion['branch_location'].'">Source</a>';

		return $content;
	}
}
