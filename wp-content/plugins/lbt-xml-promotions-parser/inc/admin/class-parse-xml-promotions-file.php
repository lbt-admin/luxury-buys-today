<?php
namespace Webpigment\Admin;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class Parse_XML_Promotions_File {
    use \Webpigment\Traits\Singleton;

	/**
	 * Initialize class
	 *
	 * @return  void
	 */
	private function _initialize() {
		add_action( 'admin_post_lbt_xml_parse_promotions', [ $this, 'parse_xml_promotions_file' ] );
    }

	/**
	 * Parse XML Promotions file
	 *
	 * @return  void
	 */
    public function parse_xml_promotions_file() {
        $args = $_POST;

        if ( ! wp_verify_nonce( $args['__nonce'], 'lbt-xml-parse-promotions-nonce' ) ) {
            wp_die( __( 'Invalid nonce' ) );
        }

        if ( ! isset( $_FILES['xml_promotions_file'] ) ) {
            wp_die( __( 'No XML File Chosen' ) );
        }

        if ( $_FILES['xml_promotions_file']['error'] > 0 ) {
            wp_die( 'Error code: ' . $_FILES['xml_promotions_file']['error'] );
        }

        $name = $_FILES['xml_promotions_file']['name'];
        $name_parts = explode( '.', $name );

        $ext = strtolower( end( $name_parts ) );

        if ( 'xml' !== $ext ) {
            wp_die( __( 'Not a XML file' ) );
        }

		$tmp_name = $_FILES['xml_promotions_file']['tmp_name'];

		$this->_parse_xml( $tmp_name );

        wp_safe_redirect( $_SERVER['HTTP_REFERER'] );
        exit;
	}

	/**
	 * Parse the XML as array and save in options table
	 *
	 * @param   string  $file
	 * @param   bool    $skip_options
	 *
	 * @return  void
	 */
	public function _parse_xml( $file, $skip_options = false ) {
		libxml_use_internal_errors( true );
		$file = str_replace( '/wordpress/', '/', $file );
		$xml_document = simplexml_load_file( $file );

		if ( false === $xml_document ) {
			_e( "There were errors parsing the XML file\n" );

			foreach ( libxml_get_errors() as $error ) {
				echo $error->message;
			}

			if ( $skip_options ) {
				return array();
			}

			exit;
		}

		$json_document = json_encode( $xml_document );
		$json_output = json_decode( $json_document, true );

		if ( $skip_options ) {
			$items = $json_output['body']['rss']['channel']['item'];
		} else {
			$items = $json_output['channel']['item'];
		}

		$formated_items = [];

		if ( ! empty( $items ) ) {
			foreach ( $items  as $item ) {
				$item_id = sanitize_title( $item['listing_item_name'] );
				$item['ID'] = $item_id;
				$formated_items[ $item_id ] = $item;
			}

			if ( ! $skip_options ) {
				update_option( '_xml_christies_propmotions', $formated_items );
			}
		}

		return $formated_items;
	}
}
