<?php
namespace Webpigment\Admin;

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class Admin_Dashboard {
    use \Webpigment\Traits\Singleton;

    /**
     * Initialize class
     *
     * @return  void
     */
	private function _initialize() {
		add_action( 'admin_menu', [ $this, 'menu' ] );
		add_action( 'admin_enqueue_scripts', [ $this, 'load_assets'] );
	}

    /**
     * Register menu page
     *
     * @return  void
     */
	public function menu() {
		add_menu_page(
            'XML Promotions Parser',
            'XML Promotions Parser Settings',
            'administrator',
            'lbt-xml-parser-settings',
            [ $this,'settings' ],
            'dashicons-admin-generic'
        );
	}

    /**
     * Show page settings
     *
     * @return  void
     */
	public function settings() {
		include_once LBT_XML_PARSER_PLUGIN_DIR . 'views/admin/template-admin-dashboard.php';
	}

	/**
	 * Load assest for admin dashboard
	 *
	 * @return  void
	 */
	public function load_assets( $hook ) {
		if ( 'toplevel_page_lbt-xml-parser-settings' !== $hook ) {
			return;
		}

		wp_enqueue_script(
			'lbt-xml-parsing-admin-script',
			LBT_XML_PARSER_PLUGIN_URL . 'assets/admin/js/script.js',
			['jquery'],
			LBT_XML_PARSER_VERSION,
			true
		);

		wp_localize_script(
			'lbt-xml-parsing-admin-script',
			'lbtXMLParser',
			[
				'wpClearXmlPromotionsNonce' => wp_create_nonce( 'lbt-xml-promotions-clear-nonce' ),
				'wpClearXmlPromotionsAction' => 'lbt_clear_file_promotions'
			]
		);
	}
}
