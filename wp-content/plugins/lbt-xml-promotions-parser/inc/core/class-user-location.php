<?php
namespace Webpigment\Core;

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class User_Location {
	use \Webpigment\Traits\Singleton;

	/**
	 * Fetch visitor location
	 *
	 * @param mixed<string|array> $region_code	State code ( Texas = TX )
	 *
	 * @return  bool	If its in the region code or not
	 */
	public function get_user_location( $region_code ) {
		if ( ! empty($_SERVER['HTTP_CLIENT_IP'] ) ) {
			$ip = $_SERVER['HTTP_CLIENT_IP'];
		} elseif ( ! empty($_SERVER['HTTP_X_FORWARDED_FOR'] ) ) {
			$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
		} else {
			$ip = $_SERVER['REMOTE_ADDR'];
		}

		$user_id = get_current_user_id();

		if ( $user_id > 0 ) {
			$get_zip = get_user_meta( $user_id, 'zip_code', true );

			if ( ! empty( $get_zip ) ) {
				$geocode = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address=' . esc_attr( $get_zip ) . '&key=AIzaSyDkE0osaaC14WeHZ75j54HnXqDjz9Z5fg');
				$output = json_decode( $geocode );
				if ($output->results) {
					$user_location = $output->results[0]->formatted_address;
				}
			}
		}

		if ( false == $user_location ) {
			$get_ip = explode( ',', $ip );
			if ( ! empty( $get_ip ) ) {
				$ip = $get_ip[0];
			}

			$geocode = @unserialize( file_get_contents( 'http://ip-api.com/php/' . $ip ) );
			if ( 'fail' != $geocode['status'] ) {
				$user_location = $geocode['regionName'] . ', ' . $geocode['region'];
			}
		}

		if ( is_string( $region_code ) && strpos( $user_location, $region_code ) !== false ) {
			return true;
		}

		if ( is_array( $region_code ) ) {
			$allowed = array_filter( $region_code, function( $rc ) use ( $user_location ) {
				return strpos( $user_location, $rc ) !== false;
			});

			if ( ! empty( $allowed ) ) {
				return true;
			}
		}

		return false;
	}
}
