<?php
/**
 * Autoloader
 *
 * @param   string  $class
 *
 * @return  boolean
 */
function lbt_xml_parser_plugin_autoloader( $class ) {
    $dir = '/inc';
    $type = 'class';

	switch ( $class ) {
		case false !== strpos( $class, 'Webpigment\\Admin\\' ):
										$class = strtolower( str_replace( 'Webpigment\\Admin', '', $class ) );
										$dir .= '/admin';
										break;
		case false !== strpos( $class, 'Webpigment\\API\\' ):
										$class = strtolower( str_replace( 'Webpigment\\API', '', $class ) );
										$dir .= '/api';
										break;
		case false !== strpos( $class, 'Webpigment\\Traits\\' ):
                                        $class = strtolower( str_replace( 'Webpigment\\Traits', '', $class ) );
                                        $dir .= '/traits';
                                        $type = 'trait';
										break;
		case false !== strpos( $class, 'Webpigment\\Core\\' ):
                                        $class = strtolower( str_replace( 'Webpigment\\Core', '', $class ) );
                                        $dir .= '/core';
										break;
		case false !== strpos( $class, 'Webpigment\\' ):
                                        $class = strtolower( str_replace( 'Webpigment', '', $class ) );
										break;
		default: return;
	}

	$filename = LBT_XML_PARSER_PLUGIN_DIR . $dir . str_replace( '_', '-', str_replace( '\\', '/' . $type . '-', $class ) ) . '.php';

	if ( file_exists( $filename ) ) {
		require_once $filename;

		if ( class_exists( $class ) ) {
			return true;
		}
	}

	return false;
}

spl_autoload_register( 'lbt_xml_parser_plugin_autoloader' );
