<?php
/**
 * Cronofy Form Ajax Class
 *
 * @package LBTCronofyForm
 */

namespace Webpigment\LBTCronofyForm\AJAX;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Cronofy Form class
 */
class Cronofy_Form {
	use \Webpigment\LBTCronofyForm\Traits\Singleton;

	/**
	 * Initialize class
	 *
	 * @return  void
	 */
	private function initialize() {
		add_action( 'wp_ajax_lbt_cronofy_calendar_request', array( $this, 'calendar_request_email' ) );
		add_action( 'wp_ajax_nopriv_lbt_cronofy_calendar_request', array( $this, 'calendar_request_email' ) );
	}

	/**
	 * AJAX - Send calendar request email
	 *
	 * Action: lbt_cronofy_calendar_request
	 *
	 * @return  void
	 */
	public function calendar_request_email() {
		$nonce_check = check_ajax_referer( 'lbt-ajax-cronofy-caledar-request', '__nonce', false );

		if ( ! $nonce_check ) {
			wp_send_json_error(
				__( 'Invalid nonce', 'lbt-cronofy-form' ),
				403
			);
		} elseif ( $nonce_check > 1 ) {
			wp_send_json_error(
				__( 'The form expired. Please refresh the page and try agian.', 'lbt-cronofy-form' ),
				403
			);
		}

		$args = $_POST;

		if ( empty( $args['email'] ) ) {
			wp_send_json_error(
				__( 'Email is required' ),
				400
			);
		}

		if ( ! is_email( $args['email'] ) ) {
			wp_send_json_error(
				__( 'Invalid Email' ),
				400
			);
		}

		$redirect_uri = rawurlencode( trailingslashit( get_site_url() ) . 'cronofy' );
		$client_id    = get_option( '_lbt_cronofy_form_dashboard', array() )['app_client_id'] ?? array();

		if ( empty( $client_id ) ) {
			wp_send_json_error(
				__( 'Client ID not added. Please setup plugin options in Cronofy Form', 'lbt-cronofy-form' ),
				400
			);
		}

		$state_hash = md5( get_site_url() . $args['email'] );

		$calendar_auth_link = 'https://app.cronofy.com/oauth/v2/authorize?client_id=' . $client_id . '&redirect_uri=' . $redirect_uri . '&response_type=code&scope=create_calendar&state=' . $state_hash;

		$mail_content = sprintf(
			'<p>%s<p><a href="%s" target="_blank">%s</a>',
			esc_html__( 'You have been invited to create a calendar on Cronofy. Click on the link below to authorize your calendar.', 'lbt-cronofy-form' ),
			esc_url( $calendar_auth_link ),
			esc_html__( 'Authorize Calendar' )
		);

		$headers = array( 'Content-Type: text/html; charset=UTF-8' );

		$mail_sent = wp_mail( $args['email'], 'LuxuryBuysToday Calendar Acivation', $mail_content, $headers );

		if ( ! $mail_sent ) {
			wp_send_json_error(
				__( 'Failed to send email' ),
				500
			);
		}

		wp_send_json_success(
			array(
				'message' => __( 'Request sent successfully. Please check you email for further instructions.', 'lbt-cronofy-forn' ),
			)
		);
	}
}
