<?php
/**
 * Cronofy calendar auth
 *
 * @package LBTCronofyForm
 */

namespace Webpigment\LBTCronofyForm;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Cronofy Calendar Auth
 */
class Calendar_Authorization {
	use \Webpigment\LBTCronofyForm\Traits\Singleton;

	/**
	 * If exits, invoked in Singleton trait private constructor
	 *
	 * @return  void
	 */
	private function initialize() {
		add_filter( 'template_include', array( $this, 'load_template' ) );
		add_filter( 'query_vars', array( $this, 'add_query_var' ) );
		add_action( 'init', array( $this, 'add_rewrite_rule' ) );
		add_action( 'wp_head', array( $this, 'add_noindex' ) );
	}

	/**
	 * Add new custom query var
	 *
	 * @param   array $vars  The existing query vars.
	 *
	 * @return  array
	 */
	public function add_query_var( $vars ) {
		$vars[] = 'virtualpage';
		return $vars;
	}

	/**
	 * Add new rewrite rul
	 *
	 * @return  void
	 */
	public function add_rewrite_rule() {
		add_rewrite_rule( '^cronofy/?$', 'index.php?virtualpage=cronofy-auth', 'top' );
	}

	/**
	 * Check if virtual page is requested and change template
	 *
	 * @param   string $template The template to be loaded.
	 *
	 * @return  string
	 */
	public function load_template( $template ) {
		$virtual_page = get_query_var( 'virtualpage' );

		if ( 'cronofy-auth' === $virtual_page ) {
			$template = LBT_CRONOFY_FORM_PLUGIN_DIR . 'views/template-calendar-auth.php';
		}

		return $template;
	}

	/**
	 * Add no index meta for virtual pages
	 *
	 * @return void
	 */
	public function add_noindex() {
		$virtual_page = get_query_var( 'virtualpage' );

		if ( 'cronofy-auth' === $virtual_page ) {
			echo '<meta name="robots" content="noindex, nofollow" />';
		}
	}
}
