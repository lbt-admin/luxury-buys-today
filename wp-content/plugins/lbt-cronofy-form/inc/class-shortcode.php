<?php
/**
 * Shortcode class
 *
 * @package LBTCronofyForm
 */

namespace Webpigment\LBTCronofyForm;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Register shortcode class
 */
class Shortcode {
	use \Webpigment\LBTCronofyForm\Traits\Singleton;

	/**
	 * Initialize the class
	 *
	 * @return  void
	 */
	private function initialize() {
		add_shortcode( 'lbt_cronofy_form', array( $this, 'create_shortcode' ) );

		add_action( 'wp_enqueue_scripts', array( $this, 'load_assets' ), 20 );
	}

	/**
	 * Load assets
	 *
	 * @return  void
	 */
	public function load_assets() {
		global $post;

		if ( ! isset( $post ) ) {
			return;
		}

		if ( has_shortcode( $post->post_content, 'lbt_cronofy_form' ) ) {
			// Change file path to coresponding css file.
			wp_enqueue_style(
				'lbt-cronofy-form',
				LBT_CRONOFY_FORM_PLUGIN_URL . 'assets/public/css/shortcode.css',
				array(),
				LBT_CRONOFY_FORM_VERSION
			);

			wp_enqueue_script(
				'lbt-cronofy-form',
				LBT_CRONOFY_FORM_PLUGIN_URL . 'assets/public/js/shortcode.js',
				array( 'jquery' ),
				LBT_CRONOFY_FORM_VERSION,
				true
			);

			wp_localize_script(
				'lbt-cronofy-form',
				'cronofyForm',
				array(
					'ajaxURL' => esc_url( admin_url( 'admin-ajax.php' ) ),
				)
			);
		}
	}

	/**
	 * Create the shortcode
	 *
	 * @param   array $attr Shortcode attributes.
	 *
	 * @return  string
	 */
	public function create_shortcode( $attr ) {
		if ( ! current_user_can( 'administrator' ) ) {
			return __( 'You don\'t have permission to see this', 'wpb' );
		}

		$attr = shortcode_atts(
			array(
				'title' => esc_attr( 'This is wordpress boilerplate shortcode' ),
			),
			$attr
		);

		ob_start();
		require LBT_CRONOFY_FORM_PLUGIN_DIR . 'views/template-cronofy-form.php';
		return ob_get_clean();
	}
}
