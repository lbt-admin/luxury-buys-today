<?php
/**
 * Ontraport Request
 *
 * @package LBTCronofyForm
 */

namespace Webpigment\LBTCronofyForm;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Ontraport Request Class
 */
class Ontraport {
	use \Webpigment\LBTCronofyForm\Traits\Singleton;

	/**
	 * Ontraport API base
	 *
	 * @var string
	 */
	private $api_base = 'https://api.ontraport.com/1/';

	/**
	 * Ontraport Request wrapper
	 *
	 * @param  string $endpoint The endpoint to send request to.
	 * @param  array  $args     Request args.
	 *
	 * @throws \Exception If arguments not array or ontraport credentials not set.
	 *
	 * @return  array             Parsed response
	 */
	public function request( $endpoint, $args = array() ) {
		if ( ! is_array( $args ) ) {
			throw new \Exception(
				__( 'Data Hub Ontraport Request Error: Arguments must be an array', 'lbt-cronofy-form' ),
				400
			);
		}

		$credentials = get_option( '_lbt_cronofy_form_dashboard', array() );

		if ( empty( $credentials['ontraport_api_key'] || empty( $credentials['ontraport_app_id'] ) ) ) {
			throw new \Exception(
				__( 'Ontraport API Key or Ontraport App ID not added. Please setup plugin options in Cronofy Form', 'lbt-cronofy-form' ),
				400
			);
		}

		$default_request_args = array(
			'method'  => 'POST',
			'timeout' => '45',
			'headers' => array(
				'Accept'    => 'application/json',
				'Api-Key'   => $credentials['ontraport_api_key'],
				'Api-Appid' => $credentials['ontraport_app_id'],
			),
		);

		$request_args = wp_parse_args( $args, $default_request_args );

		$response = wp_remote_post( $this->api_base . $endpoint, $request_args );
		if ( is_wp_error( $response ) ) {
			throw new \Exception(
				$response->get_error_message(),
				$response->get_error_code()
			);
		}

		if ( $response['response']['code'] > 299 || $response['response']['code'] < 200 ) {
			throw new \Exception(
				$response['response']['message'],
				$response['response']['code']
			);
		}

		$response = json_decode( wp_remote_retrieve_body( $response ), true );

		return $response['data'];
	}

	/**
	 * Get dealership
	 *
	 * @param  array  $args     Params for searching dealership by.
	 * @param  string $relation Relation to connect the condition by.
	 *
	 * @return  array|\WP_Error
	 */
	public function get_dealership( array $args = array(), string $relation = 'AND' ) {
		if ( empty( $args ) ) {
			return new \WP_Error(
				__( 'Empty args' )
			);
		}

		$conditions = array();

		foreach ( $args as $key => $value ) {
			$conditions[] = array(
				'field' => array( 'field' => $key ),
				'op'    => '=',
				'value' => array( 'value' => $value ),
			);

			$conditions[] = $relation;
		}

		unset( $conditions[ count( $conditions ) - 1 ] );

		$query_string = 'range=1&condition=' . rawurlencode( wp_json_encode( $conditions ) );

		try {
			$dealership = $this->request(
				'Dealership-ABCs?' . $query_string,
				array(
					'method' => 'GET',
				)
			);

			if ( ! empty( $dealership ) ) {
				return $dealership[0];
			}

			return false;
		} catch ( \Exception $e ) {
			return new \WP_Error(
				$e->getCode(),
				$e->getMessage()
			);
		}
	}

	/**
	 * Get dealer contact object
	 *
	 * @param  array  $args     Params for searching contact by.
	 * @param  string $relation Relation to connect the condition by.
	 *
	 * @return array|\WP_Error
	 */
	public function get_dealer_contact_object( array $args = array(), string $relation = 'AND' ) {
		if ( empty( $args ) ) {
			return new \WP_Error(
				400,
				__( 'Empty args' )
			);
		}

		$conditions = array();

		foreach ( $args as $key => $value ) {
			$conditions[] = array(
				'field' => array( 'field' => $key ),
				'op'    => '=',
				'value' => array( 'value' => $value ),
			);

			$conditions[] = $relation;
		}

		unset( $conditions[ count( $conditions ) - 1 ] );

		$query_string = 'range=1&condition=' . rawurlencode( wp_json_encode( $conditions ) );

		try {
			$contacts = $this->request(
				'Contacts?' . $query_string,
				array(
					'method' => 'GET',
				)
			);

			if ( ! empty( $contacts ) ) {
				return $contacts[0];
			}

			return false;
		} catch ( \Exception $e ) {
			return new \WP_Error(
				$e->getCode(),
				$e->getMessage()
			);
		}
	}
}
