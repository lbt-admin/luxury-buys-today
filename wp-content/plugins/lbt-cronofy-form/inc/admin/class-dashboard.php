<?php
/**
 * Admin dashboard
 *
 * @package LBTCronofyForm
 */

namespace Webpigment\LBTCronofyForm\Admin;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Dashboard class
 */
class Dashboard {
	use \Webpigment\LBTCronofyForm\Traits\Singleton;

	/**
	 * Initialize class
	 *
	 * @return  void
	 */
	private function initialize() {
		add_action( 'admin_init', array( $this, 'register_settings' ) );
		add_action( 'admin_menu', array( $this, 'menu' ) );
	}

	/**
	 * Register menu page
	 *
	 * @return  void
	 */
	public function menu() {
		add_menu_page(
			__( 'Cronofy Form', 'lbt-cronofy-form' ),
			__( 'Cronofy Form', 'lbt-cronofy-form' ),
			'administrator',
			'cronofy-form-settings',
			array( $this, 'settings' ),
			'dashicons-admin-generic'
		);
	}

	/**
	 * Show page settings
	 *
	 * @return  void
	 */
	public function settings() {
		$dashboard_settings = get_option( '_lbt_cronofy_form_dashboard' );

		include_once LBT_CRONOFY_FORM_PLUGIN_DIR . 'views/admin/template-dashboard.php';
	}

	/**
	 * Register plugin settings
	 *
	 * @return  void
	 */
	public function register_settings() {
		register_setting(
			'lbt-cronofy-form-settings-group',
			'_lbt_cronofy_form_dashboard',
			array(
				'sanitize_callback' => array( $this, 'sanitize_options' ),
			)
		);
	}

	/**
	 * Sanitize the fields before saving
	 *
	 * @param mixed<string|array> $option The option value to be sanitized.
	 *
	 * @return  array
	 */
	public function sanitize_options( $option ) {
		if ( is_array( $option ) ) {
			foreach ( $option as $field => $value ) {
				if ( is_numeric( $value ) ) {
					$option[ $field ] = absint( $value );
				} else {
					$option[ $field ] = sanitize_text_field( $value );
				}
			}
		} else {
			$option = sanitize_text_field( $option );
		}

		return $option;
	}
}
