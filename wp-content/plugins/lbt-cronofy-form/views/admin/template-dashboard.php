<?php
/**
 * Admin Dashboard template
 *
 * @package LBTCronofyForm
 */

?>
<div class="lbt-cronofy-form-wrap">
	<h2><?php esc_html_e( 'Cronofy Form Settings', 'lbt-cronofy-form' ); ?></h2>

	<form method="post" action="options.php">
		<?php settings_fields( 'lbt-cronofy-form-settings-group' ); ?>
		<?php do_settings_sections( 'lbt-cronofy-form-settings-group' ); ?>
		<table class="form-table">
			<tr valign="top">
				<th scope="row"><?php esc_html_e( 'Cronofy App Client ID', 'lbt-cronofy-form' ); ?></th>
				<td>
					<input type="text" name="_lbt_cronofy_form_dashboard[app_client_id]" value="<?php echo esc_attr( $dashboard_settings['app_client_id'] ?? '' ); ?>" />
				</td>
			</tr>
			<tr valign="top">
				<th scope="row"><?php esc_html_e( 'Cronofy App Client Secret', 'lbt-cronofy-form' ); ?></th>
				<td>
					<input type="password" name="_lbt_cronofy_form_dashboard[app_client_secret]" value="<?php echo esc_attr( $dashboard_settings['app_client_secret'] ?? '' ); ?>" />
				</td>
			</tr>
			<tr valign="top">
				<th scope="row"><?php esc_html_e( 'Ontrapot API Key', 'lbt-cronofy-form' ); ?></th>
				<td>
					<input type="password" name="_lbt_cronofy_form_dashboard[ontraport_api_key]" value="<?php echo esc_attr( $dashboard_settings['ontraport_api_key'] ?? '' ); ?>" />
				</td>
			</tr>
			<tr valign="top">
				<th scope="row"><?php esc_html_e( 'Ontraport API App ID', 'lbt-cronofy-form' ); ?></th>
				<td>
					<input type="password" name="_lbt_cronofy_form_dashboard[ontraport_app_id]" value="<?php echo esc_attr( $dashboard_settings['ontraport_app_id'] ?? '' ); ?>" />
				</td>
			</tr>
		</table>
		<?php submit_button(); ?>
	</form>
</div>
