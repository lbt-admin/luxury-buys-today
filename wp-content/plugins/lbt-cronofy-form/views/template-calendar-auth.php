<?php
/**
 * Cronofy calendar auth template
 *
 * @package LBTCronofyForm
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

$code = sanitize_text_field( wp_unslash( $_GET['code'] ?? '' ) ); //phpcs:ignore
$state = sanitize_text_field( wp_unslash( $_GET['state'] ?? '' ) ); //phpcs:ignore

if ( empty( $code ) ) {
	die( 'No code sent' );
}

if ( empty( $state ) ) {
	die( 'No state sent' );
}

$credentials = get_option( '_lbt_cronofy_form_dashboard', array() );

if ( empty( $credentials['app_client_id'] ) || empty( $credentials['app_client_secret'] ) || empty( $credentials['ontraport_api_key'] || empty( $credentials['ontraport_app_id'] ) ) ) {
	die( esc_html__( 'Client ID, Client Secret, Ontraport API Key or Ontraport App ID not added. Please setup plugin options in Cronofy Form', 'lbt-cronofy-form' ) );
}

$response = wp_remote_post(
	'https://api.cronofy.com/oauth/token',
	array(
		'body' => array(
			'client_id'     => $credentials['app_client_id'],
			'client_secret' => $credentials['app_client_secret'],
			'grant_type'    => 'authorization_code',
			'code'          => $code,
			'redirect_uri'  => trailingslashit( get_site_url() ) . 'cronofy',
		),
	)
);

if ( is_wp_error( $response ) ) {
	die( esc_html( $response->get_error_message() ) );
}

$auth_response_data = json_decode( wp_remote_retrieve_body( $response ), true );
if ( empty( $auth_response_data ) ) {
	die( 'Did you already autorize this calendar then got an error and now you tried again? If so contact support with the error you got the first time' );
}

if ( md5( get_site_url() . $auth_response_data['linking_profile']['profile_name'] ) !== $state ) {
	die( 'You need to connect your calendar from the same email account you reacived the authorization link on' );
}

if ( empty( $auth_response_data['access_token'] ) ) {
	die( 'Access token missing' );
}

$calendar_response = wp_remote_get(
	'https://api.cronofy.com/v1/calendars',
	array(
		'headers' => array(
			'Authorization' => 'Bearer ' . $auth_response_data['access_token'],
		),
	)
);

if ( is_wp_error( $calendar_response ) ) {
	die( esc_html( $calendar_response->get_error_message() ) );
}

$calendar_data = json_decode( wp_remote_retrieve_body( $calendar_response ), true );

if ( empty( $calendar_data['calendars'] ) ) {
	die( 'No calendars found' );
}

$calendar_id   = $calendar_data['calendars'][0]['calendar_id'];
$access_token  = $auth_response_data['access_token'];
$expires_in    = $auth_response_data['expires_in'];
$refresh_token = $auth_response_data['refresh_token'];
$sub           = $auth_response_data['sub'];
$account_id    = $auth_response_data['account_id'];
$profile_id    = $auth_response_data['linking_profile']['profile_id'];
$email         = $auth_response_data['linking_profile']['profile_name'];

$ontraport_request = Webpigment\LBTCronofyForm\Ontraport::get_instance();

try {
	$contact = $ontraport_request->get_dealer_contact_object(
		array(
			'email' => $email,
		)
	);

	if ( ! $contact ) {
		die( 'Contact not found in Ontraport' );
	}

	if ( is_wp_error( $contact ) ) {
		die( 'ONTRAPORT ERROR: ' . esc_html( $contact->get_error_message() ) );
	}

	$put_contact['f2976'] = $sub;
	$put_contact['f3001'] = $credentials['app_client_secret'];
	$put_contact['f3002'] = $calendar_id;
	$put_contact['f3011'] = $credentials['app_client_id'];
	$put_contact['f3012'] = $access_token;
	$put_contact['f3013'] = $refresh_token;
	$put_contact['f3015'] = '08:00am-04:00pm';
	$put_contact['id']    = $contact['id'];
	$contact_request_status = $ontraport_request->request(
		'Contacts',
		array(
			'method' => 'PUT',
			'body'   => $put_contact,
		)
	);

	if ( is_wp_error( $contact_request_status ) ) {
		die( esc_html( $contact_request_status->get_error_message() ) );
	}
	echo '<h2>Start coping this text below</h2><pre>';
	print_r( $put_contact );
	echo '</pre><h2>End coping this text above</h2>';
	exit();

	wp_safe_redirect( get_permalink(546), 302 );
	exit;
} catch ( Exception $e ) {
	die( 'ONTRAPORT Exception: ' . esc_html( $e->getMessage() ) );
}
