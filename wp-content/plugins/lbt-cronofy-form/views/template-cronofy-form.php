<?php
/**
 * Cronofy form template
 *
 * @package LBTCronofyForm
 */

?>
<div class="lbt-cronofy-form-wrap">
	<?php if ( ! empty( $title ) ) : ?>
		<h2><?php echo esc_html( $title ); ?></h2>
	<?php endif; ?>

	<form method="post" class="lbt-cronofy-auth-form">
		<div class="lbt-form-row">
			<input type="email" name="email" class="lbt-form-input" placeholder="<?php esc_attr_e( 'Enter email', 'lbt-cronofy-form' ); ?>">
		</div>
		<div class="lbt-cronofy-form-message"></div>
		<div class="lbt-form-row">
			<input type="hidden" name="action" value="lbt_cronofy_calendar_request">
			<input type="hidden" name="__nonce" value="<?php echo esc_attr( wp_create_nonce( 'lbt-ajax-cronofy-caledar-request' ) ); ?>">
			<button type="submit" data-loading-text="<?php esc_attr_e( 'Sending...', 'lbt-cronofy-form' ); ?>"><?php esc_html_e( 'Send Calendar Request', 'lbt-cronofy-form' ); ?></button>
		</div>
	</form>
</div>
