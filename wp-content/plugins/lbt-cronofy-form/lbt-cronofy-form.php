<?php
/**
 * Plugin Name: LBT Cronofy Form
 * Description: Form for sending Cronofy calendar invites to dealers
 * Author:      Webpigment
 * Author URI:  https://www.webpigment.com
 * Version:     1.0.0
 * Text Domain: lbt-cronofy-form
 * Domain Path: /languages
 *
 * You should have received a copy of the GNU General Public License
 * along with LBT Cronofy Form. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    LBTCronofyForm
 * @author     Webpigment
 * @since      1.0.3
 * @license    GPL-3.0+
 * @copyright  Copyright (c) 2021, Webpigment
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Main Plugin Class
 */
final class LBTCronofyForm {
	/**
	 * Instance of the plugin
	 *
	 * @var LBTCronofyForm
	 */
	private static $instance;

	/**
	 * Plugin version
	 *
	 * @var string
	 */
	private $version = '1.0.0';

	/**
	 * Instanciate the plugin
	 */
	public static function get_instance() {
		if ( ! isset( self::$instance ) && ! ( self::$instance instanceof LBTCronofyForm ) ) {
			self::$instance = new LBTCronofyForm();
			self::$instance->constants();
			self::$instance->includes();

			add_action( 'plugins_loaded', array( self::$instance, 'objects' ) );
			add_action( 'plugins_loaded', array( self::$instance, 'load_textdomain' ) );
		}

		return self::$instance;
	}

	/**
	 * 3rd party includes
	 *
	 * @return  void
	 */
	private function includes() {
		require_once LBT_CRONOFY_FORM_PLUGIN_DIR . 'inc/core/autoloader.php';
	}

	/**
	 * Define plugin constants
	 *
	 * @return  void
	 */
	private function constants() {
		// Plugin version.
		if ( ! defined( 'LBT_CRONOFY_FORM_VERSION' ) ) {
			define( 'LBT_CRONOFY_FORM_VERSION', $this->version );
		}

		// Plugin Folder Path.
		if ( ! defined( 'LBT_CRONOFY_FORM_PLUGIN_DIR' ) ) {
			define( 'LBT_CRONOFY_FORM_PLUGIN_DIR', trailingslashit( plugin_dir_path( __FILE__ ) ) );
		}

		// Plugin Folder URL.
		if ( ! defined( 'LBT_CRONOFY_FORM_PLUGIN_URL' ) ) {
			define( 'LBT_CRONOFY_FORM_PLUGIN_URL', trailingslashit( plugin_dir_url( __FILE__ ) ) );
		}

		// Plugin Root File.
		if ( ! defined( 'LBT_CRONOFY_FORM_PLUGIN_FILE' ) ) {
			define( 'LBT_CRONOFY_FORM_PLUGIN_FILE', __FILE__ );
		}
	}

	/**
	 * Initialize classes / objects here
	 *
	 * @return  void
	 */
	public function objects() {
		// Global objects.
		\Webpigment\LBTCronofyForm\Calendar_Authorization::get_instance();
		\Webpigment\LBTCronofyForm\Shortcode::get_instance();
		\Webpigment\LBTCronofyForm\AJAX\Cronofy_Form::get_instance();

		// Init classes if is Admin/Dashboard.
		if ( is_admin() ) {
			\Webpigment\LBTCronofyForm\Admin\Dashboard::get_instance();
		}
	}

	/**
	 * Register textdomain
	 *
	 * @return  void
	 */
	public function load_textdomain() {
		load_plugin_textdomain( 'lbt-cronofy-form', false, basename( dirname( __FILE__ ) ) . '/languages' );
	}
}

LBTCronofyForm::get_instance();
