(function ($) {
	$(function () {
		const $cronofyForm = $(".lbt-cronofy-auth-form");
		const $submitButton = $cronofyForm.find("button[type=submit]");
		const submitButtonText = $submitButton.text();
		const $message = $cronofyForm.find(".lbt-cronofy-form-message");

		$cronofyForm.on("submit", function (e) {
			e.preventDefault();

			if ($(this).find("input[type=email]").first().val().trim().length === 0) {
				$message.text("Email is required");
				$message.addClass("lbt-cronofy-form-error");
				$message.show();
				return;
			}

			const formData = new FormData($cronofyForm[0]);

			$.ajax({
				url: cronofyForm.ajaxURL,
				type: "POST",
				data: formData,
				contentType: false,
				processData: false,
				beforeSend: function () {
					$message.text("");
					$message
						.removeClass("lbt-cronofy-form-error")
						.removeClass("lbt-cronofy-form-success");
					$message.hide();

					$submitButton.text($submitButton.data("loadingText"));
					$submitButton.prop("disabled", true);
				},
				success: function (response) {
					if (response.success) {
						$message.text(response.data.message);
						$message.addClass("lbt-cronofy-form-success");
						$message.show();

						setTimeout(function () {
							$message.text("");
							$message
								.removeClass("lbt-cronofy-form-error")
								.removeClass("lbt-cronofy-form-success");
							$message.hide();
						}, 1500);
					}
					$submitButton.prop("disabled", false);
				},
				error: function (xhr, status, error) {
					$message.text(xhr.responseJSON.data);
					$message.addClass("lbt-cronofy-form-error");
					$message.show();
					$submitButton.text(submitButtonText);
					$submitButton.prop("disabled", false);
				},
			});
		});
	});
})(jQuery);
