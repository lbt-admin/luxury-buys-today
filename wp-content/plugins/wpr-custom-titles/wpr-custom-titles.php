<?php
/**
 * Plugin Name:  WPRiders Custom Titles
 * Plugin URI:   https://www.wpriders.com/hire-us-ref
 * Description:  Allow admins to save custom Titles for each Industry
 * Author:       Ovidiu George Irodiu from WPRiders
 * Author URI:   https://www.wpriders.com/hire-us-ref
 * License:      GPL2
 * License URI:  https://www.gnu.org/licenses/gpl-2.0.html
 */

if ( ! class_exists( 'WPR_Custom_Titles' ) ) {
	/**
	 * Class WPR_Custom_Titles
	 */
	class WPR_Custom_Titles {
		/**
		 * WPR_Custom_Titles constructor.
		 */
		function __construct() {
			add_action( 'init', array( &$this, 'wpr_create_title_cpt' ) );
			if ( is_admin() ) {
				add_action( 'load-post.php', array( $this, 'init_metabox' ) );
				add_action( 'load-post-new.php', array( $this, 'init_metabox' ) );
			}
//			add_action( 'pmxi_saved_post', array( &$this, 'wpr_title_imported' ), 10, 1 );
		}

		public function init_metabox() {
			add_action( 'add_meta_boxes', array( $this, 'wpr_register_industry_metabox' ) );
			add_action( 'save_post', array( $this, 'wpr_industry_save_metabox' ), 10, 2 );
		}

		/**
		 * Associate Custom title import to Industries
		 *
		 * @param $post_id
		 */
		function wpr_title_imported( $post_id ) {
			if ( get_post_type( $post_id ) == 'wpr_custom_title' ) {
				wp_set_post_categories( $post_id, array( 8, 13, 14, 17 ) );
			}
		}

		/**
		 * Create Title CPT
		 */
		function wpr_create_title_cpt() {
			$labels = array(
				'name'               => _x( 'Custom Titles', 'wpr-lang' ),
				'singular_name'      => _x( 'Custom Titles', 'wpr-lang' ),
				'add_new'            => _x( 'Add New', 'wpr_lang' ),
				'add_new_item'       => __( 'Add New title' ),
				'edit_item'          => __( 'Edit title' ),
				'new_item'           => __( 'New title' ),
				'all_items'          => __( 'All titles' ),
				'view_item'          => __( 'View title' ),
				'search_items'       => __( 'Search title' ),
				'not_found'          => __( 'No title found' ),
				'not_found_in_trash' => __( 'No title found in the Trash' ),
				'parent_item_colon'  => '',
				'menu_name'          => 'Custom Article Title',
			);
			$args   = array(
				'labels'              => $labels,
				'description'         => 'Holds Custom Article Title specific data',
				'public'              => false,
				'show_ui'             => true,
				'show_in_menu'        => true,
				'show_in_nav_menus'   => true,
				'show_in_admin_bar'   => true,
				'supports'            => array( 'title' ),
				'has_archive'         => true,
				'exclude_from_search' => true,
				'hierarchical'        => true,
				'capability_type'     => 'post',
			);
			register_post_type( 'wpr_custom_title', $args );
		}

		/**
		 * Define metabox settings
		 */
		function wpr_register_industry_metabox() {
			add_meta_box( 'display_industry_div', __( 'Industries' ), array( &$this, 'display_industry_metabox' ), 'wpr_custom_title', 'side', 'high', array( 'taxonomy' => 'category' ) );
			add_meta_box( 'display_promotions_div', __( 'Promotion' ), array( &$this, 'display_promotion_metabox' ), 'wpr_custom_title', 'side', 'high' );
		}

		/**
		 * Display metabox
		 *
		 * @param $post
		 */
		function display_industry_metabox( $post ) {
			wp_nonce_field( 'industry_nonce_action', 'industry_nonce' );
			?>
            <div id="custom-category" class="display_industry">

                <div id="custom-category-all">

                    <input type="hidden" name="post_category[]" value="0"/>

                    <ul id="custom-category-checklist" data-wp-lists="list:category" class="categorychecklist form-no-clear">
						<?php
						$categories = get_categories( array(
							'orderby' => 'name',
							'parent'  => 0,
							'exclude' => array( 1, 7, 35, 43 ),
						) );

						$term_list = wp_get_post_terms( $post->ID, 'category', array( "fields" => "ids" ) );

						if ( $categories ) {
							foreach ( $categories as $category ) {
								$checked = '';
								if ( in_array( $category->term_id, $term_list ) ) {
									$checked = ' checked';
								}
								printf( '<li id="category-%1$s"><label class="selectit"><input value="%1$s" type="checkbox" name="post_category[]" id="in-category-%1$s" %3$s> %2$s</label></li>', $category->term_id, $category->name, $checked );
							}
						}
						?>
                    </ul>
                </div>
            </div>
			<?php
		}

		/**
		 * Save promotion title custom field
		 *
		 * @return void 
		 */
		public function display_promotion_metabox( $post ) {
			$is_promotion_title = get_post_meta( $post->ID, 'is_promotion_title', true );
			?>
			<div id="promotion-field" class="display_promotion">
				<div id="promotion-field-all">
					<input type="checkbox" id="is-promotion" name="is_promotion_title" value="1" <?php checked( $is_promotion_title ); ?>/>
					<label for="is-promotion">This is promotion title</label>
				</div>
			</div>
			<?php
		}

		/**
		 * Save data
		 *
		 * @param $post_id
		 * @param $post
		 */
		function wpr_industry_save_metabox( $post_id, $post ) {
			$nonce_name   = isset( $_POST['industry_nonce'] ) ? $_POST['industry_nonce'] : '';
			$nonce_action = 'industry_nonce_action';

			// Check if nonce is set.
			if ( ! isset( $nonce_name ) ) {
				return;
			}

			// Check if nonce is valid.
			if ( ! wp_verify_nonce( $nonce_name, $nonce_action ) ) {
				return;
			}

			// Check if user has permissions to save data.
			if ( ! current_user_can( 'edit_post', $post_id ) ) {
				return;
			}

			// Check if not an autosave.
			if ( wp_is_post_autosave( $post_id ) ) {
				return;
			}

			// Check if not a revision.
			if ( wp_is_post_revision( $post_id ) ) {
				return;
			}

			wp_set_post_terms( $post_id, $_POST['post_category'], 'category' );

			if ( isset( $_POST['is_promotion_title'] ) ) {
				update_post_meta( $post_id, 1, 'is_promotion_title' );
			} else {
				delete_post_meta( $post_id, 'is_promotion_title' );
			}
		}
	}
}

$wpr_custom_title = new WPR_Custom_Titles();
