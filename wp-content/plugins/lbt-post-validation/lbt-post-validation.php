<?php
/**
 * Plugin Name: LBT Post Validation
 * Description: Validate posts by looking for broken images and video links
 * Author:      Webpigment
 * Author URI:  https://www.webpigment.com
 * Version:     1.0.0
 *
 * @package    LBT_Post_Validation
 * @author     Webpigment
 * @since      1.0.0
 * @license    GPL-3.0+
 * @copyright  Copyright (c) 2021, Webpigment
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Post Validation
 */
class LBT_Post_Validation {
	/**
	 * Instance of the object
	 *
	 * @var \Object
	 */
	private static $instance = null;

	/**
	 * Setup singleton instance
	 *
	 * @return  \Object
	 */
	public static function get_instance() {
		if ( is_null( self::$instance ) ) {
			self::$instance = new static();

			register_activation_hook( __FILE__, array( self::$instance, 'on_activation' ) );
			register_deactivation_hook( __FILE__, array( self::$instance, 'on_deactivation' ) );
		}

		return self::$instance;
	}

	/**
	 * Private constructor
	 *
	 * @return  void
	 */
	private function __construct() {
		add_filter( 'cron_schedules', array( $this, 'cron_schedules' ) );

		add_action( 'lbt_validate_posts', array( $this, 'validate_posts' ) );

		if ( isset( $_GET['test_post_validation'] ) ) {
			add_action( 'init', function() {
				$this->validate_posts();
			});
		}
	}

	/**
	 * On plugin activation
	 *
	 * @return  void
	 */
	public function on_activation() {
		$this->run_cron();
	}

	/**
	 * On plugin deactivation
	 *
	 * @return  void
	 */
	public function on_deactivation() {
		$this->clear();
	}

	/**
	 * Add custom cron schedules
	 *
	 * @param array $schedules The existing schedules.
	 *
	 * @return  array
	 */
	public function cron_schedules( $schedules ) {
		$schedules['15_minutes'] = array(
			'interval' => 900,
			'display'  => __( 'Every 15 minutes' ),
		);

		return $schedules;
	}

	/**
	 * Run cron
	 *
	 * @return  void
	 */
	public function run_cron() {
		$next = wp_next_scheduled( 'lbt_validate_posts' );

		if ( ! $next ) {
			wp_schedule_event( time(), '15_minutes', 'lbt_validate_posts' );
		}
	}

	/**
	 * Clear cron schedules
	 *
	 * @return  void
	 */
	public function clear() {
		wp_clear_scheduled_hook( 'lbt_validate_posts' );
	}

	/**
	 * Validate posts
	 *
	 * @return  void
	 */
	public function validate_posts() {
		$total_posts         = wp_count_posts();
		$this->lbt_validation_log( json_encode( $total_posts ) );
		$total_publish_posts = (int) $total_posts->publish;

		$posts_per_cron = $total_publish_posts / 96;

		$validated_posts = get_option( 'lbt_validated_posts', array() );

		$query_args = array(
			'post_type'      => 'post',
			'post_status'    => 'publish',
			'posts_per_page' => round( $posts_per_cron ),
			'order_by'       => 'rand',
			'tag__not_in'    => array( 81 ),
		);

		if ( ! empty( $validated_posts ) ) {
			$query_args['post__not_in'] = $validated_posts;
		}

		$query = new \WP_Query( $query_args );

		if ( ! $query->have_posts() ) {
			$this->lbt_validation_log( 'No posts found. Script will start a new' );
			delete_option( 'lbt_validated_posts' );
			return;
		}

		$img_regex     = '/<img[^>]* src=\"([^\"]*)\"[^>]*>/';
		$youtube_regex = '/(?:https?:\/\/)?(?:www.)?(?:youtube.com|youtu.be)\/(?:watch\?v=)?([^\s]+)/';

		$this->lbt_validation_log( 'TOTAL PUBLISH POSTS FOUND: ' . round( $total_publish_posts ) );
		$this->lbt_validation_log( 'TOTAL POSTS PER CRON: ' . round( $posts_per_cron ) );
		$this->lbt_validation_log( 'TOTAL POSTS FOUND IN THIS ITERATION : ' . count( $query->posts ) );
		$total_deleted = 0;

		$skip            = array( 'christies-promotions', 'sothebys-promotions' );
		$skip_categories = array( 36, 37, 38, 39 );

		$default_image_code = md5( file_get_contents( trailingslashit( get_site_url() ) . 'wp-content/uploads/2021/08/1628107460_458_hqdefault.jpg' ) );

		foreach ( $query->posts as $p ) {
			if ( in_array( $p->post_name, $skip, true ) ) {
				continue;
			}

			$post_categories     = get_the_category( $p->ID );

			if ( ! empty( $post_categories ) ) {
				$post_categories_ids = wp_list_pluck( $post_categories, 'term_id' );

				if ( ! empty( array_intersect( $skip_categories, $post_categories_ids ) ) ) {
					continue;
				}
			}

			$post_content = $p->post_content;

			$total_img_matches = preg_match_all( $img_regex, $post_content, $matches, PREG_PATTERN_ORDER );
			$total_ytb_matches = preg_match_all( $youtube_regex, $post_content, $v_matches, PREG_PATTERN_ORDER );

			if ( false === $total_img_matches || false === $total_ytb_matches ) {
				$this->lbt_validation_log( 'REGEX returned ERROR while checking if images and youtube videos are present in the content' );
				continue;
			}

			if ( 0 === (int) $total_img_matches && 0 === (int) $total_ytb_matches ) {
				$this->lbt_validation_log( 'No images or videos found in post content. Post DELETED (' . $p->post_title . ' - ' . get_permalink( $p->ID ) . ' )' );
				$total_deleted++;
				wp_delete_post( $p->ID, true );
				continue;
			}

			$broken_images = 0;
			$broken_videos = 0;

			if ( 0 === (int) $total_ytb_matches ) {
				$imgs_tag = $matches[0];
				$imgs_src = $matches[1];

				foreach ( $imgs_src as $index => $src ) {
					$img_exists = $this->remote_file_exists( $src );

					if ( ! $img_exists && 1 === $total_img_matches ) {
						$this->lbt_validation_log( 'No videos found. 1 broken image found. Post DELETED (' . $p->post_title . ' - ' . get_permalink( $p->ID ) . ' )' );
						$total_deleted++;
						wp_delete_post( $p->ID, true );
						break;
					} elseif ( ! $img_exists || ( $img_exists && md5( file_get_contents( $src ) ) === $default_image_code ) ) {
						$broken_images++;
						$post_content = str_replace( $imgs_tag[ $index ], '', $post_content );
					}
				}

				if ( $broken_images === (int) $total_img_matches ) {
					$this->lbt_validation_log( 'No videos found. ' . $broken_images . ' broken image found. Post DELETED (' . $p->post_title . ' - ' . get_permalink( $p->ID ) . ' )' );
					$total_deleted++;
					wp_delete_post( $p->ID, true );
					continue;
				}

				if ( $broken_images > 0 ) {
					wp_update_post(
						array(
							'ID'           => $p->ID,
							'post_content' => $post_content,
						)
					);
				}
			} elseif ( 0 === $total_img_matches ) {
				$vids_src = $v_matches[0];

				foreach ( $vids_src as $index => $src ) {
					$vid_exists = $this->youtube_video_exists( $src );

					if ( ! $vid_exists && 1 === $total_ytb_matches ) {
						$this->lbt_validation_log( 'No images found. 1 broken video link found. Post DELETED (' . $p->post_title . ' - ' . get_permalink( $p->ID ) . ' )' );
						$total_deleted++;
						wp_delete_post( $p->ID, true );
						break;
					} elseif ( ! $vid_exists ) {
						$broken_videos++;
						$post_content = str_replace( $src, '', $post_content );
					}
				}

				if ( $broken_videos === (int) $total_ytb_matches ) {
					$this->lbt_validation_log( 'No images found. ' . $broken_videos . ' broken video links found. Post DELETED (' . $p->post_title . ' - ' . get_permalink( $p->ID ) . ' )' );
					$total_deleted++;
					wp_delete_post( $p->ID, true );
					continue;
				}

				if ( $broken_videos > 0 ) {
					wp_update_post(
						array(
							'ID'           => $p->ID,
							'post_content' => $post_content,
						)
					);
				}
			} elseif ( $total_img_matches && $total_ytb_matches ) {
				$imgs_tag = $matches[0];
				$imgs_src = $matches[1];
				$vids_src = $v_matches[0];

				foreach ( $imgs_src as $index => $src ) {
					$img_exists = $this->remote_file_exists( $src );

					if ( ! $img_exists || ( $img_exists && md5( file_get_contents( $src ) ) === $default_image_code ) ) {
						$broken_images++;
						$post_content = str_replace( $imgs_tag[ $index ], '', $post_content );
					}
				}

				foreach ( $vids_src as $index => $src ) {
					$vid_exists = $this->youtube_video_exists( $src );

					if ( ! $vid_exists ) {
						$broken_videos++;
						$post_content = str_replace( $src, '', $post_content );
					}
				}

				if ( (int) $total_ytb_matches === $broken_videos && (int) $total_img_matches === $broken_images ) {
					$this->lbt_validation_log( $broken_videos . ' broken video links found and ' . $broken_images . ' broken images found. Post DELETED (' . $p->post_title . ' - ' . get_permalink( $p->ID ) . ' )' );
					$total_deleted++;
					wp_delete_post( $p->ID, true );
					continue;
				}

				if ( $broken_videos > 0 || $broken_images > 0 ) {
					wp_update_post(
						array(
							'ID'           => $p->ID,
							'post_content' => $post_content,
						)
					);
				}
			}

			$validated_posts[] = $p->ID;
		}

		$this->lbt_validation_log( 'TOTAL POSTS DELETED: ' . $total_deleted );

		update_option( 'lbt_validated_posts', $validated_posts );
	}

	/**
	 * Check if a remote file exists.
	 *
	 * @param  string $url The url to the remote file.
	 * @return bool        Whether the remote file exists.
	 */
	private function remote_file_exists( $url ) {
		$response = wp_remote_head( $url );

		return 200 === wp_remote_retrieve_response_code( $response );
	}

	/**
	 * Check if youtube video exists
	 *
	 * @param   string $url  The video url.
	 *
	 * @return  bool
	 */
	private function youtube_video_exists( $url ) {
		$url = 'https://www.youtube.com/oembed?url=' . $url . '&format=json';

		return $this->remote_file_exists( $url );
	}

	/**
	 * Post validation custom log
	 *
	 * @param   mixed  $entry Some data to write in log.
	 * @param   string $mode Write, append.
	 * @param   string $file Name of log file.
	 *
	 * @return  string
	 */
	private function lbt_validation_log( $entry, $mode = 'a', $file = 'lbt_post_validation' ) {
		// Get WordPress uploads directory.
		$upload_dir = wp_upload_dir();
		$upload_dir = $upload_dir['basedir'];

		// If the entry is array, json_encode.
		if ( is_array( $entry ) ) {
			$entry = json_encode( $entry );
		}

		// Write the log file.
		$file  = $upload_dir . '/' . $file . '.log';
		$file  = fopen( $file, $mode );
		$bytes = fwrite( $file, current_time( 'mysql' ) . "::" . $entry . "\n" );
		fclose( $file );

		return $bytes;
	}
}

LBT_Post_Validation::get_instance();
