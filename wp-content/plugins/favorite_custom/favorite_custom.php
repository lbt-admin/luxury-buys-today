<?php

/*
  Plugin Name: Favorite Custom
  Plugin URI: http://www.wpriders.com/
  Description: Favorite
  Version: 1.0.0
  Author: Florin from WPRiders
  Author URI: http://www.wpriders.com/
  Text Domain: favorite_custom
  Domain Path: /languages
 */

use Favorites\Entities\Favorite;
use Favorites\Entities\User\UserRepository;
use Favorites\Entities\Post\FavoriteCount;
use Favorites\Config\SettingsRepository;

add_action('plugins_loaded', function () {
    if (class_exists('Favorites\Entities\Favorite\FavoriteButton')) {

        class CustomFavoriteButton extends Favorites\Entities\Favorite\FavoriteButton {

            /**
             * The Post ID
             */
            private $post_id;

            /**
             * Site ID
             */
            private $site_id;

            /**
             * Group ID
             */
            private $group_id;

            /**
             * User Respository
             */
            private $user;

            /**
             * Favorite Count Object
             */
            private $count;

            /**
             * Button Options
             * @var array
             */
            private $button_options;

            /**
             * Favorited?
             * @var bool
             */
            private $favorited;

            /**
             * Settings Repository
             */
            private $settings_repo;

            public function __construct($post_id, $site_id, $group_id = 1) {
                parent::__construct($post_id, $site_id, $group_id);
                $this->user = new UserRepository;
                $this->settings_repo = new SettingsRepository;
                $this->count = new FavoriteCount;
                $this->post_id = $post_id;
                $this->site_id = $site_id;
                $this->group_id = $group_id;
            }

            public function display_like() {
                if (!$this->settings_repo->cacheEnabled()) {
                    $loading = false;
                }
                if (!$this->user->getsButton()) {
                    return false;
                }

                $this->button_options = $this->settings_repo->formattedButtonOptions();
                $this->favorited = ( $this->user->isFavorite($this->post_id, $this->site_id, null, $this->group_id) ) ? true : false;
                $count = $this->count->getCount($this->post_id, $this->site_id);
                $like_string = __('Like', 'lbtv2') . '<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 26 28" fill="none" stroke="currentColor" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" class="feather feather-thumbs-up"><path d="M14 9V5a3 3 0 0 0-3-3l-4 9v11h11.28a2 2 0 0 0 2-1.7l1.38-9a2 2 0 0 0-2-2.3zM7 22H4a2 2 0 0 1-2-2v-7a2 2 0 0 1 2-2h3"></svg>';
                $out = <<<SECTION
			<p class="simplefavorite-button like" data-postid="{$this->post_id}" data-siteid="{$this->site_id}" data-groupid="{$this->group_id}" data-favoritecount="{$count}">{$like_string}</p>
SECTION;


                return $out;
            }

            public function display_lookbook() {
                if (!$this->settings_repo->cacheEnabled()) {
                    $loading = false;
                }
                if (!$this->user->getsButton()) {
                    return false;
                }

                $this->button_options = $this->settings_repo->formattedButtonOptions();
                $this->favorited = ( $this->user->isFavorite($this->post_id, $this->site_id, null, $this->group_id) ) ? true : false;
                $count = $this->count->getCount($this->post_id, $this->site_id);
                if (!$this->favorited) {
                    $like_string = __('Add To Lookbook', 'lbtv2');
                } else {
                    $like_string = __('Remove From Lookbook', 'lbtv2');
                }
                $out = <<<SECTION
			<div class="simplefavorite-button lookbook blackButton" data-postid="{$this->post_id}" data-siteid="{$this->site_id}" data-groupid="{$this->group_id}" data-favoritecount="{$count}">{$like_string}</div>
SECTION;


                return $out;
            }

            public function display_img() {
                global $wp;
                if (!$this->settings_repo->cacheEnabled()) {
                    $loading = false;
                }
                if (!$this->user->getsButton()) {
                    return false;
                }

                $this->button_options = $this->settings_repo->formattedButtonOptions();
                $this->favorited = ( $this->user->isFavorite($this->post_id, $this->site_id, null, $this->group_id) ) ? true : false;
                $count = $this->count->getCount($this->post_id, $this->site_id);
                $look_new = get_image_path('add_to_lookbook_1.svg');
                $look_saved = get_image_path('add_to_lookbook_red.svg');
                $page_visited = $wp->request;
                $out = <<<SECTION
			<img src="{$look_new}" class="simplefavorite-button bookmark" data-postid="{$this->post_id}" data-siteid="{$this->site_id}" data-groupid="{$this->group_id}" data-favoritecount="{$count}" title="Add To Lookbook"/>
			<img src="{$look_saved}" class="simplefavorite-button bookmark activated" data-postid="{$this->post_id}" data-siteid="{$this->site_id}" data-groupid="{$this->group_id}" data-favoritecount="{$count}" data-pagevisited="{$page_visited}" title="Remove From Lookbook"/>
SECTION;


                return $out;
            }

        }

    }
});

function get_favorite_as_like($post_id = null, $site_id = null, $group_id = null) {
    global $blog_id;
    if (!$post_id) {
        $post_id = get_the_id();
    }
    if (!$group_id) {
        $group_id = 1;
    }
    $site_id = ( is_multisite() && is_null($site_id) ) ? $blog_id : $site_id;
    if (!is_multisite()) {
        $site_id = 1;
    }
    $button = new CustomFavoriteButton($post_id, $site_id);

    return $button->display_like();
}

function get_favorite_as_image($post_id = null, $site_id = null, $group_id = null) {
    global $blog_id;
    if (!$post_id) {
        $post_id = get_the_id();
    }
    if (!$group_id) {
        $group_id = 1;
    }
    $site_id = ( is_multisite() && is_null($site_id) ) ? $blog_id : $site_id;
    if (!is_multisite()) {
        $site_id = 1;
    }
    $button = new CustomFavoriteButton($post_id, $site_id);

    return $button->display_img();
}

function get_favorite_as_lookbook($post_id = null, $site_id = null, $group_id = null) {
    global $blog_id;
    if (!$post_id) {
        $post_id = get_the_id();
    }
    if (!$group_id) {
        $group_id = 1;
    }
    $site_id = ( is_multisite() && is_null($site_id) ) ? $blog_id : $site_id;
    if (!is_multisite()) {
        $site_id = 1;
    }
    $button = new CustomFavoriteButton($post_id, $site_id);

    return $button->display_lookbook();
}
