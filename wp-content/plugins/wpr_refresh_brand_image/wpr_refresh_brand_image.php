<?php
/**
 * Plugin Name:  WPRiders Refresh Brand Image
 * Plugin URI:   https://www.wpriders.com/hire-us-ref
 * Description:  This Refresh Brand Image
 * Author:       Ovidiu George Irodiu from WPRiders
 * Author URI:   https://www.wpriders.com/hire-us-ref
 * License:      GPL2
 * License URI:  https://www.gnu.org/licenses/gpl-2.0.html
 */

if ( ! defined( 'ABSPATH' ) ) {
	header( 'Status: 403 Forbidden' );
	header( 'HTTP/1.1 403 Forbidden' );
	exit;
}

if ( ! class_exists( 'WPR_Refresh_Brand_Image' ) ) {
	/**
	 * Class WPR_Refresh_Brand_Image
	 */
	class WPR_Refresh_Brand_Image {
		private $wpr_facebook_cron = 'wpr_facebook_get_image';

		/**
		 * WPR_Refresh_Brand_Image constructor.
		 */
		function __construct() {
			register_activation_hook( __FILE__, array( $this, 'wpr_create_db_table' ) );
			register_deactivation_hook( __FILE__, 'wpr_plugin_deactivation' );

			add_action( 'create_category', array( $this, 'wpr_brand_add_new' ), 10, 1 );
			add_action( 'edited_category', array( $this, 'wpr_brand_update' ), 10, 2 );
			add_filter( 'cron_schedules', array( $this, 'wpr_add_cron_interval' ) );
			add_action( 'wpr_fetch_fb_profile_image_for_brand', array( $this, 'wpr_fetch_fb_profile_image_for_brand_function' ) );
			add_action( 'wpr_reset_fetching_fb_profile_images', array( $this, 'wpr_reset_fetching_fb_profile_images_function' ) );
		}

		/**
		 * Add new Brand
		 *
		 * @param $term_id
		 */
		function wpr_brand_add_new( $term_id ) {
			$this->wpr_do_the_brand_saving( $term_id );
		}

		/**
		 * Edit Brand
		 *
		 * @param $term_id
		 * @param $taxonomy
		 */
		function wpr_brand_update( $term_id, $taxonomy ) {
			$this->wpr_do_the_brand_saving( $term_id );
		}

		/**
		 * Execute Brand entry
		 */
		function wpr_fetch_fb_profile_image_for_brand_function() {
			$brand_info = $this->wpr_fetch_brand_to_execute();
			if ( $brand_info ) {
				$fetch_facebook_profile_img = $this->wpr_fetch_facebook_profile_image( $brand_info[0]->facebook_id );

				if ( ! empty( $fetch_facebook_profile_img ) ) {
					update_term_meta( absint( $brand_info[0]->brand_id ), 'wpr_brand_image_url', esc_url( $fetch_facebook_profile_img ) );

					$brand_data = array(
						'brand_id'    => absint( $brand_info[0]->brand_id ),
						'facebook_id' => esc_attr( $brand_info[0]->facebook_id ),
						'fetched'     => 1,
					);

					$this->wpr_update_brand_data( $brand_data, $brand_info[0]->brand_id );
				} else {
					$brand_data = array(
						'fetched' => 1,
					);

					$this->wpr_update_brand_data( $brand_data, $brand_info[0]->brand_id );
				}
			}
		}

		/**
		 * Reset Brand entries
		 */
		function wpr_reset_fetching_fb_profile_images_function() {
			global $wpdb;

			$reset_table = $wpdb->prefix . $this->wpr_facebook_cron;
			$wpdb->query(
				$wpdb->prepare(
					"
	                 UPDATE $reset_table
					 SET fetched = %d
					",
					0
				)
			);
		}

		/**
		 * Fetch facebook profile image by facebook id
		 *
		 * @param $facebook_id
		 *
		 * @return bool|string
		 */
		function wpr_fetch_facebook_profile_image( $facebook_id ) {
			if ( empty( $facebook_id ) ) {
				return false;
			}

			$link = "http://graph.facebook.com/{$facebook_id}/picture?width=600";

			// Get profile picture info from FB via cURL
			$url = $link . "&redirect=false";
			$ch  = curl_init();
			curl_setopt( $ch, CURLOPT_URL, $url );
			curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
			$profile_picture = curl_exec( $ch );
			curl_close( $ch );

			$image_url = '';
			if ( $profile_picture ) {
				$profile_picture = json_decode( $profile_picture );

				$image_url = esc_url_raw( $profile_picture->data->url );
			}

			return $image_url;
		}

		/**
		 * Get wp charset collate
		 *
		 * @return string
		 */
		protected static function get_wp_charset_collate() {
			global $wpdb;
			$charset_collate = '';

			if ( ! empty ( $wpdb->charset ) )
				$charset_collate = "DEFAULT CHARACTER SET $wpdb->charset";

			if ( ! empty ( $wpdb->collate ) )
				$charset_collate .= " COLLATE $wpdb->collate";

			return $charset_collate;
		}

		/**
		 * Create DB table
		 */
		function wpr_create_db_table() {
			global $wpdb;

//			$charset_collate = self::get_wp_charset_collate();

			$wpr_facebook_table = $wpdb->prefix . $this->wpr_facebook_cron;
			if ( $wpdb->get_var( "show tables like '$wpr_facebook_table'" ) !== $wpr_facebook_table ) {

				$sql = 'CREATE TABLE `' . $wpr_facebook_table . '` ( ';
				$sql .= '  `id` bigint(20) NOT NULL AUTO_INCREMENT, ';
				$sql .= '  `brand_id` bigint(20) NOT NULL, ';
				$sql .= '  `facebook_id` longtext  COLLATE utf8_unicode_ci, ';
				$sql .= '  `fetched` int(1) NOT NULL DEFAULT "0", ';
				$sql .= '  PRIMARY KEY (`id`) ';
				$sql .= ") ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;";
				require_once( ABSPATH . '/wp-admin/includes/upgrade.php' );
				dbDelta( $sql );
			}

			$this->wpr_save_brands_term();

			// Register cron events
			if ( ! wp_next_scheduled( 'wpr_fetch_fb_profile_image_for_brand' ) ) {
				wp_schedule_event( time(), '1min', 'wpr_fetch_fb_profile_image_for_brand' );
			}

			if ( ! wp_next_scheduled( 'wpr_reset_fetching_fb_profile_images' ) ) {
				wp_schedule_event( time(), 'daily', 'wpr_reset_fetching_fb_profile_images' );
			}
		}

		/**
		 * Remove Cron jobs
		 */
		function wpr_plugin_deactivation() {
			wp_clear_scheduled_hook( 'wpr_fetch_fb_profile_image_for_brand' );
			wp_clear_scheduled_hook( 'wpr_reset_fetching_fb_profile_images' );
		}

		/**
		 * Save Brand Categories
		 */
		function wpr_save_brands_term() {
			$categories = get_categories( array(
				'orderby' => 'name',
				'order'   => 'ASC',
				'exclude' => 1,
			) );

			if ( ! empty( $categories ) ) {
				foreach ( $categories as $category ) {
					if ( $category->parent > 0 ) {
						$this->wpr_do_the_brand_saving( $category->term_id );
					}
				}
			}
		}

		/**
		 * Add/Save Brand
		 *
		 * @param $brand_id
		 */
		function wpr_do_the_brand_saving( $brand_id ) {
			if ( ! is_int( $brand_id ) ) {
				return;
			}

			if ( $this->wpr_fetch_brand_facebook_url( $brand_id ) ) {
				$brand_data = array(
					'brand_id'    => absint( $brand_id ),
					'facebook_id' => esc_attr( $this->wpr_fetch_facebook_id_from_url( $this->wpr_fetch_brand_facebook_url( $brand_id ) ) ),
					'fetched'     => 0,
				);

				if ( $this->wpr_is_brand_saved( $brand_id ) ) {
					$this->wpr_update_brand_data( $brand_data, $brand_id );
				} else {
					$this->wpr_insert_brand_data( $brand_data );
				}
			}
		}

		/**
		 * Fetch facebook user id
		 *
		 * @param $url
		 *
		 * @return bool|string
		 */
		function wpr_fetch_facebook_id_from_url( $url ) {
			$facebook_id = false;

			if ( empty( $url ) ) {
				return $facebook_id;
			}

			$pattern = '/(?:https?:\/\/)?(?:www\.)?facebook\.com\/(?:(?:\w)*#!\/)?(?:pages\/)?(?:[\w\-]*\/)*?(\/)?([\w\-\.]*)/i';
			preg_match( $pattern, $url, $matches );

			if ( ! empty( $matches ) ) {
				$facebook_id = esc_attr( $matches[2] );
			}

			return $facebook_id;
		}

		/**
		 * Fetch brand facebook url
		 *
		 * @param $brand_id
		 *
		 * @return bool|string
		 */
		function wpr_fetch_brand_facebook_url( $brand_id ) {
			$facebook_url        = false;
			$wpr_brand_link_text = get_term_meta( $brand_id, 'wpr_brand_link_text', true );

			if ( $wpr_brand_link_text ) {
				$facebook_url = esc_url_raw( $wpr_brand_link_text );
			}

			return $facebook_url;
		}

		/**
		 * Check if Brand is saved in Facebook table
		 *
		 * @param $brand_id
		 *
		 * @return bool|null|string
		 */
		function wpr_is_brand_saved( $brand_id ) {
			if ( empty( $brand_id ) ) {
				return false;
			}

			$get_brand = false;
			if ( is_int( $brand_id ) ) {
				global $wpdb;
				$wpr_facebook_table = $wpdb->prefix . $this->wpr_facebook_cron;

				$get_brand = $wpdb->get_var( $wpdb->prepare( "SELECT COUNT(*) FROM {$wpr_facebook_table} WHERE brand_id = %d", $brand_id ) );
			}

			return $get_brand;
		}

		/**
		 * Fetch not fetched Brand
		 *
		 * @return bool
		 */
		function wpr_fetch_brand_to_execute() {
			global $wpdb;

			$table_brand = $wpdb->prefix . $this->wpr_facebook_cron;
			$result      = $wpdb->get_results(
				$wpdb->prepare( "SELECT brand_id,facebook_id FROM $table_brand WHERE fetched = %d LIMIT 1", 0 )
				, OBJECT );

			if ( $result ) {
				return $result;
			} else {
				return false;
			}
		}

		/**
		 * Insert in table
		 *
		 * @param $save_data
		 */
		function wpr_insert_brand_data( $save_data ) {
			global $wpdb;

			$wpdb->insert(
				$wpdb->prefix . $this->wpr_facebook_cron,
				$save_data,
				array(
					'%d',
					'%s',
					'%d'
				)
			);
		}

		/**
		 * Update table
		 *
		 * @param $update_data
		 * @param $brand_id
		 */
		function wpr_update_brand_data( $update_data, $brand_id ) {
			global $wpdb;

			$wpdb->update(
				$wpdb->prefix . $this->wpr_facebook_cron,
				$update_data,
				array( 'brand_id' => $brand_id ),
				array(
					'%d',
					'%s',
					'%d'
				),
				array( '%d' )
			);
		}

		/**
		 * Add 1 minute cron job interval
		 *
		 * @param $schedules
		 *
		 * @return mixed
		 */
		function wpr_add_cron_interval( $schedules ) {
			if ( ! isset( $schedules["1min"] ) ) {
				$schedules["1min"] = array(
					'interval' => 60,
					'display'  => __( 'Once every 1 minute' )
				);
			}

			return $schedules;
		}
	}
}
$wpr_refresh_brand_image = new WPR_Refresh_Brand_Image();