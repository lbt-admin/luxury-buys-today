<?php
/**
 * Plugin Name:  WPRiders Overwrite Yoast Open Graph tags
 * Plugin URI:   https://www.wpriders.com/hire-us-ref
 * Description:  This Overwrite Yoast Open Graph tags
 * Author:       Ovidiu George Irodiu from WPRiders
 * Author URI:   https://www.wpriders.com/hire-us-ref
 * License:      GPL2
 * License URI:  https://www.gnu.org/licenses/gpl-2.0.html
 */

if ( ! defined( 'ABSPATH' ) ) {
	header( 'Status: 403 Forbidden' );
	header( 'HTTP/1.1 403 Forbidden' );
	exit;
}

if ( ! class_exists( 'WPR_Overwrite_Yoast_OG' ) ) {
	/**
	 * Class WPR_Overwrite_Yoast_OG
	 */
	class WPR_Overwrite_Yoast_OG {
		/**
		 * WPR_Overwrite_Yoast_OG constructor.
		 */
		public function __construct() {
			add_filter( 'wpseo_opengraph_image', array( $this, 'wpr_change_image' ), 30 );
			add_filter( 'wpseo_twitter_image', array( $this, 'wpr_change_image' ), 30 );
		}

		/**
		 * Overwrite OG image
		 *
		 * @param $image
		 *
		 * @return mixed|string
		 */
		function wpr_change_image( $image ) {
			if ( is_singular() ) {
				$image = $this->wpr_get_singular_image();
			} elseif ( is_category() ) {
				$image = $this->wpr_get_category_image();
			}

			// Default image.
			if ( empty( $image ) ) {
				$social_options = get_option( 'wpseo_social' );

				if ( ! empty( $social_options['og_default_image'] ) ) {
					$image = $social_options['og_default_image'];
				}
			}

			return $image;
		}

		/**
		 * Get category page image
		 *
		 * @return string
		 */
		public function wpr_get_category_image() {
			$category    = get_the_category();
			$category_id = $category[0]->term_id;
			$image_id    = get_term_meta( $category_id, 'category-image-id', true );
			$brand_image = wp_get_attachment_image_src( $image_id, 'large' );

			if ( ! empty( $brand_image ) ) {
				$image = esc_attr( $brand_image[0] );
			} else {
				$seo_options = WPSEO_Options::get_option( 'wpseo_social' );
				$image       = $seo_options['og_default_image'];
			}

			return $image;
		}

		/**
		 * Single page/post image
		 *
		 * @return bool|string
		 */
		public function wpr_get_singular_image() {
			global $post;

			$saved_image     = get_post_meta( $post->ID, 'wpr_social_image', true );
			$is_email_parser = get_post_meta( $post->ID, 'wpr_email_source', true );

			// If is twitter cards post
			$is_twitter_cards = get_post_meta( $post->ID, 'wpr_remote_twitter_cards', true );
			if ( $is_twitter_cards ) {
				return esc_url( $saved_image );
			}

			// Check if a social image has already been saved to the meta.
			if ( ! empty( $saved_image ) && ( ! $is_email_parser || $is_twitter_cards ) ) {
				return esc_url( $saved_image );
			}

			$image_min_width  = 200;
			$image_min_height = 200;

			$content = apply_filters( 'the_content', $post->post_content );

			$final_image = false;
			$images      = array();

			// External image
			$remote_image = get_post_meta( $post->ID, 'wpr_remote_featured_img', true );

			if ( ! get_post_meta( $post->ID, 'wpr_email_source', true ) ) {
				$clear_image = explode( '?', $remote_image );
				if ( ! empty( $clear_image ) ) {
					$remote_image = reset( $clear_image );
				}
			}

			$images[]['img_src'] = $remote_image;

			// Featured image
			$featured_image = get_the_post_thumbnail_url( $post->ID, 'full' );
			if ( ! empty( $featured_image ) ) {
				$images[]['img_src'] = $featured_image;
			}

			// Youtube image
			preg_match_all( '((?:www\.)?(?:youtube(?:-nocookie)?\.com\/(?:v\/|watch\?v=|embed\/)|youtu\.be\/)([a-zA-Z0-9?&=_-]*))', $content, $urls );
			$urls = array_filter( $urls );

			if ( ! empty( $urls ) ) {
				$youtube_url = $urls[0][0];

				$youtube_id = '';
				if ( strlen( trim( $youtube_url ) ) > 0 ) {
					$youtube_id = getYoutubeIdFromUrl( $youtube_url );
				}

				if ( $youtube_id ) {
					$youtube_image = 'https://img.youtube.com/vi/' . $youtube_id . '/hqdefault.jpg';
					// Get the bigger image
					$youtube_maxres_image = 'https://img.youtube.com/vi/' . trim( $youtube_id ) . '/maxresdefault.jpg';
					$youtube_sd_image     = 'https://img.youtube.com/vi/' . trim( $youtube_id ) . '/sddefault.jpg';
					if ( $this->remote_image_exists( $youtube_maxres_image ) ) {
						$youtube_image = $youtube_maxres_image;
					} elseif ( $this->remote_image_exists( $youtube_sd_image ) ) {
						$youtube_image = $youtube_sd_image;
					}
					$images[]['img_src'] = $youtube_image;
				}
			}

			$images = array_filter( $images, function ( $image ) {
				// Filter out empty and non-accessible images.
				return ( ! empty( $image['img_src'] ) && $this->remote_image_exists( $image['img_src'] ) );
			} );

			// Get image from content if there is none available
			if ( ! empty( $content ) && empty( $images ) ) {
				$doc = new DOMDocument();
				libxml_use_internal_errors( true );
				$doc->loadHTML( $content );
				libxml_clear_errors();

				$image_tags = $doc->getElementsByTagName( 'img' );

				if ( $image_tags ) {
					$img_min_width  = $image_min_width;
					$img_min_height = $image_min_height;

					foreach ( $image_tags as $tag ) {
						$pic = $tag->getAttribute( 'data-original' );

						if ( ! $pic ) {
							$pic = $tag->getAttribute( 'src' );
						}

						if ( ! preg_match( "~^(?:f|ht)tps?://~i", $pic ) ) {
							$pic = "http:" . $pic;
						}

						$data = file_get_contents( $pic, null, null, 0, 32768 );
						$img  = @imagecreatefromstring( $data );

						// Find the largest image in the post content.
						if ( $img && ( imagesx( $img ) * imagesy( $img ) > $img_min_width * $img_min_height ) ) {
							$img_min_width  = imagesx( $img );
							$img_min_height = imagesy( $img );
							$content_image  = $pic;
							break;
						}
					}

					if ( ! empty( $content_image ) && empty( $remote_image ) ) {
						$clear_image = explode( '?', $content_image );
						if ( ! empty( $clear_image ) ) {
//							$content_image = reset( $clear_image );
						}

						$images[]['img_src'] = $content_image;

						update_post_meta( $post->ID, 'wpr_remote_featured_img', esc_url_raw( $content_image ) );
						update_post_meta( $post->ID, 'wpr_remote_image_atts', array(
							'width'  => $img_min_width,
							'height' => $img_min_height,
						) );
					}
				}
			}

			if ( empty( $images ) ) {
				return false;
			}

			// Find the largest image from all sources.
			$selected_images = array();
			$minimum_value   = absint( $image_min_width * $image_min_height );
			foreach ( $images as $image ) {
				$image_size = getimagesize( $image['img_src'] );

				if ( empty( $image_size ) ) {
					continue;
				}

				list( $image_width, $image_height ) = $image_size;
				$this_image_dimension = absint( $image_width * $image_height );

				if ( $this_image_dimension > $minimum_value ) {
					$minimum_value = $this_image_dimension;
					$final_image   = $image['img_src'];
				}
			}

			if ( $final_image ) {
				// If it's an external image, save it locally.
				if ( isset( $_SERVER['HTTP_HOST'] ) && strpos( $final_image, $_SERVER['HTTP_HOST'] ) === false ) {
					$local_final_image = wpr_save_media_from_url( $final_image );

					if ( $local_final_image ) {
						$final_image = $local_final_image;
					}
				}

				// Save the best available image to the post's meta.
				update_post_meta( $post->ID, 'wpr_social_image', $final_image );

				return esc_url( $final_image );
			}

			return false;
		}

		/**
		 * Check if image exists at given URL and is accessible.
		 *
		 * @param  string $url
		 *
		 * @return bool
		 */
		public function remote_image_exists( $url ) {
			if ( empty( $url ) ) {
				return false;
			}

			if ( isset( $_SERVER['HTTP_HOST'] ) && strpos( $url, $_SERVER['HTTP_HOST'] ) !== false ) {
				// Image is local, don't send head request.
				return true;
			}

			$remote_head = wp_remote_head( $url );

			if ( is_wp_error( $remote_head ) || 200 !== $remote_head['response']['code'] ) {
				return false;
			}

			return true;
		}
	}
}

$wpr_overwrite_yoast_og = new WPR_Overwrite_Yoast_OG();
