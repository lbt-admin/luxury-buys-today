<?php
/**
 * Plugin Name:  WPRiders Regenerate Comments
 * Plugin URI:   https://www.wpriders.com/hire-us-ref
 * Description:  This Regenerates Articles Comments
 * Author:       Ovidiu George Irodiu from WPRiders
 * Author URI:   https://www.wpriders.com/hire-us-ref
 * License:      GPL2
 * License URI:  https://www.gnu.org/licenses/gpl-2.0.html
 */

if ( ! defined( 'ABSPATH' ) ) {
	header( 'Status: 403 Forbidden' );
	header( 'HTTP/1.1 403 Forbidden' );
	exit;
}

if ( ! class_exists( 'WPR_Regenerate_Comments' ) ) {
	/**
	 * Class WPR_Regenerate_Comments
	 */
	class WPR_Regenerate_Comments {
		/**
		 * WPR_Regenerate_Comments constructor.
		 */
		function __construct() {
			add_action( 'admin_menu', array( &$this, 'wpr_add_submenu_post_template' ) );
			add_action( 'admin_enqueue_scripts', array( &$this, 'wpr_enqueue_scripts' ), 10 );
			add_action( 'wp_ajax_wpr_regenerate_comments_action', array( &$this, 'wpr_regenerate_comments_action' ), 10 );
//			add_action( 'wp_ajax_wpr_delete_old_posts', array( &$this, 'wpr_delete_old_posts' ), 10 );
		}

		/**
		 * Delete old posts
		 */
		function wpr_delete_old_posts() {
			$args  = array(
				'fields'         => 'ids',
				'post_type'      => 'post',
				'posts_per_page' => 1000,
				'date_query'     => array(
					'before'    => array(
						'year'  => 2017,
						'month' => 12,
						'day'   => 27,
					),
					'inclusive' => true,
				)
			);
			$posts = new WP_Query( $args );

			if ( $posts->have_posts() ) {
				while ( $posts->have_posts() ) {
					$posts->the_post();
					wp_delete_post( get_the_ID() );
				}
			}

			wp_reset_postdata();
		}

		public function wpr_enqueue_scripts() {
			wp_enqueue_script( 'jquery-ui-datepicker' );
			wp_register_style( 'jquery-ui', '//ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css' );
			wp_enqueue_style( 'jquery-ui' );
		}

		/**
		 * Add menu item
		 */
		public function wpr_add_submenu_post_template() {
			add_submenu_page(
				'edit.php',
				'Regenerate Comments',
				'Regenerate Comments',
				'manage_options',
				'wpr-regenerate-comments',
				array( &$this, 'wpr_regenerate_comments' ) );
		}

		/**
		 * Template
		 */
		public function wpr_regenerate_comments() {
			include_once( plugin_dir_path( __FILE__ ) . '/templates/wpr_regenerate_options.php' );
		}

		/**
		 * Run Regenerate Comment
		 */
		public function wpr_regenerate_comments_action() {
			parse_str( $_POST['dataform'], $form );
			if ( ! wp_verify_nonce( $form['wpr_regenerate_comments_nonce'], 'wpr_regenerate_comments_action' ) ) {
				return;
			}

			$args  = array(
				'posts_per_page' => - 1,
				'post_type'      => 'post',
				'date_query'     => array(
					array(
						'after'     => date( "F j, Y", strtotime( "-1 day", strtotime( $form['wpr_start_date'] ) ) ),
						'before'    => date( "F j, Y", strtotime( "+1 day", strtotime( $form['wpr_end_date'] ) ) ),
						'inclusive' => true,
					),
				),
			);
			$query = new WP_Query( $args );
			$posts = $query->get_posts();

			if ( $posts ) {
				foreach ( $posts as $post ) {
					$this->wpr_generate_random_article_content( $post->ID );
				}
			}

			wp_die();
		}

		/**
		 * Generate random comment
		 *
		 * @param $post_id
		 */
		function wpr_generate_random_article_content( $post_id ) {
			$edgar_cat  = array(
				'8'  => '36',
				'13' => '37',
				'14' => '38',
				'17' => '39',
			);
			$categories = get_the_category( $post_id );
			$category   = $categories[0];
			$cat_id     = $category->term_id;

			if ( $category->parent > 0 ) {
				$cat_id = $category->parent;
			}
			$get_post_from = $edgar_cat[ $cat_id ];

			$args = array(
				'post_type'      => 'post',
				'post_status'    => 'published',
				'posts_per_page' => 1,
				'post__not_in'   => array( $post_id ),
				'date_query'     => array( 'after' => '-7 days' ),
				'orderby'        => 'rand',
				'meta_key'       => 'wpr_wordai_string',
				'category__in'   => $get_post_from,
			);

			$edd_posts    = new WP_Query( $args );
			$post_content = '';
			$edd_post_id  = '';
			if ( $edd_posts->have_posts() ) {
				// The Loop
				while ( $edd_posts->have_posts() ) {
					$edd_posts->the_post();
					$post_content = get_post_meta( $edd_posts->post->ID, 'wpr_wordai_string', true );
					$edd_post_id  = $edd_posts->post->ID;
				}
				wp_reset_postdata();
			} else {
				unset( $args['date_query'] );

				$edd_posts = new WP_Query( $args );

				if ( $edd_posts->have_posts() ) {
					// The 2nd Loop
					while ( $edd_posts->have_posts() ) {
						$edd_posts->the_post();
						$post_content = get_post_meta( $edd_posts->post->ID, 'wpr_wordai_string', true );
						$edd_post_id  = $edd_posts->post->ID;
					}
					wp_reset_postdata();
				}
			}

			if ( ! empty( $post_content ) ) {
				$register_data = array(
					'spin_content' => array(
						'ID'           => $post_id,
						'post_content' => $post_content,
					),
				);

				$request_spin = new WPR_Run_Text_Spin();
				$request_spin->wpr_init_cron( $register_data );

				// Use the Spintax
//				$spintax = new Spintax();
//				wpr_add_post_meta( 'post', absint( $post_id ), 'wpr_spin_the_content', $spintax->process( wp_strip_all_tags( $post_content ) ) );
				update_post_meta( absint( $post_id ), 'wpr_spinned_edd_article', absint( $edd_post_id ) );

//				update_post_meta( $post_id, 'wpr_title_changed', sprintf( 'Random title from our custom title database for %s', get_cat_name( $cat_id ) ) );
			} else {
				$fetch_random_edd = $this->wpr_fetch_random_edd_content( $get_post_from, $post_id );
				if ( $fetch_random_edd ) {
					$register_data = array(
						'fetch_wordai_string' => array(
							'ID'           => $post_id,
							'post_content' => wp_strip_all_tags( $fetch_random_edd['post_content'] ),
							'edd_post_id'  => $fetch_random_edd['post_id'],
						),
					);

					$request_spin = new WPR_Run_Process();
					$request_spin->wpr_init_cron( $register_data );

					update_post_meta( absint( $post_id ), 'wpr_spinned_edd_article', absint( $fetch_random_edd['post_id'] ) );
				}
			}
		}

		/**
		 * Fetch random Edd Article content
		 *
		 * @param $get_post_from
		 * @param int $exclude A post ID to exclude from the query (optional).
		 *
		 * @return array
		 */
		function wpr_fetch_random_edd_content( $get_post_from, $exclude = null ) {
			$args = array(
				'post_type'      => 'post',
				'post_status'    => 'published',
				'posts_per_page' => 1,
				'orderby'        => 'rand',
				'cat'            => absint( $get_post_from ),
			);

			if ( $exclude ) {
				$args['post__not_in'] = array( absint( $exclude ) );
			}

			$edd_posts = new WP_Query( $args );

			$response = array();
			if ( $edd_posts->have_posts() ) {
				// The 2nd Loop
				while ( $edd_posts->have_posts() ) {
					$edd_posts->the_post();
					$response['post_id']      = $edd_posts->post->ID;
					$response['post_content'] = apply_filters( 'the_content', get_post_field( 'post_content', $edd_posts->post->ID ) );
				}
				wp_reset_postdata();
			}

			return $response;
		}
	}
}

new WPR_Regenerate_Comments();
