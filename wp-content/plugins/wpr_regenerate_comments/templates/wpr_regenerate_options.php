<?php
if ( ! defined( 'ABSPATH' ) ) {
	header( 'Status: 403 Forbidden' );
	header( 'HTTP/1.1 403 Forbidden' );
	exit;
}
?>

<script type="text/javascript">
    jQuery(document).ready(function ($) {
        $('.wpr_date_picker').datepicker({
            dateFormat: 'yy-mm-dd',
            maxDate: new Date
        });

        $("#wpr_end_date").datepicker({
            dateFormat: 'yy-mm-dd'
        }).datepicker("setDate", "0");

        $("#wpr_start_date").datepicker({
            dateFormat: 'yy-mm-dd'
        }).datepicker("setDate", "0");

        $('#wpr_regenerate_comments').on('submit', function (e) {
            e.preventDefault();

            $(this).append('<i class="fa fa-spinner fa-spin" style="font-size: 30px"></i>');

            var data = {
                action: 'wpr_regenerate_comments_action',
                dataform: $(this).serialize()
            };

            $.ajax({
                type: 'POST',
                url: "<?php echo admin_url( 'admin-ajax.php' ); ?>",
                data: data,
                success: function (data) {
                    $('i').remove('.fa-spinner');
                },
                error: function (request, status, error) {
                    console.log('Error');
                }
            });
        });

        $('#wpr-delete-old-posts').on('click', function (e) {
            e.preventDefault();

            $(this).append('<i class="fa fa-spinner fa-spin" style="font-size: 30px"></i>');

            var data = {
                action: 'wpr_delete_old_posts'
            };

            $.ajax({
                type: 'POST',
                url: "<?php echo admin_url( 'admin-ajax.php' ); ?>",
                data: data,
                success: function (data) {
                    $('i').remove('.fa-spinner');
                },
                error: function (request, status, error) {
                    console.log('Error');
                }
            });
        });
    });
</script>

<div class="wrap">
    <h2><?php echo __( 'Regenerate Articles Comments' ); ?></h2>

    <p><?php echo __( 'Articles Comments Regeneration will run in background with wp_cron()' ); ?></p>

    <form method="post" id="wpr_regenerate_comments" class="">
		<?php wp_nonce_field( 'wpr_regenerate_comments_action', 'wpr_regenerate_comments_nonce' ); ?>
        <table>
            <tr>
                <td width="15%">
                    <label for="wpr_new_email"><?php echo __( 'Articles starting from' ); ?></label>
                </td>
                <td width="10%"><input type="text" name="wpr_start_date" id="wpr_start_date" class="wpr_date_picker"/></td>
                <td width="5%" align="center">
                    <label for="wpr_new_email"><?php echo __( 'to' ); ?></label>
                </td>
                <td><input type="text" name="wpr_end_date" id="wpr_end_date" class="wpr_date_picker"/></td>
            </tr>
            <tr>
                <td colspan="3">
					<?php submit_button( 'Regenerate Comments', 'primary', 'wpr_regenerate' ); ?>
                </td>
            </tr>
        </table>
    </form>
<!--    <button id="wpr-delete-old-posts">--><?php //echo __( 'Delete old post till 27.12.2017 date. 1000 articles per run.' ); ?><!--</button>-->
</div>