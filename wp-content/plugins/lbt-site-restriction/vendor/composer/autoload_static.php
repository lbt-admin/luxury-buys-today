<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInitabe547bfdc79ae632bd1e8b6d02005bc
{
    public static $prefixLengthsPsr4 = array (
        'O' => 
        array (
            'OntraportAPI\\' => 13,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'OntraportAPI\\' => 
        array (
            0 => __DIR__ . '/..' . '/ontraport/sdk-php/src',
        ),
    );

    public static $classMap = array (
        'Composer\\InstalledVersions' => __DIR__ . '/..' . '/composer/InstalledVersions.php',
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInitabe547bfdc79ae632bd1e8b6d02005bc::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInitabe547bfdc79ae632bd1e8b6d02005bc::$prefixDirsPsr4;
            $loader->classMap = ComposerStaticInitabe547bfdc79ae632bd1e8b6d02005bc::$classMap;

        }, null, ClassLoader::class);
    }
}
