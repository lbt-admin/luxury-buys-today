<?php
/**
 * Template Name: Restricted Access Landing Page
 */

//phpcs:disable

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <?php wp_head(); ?>

    <style>
        html {
            margin: 0 !important;
            padding: 0;
        }

        body.page-template-restricted-access-landing-page {
            background: rgb(0,0,0);
            background: linear-gradient(180deg, rgba(0,0,0,1) 0%, rgba(0,0,0,0.938813025210084) 28%, rgba(119,119,119,1) 100%);
            background-repeat: no-repeat;
            padding: 50px 0;
        }

        .lbt-restricted-access-content {
            width: 60%;
            margin: 0 auto;
        }

        .lbt-restricted-access-content h2 {
            margin: 100px 0 50px;
            font-size: 72px;
            color: #ffffff;
        }

        .lbt-restricted-access-content p {
            margin-bottom: 55px;
            font-size: 34px;
            letter-spacing: 1px;
            text-align: center;
            color: #ffffff;
        }

        .lbt-restricted-access-content .gform_wrapper .gform_body {
            width: 100%;
            margin: 0 auto;
        }

        .lbt-restricted-access-content  form#gform_17 {
            background: #fff;
            padding: 20px;
            width: 60%;
        }
 
        .lbt-restricted-access-content .gform_wrapper .gform_footer input[type=submit] {
            width: 98%;
            height: 50px;
        }

        .lbt-restricted-access-content .gform_wrapper .gform_body .ginput_container_phone,
        .lbt-restricted-access-content .gform_wrapper .gform_body .ginput_container_email,
        .lbt-restricted-access-content .gform_wrapper .gform_body .ginput_container_phone input, 
        .lbt-restricted-access-content .gform_wrapper .gform_body .ginput_container_email input {
            width: 100%;
        }

        .lbt-restricted-access-content .gform_wrapper .gform_body .name_first label,
        .lbt-restricted-access-content .gform_wrapper .gform_body .name_last label {
            display: none;
        }

        .lbt-restricted-access-content .gform_wrapper .gform_body input {
            border: none;
            border-radius: 0;
            background: #C4CDCD;
            height: 50px;
        }

    </style>
</head>
<body <?php body_class(); ?>>
    <main>
        <div class="lbt-restricted-access-content">
            <?php the_content(); ?>
        </div>
    </main>
    <?php if ( ! is_user_logged_in() ): ?>
        <div class="login_register_pop"><div class="login_register_wrap"><div class="login_register_box">
            <div class="modal_header">
                <img src="https://luxurybuystoday-media.s3.amazonaws.com/media/uploads/2020/01/login-logo-png.png" alt="" />
                <button type="button" class="login-close" aria-label="Close">×</button>
            </div>
            <div class="zm-login-form">
            <div><?php echo do_shortcode('[gravityform id="3" ajax="false" title="false" description="false"]');?></div>
            <div style="text-align: center; margin-top: -20px;"><?php echo do_shortcode('[oa_social_login]');?></div>
            <p style="text-align: center; font-weight: 600;">Don't have an account? <a class="zm-signup" style="text-decoration: underline" href="#">Sign up</a></p>
            <p style="text-align: center"><a class="popmake-reset-password" href="/wp-login.php?action=lostpassword">Lost your password?</a></p>
            <?php if(!is_home()){?>
            <iframe style='display:none;width:0px;height:0px;' src='about:blank' name='gravity_login_frame' id='gravity_login_frame'>This iframe contains the logic required to handle Ajax powered Gravity Forms.</iframe>
                        <script type='text/javascript'>jQuery(document).ready(function($){$('.login_register_pop #gform_3').attr("target","gravity_login_frame");jQuery('#gravity_login_frame').load( function(){var contents = jQuery(this).contents().find('*').html();var is_postback = contents.indexOf('GF_AJAX_POSTBACK') >= 0;if(!is_postback){return;}var form_content = jQuery(this).contents().find('#gform_wrapper_3');var is_confirmation = jQuery(this).contents().find('#gform_confirmation_wrapper_3').length > 0;var is_redirect = contents.indexOf('gformRedirect(){') >= 0;var is_form = form_content.length > 0 && ! is_redirect && ! is_confirmation;if(is_form){jQuery('#gform_wrapper_3').html(form_content.html());if(form_content.hasClass('gform_validation_error')){jQuery('#gform_wrapper_3').addClass('gform_validation_error');} else {jQuery('#gform_wrapper_3').removeClass('gform_validation_error');}setTimeout( function() { /* delay the scroll by 50 milliseconds to fix a bug in chrome */  }, 50 );if(window['gformInitDatepicker']) {gformInitDatepicker();}if(window['gformInitPriceFields']) {gformInitPriceFields();}var current_page = jQuery('#gform_source_page_number_3').val();jQuery(document).trigger('gform_page_loaded', [3, current_page]);window['gf_submitting_3'] = false;}else if(!is_redirect){var confirmation_content = jQuery(this).contents().find('.GF_AJAX_POSTBACK').html();if(!confirmation_content){confirmation_content = contents;}setTimeout(function(){jQuery('#gform_wrapper_3').replaceWith(confirmation_content);jQuery(document).trigger('gform_confirmation_loaded', [3]);window['gf_submitting_3'] = false;}, 50);}else{jQuery('#gform_3').append(contents);if(window['gformRedirect']) {gformRedirect();}}jQuery(document).trigger('gform_post_render', [3, current_page]);} );} );</script>
                    <?php } ?>
                </div>
                <!-- login-form end-->
                <div class="zm-register-form">
                <div class="login-signup">
                    <div class="login-signup-container">
                        <div class="login-left">
                        <h2>Create your account</h2>
                            <div class="signup-benifits">
                            <ul>
                            <li><img src="https://luxurybuystoday-media.s3.amazonaws.com/media/uploads/2018/04/32845-s-1.jpg" alt="" width="150" height="150" class="alignnone size-full wp-image-340807" /> <span>vip special member discount price notices</span></li>
                            <li><img src="https://luxurybuystoday-media.s3.amazonaws.com/media/uploads/2018/04/32845-s-2.jpg" alt="" width="150" height="150" class="alignnone size-full wp-image-340806" /> <span>prized invitations to local sales events</span></li>
                            <li><img src="https://luxurybuystoday-media.s3.amazonaws.com/media/uploads/2018/04/32845-s-3.jpg" alt="" width="150" height="150" class="alignnone size-full wp-image-340805" /> <span>daily brand news, events and promotions</span></li>
                            <li><img src="https://luxurybuystoday-media.s3.amazonaws.com/media/uploads/2018/04/32845-s-4.jpg" alt="" width="150" height="150" class="alignnone size-full wp-image-340804" /> <span>free live concierge services</span></li>
                            </ul>
                        </div>
                        <p style="text-align: center;">Already have an account? <a class="zm-login" href="#">Sign In</a></p>
                    </div>
                    <div class="login-right">
                        <?php echo do_shortcode('[gravityform id="16" ajax="true" title="false" description="false"]');?>
                        <p style="text-align: center;">OR LOG IN WITH</p>
                        <div style="text-align: center;"><?php echo do_shortcode('[oa_social_login]');?></div>
                    </div>
                </div>
            </div>
            <?php if(!is_home()){?>
            <iframe style='display:none;width:0px;height:0px;' src='about:blank' name='gravity_register_frame' id='gravity_register_frame'>This iframe contains the logic required to handle Ajax powered Gravity Forms.</iframe>
                        <script type='text/javascript'>jQuery(document).ready(function($){$('.login_register_pop #gform_16').attr("target","gravity_register_frame");jQuery('#gravity_register_frame').load( function(){var contents = jQuery(this).contents().find('*').html();var is_postback = contents.indexOf('GF_AJAX_POSTBACK') >= 0;if(!is_postback){return;}var form_content = jQuery(this).contents().find('#gform_wrapper_16');var is_confirmation = jQuery(this).contents().find('#gform_confirmation_wrapper_16').length > 0;var is_redirect = contents.indexOf('gformRedirect(){') >= 0;var is_form = form_content.length > 0 && ! is_redirect && ! is_confirmation;if(is_form){jQuery('#gform_wrapper_16').html(form_content.html());if(form_content.hasClass('gform_validation_error')){jQuery('#gform_wrapper_16').addClass('gform_validation_error');} else {jQuery('#gform_wrapper_16').removeClass('gform_validation_error');}setTimeout( function() { /* delay the scroll by 50 milliseconds to fix a bug in chrome */  }, 50 );if(window['gformInitDatepicker']) {gformInitDatepicker();}if(window['gformInitPriceFields']) {gformInitPriceFields();}var current_page = jQuery('#gform_source_page_number_16').val();jQuery(document).trigger('gform_page_loaded', [16, current_page]);window['gf_submitting_16'] = false;}else if(!is_redirect){var confirmation_content = jQuery(this).contents().find('.GF_AJAX_POSTBACK').html();if(!confirmation_content){confirmation_content = contents;}setTimeout(function(){jQuery('#gform_wrapper_16').replaceWith(confirmation_content);jQuery(document).trigger('gform_confirmation_loaded', [16]);window['gf_submitting_16'] = false;}, 50);}else{jQuery('#gform_16').append(contents);if(window['gformRedirect']) {gformRedirect();}}jQuery(document).trigger('gform_post_render', [16, current_page]);} );} );</script>
                <?php } ?>
        </div>
        <?php wp_footer(); ?>
        <script>
            (function($){
                $(function() {
                    $('.login_register').on('click', function (e) {
                        e.preventDefault();
                        $('.login_register_pop').show();
                    });
                    $('.login-close').on('click', function (e) {
                        e.preventDefault();
                        $('.login_register_pop').hide();
                        $('.zm-register-form').hide();
                        $('.zm-login-form').show();
                        $('.login_register_pop').removeClass('wide');
                    });
                    $('.zm-signup').on('click', function (e) {
                        e.preventDefault();
                        $('.login_register_pop').show();
                        $('.zm-login-form').hide();
                        $('.zm-register-form').show();
                        $('.login_register_pop').addClass('wide');
                    });
                    $('.zm-login').on('click', function (e) {
                        e.preventDefault();
                        $('.login_register_pop').show();
                        $('.zm-register-form').hide();
                        $('.zm-login-form').show();
                        $('.login_register_pop').removeClass('wide');
                    });
                })
            })(jQuery)
        </script>
    <?php endif; ?>
</body>
</html>