<?php
// phpcs:disable
/**
 * Plugin Name: LBT Site Restriction
 * Description: Restrict Site for given country codes and ips
 * Author:      Webpigment
 * Author URI:  https://www.webpigment.com
 * Version:     1.0.0
 *
 * @package    LBT_Site_Restriction
 * @author     Webpigment
 * @since      1.0.0
 * @license    GPL-3.0+
 * @copyright  Copyright (c) 2020, Webpigment
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Site Restriction
 */
class LBT_Site_Restriction {
	/**
	 * Instance of the object
	 *
	 * @var \Object
	 */
	private static $instance = null;

	/**
	 * Allowed zip codes
	 *
	 * @var array
	 */
	private $allowed_zip_codes = array(
		75201,
		75202,
		75203,
		75204,
		75205,
		75206,
		75207,
		75208,
		75209,
		75210,
		75211,
		75212,
		75214,
		75215,
		75216,
		75217,
		75218,
		75219,
		75220,
		75221,
		75222,
		75223,
		75224,
		75225,
		75226,
		75227,
		75228,
		75229,
		75230,
		75231,
		75232,
		75233,
		75234,
		75235,
		75236,
		75237,
		75238,
		75239,
		75240,
		75241,
		75242,
		75243,
		75244,
		75245,
		75246,
		75247,
		75248,
		75249,
		75250,
		75251,
		75252,
		75253,
		75258,
		75260,
		75261,
		75262,
		75263,
		75264,
		75265,
		75266,
		75267,
		75270,
		75275,
		75277,
		75283,
		75284,
		75285,
		75286,
		75287,
		75294,
		75295,
		75301,
		75303,
		75310,
		75312,
		75313,
		75315,
		75320,
		75323,
		75326,
		75336,
		75339,
		75342,
		75346,
		75350,
		75353,
		75354,
		75355,
		75356,
		75357,
		75359,
		75360,
		75363,
		75364,
		75367,
		75368,
		75370,
		75371,
		75372,
		75373,
		75374,
		75376,
		75378,
		75379,
		75380,
		75381,
		75382,
		75386,
		75387,
		75388,
		75389,
		75390,
		75391,
		75392,
		75393,
		75394,
		75395,
		75396,
		75397,
		75398,
	);


	/**
	 * Setup singleton instance
	 *
	 * @return  \Object
	 */
	public static function getinstance() {
		if ( is_null( self::$instance ) ) {
			self::$instance = new static();
		}

		return self::$instance;
	}

	/**
	 * Private constructor
	 *
	 * @return  void
	 */
	private function __construct() {
		add_action( 'template_redirect', array( $this, 'restrict_access' ) );

		add_filter( 'theme_page_templates', array( $this, 'register_templates' ), 20, 3 );
		add_filter( 'page_template', array( $this, 'load_templates' ) );
		add_action( 'gform_after_submission_17', array( $this, 'after_newsletter_form_subscription' ), 10, 2 );
		add_action( 'oa_social_login_action_after_user_insert', array( $this, 'create_ontraport_contact_and_tag' ), 10, 2 );

		require_once dirname( __FILE__ ) . '/vendor/autoload.php';
	}

	/**
	 * Register templates
	 *
	 * @param   array $page_template The page templates.
	 * @param   array $wp_theme The theme object.
	 * @param   array $post The post being edited, provided for context, or null.
	 *
	 * @return  array
	 */
	public function register_templates( $page_template, $wp_theme, $post ) {
		$page_template['restricted-access-landing-page.php'] = _x( 'Restricted Access Landing Page', 'LBT Site restriction template', 'lbt' );
		
		return $page_template;
	}

	/**
	 * Load templates
	 *
	 * @param   string $page_template  The page templates.
	 *
	 * @return  string
	 */
	public function load_templates( $page_template ) {
		if ( 'restricted-access-landing-page.php' === get_page_template_slug() ) {
			$page_template = dirname( __FILE__ ) . '/restricted-access-landing-page.php';
		}

		return $page_template;
	}


	/**
	 * Fetch visitor location
	 *
	 * @param string              $type country_code | ip | zip.
	 * @param mixed<string|array> $value State code ( Texas = TX ), IP | ZIP CODE.
	 *
	 * @return  bool If its in the region code or not
	 */
	public function get_user_location( $type = 'country_code', $value = '' ) {
		if ( ! empty( $_SERVER['HTTP_CLIENT_IP'] ) ) {
			$ip = $_SERVER['HTTP_CLIENT_IP'];
		} elseif ( ! empty( $_SERVER['HTTP_X_FORWARDED_FOR'] ) ) {
			$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
		} else {
			$ip = $_SERVER['REMOTE_ADDR'];
		}

		// if ( $ip == '127.0.0.1') {
		//     return true;
		// }

		$user_id = get_current_user_id();

		switch ( $type ) {
			case 'ip':
				if ( $value == $ip ) {
					return true;
				}

				return false;
			case 'country_code':
				$user_location = false;
				if ( $user_id > 0 ) {
					$get_zip = get_user_meta( $user_id, 'zip_code', true );

					if ( ! empty( $get_zip ) ) {
						$geocode = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address=' . esc_attr( $get_zip ) . '&key=AIzaSyDkE0osaaC14WeHZ75j54HnXqDjz9Z5fg');
						$output  = json_decode( $geocode );
						if ( $output->results ) {
							$user_location = $output->results[0]->formatted_address;
						}
					}
				}

				if ( ! $user_location ) {
					$get_ip = explode( ',', $ip );
					if ( ! empty( $get_ip ) ) {
						$ip = $get_ip[0];
					}

					$geocode = @unserialize( file_get_contents( 'http://ip-api.com/php/' . $ip ) );

					if ( 'fail' !== $geocode['status'] ) {
						$user_location = $geocode['regionName'] . ', ' . $geocode['region'] . ', ' . $geocode['countryCode'];

						if ( is_string( $value ) && strpos( $user_location, $value ) !== false ) {
							return true;
						}
					}

					if ( is_array( $value ) ) {
						$allowed = array_filter(
							$value,
							function( $rc ) use ( $user_location ) {
								return strpos( $user_location, $rc ) !== false;
							}
						);

						if ( ! empty( $allowed ) ) {
							return true;
						}
					}
				}

				return false;
			case 'zip':
				$zip = false;

				if ( ! empty( $value ) && is_string( $value ) && in_array( $value, $this->allowed_zip_codes ) ) {
					return true;
				} elseif ( ! empty( $value ) && is_array( $value ) && ! empty( array_intersect( $value, $this->allowed_zip_codes ) ) ) {
					return true;
				} else {
					if ( $user_id > 0 ) {
						$zip = get_user_meta( $user_id, 'zip_code', true );
					}

					if ( ! $zip ) {
						$get_ip = explode( ',', $ip );
						if ( ! empty( $get_ip ) ) {
							$ip = $get_ip[0];
						}

						$geocode = @unserialize( file_get_contents( 'http://ip-api.com/php/' . $ip ) );

						if ( 'fail' !== $geocode['status'] ) {
							$zip = $geocode['zip'];
						}
					}

					if ( ! empty( $zip ) && in_array( $zip, $this->allowed_zip_codes ) ) {
						return true;
					}
				}

				return false;
		}
	}

	/**
	 * Restrict access
	 *
	 * @return  void
	 */
	public function restrict_access() {
		$page_slug = 'coming-soon-2';

		$page = get_page_by_path( $page_slug );

		if ( empty( $page ) ) {
			global $wp_query;
			$wp_query->set_404();
			status_header( 404 );
			return;
		}

		if ( strpos( ( $_SERVER['REQUEST_URI'] ?? '' ), $page_slug ) !== false ) {
			return;
		}

		if ( is_user_logged_in() ) {
			if ( current_user_can( 'admin' ) ) {
				return;
			}

			$user_data = wp_get_current_user();

			if ( strpos( $user_data->user_email, '@hokuapps.com' ) ) {
				return;
			}
		}

		$status = $this->get_user_location( 'zip' );
		
		if ( ! $status ) {
			wp_safe_redirect( '/' . $page_slug );
			exit;
		}
	}

	/**
	 * After newsletter form subscription
	 *
	 * @param   Entry  $entry  The entry.
	 * @param   Form  $form   The Form.
	 *
	 * @return  void          
	 */
	public function after_newsletter_form_subscription( $entry, $form ) {
		$email = rgar( $entry, '2' );

		$params = array(
			'email' => $email
		);

		$this->create_contact_and_tag( $params );
	}

	/**
	 * Create ontraport contact and tag after social network login on subscription
	 *
	 * @param   object  $user_data  
	 * @param   object  $identity   
	 *
	 * @return  void              
	 */
	public function create_ontraport_contact_and_tag( $user_data, $identity ) {
		if ( isset( $_SERVER['REQUEST_URI'] ) && false === strpos( $_SERVER['REQUEST_URI'], 'coming-soon-2' ) ) {
			return;
		}

		$params = array(
			'firstname' => $identity->name->givenName,
			'lastname'  => $identity->name->familyName,
			'email'     => $user_data->data->user_email,
		);

		if ( ! empty( $identity->loginLocation->country->short ) ) {
			$params['country'] = $identity->loginLocation->country->short;
		}

		$this->create_contact_and_tag( $params );

		session_destroy();
	}

	/**
	 * Create contact and tag in ontraport
	 *
	 * @param   array $params  
	 *	
	 * @return  void
	 */
	private function create_contact_and_tag( $params = array() ) {
		$client = new OntraportAPI\Ontraport( "2_137325_S0z9TDx5I","a03Nn6WLAYTugEy" );

		$contact = new OntraportAPI\Contacts( $client );
		$response = $contact->create($params);
		$response = json_decode( $response, true );
		
		if ( 0 !== $response['code'] ) {
			return;
		}

		$contact_id = $response['data']['id'];

		$object = new OntraportAPI\Objects( $client );

		$tag_response = $object->addTag( 
			array (
				'objectID' => 0,
				'add_list' => array( 642, 536 ), // GeoGate and Newsletter tag id.
				'ids' => array( $contact_id ),
			)
		);
	}
}

if ( ! isset( $_GET['bypass-geo-restriction'] ) ) {
	if ( ! isset( $_COOKIE['bypass-geo-restriction'] ) ) {
		LBT_Site_Restriction::getinstance();
	}
} else {
	setcookie( 'bypass-geo-restriction', 1, time() + 60 * 60 * 24 * 30, '/' );
}
