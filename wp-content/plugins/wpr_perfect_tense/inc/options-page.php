<?php
if ( ! defined( 'ABSPATH' ) ) {
	die( 'You are not allowed to call this page directly.' );
}
?>
<div class="wrap">
	<div id="icon-options-drip" class="icon32"><br></div>
	<h2><?php echo __( 'Perfect Tense API Settings' ); ?></h2>

	<form name="perfecttense-settings-form" id="perfecttense-settings-form" method="post" action="options.php">
		<?php
		settings_fields( 'perfecttense_api_options' );
		do_settings_sections( 'wp-perfecttense-api' );
		?>
		<?php submit_button(); ?>
	</form>
</div>
