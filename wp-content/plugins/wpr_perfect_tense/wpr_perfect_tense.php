<?php
/**
 * Plugin Name:  WPRiders Perfect Tense API
 * Plugin URI:   https://www.wpriders.com/hire-us-ref
 * Description:  This use Perfect Tense API
 * Author:       Ovidiu George Irodiu from WPRiders
 * Author URI:   https://www.wpriders.com/hire-us-ref
 * License:      GPL2
 * License URI:  https://www.gnu.org/licenses/gpl-2.0.html
 */

if ( ! defined( 'ABSPATH' ) ) {
	header( 'Status: 403 Forbidden' );
	header( 'HTTP/1.1 403 Forbidden' );
	exit;
}

if ( ! class_exists( 'Wpr_Perfect_Tense' ) ) {
	/**
	 * Class Wpr_Past_Tense
	 */
	class Wpr_Perfect_Tense {
		/**
		 * Past Tense options
		 * @var string
		 */
		var $options_name = 'perfecttense_api_options';
		/**
		 * Options
		 * @var
		 */
		var $options;
		/**
		 * @var string the base url http requests are made to
		 */
		private static $endpoint = 'https://api.perfecttense.com/correct';
		/**
		 * @var null
		 */
		protected static $api_key = null;

		/**
		 * Wpr_Past_Tense constructor.
		 */
		public function __construct() {
			$this->options = get_option( $this->options_name );
			self::$api_key = $this->get_option( 'perfecttense_api_key' );

			add_action( 'admin_menu', array( &$this, 'perfecttense_admin_menu' ) );
			add_action( 'admin_init', array( &$this, 'admin_perfecttense_register_settings' ) );
		}

		/**
		 * Add Perfect Tense settings page
		 */
		public function perfecttense_admin_menu() {
			add_options_page( 'Perfect Tense', 'Perfect Tense', 'manage_options', 'wp-perfecttense-api', array( &$this, 'perfecttense_api_options_page' ) );
		}

		/**
		 * Perfect Tense option page
		 */
		public function perfecttense_api_options_page() {
			if ( ! current_user_can( 'manage_options' ) ) {
				wp_die( esc_html( 'You do not have sufficient permissions to access this page. ' ) );
			}

			include( plugin_dir_path( __FILE__ ) . 'inc/options-page.php' );
		}

		/**
		 * Register all the settings for the options page (Settings API)
		 *
		 * @uses register_setting()
		 * @uses add_settings_section()
		 * @uses add_settings_field()
		 */
		public function admin_perfecttense_register_settings() {
			register_setting( $this->options_name, $this->options_name, array( &$this, 'validate_settings' ) );
			add_settings_section( 'perfecttense_api_settings', 'Perfect Tense account', array( &$this, 'admin_section_code_settings' ), 'wp-perfecttense-api' );

			// Contact Record
			add_settings_field( 'perfecttense_api_key', 'Perfect Tense API Key', array( &$this, 'perfecttense_option_sync_key' ), 'wp-perfecttense-api', 'perfecttense_api_settings' );
		}

		/**
		 * Validates user supplied settings and sanitizes the input
		 *
		 * @param $input
		 *
		 * @return mixed|void
		 */
		public function validate_settings( $input ) {
			$options = $this->options;

			if ( isset( $input['perfecttense_api_key'] ) ) {
				$options['perfecttense_api_key'] = trim( $input['perfecttense_api_key'] );
			}

			return $options;
		}

		/**
		 * Output the description for the Drip token
		 */
		public function admin_section_code_settings() {
			echo '<p>' . esc_html( 'Insert Perfect Tense API Key' ) . '</p>';
		}

		/**
		 * Contact Record
		 * Output the input for the Drip token option
		 */
		public function perfecttense_option_sync_key() {
			echo sprintf( "<input type='text' name='perfecttense_api_options[perfecttense_api_key]' size='20' value='%s' />", esc_attr( $this->get_option( 'perfecttense_api_key' ) ) );
		}

		/**
		 * Lookup an option from the options array
		 *
		 * @param $key
		 *
		 * @return null
		 */
		public function get_option( $key ) {
			if ( isset( $this->options[ $key ] ) && '' != $this->options[ $key ] ) {
				return $this->options[ $key ];
			} else {
				return null;
			}
		}

		static function wpr_send_request( $body ) {
			$response = array();
			$success  = false;

			if ( ! empty( self::$api_key ) ) {
				$arguments = array(
					'method'      => 'POST',
					'httpversion' => '1.0',
					'blocking'    => true,
					'cookies'     => array(),
					'timeout'     => 30,
					'headers'     => array(
						'Authorization' => self::$api_key,
					),
					'body'        => empty( $body ) ? array() : array( 'text' => esc_attr( $body ), 'responseType' => 'corrected' ),
				);

				$raw_response = wp_remote_post( self::$endpoint, $arguments );

				$response = json_decode( wp_remote_retrieve_body( $raw_response ), true );

				if ( is_wp_error( $raw_response ) || ( ! in_array( wp_remote_retrieve_response_code( $raw_response ), array(
						200,
						201,
						204
					) ) )
				) {

				} else {
					$success = $response['status'];
					if ( ! empty( $response ) ) {

						$response = $response['corrected'];

					}
				}

			}

			return array( 'success' => $success, 'response' => $response );
		}
	}
}
$wpr_perfect_tense = new Wpr_Perfect_Tense();
