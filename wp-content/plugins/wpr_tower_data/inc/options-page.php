<?php
if ( ! defined( 'ABSPATH' ) ) {
	die( 'You are not allowed to call this page directly.' );
}
?>
<div class="wrap">
	<div id="icon-options-drip" class="icon32"><br></div>
	<h2><?php echo __( 'Tower Data API Settings' ); ?></h2>

	<form name="tower-settings-form" id="tower-settings-form" method="post" action="options.php">
		<?php
		settings_fields( 'tower_api_options' );
		do_settings_sections( 'wp-tower-api' );
		?>
		<?php submit_button(); ?>
	</form>
</div>
