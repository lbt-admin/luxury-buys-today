(function ($) {
    $('.see_intelligence_button').on('click', function (e) {
        e.preventDefault();
        var fetchfor = $(this).data('fetch');
        wpr_get_intelligence_data(fetchfor);
    });

    function wpr_get_intelligence_data(fetchfor) {
        jQuery.ajax({
            type: 'POST',
            url: wpr_tower_data_ajax_object.wpr_tower_ajax_url,
            data: {
                fetchfor: fetchfor,
                action: 'wpr_fetch_intelligence',
                noncetower: wpr_tower_data_ajax_object.noncetower
            },
            success: function (data) {
                swal({
                    title: 'Confidential',
                    html: data,
                    type: 'warning',
                    customClass: 'swal-wide'
                });
            },
            error: function (request, status, error) {
                swal({
                    title: '<i class="fa fa-file-image-o" aria-hidden="true"></i>',
                    text: wpr_tower_data_ajax_object.wpr_pupup_ajax_error_message,
                    type: 'warning'
                });
            }
        });
    }
})(jQuery);
