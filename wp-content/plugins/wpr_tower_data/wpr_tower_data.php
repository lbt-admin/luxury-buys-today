<?php
/**
 * Plugin Name:  WPRiders Tower Data API
 * Plugin URI:   https://www.wpriders.com/hire-us-ref
 * Description:  This connects to Tower Data API
 * Author:       Ovidiu George Irodiu from WPRiders
 * Author URI:   https://www.wpriders.com/hire-us-ref
 * License:      GPL2
 * License URI:  https://www.gnu.org/licenses/gpl-2.0.html
 */

namespace TowerData;
include( plugin_dir_path( __FILE__ ) . 'inc/TowerDataApi.php' );

if ( ! defined( 'ABSPATH' ) ) {
	header( 'Status: 403 Forbidden' );
	header( 'HTTP/1.1 403 Forbidden' );
	exit;
}

if ( ! class_exists( 'WPR_Tower_Data' ) ) {
	/**
	 * Class WPR_Tower_Data
	 */
	class WPR_Tower_Data {
		/**
		 * @var null
		 */
		protected $demographic_key = null;
		/**
		 * @var null
		 */
		protected $household_key = null;
		/**
		 * @var null
		 */
		protected $interest_key = null;
		/**
		 * Options
		 * @var
		 */
		var $options;
		/**
		 * Tower Data options
		 * @var string
		 */
		var $options_name = 'tower_api_options';

		/**
		 * WPR_Tower_Data constructor.
		 */
		function __construct() {
			$this->options = get_option( $this->options_name, true );

			add_action( 'wp_ajax_wpr_fetch_intelligence', array( &$this, 'do_wpr_fetch_intelligence' ) );
			add_action( 'wp_enqueue_scripts', array( &$this, 'wpr_enqueue_scripts' ) );

			add_action( 'admin_menu', array( &$this, 'tower_admin_menu' ) );
			add_action( 'admin_init', array( &$this, 'admin_register_settings' ) );

			$this->demographic_key = array( 'age', 'gender', 'zip' );
			$this->household_key   = array(
				'education',
				'net_worth',
				'household_income',
				'home_market_value',
				'home_owner_status',
				'length_of_residence',
				'marital_status',
				'occupation',
				'presence_of_children'
			);
			$this->interest_key    = array( 'interests' );

			// Used on new user registration
			add_action( 'gform_user_registered', array( &$this, 'wpr_user_register' ), 50, 4 );
			add_action( 'oa_social_login_action_after_user_insert', array( &$this, 'wpr_social_user_register' ), 10, 2 );
		}

		/**
		 * Add Tower Data settings page
		 */
		function tower_admin_menu() {
			add_options_page( 'Tower Data API', 'Tower Data  API', 'manage_options', 'wp-tower-api', array( &$this, 'tower_api_options_page' ) );
		}

		/**
		 * Option page
		 */
		function tower_api_options_page() {
			if ( ! current_user_can( 'manage_options' ) ) {
				wp_die( esc_html( 'You do not have sufficient permissions to access this page . ' ) );
			}

			include( plugin_dir_path( __FILE__ ) . 'inc/options-page.php' );
		}

		/**
		 * Register all the settings for the options page (Settings API)
		 *
		 * @uses register_setting()
		 * @uses add_settings_section()
		 * @uses add_settings_field()
		 */
		public function admin_register_settings() {
			register_setting( 'tower_api_options', 'tower_api_options', array( &$this, 'validate_settings' ) );
			add_settings_section( 'tower_api_settings', 'Tower Data account', array( &$this, 'admin_section_code_settings' ), 'wp-tower-api' );
			add_settings_field( 'tower_api_key', 'API Key', array( &$this, 'admin_option_sync_key' ), 'wp-tower-api', 'tower_api_settings' );
		}

		/**
		 * Validates user supplied settings and sanitizes the input
		 *
		 * @param $input
		 *
		 * @return mixed|void
		 */
		public function validate_settings( $input ) {
			if ( isset( $input['tower_api_key'] ) ) {
				$options['tower_api_key'] = trim( $input['tower_api_key'] );
			}

			return $options;
		}

		/**
		 * Output the description
		 */
		public function admin_section_code_settings() {
			echo '<p>' . esc_html( 'Insert Tower Data API Key' ) . '</p>';
		}

		/**
		 * Output the input option
		 */
		public function admin_option_sync_key() {
			echo sprintf( "<input type='text' name='tower_api_options[tower_api_key]' size='20' value='%s' />", esc_attr( $this->get_option( 'tower_api_key' ) ) );
		}

		/**
		 * Enqueue scripts
		 */
		function wpr_enqueue_scripts() {
			wp_enqueue_style( 'sweetalert2', plugin_dir_url( __FILE__ ) . 'assets/css/sweetalert2.min.css' );
			wp_enqueue_script( 'sweetalert2', plugin_dir_url( __FILE__ ) . 'assets/js/sweetalert2.min.js', array( 'jquery' ), '1.0.0', true );

			wp_enqueue_style( 'wpr-tower-data', plugin_dir_url( __FILE__ ) . 'assets/css/wpr-tower-data.css' );
			wp_register_script( 'wpr-tower-data-script', plugin_dir_url( __FILE__ ) . 'assets/js/wpr-towerdata.js', array( 'jquery' ), '1.0.0', true );

			$scheme = 'http';
			if ( is_ssl() ) {
				$scheme = 'https';
			}

			$args = array(
				'noncetower'                   => wp_create_nonce( 'wpr-tower-data' ),
				'wpr_tower_ajax_url'           => admin_url( 'admin-ajax.php', $scheme ),
				'wpr_pupup_ajax_error_message' => __( 'Prospect intelligence unavailable or invalid email address.', 'wpr-tower-data' ),
			);
			wp_localize_script( 'wpr-tower-data-script', 'wpr_tower_data_ajax_object', $args );
			wp_enqueue_script( 'wpr-tower-data-script' );
		}

		/**
		 * Fetch intelligence
		 */
		function do_wpr_fetch_intelligence() {
			check_ajax_referer( 'wpr-tower-data', 'noncetower' );

			$person = esc_attr( $_POST['fetchfor'] );

			if ( empty( $person ) ) {
				echo __( 'Prospect intelligence unavailable or invalid email address!' );
				wp_die();
			}

			$api = new TowerDataApi( $this->get_option( 'tower_api_key' ) );

			try {
				$response = $api->query_by_email( $person, $hash_email = false );

				if ( $response ) {
					$demographic_data = $household_data = $interest_data = array();

					foreach ( $response as $key => $value ) {
						if ( in_array( $key, $this->demographic_key ) ) {
							$demographic_data[ $key ] = $value;
						}
						if ( in_array( $key, $this->household_key ) ) {
							$household_data[ $key ] = $value;
						}
						if ( in_array( $key, $this->interest_key ) && is_array( $value ) ) {
							$i = 1;
							foreach ( $value as $type => $bol ) {
								if ( $bol ) {
									$interest_data[ 'interest_' . $i ] = $type;
								}
								$i ++;
							}
						}
					}

					if ( ! empty( $demographic_data ) || ! empty( $household_data ) || ! empty( $interest_data ) ) {
						// Demographic Data
						$this->wpr_display_table_info( $demographic_data, __( 'Demographic Data' ) );
						// Household Data
						$this->wpr_display_table_info( $household_data, __( 'Household Data' ) );
						// Interest & Purchase Data
						$this->wpr_display_table_info( $interest_data, __( 'Interest & Purchase Data' ) );
					} else {
						echo __( 'There is no info available!' );
					}

				}
			} catch ( \Exception $e ) {
				echo 'Caught exception: ' . $e->getMessage() . "\n";
			}

			wp_die();
		}

		/**
		 * On user register
		 *
		 * @param $user_id
		 * @param $config
		 * @param $entry
		 * @param $password
		 */
		function wpr_user_register( $user_id, $config, $entry, $password ) {
			$user_info = get_user_by( 'id', $user_id );

			$user_email = esc_attr( $user_info->user_email );
			if ( empty( $user_email ) ) {
				return;
			}

			$fetch_data = $this->wpr_fetch_email_info( $user_email );
			if ( ! empty( $fetch_data ) ) {
				update_user_meta( $user_id, 'wpr_towerdata_info', $fetch_data );
			}
		}

		/**
		 * On user register
		 *
		 * @param $user_data
		 * @param $identity
		 */
		function wpr_social_user_register( $user_data, $identity ) {
			if ( $user_data ) {
				$user_email = esc_attr( $user_data->user_email );
				if ( empty( $user_email ) ) {
					return;
				}

				$fetch_data = $this->wpr_fetch_email_info( $user_email );
				if ( ! empty( $fetch_data ) ) {
					update_user_meta( absint( $user_data->ID ), 'wpr_towerdata_info', $fetch_data );
				}
			}
		}

		/**
		 * Get user info from TowerData
		 *
		 * @param $email_address
		 *
		 * @return array|mixed|object
		 */
		function wpr_fetch_email_info( $email_address ) {
			if ( empty( $email_address ) ) {
				echo __( 'Prospect intelligence unavailable or invalid email address!' );
				wp_die();
			}

			$api = new TowerDataApi( $this->get_option( 'tower_api_key' ) );

			try {
				$response = $api->query_by_email( $email_address, $hash_email = false );

				if ( $response ) {
					$response_data = $response;
				} else {
					$response_data = array( "error" => "No response" );
				}

			} catch ( \Exception $e ) {
				$response_data = array( "error" => "Caught exception: " . $e->getMessage() . "\n" );
			}

			return $response_data;
		}

		/**
		 * Display the info table
		 *
		 * @param $data
		 * @param string $type
		 */
		function wpr_display_table_info( $data, $type = '' ) {
			if ( ! empty( $data ) ) {

				echo '<table class="wpr-display-tower-data">';
				if ( 'Interest & Purchase Data' == $type ) {

				} else {
					echo sprintf( '<tr><th colspan="2">%s</th></tr>', esc_attr( $type ) );
				}

				foreach ( $data as $key => $val ) {
					echo '<tr>';
					if ( 0 === strpos( $key, 'interest_' ) ) {
						$display_interests = ucfirst( str_replace( '_', ' ', $val ) );
						echo sprintf( '<td>%s</td>', esc_attr( $display_interests ) );
					} else {
						$key = ucfirst( str_replace( '_', ' ', $key ) );
						echo sprintf( '<td>%s</td><td>%s</td>', esc_attr( $key ), esc_attr( $val ) );
					}
					echo '</tr>';
				}
				echo '</table>';
			}
		}

		/**
		 * Lookup an option from the options array
		 *
		 * @param $key
		 *
		 * @return null
		 */
		public function get_option( $key ) {
			if ( isset( $this->options[ $key ] ) && '' != $this->options[ $key ] ) {
				return $this->options[ $key ];
			} else {
				return null;
			}
		}
	}
}
$do_content_to_tower = new WPR_Tower_Data();