<?php

if (!defined('ABSPATH')) {
    header('Status: 403 Forbidden');
    header('HTTP/1.1 403 Forbidden');
    exit;
}




?>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" type='text/css' media='all'>
<script type='text/javascript' src='https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js'></script>

<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" type='text/css' media='all'>
<script type='text/javascript' src='https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js'></script>
<style>
    .panel-heading .nav-link {
        padding-bottom:0px;
        margin-bottom:0px;
    }
    .panel-heading .nav-link:hover {
        border-color:#ffffff;
    }

</style>

<div class="wrap">
    <h2>
        LBT DataHub</h2>


    <div class="panel with-nav-tabs panel-default">
        <div class="panel-heading">
            <ul class="nav nav-tabs">
                <li class="nav-link "><a href="#tab1default" data-toggle="tab" class="nav-link active">Settings</a></li>
                <li class="nav-link"><a href="#tab2default" data-toggle="tab" class="nav-link">Error Logs</a></li>



            </ul>
        </div>
        <div class="panel-body">
            <div class="tab-content">
                <div class="tab-pane  in active" id="tab1default">
                    <form method="post" action="options.php">



                                    <?php
                                    settings_fields( 'datahub_fields' );
                                    do_settings_sections( 'datahub_fields' );
                                    submit_button(); ?>


                    </form>




                </div>


                <div class="tab-pane " id="tab2default">

                    <?php include_once( DATAHUB_PLUGIN_PATH . '/templates/error_logs.php' ); ?>
                </div>

            </div>
        </div>
    </div>









</div>


