<?php

if (!defined('ABSPATH')) {
    header('Status: 403 Forbidden');
    header('HTTP/1.1 403 Forbidden');
    exit;
}
?>
<script>
    jQuery(document).ready(function ($) {
        $('#datahub').DataTable({
            "pageLength": 10,
            "order": [[3, "desc"]],
            "columns": [
                {  width: '200px'},
                {  width: '200px'},
                {  width: '300px'},
               


            ],
        });
    });
</script>
<style>
    table #datahub
    {
        table-layout: fixed;
    }

    table td
    {
        word-wrap:break-word;

        overflow: auto;
    }
</style>
<div class="wrap">
    <h2>DataHub Error Logs</h2>

    <div id="parser_logs">
        <div class="datahub_tab">
            <table id="datahub"
                   class="display wrap">
                <thead>
                <tr>

                    <th>Error Message</th>
                    <th>User Id</th>
                    <th>Date</th>

                </tr>
                </thead>
                <tbody>
                <?php
                $row = 1;
                $filename = get_home_path() . "/logs/" . 'datahub_log.csv';
                if (($handle = fopen($filename, "r")) !== FALSE) {


                    while (($data = fgetcsv($handle, 0, "`")) !== FALSE) {


                        ?>
                        <tr>

                            <td><?php echo$data[0]; ?></td>
                            <td><?php echo $data[1]; ?></td>
                            <td><?php echo $data[2]; ?></td>
                        </tr>
                    <?php }
                } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
