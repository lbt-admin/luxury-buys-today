<?php
/**
 * Plugin Name:  LBT DataHub
 * Plugin URI:   https://www.luxurybuystoday.com
 * Description:  This connects to the XsellCast API
 * Plugin Author: Dennis Peacock (LBT Team)
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
// ini_set("mail.log", "/tmp/mail.log");
// ini_set("mail.add_x_header", TRUE);
define( 'DATAHUB_PLUGIN_PATH', plugin_dir_path( __FILE__ ) );


if ( ! class_exists( 'LBT_datahub' ) ) {

	/**
	 * Class LBT_datahub
	 */
	class LBT_datahub {

		/**
		 * @var null|string
		 */
		private $_client_id = null;
		/**
		 * @var null|string
		 */
		private $_client_secret = null;
		/**
		 * @var null
		 */
		private $oauth_token = null;
		/**
		 * @var string
		 */
		private $endpoint;
		/**
		 * @var string
		 */
		private $abcEmails;
		/**
		 * @var string
		 */
		private $_get_token = '/oauth/request-token';
		/**
		 * @var array
		 */
		private $response_array = array( 200, 201, 204 );


		function __construct() {

			// setup api URL
			$this->endpoint = get_option( 'api_url' ) . '/v1/';
			// setup client keys
			$this->_client_id     = get_option( 'client_id' );
			$this->_client_secret = get_option( 'client_secret' );
			$this->_get_token     = $this->endpoint . $this->_get_token;
			// generate token
			// $this->get_token();

			// setup emails for need an ABC
			$this->abcEmails = get_option( 'abc_emails' );

			// add menu page
			add_action( 'admin_menu', array( $this, 'datahub_settings' ) );
			add_action( 'admin_init', array( $this, 'setup_sections' ) );
			add_action( 'admin_init', array( $this, 'setup_fields' ) );

			// add CTA ajax events
			add_action( 'wp_ajax_save_cta', array( $this, 'save_cta' ) );
			add_action( 'wp_ajax_get_dealership', array( $this, 'get_dealership_data' ) );
			add_filter( 'wp_ajax_nopriv_get_dealership', array( $this, 'get_dealership_data' ) );
			// LBT > DataHub Hooks.
			$this->add_datahub_hooks();
		}


		function datahub_settings() {
			add_menu_page(
				'Settings Admin',
				'Datahub API',
				'manage_options',
				'datahub_api',
				array( $this, 'display_datahub_menu' )
			);

		}

		function display_datahub_menu() {
			include_once DATAHUB_PLUGIN_PATH . '/templates/datahub_options.php';
		}

		function setup_sections() {
			add_settings_section( 'first_section', 'API URL', array( $this, 'section_callback' ), 'datahub_fields' );
			add_settings_section( 'second_section', 'Set Client Keys', array( $this, 'section_callback' ), 'datahub_fields' );
			add_settings_section( 'third_section', 'Set Need ABC Recipient Emails', array( $this, 'section_callback' ), 'datahub_fields' );
		}

		function setup_fields() {
			$fields = array(
				array(
					'uid'     => 'api_url',
					'label'   => 'API URL',
					'section' => 'first_section',

				),
				array(
					'uid'     => 'client_id',
					'label'   => 'Client Id',
					'section' => 'second_section',

				),
				array(
					'uid'     => 'client_secret',
					'label'   => 'Client Secret',
					'section' => 'second_section',
				),
				array(
					'uid'     => 'abc_emails',
					'label'   => 'Set Need ABC Recipient Emails (Seperate with commas)',
					'section' => 'third_section',
				),
			);

			foreach ( $fields as $field ) {
				add_settings_field( $field['uid'], $field['label'], array( $this, 'field_callback' ), 'datahub_fields', $field['section'], $field );
				register_setting( 'datahub_fields', $field['uid'] );
			}

		}

		public function field_callback( $arguments ) {
			$value = get_option( $arguments['uid'] ); // Get the current value, if there is one
			if ( ! $value ) { // If no value exists
				$value = ''; // Set to empty
			}
			printf( '<input name="%1$s" id="%1$s" type="text"  value="%2$s" />', $arguments['uid'], $value );
		}

		public function section_callback( $arguments ) {

		}


		/**
		 * Get access token
		 *
		 * @return null
		 */
		function get_token() {
			$post_data = array(
				'client_id'     => $this->_client_id,
				'client_secret' => $this->_client_secret,
			);

			$arguments = array(
				'method'    => 'POST',
				'timeout'   => 30,
				'sslverify' => false,
				'headers'   => array(
					'Content-Type' => 'application/x-www-form-urlencoded',
				),
				'body'      => empty( $post_data ) ? array() : http_build_query( $post_data ),
			);

			$raw_response = wp_remote_post( $this->_get_token, $arguments );
			$response     = json_decode( wp_remote_retrieve_body( $raw_response ), true );
			if ( ! in_array( wp_remote_retrieve_response_code( $raw_response ), $this->response_array )
			) {
				$response = $response['message'];
			} else {

				$token             = $response['data']['access_token'];
				$this->oauth_token = $token;

			}

			return;
		}

		/**
		 * Create Prospect
		 *
		 * @param array
		 *
		 * @return array|mixed|object
		 */
		function create_prospect( $prospect ) {
			$post_vars = array(
				'first_name'  => $prospect->first_name,
				'last_name'   => $prospect->last_name,
				'email'       => $prospect->user_email,
				'phone_cell'  => preg_replace( '/[^0-9]/', '', $prospect->phone ),
				'avatar'      => '',
				'address_zip' => $prospect->zip_code,
				'f2882' 	  => 183 // Website
			);

			if ( empty( $post_vars['phone_cell'] ) ) {
				$post_vars['phone_cell'] = '0';
			}

			if ( empty( $post_vars['address_zip'] ) ) {
				$post_vars['address_zip'] = '0000';
			}

			if ( $prospect->street ) {
				$post_vars['address_line_one'] = $prospect['street'];
				$post_vars['address_line_two'] = '';
				$post_vars['address_city']     = $prospect['city'];
				$post_vars['address_state']    = $prospect['state'];
				$post_vars['address_country']  = 'United States';
				$post_vars['phone_home']       = '';
				$post_vars['phone_office']     = '';
			}

			try {
				$response = wp_lbt_ontraport_request(
					'Contacts',
					array(
						'body' => $post_vars,
					)
				);

				foreach ( $response as $xsc_key => $xsc_value ) {
					if ( $xsc_key == 'id' ) {
						$xsc_id = $xsc_value;

					}
				}

				add_user_meta( $prospect->id, 'xsc_id', $xsc_id, true );

				return $xsc_id;
			} catch ( \Exception $e ) {
				$error_message = $e->getMessage();
				$logData       = array( "Something went wrong: $error_message", $prospect->user_email );
				$this->datahub_log( $logData );
			}
		}


		/**
		 * Edit Prospect
		 *
		 * @param prospect id
		 * @param data
		 *
		 * @return array|mixed|object
		 */
		function update_prospect( $prospectId, $data ) {

			$prospect = array_map(
				function ( $a ) {
					return $a[0];
				},
				get_user_meta( $prospectId )
			);

			$updateUrl = $this->endpoint . 'prospect/' . $prospect['xsc_id'];

			// build post array
			$post_vars = array(
				'first_name' => $prospect['first_name'],
				'last_name'  => $prospect['last_name'],
				'email'      => $prospect['user_email'],
				'phone_cell' => preg_replace( '/[^0-9]/', '', $prospect['phone'] ),
				'avatar'     => $prospect['wp_user_avatar'],
			);

			if ( $prospect['street'] ) {
				$post_vars[] = array( 'address_line_one' => $prospect['street'] );
				$post_vars[] = array( 'address_line_two' => '' );
				$post_vars[] = array( 'address_city' => $prospect['city'] );
				$post_vars[] = array( 'address_state' => $prospect['state'] );
				$post_vars[] = array( 'address_country' => 'United States' );
				$post_vars[] = array( 'phone_home' => '' );
				$post_vars[] = array( 'phone_office' => '' );
			}

			$headers = array(
				'Accept'        => 'application/json',
				'Authorization' => 'Bearer ' . $this->oauth_token,
			);

			$response = wp_remote_post(
				$updateUrl,
				array(
					'method'      => 'POST',
					'timeout'     => 45,
					'redirection' => 5,
					'httpversion' => '1.0',
					'blocking'    => true,
					'headers'     => $headers,
					'body'        => $post_vars,
				)
			);

			if ( is_wp_error( $response ) ) {
				$error_message = $response->get_error_message();
				$logData       = array( "Something went wrong: $error_message", $prospect['user_email'] );
				$this->datahub_log( $logData );
			} else {

				$xsc_id = json_decode( $response, true );

				return $xsc_id;
			}

		}

		function save_cta() {
			$params = $_POST;

			if ( ! wp_verify_nonce( $params['nonce'], 'wpr-dealer-location' ) ) {
				wp_send_json_error(
					__( 'Invalid nonce' ),
					403
				);
			}

			if ( empty( $params['dealer_id'] ) ) {
				wp_send_json_error( __( 'Missing dealership id' ), 400 );
			}

			$category_id  = absint( $params['category_id'] );

			if ( ! $category_id ) {
				wp_send_json_error( __( 'Missing category id' ), 400 );
			}

			$user_id     = get_user_meta( get_current_user_id(), 'xsc_id', true );
			if ( empty( $user_id ) ) {
				$prospect_id = get_user_meta( get_current_user_id(), 'wpr_ontraport_id', true ) ? get_user_meta( get_current_user_id(), 'wpr_ontraport_id', true ) : '2374';
			} else {
				$prospect_id = $user_id;
			}

			$post_id      = $params['post_id'] ?? 0;
			$current_url  = $params['current_url'] ?? '';

			$todo_type = '';
			switch ( $params['todo'] ) {
				case 'price':
					$todo_type = 'Request for Price';
					break;
				case 'learn':
					$todo_type = 'Request for Info';
					break;
				case 'contact':
					$todo_type = 'Request for Call';
					break;
				default:
					$todo_type = 'Request for Appointment';
			}

			$thumbnail = $post_id ? get_post_meta( $post_id, 'wpr_remote_featured_img', true ) : '';

			if ( $post_id && has_post_thumbnail( $post_id ) ) {
				$thumbnail = get_the_post_thumbnail_url( $post_id );
			}

			$excerpt = 'none';

			if ( $post_id ) {
				$excerpt = get_the_excerpt( $post_id );
				$excerpt = $excerpt == '' ? 'none' : $excerpt;
			}

			$category = get_category( $category_id );

			$brand       = $category->cat_name;
			$category_id = $category->cat_ID;
			$industry    = $category->category_parent;

			$current_user       = wp_get_current_user();
			$current_user_email = $current_user ? $current_user->user_email : false;

			$dealer_email = false;

			if ( $current_user_email ) {
				$email_domain = explode( '@', $current_user_email )[1];
				if ( 'hokuapps.com' === $email_domain ) {
					switch ( $brand ) {
						case 'BMW':
							$dealer_email = 'ed@broyhill.net';
							break;
						case 'Audi':
							$dealer_email = 'careertowntest1@' . $email_domain;
							break;
						case 'Mercedes-Benz':
							$dealer_email = 'alohaankurj@gmail.com';
							break;
						case 'Ferrari':
							$dealer_email = 'oyster93@yahoo.com';
							break;
						case 'Jaguar':
							$dealer_email = 'mitko.kockovski@gmail.com';
							break;
						case 'Lexus':
							$dealer_email = 'adam.l.humburg@gmail.com';
							break;
						case 'Bloomingdales':
							$dealer_email = 'careertowntest3@' . $email_domain;
							break;
						case 'Nordstrom':
							$dealer_email = 'billfutreal@gmail.com';
							break;
						case 'Brooks Brothers':
							$dealer_email = 'hello@ugurcan.me';
							break;
						case 'Ethan Allen':
							$dealer_email = 'msdolphin@lethadavis.com';
							break;
					}
				}
			}

			$dealer_id = $params['dealer_id'];

			if ( $dealer_email ) {
				$dealership = $this->_get_dealership_by( 'email', $dealer_email );
				if ( ! is_wp_error( $dealership ) && false !== $dealership ) {
					$dealer_id = $dealership['id'];
				}
			}

			$temp_match = array();
			$match = $this->get_cta_by_seller_and_prospect_id( $dealer_id, $prospect_id );
			$temp_match[] = $match;

			if ( is_wp_error( $match ) ) {
				$error_message = $match->get_error_message();
				$logData       = array( "Something went wrong: $error_message saving cta" );
				$this->datahub_log( $logData );
				wp_send_json_error( array( 'First Match issue', $error_message, $logData ), $match->get_error_code() );
			}

			try {
				if ( ! $match ) {
					$temp_match[] = 'not a match';
					$match = wp_lbt_ontraport_request(
						'Matches',
						array(
							'body' => array(
								'f2257' => $dealer_id,
								'f2260' => $prospect_id,
							),
						)
					);
				}
				$match_id = $match['id'];

				$activity_params = array(
					'f2761' => $post_id ? get_the_title( $post_id ) : '',
					'f2762' => $match_id,
					'f2763' => $thumbnail ? $thumbnail : '',
					'f2764' => $current_url,
					'f2765' => '6',
					'f2766' => strtotime('now'),
					'f2767' => date( 'H:i:s' ),
					'f2768' => $todo_type,
					'f2800' => true,
				);

				$activity_params = array_filter( $activity_params, 'strlen' );

				$activity = wp_lbt_ontraport_request(
					'Activities',
					array(
						'body' => $activity_params,
					)
				);

				wp_send_json_success(
					array(
						'prospect_id'   => $prospect_id,
						'dealership_id' => $dealer_id,
						'activity'      => $activity['id'],
					)
				);
			} catch ( \Exception $e ) {
				$logData = array( 'Something went wrong while saving cta: ' . $e->getMessage() );
				$this->datahub_log( $logData );
				wp_send_json_error( $e->getMessage() );
			}
		}


		/**
		 * Get dealership by email
		 *
		 * @param   string $email
		 *
		 * @return  array
		 */
		private function _get_dealership_by( $type, $value ) {
			if ( 'email' === $type && ! is_email( $value ) ) {
				error_log( 'Save CTA - Get Dealership email error: Invalid email' );
				return new \WP_Error(
					400,
					__( 'Invalid email' )
				);
			}

			if ( 'email' === $type ) {
				return wp_lbt_ontraport_get_dealership_by_contact_email( $value );
			}

			// This email check will be removed after client approves the update above.
			if ( 'email' === $type ) {
				$field = 'f2737';
				$field = 'f2930';
			} elseif ( 'unique_id' === $type ) {
				$field = 'unique_id';
			} else {
				return new \WP_Error(
					400,
					__( 'Invalid type. Can be "email" or "unique_id' )
				);
			}

			$condition[] = array(
				'field' => array( 'field' => $field ),
				'op'    => '=',
				'value' => array( 'value' => $value ),
			);

			$query_string = 'range=1&condition=' . urlencode( json_encode( $condition ) );

			try {
				$dealership = wp_lbt_ontraport_request(
					'Dealership-ABCs?' . $query_string,
					array(
						'method' => 'GET',
					)
				);

				if ( ! empty( $dealership ) ) {
					return $dealership[0];
				}

				return false;
			} catch ( \Exception $e ) {
				error_log( 'Save CTA - Get Dealership email error: ' . $e->getMessage() );
				return new \WP_Error(
					$e->getCode(),
					$e->getMessage()
				);
			}
		}

		/**
		 * Get dealership
		 *
		 * @param   array  $args  Params for searching dealership by
		 * @param   string $relation  Relation to connect the condition by
		 *
		 * @return  array
		 */
		private function _get_dealership( array $args = array(), $relation = 'AND' ) {
			return wp_lbt_ontraport_get_dealership( $args, $relation );
		}

		/**
		 * Get match by seller and prospect id
		 *
		 * @param   int   $seller_id
		 * @param   int   $prospect_id
		 * @param   array $headers
		 *
		 * @return  mixed
		 */
		public function get_cta_by_seller_and_prospect_id( $seller_id, $prospect_id ) {
			$condition[] = array(
				'field' => array( 'field' => 'f2257' ),
				// 'field' => array( 'field' => 'f2983' ),
				'op'    => '=',
				'value' => array( 'value' => $seller_id ),
			);

			$condition[] = 'AND';

			$condition[] = array(
				'field' => array( 'field' => 'f2260' ),
				'op'    => '=',
				'value' => array( 'value' => $prospect_id ),
			);

			try {
				$match = wp_lbt_ontraport_request(
					'Matches?range=1&condition=' . urlencode( json_encode( $condition ) ),
					array(
						'method' => 'GET',
					)
				);
				if ( is_array( $match[0] ) && sizeof( $match[0] ) > 0 ) {
					return $match[0];
				}

				return false;
			} catch ( \Exception $e ) {
				return new \WP_Error(
					$e->getCode(),
					$e->getMessage()
				);
			}
		}

		public function get_dealership_data() {

			$brand_slug = $_POST['slug'];

			$user_id = get_user_meta( get_current_user_id(), 'xsc_id', true );

			$dealershipUrl = $this->endpoint . 'prospect/' . $user_id . '/dealerships';

			$dealershipUrl .= '?brand_slug=' . $brand_slug;

			if ( ! is_user_logged_in() || ! $user_id ) {
				$dealershipUrl = $this->endpoint . 'prospect/geolocate/dealerships/no_id?brand_slug=' . $brand_slug;
				$user_lat      = false;
				$user_lng      = false;

				if ( ! empty( $_SERVER['HTTP_CLIENT_IP'] ) ) {
					$ip = $_SERVER['HTTP_CLIENT_IP'];
				} elseif ( ! empty( $_SERVER['HTTP_X_FORWARDED_FOR'] ) ) {
					$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
				} else {
					$ip = $_SERVER['REMOTE_ADDR'];
				}

				$get_ip = explode( ',', $ip );
				if ( ! empty( $get_ip ) ) {
					$ip = $get_ip[0];
				}
				if ( $ip == '127.0.0.1' ) {
					$ip = '71.10.113.100';
				}
				$geocode = @unserialize( file_get_contents( 'http://ip-api.com/php/' . $ip ) );
				if ( 'fail' != $geocode['status'] ) {
					$user_lat = $geocode['lat'];
					$user_lng = $geocode['lon'];
				}

				$dealershipUrl .= '&lat=' . $user_lat;
				$dealershipUrl .= '&lng=' . $user_lng;

			}

			$args = array(
				'sslverify' => false,

			);

			$response = wp_remote_get( $dealershipUrl, $args );

			if ( ! is_wp_error( $response ) ) {
				$xsc_response = json_decode( $response['body'], true );
				// print_r($xsc_response);
				echo $response['body'];
				die;
			} else {
				echo $response->get_error_message();
				die;
			}

		}


		// hook into the events we need to fire api calls
		function add_datahub_hooks() {
			// Prospect registers via the form.
			add_action(
				'gform_user_registered',
				function ( $user_id, $feed, $entry, $user_pass ) {
					$this->create_prospect( get_userdata( $user_id ) );
				},
				10,
				4
			);

			// Prospect registers via social network and finishes the complete your data form.
			add_action(
				'gform_after_submission_8',
				function ( $entry, $form ) {

					$user           = get_userdata( get_current_user_id() );
					$user->phone    = $entry[1];
					$user->zip_code = $entry['2.5'];

					update_user_meta( get_current_user_id(), 'zip_code', $user->zip_code );
					update_user_meta( get_current_user_id(), 'phone', $user->phone );
					$this->create_prospect( $user );
				},
				10,
				2
			);

			// Prospect edits profile info.
			add_action(
				'wpr_profile_updated',
				function ( $user_id, $data ) {
					if ( empty( $data['profile'] ) ) {
						return;
					}

					$this->update_prospect( $user_id, $data['profile'] );
				},
				10,
				2
			);

			// Prospect updates avatar.
			add_action(
				'update_user_meta',
				function ( $meta_id, $user_id, $meta_key, $_meta_value ) {
					if ( 'wp_user_avatar' === $meta_key && isset( $_meta_value[0]['avatar_url'] ) ) {
						$this->update_prospect( $user_id, $_meta_value[0] );
					}
				},
				10,
				4
			);

		}


		function datahub_log( $data ) {
			if ( ! is_dir( WP_CONTENT_DIR . '/logs/' ) ) {
				// Directory does not exist, so lets create it.
				mkdir( WP_CONTENT_DIR . '/logs/', 0755 );
			}
			$filename = WP_CONTENT_DIR . '/logs/' . 'datahub_log.csv';

			if ( ! file_exists( $filename ) ) {
				$fp = fopen( $filename, 'w' );
			} else {

				$fp = fopen( $filename, 'a' );
			}

			$data[] = date( 'Y-m-d H:i:s' );
			fputcsv( $fp, $data, '`' );

			fclose( $fp );
			return;
		}


		// end class
	}
	// end if class exists
}

new LBT_datahub();
