<?php
/**
 * Plugin Name: WPRiders Infinite Scroll Masonry
 * Plugin URI: http://www.wpriders.com
 * Description: This plugin creates Infinite Scroll Masonry
 * Version: 1.0.0
 * Author: Ovidiu George Irodiu from Wpriders
 * Author URI: http://www.wpriders.com
 * License: GPL2
 */
if ( ! defined( 'ABSPATH' ) ) {
	die( 'You are not allowed to call this page directly.' );
}

if ( ! class_exists( 'WPR_infinite_scroll' ) ) {
	/**
	 * Class WPR_infinite_scroll
	 */
	class WPR_infinite_scroll {
		/**
		 * Holds the values to be used in the fields callbacks
		 */
		private $options;

		/**
		 * WPR_infinite_scroll constructor.
		 */
		function __construct() {
			add_action( 'admin_menu', array( &$this, 'options_menu_init' ) );
			add_action( 'admin_init', array( &$this, 'page_init' ) );
			add_action( 'wp_enqueue_scripts', array( &$this, 'wpr_enqueue_scripts' ) );
			add_action( 'wp_footer', array( &$this, 'wpr_footer_load_script' ) );
		}

		/**
		 * Enqueue scripts
		 */
		function wpr_enqueue_scripts() {
			wp_enqueue_style( 'wpr-infinite-scroll-style', plugin_dir_url( __FILE__ ) . 'css/wpr-mis.css', array(), time() );
			wp_enqueue_script( 'wpr-infinite-scroll-script', plugin_dir_url( __FILE__ ) . 'js/jquery.infinitescroll.min.js', array( 'jquery' ), '2.1.0', true );
		}

		/**
		 * Register our menu with WordPress
		 */
		function options_menu_init() {
			add_options_page(
				__( 'Infinite Scroll', 'wpr-infinite-scroll' ),
				__( 'Infinite Scroll', 'wpr-infinite-scroll' ),
				'manage_options',
				'infinite_scroll_options', array( &$this, 'wpr_infinite_scroll_options' )
			);
		}

		/**
		 * top level menu:
		 * callback functions
		 */
		function wpr_infinite_scroll_options() {
			if ( ! current_user_can( 'manage_options' ) ) {
				return;
			}

			$this->options = get_option( 'wpr_infinite_scroll_settings' );
			?>
            <div class="wrap">
                <h1><?= esc_html( get_admin_page_title() ); ?></h1>
                <form action="options.php" method="post">
					<?php
					settings_fields( 'wpr_infinite_scroll_set' );
					do_settings_sections( 'infinite_scroll_options' );
					submit_button( 'Save Settings' );
					?>
                </form>
            </div>
			<?php
		}

		/**
		 * Register and add settings
		 */
		public function page_init() {
			register_setting(
				'wpr_infinite_scroll_set',
				'wpr_infinite_scroll_settings',
				array( $this, 'sanitize' )
			);

			add_settings_section(
				'masonry_infinite_scroll',
				'Masonry Infinite Scroll Settings',
				array( $this, 'print_section_info' ),
				'infinite_scroll_options'
			);

			add_settings_field(
				'wpr_masonry_nav_selector',
				'Nav container',
				array( $this, 'wpr_masonry_nav_selector_callback' ),
				'infinite_scroll_options',
				'masonry_infinite_scroll'
			);

			add_settings_field(
				'wpr_masonry_nav_next_selector',
				'Nav next page',
				array( $this, 'wpr_masonry_nav_next_selector_callback' ),
				'infinite_scroll_options',
				'masonry_infinite_scroll'
			);

			add_settings_field(
				'wpr_masonry_item_selector',
				'Post container',
				array( $this, 'wpr_masonry_item_selector_callback' ),
				'infinite_scroll_options',
				'masonry_infinite_scroll'
			);

			add_settings_field(
				'wpr_masonry_container',
				'Masonry container',
				array( $this, 'wpr_masonry_container_callback' ),
				'infinite_scroll_options',
				'masonry_infinite_scroll'
			);
		}

		/**
		 * Sanitize each setting field as needed
		 *
		 * @param array $input Contains all settings fields as array keys
		 */
		public function sanitize( $input ) {
			$new_input = array();
			if ( isset( $input['wpr_masonry_nav_selector'] ) ) {
				$new_input['wpr_masonry_nav_selector'] = sanitize_text_field( $input['wpr_masonry_nav_selector'] );
			}

			if ( isset( $input['wpr_masonry_nav_next_selector'] ) ) {
				$new_input['wpr_masonry_nav_next_selector'] = sanitize_text_field( $input['wpr_masonry_nav_next_selector'] );
			}

			if ( isset( $input['wpr_masonry_item_selector'] ) ) {
				$new_input['wpr_masonry_item_selector'] = sanitize_text_field( $input['wpr_masonry_item_selector'] );
			}

			if ( isset( $input['wpr_masonry_container'] ) ) {
				$new_input['wpr_masonry_container'] = sanitize_text_field( $input['wpr_masonry_container'] );
			}

			return $new_input;
		}

		/**
		 * Print the Section text
		 */
		public function print_section_info() {
			echo 'Enter your settings below:';
		}

		/**
		 * Get the settings option array and print one of its values
		 */
		public function wpr_masonry_nav_selector_callback() {
			printf(
				'<input type="text" id="wpr_masonry_nav_selector" name="wpr_infinite_scroll_settings[wpr_masonry_nav_selector]" value="%s" />',
				isset( $this->options['wpr_masonry_nav_selector'] ) ? esc_attr( $this->options['wpr_masonry_nav_selector'] ) : ''
			);
		}

		/**
		 * Get the settings option array and print one of its values
		 */
		public function wpr_masonry_nav_next_selector_callback() {
			printf(
				'<input type="text" id="wpr_masonry_nav_next_selector" name="wpr_infinite_scroll_settings[wpr_masonry_nav_next_selector]" value="%s" />',
				isset( $this->options['wpr_masonry_nav_next_selector'] ) ? esc_attr( $this->options['wpr_masonry_nav_next_selector'] ) : ''
			);
		}

		/**
		 * Get the settings option array and print one of its values
		 */
		public function wpr_masonry_item_selector_callback() {
			printf(
				'<input type="text" id="wpr_masonry_item_selector" name="wpr_infinite_scroll_settings[wpr_masonry_item_selector]" value="%s" />',
				isset( $this->options['wpr_masonry_item_selector'] ) ? esc_attr( $this->options['wpr_masonry_item_selector'] ) : ''
			);
		}

		/**
		 * Get the settings option array and print one of its values
		 */
		public function wpr_masonry_container_callback() {
			printf(
				'<input type="text" id="wpr_masonry_container" name="wpr_infinite_scroll_settings[wpr_masonry_container]" value="%s" />',
				isset( $this->options['wpr_masonry_container'] ) ? esc_attr( $this->options['wpr_masonry_container'] ) : ''
			);
		}

		/**
		 * Load script in footer
		 */
		function wpr_footer_load_script() {
			$wpr_mis_options = get_option( 'wpr_infinite_scroll_settings' );

			$page_id = get_the_ID();

			if ( ! empty( $wpr_mis_options ) && ( is_category() || is_archive() || $page_id == 11511 || $page_id == 23472 ) ) {
				?>
                <script type="text/javascript">
                    jQuery(document).ready(function() {
					//jQuery(window).on('load', function($){
                        jQuery(function ($) {
                            // define content container
                            var $container = $('<?php echo $wpr_mis_options['wpr_masonry_container'];?>');

                            <?php
                            if ( !empty($wpr_mis_options['wpr_masonry_container']) ) { ?>
                            $($container).after('<div id="wpr-loading-animation"></div>');
                            <?php } ?>

                            // apply masonry to loaded elements
                            $container.imagesLoaded(function () {
                                setTimeout(function () {
                                $container.masonry({
                                    itemSelector: '<?php echo $wpr_mis_options['wpr_masonry_item_selector'];?>',
                                    isAnimated: true,
                                });
                                }, 3000);

                            });

                            // apply infinite scrolling
                            // $container.infinitescroll({
                            //         navSelector: '<?php echo $wpr_mis_options['wpr_masonry_nav_selector'];?>',
                            //         nextSelector: '<?php echo $wpr_mis_options['wpr_masonry_nav_next_selector'];?>',
                            //         itemSelector: '<?php echo $wpr_mis_options['wpr_masonry_item_selector'];?>',
                            //         extraScrollPx: 0,
                            //         prefill: true,
                            //         loading: {
                            //             finishedMsg: '<?php esc_html_e('No more posts to load.', 'wpr-masonry-infinite-scroll');?>',
                            //             msgText: '<?php esc_html_e('Loading more posts...', 'wpr-masonry-infinite-scroll');?>',
                            //             img: '<?php echo plugins_url('/images/loading.gif', __FILE__); ?>',
                            //             selector: '#wpr-loading-animation'
                            //         }
                            //     },
                            //     // trigger Masonry and google analytics in the callback
                            //     function (newElements, opts) {
                            //         if ($('#remove-cpt-from-list').length) {
                            //             var $post_id = $('#remove-cpt-from-list').data("id");
                            //             var index_nr;
                            //             $.each(newElements, function (key, value) {
                            //                 if ($post_id == value.id) {
                            //                     index_nr = key;
                            //                 }
                            //             });
                            //             if (index_nr) {
                            //                 newElements.splice(index_nr, 1);
                            //             }
                            //         }
                            //         // track google page view
                            //         var page = opts.state.currPage;
                            //         try {
                            //             ga('send', 'pageview', '/page/' + page);
                            //         } catch (err) {
                            //         }

                            //         // masonry elements that have just loaded onto the page
                            //         var $newElems = $(newElements).css({opacity: 0});
                            //         var $newElemsIDs = $newElems.map(function () {
                            //             return this.id;
                            //         }).get();

                            //         $(newElements).each(function () {
                            //             var $this = $(this);

                            //             $this.removeClass('wpr-display-shadow daily-promotion');
                            //             $this.find('h2.main-title').remove();
                            //         });

                            //         $container.masonry('appended', $newElems, true);
                            //         $newElems.animate({opacity: 1});

                            //         /*$newElems.imagesLoaded(function () {
                            //             $container.masonry('appended', $newElems, true);
                            //             setTimeout(function () {
                            //                 $newElems.animate({opacity: 1});
                            //             }, 250);
                            //             // apply code here to do stuff after images loaded
                            //         });*/
                            //     });

                        });
                    });

                </script>
				<?php

			}
		}
	}
}
$wpr_cpt_scroll = new WPR_infinite_scroll();
