<?php
namespace LBT\FeedsSiteAPI;

/**
 * Endpoints that will accept requests coming from Data Hub and Mobile
 */
class Site_Endpoints {
	private static $instance = null;

	private $category_mapper = array(
		8  => 3,
		30 => 4,
		9  => 6,
		28 => 12,
		5  => 21,
		33 => 24,
		10 => 14,
		18 => 16,
		75 => 45,
		36 => 46,
		37 => 47,
		38 => 48,
		39 => 49,
		76 => 50,
		77 => 51,
		13 => 52,
		19 => 53,
		20 => 54,
		21 => 55,
		15 => 56,
		22 => 57,
		23 => 58,
		14 => 59,
		6  => 60,
		24 => 61,
		17 => 62,
		25 => 63,
		26 => 64,
	);

	public static function get_instance() {
		if ( is_null( self::$instance ) ) {
			self::$instance = new static();
		}

		return self::$instance;
	}

	private function __construct() {
		add_action(
			'rest_api_init',
			function() {
				$this->_load_routes();
			}
		);
	}

	/**
	 * Load routes
	 *
	 * Format:  endpoint@callback => http_method
	 *
	 * @return  void
	 */
	private function _load_routes() {
		$routes = array(
			'offer@save_offer' => 'post',
		);

		$this->_register_endpoints( $routes );
	}

	/**
	 * Register Site Endpoints
	 *
	 * @return  void
	 */
	private function _register_endpoints( $routes ) {
		if ( ! is_array( $routes ) ) {
			error_log( 'Site Endpoints Error: Routes must be an array.' );
			return;
		}

		foreach ( $routes as $action => $http_method ) {
			$action_params = explode( '@', $action );
			$endpoint      = $action_params[0];
			$method        = $action_params[1];

			register_rest_route(
				'data-hub/v1',
				'/' . $endpoint,
				array(
					'methods'  => $http_method,
					'callback' => array( $this, $method ),
				)
			);
		}
	}


	/**
	 * Save offer
	 *
	 * @param   \WP_REST_Request  $request
	 *
	 * @return  JSON
	 */
	public function save_offer( \WP_REST_Request $request ) {
		$data = $request->get_params();

		if ( empty( $data['title'] ) ) {
			wp_send_json_error(
				__( 'Required fields missing' ),
				400
			);
		}

		$start_date  = ! empty( $data['start_date'] ) ? date( 'Y-m-d H:i:s', strtotime( sanitize_text_field( $data['start_date'] ) ) ) : date( 'Y-m-d H:i:s' );
		$description = ! empty( $data['description'] ) ? sanitize_text_field( $data['description'] ) : '';
		$author      = ! empty( $data['rep_email'] ) ? $this->_get_user_id_by_email( $data['rep_email'] ) : '';

		$post_data = array(
			'post_title'   => sanitize_text_field( $data['title'] ),
			'post_date'    => $start_date,
			'post_content' => $description,
			'post_author'  => $author,
			'post_status'  => 'publish',
		);

		if ( ! isset( $data['offer_id'] ) || empty( $data['offer_id'] ) ) {
			$post_id  = wp_insert_post( $post_data );
			$offer_id = md5( get_permalink( $post_id ) );
			update_post_meta( $post_id, 'offer_id', $offer_id );
		} else {
			$offer_id = $data['offer_id'];

			$offer_post = new \WP_Query(
				array(
					'post_type'      => 'post',
					'posts_per_page' => 1,
					'fields'         => 'ids',
					'meta_query'     => array(
						array(
							'key'   => 'offer_id',
							'value' => $offer_id,
						),
					),
				)
			);

			if ( $offer_post->have_posts() ) {
				$post_id         = $offer_post->posts[0];
				$post_data['ID'] = $post_id;
				wp_update_post( $post_data );
			} else {
				$post_id  = wp_insert_post( $post_data );
				$offer_id = md5( get_permalink( $post_id ) );
				update_post_meta( $post_id, 'offer_id', $offer_id );
			}
		}

		if ( ! empty( $data['brand_id'] ) && absint( $data['brand_id'] ) > 0 && ! empty( $data['cat_id'] ) && absint( $data['cat_id'] ) > 0 && isset( $this->category_mapper[ $data['cat_id'] ] ) && isset( $this->category_mapper[ $data['brand_id'] ] ) ) {
			wp_set_post_categories( $post_id, array( (int) $this->category_mapper[ $data['brand_id'] ], (int) $this->category_mapper[ $data['cat_id'] ] ) );
		}

		$first_image = 1;

		foreach ( $data['images'] as $image ) {
			$attachment_id = $this->_get_attachment_id( $image, $post_id );

			if ( $first_image ) {
				set_post_thumbnail( $post_id, $attachment_id );
				$first_image = 0;
			}
		}

		$dealer_id = $data['rep_id'] ?? 0;

		foreach ( $data as $key => $value ) {
			update_post_meta( $post_id, '_hub_' . sanitize_title( $key ), sanitize_text_field( $value ) );
		}

		wp_set_object_terms( $post_id, 'dealership-' . $dealer_id, 'post_tag', true );

		return wp_send_json_success( array( 'offer_id' => $offer_id ) );
	}

	/**
	 * Get attachment id by image url
	 *
	 * @param   string  $image_url
	 * @param   int  $post_id
	 *
	 * @return  int
	 */
	private function _get_attachment_id( $image_url, $post_id = 0 ) {
		$image_url = esc_url_raw( $image_url );

		global $wpdb;
		$query = $wpdb->prepare( "SELECT post_id FROM {$wpdb->postmeta} WHERE meta_key= 'image_url' and meta_value = %s", $image_url );

		$attach_id = $wpdb->get_var( $query );

		if ( $attach_id ) {
			return $attach_id;
		}

		$upload_dir = wp_upload_dir();

		$image_data = file_get_contents( $image_url );

		$filename = basename( $image_url );

		if ( wp_mkdir_p( $upload_dir['path'] ) ) {
			$file = $upload_dir['path'] . '/' . $filename;
		} else {
			$file = $upload_dir['basedir'] . '/' . $filename;
		}

		file_put_contents( $file, $image_data );

		$wp_filetype = wp_check_filetype( $filename, null );

		$attachment = array(
			'post_mime_type' => $wp_filetype['type'],
			'post_title'     => sanitize_file_name( $filename ),
			'post_content'   => '',
			'post_parent'    => $post_id,
			'post_status'    => 'inherit',
		);

		$attach_id = wp_insert_attachment( $attachment, $file );
		require_once ABSPATH . 'wp-admin/includes/image.php';
		$attach_data = wp_generate_attachment_metadata( $attach_id, $file );
		wp_update_attachment_metadata( $attach_id, $attach_data );
		update_post_meta( $attach_id, 'image_url', $image_url );
		return $attach_id;
	}
}
