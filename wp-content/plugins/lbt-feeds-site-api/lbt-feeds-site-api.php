<?php
/**
 * Plugin Name:       Luxury Buys Today - Feeds Site API
 * Description:       Provides methods for connecting to the site Luxury Buys Today API and sending / fetching data.
 * Version:           1.0.0
 * Author:            Webpigment
 */

if ( ! defined( 'ABSPATH' ) ) {
	die();
}

/**
 * Autoloader for LBT Site API
 *
 * @param   string  $class
 *
 * @return  boolean
 */
function lbt_feeds_site_api_autoloader( $class ) {
	if ( false === strpos( $class, 'LBT\\FeedsSiteAPI\\' ) ) {
		return;
	}

	$class = strtolower( str_replace( 'LBT\\FeedsSiteAPI', '', $class ) );
	$dir   = '/inc';

	$filename = dirname( __FILE__ ) . $dir . str_replace( '_', '-', str_replace( '\\', '/class-', $class ) ) . '.php';

	if ( file_exists( $filename ) ) {
		require_once $filename;

		if ( class_exists( $class ) ) {
			return true;
		}
	}

	return false;
}
spl_autoload_register( 'lbt_feeds_site_api_autoloader' );

final class LBT_Feeds_Site_API {
	private static $instance = null;
	private $_token          = null;

	public static function get_instance() {
		if ( is_null( self::$instance ) ) {
			self::$instance = new static();
		}

		return self::$instance;
	}

	private function __construct() {
		// Endpoints that receive requests
		LBT\FeedsSiteAPI\Site_Endpoints::get_instance();
	}
}

LBT_Feeds_Site_API::get_instance();
