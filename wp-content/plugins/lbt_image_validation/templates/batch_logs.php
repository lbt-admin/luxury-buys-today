<?php

if (!defined('ABSPATH')) {
    header('Status: 403 Forbidden');
    header('HTTP/1.1 403 Forbidden');
    exit;
}
?>
<script>
    jQuery(document).ready(function ($) {
        $('#batch_table').DataTable({
            "pageLength": 10,
            "order": [[5, "desc"]],
            "columns": [
                {width: '200px'},
                {width: '200px'},
                {width: '200px'},
                {width: '200px'},
                {width: '200px'},
                {width: '200px'},
                {width: '200px'},
                {width: '200px'},


            ],
        });
    });
</script>
<style>
    table #batch_table {
        table-layout: fixed;
    }

    table td {
        word-wrap: break-word;
        text-align: center;
        overflow: auto;
    }
</style>
<div class="wrap">
    <h2>
        Image
        Validation
        Batch
        Logs</h2>

    <div id="parser_logs">
        <div class="batch_table_tab">

            <?php

            $term = 'image_batch_log';
            $files = scan_dir($path, $term);

            $outputArray = [];
            foreach ($files as $file) {
                $dataArray = [];
                $filename = $path . "/" . $file;
                if (($handle = fopen($filename, "r")) !== FALSE) {

                    $dataArray['processed'] = 0;
                    $dataArray['error'] = 0;
                    $dataArray['previous'] = 0;
                    while (($data = fgetcsv($handle, 0, "`")) !== FALSE) {
                        $dataArray['start'] = $data[0];
                        $dataArray['end'] = $data[1];
                        $dataArray['processed'] += $data[2];
                        $dataArray['error'] += $data[3];
                        $dataArray['previous'] += $data[4];
                        $dataArray['ran'] = $data[5];
                    }
                    $batch_id = explode("_", $file)[0];
                    $dataArray['batch']=$batch_id;
                    if (file_exists($path . "/" . $batch_id . "_running_batch.csv")) {
                        $dataArray['status'] = 'Running';
                    } else {
                        $dataArray['status'] = 'Complete';
                    }

                    $outputArray[] = $dataArray;

                }
            }
            ?>
            <table id="batch_table"
                   class="display wrap dataTable">
                <thead>
                <tr>

                    <th>
                        Start
                        Date
                    </th>
                    <th>
                        End
                        Date
                    </th>
                    <th>
                        Posts
                        Processed
                    </th>
                    <th>
                        Error
                        Count
                    </th>
                    <th>
                        Previously
                        Processed
                    </th>
                    <th>
                        Batch
                        Ran
                        On
                    </th>
                    <th>
                        Batch
                        Status
                    </th>
                    <th>Batch ID</th>
                </tr>

                </thead>
                <tbody>
                <?php

                foreach ($outputArray as $output) {


                    ?>
                    <tr>

                        <td><?php echo $output['start']; ?></td>
                        <td><?php echo $output['end']; ?></td>
                        <td><?php echo $output['processed']; ?></td>
                        <td><?php echo $output['error']; ?></td>
                        <td><?php echo $output['previous']; ?></td>
                        <td><?php echo $output['ran']; ?></td>
                        <td><?php echo $output['status']; ?></td>
                        <td><?php echo $output['batch']; ?></td>
                    </tr>
                <?php }
                ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
