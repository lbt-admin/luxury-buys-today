<?php

if (!defined('ABSPATH')) {
    header('Status: 403 Forbidden');
    header('HTTP/1.1 403 Forbidden');
    exit;
}
?>
<script>
    jQuery(document).ready(function ($) {
        $('#log-table').DataTable({
            "pageLength": 10,
            "order": [[3, "desc"]],
            "columns": [
                {  width: '600px'},
                {  width: '200px'},
                {  width: '300px'},
                {  width: '300px'},


            ],
        });
    });
</script>
<style>
    table #log-table
    {
        table-layout: fixed;
    }

    table td
    {
        word-wrap:break-word;

        overflow: auto;
    }
</style>
<div class="wrap">
    <h2>Image Validation Error Logs</h2>

    <div id="parser_logs">
        <div class="log-table_tab">

            <?php

            $term = 'image_log';
            $files = scan_dir($path, $term);

            $outputArray = [];
            foreach ($files as $file) {
                $dataArray = [];
                $filename = $path . "/" . $file;
                if (($handle = fopen($filename, "r")) !== FALSE) {


                    while (($data = fgetcsv($handle, 0, "`")) !== FALSE) {
                        $dataArray['url'] = $data[0];
                        $dataArray['link'] = $data[1];
                        $dataArray['date'] = $data[2];


                        $batch_id = explode("_", $file)[0];
                        $dataArray['batch']=$batch_id;


                        $outputArray[] = $dataArray;

                    }






                }
            }
            ?>

            <table id="log-table"
                   class="display wrap">
                <thead>
                <tr>

                    <th>Image Url</th>
                    <th>Post Link</th>
                    <th>Date</th>
                    <th>Batch ID</th>

                </tr>
                </thead>
                <tbody>
                <?php
                foreach ($outputArray as $output) {


                        ?>
                        <tr>

                            <td><?php echo $output['url']; ?></td>
                            <td><a href="<?php echo get_permalink($output['link']); ?>">View Post</a></td>
                            <td><?php echo $output['date']; ?></td>
                            <td><?php echo $output['batch']; ?></td>
                        </tr>
                    <?php
                } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
