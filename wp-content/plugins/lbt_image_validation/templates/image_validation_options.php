<?php

if (!defined('ABSPATH')) {
    header('Status: 403 Forbidden');
    header('HTTP/1.1 403 Forbidden');
    exit;
}
$path = WP_CONTENT_DIR . "/logs/image-validation";
function scan_dir($dir,$term)
{
    $ignored = array('.', '..', '.svn', '.htaccess');

    $files = array();
    foreach (scandir($dir) as $file) {
        if (in_array($file, $ignored)) continue;
        $files[$file] = filemtime($dir . '/' . $file);
    }

    arsort($files);
    $files = array_keys($files);


    arsort($files, SORT_NUMERIC);
    $ser = function ($val) use ($term) {
        return (stripos($val, $term) !== false ? true : false);
    };
    $files = array_filter($files, $ser);


    return ($files) ? $files : false;
}

?>
<link rel="stylesheet"
      href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
      type='text/css'
      media='all'>
<script type='text/javascript'
        src='https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js'></script>

<link rel="stylesheet"
      href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css"
      type='text/css'
      media='all'>
<script type='text/javascript'
        src='https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js'></script>
<style>
    .panel-heading .nav-link {
        padding-bottom: 0px;
        margin-bottom: 0px;
    }

    .panel-heading .nav-link:hover {
        border-color: #ffffff;
    }

</style>

<div class="wrap">
    <h2>
        LBT
        Image
        Validation</h2>


    <div class="panel with-nav-tabs panel-default">
        <div class="panel-heading">
            <ul class="nav nav-tabs">
                <li class="nav-link ">
                    <a href="#tab1default"
                       data-toggle="tab"
                       class="nav-link active">Run
                        Validator</a>
                </li>
                <li class="nav-link">
                    <a href="#tab2default"
                       data-toggle="tab"
                       class="nav-link">Running Batches</a>
                </li>
                <li class="nav-link">
                    <a href="#tab3default"
                       data-toggle="tab"
                       class="nav-link">Error
                        Logs</a>
                </li>
                <li class="nav-link">
                    <a href="#tab4default"
                       data-toggle="tab"
                       class="nav-link">Batch
                        Logs</a>
                </li>


            </ul>
        </div>
        <div class="panel-body">
            <div class="tab-content">
                <div class="tab-pane  in active"
                     id="tab1default">

                    <h4>
                        Run
                        Image
                        Validator</h4>

                    <form method="post"
                          action="">
                        <div class="form-group">
                            <label for="startInput">Start
                                Date</label>
                            <input id="startInput"
                                   name="start_date"
                                   type="date">
                        </div>
                        <div class="form-group">
                            <label for="endInput">Start
                                Date</label>
                            <input id="endInput"
                                   name="end_date"
                                   type="date">
                        </div>

                    </form>
                    <p>
                        <button class="button button-primary"
                                id="imagesButton">
                            Validate
                            Images
                        </button>
                    </p>


                    <code class="validateBox"
                          style="display:none;"></code>


                </div>
                <div class="tab-pane "
                     id="tab2default">

                    <?php include_once(IMAGE_VALIDATION_PLUGIN_PATH . '/templates/running_logs.php'); ?>
                </div>

                <div class="tab-pane "
                     id="tab3default">

                    <?php include_once(IMAGE_VALIDATION_PLUGIN_PATH . '/templates/error_logs.php'); ?>
                </div>
                <div class="tab-pane "
                     id="tab4default">

                    <?php include_once(IMAGE_VALIDATION_PLUGIN_PATH . '/templates/batch_logs.php'); ?>
                </div>

            </div>
        </div>
    </div>


</div>


<script type="application/javascript">

    jQuery('#imagesButton').on('click', function (event) {
        jQuery('#imagesButton').append('<i class="fa fa-spin fa-spinner"></i>');
        jQuery('.validateBox').hide();
        runJob();
    });


    function runJob() {

        start_date=jQuery('#startInput').val();
        end_date=jQuery('#endInput').val();

        jQuery.ajax({
            type: 'POST',
            url: "<?php echo admin_url('admin-ajax.php'); ?>",
            data: {
                action: 'validation_get_posts',
                start_date: start_date,
                end_date: end_date
            },
            success: function (data) {
                jQuery('#imagesButton').find('i').remove();
                jQuery('.validateBox').show();
                jQuery('.validateBox').html("Batch Submitted");

            },
            error: function (errorThrown) {
                jQuery('.validateBox').hide();
                jQuery('#imagesButton').find('i').remove();
                console.log(errorThrown);
            }
        });
    }


</script>