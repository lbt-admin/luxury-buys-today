<?php
/**
 * Plugin Name:  LBT Image Validation
 * Plugin URI:   https://www.luxurybuystoday.com
 * Description:  This checks for dead links in posts
 * Plugin Author: Dennis Peacock (LBT Team)
 */


if (!defined('ABSPATH')) {
    exit;
}
define('IMAGE_VALIDATION_PLUGIN_PATH', plugin_dir_path(__FILE__));


//setup cron timing
add_filter('cron_schedules', 'image_validation_add_cron_interval');

function image_validation_add_cron_interval($schedules)
{
    $schedules['five_seconds'] = array(
        'interval' => 5,
        'display' => esc_html__('Every Five Seconds'),
    );

    return $schedules;
}


if (!class_exists('LBT_image_validation')) {

    /**
     * Class LBT_image_validation
     */
    class LBT_image_validation
    {

        /**
         * LBT_image_validation constructor.
         */

        /**
         * @var array
         */
        private $response_array = [200, 201, 204];

        /**
         * @var integer
         */
        private $error_count = 0;

        /**
         * @var string
         */
        private $replace_url = '';

        /**
         * @var string
         */
        private $batch_id = '';

        /**
         * @var string
         */
        private $logLocation = "/logs/image-validation/";

        function __construct()
        {

            //get path for default image
            $this->replace_url = get_template_directory() . '/assets/images/no-content-placeholder.png';

            //menu
            add_action('admin_menu', [$this, 'image_validation_display']);
            //add ajax events
            add_action('wp_ajax_validation_get_posts', [$this, 'validation_set_batch']);
            add_action('wp_ajax_nopriv_validation_get_posts', [$this, 'validation_set_batch']);


            //setup cron hook
            add_action('process_image_validator', [$this, 'run_batch']);


            //create logs directory
            $this->check_log_directory();
        }

        function check_log_directory()
        {

            if (!is_dir(WP_CONTENT_DIR . $this->logLocation)) {
                //Directory does not exist, so lets create it.
                mkdir(WP_CONTENT_DIR . $this->logLocation, 0755, true);
            }
        }

        //menu functions
        function image_validation_display()
        {
            add_submenu_page(
                'edit.php',
                'LBT Image Validation',
                'LBT Image Validation',
                'manage_options',
                'lbt_image_validation',
                [$this, 'display_image_validation_menu']
            );

        }

        function display_image_validation_menu()
        {
            include_once(IMAGE_VALIDATION_PLUGIN_PATH . '/templates/image_validation_options.php');
        }


        //create cron job

        function validation_set_batch()
        {

            $filename = WP_CONTENT_DIR . $this->logLocation . "start_batch.csv";

            $fp = fopen($filename, 'w');
            $id = uniqid();
            $data = [$_POST['start_date'], $_POST['end_date'], $id];
            fputcsv($fp, $data, '`');
            $this->run_batch();

        }

        //run cron job
        function run_batch()
        {
            $filename = WP_CONTENT_DIR . $this->logLocation . "start_batch.csv";
            if (($handle = fopen($filename, "r")) !== FALSE) {

                while (($data = fgetcsv($handle, 0, "`")) !== FALSE) {


                    $this->batch_id = $data[2];
                    $this->validation_get_posts($data[0], $data[1]);


                }
            }


        }


        function validation_get_posts($start, $end)
        {


            $processed = 0;
            $filename = WP_CONTENT_DIR . $this->logLocation . $this->batch_id . "_running_batch.csv";
            //check for batch to run
            if (!file_exists($filename)) {
                //create post ids for this batch

                $this->generate_post_ids($start, $end);
            }

            $handle = fopen($filename, "r");

            $post_ids = fgetcsv($handle, 0, "`");

            $current_batch = array_chunk($post_ids, 40);
            //counter
            $x = 0;
            foreach ($current_batch[0] as $singlePost) {
                $post = get_post($singlePost);
                if (get_post_meta($singlePost, 'image_processed', true) == 0) {
                    $this->replace_image_link($post->post_content, $singlePost);
                    update_post_meta($singlePost, 'image_processed', 1);
                } elseif (get_post_meta($singlePost, 'image_processed', true) == 1) {
                    //count images that have already been processed
                    $processed++;

                }
                //update counter
                $x++;
            }

            //remove processed ids
            unset($current_batch[0]);

            //put the chunks back together to put back into process file
            $remainingIds = call_user_func_array('array_merge', $current_batch);
            if (count($remainingIds) > 0) {
                $handle = fopen($filename, "w");
                fputcsv($handle, $remainingIds, '`');
            } else {
                //remove post_ids file since its finished processing
                unlink($filename);

                //update batch list to remove this batch
                $filename2 = WP_CONTENT_DIR . $this->logLocation . "start_batch.csv";
                $batchArray = [];
                if (($handle2 = fopen($filename2, "r")) !== FALSE) {
                    while (($data = fgetcsv($handle, 0, "`")) !== FALSE) {
                        if ($data[2] != $this->batch_id) {
                            $batchArray[] = $data;
                        }
                    }
                    $fp = fopen($filename2, 'w');

                    foreach ($batchArray as $batch) {
                        fputcsv($fp, $batch, '`');
                    }

                }

            }

            $data = [$start, $end, $x, $this->error_count, $processed];
            $this->batch_log($data);


            //cleanup

            wp_die();
        }

        function generate_post_ids($start, $end)
        {
            $post_ids = [];
            global $wpdb;
            $args = array(
                'posts_per_page' => -1,
                'post_type' => 'post',
                'date_query' => array(
                    array(
                        'after' => date("F j, Y", strtotime($start)),
                        'before' => date("F j, Y", strtotime($end)),
                        'inclusive' => true,
                    ),
                ),
            );
            $query = new WP_Query($args);

            $posts = $query->get_posts();

            //spin through results
            if ($posts) {


                foreach ($posts as $post) {
                    $post_ids[] = $post->ID;
                }
            }

            //create file with list of ids to process
            $filename = WP_CONTENT_DIR . $this->logLocation . $this->batch_id . "_running_batch.csv";
            $fp = fopen($filename, 'w');
            fputcsv($fp, $post_ids, '`');


        }

        function replace_image_link($content, $id)
        {

            //get image src
            $pattern = '@src="([^"]+)"@';
            $content = preg_match($pattern, $content, $matches);


            if (!empty($matches[1])) {

                $this->check_url($matches[1], $id);


            }

        }

        function check_url($url, $id)
        {

            //check url
            $raw_response = wp_remote_get($url);

            //see if we get a bad response
            if (!in_array(wp_remote_retrieve_response_code($raw_response), $this->response_array)) {

                $data = [$url, $id];
                // add to log if response wasnt 20*
                $this->image_log($data);
                $this->error_count++;

                //replace url in post content
                $content = get_the_content($id);
                $content = str_replace($url, $this->replace_url, $content);
                $post_array = ['ID' => $id, 'post_content' => $content];
                wp_update_post($post_array);

                $checkMeta=get_post_meta($id,'wpr_remote_featured_img',true);
                if($checkMeta==$url) {
                    update_post_meta($id,'wpr_remote_featured_img',$this->replace_url);
                }

                if (has_post_thumbnail( $id )) {
                    update_post_meta($id,'wpr_remote_featured_img',get_the_post_thumbnail_url($id));
                }
                //set custom fields
                update_post_meta($id, 'image_error_url', $url);
            }

            return;
        }

        function image_log($data)
        {

            $filename = WP_CONTENT_DIR . $this->logLocation . $this->batch_id . "_image_log.csv";

            if (!file_exists($filename)) {
                $fp = fopen($filename, 'w');
            } else {
                $fp = fopen($filename, 'a');
            }


            $data[] = date('Y-m-d H:i:s');
            fputcsv($fp, $data, '`');


            fclose($fp);

        }


        function batch_log($data)
        {

            $filename = WP_CONTENT_DIR . $this->logLocation . $this->batch_id . "_image_batch_log.csv";

            if (!file_exists($filename)) {
                $fp = fopen($filename, 'w');
            } else {
                $fp = fopen($filename, 'a');
            }


            $data[] = date('Y-m-d H:i:s');
            fputcsv($fp, $data, '`');


            fclose($fp);

        }


        //end class
    }
//end if class exists
}

new LBT_image_validation();
