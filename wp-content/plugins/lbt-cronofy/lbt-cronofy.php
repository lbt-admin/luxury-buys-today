<?php
/**
 * Plugin Name: LBT Cronofy
 * Description: Creat appointments in Cronofy
 * Author:      LBTCronofy
 * Author URI:  https://www.webpigment.com
 * Version:     1.0.0
 * Text Domain: lbtcronofy
 *
 * @package    LBTCronofy
 * @author     LBTCronofy
 * @since      1.0.0
 * @license    GPL-3.0+
 * @copyright  Copyright (c) 2020, LBTCronofy
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

final class LBTCronofy {
	/**
	 * Instance of the plugin
	 *
	 * @var LBTCronofy
	 */
	private static $_instance;

	/**
	 * Plugin version
	 *
	 * @var string
	 */
	private $_version = '1.0.0';

	public static function instance() {
		if ( ! isset( self::$_instance ) && ! ( self::$_instance instanceof LBTCronofy ) ) {
			self::$_instance = new LBTCronofy();
			self::$_instance->constants();
			self::$_instance->includes();

			add_action( 'plugins_loaded', [ self::$_instance, 'objects' ] );
		}

		return self::$_instance;
	}

	/**
	 * 3rd party includes
	 *
	 * @return  void
	 */
	private function includes() {
		require_once dirname( __FILE__ ) . '/vendor/autoload.php';
		require_once LBT_CRONOFY_PLUGIN_DIR . 'inc/core/autoloader.php';
	}

	/**
	 * Define plugin constants
	 *
	 * @return  void
	 */
	private function constants() {
		// Plugin version
		if ( ! defined( 'LBT_CRONOFY_VERSION' ) ) {
			define( 'LBT_CRONOFY_VERSION', $this->_version );
		}

		// Plugin Folder Path
		if ( ! defined( 'LBT_CRONOFY_PLUGIN_DIR' ) ) {
			define( 'LBT_CRONOFY_PLUGIN_DIR', trailingslashit( plugin_dir_path( __FILE__ ) ) );
		}

		// Plugin Folder URL
		if ( ! defined( 'LBT_CRONOFY_PLUGIN_URL' ) ) {
			define( 'LBT_CRONOFY_PLUGIN_URL', trailingslashit( plugin_dir_url( __FILE__ ) ) );
		}
	}

	/**
	 * Initialize classes / objects here
	 *
	 * @return  void
	 */
	public function objects() {
		\LBTCronofy\API\AJAX_Handler::get_instance();

		if ( ! is_admin() ) {
			\LBTCronofy\Cronofy::get_instance();
		}
	}
}

/**
 * Load the plugin
 *
 * @return  LBTCronofy
 */
function lbtcronofy() {
	return LBTCronofy::instance();
}
lbtcronofy();

/**
 * Cronofy client
 *
 * @return  \LBTCronofy\Core\Cronofy_Client
 */
function lbtcronofy_client() {
	return \LBTCronofy\Core\Cronofy_Client::get_instance();
}

/**
 * Datahub client
 *
 * @return  \LBTCronofy\Datahub
 */
function lbtcronofy_datahub() {
	return \LBTCronofy\Datahub::get_instance();
}

add_action(
	'rest_api_init',
	function () {
		register_rest_route(
			'wp/v2',
			'/update-ratings',
			array(
				'methods'  => 'POST',
				'callback' => 'lbt_update_ratings',
			)
		);
		register_rest_route(
			'wp/v2',
			'/update-ratings',
			array(
				'methods'  => 'GET',
				'callback' => 'lbt_get_posts_links_ids',
			)
		);
	}
);

function lbt_update_ratings( WP_REST_Request $request ) {
	$parameters = $request->get_params();
	foreach ( $parameters['data'] as $post_id => $param ) {
		update_post_meta( $post_id, 'offer_raiting', $param );
		delete_post_meta( $post_id, 'lbt_broyhill_comment' );
	}
	wp_mail('mitko.kockovski@gmail.com,billfutreal@gmail.com','raiting update staging', print_r( $request, true ) );
	return array( 'status' => 'OK' );
}

function lbt_get_posts_links_ids() {
	// The Query
	$args   = array(
		'posts_per_page' => 1000,
		'date_query' => array(
			array(
				'before' => '24 hours ago'
			)
		),
		'category__not_in' => array( 75, 36, 37, 38, 39 ),
		'meta_query' => array(
			array(
				'key'     => 'wpr_article_source_url',
				'value'   => '',
				'compare' => '!=',
			),
		),
	);
	$query1 = new WP_Query( $args );

	// The Loop
	$return = array();
	while ( $query1->have_posts() ) {
		$query1->the_post();
		if ( get_post_meta( get_the_ID(), 'wpr_article_source_url', true ) ) {
			$return[get_the_ID()] = get_post_meta( get_the_ID(), 'wpr_article_source_url', true );
		}
	}

	/*
	 Restore original Post Data
	 * NB: Because we are using new WP_Query we aren't stomping on the
	 * original $wp_query and it does not need to be reset with
	 * wp_reset_query(). We just need to set the post data back up with
	 * wp_reset_postdata().
	 */
	wp_reset_postdata();
	return $return;
}
