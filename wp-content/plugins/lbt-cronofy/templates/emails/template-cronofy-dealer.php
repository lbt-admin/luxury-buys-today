<p>Dear <?php echo esc_html( $contact[0]['firstname'] ); ?>,</p>

<p><?php echo esc_html( $userdata->first_name ); ?> has requested an appointment with you.</p>

<p>Here’s their contact information:</p>

<p><?php echo esc_html( $userdata->first_name ) . ' ' . esc_html( $userdata->last_name ); ?></p>

<p><?php echo esc_html( $userdata->user_email ); ?></p>

<p><?php echo esc_html( get_user_meta( get_current_user_id(), 'phone', true ) ); ?></p>

<p>Please reach out to them as soon as possible to schedule an appointment - they are very interested in your dealership and offers.</p>
