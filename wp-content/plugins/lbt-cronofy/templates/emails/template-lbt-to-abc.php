<div>
    <p><?php echo date_i18n( 'm d, Y');  ?></p>

    <p>Dear <?php echo $dealer_name; ?>,</p>

    <p><?php echo $prospect_name; ?> has requested an appointment for this <?php echo $post_link; ?>.</p>

    <p>
        <!-- When: 	Friday, Sept 25, 2020 1pm – 1:30pm (PDT)<br/> -->
        When: <?php echo $event_start_dealer; ?><br/>
        Where: 	<?php echo $dealer_address; ?>
    </p>

    <p>Please confirm this appointment in your calendar. If you need to reschedule, please make sure you follow up as soon as possible.</p>

    <p>
        <?php echo $dealer_name; ?><br/>
        <?php echo $dealer_phone; ?><br/>
        <?php echo $dealer_mail; ?>
    </p>

    <p style="font-size: 10px;">
        We’re all about providing first-class service — it’s our top priority.<br/>
        If you have any issues please contact us at <a href="mailto:support@luxurybuystoday.com?subject=Appointment Issue">support@luxurybuystoday.com</a>.
    </p>
</div>
