<?php
/**
 * Autoloader
 *
 * @param   string  $class
 *
 * @return  boolean
 */
function lbtcronofy_plugin_autoloader( $class ) {
    $dir = '/inc';
    $type = 'class';


	switch ( $class ) {
        case false !== strpos( $class, 'LBTCronofy\\API\\' ):
            $class = strtolower( str_replace( 'LBTCronofy\\API', '', $class ) );
            $dir .= '/api';
        break;
		case false !== strpos( $class, 'LBTCronofy\\Traits\\' ):
            $class = strtolower( str_replace( 'LBTCronofy\\Traits', '', $class ) );
            $dir .= '/traits';
            $type = 'trait';
        break;
		case false !== strpos( $class, 'LBTCronofy\\Core\\' ):
            $class = strtolower( str_replace( 'LBTCronofy\\Core', '', $class ) );
            $dir .= '/core';
        break;
		case false !== strpos( $class, 'LBTCronofy\\' ):
            $class = strtolower( str_replace( 'LBTCronofy', '', $class ) );
        break;
		default: return;
	}

	$filename = LBT_CRONOFY_PLUGIN_DIR . $dir . str_replace( '_', '-', str_replace( '\\', '/' . $type . '-', $class ) ) . '.php';

    if ( file_exists( $filename ) ) {
		require_once $filename;

		if ( class_exists( $class ) ) {
			return true;
		}
	}

	return false;
}

spl_autoload_register( 'lbtcronofy_plugin_autoloader' );
