<?php
namespace LBTCronofy\Core;

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class Cronofy_Client {
    use \LBTCronofy\Traits\Singleton;

    private $_api_base = 'https://api.cronofy.com/v1/';

    private $_refresh_token_tries = 0;

    /**
     * If exits, invoked in Singleton traint private constructor
     *
     * @return  void
     */
    private function _initialize() {

    }

    /**
     * Cronofy request helper
     *
     * @param   string $endpoint
     * @param   array $args
     *
     * @throws Exception
     *
     * @return  array
     */
    public function request( $endpoint, $args = array(), $token = false ) {
        if ( empty( $args['category_id'] ) ) {
            throw new \Exception( __( 'category_id required in arguments' ) );
        }

        $category_id = $args['category_id'];

        $cronofy_data = $this->get_closest_dealer_cronofy_acc_id( absint( $category_id ) );

        if ( is_wp_error( $cronofy_data ) ) {
            throw new \Exception( $cronofy_data->get_error_message() );
        }

        unset( $args['category_id'] );

        $default_request_args = array(
            'method'  => 'POST',
            'timeout' => '45',
            'headers' => array(
                'Authorization' => 'Bearer ' . ( $token ? $cronofy_data['token'] : $cronofy_data['secret_key'] ),
                'Content-type' => 'application/json'
            ),
        );

        $request_args = wp_parse_args( $args, $default_request_args );

        $response = wp_remote_post( $this->_api_base . $endpoint, $request_args );

        $original_res = $response;

        if ( is_wp_error( $response ) ) {
            error_log( 'LBT Cronofy Request Error: ' . $response->get_error_message() );
            throw new \Exception(
                $response->get_error_message()
            );
        }

        if ( isset( $response['response']['code'] ) && 401 == $response['response']['code'] ) {
			if ( 3 == $this->_refresh_token_tries ) {
				error_log( 'Cronofy Request Error: 401 Unauthorized error after 3 tries of token refresh');
				throw new \Exception(
					__( '401 Unauthorized after 3 tries of token refresh' )
				);
			}
			try {
				$this->_refresh_token_tries++;
                $res = $this->_refresh_token( $cronofy_data['dealer_id'] );
                $args['category_id'] = $category_id;
				$this->request( $endpoint, $args, true );
			} catch ( \Exception $e ) {
				throw new \Exception( $e->getMessage() );
			}
		}

        $response = json_decode( wp_remote_retrieve_body( $response ), true );

        return $response;
    }

    /**
     * Refresh token for dealer
     *
     * @param   int  $dealer_id
     *
     * @return  void
     */
    private function _refresh_token( $dealer_id ) {
        $key = md5( 'lbt_dealer_cronid_' . $dealer_id );

        $dealer_data = get_transient( $key );

        if ( ! $dealer_data ) {
            error_log( 'Failed to refresh token. Missing required params'  );
			throw new \Exception(
				__( 'Failed to refresh token. Missing required params' )
			);
        }

        $body = array(
            'client_id' => $dealer_data['cliend_id'],
            'client_secret' => $dealer_data['secret_key'],
            'grant_type' => 'refresh_token',
            'refresh_token' => $dealer_data['refresh_token']
        );

		$args = array(
			'method'  => 'POST',
			'timeout' => '45',
			'headers' => array(
				'Accept' => 'application/json',
			),
			'body' => $body
		);

		$response = wp_remote_post( 'https://api.cronofy.com/oauth/token', $args );

		if ( is_wp_error( $response ) ) {
			error_log( 'Data Hub Site Cronofy Request Error: ' . $response->get_error_message() );
			throw new \Exception(
				$response->get_error_message()
			);
		}

		$response = json_decode( wp_remote_retrieve_body( $response ), true );

		if ( ! empty( $response['access_token'] ) ) {
            if ( $dealer_data ) {
                $dealer_data['token'] = sanitize_text_field( $response['access_token'] );
                $dealer_data['token_exp'] = sanitize_text_field( $response['expires_in'] );

                set_transient( $key, $dealer_data, YEAR_IN_SECONDS );
            }
        }

        return $response;
	}

    /**
     * Get element token
     *
     * @param   string  $acc_id
     * @param   string  $type
     * @param   int  $category_id
     *
     * @return  mixed<string|boolean>   Token on success, false on error or missing something
     */
    public function get_element_token( $acc_id, $type, $category_id ) {
        $key = md5( 'lbt_eltk_' . $acc_id );

        $tok = wp_cache_get( $key );

        if ( $tok ) {
            return $tok;
        }

        $tok = get_transient( $key );

        if ( $tok ) {
            return $tok;
        }

        try {
            $request_el_token = $this->request( 'element_tokens', [
                'category_id' => $category_id,
                'body' => json_encode([
                    'version' => '1',
                    'permissions' => [ $type ],
                    'subs' => [ $acc_id ],
                    'origin' => get_site_url()
                ])
            ] );

            if ( empty( $request_el_token['element_token']['token'] ) ) {
                return new \WP_Error(
					404,
					__( 'Element Token empty' )
				);
            }

            wp_cache_set( $key, $request_el_token['element_token']['token'], 'lbt-cronofy' );
            set_transient( $key, $request_el_token['element_token']['token'], $request_el_token['element_token']['expires_in'] );
            return $request_el_token['element_token']['token'];
        } catch ( \Exception $e ) {
            error_log( $e->getMessage() );
            return new \WP_Error(
				500,
				$e->getMessage()
			);
        }
    }

    /**
     * Get closest dealer cronofy acc id
     *
     * @param int $category_id
     * @param string $dealer_id     If dealer_id is provided return account for dealer.
     *
     * @return  mixed<array|boolean>   Array of cronofy data on success, false on not found or missing something
     */
    public function get_closest_dealer_cronofy_acc_id( $category_id = 0, $dealer_id = null ) {
        if ( ! function_exists( 'wp_lbt_ontraport_request' ) ) {
            error_log('wp_lbt_ontraport_request function missing');
            return;
        }

		if ( ! $dealer_id ) {
			$dealer = \LBT\Dealer_Locator::get_nearby_dealer( $category_id );

			if ( is_wp_error( $dealer ) ) {
				return $dealer;
			}

			$dealer_id = $dealer['id'];
		}

        $key = md5( 'lbt_dealer_cronid_' . $dealer_id );

        $cron_acc = wp_cache_get( $key );

        if ( $cron_acc ) {
            return $cron_acc;
        }

        if ( empty( $dealer_id ) ) {
            return new \WP_Error(
                404,
                __( 'Dealer ID missing' )
            );
        }

        try {
			$condition[] = array(
				'field' => array( 'field' => 'f2963' ),
				'op'    => '=',
				'value' => array( 'value' => $dealer_id ),
			);
			$condition[] = 'AND';
			$condition[] = array(
				'field' => array( 'field' => 'f2882' ),
				'op'    => '=',
				'value' => array( 'value' => 182 ),
			);
			$condition[] = 'AND';
			$condition[] = array(
				'field' => array( 'field' => 'f2964' ),
				'op'    => '=',
				'value' => array( 'value' => 1 ),
			);

            $contact = wp_lbt_ontraport_request('Contacts?range=1&condition=' . urlencode( wp_json_encode( $condition ) ), [ 'method' => 'GET' ] );

			if( isset( $_GET['mitko123222123'])) {
				echo '<pre>';
				print_r( $contact );
				print_r( $dealer_id );
				echo '</pre>';
			}

			if ( is_wp_error( $contact ) ) {
				return $contact;
			}

			if ( empty( $contact[0]['id'] ) ) {
				return new \WP_Error(
					'contact_not_found',
					__( 'Dealership Contact not found' )
				);
			}

			$has_free_trial = false !== strpos( $contact[0]['contact_cat'], '*/*2044*/*' );

			if ( isset( $_GET['skip_free_trial'] ) ) {
				$has_free_trial = true;
			}

			$cronofy_data = [
				'acc' => '',
				'secret_key' => '',
				'calendar_id' => '',
				'cliend_id' => '',
				'token' => '',
				'refresh_token' => '',
				'dealer_email' => $contact[0]['email'],
				'dealer_id' => $dealer_id,
				'first_name' => $contact[0]['firstname'],
				'last_name' => $contact[0]['lastname'],
				'dealer_phone' => $contact[0]['cell_phone'],
				'working_hours' => $contact[0]['f3015'],
				'contact_id' => $contact[0]['id'],
				'free_trial' => $has_free_trial,
				'tags'       => $contact[0]['contact_cat'],
			];

			if ( ! $has_free_trial ) {
				$userdata = wp_get_current_user();

				$subject = 'New Prospect';
				ob_start();
				require LBT_CRONOFY_PLUGIN_DIR . 'templates/emails/template-cronofy-dealer.php';
				$message = ob_get_clean();

				$headers = array(
					'Content-Type: text/html; charset=UTF-8',
				);

				wp_mail( $contact[0]['email'], $subject, $message, $headers );
			}

            if (
                ! empty( $contact[0]['f2976'] ) &&
                ! empty( $contact[0]['f3001'] ) && ! empty( $contact[0]['f3002'] ) &&
                ! empty( $contact[0]['f3011'] ) && ! empty( $contact[0]['f3012'] ) && ! empty( $contact[0]['f3013'] )
            ) {
				$cronofy_data['acc'] = $contact[0]['f2976'];
				$cronofy_data['secret_key'] = $contact[0]['f3001'];
				$cronofy_data['calendar_id'] = $contact[0]['f3002'];
				$cronofy_data['cliend_id'] = $contact[0]['f3011'];
				$cronofy_data['token'] = $contact[0]['f3012'];
				$cronofy_data['refresh_token'] = $contact[0]['f3013'];

				wp_cache_set( $key, $cronofy_data, 'lbt-cronofy' );

                return $cronofy_data;
            }

			wp_cache_set( $key, $cronofy_data, 'lbt-cronofy', 86400 );

            return new \WP_Error(
                'contact_no_cronofy_key',
                __( 'Dealership Contact does not have Cronofy Key' ),
				$cronofy_data
            );
        } catch ( \Exception $e ) {
            error_log( 'CONTACTS ERROR:\n' );
            error_log( $e->getMessage() );
            return new \WP_Error(
                500,
                __( $e->getMessage() )
            );
        }
    }

	private function _get_dealership_by( $type, $value ) {
		if ( 'email' === $type && ! is_email( $value ) ) {
			error_log( 'Save CTA - Get Dealership email error: Invalid email' );
			return new \WP_Error(
				400,
				__( 'Invalid email' )
			);
		}

		if ( 'email' === $type ) {
			return wp_lbt_ontraport_get_dealership_by_contact_email( $value );
		}

		// This email check will be removed after client approves the update above.
		if ( 'email' === $type ) {
			$field = 'f2737';
			$field = 'f2930';
		} elseif ( 'unique_id' === $type ) {
			$field = 'unique_id';
		} else {
			return new \WP_Error(
				400,
				__( 'Invalid type. Can be "email" or "unique_id' )
			);
		}

		$condition[] = array(
			'field' => array( 'field' => $field ),
			'op'    => '=',
			'value' => array( 'value' => $value ),
		);

		$query_string = 'range=1&condition=' . urlencode( json_encode( $condition ) );

		try {
			$dealership = wp_lbt_ontraport_request(
				'Dealership-ABCs?' . $query_string,
				array(
					'method' => 'GET',
				)
			);

			if ( ! empty( $dealership ) ) {
				return $dealership[0];
			}

			return false;
		} catch ( \Exception $e ) {
			error_log( 'Save CTA - Get Dealership email error: ' . $e->getMessage() );
			return new \WP_Error(
				$e->getCode(),
				$e->getMessage()
			);
		}
	}

    /**
     * Get dealer id
     *
     * @return  int
     */
    public function get_dealer_id( $category_id ) {
        if ( ! $category_id ) {
            return new \WP_Error(
                400,
                __( 'Category ID missing' )
            );
        }
        if ( class_exists( '\LBT\Dealer_Locator' ) ) {

            $dealer = \LBT\Dealer_Locator::get_nearby_dealer( $category_id );

			if ( is_wp_error( $dealer ) ) {
				return $dealer;
			}

            return $dealer['id'];
        }

        return new \WP_Error(
            404,
            __( 'Function "\LBT\Dealer" not found' )
        );
    }
}
