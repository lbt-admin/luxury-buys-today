<?php
namespace LBTCronofy;

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class Datahub {
    use \LBTCronofy\Traits\Singleton;

    private $_base_url = 'https://v2.luxurybuystoday.com/wp-json/lbt-hub/v1/';

    /**
     * Send request to Datahub
     *
     * @param   string  $endpoint
     * @param   array $args
     *
     * @throws \Exception
     *
     * @return  mixed
     */
    public function request( $endpoint, $args = [] ) {
        $request_args = [
            'method' => 'POST',
            'timeout' => '45',
            'headers' => array(
                'Content-type' => 'application/json'
            ),
        ];

        if ( ! empty( $args ) ) {
            $request_args = wp_parse_args( $args, $request_args );
        }

        $response = wp_remote_post( $this->_base_url . $endpoint, $request_args );


		if ( is_wp_error( $response ) ) {
            error_log( 'LBT Datahub Request Error: ' . $response->get_error_message() );
			throw new \Exception(
                $response->get_error_message()
			);
        }

        $response = json_decode( wp_remote_retrieve_body( $response ), true );

        return $response['data'];
    }

    /**
     * Send newly created appointment to datahub
     *
     * @param   array  $event
     *
     * @return  mixed
     */
    public function send_new_appointment( $event ) {
        try {
            $response = $this->request( 'cronofy/appointment', [
                'body' => json_encode( $event )
            ] );

            return $response;
        } catch ( \Exception $e ) {
            return $e->getMessage();
        }
    }
}
