<?php
namespace LBTCronofy\API;

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class AJAX_Handler {
    use \LBTCronofy\Traits\Singleton;

    /**
     * Initialize class
     *
     * @return  void
     */
    private function _initialize() {
        add_action( 'wp_ajax_lbt_cronofy_add_schedule', [ $this, 'cronofy_add_schedule'] );
    }

    /**
     * Add schedule / event to cronofy
     *
     * @return  \JSON
     */
    public function cronofy_add_schedule() {
        $args = $_POST;

        if ( ! wp_verify_nonce( $args['_wpnonce'], 'ajax-lbt-cronofy' ) ) {
            wp_send_json_error(
                __( 'Invalid nonce', 'lbtcronofy' ),
                403
            );
        }

        if ( ( ! $args['skip_cal'] && ( empty( $args['slot'] ) || empty( $args['cal_id'] ) ) ) || empty( $args['dealer_id'] ) ) {
            wp_send_json_error(
                __( 'Missing required "slot", "dealer_id" or "cal_id" params', 'lbtcronofy' ),
                400
            );
        }

        if ( empty( $args['email'] ) || ! is_email( $args['email'] ) ) {
            wp_send_json_error( __( 'Customer email missing or invalid' ) );
        }

        try {
            $dealer_state = $args['request']['dealer_state'];

            if ( 'NY' == $dealer_state || empty( $dealer_state ) ) {
                $dealer_state = 'New York';
            }

            $dealer_state = str_replace( ' ', '_',  $dealer_state );

            $tz = 'America/' . $dealer_state;
            $client_timezone = $args['client_timezone'];
            $event_start_dealer = null;

			if ( ! $args['skip_cal'] ) {
				$event_start_date = new \DateTime( $args['slot']['start'] );
				$event_end_date = new \DateTime( $args['slot']['end'] );

				$dealer_timezone_obj = new \DateTimeZone( $tz );
				$client_timezone_obj = new \DateTimeZone( $client_timezone );

				$event_start_date->setTimezone( $dealer_timezone_obj );
				$event_start_dealer = $event_start_date->format( 'l, M d, Y g:ia' );
				$event_start_date->setTimezone( $client_timezone_obj );
				$event_start_client_org = $event_start_date->format( 'l, M d, Y g:ia' );

				$event_end_date->setTimezone( $dealer_timezone_obj );
				$event_end_dealer = $event_end_date->format( 'g:ia' );
				$event_end_date->setTimezone( $client_timezone_obj );
				$event_end_client = $event_end_date->format( 'g:ia' );

				$event_start_client = $event_start_client_org . ' - ' . $event_end_client;
				$event_start_dealer = $event_start_dealer . ' - ' . $event_end_dealer;
			}

            $prospect_email = $args['email'];

            $summary = ! empty( $args['request']['post_id'] ) ? get_the_title( $args['request']['post_id'] ) : get_cat_name( $args['category_id'] );

            $event = [
                'event_id' => 'lbr-appointment-id_' . wp_rand(0, 9999999),
                'summary' => $summary . ' Appointment',
                'description' => 'Appointment for ' . $summary,
                'start' => $args['slot']['start'] ?? '',
                'end' => $args['slot']['end'] ?? '',
                'location' => [
                    'description' => $args['request']['dealer_address']
                ],
                'tzid' => $tz,
                'attendees' => [
                    "invite" => [
                        [
                            'email' => $prospect_email
                        ]
                    ],
                ]
            ];

			$direct_event_response = 'Skip Calendar Event';

			if ( ! $args['skip_cal'] ) {
				$direct_event_response = lbtcronofy_client()->request('calendars/' . $args['cal_id'] . '/events', [
					'category_id' => $args['category_id'] ?? 0,
					'body' => json_encode( $event )
				], true );
			}


            $mail_status = null;
            $dealer_mail = ! empty( $args['dealer_email'] ) && is_email( $args['dealer_email'] ) ? $args['dealer_email'] : false;

            $headers = [ 'Content-Type: text/html; charset=UTF-8' ];

            $post_link = ! empty( $args['request']['post_id'] ) ? '<a href="' . get_permalink( $args['request']['post_id'] ) . '">' . get_the_title( $args['request']['post_id'] ) . '</a>' : '<a href="' . get_category_link( $args['category_id'] ) . '">' . get_cat_name( $args['category_id'] ) . '</a>';
            $dealer_address = $args['request']['dealer_address'] ?? '';
            $dealer_name = $args['dealer_name'];
            $dealer_phone = $args['dealer_phone'];

            $current_user = wp_get_current_user();
            $prospect_name = $current_user->user_firstname . ' ' . $current_user->user_lastname;
            $prospect_first_name = $current_user->user_firstname;

            ob_start();
            include LBT_CRONOFY_PLUGIN_DIR . 'templates/emails/template-lbt-to-abc.php';
            $message = ob_get_clean();

            if ( $dealer_mail ) {
                $dealer_mail_status = wp_mail( $dealer_mail, __( 'Appointment Requested' ), $message, $headers );
            }

            ob_start();
            include LBT_CRONOFY_PLUGIN_DIR . 'templates/emails/template-lbt-to-member.php';
            $message = ob_get_clean();

            $prospect_mail_status = wp_mail( $prospect_email, __( 'Your Appointment Request' ), $message, $headers );

            $event['dealer_id'] = $args['dealer_id'];
            $event['duration'] = 60; // 1h
            $event['email'] = $args['email'];
            $event['start'] = $event_start_client_org;
            $event['contact_id'] = $args['contact_id'];
            $event['title'] = 'Appointment for ' . $summary;
            $event['url'] = $args['url'];
            $event['status'] = 'pending';
            $event['appointment_date'] = $event_start_dealer;
			$event['dealer_timezone'] = $tz;
			$event['client_timezone'] = $client_timezone;
			$event['tz'] = $client_timezone;
			$event['offer_id'] = md5( get_permalink( $args['request']['post_id'] ?? '' ) );

            $datahub_response = lbtcronofy_datahub()->send_new_appointment( $event );

            if( $response == null ) {
                $response = [
                    'dealer_mail' => $dealer_mail_status ?? '',
                    'prospect_mail' => $prospect_mail_status ?? '',
                    'event_response' => $direct_event_response,
                    'datahub_response' => $datahub_response ?? '',
                    'datahub_request' => $event ?? '',
                ];

                wp_send_json_success( $response );
            }

            // @todo Add error logs or notifications if event failed to create on datahub or dealer email not sent

            wp_send_json_error( __( 'An error occured while creating the appointment' ) );
        } catch ( \Exception $e ) {
            error_log( $e->getMessage() );
            wp_send_json_error( $e->getMessage() );
        }
    }
}
