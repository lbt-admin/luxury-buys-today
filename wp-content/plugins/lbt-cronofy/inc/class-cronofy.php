<?php
namespace LBTCronofy;

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class Cronofy {
    use \LBTCronofy\Traits\Singleton;

    /**
     * If exits, invoked in Singleton trait private constructor
     *
     * @return  void
     */
    private function _initialize() {
        add_action( 'wp_footer', [ $this, 'footer' ] );
        add_action( 'wp_enqueue_scripts', [ $this, 'register_assets' ], 20 );
	}

    /**
     * Register assets
     *
     * @return  void
     */
	public function register_assets() {
        if ( ! is_singular( 'post' ) && ! is_archive() ) {
            return;
        }

        $category_id = 0;

        if ( is_archive() ) {
            $category_id = get_queried_object()->term_id;
        } else if ( is_single() ) {
            $cats = get_the_category( get_the_ID() );

            if ( ! empty( $cats[0] ) ) {
                $category_id = $cats[0]->cat_ID;
            }
        }

        $cronofy_data = lbtcronofy_client()->get_closest_dealer_cronofy_acc_id( $category_id );

        if ( isset($_GET['test'])) {
            var_dump($cronofy_data);
            die;
        }

		wp_register_script( 'lbt-cronofy-elements', 'https://elements.cronofy.com/js/CronofyElements.v1.20.7.js', [], null, true );
        wp_register_script( 'lbt-momentjs', LBT_CRONOFY_PLUGIN_URL . 'assets/public/libraries/moment.js', [], null, true );

		wp_enqueue_script(
            'lbt-cronofy-script',
            LBT_CRONOFY_PLUGIN_URL . 'assets/public/js/script.js',
			['jquery', 'lbt-momentjs', 'lbt-cronofy-elements'],
            LBT_CRONOFY_VERSION,
            true
        );

        if ( empty( $cronofy_data ) || ! is_wp_error( $cronofy_data ) ) {
			$error_code = is_wp_error( $cronofy_data ) ? $cronofy_data->get_error_code() : 'N/A';

			$localize_data = [
                'ajaxUrl' => admin_url( 'admin-ajax.php' ),
				'salepersonAvailable' => false,
				'isUserLoggedIn' => is_user_logged_in(),
				'errorCode' => $error_code,
			];

			if ( 'contact_no_cronofy_key' === $error_code ) {
				$cronofy_data = $cronofy_data->get_error_data();
				$localize_data['dealerID'] = $cronofy_data['dealer_id'];
				$localize_data['dealerEmail'] = $cronofy_data['email'];
				$localize_data['dealerName'] = ( $cronofy_data['first_name'] ?? '' ) . ' ' . ( $cronofy_data['last_name'] ?? '' );
				$localize_data['dealerPhone'] = $cronofy_data['dealer_phone'] ?? '';
				$localize_data['dealerWorkingHours'] = $dealer_working_hours;
				$localize_data['categoryID'] = $category_id;
				$localize_data['contactID'] = $cronofy_data['contact_id'];
				$localize_data['url'] = get_permalink();
				$localize_data['offerID'] = is_singular( 'post' ) ? md5( get_the_ID() ) : '';
				$localize_data['prospectEmail'] = is_user_logged_in() ? wp_get_current_user()->user_email : '';
				$localize_data['wpNonce'] = wp_create_nonce( 'ajax-lbt-cronofy' );
				$localize_data['salepersonAvailable'] = true;
				$localize_data['freeTrial'] = isset( $_GET['skip_free_trial'] ) ? 1 : $cronofy_data['free_trial'];
				$localize_data['tags']      = $cronofy_data['tags'];
			}

			wp_localize_script(
				'lbt-cronofy-script',
				'cronofy',
				$localize_data
			);

            return;
        }

	    if ( is_array( $cronofy_data ) && ( $cronofy_data['token'] === 'kO5Xd99UXDTF9j3TA0leXimZl9XVFYlM' || $cronofy_data['token'] === 'wrgxaLjD6nKXjoH9vTsSehJsQa07_RhL' ) ) {
		   $cronofy_data['token'] = 'lYFtDX-4qNz4waFZ3ryaWSH4055M2_e5';
	    }

        $element_token = null;

        if ( is_array( $cronofy_data ) && isset( $cronofy_data['acc'] ) ) {
           $element_token = lbtcronofy_client()->get_element_token( $cronofy_data['acc'], 'availability', $category_id );
        }

        if ( isset($_GET['test233'])) {
		   echo '<pre>';
            print_r($cronofy_data);
            print_r($element_token);
            die;
        }

        if ( ! $element_token ) {
            return;
        }

        wp_enqueue_style(
            'lbt-cronofy-style',
            LBT_CRONOFY_PLUGIN_URL . 'assets/public/css/style.css',
            [],
            LBT_CRONOFY_VERSION
        );

        $dealer_working_hours = $cronofy_data['working_hours'] ?? '08:00am-04:00pm';
        $dealer_working_hours = explode( '-', $dealer_working_hours );

        if ( 2 !== count( $dealer_working_hours ) ) {
            $dealer_working_hours = [];
            $dealer_working_hours[0] = '08:00am';
            $dealer_working_hours[1] = '04:00pm';
        }

        wp_localize_script(
            'lbt-cronofy-script',
            'cronofy',
            [
                'ajaxUrl' => admin_url( 'admin-ajax.php' ),
                'elementToken' => $element_token,
                'account' => $cronofy_data['acc'],
                'calID' => $cronofy_data['calendar_id'],
                'dealerID' => $cronofy_data['dealer_id'],
                'dealerEmail' => $cronofy_data['email'],
                'dealerName' => ( $cronofy_data['first_name'] ?? '' ) . ' ' . ( $cronofy_data['last_name'] ?? '' ),
                'dealerPhone' => $cronofy_data['dealer_phone'] ?? '',
                'dealerWorkingHours' => $dealer_working_hours,
                'categoryID' => $category_id,
                'contactID' => $cronofy_data['contact_id'],
                'url' => get_permalink(),
				'offerID' => is_singular( 'post' ) ? md5( get_the_ID() ) : '',
                'prospectEmail' => is_user_logged_in() ? wp_get_current_user()->user_email : '',
                'prospectID' => is_user_logged_in() ? get_current_user_id() : '',
                'wpNonce' => wp_create_nonce( 'ajax-lbt-cronofy' ),
				'salepersonAvailable' => true,
				'freeTrial' => isset( $_GET['skip_free_trial'] ) ? 1 : $cronofy_data['free_trial'],
				'tags'      => $cronofy_data['tags'],
            ]
        );
    }

    /**
     * Load modal in footer
     *
     * @return  void
     */
    public function footer() {
	    if ( ! is_archive() && ! is_singular( 'post' ) ) {
		    return;
	    }

		echo '<input type="hidden" name="lbt_cronofy_enabled">';

	    $category_id = 0;

	    if ( is_archive() ) {
		    $category_id = get_queried_object()->term_id;
	    } else if ( is_single() ) {
		    $cats = get_the_category( get_the_ID() );

		    if ( ! empty( $cats[0] ) ) {
			    $category_id = $cats[0]->cat_ID;
		    }
	    }
	    $cronofy_data = lbtcronofy_client()->get_closest_dealer_cronofy_acc_id( $category_id );

	    if ( is_wp_error( $cronofy_data ) ) {
		  echo '<input type="hidden" name="lbt_cronofy_error_data" data-err-code="' . esc_attr( $cronofy_data->get_error_code() ) . '" value="' . esc_attr( $cronofy_data->get_error_message() ) . '">';
		  return;
	    }

	    if( isset( $_GET['mitko123222123'])) {
		    echo '<pre>';
		    print_r( $cronofy_data );
		    echo '</pre>';
	    }
	    $element_token = lbtcronofy_client()->get_element_token( $cronofy_data['acc'], 'availability', $category_id );

	    if ( is_wp_error( $element_token ) ) {
			echo '<input type="hidden" name="lbt_cronofy_error_data" value="' . esc_attr( $element_token->get_error_message() ) . '">';
		    return;
	    }

        echo '
            <div class="wpr-cronofy-calendar-overlay"></div>
            <div class="wpr-cronofy-calendar-modal">
                <div class="wpr-cronofy-calendar-modal-header">
                    <span class="wpr-cronofy-modal-close">x</span>
                </div>
                <div id="cronofy-availability-viewer"></div>
            </div>
        ';
    }
}
