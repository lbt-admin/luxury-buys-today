(function ($) {
	$(function () {
		if (typeof cronofy === "undefined") {
			console.info("Cronofy Localized object not found");
			return;
		}

		$(document).on("click", ".lbt-appointment-button", function () {
			const $this = $(this);
			if (!cronofy.isUserLoggedIn) {
				$("#lbt-login-form-section .lbt-form-switch").first().trigger("click");
			} else {
				if (cronofy.errorCode === "contact_no_cronofy_key") {
					zone = Intl.DateTimeFormat().resolvedOptions().timeZone;
					if (!zone.indexOf("America") >= 0) {
						zone = "America/Los_Angeles";
					}
					$.ajax({
						url: cronofy.ajaxUrl,
						type: "POST",
						beforeSend: function () {
							$this.prop("disabled", true);
							$this.append(
								'<div class="lbt-cta-btn-spinner"><span class="fa fa-spinner fa-spin"></span></div>'
							);
						},
						data: {
							skip_cal: true,
							slot: null,
							cal_id: null,
							dealer_id: cronofy.dealerID,
							dealer_email: cronofy.dealerEmail,
							dealer_name: cronofy.dealerName,
							dealer_phone: cronofy.dealerPhone,
							email: cronofy.prospectEmail,
							account: cronofy.account,
							category_id: cronofy.categoryID,
							contact_id: cronofy.contactID,
							prospect_id: cronofy.prospectID,
							free_trial: cronofy.freeTrial,
							tags: cronofy.tags,
							url: cronofy.url,
							client_timezone: zone,
							offer_id: cronofy.offerID,
							request: $(".wpr-cronofy-calendar-modal").data("request"),
							action: "lbt_cronofy_add_schedule",
							_wpnonce: cronofy.wpNonce,
						},
						success: function (result) {
							$this.prop("disabled", false);
							$this.find(".lbt-cta-btn-spinner").remove();
							if (result.success) {
								alert("Appointment sent!");
								$(".wpr-cronofy-calendar-overlay").hide();
								$(".wpr-cronofy-calendar-modal").hide();
							} else {
								alert(result.data);
							}
						},
						error: function (xhr, status, error) {
							$this.prop("disabled", false);
							$this.find(".lbt-cta-btn-spinner").remove();
							alert(xhr.responseJSON.data);
						},
					});
				} else {
					$('[data-target="coming-soon-modal"]').show();
				}
			}
		});

		if (cronofy.errorCode === "contact_no_cronofy_key") {
			return;
		}

		if (!cronofy.salepersonAvailable && !cronofy.freeTrial) {
			return;
		}

		const availablePeriods = [];

		for (let i = 0; i < 21; i++) {
			let date;
			let time;

			if (i == 0) {
				time = moment().add(1, "hours").format("h:mmA");
				date = moment().format("YYYY-MM-DD");
			} else {
				time = cronofy.dealerWorkingHours[0];
				date = moment().add(i, "days").format("YYYY-MM-DD");
			}

			startTime = moment(date + " " + time, ["YYYY-MM-DD h:mmA"]).format();
			endTime = moment(date + " " + cronofy.dealerWorkingHours[1], [
				"YYYY-MM-DD h:mmA",
			]).format();

			availablePeriods.push({
				start: startTime,
				end: endTime,
			});
		}

		CronofyElements.AvailabilityViewer({
			element_token: cronofy.elementToken,
			target_id: "cronofy-availability-viewer",
			availability_query: {
				participants: [
					{
						required: "all",
						members: [{ sub: cronofy.account }],
					},
				],
				required_duration: { minutes: 60 },
				available_periods: availablePeriods,
			},
			config: {
				start_time: moment(cronofy.dealerWorkingHours[0], ["h:mmA"]).format(
					"HH:mm"
				),
				end_time: moment(cronofy.dealerWorkingHours[1], ["h:mmA"]).format(
					"HH:mm"
				),
				interval: 60,
			},
			styles: {
				prefix: "wpr-lbt",
			},
			callback: (notification) => {
				if (notification.notification.type == "slot_selected") {
					createEvent(notification.notification.slot);
				}
			},
		});
		const createEvent = function (slot) {
			zone = Intl.DateTimeFormat().resolvedOptions().timeZone;
			if (!zone.indexOf("America") >= 0) {
				zone = "America/Los_Angeles";
			}
			$.ajax({
				url: cronofy.ajaxUrl,
				type: "POST",
				data: {
					skip_cal: false,
					slot: slot,
					cal_id: cronofy.calID,
					dealer_id: cronofy.dealerID,
					dealer_email: cronofy.dealerEmail,
					dealer_name: cronofy.dealerName,
					dealer_phone: cronofy.dealerPhone,
					email: cronofy.prospectEmail,
					account: cronofy.account,
					category_id: cronofy.categoryID,
					contact_id: cronofy.contactID,
					prospect_id: cronofy.prospectID,
					free_trial: cronofy.freeTrial,
					tags: cronofy.tags,
					url: cronofy.url,
					client_timezone: zone,
					offer_id: cronofy.offerID,
					request: $(".wpr-cronofy-calendar-modal").data("request"),
					action: "lbt_cronofy_add_schedule",
					_wpnonce: cronofy.wpNonce,
				},
				success: function (result) {
					if (result.success) {
						alert("Appointment sent!");
						$(".wpr-cronofy-calendar-overlay").hide();
						$(".wpr-cronofy-calendar-modal").hide();
					} else {
						alert(result.data);
					}
				},
				error: function (xhr, status, error) {
					alert(xhr.responseJSON.data);
				},
			});
		};
	});
})(jQuery);
