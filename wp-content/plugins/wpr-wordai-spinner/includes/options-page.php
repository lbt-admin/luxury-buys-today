<div class="wrap">
	<h2><?php echo __('WordAi API'); ?></h2>

	<form name="wordai-api-settings-form" id="wordai-api-settings-form" method="post" action="options.php">
		<?php
		settings_fields( 'wordai_api_options' );
		do_settings_sections( 'wpr-wordai-api' );
		?>
		<?php submit_button(); ?>
	</form>
</div>