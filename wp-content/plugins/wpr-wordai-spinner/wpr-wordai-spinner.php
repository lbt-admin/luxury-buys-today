<?php
/**
 * Plugin Name: Add WordAi API
 * Plugin URI: http://www.wpriders.com
 * Description: This plugin add WordAi API
 * Version: 1.0.0
 * Author: Ovidiu George Irodiu from WPRiders
 * Author URI: http://www.wpriders.com
 * License: GPL2
 */

if ( ! defined( 'ABSPATH' ) ) {
	header( 'Status: 403 Forbidden' );
	header( 'HTTP/1.1 403 Forbidden' );
	exit;
}



/**
 * Usage example
 *
 * Wpr_Wordai_api::wpr_wordai_api_call( 'The text that you would like WordAi to spin', 'Regular', array( 'returnspin' => 'true' ) );
 */
if ( ! class_exists( 'Wpr_Wordai_api' ) ) {
	/**
	 * Class Wpr_Wordai_api
	 */
	class Wpr_Wordai_api {
		static $option_name = 'wpr_wordai_api';
		static $namespace = 'wpr-wordai-api';
		public $settings_path = '';
		protected $options;
		protected static $wordai_email = null;
		protected static $wordai_pass = null;

		/**
		 * Wpr_Wordai_api constructor.
		 */
		function __construct() {
			$this->options       = get_option( self::$option_name );
			$this->settings_path = 'options-general.php?page=wpr-wordai-api-settings';
			add_action( 'admin_menu', array( &$this, 'wordai_api_admin_menu' ) );
			add_action( 'admin_init', array( &$this, 'admin_register_settings' ) );

			self::$wordai_email = $this->wpr_get_option( 'wordai_email' );
			self::$wordai_pass  = $this->wpr_get_option( 'wordai_pass' );
		}

		/**
		 * Sets default options upon activation
		 */
		public function activate() {
			if ( ! isset( $this->options['wordai_email'] ) ) {
				$this->options['wordai_email'] = '';
			}

			if ( ! isset( $this->options['wordai_pass'] ) ) {
				$this->options['wordai_pass'] = '';
			}

			update_option( self::$option_name, $this->options );

			wp_redirect( $this->settings_path );
		}

		/**
		 * Clean up after deactivation
		 */
		public function deactivate() {

		}

		/**
		 * Register admin menu
		 */
		public function wordai_api_admin_menu() {
			add_options_page( 'WordAi API', 'WordAi API', 'manage_options', self::$namespace, array( &$this, 'wordai_api_options_page' ) );
		}

		/**
		 * Template location
		 */
		public function wordai_api_options_page() {
			if ( ! current_user_can( 'manage_options' ) ) {
				wp_die( esc_html( 'You do not have sufficient permissions to access this page . ' ) );
			}

			include( plugin_dir_path( __FILE__ ) . 'includes/options-page.php' );
		}

		/**
		 * Register all the settings for the options page (Settings API)
		 *
		 * @uses register_setting()
		 * @uses add_settings_section()
		 * @uses add_settings_field()
		 */
		public function admin_register_settings() {
			register_setting( 'wordai_api_options', self::$option_name, array( &$this, 'validate_settings' ) );
			add_settings_section( 'wordai_api_settings', 'WordAi account', array( &$this, 'admin_section_code_settings' ), self::$namespace );
			add_settings_field( 'wordai_api_email', 'Email', array( &$this, 'admin_option_api_email' ), self::$namespace, 'wordai_api_settings' );
			add_settings_field( 'wordai_api_pass', 'Password', array( &$this, 'admin_option_api_pass' ), self::$namespace, 'wordai_api_settings' );
		}

		/**
		 * Validates user supplied settings and sanitizes the input
		 *
		 * @param $input
		 *
		 * @return mixed|void
		 */
		public function validate_settings( $input ) {
			$options = $this->options;

			if ( isset( $input['wordai_email'] ) ) {
				$wordai_email = trim( $input['wordai_email'] );

				if ( '' !== $wordai_email && is_email( $wordai_email ) ) {
					$options['wordai_email'] = $wordai_email;
				} else {
					add_settings_error( 'wordai_email', self::$namespace . '_wordai_api_email_error', 'Please enter a valid email', 'error' );
				}
			}

			if ( isset( $input['wordai_pass'] ) ) {
				$wordai_pass = trim( $input['wordai_pass'] );

				if ( '' !== $wordai_pass ) {
					$options['wordai_pass'] = $wordai_pass;
				} else {
					add_settings_error( 'wordai_pass', self::$namespace . '_wordai_api_pass_error', 'Please enter WordAi password', 'error' );
				}
			}

			return $options;
		}

		/**
		 * Output the description for WordAi
		 */
		public function admin_section_code_settings() {
			echo '<p>' . esc_html( 'Insert WordAi Email & Password bellow' ) . '</p>';
		}

		/**
		 * Output the input for the WordAi email
		 */
		public function admin_option_api_email() {
			echo sprintf( "<input type='text' name='%s[wordai_email]' size='20' value='%s' />", self::$option_name, esc_attr( $this->wpr_get_option( 'wordai_email' ) ) );
		}

		/**
		 * Output the input for the WordAi pass
		 */
		public function admin_option_api_pass() {
			echo sprintf( "<input type='password' name='%s[wordai_pass]' size='20' value='%s' />", self::$option_name, esc_attr( $this->wpr_get_option( 'wordai_pass' ) ) );
		}

		/**
		 * API call to WordAi
		 *
		 * @param $text
		 * @param $quality
		 * @param array $attrs
		 *
		 * @return mixed|string
		 */
		static function wpr_wordai_api_call( $text, $quality, $attrs = array() ) {

			if ( isset( $text ) && isset( $quality ) && isset( self::$wordai_email ) && isset( self::$wordai_pass ) ) {
				$text = urlencode( $text );

				$send_variables = "s=$text&quality=$quality";
				$send_variables .= "&email=" . self::$wordai_email;
				$send_variables .= "&hash=" . md5( substr( md5( self::$wordai_pass ), 0, 15 ) );

				if ( ! empty( $attrs ) ) {
					foreach ( $attrs as $key => $val ) {
						$send_variables .= "&$key=$val";
					}
				}

				// Send API data
				$ch = curl_init( 'http://wordai.com/users/turing-api.php' );
				curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );
				curl_setopt( $ch, CURLOPT_POST, 1 );
				curl_setopt( $ch, CURLOPT_POSTFIELDS, $send_variables );
				$result = curl_exec( $ch );
				curl_close( $ch );

				if ( strpos( $result, 'You are sending too many requests in parallel.' ) === false ) {
					return $result;
				} else {
					return '';
				}
			} else {
				return '';
			}
		}

		/**
		 * Lookup an option from the options array
		 *
		 * @param $key
		 *
		 * @return null
		 */
		public function wpr_get_option( $key ) {
			if ( isset( $this->options[ $key ] ) && '' != $this->options[ $key ] ) {
				return $this->options[ $key ];
			} else {
				return null;
			}
		}
	}
}

/**
 * Run
 */
$run_settings = new Wpr_Wordai_api();
register_activation_hook( __FILE__, array( $run_settings, 'activate' ) );
