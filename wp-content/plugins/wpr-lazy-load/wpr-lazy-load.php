<?php
/**
 * Plugin Name: Lazy load images for e-mail posts
 * Plugin URI: http://www.wpriders.com
 * Description: This plugin is specially made to load images form external sources and ease the page load.
 * Version: 1.0.0
 * Author: Mihai Irodiu from WPRiders
 * Author URI: http://www.wpriders.com
 * License: GPL2
 */

if ( ! defined( 'ABSPATH' ) ) { // You shall not pass \-/
	header( 'Status: 403 Forbidden' );
	header( 'HTTP/1.1 403 Forbidden' );
	exit;
}

define( 'WPR_LAZY_URI', plugins_url() . "/" . dirname( plugin_basename( __FILE__ ) ) ); // usage WPR_LAZY_URI.'/path to file...';
define( 'WPR_LAZY_PATH', plugin_dir_path( __FILE__ ) ); // usage WPR_LAZY_PATH.'folder/file...';

class WPR_Lazy_Load {

	function __construct() {
		add_action( 'wp_enqueue_scripts', array( &$this, 'wpr_enqueue_scripts' ), 10 );
	}

	function wpr_enqueue_scripts() {
		wp_enqueue_script( 'wpr-lzyld-img', WPR_LAZY_URI . '/assets/js/imagesloaded.pkgd.min.js', array( 'jquery' ), '1.9.7', true );
		wp_enqueue_script( 'wpr-lzyld', WPR_LAZY_URI . '/assets/js/jquery.lazyload.min.js', array( 'jquery' ), '1.9.7', true );
		wp_enqueue_script( 'wpr-lzyldopts', WPR_LAZY_URI . '/assets/js/lazyload-settings.js', array( 'wpr-lzyld', 'wpr-lzyld-img' ), filemtime( WPR_LAZY_PATH . '/assets/js/lazyload-settings.js' ), true );
	}
}

new WPR_Lazy_Load();
