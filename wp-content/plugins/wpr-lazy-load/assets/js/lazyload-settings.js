/**
 * Created by Mihai Irodiu on 14/03/17.
 */

(function ($) {
    $parent = $('body');
    $(document).ready(function(){


        $("img.lazyload").lazyload();
        if ($parent.length) {
        var images_list = $parent.find('img[data-original]');
        //console.log(images_list);
        if (images_list.length) {
            __wpr_load_images(images_list);
        }

        function __wpr_load_images(element) {
            element.lazyload({
                effect: "fadeIn",
                skip_invisible: false,
                event: "wpr_load_all",
                load: function () {
                    var $container = jQuery('.masonry-wrap');
                    console.log( imagesLoaded, console.log( $container ) );
                    $container.imagesLoaded(function () {
                    setTimeout(function () {
                        $container.masonry({
                            isAnimated: true
                        });
                    }, 3000);
                    });
                    $(this).addClass("wpr-loaded-image").css('opacity', '1');
                }
            });
            //setTimeout(function(){  $(".js-masonry-wrap").masonry( 'reloadItems' ); }, 1000);
        }

        $(document).ajaxComplete(function (event, xhr, settings) {

            if (settings.url.indexOf('/page') !== -1) {
                console.log('triggered');
                // settings.url == something you need
                // xhr.responseText response text
                var images_list = $parent.find('img[data-original]').not(".wpr-loaded-image");
                if (images_list.length) {
                    __wpr_load_images(images_list);
                    setTimeout(function () {
                        images_list.trigger("wpr_load_all")
                    }, 1000);
                }
            }
        });
    }
    })
    $(window).bind("load", function () {
        var timeout = setTimeout(function () {
            var images_list = $parent.find('img[data-original]');
            images_list.trigger("wpr_load_all")
        }, 300);
    });

})(jQuery);
