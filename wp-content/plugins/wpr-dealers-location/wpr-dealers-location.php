<?php
/**
 * Plugin Name: WPRiders Dealers location
 * Plugin URI: http://www.wpriders.com
 * Description: This plugin creates dealers location CPT
 * Version: 1.0.0
 * Author: Ovidiu George Irodiu from Wpriders
 * Author URI: http://www.wpriders.com
 * License: GPL2
 */
if (!defined('ABSPATH')) {
    die('You are not allowed to call this page directly.');
}

include(plugin_dir_path(__FILE__) . 'classes/create_tables.php');
include(plugin_dir_path(__FILE__) . 'classes/display_settings.php');
include(plugin_dir_path(__FILE__) . 'classes/ajax_calls.php');
include(plugin_dir_path(__FILE__) . 'classes/dealer_widget.php');
include(plugin_dir_path(__FILE__) . 'classes/ontraport.php');
include(plugin_dir_path(__FILE__) . 'classes/class-api.php');
include(plugin_dir_path(__FILE__) . 'inc/theme_hooks.php');

use InitiateWPR\Tables\Wpr_Create_Tables;

$CreateTables = new Wpr_Create_Tables();

use InitiateWPR\DisplaySettings\Wpr_Load_Settings;

$LoadSettings = new Wpr_Load_Settings();

use InitiateWPR\AjaxCalls\Wpr_Ajax_Calls;

$Wpr_Ajax_Calls = new Wpr_Ajax_Calls();

require_once(plugin_dir_path(__FILE__) . 'src/Ontraport.php');
require_once(plugin_dir_path(__FILE__) . 'src/hubspot/vendor/autoload.php');

use OntraportAPI\Ontraport;
// Call Object type
use OntraportAPI\ObjectType;

if (!class_exists('WPR_dealers_location')) {
    /**
     * Class WPR_dealers_location
     */
    class WPR_dealers_location
    {
        /**
         * Instance of the object
         *
         * @var \Object
         */
        private static $_instance = null;

        /**
         * @var string
         */
        protected $action = 'wpr_background_request';
        /**
         * Start time of current process.
         *
         * (default value: 0)
         *
         * @var int
         * @access protected
         */
        protected $start_time = 0;

        /**
         * Cron_hook_identifier
         *
         * @var mixed
         * @access protected
         */
        protected $cron_hook_identifier;

        /**
         * Cron_interval_identifier
         *
         * @var mixed
         * @access protected
         */
        protected $cron_interval_identifier;
        /**
         * Prefix
         *
         * (default value: 'wp')
         *
         * @var string
         * @access protected
         */
        protected $prefix = 'wp';

        /**
         * Identifier
         *
         * @var mixed
         * @access protected
         */
        protected $identifier;

        /**
         * @var int
         */
        var $radius;
        /**
         * Options
         * @var
         */
        var $options;
        /**
         * @var null
         */
        protected $_ontraport_api = null;
        /**
         * @var null
         */
        protected $_ontraport_key = null;
        /**
         * @var null
         */
        protected $_ontraport_app_id = null;
        /**
         * Ontraport options
         * @var string
         */
        var $options_name = 'ontraport_api_options';
        /**
         * @var array
         */
        var $ontraport_tag = array();

        /**
         * @var null
         */
        protected $_hubspot_api = null;
        /**
         * @var null
         */
        protected $_hubspot_key = null;
        /**
         * @var string
         */
        var $hubspot_options_name = 'hubspot_api_options';
        /**
         * @var null
         */
        public $_error_report = null;

        /**
         * Setup singleton instanc
         *
         * @return  \Object
         */
        public static function get_instance() {
            if ( is_null( self::$_instance ) ) {
                self::$_instance = new static();
            }

            return self::$_instance;
        }

        /**
         * WPR_dealers_location constructor.
         */
        private function __construct()
        {
            $this->radius = 12600;
            $this->options = get_option($this->options_name);

            $this->hubspot_options = get_option($this->hubspot_options_name);
            $this->_hubspot_key = $this->hubspot_options['hubspot_api_key'];
            $this->wp_hubspot_api_call($this->_hubspot_key);

            add_shortcode('wpr-dealer-info', array(&$this, 'wpr_get_dealer_info'));

            add_action('wp_ajax_wpr_my_offer_user', array(&$this, 'wpr_my_offer_user'));
            add_action('wp_ajax_wpr_map_location_click', array(&$this, 'wpr_map_location_click'));
            add_action('wp_ajax_nopriv_wpr_map_location_click', array(&$this, 'wpr_map_location_click'));
            add_action('wp_ajax_nopriv_wpr_register_click', array(&$this, 'wpr_register_click'));

            add_shortcode('wpr-ba-offers', array(&$this, 'wpr_get_ba_offers'));
            add_action('pre_get_posts', array(&$this, 'wpr_hide_show_ba_offers_by_default'), 20, 1);
            add_filter('webpigment_get_user_category_dealer', array( &$this, 'get_user_category_dealer' ) );
            // add_action( 'wp_footer', array( &$this, 'wpr_add_footer_script' ) );

            add_action('admin_menu', array(&$this, 'ontraport_admin_menu'));
            add_action('admin_init', array(&$this, 'admin_register_settings'));
            add_action('gform_user_registered', array(&$this, 'wpr_ontraport_user_register'), 50, 4);
            add_action('oa_social_login_action_after_user_insert', array(&$this, 'wpr_ontraport_social_user_register'), 10, 2);
            // add_action( 'wsl_hook_process_login_after_wp_insert_user', array( &$this, 'wpr_ontraport_social_user_register' ), 50, 3 );
            // add_action( 'wsl_hook_process_login_before_wp_safe_redirect', array( &$this, 'wpr_ontraport_social_user_login' ), 50, 4 );
            add_action('profile_update', array(&$this, 'wpr_ontraport_profile_update'), 20, 2);
            add_action('wpmu_delete_user', array(&$this, 'wpr_user_delete_clear_social_login'));
            add_action('delete_user', array(&$this, 'wpr_user_delete_clear_social_login'));

            add_action('wp_ajax_wpr_ontraport_display_logs', array(&$this, 'wpr_ontraport_display_logs'));

            $this->ontraport_tag = $this->get_option('ontraport_categories_associated');

            add_action('admin_menu', array(&$this, 'hubspot_admin_menu'));

            add_action('wp_ajax_wpr_hubspot_display_logs', array(&$this, 'wpr_hubspot_display_logs'));

            add_action('rest_api_init', array(&$this, 'register_rest_routes'));

            add_filter('wp_footer', array(&$this, 'wpr_user_cta_clicked'));

            // DO THE CRON
            $this->identifier = $this->prefix . '_' . $this->action;

            $this->cron_hook_identifier = $this->identifier . '_cron';
            $this->cron_interval_identifier = $this->identifier . '_cron_interval';

            add_action($this->cron_hook_identifier, array($this, 'handle_cron_healthcheck'));
            add_filter('cron_schedules', array($this, 'schedule_cron_healthcheck'));

//			add_action( 'init', array( $this, 'wpr_rerun_abc_query' ) );
//			add_action( 'init', array( $this, 'wpr_test_ontraport' ) );

            add_action('wp_ajax_wpr_choose_the_brands', array(&$this, 'wpr_choose_the_brands'));
            add_action('wp_ajax_nopriv_wpr_choose_the_brands', array(&$this, 'wpr_choose_the_brands'));
            add_shortcode('wpr-single-offer-brands', array(&$this, 'wpr_single_offer_brands'));

            add_action('widgets_init', array(&$this, 'wpr_dealer_widget'));
            add_action('wp_ajax_wpr_load_dealer_display', array(&$this, 'wpr_load_dealer_display'));
            add_action('wp_ajax_nopriv_wpr_load_dealer_display', array(&$this, 'wpr_load_dealer_display'));

            add_action('wpr_obtain_dealer_data_info_not_logged_in', array(&$this, 'wpr_obtain_dealer_data_info_not_logged_in'), 10, 3);
            add_action('wpr_obtain_dealer_data_info', array(&$this, 'wpr_obtain_dealer_data_info'), 10, 2);

            register_deactivation_hook( __FILE__, array( $this, 'on_deactivate' ) );
        }

        public function get_user_category_dealer( $category_id ) {
	        return $this->wpr_closest_dealer_id($category_id);
        }
        /**
         * On plugin deactivation
         *
         * @return  void
         */
        public function on_deactivate() {
            $next_run = wp_next_scheduled( 'wpr_ontarport_dealerships_sync' );

            if ( $next_run ) {
                wp_unschedule_event( $next_run, 'wpr_ontarport_dealerships_sync' );
            }
        }

        /**
         * Display Not Logged in Dealer Info
         *
         * @param $category_id
         * @param $loc_type
         * @param $got_dealer
         * @param $output
         */
        function wpr_obtain_dealer_data_info_not_logged_in($category_id, $loc_type, $got_dealer, $dealer = array() )
        {
            $output = 'No Dealer Found';

            $testing = get_category( $category_id );
            $term_image_id = get_term_meta($category_id, 'category-image-id', true);
            $term_attachment =get_the_attachment_link($term_image_id);
            $term_attachment_url =wp_get_attachment_url($term_image_id);
            //$dealer_template = 'display-dealer-info-not-logged-in.php';
            $dealer_template = 'dealer-block-single-new.php';
            if ('archive' == $loc_type) {
                $dealer_template = 'dealer-block-archive-new.php';
            }

            $category_name = get_cat_name($category_id);

            $catalog_url = get_term_meta($category_id, 'wpr_brand_catalog', true);
            $image_id = get_term_meta($category_id, 'category-image-id', true);
            $brand_image = wp_get_attachment_image($image_id, 'large');

            $wpr_brand_image_url = get_term_meta($category_id, 'wpr_brand_image_url', true);
            $wpr_brand_image_link = get_term_meta($category_id, 'wpr_brand_image_link', true);
            $wpr_brand_link_text = get_term_meta($category_id, 'wpr_brand_link_text', true);

            $display_brand_image = '';

            if ($wpr_brand_image_url) {
                if ($wpr_brand_image_link) {
                    $display_brand_image .= '<a href="' . esc_url($wpr_brand_image_link) . '" target="_blank">';
                }
                $display_brand_image .= '<img src="' . esc_url($wpr_brand_image_url) . '" alt="" class="attachment-large size-large" />';
                if ($wpr_brand_link_text) {
                    $display_brand_image .= '<div>' . esc_attr($wpr_brand_link_text) . '</div>';
                }
                if ($wpr_brand_image_link) {
                    $display_brand_image .= '</a>';
                }

                $brand_image = $display_brand_image;
            }
		  if ( empty( $dealer ) ) {
               $dealer = $this->wpr_get_closest_dealer_data( $category_id );
		  }

            if ( ! empty( $dealer ) ) {
                $store_address = '';
                $google_map = '';
                $got_dealer = true;
                $output = '<div id="wpr-dealer-location" data-deal="' . $dealer['id'] . '" data-loc="' . $category_id . '" class="widget-content wertyuiop">';
                ob_start();
                include( plugin_dir_path( __FILE__ ) . 'template/' . $dealer_template );
                $incl_output = ob_get_contents();
                ob_end_clean();
                $output.= $incl_output;
                $output .= '</div>';
            }

            echo $output;
        }

        /**
         * Display Logged in Dealer Info
         *
         * @param $category_id
         * @param $output
         */
        function wpr_obtain_dealer_data_info($category_id, $output)
        {
            $category_name = get_cat_name($category_id);
            $catalog_url = get_term_meta($category_id, 'wpr_brand_catalog');
            $image_id = get_term_meta($category_id, 'category-image-id', true);
            $brand_image = wp_get_attachment_image($image_id, 'large');

            $wpr_brand_image_url = get_term_meta($category_id, 'wpr_brand_image_url', true);
            $wpr_brand_image_link = get_term_meta($category_id, 'wpr_brand_image_link', true);
            $wpr_brand_link_text = get_term_meta($category_id, 'wpr_brand_link_text', true);

            $display_brand_image = '';
            if ($wpr_brand_image_url) {
                if ($wpr_brand_image_link) {
                    $display_brand_image .= '<a href="' . esc_url($wpr_brand_image_link) . '" target="_blank">';
                }
                $display_brand_image .= '<img src="' . esc_url($wpr_brand_image_url) . '" alt="" class="attachment-large size-large" />';
                $display_brand_image .= '<div>' . esc_attr($category_name) . '</div>';
                if ($wpr_brand_link_text) {
                    $display_brand_image .= '<div class="wpr-display-social-url">' . esc_attr($wpr_brand_link_text) . '</div>';
                }
                if ($wpr_brand_image_link) {
                    $display_brand_image .= '</a>';
                }

                $brand_image = $display_brand_image;
            }

            // Template call
            $dealer_template = 'display-dealer-info.php';
            if (is_archive()) {
                $dealer_template = 'display-dealer-info-archive.php';
            }

            switch_to_blog(2);
            $args = array(
                'post_type' => 'wpr_dealer_info',
                'status' => 'publish',
                'meta_key' => 'wpr_category_id',
                'meta_value' => $category_id,
                'posts_per_page' => 1,
            );
            remove_action('pre_get_posts', array($this, 'wpr_hide_show_ba_offers_by_default'), 20);
            add_filter('posts_clauses', array($this, 'wpr_display_closest_dealer'), 10, 2);
            $query = new WP_Query($args);
            //echo $query->request;
            $posts = $query->posts;

            if ($query->have_posts()) {
                $store_address = '';
                $google_map = '';
                $got_dealer = true;
                //$output .= '<h1 class="widget-title">' . $category_name . '</h1>';
                $output .= '<div id="wpr-dealer-location" data-deal="' . $posts[0]->ID . '" data-loc="' . $category_id . '" class="widget-content">';
                while ($query->have_posts()) {
                    $query->the_post();
                    $dealer_fields = get_fields(get_the_ID());
                    $dealer_template = 'prime_template.php';
                    require(plugin_dir_path(__FILE__) . 'template/' . $dealer_template);
                }
                $output .= '</div>';
            }

            wp_reset_postdata();
            remove_filter('posts_clauses', array($this, 'wpr_display_closest_dealer'));

            if (false === $got_dealer) {
                add_filter('posts_clauses', array($this, 'wpr_display_new_york_dealer'), 10, 2);
                $query = new WP_Query($args);
                $posts = $query->posts;

                if ($query->have_posts()) {
                    $store_address = '';
                    $google_map = '';
                    $output .= '<div id="wpr-dealer-location" data-deal="' . $posts[0]->ID . '" data-loc="' . $category_id . '" class="widget-content">';
                    while ($query->have_posts()) {
                        $query->the_post();
                        $dealer_fields = get_fields(get_the_ID());
                        $dealer_template = 'prime_template';
                        require(plugin_dir_path(__FILE__) . 'template/' . $dealer_template);
                    }
                    $output .= '</div>';
                } else {
                    $output .= '<h1 class="widget-title">' . $category_name . '</h1>';
                    $output .= '';
                }

                wp_reset_postdata();
                remove_filter('posts_clauses', array($this, 'wpr_display_new_york_dealer'));
                add_action('pre_get_posts', array($this, 'wpr_hide_show_ba_offers_by_default'), 20, 1);
            }
            restore_current_blog();

            echo $output;
        }

        /**
         * Prospect Clicks CTA button
         */
        function wpr_user_cta_clicked()
        {

            if (isset($_COOKIE['wpr_button_clicked']) &&
                '' != $_COOKIE['wpr_button_clicked'] &&
                isset($_COOKIE['wpr_user_click']) &&
                '' != $_COOKIE['wpr_user_click'] &&
                is_user_logged_in() && !is_page(array('complete-your-profile'))
            ) {

                $get_data = json_decode(stripslashes($_COOKIE['wpr_user_click']), true);
                ?>
                <script>
                    jQuery(document).ready(function ($) {
                        var wprBtn;

                        <?php
                        if($get_data['to_do'] && !empty($get_data['to_do'])) {

                        if(is_single()) {
                        if ( 'favorite' == $get_data['to_do'] ) { ?>

                        wprBtn = $('.simplefavorite-button[data-postid="<?php echo $get_data['offer_id']; ?>"]');
                        <?php } else { ?>

                        wprBtn = $('.wpr-single-offer-buttons[data-do="<?php echo $get_data['to_do']; ?>"]');
                        <?php } ?>
                        <?php } else {
                        if($get_data['offer_id'] && !empty($get_data['offer_id'])) {
                        if ( 'favorite' == $get_data['to_do'] ) { ?>

                        wprBtn = $('.simplefavorite-button[data-postid="<?php echo $get_data['offer_id']; ?>"]');
                        <?php } else { ?>

                        wprBtn = $('.entry-header[data-deal-id="<?php echo $get_data['offer_id']; ?>"] a[data-do="<?php echo $get_data['to_do']; ?>"]');
                        <?php }
                        } else { ?>
                        wprBtn = $('.wpr-dealer-loc-button a[data-do="<?php echo $get_data['to_do']; ?>"]');
                        <?php } }  ?>

                        setTimeout(
                            function () {
                                wprBtn.trigger("click");

                                Cookies.remove('wpr_user_click', {path: ''});
                                Cookies.remove('wpr_button_clicked', {path: ''});
                                <?php if ( 'favorite' == $get_data['to_do'] ) { ?>
                                Cookies.set('wpr_favorite_click', '1', {
                                    expires: 7,
                                    path: ''
                                });
                                <?php } ?>
                            }, 2000
                        );
                        <?php } ?>
                    });
                </script>
                <?php

                // Unset cookies
                unset($_COOKIE['wpr_button_clicked']);
                unset($_COOKIE['wpr_user_click']);
                unset($_COOKIE['wpr_user_brand_page']);
                setcookie('wpr_button_clicked', '', time() - (15 * 60), COOKIEPATH, COOKIE_DOMAIN);
                setcookie('wpr_user_click', '', time() - (15 * 60), COOKIEPATH, COOKIE_DOMAIN);
                setcookie('wpr_user_brand_page', '', time() - (15 * 60), COOKIEPATH, COOKIE_DOMAIN);
            }

            if (!is_user_logged_in() && is_page(array('login', 'register'))) {

                ?>
                <script>
                    jQuery(document).ready(function ($) {
                        if ($('.wp-social-login-provider').length) {
                            if (Cookies.get('wpr_user_click')) {
                                var getCoo = jQuery.parseJSON(Cookies.get('wpr_user_click'));

                                $('a.wp-social-login-provider').each(function () {
                                    var value = $(this).attr('href');
                                    var newURL = value.replace(/([?&]redirect_to)=([^#&]*)/g, '$1=' + getCoo.current_url);
                                    $(this).attr('href', newURL);
                                });
                            }
                        }
                    });
                </script>
                <?php
            }

            if (is_page(array('complete-your-profile')) && is_user_logged_in()) {
                update_user_meta(get_current_user_id(), 'wpr_complete_data', 1);

                ?>
                <script>
                    jQuery(document).ready(function ($) {
                        if ($('form.wpr-complete-data').length) {
                            if (Cookies.get('wpr_user_click')) {
                                var getCoo = jQuery.parseJSON(Cookies.get('wpr_user_click'));
                                $('form.wpr-complete-data').find('.gform_hidden input').val(getCoo.current_url);
                            } else {
                                $('form.wpr-complete-data').find('.gform_hidden input').val('<?php echo esc_url(home_url()); ?>');
                            }
                        }
                    });
                </script>
                <?php
            }


            if ('wpr_dealer_info' == get_post_type()) { ?>
                <script>
                    jQuery(document).ready(function ($) {
                        if ($('div#comments').length) {
                            $('div#comments').remove();
                        }
                    });
                </script>
            <?php }
        }

        /**
         * Register Dealer Widget
         */
        function wpr_dealer_widget()
        {
            register_widget('wpr_dealer_widget');
        }

        /**
         * Display dealer info
         */
        function wpr_load_dealer_display()
        {
            check_ajax_referer('wpr-dealer-location', 'nonce');
            $output = '';
            $got_dealer = false;
            $category_id = absint($_POST['onpage']);
            $loc_type = esc_attr($_POST['loctype']);
            if ($category_id) {
                do_action('wpr_obtain_dealer_data_info_not_logged_in', $category_id, $loc_type, $got_dealer);
            }

            echo $output;

            wp_die();
        }

        /**
         * Save filter
         */
        function wpr_choose_the_brands()
        {
            if (!isset($_POST['nonce'])) {
                return;
            }

            if (!wp_verify_nonce($_POST['nonce'], 'wpr-dealer-location')) {
                return;
            }

            parse_str($_POST['form'], $brands);

            if (!empty($brands['post_category'])) {
                $exclude_categories = array('36', '37', '38', '39');
                $save_brands = array_diff($brands['post_category'], $exclude_categories);

                $to_cookie = array(
                    'brands' => $save_brands,
                    'url' => $_POST['current_url'],
                );
                $selected_brands = json_encode($to_cookie);

                if (is_user_logged_in()) {
                    $redirect_to_url = get_permalink(get_page_by_path('my-offers'));
                    update_user_meta(get_current_user_id(), 'wpr_selected_brands', $brands['post_category']);
                } else {
                    $redirect_to_url = 1;
                    setcookie('wpr_selected_brands', $selected_brands, 30 * DAYS_IN_SECONDS, COOKIEPATH, COOKIE_DOMAIN);
                }

                $redirect = array(
                    'url' => $redirect_to_url,
                );

                echo wp_json_encode($redirect);
            }
            wp_die();
        }

        /**
         * Display brands filter
         */
        function wpr_single_offer_brands($atts)
        {
            ob_start();
            /* Set up the default arguments. */
            $defaults = array();

            /* Parse the arguments. */
            extract(shortcode_atts($defaults, $atts));

            get_template_part('partials/content', 'panel');
            get_template_part('partials/content', 'filter-button');

            return ob_get_clean();
        }

        /**
         * Delete user data from social login table
         *
         * @param $user_id
         */
        function wpr_user_delete_clear_social_login($user_id)
        {
            global $wpdb;
            if (!is_numeric($user_id)) {
                return false;
            }

            $user_id = (int)$user_id;

            $wpdb->query(
                $wpdb->prepare(
                    "
                        DELETE FROM `{$wpdb->prefix}wslusersprofiles`
                        WHERE `user_id` = %d
                    ",
                    $user_id
                )
            );

            $wpdb->query(
                $wpdb->prepare(
                    "
                        DELETE FROM `{$wpdb->prefix}2_wslusersprofiles`
                        WHERE `user_id` = %d
                    ",
                    $user_id
                )
            );
        }

        /**
         * Contact Record
         *
         * @param $_ontraport_key
         * @param $_ontraport_app_id
         */
        function wp_ontraport_api_call($type = '')
        {
            $ontraport_data = get_option($this->options_name, true);
            $this->_ontraport_api = new OntraportAPI\Ontraport($ontraport_data['ontraport' . $type . '_api_id'], $ontraport_data['ontraport' . $type . '_api_key']);
        }

        /**
         * Add ontraport settings page
         */
        function ontraport_admin_menu()
        {
            add_options_page('Ontraport API', 'Ontraport API', 'manage_options', 'wp-ontraport-api', array(&$this, 'ontraport_api_options_page'));
        }

        /**
         *
         */
        function ontraport_api_options_page()
        {
            if (!current_user_can('manage_options')) {
                wp_die(esc_html('You do not have sufficient permissions to access this page . '));
            }

            include(plugin_dir_path(__FILE__) . 'inc/options-page.php');
        }

        /**
         * @param $_hubspot_key
         */
        function wp_hubspot_api_call($key = null)
        {
            if (empty($key)) {
                return;
            }
            $this->_hubspot_api = SevenShores\Hubspot\Factory::create($key, null, array('http_errors' => false));
        }

        /**
         * Add HubSpot settings page
         */
        function hubspot_admin_menu()
        {
            add_options_page('HubSpot API', 'HubSpot API', 'manage_options', 'wp-hubspot-api', array(&$this, 'hubspot_api_options_page'));
        }

        /**
         *
         */
        function hubspot_api_options_page()
        {
            if (!current_user_can('manage_options')) {
                wp_die(esc_html('You do not have sufficient permissions to access this page . '));
            }

            include(plugin_dir_path(__FILE__) . 'inc/hubspot-options-page.php');
        }

        /**
         * Register all the settings for the options page (Settings API)
         *
         * @uses register_setting()
         * @uses add_settings_section()
         * @uses add_settings_field()
         */
        public function admin_register_settings()
        {
            register_setting($this->options_name, $this->options_name, array(&$this, 'validate_settings'));
            add_settings_section('ontraport_api_settings', 'Ontraport account', array(&$this, 'admin_section_code_settings'), 'wp-ontraport-api');

            // Contact Record
            add_settings_field('ontraport_api_key', 'Contact Record API Key', array(&$this, 'admin_option_sync_key'), 'wp-ontraport-api', 'ontraport_api_settings');
            add_settings_field('ontraport_api_id', 'Contact Record API ID', array(&$this, 'admin_option_sync_app_id'), 'wp-ontraport-api', 'ontraport_api_settings');
            // REQ_PRICE
            add_settings_field('ontraport_rp_api_key', 'REQ_PRICE API Key', array(&$this, 'admin_option_rp_sync_key'), 'wp-ontraport-api', 'ontraport_api_settings');
            add_settings_field('ontraport_rp_api_id', 'REQ_PRICE API ID', array(&$this, 'admin_option_rp_sync_app_id'), 'wp-ontraport-api', 'ontraport_api_settings');
            // REQ_APPT
            add_settings_field('ontraport_app_api_key', 'REQ_APPT API Key', array(&$this, 'admin_option_app_sync_key'), 'wp-ontraport-api', 'ontraport_api_settings');
            add_settings_field('ontraport_app_api_id', 'REQ_APPT API ID', array(&$this, 'admin_option_app_sync_app_id'), 'wp-ontraport-api', 'ontraport_api_settings');
            // REQ_INFO
            add_settings_field('ontraport_info_api_key', 'REQ_INFO API Key', array(&$this, 'admin_option_info_sync_key'), 'wp-ontraport-api', 'ontraport_api_settings');
            add_settings_field('ontraport_info_api_id', 'REQ_INFO API ID', array(&$this, 'admin_option_info_sync_app_id'), 'wp-ontraport-api', 'ontraport_api_settings');
            // REQ_CALL
            add_settings_field('ontraport_call_api_key', 'REQ_CALL API Key', array(&$this, 'admin_option_call_sync_key'), 'wp-ontraport-api', 'ontraport_api_settings');
            add_settings_field('ontraport_call_api_id', 'REQ_CALL API ID', array(&$this, 'admin_option_call_sync_app_id'), 'wp-ontraport-api', 'ontraport_api_settings');

            add_settings_section('ontraport_assign_categories', 'Assign categories to a Ontraport TAG', array(&$this, 'admin_section_assign_categories'), 'wp-ontraport-api-assign-categories');
            add_settings_field(
                'ontraport_assign_categories', 'Assign categories', array(
                &$this,
                'admin_option_assign_categories',
            ), 'wp-ontraport-api-assign-categories', 'ontraport_assign_categories'
            );

            register_setting($this->hubspot_options_name, $this->hubspot_options_name, array(&$this, 'validate_hubspot_settings'));
            add_settings_section('hubspot_api_settings', 'Hubspot account', function () {
                echo '<p>' . esc_html('Insert HubSpot API Key below') . '</p>';
            }, 'wp-hubspot-api');
            add_settings_field('hubspot_api_key', 'API Key', function () {
                echo sprintf("<input type='text' name='hubspot_api_options[hubspot_api_key]' size='20' value='%s' />", esc_attr($this->get_hubspot_option('hubspot_api_key')));
            }, 'wp-hubspot-api', 'hubspot_api_settings');
        }

        /**
         * Validates user supplied settings and sanitizes the input
         *
         * @param $input
         *
         * @return mixed|void
         */
        public function validate_settings($input)
        {
            $options = $this->options;

            if (isset($input['ontraport_api_key'])) {
                $options['ontraport_api_key'] = trim($input['ontraport_api_key']);
            }

            if (isset($input['ontraport_api_id'])) {
                $options['ontraport_api_id'] = trim($input['ontraport_api_id']);
            }

            if (isset($input['ontraport_rp_api_key'])) {
                $options['ontraport_rp_api_key'] = trim($input['ontraport_rp_api_key']);
            }

            if (isset($input['ontraport_rp_api_id'])) {
                $options['ontraport_rp_api_id'] = trim($input['ontraport_rp_api_id']);
            }

            if (isset($input['ontraport_app_api_key'])) {
                $options['ontraport_app_api_key'] = trim($input['ontraport_app_api_key']);
            }

            if (isset($input['ontraport_app_api_id'])) {
                $options['ontraport_app_api_id'] = trim($input['ontraport_app_api_id']);
            }

            if (isset($input['ontraport_info_api_key'])) {
                $options['ontraport_info_api_key'] = trim($input['ontraport_info_api_key']);
            }

            if (isset($input['ontraport_info_api_id'])) {
                $options['ontraport_info_api_id'] = trim($input['ontraport_info_api_id']);
            }

            if (isset($input['ontraport_call_api_key'])) {
                $options['ontraport_call_api_key'] = trim($input['ontraport_call_api_key']);
            }

            if (isset($input['ontraport_call_api_id'])) {
                $options['ontraport_call_api_id'] = trim($input['ontraport_call_api_id']);
            }

            if (isset($input['list_ontraport_tags'])) {
                $campaigns = array();
                foreach ($input as $key => $item) {
                    if (empty($item)) {
                        continue;
                    }
                    $campaigns[$key] = $item;
                }
                $options['ontraport_categories_associated'] = $campaigns;
            }

            return $options;
        }

        /**
         * Validates user supplied settings and sanitizes the input
         *
         * @param $input
         *
         * @return mixed|void
         */
        public function validate_hubspot_settings($input)
        {
            $options = $this->hubspot_options;

            if (isset($input['hubspot_api_key'])) {
                $options['hubspot_api_key'] = trim($input['hubspot_api_key']);
            }

            return $options;
        }

        /**
         * Output the description for the Drip token
         */
        public function admin_section_code_settings()
        {
            echo '<p>' . esc_html('Insert Ontraport API Key & Account ID below') . '</p>';
        }

        /**
         * Contact Record
         * Output the input for the Drip token option
         */
        public function admin_option_sync_key()
        {
            echo sprintf("<input type='text' name='ontraport_api_options[ontraport_api_key]' size='20' value='%s' />", esc_attr($this->get_option('ontraport_api_key')));
        }

        /**
         * Contact Record
         * Output Drip Account ID
         */
        public function admin_option_sync_app_id()
        {
            echo sprintf("<input type='text' name='ontraport_api_options[ontraport_api_id]' size='20' value='%s' />", esc_attr($this->get_option('ontraport_api_id')));
        }

        /**
         * REQ_PRICE
         * Output the input for the Drip token option
         */
        public function admin_option_rp_sync_key()
        {
            echo sprintf("<input type='text' name='ontraport_api_options[ontraport_rp_api_key]' size='20' value='%s' />", esc_attr($this->get_option('ontraport_rp_api_key')));
        }

        /**
         * REQ_PRICE
         * Output Drip Account ID
         */
        public function admin_option_rp_sync_app_id()
        {
            echo sprintf("<input type='text' name='ontraport_api_options[ontraport_rp_api_id]' size='20' value='%s' />", esc_attr($this->get_option('ontraport_rp_api_id')));
        }

        /**
         * REQ_APPT
         * Output the input for the Drip token option
         */
        public function admin_option_app_sync_key()
        {
            echo sprintf("<input type='text' name='ontraport_api_options[ontraport_app_api_key]' size='20' value='%s' />", esc_attr($this->get_option('ontraport_app_api_key')));
        }

        /**
         * REQ_APPT
         * Output Drip Account ID
         */
        public function admin_option_app_sync_app_id()
        {
            echo sprintf("<input type='text' name='ontraport_api_options[ontraport_app_api_id]' size='20' value='%s' />", esc_attr($this->get_option('ontraport_app_api_id')));
        }

        /**
         * REQ_INFO
         * Output the input for the Drip token option
         */
        public function admin_option_info_sync_key()
        {
            echo sprintf("<input type='text' name='ontraport_api_options[ontraport_info_api_key]' size='20' value='%s' />", esc_attr($this->get_option('ontraport_info_api_key')));
        }

        /**
         * REQ_INFO
         * Output Drip Account ID
         */
        public function admin_option_info_sync_app_id()
        {
            echo sprintf("<input type='text' name='ontraport_api_options[ontraport_info_api_id]' size='20' value='%s' />", esc_attr($this->get_option('ontraport_info_api_id')));
        }

        /**
         * REQ_CALL
         * Output the input for the Drip token option
         */
        public function admin_option_call_sync_key()
        {
            echo sprintf("<input type='text' name='ontraport_api_options[ontraport_call_api_key]' size='20' value='%s' />", esc_attr($this->get_option('ontraport_call_api_key')));
        }

        /**
         * REQ_CALL
         * Output Drip Account ID
         */
        public function admin_option_call_sync_app_id()
        {
            echo sprintf("<input type='text' name='ontraport_api_options[ontraport_call_api_id]' size='20' value='%s' />", esc_attr($this->get_option('ontraport_call_api_id')));
        }

        /**
         * Assign categories to a Ontraport TAG
         */
        public function admin_section_assign_categories()
        {
            echo '<p>' . esc_html('Assign categories to a Ontraport TAG') . '</p>';
        }

        /**
         * Add tags dropdowns
         */
        function admin_option_assign_categories()
        {
            $response = $this->wpr_retrive_all_tags();

            if (!empty($response)) {
                $get_campaign_assoc = $this->get_option('ontraport_categories_associated');
                $categories = get_categories('exclude=1');

                $display_select = <<<SELECT
<input type="hidden" name="ontraport_api_options[list_ontraport_tags]" />
<table>
SELECT;
                foreach ($categories as $category) {
                    $display_select .= <<<SELECT
<tr><td><label>{$category->name}</label></td>
<td><select name="ontraport_api_options[{$category->term_id}]" class="select2">
<option value="">None</option>
SELECT;
                    foreach ($response as $tag) {
                        $selected = ($tag['tag_id'] === $get_campaign_assoc[$category->term_id]) ? ' selected' : '';
                        $display_select .= <<<SELECT
<option value="{$tag['tag_id']}"{$selected}>{$tag['tag_name']}</option>
SELECT;
                    }

                    $display_select .= <<<SELECT
</select></td></tr>
SELECT;
                }
                $display_select .= <<<SELECT
</table>
SELECT;
                echo $display_select;
            }
        }

        function wpr_test_ontraport()
        {

            $request_params = array(
                'objectID' => ObjectType::CONTACT,
                'firstname' => 'Test',
                'lastname' => 'Ontraport',
                'email' => 'wprlcontraportprospect6@mailinator.com',
                'zip' => '10007',
                'sms_number' => '(021) 000-0000',
                'f1529' => 'wprlcontraportprospect6',
                'f1739' => 'https://secure.gravatar.com/avatar/a050f573366f1b9f4a908d3e6c7d2008?s=150&d=mm&r=g',
            );

            $this->wp_ontraport_api_call();

            $response = $this->_ontraport_api->object()->saveOrUpdate($request_params);
            echo '<pre>';
            print_r($response);
            echo '</pre>';
        }

        /**
         * Send data to ontraport when new user registers
         *
         * @param $user_id
         */
        function wpr_ontraport_user_register($user_id, $config, $entry, $password)
        {
            if ($user_id) {
                $user_info = get_user_by('id', $user_id);
                $user_zip_code = get_user_meta($user_id, 'zip_code', true);
                $user_phone = get_user_meta($user_id, 'phone', true);

                if (in_array('wpr_prospect', $user_info->roles)) {
                    $user_avatar_url = get_user_meta($user_id, 'wpr_user_avatar', true);
                    $user_meta_picture = get_user_meta($user_id, 'oa_social_login_user_picture', true);

                    $user_avatar = get_avatar_url($user_id);
                    if ($user_meta_picture) {
                        $user_avatar = $user_meta_picture;
                    }
                    if ($user_avatar_url) {
                        $user_avatar = $user_avatar_url;
                    }

                    $request_params = array(
                        'objectID' => ObjectType::CONTACT,
                        'firstname' => esc_attr($user_info->first_name),
                        'lastname' => esc_attr($user_info->last_name),
                        'email' => esc_attr($user_info->user_email),
                        'zip' => esc_attr($user_zip_code),
                        'sms_number' => esc_attr($user_phone),
                        'f1529' => esc_attr($user_info->user_login),
                        'f1739' => esc_url_raw($user_avatar),
                    );

                    $this->wp_ontraport_api_call();
                    $response = $this->_ontraport_api->object()->saveOrUpdate($request_params);
                    $this->wpr_write_ontraport_log('Prospect register', $response);

                    if ($response) {
                        $response_parse = json_decode($response, true);
                        update_user_meta($user_id, 'wpr_ontraport_id', $response_parse['data']['id']);

                        $add_tags = array(
                            'objectID' => ObjectType::CONTACT,
                            'ids' => absint($response_parse['data']['id']),
                            'add_list' => '2,55',
                        );
                        $response_tags = $this->_ontraport_api->object()->addTag($add_tags);
                        $this->wpr_write_ontraport_log('Prospect register add tags', $response_tags);
                    }
                }
            }
        }

        /**
         * Update user data
         *
         * @param $user_id
         * @param $provider
         * @param $hybridauth_user_profile
         * @param $redirect_to
         */
        function wpr_ontraport_social_user_login($user_id, $provider, $hybridauth_user_profile, $redirect_to)
        {
            if ($user_id) {
                if ('' != $hybridauth_user_profile->phone) {
                    update_user_meta($user_id, 'phone', esc_attr($hybridauth_user_profile->phone));
                }

                if ('' != $hybridauth_user_profile->zip) {
                    update_user_meta($user_id, 'zip_code', esc_attr($hybridauth_user_profile->zip));
                }
            }
        }

        /**
         * Send data to ontraport when new user registers with social login
         *
         * @param $user_data
         * @param $identity
         */
        function wpr_ontraport_social_user_register($user_data, $identity)
        {
            if ($user_data) {
                $user_id = absint($user_data->ID);
                if ('' != $identity->phone) {
                    update_user_meta($user_id, 'phone', esc_attr($identity->phone));
                }

                if ('' != $identity->zip) {
                    update_user_meta($user_id, 'zip_code', esc_attr($identity->zip));
                }

                $user_zip_code = get_user_meta($user_id, 'zip_code', true);
                $user_phone = get_user_meta($user_id, 'phone', true);
                $user_avatar_url = get_user_meta($user_id, 'wpr_user_avatar', true);
                $user_meta_picture = get_user_meta($user_id, 'oa_social_login_user_picture', true);

                $user_avatar = get_avatar_url($user_id);
                if ($user_meta_picture) {
                    $user_avatar = $user_meta_picture;
                }
                if ($user_avatar_url) {
                    $user_avatar = $user_avatar_url;
                }

                if (in_array('wpr_prospect', $user_data->roles)) {
                    $request_params = array(
                        'objectID' => ObjectType::CONTACT,
                        'firstname' => esc_attr($user_data->first_name),
                        'lastname' => esc_attr($user_data->last_name),
                        'email' => esc_attr($user_data->user_email),
                        'zip' => esc_attr($user_zip_code),
                        'sms_number' => esc_attr($user_phone),
                        'f1529' => esc_attr($user_data->user_login),
                        'f1739' => esc_url($user_avatar),
                    );
                    $this->wp_ontraport_api_call();
                    $response = $this->_ontraport_api->object()->saveOrUpdate($request_params);
                    $this->wpr_write_ontraport_log('Prospect Social Login', $response);

                    if ($response) {
                        $response_parse = json_decode($response, true);
                        update_user_meta($user_id, 'wpr_ontraport_id', $response_parse['data']['id']);

                        $add_tags = array(
                            'objectID' => ObjectType::CONTACT,
                            'ids' => absint($response_parse['data']['id']),
                            'add_list' => '2,55',
                        );
                        $response_tags = $this->_ontraport_api->object()->addTag($add_tags);
                        $this->wpr_write_ontraport_log('Prospect Social Login add tags', $response_tags);
                    }
                }
            }
        }

        /**
         * Update Ontraport contact info
         *
         * @param $user_id
         * @param $old_user_data
         */
        function wpr_ontraport_profile_update($user_id, $old_user_data)
        {
            if ($user_id) {
                $user_info = get_user_by('id', $user_id);
                $ontraport_id = get_user_meta($user_id, 'wpr_ontraport_id', true);

                if (in_array('wpr_prospect', $user_info->roles) && !empty($ontraport_id)) {
                    $this->wp_ontraport_api_call();
                    if (isset($_POST['profile'])) {
                        $user_zip_code = $_POST['profile']['zip_code'];
                        $user_phone = $_POST['profile']['phone'];
                        $user_address = $_POST['profile']['street'];
                        $user_city = $_POST['profile']['city'];
                        $user_state = $_POST['profile']['state'];
                    } else {
                        $user_zip_code = get_user_meta($user_id, 'zip_code', true);
                        $user_phone = get_user_meta($user_id, 'phone', true);
                        $user_address = get_user_meta($user_id, 'street', true);
                        $user_city = get_user_meta($user_id, 'city', true);
                        $user_state = get_user_meta($user_id, 'state', true);
                    }

                    $user_avatar_url = get_user_meta($user_id, 'wpr_user_avatar', true);
                    $user_meta_picture = get_user_meta($user_id, 'oa_social_login_user_picture', true);

                    $user_avatar = get_avatar_url($user_id);
                    if ($user_meta_picture) {
                        $user_avatar = $user_meta_picture;
                    }
                    if ($user_avatar_url) {
                        $user_avatar = $user_avatar_url;
                    }

                    $request_params = array(
                        'objectID' => ObjectType::CONTACT,
                        'id' => absint($ontraport_id),
                        'firstname' => esc_attr($user_info->first_name),
                        'lastname' => esc_attr($user_info->last_name),
                        'email' => esc_attr($user_info->user_email),
                        'zip' => esc_attr($user_zip_code),
                        'sms_number' => esc_attr($user_phone),
                        'address' => esc_attr($user_address),
                        'city' => esc_attr($user_city),
                        'state' => esc_attr($user_state),
                        'f1739' => esc_url($user_avatar),
                    );

                    $response = $this->_ontraport_api->object()->update($request_params);
                    $this->wpr_write_ontraport_log('Prospect Profile Update', $response);
                }
            }
        }

        /**
         * Search multidimensional array
         *
         * @param $needle
         * @param $haystack
         * @param bool $strict
         *
         * @return bool
         */
        function in_array_r($needle, $haystack, $strict = false)
        {
            foreach ($haystack as $item) {
                if (($strict ? $item === $needle : $item == $needle) || (is_array($item) && $this->in_array_r($needle, $item, $strict))) {
                    return true;
                }
            }

            return false;
        }

        /**
         * Update user My Offers
         */
        function wpr_my_offer_user()
        {
            if (is_user_logged_in()) {

                if (!isset($_POST['nonce'])) {
                    return;
                }

                if (!wp_verify_nonce($_POST['nonce'], 'wpr-dealer-location')) {
                    return;
                }

                $user_id = get_current_user_id();
                $post_id = absint($_POST['postsd']);
                $dealer_id = absint($_POST['wprDeal']);
                $categ_id = absint($_POST['wprCat']);

                $favorite_categories = $this->wpr_get_favorite_categories($user_id);

                update_user_meta($user_id, 'wpr_user_my_offers', $favorite_categories);

                $this->wpr_add_brand_as_selected($user_id, $post_id);

                if (isset($_POST['status']) && 'active' == $_POST['status']) {
                    $this->wp_ontraport_api_call();

                    $ba_id = 0;
                    switch_to_blog(2);
                    if ('' != $dealer_id && '' != $categ_id) {
                        $ba_info = $this->wpr_dealer_ba_info($categ_id, $dealer_id);
                        $ba_id = absint($ba_info[0]->ID);
                    }
                    $ontraport_ba_id = get_user_meta($ba_id, 'wpr_ontraport_id', true);
                    $my_prospect_url = get_permalink(get_page_by_path('prospect-info'));
                    restore_current_blog();

                    // Save to History CTA clicked
                    $to_save = array(
                        'user_id' => $user_id,
                        'post_id' => $post_id,
                        'btn_type' => 'lookbook',
                        'cta_type' => 'lookbook',
                        'date' => current_time('mysql'),
                        'dealer_id' => $dealer_id,
                        'brand_id' => $categ_id,
                        'page_type' => $post_id,
                    );

                    if (!empty($ba_info)) {
                        $to_save['abc_id'] = $ba_info[0]->ID;
                    }

                    $this->wpr_save_cta_button_click($to_save);

                    if ($ba_info[0] && in_array('business_associate', $ba_info[0]->roles)) {
                        $ba_contact_tags = array();
                        array_push($ba_contact_tags, 44);

                        $user_info = get_userdata(absint($user_id));

                        $user_avatar_url = get_user_meta($user_id, 'wpr_user_avatar', true);
                        $user_meta_picture = get_user_meta($user_id, 'oa_social_login_user_picture', true);

                        $user_avatar = get_avatar_url($user_id);
                        if ($user_meta_picture) {
                            $user_avatar = $user_meta_picture;
                        }
                        if ($user_avatar_url) {
                            $user_avatar = $user_avatar_url;
                        }

                        $request_params = array(
                            'objectID' => ObjectType::CONTACT,
                            'id' => absint($ontraport_ba_id),
                            'f1464' => esc_attr(html_entity_decode(get_the_title($post_id))),
                            'f1447' => esc_attr($user_info->first_name),
                            'f1448' => esc_attr($user_info->last_name),
                            'f1449' => esc_url($my_prospect_url . '?prospector=' . $user_id),
                            'f1584' => esc_url($user_avatar),
                            'f1588' => esc_url($this->wpr_get_post_image_source($post_id)),
                        );

                        $response_ontraport = $this->_ontraport_api->object()->update($request_params);
                        $this->wpr_write_ontraport_log('Add Offer to My Offers', $response_ontraport);

                        if ($response_ontraport) {
                            $add_tags = array(
                                'objectID' => ObjectType::CONTACT,
                                'ids' => absint($ontraport_ba_id),
                                'add_list' => implode(',', $ba_contact_tags),
                            );
                            $response_tags = $this->_ontraport_api->object()->addTag($add_tags);
                            $this->wpr_write_ontraport_log('Add Offer to My Offers add tags', $response_tags);
                        }
                    }
                }
            }

            wp_die();
        }

        /**
         * Add Brand as selected when user clicks Add to Lookbook
         *
         * @param $user_id
         * @param $post_id
         */
        function wpr_add_brand_as_selected($user_id, $post_id)
        {
            $brands_array = array();
            $current_brands = get_user_meta($user_id, 'wpr_selected_brands', false);
            if ($current_brands) {
                $brands_array = $current_brands;
            }
            $category = get_the_category($post_id);
            $category_id = $category[0]->term_id;

            if ($category_id && !in_array($category_id, $current_brands)) {
                array_push($brands_array, absint($category_id));
                update_user_meta($user_id, 'wpr_selected_brands', $brands_array);
            }
        }

        /**
         * Get user favorite categories
         *
         * @param $user_id
         *
         * @return array
         */
        function wpr_get_favorite_categories($user_id)
        {
            $post_categories = array();
            $user_favorites = get_user_meta($user_id, 'simplefavorites', true);

            if (!empty($user_favorites)) {
                foreach ($user_favorites[0]['posts'] as $postid) {
                    $categories = wp_get_post_categories($postid);
                    foreach ($categories as $category) {
                        $post_categories[] = $category;
                    }
                }
                $post_categories = array_unique($post_categories, SORT_REGULAR);
            }

            return $post_categories;
        }

        /**
         * Hide BA custom offers by default
         *
         * @param $where
         *
         * @return string
         */
        function wpr_hide_show_ba_offers_by_default($query)
        {
            if (is_admin() && $query->is_main_query() && !is_singular()) {
                $meta_query = array(
                    array(
                        'key' => 'wpr_dealer_id',
                        'compare' => 'NOT EXISTS',
                    ),
                );
                $query->set('meta_query', $meta_query);
            }
        }

        /**
         * Get closest dealer by geo location
         *
         * @param $category_id
         *
         * @return string
         */
        function wpr_closest_dealer_id($category_id)
        {
            $closest_dealer = $this->wpr_get_closest_dealer_data( $category_id );

            if ( ! empty( $closest_dealer ) ) {
                return $closest_dealer['id'];
            }

            return '';
        }

        /**
         * Display BA offers
         *
         * @param $atts
         *
         * @return string
         */
        function wpr_get_ba_offers($atts)
        {
            ob_start();
            /* Set up the default arguments. */
            $defaults = array();

            /* Parse the arguments. */
            extract(shortcode_atts($defaults, $atts));

            $category = get_the_category();
            $category_id = $category[0]->term_id;

            switch_to_blog(2);
            $dealer_id = $this->wpr_closest_dealer_id($category_id);
            restore_current_blog();
            if (!empty($dealer_id) && is_user_logged_in()) {
                remove_action('pre_get_posts', array($this, 'wpr_hide_show_ba_offers_by_default'), 20);
                $ba_id = 0;
                switch_to_blog(2);
                $ba_info = $this->wpr_dealer_ba_info($category_id, $dealer_id);
                restore_current_blog();
                if (!empty($ba_info)) {
                    $ba_id = $ba_info[0]->ID;
                }

                if ($ba_id) {
                    $args = array(
                        'post_type' => 'post',
                        'status' => 'publish',
                        'cat' => $category_id,
                        'meta_key' => 'wpr_dealer_id',
                        'meta_value' => $dealer_id,
                        'author' => $ba_id,
                    );
                    $query1 = new WP_Query($args);
                    if ($query1->have_posts()) {
                        echo sprintf('<div class="wpr_display_custom_offers">');
                        echo sprintf('<h2>%s</h2>', __('Custom offers'));
                        do_action('novus_loop_before');
                        while ($query1->have_posts()) {
                            $query1->the_post();
                            get_template_part('partials/content', get_post_format());
                        }
                        do_action('novus_loop_after');
                        echo sprintf('</div>');
                    }
                }

                wp_reset_postdata();
                add_action('pre_get_posts', array($this, 'wpr_hide_show_ba_offers_by_default'), 20, 1);
            }

            return ob_get_clean();
        }

        /**
         * Dealer info shortcode
         *
         * @param $atts
         *
         * @return string
         */
        function wpr_get_dealer_info($atts)
        {
            $output = '';
            $got_a_ba = $got_dealer = false;
            $appointment_btn = '';
            $learn_more = '';

            /* Set up the default arguments. */
            $defaults = array();

            /* Parse the arguments. */
            extract(shortcode_atts($defaults, $atts));
            if (is_single() || is_archive()) {

                $post_id = get_the_ID() ? get_the_ID() : '';
                if ($post_id) {
                    $category = get_the_category($post_id);
                    $category_id = $category[0]->term_id;
                    // Do the Dealer Display Info
                    do_action('wpr_obtain_dealer_data_info', $category_id, $output);
                }
            }

            return $output;
        }

        /**
         * Get closes dealer data
         *
         * @param   int  $category_id
         *
         * @return  array   Ontraport dealer data
         */
        function wpr_get_closest_dealer_data( $category_id ) {
	        switch_to_blog(1);

	        global $wpdb;

	        $table_name =  $wpdb->prefix . 'wpr_ontraport_dealerships';

	        $brand = get_cat_name( $category_id );

	        if ( strpos( $brand, 'Tiffany' ) !== false ) {
		        $brand = 'Tiffany';
	        }
	        if ( strpos( $brand, 'Christie' ) !== false ) {
		        $brand = 'hristie';
	        }
	        $current_user       = wp_get_current_user();
	        $current_user_email = $current_user ? $current_user->user_email : false;

	        $dealer_email = false;
	        $dealerships = false;

	        if ( $current_user_email ) {
		        $email_domain = explode( '@', $current_user_email )[1];
		        if ( 'hokuapps.com' === $email_domain ) {
			        switch ( $brand ) {
				        case 'BMW':
					        $dealer_email = 'ed@broyhill.net';
					        break;
				        case 'Audi':
					        $dealer_email = 'careertowntest1@' . $email_domain;
					        break;
				        case 'Mercedes-Benz':
					        $dealer_email = 'alohaankurj@gmail.com';
					        // echo 'OK';

					        break;
				        case 'Ferrari':
					        $dealer_email = 'oyster93@yahoo.com';
					        break;
				        case 'Jaguar':
					        $dealer_email = 'mitko.kockovski@gmail.com';
					        break;
				        case 'Lexus':
					        $dealer_email = 'adam.l.humburg@gmail.com';
					        break;
				        case 'Bloomingdales':
					        $dealer_email = 'careertowntest3@' . $email_domain;
					        break;
				        case 'Nordstrom':
					        $dealer_email = 'billfutreal@gmail.com';
					        break;
				        case 'Brooks Brothers':
					        $dealer_email = 'hello@ugurcan.me';
					        break;
				        case 'Ethan Allen':
					        $dealer_email = 'msdolphin@lethadavis.com';
					        break;
			        }
				   if( $dealer_email ) {
					   $sql = $wpdb->prepare( "SELECT data{fields} FROM {$table_name} WHERE data LIKE %s", '%' . $dealer_email . '%' );
					   $sql = $this->wpr_display_closest_dealer_2( $sql );
					   $dealerships = $wpdb->get_results( $sql );
				   }
		        }
	        }

		   if ( empty( $dealerships ) ) {
		       $sql = $wpdb->prepare( "SELECT data{fields} FROM {$table_name} WHERE brand LIKE %s", '%' . $brand . '%' );

		       $sql = $this->wpr_display_closest_dealer_2( $sql );

		       $dealerships = $wpdb->get_results( $sql );
		   }


	        restore_current_blog();

            $dealer = [];

            if ( $dealerships ) {
                $dealer = maybe_unserialize( $dealerships[0]->data );
            }

            wp_cache_set( $key, $dealer, 'wpr-dealer' );

            return $dealer;
        }

        /**
         * @param string $sql
         *
         * @return mixed
         */
        function wpr_display_closest_dealer_2( $sql )
        {
            global $wpdb;

            $user_lat = false;
            $user_lng = false;

            if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
                $ip = $_SERVER['HTTP_CLIENT_IP'];
            } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
                $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
            } else {
                $ip = $_SERVER['REMOTE_ADDR'];
            }

            $user_id = get_current_user_id();
            if ($user_id > 0) {
                $get_zip = get_user_meta($user_id, 'zip_code', true);

                if (!empty($get_zip)) {
                    $geocode = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address=' . esc_attr($get_zip) . '&key=AIzaSyDkE0osaaC14WeHZ75j54HnXqDjz9Z5fg8');
                    $output = json_decode($geocode);
                    if ($output->results) {
                        $user_lat = $output->results[0]->geometry->location->lat;
                        $user_lng = $output->results[0]->geometry->location->lng;
                    }
                }

                //var_dump($user_lat);
            }

            if ($user_lat == false) {
                $get_ip = explode(',', $ip);
                if (!empty($get_ip)) {
                    $ip = $get_ip[0];
                }
                $geocode = @unserialize(file_get_contents('http://ip-api.com/php/' . $ip));
                if ('fail' != $geocode['status']) {
                    $user_lat = $geocode['lat'];
                    $user_lng = $geocode['lon'];
                }
            }

            //get the user's current location ( lat/ lng )
            if ($user_lat == false || $user_lng == false) {
                $user_lat = '40.748194';
                $user_lng = '-73.984925';
            }


            //abort if user's current location not exist
            if ($user_lat == false || $user_lng == false) {
                return $sql;
            }

            //set some values
            $radius = $this->radius; //can be any value
            $units = 3959; //3959 for miles or 6371 for kilometers

            $sql = str_replace( '{fields}', $wpdb->prepare(
                ", ROUND( %d * acos( cos( radians( %s ) ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians( %s ) ) + sin( radians( %s ) ) * sin( radians( latitude ) ) ),1 ) AS distance",
                $units, $user_lat, $user_lng, $user_lat
            ), $sql );

            $sql .= " GROUP BY id ORDER BY distance ASC LIMIT 1";

            return $sql;
        }

        /**
         * @param $clauses
         * @param $query
         *
         * @return mixed
         */
        function wpr_display_closest_dealer($clauses, $query)
        {
            global $wpdb;

            $user_lat = false;
            $user_lng = false;

            if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
                $ip = $_SERVER['HTTP_CLIENT_IP'];
            } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
                $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
            } else {
                $ip = $_SERVER['REMOTE_ADDR'];
            }

            $user_id = get_current_user_id();
            if ($user_id > 0) {
                $get_zip = get_user_meta($user_id, 'zip_code', true);


                if (!empty($get_zip)) {
                    //$geocode = file_get_contents('http://maps.google.com/maps/api/geocode/json?address=' . esc_attr($get_zip) . '&amp;sensor=false');
                    $geocode = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address=' . esc_attr($get_zip) . '&key=AIzaSyDkE0osaaC14WeHZ75j54HnXqDjz9Z5fg8');
                    $output = json_decode($geocode);
                    if ($output->results) {
                        $user_lat = $output->results[0]->geometry->location->lat;
                        $user_lng = $output->results[0]->geometry->location->lng;
                    }
                }

                //var_dump($user_lat);
            }

            if ($user_lat == false) {
                $get_ip = explode(',', $ip);
                if (!empty($get_ip)) {
                    $ip = $get_ip[0];
                }

                $geocode = @unserialize(file_get_contents('http://ip-api.com/php/' . $ip));
                if ('fail' != $geocode['status']) {
                    $user_lat = $geocode['lat'];
                    $user_lng = $geocode['lon'];
                }
            }

            //get the user's current location ( lat/ lng )
            if ($user_lat == false || $user_lng == false) {
                $user_lat = '40.748194';
                $user_lng = '-73.984925';
            }

            //abort if user's current location not exist
            if ($user_lat == false || $user_lng == false) {
                return $clauses;
            }

            //set some values
            $radius = $this->radius; //can be any value
            $orderby = 'distance ASC'; //can be user with post_title, post_date, ID and so on...
            $units = 3959; //3959 for miles or 6371 for kilometers

            // join the location table into the query
            $clauses['join'] .= " INNER JOIN `{$wpdb->prefix}places_locator` gmwlocations ON $wpdb->posts.ID = gmwlocations.post_id ";
            // add the radius calculation and add the locations fields into the results
            $clauses['fields'] .= $wpdb->prepare(
                ', gmwlocations.formatted_address , ROUND( %d * acos( cos( radians( %s ) ) * cos( radians( gmwlocations.lat ) ) * cos( radians( gmwlocations.long ) - radians( %s ) ) + sin( radians( %s ) ) * sin( radians( gmwlocations.lat) ) ),1 ) AS distance',
                array($units, $user_lat, $user_lng, $user_lat)
            );
            $clauses['groupby'] = $wpdb->prepare(" $wpdb->posts.ID HAVING distance <= %d OR distance IS NULL", $radius);
            $clauses['orderby'] = $orderby;
            $clauses['order'] = 'DESC';

            return $clauses;
        }

        /**
         * New York dealer
         *
         * @param $clauses
         * @param $query
         *
         * @return mixed
         */
        function wpr_display_new_york_dealer($clauses, $query)
        {
            global $wpdb;

            $user_lat = '40.748194';
            $user_lng = '-73.984925';
            $radius = $this->radius; //can be any value
            $orderby = 'distance ASC'; //can be user with post_title, post_date, ID and so on...
            $units = 3959; //3959 for miles or 6371 for kilometers

            // join the location table into the query
            $clauses['join'] .= " INNER JOIN `{$wpdb->prefix}places_locator` gmwlocations ON $wpdb->posts.ID = gmwlocations.post_id ";
            // add the radius calculation and add the locations fields into the results
            $clauses['fields'] .= $wpdb->prepare(
                ', gmwlocations.formatted_address , ROUND( %d * acos( cos( radians( %s ) ) * cos( radians( gmwlocations.lat ) ) * cos( radians( gmwlocations.long ) - radians( %s ) ) + sin( radians( %s ) ) * sin( radians( gmwlocations.lat) ) ),1 ) AS distance',
                array($units, $user_lat, $user_lng, $user_lat)
            );
            $clauses['groupby'] = $wpdb->prepare(" $wpdb->posts.ID HAVING distance <= %d OR distance IS NULL", $radius);
            $clauses['orderby'] = $orderby;
            $clauses['order'] = 'DESC';

            return $clauses;
        }

        public function wpr_register_click()
        {
            check_ajax_referer('wpr-dealer-location', 'nonce');
            if (!is_user_logged_in()) {
                $dealer_id = absint($_POST['deal']);
                $categ_id = absint($_POST['categ']);
                $to_do = esc_attr($_POST['todo']);
                $offer_id = absint($_POST['current_offer']);
                $current_url = esc_url($_POST['current_url']);

                if (!$categ_id && $offer_id) {
                    $category = get_the_category($offer_id);
                    $categ_id = $category[0]->term_id;
                }

                if ($categ_id) {
                    $dealer_id = $this->wpr_return_dealer_id($categ_id);
                }

                $data_click = array(
                    'dealer_id' => $dealer_id,
                    'categ_id' => $categ_id,
                    'to_do' => $to_do,
                    'offer_id' => $offer_id,
                    'current_url' => $current_url,
                );
                setcookie('wpr_user_click', json_encode($data_click), 30 * DAYS_IN_SECONDS, COOKIEPATH, COOKIE_DOMAIN);

                return;
            }
        }

        /**
         * Process Ontraport API call
         */
        public function wpr_map_location_click()
        {
            $dealer_id = absint($_POST['deal']);
            $categ_id = absint($_POST['categ']);
            $to_do = esc_attr($_POST['todo']);
            $offer_id = isset($_POST['current_offer']) ? absint($_POST['current_offer']) : '';
            $current_url = esc_url($_POST['current_url']);

            if (!$categ_id && $offer_id) {
                $category = get_the_category($offer_id);
                $categ_id = $category[0]->term_id;
            }

            if ($categ_id && !$dealer_id) {
                $dealer_id = $this->wpr_return_dealer_id($categ_id);
            }

            $data_click = array(
                'dealer_id' => $dealer_id,
                'categ_id' => $categ_id,
                'to_do' => $to_do,
                'offer_id' => $offer_id,
                'current_url' => $current_url,
            );

            if (!is_user_logged_in()) {
                setcookie('wpr_user_click', json_encode($data_click), 30 * DAYS_IN_SECONDS, COOKIEPATH, COOKIE_DOMAIN);

                return;
            }
            // Get current logged in user
            $current_user = wp_get_current_user();

            // Have LC completed the account info
            if ($current_user && !$this->wpr_user_data_filled($current_user->ID)) {
                setcookie('wpr_user_click', json_encode($data_click), 30 * DAYS_IN_SECONDS, COOKIEPATH, COOKIE_DOMAIN);

                $display_message = esc_attr(get_cat_name($categ_id));

                setcookie('wpr_user_brand_page', $display_message, 30 * DAYS_IN_SECONDS, COOKIEPATH, COOKIE_DOMAIN);
                $fill_data_url = get_permalink(get_page_by_path('complete-your-profile'));
                $redirect = array(
                    'text' => esc_url_raw($fill_data_url),
                    'response' => 'redirect',
                );
                echo wp_json_encode($redirect);

                wp_die();
            }

            /**
             * Default message
             */
            $response = __('<b>Your request has been sent.</b><br />', 'wpr-dealers');
            $response .= __('An Authorized Brand Consultant will be in touch with you as soon as possible.<br /><br />', 'wpr-dealers');
            $response .= __('<span>Thank you for your interest!</span>', 'wpr-dealers');
            $response_type = 'success';

            if (!isset($_POST['nonce'])) {
                $response_type = 'error';
                $response = __('Not allowed.', 'wpr-dealers');
                echo wp_json_encode(
                    array(
                        'text' => $response,
                        'response' => $response_type,
                    )
                );

                wp_die();
            }

            if (!wp_verify_nonce($_POST['nonce'], 'wpr-dealer-location')) {
                $response_type = 'error';
                $response = __('Not allowed.', 'wpr-dealers');
                echo wp_json_encode(
                    array(
                        'text' => $response,
                        'response' => $response_type,
                    )
                );

                wp_die();
            }

            if ('' == $dealer_id && '' == $categ_id) {
                $response_type = 'error';
                $response = __('<b>Authorized Brand Consultant not found!</b><br /> Please get in touch with support.', 'wpr-dealers');
                echo wp_json_encode(
                    array(
                        'text' => $response,
                        'response' => $response_type,
                    )
                );

                wp_die();
            }

            /**
             * CTA BUTTON CLICKED
             */
            $data_click['user_id'] = $current_user->ID;
            try {
                $result = $this->data(
                    array(
                        'cta_clicked' => $data_click,
                    )
                )->save()->dispatch()->wpr_generate_error();

                if (!empty($result)) {
                    $response_type = 'error';
                    $response = sprintf('<b>%s</b><br />%s', __('There was an error.', 'wpr-dealers'), __('Please try again later.', 'wpr-dealers'));
                }
            } catch (Exception $e) {
                $response_type = 'error';
                $response = sprintf('<b>%s</b><br />%s', __('There was an error.', 'wpr-dealers'), __('Please try again later.', 'wpr-dealers'));
            }
            /**
             * CTA BUTTON CLICKED END
             */

            echo wp_json_encode(
                array(
                    'text' => $response,
                    'response' => $response_type,
                )
            );

            wp_die();
        }

        /**
         * Get user Ontraport ID
         *
         * @param $user_id
         *
         * @return bool|mixed
         */
        function wpr_get_ontraport_id($user_id)
        {
            if ($user_id) {
                $ontraport_id = get_user_meta(absint($user_id), 'wpr_ontraport_id', true);

                return absint($ontraport_id);
            }

            return false;
        }

        /**
         * Save CTA button clicked
         *
         * @param $args
         */
        function wpr_save_cta_button_click($args)
        {
            if (!empty($args) && is_array($args)) {
                global $wpdb;

                $wpdb->insert(
                    $wpdb->prefix . 'wpr_cta_buttons',
                    array(
                        'user_id' => absint($args['user_id']),
                        'post_id' => absint($args['post_id']),
                        'btn_type' => esc_attr($args['btn_type']),
                        'btn_string' => esc_attr($args['cta_type']),
                        'date' => esc_attr($args['date']),
                        'dealer_id' => absint($args['dealer_id']),
                        'brand_id' => absint($args['brand_id']),
                        'abc_id' => absint($args['abc_id']),
                        'page_type' => absint($args['page_type']),
                    ),
                    array(
                        '%d',
                        '%d',
                        '%s',
                        '%s',
                        '%s',
                        '%d',
                        '%d',
                        '%d',
                    )
                );
            }
        }

        /**
         * Add LC inline if there is no ABC
         *
         * @param $args
         */
        function wpr_add_to_waiting_line($args)
        {
            if (!empty($args) && is_array($args)) {
                global $wpdb;

                $wpdb->insert(
                    $wpdb->prefix . 'wpr_waiting_abc_list',
                    array(
                        'lc_id' => absint($args['user_id']),
                        'post_id' => absint($args['post_id']),
                        'brand_id' => absint($args['brand_id']),
                        'dealer_id' => absint($args['dealer_id']),
                        'lc_order' => absint($args['lc_order']),
                        'btn_type' => esc_attr($args['btn_type']),
                        'api_call' => esc_attr($args['api_call']),
                        'date' => esc_attr($args['date']),
                    ),
                    array(
                        '%d',
                        '%d',
                        '%d',
                        '%d',
                        '%d',
                        '%s',
                        '%s',
                        '%s',
                    )
                );
            }
        }

        /**
         * Delete dealer from waiting list
         *
         * @param $dealer_id
         */
        function wpr_remove_from_waiting_list($dealer_id)
        {
            if ($dealer_id) {
                global $wpdb;

                $wpdb->delete(
                    $wpdb->prefix . 'wpr_waiting_abc_list',
                    array(
                        'dealer_id' => absint($dealer_id)
                    ),
                    array(
                        '%d'
                    )
                );
            }
        }

        /**
         * Check if entry on dealer_id exists
         *
         * @param $dealer_id
         *
         * @return bool
         */
        function wpr_is_in_waiting_list($dealer_id)
        {
            $waiting = true;

            if ($dealer_id) {
                global $wpdb;
                $search_entry = $wpdb->get_var($wpdb->prepare(
                    "
                        SELECT COUNT(*)
                        FROM {$wpdb->prefix}wpr_waiting_abc_list
                        WHERE dealer_id = %d
                    ",
                    absint($dealer_id)
                ));

                if (0 == $search_entry) {
                    $waiting = false;
                }
            }

            return $waiting;
        }

        /**
         * Is LC in waiting list
         *
         * @param $lc_id
         * @param $dealer_id
         *
         * @return bool
         */
        function wpr_is_lc_in_waiting_list($lc_id, $dealer_id)
        {
            $waiting = true;

            if ($dealer_id) {
                global $wpdb;
                $search_entry = $wpdb->get_var($wpdb->prepare(
                    "
                        SELECT COUNT(*)
                        FROM {$wpdb->prefix}wpr_waiting_abc_list
                        WHERE dealer_id = %d
                        AND lc_id = %d
                    ",
                    absint($dealer_id),
                    absint($lc_id)
                ));

                if (0 == $search_entry) {
                    $waiting = false;
                }
            }

            return $waiting;
        }

        /**
         * If exists get order number
         *
         * @param $lc_id
         * @param $dealer_id
         *
         * @return bool
         */
        function wpr_get_lc_order_nr($lc_id, $dealer_id)
        {
            if ($dealer_id) {
                global $wpdb;
                $search_entry = $wpdb->get_row($wpdb->prepare(
                    "
                        SELECT lc_order
                        FROM {$wpdb->prefix}wpr_waiting_abc_list
                        WHERE dealer_id = %d
                        AND lc_id = %d
                    ",
                    absint($dealer_id),
                    absint($lc_id)
                ));

                if ($search_entry) {
                    return $search_entry->lc_order;
                }
            }

            return false;
        }

        /**
         * Get LC ids frm waiting list for dealership id
         *
         * @param $dealer_id
         *
         * @return array|bool
         */
        function wpr_get_waiting_list_ids($dealer_id)
        {
            if ($dealer_id) {
                global $wpdb;

                $get_entry = $wpdb->get_results($wpdb->prepare(
                    "
                        SELECT
                            lc_id
                        FROM
                            {$wpdb->prefix}wpr_waiting_abc_list
                        WHERE
                            dealer_id = %d
                        GROUP BY
                            lc_id
                        ORDER BY
                            id
                        ASC
                    ",
                    absint($dealer_id)
                ));

                if ($get_entry) {
                    $ids = array();
                    foreach ($get_entry as $lc_id) {
                        array_push($ids, absint($lc_id->lc_id));
                    }

                    return $ids;
                }
            }

            return false;
        }

        /**
         * Get BTN Type
         *
         * @param $dealer_id
         * @param $lc_id
         * @param $post_id
         *
         * @return bool
         */
        function wpr_get_waiting_list_to_do($dealer_id, $lc_id, $post_id)
        {
            if ($dealer_id) {
                global $wpdb;

                $search_entry = $wpdb->get_row($wpdb->prepare(
                    "
                        SELECT
                            btn_type
                        FROM
                            {$wpdb->prefix}wpr_waiting_abc_list
                        WHERE
                            dealer_id = %d
                        AND
                            lc_id = %d
                        AND
                            post_id = %d
                        ORDER BY
                            id
                        ASC
                    ",
                    absint($dealer_id),
                    absint($lc_id),
                    absint($post_id)
                ));

                if ($search_entry) {
                    return $search_entry->btn_type;
                }
            }

            return false;
        }

        /**
         * Generate offer code
         *
         * @param $brand_id
         * @param $offer_id
         *
         * @return string
         */
        function wpr_generate_offer_code($brand_id, $offer_id)
        {
            $offer_code = 'LC_';
            $offer_code .= strtoupper($this->wpr_get_cat_slug($brand_id));
            $offer_code .= '_' . $offer_id;

            $post_author = get_post_field('post_author', $offer_id);
            $wp_user_obj = new WP_User(
                absint($post_author),
                '',
                2
            );

            if (!empty($wp_user_obj)) {
                if (in_array('business_associate', $wp_user_obj->roles)) {
                    $offer_code .= '_' . $post_author;
                }
            }

            $get_all_tags = $this->wpr_retrive_all_tags($offer_code);
            if (!empty($get_all_tags)) {
                $tag_id = $get_all_tags[0]['tag_id'];
            } else {
                $tag_id = $this->wpr_create_tag($offer_code);
            }

            return $tag_id;
        }

        /**
         * Get category slug
         *
         * @param $cat_id
         *
         * @return mixed
         */
        function wpr_get_cat_slug($cat_id)
        {
            $cat_id = absint($cat_id);
            $category = get_category($cat_id);

            return $category->slug;
        }

        /**
         * Add new tag
         *
         * @param $offer_code
         *
         * @return mixed
         */
        function wpr_create_tag($offer_code)
        {
            $this->wp_ontraport_api_call();
            $requestParams = array(
                'objectID' => ObjectType::TAG,
                'tag_name' => $offer_code,
            );
            $response = $this->_ontraport_api->object()->create($requestParams);
            $this->wpr_write_ontraport_log('Add new tag to Ontraport', $response);
            $response = json_decode($response, true);

            return $response['data']['tag_id'];
        }

        /**
         * Search the tag from ontraport
         *
         * @return array|mixed|object
         */
        function wpr_retrive_all_tags($offer_code = '')
        {
            $this->wp_ontraport_api_call();
            $requestParams = array(
                'objectID' => ObjectType::TAG,
                'listFields' => 'tag_id,tag_name'
            );

            if (!empty($offer_code)) {
                $requestParams['search'] = $offer_code;
            }

            $response = $this->_ontraport_api->object()->retrieveMultiple($requestParams);
            $this->wpr_write_ontraport_log('Retrieve all the tags from Ontraport', $response);
            $response = json_decode($response, true);

            return $response['data'];
        }

        /**
         * Get BA info
         *
         * @param $category_id
         * @param $dealer_id
         *
         * @return array
         */
        function wpr_dealer_ba_info($category_id, $dealer_id)
        {
            $args = array(
                'role' => 'business_associate',
                'meta_query' => array(
                    'relation' => 'AND',
                    array(
                        'key' => 'wpr_brand_association',
                        'value' => $category_id,
                        'compare' => '=',
                    ),
                    array(
                        'key' => 'wpr_dealer_association',
                        'value' => $dealer_id,
                        'compare' => '=',
                    ),
                ),
                'fields' => 'all',
                'number' => 1,
            );
            $user_query = new WP_User_Query($args);

            return $user_query->results;
        }

        /**
         * Return Dealer ID
         *
         * @param $category_id
         *
         * @return string
         */
        function wpr_return_dealer_id($category_id)
        {
            $dealer_id = '';
            switch_to_blog(2);
            $args = array(
                'post_type' => 'wpr_dealer_info',
                'status' => 'publish',
                'meta_key' => 'wpr_category_id',
                'meta_value' => $category_id,
                'posts_per_page' => 1,
            );
            remove_action('pre_get_posts', array($this, 'wpr_hide_show_ba_offers_by_default'), 20);
            add_filter('posts_clauses', array($this, 'wpr_display_closest_dealer'), 10, 2);
            $query = new WP_Query($args);
            $posts = $query->posts;

            if ($query->have_posts()) {
                $dealer_id = $posts[0]->ID;
            }

            wp_reset_postdata();
            remove_filter('posts_clauses', array($this, 'wpr_display_closest_dealer'));

            restore_current_blog();

            return $dealer_id;
        }

        /**
         * Get user info
         *
         * @param $user_id
         *
         * @return string|void
         */
        function wpr_user_info_email_body($user_id)
        {
            if (empty($user_id) || !is_int($user_id)) {
                return;
            }
            $user_info = '';
            $user_data_info = get_userdata($user_id);
            $first_last_name = $user_data_info->first_name . ' ' . $user_data_info->last_name;
            if (empty($user_data_info->first_name) || empty($user_data_info->last_name)) {
                $first_last_name = $user_data_info->display_name;
            }

            if (!empty($first_last_name)) {
                $user_info .= __('Subscriber Name', 'wpr-dealers') . ': ' . $first_last_name . '<br />';
            }

            if (!empty($user_data_info->user_email)) {
                $user_info .= __('Subscriber Email', 'wpr-dealers') . ': ' . $user_data_info->user_email . '<br />';
            }

            if (!empty($user_data_info->phone)) {
                $user_info .= __('Subscriber Phone', 'wpr-dealers') . ': ' . $user_data_info->phone . '<br />';
            }

            if (!empty($user_data_info->zip_code)) {
                $user_info .= __('Zip Code', 'wpr-dealers') . ': ' . $user_data_info->zip_code . '<br />';
            }

            //          $prospect_address = '';
            //          if ( ! empty( $user_data_info->street ) ) {
            //              $prospect_address .= __( 'Street', 'wpr-dealers' ) . ': ' . $user_data_info->street . ', ';
            //          }
            //          if ( ! empty( $user_data_info->city ) ) {
            //              $prospect_address .= __( 'City', 'wpr-dealers' ) . ': ' . $user_data_info->city . ', ';
            //          }
            //          if ( ! empty( $user_data_info->zip_code ) ) {
            //              $prospect_address .= __( 'Zip code', 'wpr-dealers' ) . ': ' . $user_data_info->zip_code . ', ';
            //          }
            //          if ( ! empty( $user_data_info->state ) ) {
            //              $prospect_address .= __( 'State', 'wpr-dealers' ) . ': ' . $user_data_info->state . '';
            //          }
            //
            //          if ( ! empty( $prospect_address ) ) {
            //              $user_info .= __( 'Address', 'wpr-dealers' ) . ': ' . $prospect_address . '<br />';
            //          }

            return $user_info;
        }

        /**
         * Search location autocomplete
         */
        public function wpr_add_footer_script()
        {
            ?>
            <script type="text/javascript">
                var input = document.getElementById('wpr_search_location');

                if (input !== null) {
                    var options = {
                        types: ['geocode'],
                    };

                    var autocomplete = new google.maps.places.Autocomplete(input, options);

                    google.maps.event.addListener(autocomplete, 'place_changed', function (e) {

                        var place = autocomplete.getPlace();

                        if (!place.geometry) {
                            return;
                        }
                    });
                }
            </script>
            <?php

        }

        public function dispatch()
        {
            // Schedule the cron healthcheck.
            $this->wpr_register_schedule_event();

            return $this;
        }

        /**
         * Set data used during the request
         *
         * @param array $data Data.
         *
         * @return $this
         */
        public function data($data)
        {
            $this->_error_report = null;

            $this->data = $data;

            return $this;
        }

        /**
         * Schedule cron healthcheck
         *
         * @access public
         *
         * @param mixed $schedules Schedules.
         *
         * @return mixed
         */
        public function schedule_cron_healthcheck($schedules)
        {
            $interval = apply_filters($this->identifier . '_cron_interval', 1);

            if (property_exists($this, 'cron_interval')) {
                $interval = apply_filters($this->identifier . '_cron_interval', $this->cron_interval_identifier);
            }

            // Adds every 1 minutes to the existing schedules.
            $schedules[$this->identifier . '_cron_interval'] = array(
                'interval' => MINUTE_IN_SECONDS * $interval,
                'display' => sprintf(__('Every %d Minutes', 'wpr-dealers'), $interval),
            );

            return $schedules;
        }

        /**
         * Register Cron if not exists
         */
        function wpr_register_schedule_event()
        {
            if (!wp_next_scheduled($this->cron_hook_identifier)) {
                $time = time() + 20;
                wp_schedule_event($time, $this->cron_interval_identifier, $this->cron_hook_identifier);
            }
        }

        /**
         * @return null
         */
        function wpr_generate_error()
        {
            return $this->_error_report;
        }

        /**
         * Save queue
         *
         * @return $this
         */
        public function save()
        {
            $key = $this->generate_key();

            if (!empty($this->data)) {
                $insert = update_site_option($key, $this->data);

                if (!$insert) {
                    $this->_error_report = 'Error';
                }
            } else {
                $this->_error_report = 'Error';
            }

            return $this;
        }

        /**
         * Update queue
         *
         * @param string $key Key.
         * @param array $data Data.
         *
         * @return $this
         */
        public function update($key, $data)
        {
            if (!empty($data)) {
                update_site_option($key, $data);
            }

            return $this;
        }

        /**
         * Delete queue
         *
         * @param string $key Key.
         *
         * @return $this
         */
        public function delete($key)
        {
            delete_site_option($key);

            return $this;
        }

        /**
         * Generate key
         *
         * Generates a unique key based on microtime. Queue items are
         * given a unique key so that they can be merged upon save.
         *
         * @param int $length Length.
         *
         * @return string
         */
        protected function generate_key($length = 64)
        {
            $unique = md5(microtime() . rand());
            $prepend = $this->identifier . '_batch_';

            return substr($prepend . $unique, 0, $length);
        }

        /**
         * Is queue empty
         *
         * @return bool
         */
        protected function is_queue_empty()
        {
            global $wpdb;

            $table = $wpdb->options;
            $column = 'option_name';

            if (is_multisite()) {
                $table = $wpdb->sitemeta;
                $column = 'meta_key';
            }

            $key = $this->identifier . '_batch_%';

            $count = $wpdb->get_var(
                $wpdb->prepare(
                    "
                    SELECT COUNT(*)
                    FROM {$table}
                    WHERE {$column} LIKE %s
                ", $key
                )
            );

            return ($count > 0) ? false : true;
        }

        /**
         * Is process running
         *
         * Check whether the current process is already running
         * in a background process.
         */
        protected function is_process_running()
        {
            if (get_site_transient($this->identifier . '_process_lock')) {
                // Process already running.
                return true;
            }

            return false;
        }

        /**
         * Lock process
         *
         * Lock the process so that multiple instances can't run simultaneously.
         * Override if applicable, but the duration should be greater than that
         * defined in the time_exceeded() method.
         */
        protected function lock_process()
        {
            $this->start_time = time(); // Set start time of current process.

            $lock_duration = (property_exists($this, 'queue_lock_time')) ? $this->queue_lock_time : 60; // 1 minute
            $lock_duration = apply_filters($this->identifier . '_queue_lock_time', $lock_duration);

            set_site_transient($this->identifier . '_process_lock', microtime(), $lock_duration);
        }

        /**
         * Unlock process
         *
         * Unlock the process so that other instances can spawn.
         *
         * @return $this
         */
        protected function unlock_process()
        {
            delete_site_transient($this->identifier . '_process_lock');

            return $this;
        }

        /**
         * Get batch
         *
         * @return stdClass Return the first batch from the queue
         */
        protected function get_batch()
        {
            global $wpdb;

            $table = $wpdb->options;
            $column = 'option_name';
            $key_column = 'option_id';
            $value_column = 'option_value';

            if (is_multisite()) {
                $table = $wpdb->sitemeta;
                $column = 'meta_key';
                $key_column = 'meta_id';
                $value_column = 'meta_value';
            }

            $key = $this->identifier . '_batch_%';

            $query = $wpdb->get_row(
                $wpdb->prepare(
                    "
                SELECT *
                FROM {$table}
                WHERE {$column} LIKE %s
                ORDER BY {$key_column} ASC
                LIMIT 1
            ", $key
                )
            );

            $batch = new stdClass();
            $batch->key = $query->$column;
            $batch->data = maybe_unserialize($query->$value_column);

            return $batch;
        }

        /**
         * Handle
         *
         * Pass each queue item to the task handler, while remaining
         * within server memory and time limit constraints.
         */
        protected function handle()
        {
            $this->lock_process();

            do {
                $batch = $this->get_batch();

                // Process tasks
                foreach ($batch->data as $key => $value) {

                    if ('lc_tag' == $key || 'ba_tag' == $key) {
                        $task = $this->add_tags_task($value);
                    } elseif ('hs_abc' == $key) {
                        $task = $this->hs_abc_task($value);
                    } elseif ('cta_clicked' == $key) {
                        $task = $this->cta_clicked_task($value);
                    } else {
                        $task = $this->task($value);
                    }

                    if (false !== $task) {
                        $batch->data[$key] = $task;
                    } else {
                        unset($batch->data[$key]);
                    }

                    if ($this->time_exceeded() || $this->memory_exceeded()) {
                        // Batch limits reached.
                        break;
                    }
                }

                // Update or delete current batch.
                if (!empty($batch->data)) {
                    $this->update($batch->key, $batch->data);
                } else {
                    $this->delete($batch->key);
                }
            } while (!$this->time_exceeded() && !$this->memory_exceeded() && !$this->is_queue_empty());

            $this->unlock_process();

            // Start next batch or complete process.
            if (!$this->is_queue_empty()) {
                $result = $this->dispatch()->wpr_generate_error();
                if (!empty($result)) {
                    $this->wpr_write_ontraport_log('Execute cron job on handle', $result);
                }
            } else {
                $this->complete();
            }

            wp_die();
        }

        /**
         * Set Data to be send
         *
         * @param $item
         *
         * @return bool|string
         */
        protected function cta_clicked_task($item)
        {
            $response = true;

            try {
                $dealer_id = absint($item['dealer_id']);
                $categ_id = absint($item['categ_id']);
                $to_do = esc_attr($item['to_do']);
                $offer_id = absint($item['offer_id']);
                $current_url = esc_url($item['current_url']);

                $current_user = get_user_by('id', absint($item['user_id']));

                // Add offer to Lookbook
                if (!$this->wpr_is_in_favorite($offer_id, get_current_blog_id(), $current_user->ID)) {
                    $this->wpr_add_to_lookbook($current_user->ID, $offer_id, get_current_blog_id());
                }

                $button = $ontraport_button = $type_lc = $type_ba = $api_call = '';
                switch ($to_do) {
                    case 'learn':
                        $button = $type_lc = __('for more info', 'wpr-dealers');
                        $type_ba = __('requested more info', 'wpr-dealers');
                        $ontraport_button = '17';
                        $api_call = 'info';
                        break;
                    case 'map':
                        $button = __('Map Location', 'wpr-dealers');
                        break;
                    case 'price':
                        $button = __('about price', 'wpr-dealers');
                        $type_lc = __('for a price', 'wpr-dealers');
                        $type_ba = __('requested a price', 'wpr-dealers');
                        $ontraport_button = '15';
                        $api_call = 'rp';
                        break;
                    case 'appointment':
                        $button = $type_lc = __('for an appointment', 'wpr-dealers');
                        $type_ba = __('requested an appointment', 'wpr-dealers');
                        $ontraport_button = '16';
                        $api_call = 'app';
                        break;
                    case 'contact':
                        $button = __('to be contacted', 'wpr-dealers');
                        $type_lc = __('for a call', 'wpr-dealers');
                        $type_ba = __('requested a call', 'wpr-dealers');
                        $ontraport_button = '18';
                        $api_call = 'call';
                }

                $dealer_name = $dealer_fields = '';

                // Switch to second blog
                switch_to_blog(2);
                if ('' != $dealer_id && '' != $categ_id) {
                    $args = array(
                        'role' => 'business_associate',
                        'meta_query' => array(
                            'relation' => 'AND',
                            array(
                                'key' => 'wpr_brand_association',
                                'value' => $categ_id,
                                'compare' => '=',
                            ),
                            array(
                                'key' => 'wpr_dealer_association',
                                'value' => $dealer_id,
                                'compare' => '=',
                            ),
                        ),
                        'fields' => 'all',
                        'number' => 1,
                    );
                    $user_query = new WP_User_Query($args);

                    $ba_info = $user_query->results;
                }

                if (!empty($dealer_id)) {
                    $dealer_name = get_the_title($dealer_id);
                }

                $dealer_phone = get_post_meta($dealer_id, 'wpr_store_phone_number', true);
                $dealer_address = get_post_meta($dealer_id, 'wpr_store_address', true);
                $dealer_address2 = get_post_meta($dealer_id, 'wpr_store_address_2', true);
                $dealer_city = get_post_meta($dealer_id, 'wpr_store_city', true);
                $dealer_state = get_post_meta($dealer_id, 'wpr_store_state', true);
                $dealer_email = get_post_meta($dealer_id, 'wpr_store_email_address', true);

                if (empty($dealer_email)) {
                    $dealer_email = $this->wpr_generate_random_email();
                    update_post_meta($dealer_id, 'wpr_store_email_address', esc_attr($dealer_email));
                }
                // Switch to primary blog
                restore_current_blog();


                // get ontraport id
                $ontraport_id = get_user_meta($current_user->ID, 'wpr_ontraport_id', true);
                $ba_data = array();
                $ontraport_ba_id = '';

                // offer code
                $offer_code = $this->wpr_generate_offer_code(absint($categ_id), absint($offer_id));

                // Save to History CTA clicked
                $to_save = array(
                    'user_id' => $current_user->ID,
                    'post_id' => $offer_id,
                    'btn_type' => 'cta',
                    'cta_type' => $type_lc,
                    'date' => current_time('mysql'),
                    'dealer_id' => $dealer_id,
                    'brand_id' => $categ_id,
                );

                if (!empty($ba_info)) {
                    $to_save['abc_id'] = $ba_info[0]->ID;
                }

                $is_single = url_to_postid($current_url);
                if (0 === $is_single) {
                    $to_save['page_type'] = 0;
                } else {
                    $to_save['page_type'] = $offer_id;
                }

                $this->wpr_save_cta_button_click($to_save);

//				$image_id    = get_term_meta( absint( $categ_id ), 'category-image-id', true );
//				$brand_image = wp_get_attachment_image_url( $image_id, 'large' );
                $brand_image = get_term_meta($categ_id, 'wpr_brand_image_url', true);

                // BA exists
                if (!empty($ba_info)) {
                    // Save offer for My Prospects
                    $this->wpr_save_my_prospects($current_user->ID, $offer_id, $ba_info[0]->ID, $dealer_id);

                    foreach ($ba_info as $user) {
                        // BA DATA INFO
                        $ba_data = array(
                            'id' => $user->ID,
                            'first_name' => $user->first_name,
                            'last_name' => $user->last_name,
                            'phone' => $user->phone,
                            'position' => $user->position,
                        );

                        $ontraport_ba_id = get_user_meta($user->ID, 'wpr_ontraport_id', true);
                    }

                    // Send Ontraport data with BA
                    $contact_cat = array();

                    if ($categ_id && is_int($categ_id)) {
                        $child = get_category($categ_id);
                        if ($child->parent > 0) {
                            // Industry
                            array_push($contact_cat, $this->ontraport_tag[$child->parent]);
                        }
                        // Brand
                        array_push($contact_cat, $this->ontraport_tag[$categ_id]);

                        array_push($contact_cat, 63);

                        // Action button
                        if (!empty($ontraport_button)) {
                            array_push($contact_cat, $ontraport_button);
                        }

                        if ($offer_code) {
                            array_push($contact_cat, $offer_code);
                        }
                    }

                    $user_avatar_url = get_user_meta($ba_info[0]->ID, 'wpr_user_avatar', true);
                    $user_meta_picture = get_user_meta($ba_info[0]->ID, 'oa_social_login_user_picture', true);
                    $user_avatar = get_avatar_url($ba_info[0]->ID);
                    if ($user_meta_picture) {
                        $user_avatar = $user_meta_picture;
                    }
                    if ($user_avatar_url) {
                        $user_avatar = $user_avatar_url;
                    }

                    $posted_item = sprintf(' %s button pressed', ucfirst($button));

                    $requestParams_P = array(
                        'objectID' => ObjectType::CONTACT,
                        'id' => absint($ontraport_id),
                        'f1463' => esc_attr(html_entity_decode(get_the_title($offer_id))),
                        'f1588' => esc_url($this->wpr_get_post_image_source($offer_id)),
                        'f1468' => esc_attr($type_lc),
                        'f1439' => esc_attr($ba_data['first_name']),
                        'f1440' => esc_attr($ba_data['last_name']),
                        'f1441' => esc_attr($ba_data['phone']),
                        'f1470' => esc_attr($ba_data['position']),
                        'f1442' => esc_attr(html_entity_decode($dealer_name)),
                        'f1443' => esc_attr($dealer_phone),
                        'f1444' => esc_attr($dealer_address),
                        'f1445' => esc_attr($dealer_city),
                        'f1446' => esc_attr($dealer_state),
                        'f1585' => esc_url($user_avatar),
                        'f1586' => esc_url($brand_image),
                        'api_call' => $api_call,
                    );

                    // LC tags
                    $add_lc_tags = array(
                        'objectID' => ObjectType::CONTACT,
                        'ids' => absint($ontraport_id),
                        'add_list' => implode(',', $contact_cat),
                        'api_call' => $api_call,
                    );

                    $user_phone = get_user_meta($current_user->ID, 'phone', true);
                    $ba_contact_tags = array();

                    array_push($ba_contact_tags, 63);

                    $user_avatar_url = get_user_meta($current_user->ID, 'wpr_user_avatar', true);
                    $user_meta_picture = get_user_meta($current_user->ID, 'oa_social_login_user_picture', true);

                    $user_avatar = get_avatar_url($current_user->ID);
                    if ($user_meta_picture) {
                        $user_avatar = $user_meta_picture;
                    }
                    if ($user_avatar_url) {
                        $user_avatar = $user_avatar_url;
                    }

//					date_default_timezone_set( 'US/Eastern' );
//					$inquiry_date = strtotime( date( 'Y-m-d' ) );
//					$inquiry_time = date( 'h:i:s A' );

                    $inquiry_date = strtotime('now GMT+3');
                    $inquiry_time = date('h:i:s A', $inquiry_date);

                    $requestParams_BA = array(
                        'objectID' => ObjectType::CONTACT,
                        'id' => absint($ontraport_ba_id),
                        'f1464' => esc_attr(html_entity_decode(get_the_title(intval($offer_id)))),
                        'f1504' => esc_url(get_the_permalink(intval($offer_id))),
                        'f1469' => esc_attr($type_ba),
                        'f1447' => esc_attr($current_user->user_firstname),
                        'f1448' => esc_attr($current_user->user_lastname),
                        'f1449' => esc_url(get_permalink(get_page_by_path('lookbook'))),
                        'f1451' => esc_attr($user_phone),
                        'f1461' => esc_attr($current_user->user_email),
                        'f1584' => esc_url($user_avatar),
                        'f1520' => esc_attr(date('m/d/Y', $inquiry_date)),
                        'f1558' => esc_attr($inquiry_time),
                        'f1587' => esc_url($this->wpr_get_post_image_source($offer_id)),
                        'api_call' => $api_call,
                    );

                    // BA tags
                    $add_ba_tags = array(
                        'objectID' => ObjectType::CONTACT,
                        'ids' => absint($ontraport_ba_id),
                        'add_list' => implode(',', $ba_contact_tags),
                        'api_call' => $api_call,
                    );

                    try {
                        // Save data to be sent by cron
                        $result = $this->data(
                            array(
                                'prospect' => $requestParams_P,
                                'ba' => $requestParams_BA,
                            )
                        )->save()->dispatch()->wpr_generate_error();

                        if (!empty($result)) {
                            $this->wpr_write_ontraport_log('Save data to DB for Ontraport sending with exiting ABC', 'Failed');
                        } else {
                            $this->wpr_write_ontraport_log('Save data to DB for Ontraport sending with exiting ABC', 'Success');
                            $response = false;
                        }

                        // Send Tags
                        $result_tags = $this->data(
                            array(
                                'lc_tag' => $add_lc_tags,
                                'ba_tag' => $add_ba_tags,
                            )
                        )->save()->dispatch()->wpr_generate_error();

                        if (!empty($result_tags)) {
                            $this->wpr_write_ontraport_log('Save data tags to DB for Ontraport sending with exiting ABC', 'Failed');
                        } else {
                            $this->wpr_write_ontraport_log('Save data tags to DB for Ontraport sending with exiting ABC', 'Success');
                            $response = false;
                        }
                    } catch (Exception $e) {
                        $this->wpr_write_ontraport_log('Save data to DB for Ontraport sending with exiting ABC', 'Failed');
                    }

                } else {
                    // No BA Associated

                    $cron_tasks = array();

                    // Build HubSpot contact properties

                    if ($categ_id && is_int($categ_id)) {
                        // Brand
                        $ds_brand_cat = get_category($categ_id);

                        if ($ds_brand_cat->parent > 0) {
                            // Industry
                            $ds_industry_cat = get_category($ds_brand_cat->parent);

                            switch ($ds_industry_cat->slug) {
                                case 'automotive':
                                    $sc_type = __('Showroom Sales Person', 'wpr-dealers');
                                    break;
                                case 'fashion':
                                    $sc_type = __('Stylist', 'wpr-dealers');
                                    break;
                                case 'home-decor':
                                    $sc_type = __('Design Consultant or Showroom Sales Person', 'wpr-dealers');
                                    break;
                                case 'properties':
                                    $sc_type = __('Residential Broker', 'wpr-dealers');
                                    break;
                            }
                        }
                    }

                    // Send data to Ontraport ----------------------------------------------------------------

                    // Build Ontraport contact properties to update
                    $add_lc_fields = array(
                        'objectID' => ObjectType::CONTACT,
                        'id' => $this->wpr_get_ontraport_id($current_user->ID),
                        // Dealership Name
                        'f1442' => esc_attr(html_entity_decode($dealer_name)),
                        // Dealership Street Address
                        'f1444' => rtrim($dealer_address . ', ' . $dealer_address2, ', '),
                        // Dealership City
                        'f1445' => $dealer_city,
                        // Dealership State
                        'f1446' => $dealer_state,
                        // Post image source
                        'f1588' => esc_url($this->wpr_get_post_image_source($offer_id)),
                        // api call type
                        'api_call' => $api_call,
                    );

                    if ('map' != $to_do) {
                        $cron_tasks['prospect'] = $add_lc_fields;
                    }

                    // Ontraport contact tags
                    $contact_cat = array();

                    if ($categ_id && is_int($categ_id)) {
                        $child = get_category($categ_id);
                        if ($child->parent > 0) {
                            // Industry
                            array_push($contact_cat, $this->ontraport_tag[$child->parent]);
                        }

                        // Brand
                        array_push($contact_cat, $this->ontraport_tag[$categ_id]);
                    }

                    // NO BA
                    array_push($contact_cat, 48);

                    if (!empty($ontraport_button)) {
                        array_push($contact_cat, $ontraport_button);
                    }

                    if ($offer_code) {
                        array_push($contact_cat, $offer_code);
                    }

                    // LC tags
                    $add_lc_tags = array(
                        'objectID' => ObjectType::CONTACT,
                        'ids' => $this->wpr_get_ontraport_id($current_user->ID),
                        'add_list' => implode(',', array_filter($contact_cat)),
                        'api_call' => $api_call,
                    );

                    // Send data to HubSpot ------------------------------------------------------------------
//					$inquiry_date    = date_create( date( 'Y-m-d' ), timezone_open( 'UTC' ) );
//					date_default_timezone_set( 'US/Eastern' );
//					$inquiry_date    = date_create( date( 'm/d/Y' ) );
//					$inquiry_date_ts = $inquiry_date->getTimestamp() * 1000;

                    $date2 = new DateTime('today', new DateTimeZone('America/New_York'));
                    $inquiry_date_ts = strtotime($date2->format('Y-m-d H:i:s')) * 1000;

                    $date = new DateTime('now', new DateTimeZone('America/New_York'));
                    $inquiry_time = $date->format('h:i:s A');

                    $contact_properties = array(
                        'firstname' => $dealer_name,
                        'email' => esc_attr($dealer_email),
                        'phone' => esc_attr($dealer_phone),
                        'dealership_name' => $dealer_name,
                        'dealership_phone' => esc_attr($dealer_phone),
                        'dealership_email' => esc_attr($dealer_email),
                        'brand_name_being_sold' => isset($ds_brand_cat) ? $ds_brand_cat->name : '',
                        'dealership_id' => $dealer_id,
                        'prospect_id' => $current_user->ID,
                        'lc_first_name' => $current_user->user_firstname,
                        'lc_last_name' => $current_user->user_lastname,
                        'lc_phone' => $current_user->phone,
                        'lc_email' => $current_user->user_email,
                        'lc_zipcode' => $current_user->zip_code,
                        'offer_id' => isset($offer_id) ? $offer_id : '',
                        'posted_item_title_for_ba_msg' => isset($offer_id) ? html_entity_decode(get_the_title($offer_id)) : '',
                        'type_of_request_for_ba' => isset($type_ba) ? $type_ba : '',
                        'inquiry_date' => $inquiry_date_ts,
                        'inquiry_time' => $inquiry_time,
                        'type_of_abc' => isset($sc_type) ? $sc_type : '',
                        'trigger_voiq_call' => 'true',
                    );

                    $contact_properties_format = array();

                    foreach ($contact_properties as $c_p_key => $c_p_val) {
                        if (empty($c_p_val)) {
                            continue;
                        }

                        // Remove special chars and HTML entities
                        // if ( is_string($c_p_val) ) $c_p_val = preg_replace(['/ +/', '/[^A-Za-z0-9 ]/'], '', $c_p_val ) );

                        $contact_properties_format[] = array(
                            'property' => $c_p_key,
                            'value' => $c_p_val,
                        );
                    }

                    // Schedule HubSpot contact creation task
                    if ('map' != $to_do && !$this->wpr_is_in_waiting_list($dealer_id)) {
                        $cron_tasks['hs_abc'] = $contact_properties_format;
                    }

                    // Save data to be sent by cron
                    if (!empty($cron_tasks)) {
                        try {
                            // Send Fields
                            $result = $this->data($cron_tasks)->save()->dispatch()->wpr_generate_error();
                            if (!empty($result)) {
                                $this->wpr_write_ontraport_log('Save data to DB for Ontraport sending with NO ABC', 'Failed');
                            } else {
                                $this->wpr_write_ontraport_log('Save data to DB for Ontraport sending with NO ABC', 'Success');
                                $response = false;
                            }

                            // Send TAGS
                            $result_tags = $this->data(
                                array(
                                    'lc_tag' => $add_lc_tags,
                                )
                            )->save()->dispatch()->wpr_generate_error();

                            if (!empty($result_tags)) {
                                $this->wpr_write_ontraport_log('Save data tags to DB for Ontraport sending with exiting ABC', 'Failed');
                            } else {
                                $this->wpr_write_ontraport_log('Save data tags to DB for Ontraport sending with exiting ABC', 'Success');
                                $response = false;
                            }
                        } catch (Exception $e) {
                            $this->wpr_write_ontraport_log('Save data to DB for Ontraport sending with NO ABC', 'Failed');
                        }
                    }

                    $lc_order_nr = 1;
                    if ($this->wpr_is_in_waiting_list($dealer_id)) {
                        $lc_order_nr = 2;
                    }

                    if ($this->wpr_get_lc_order_nr($current_user->ID, $dealer_id)) {
                        $lc_order_nr = $this->wpr_get_lc_order_nr($current_user->ID, $dealer_id);
                    }

                    // Add to waiting list
                    $to_save = array(
                        'user_id' => $current_user->ID,
                        'post_id' => $offer_id,
                        'dealer_id' => $dealer_id,
                        'brand_id' => $categ_id,
                        'lc_order' => $lc_order_nr,
                        'btn_type' => $to_do,
                        'api_call' => $api_call,
                        'date' => current_time('mysql'),
                    );
                    $this->wpr_add_to_waiting_line($to_save);
                }

                return $response;
            } catch (Exception $e) {
                error_log('Cron task encountered an exception: ');
                error_log($e);

                // Remove from queue
                return false;
            }
        }

        /**
         * Task
         *
         * @param mixed $item Queue item to iterate over
         *
         * @return mixed
         */
        protected function task($item)
        {
            $response = true;

            try {
                $call_type = '';
                if (array_key_exists('api_call', $item)) {
                    $call_type = '_' . $item['api_call'];
                }
                $this->wp_ontraport_api_call($call_type);
                $response_ontraport = $this->_ontraport_api->object()->update($item);
                $this->wpr_write_ontraport_log('Execute cron job', $response_ontraport);

                $response_parse = json_decode($response_ontraport, true);

                if (0 == $response_parse['code']) {
                    $response = false;
                }

                return $response;
            } catch (Exception $e) {
                error_log('Cron task encountered an exception: ');
                error_log($e);

                // Remove from queue
                return false;
            }
        }

        /**
         * Add tags task
         *
         * @param $item
         *
         * @return bool
         */
        protected function add_tags_task($item)
        {
            $response = true;

            try {
                $call_type = '';
                if (array_key_exists('api_call', $item)) {
                    $call_type = '_' . $item['api_call'];
                }
                $this->wp_ontraport_api_call($call_type);
                $response_ontraport = $this->_ontraport_api->object()->addTag($item);
                $this->wpr_write_ontraport_log('Execute add tags cron job', $response_ontraport);

                $response_parse = json_decode($response_ontraport, true);
                if (0 == $response_parse['code']) {
                    $response = false;
                }

                return $response;
            } catch (Exception $e) {
                error_log('Cron task encountered an exception: ');
                error_log($e);

                // Remove from queue
                return false;
            }
        }

        /**
         * Create HubSpot contact
         *
         * @param $item
         *
         * @return bool
         */
        protected function hs_abc_task($properties)
        {
            $response = false;

            if (!is_array($properties)) {
                return false;
            }

            // Create HubSpot contact properties

            $abc_properties_created = get_option('wpr_hs_abc_properties_created', false);
            if (!$abc_properties_created) {
                $property_groups = $this->_hubspot_api->contactProperties()->getGroups();

                $this->hubspot_log('Get property groups', $property_groups);

                if (!in_array('cta_for_voiq', $property_groups)) {
                    $create_property_group = $this->_hubspot_api->contactProperties()->createGroup(
                        array(
                            'name' => 'cta_for_voiq',
                            'displayName' => 'CTA for VOIQ',
                        )
                    );

                    $this->hubspot_log('Create property group', $create_property_group);

                    $abc_properties = array(
                        // Dealership fields
                        array(
                            'name' => 'dealership_name',
                            'label' => __('Dealership Name', 'wpr-dealers'),
                        ),
                        array(
                            'name' => 'dealership_phone',
                            'label' => __('Dealership Phone', 'wpr-dealers'),
                        ),
                        array(
                            'name' => 'dealership_email',
                            'label' => __('Dealership Email', 'wpr-dealers'),
                        ),
                        array(
                            'name' => 'brand_name_being_sold',
                            'label' => __('Brand Name Being Sold', 'wpr-dealers'),
                        ),
                        array(
                            'name' => 'dealership_id',
                            'label' => __('Dealership ID', 'wpr-dealers'),
                        ),

                        // Prospect fields
                        array(
                            'name' => 'prospect_id',
                            'label' => __('Prospect ID', 'wpr-dealers'),
                        ),
                        array(
                            'name' => 'lc_first_name',
                            'label' => __('LC First Name', 'wpr-dealers'),
                        ),
                        array(
                            'name' => 'lc_last_name',
                            'label' => __('LC Last Name', 'wpr-dealers'),
                        ),
                        array(
                            'name' => 'lc_phone',
                            'label' => __('LC Phone', 'wpr-dealers'),
                        ),
                        array(
                            'name' => 'lc_email',
                            'label' => __('LC Email', 'wpr-dealers'),
                        ),
                        array(
                            'name' => 'lc_zipcode',
                            'label' => __('LC Zipcode', 'wpr-dealers'),
                        ),

                        // Offer of interest fields
                        array(
                            'name' => 'offer_id',
                            'label' => __('Offer ID', 'wpr-dealers'),
                        ),
                        array(
                            'name' => 'posted_item_title_for_ba_msg',
                            'label' => __('Posted Item Title for BA msg', 'wpr-dealers'),
                        ),
                        array(
                            'name' => 'type_of_request_for_ba',
                            'label' => __('Type of Request for BA', 'wpr-dealers'),
                        ),
                        array(
                            'name' => 'inquiry_date',
                            'label' => __('Inquiry Date', 'wpr-dealers'),
                            'type' => 'date',
                            'fieldType' => 'date',
                        ),
                        array(
                            'name' => 'inquiry_time',
                            'label' => __('Inquiry Time', 'wpr-dealers'),
                        ),

                        // Script Fields
                        array(
                            'name' => 'type_of_abc',
                            'label' => __('Type of ABC', 'wpr-dealers'),
                        ),
                        array(
                            'name' => 'trigger_voiq_call',
                            'label' => __('Trigger VOIQ Call', 'wpr-dealers'),
                            'fieldType' => 'booleancheckbox',
                        ),
                        array(
                            'name' => 'voiq_call_status',
                            'label' => __('VOIQ Call Status', 'wpr-dealers'),
                            'type' => 'enumeration',
                            'fieldType' => 'radio',
                            'options' => array(
                                array(
                                    'label' => __('NI - Not Interested', 'wpr-dealers'),
                                    'value' => 'NI',
                                ),
                                array(
                                    'label' => __('SC - Survey Completed', 'wpr-dealers'),
                                    'value' => 'SC',
                                ),
                                array(
                                    'label' => __('F - Failed', 'wpr-dealers'),
                                    'value' => 'F',
                                ),
                                array(
                                    'label' => __('DNC - Add to do not Call List', 'wpr-dealers'),
                                    'value' => 'DNC',
                                ),
                                array(
                                    'label' => __('WN - Wrong Number', 'wpr-dealers'),
                                    'value' => 'WN',
                                ),
                                array(
                                    'label' => __('NIS - Not in Service', 'wpr-dealers'),
                                    'value' => 'NIS',
                                ),
                            ),
                        ),
                    );

                    foreach ($abc_properties as $property) {
                        if (!isset($property['type'])) {
                            $property['type'] = 'string';
                        }
                        if (!isset($property['fieldType'])) {
                            $property['fieldType'] = 'text';
                        }
                        if (!isset($property['groupName'])) {
                            $property['groupName'] = 'cta_for_voiq';
                        }

                        $create_property = $this->_hubspot_api->contactProperties()->create($property);

                        $this->hubspot_log('Create property', $create_property);
                    }
                }

                update_option('wpr_hs_abc_properties_created', true);
            }

            // Decode HTML entities
            foreach ($properties as $prop_arr => $values) {
                if (is_string($values['value'])) {
                    $properties[$prop_arr]['value'] = html_entity_decode($values['value']);
                }
            }

            // Create HubSpot contact
            $create_contact = $this->_hubspot_api->contacts()->create($properties);

            // if ( $response_hs->getStatusCode() != '200' || $response_hs->getStatusCode() != 409 ) {
            // 	$response = $properties;
            // }

            $this->hubspot_log('Create contact', $create_contact);

            return $response;
        }

        /**
         * Memory exceeded
         *
         * Ensures the batch process never exceeds 90%
         * of the maximum WordPress memory.
         *
         * @return bool
         */
        protected function memory_exceeded()
        {
            $memory_limit = $this->get_memory_limit() * 0.9; // 90% of max memory
            $current_memory = memory_get_usage(true);
            $return = false;

            if ($current_memory >= $memory_limit) {
                $return = true;
            }

            return apply_filters($this->identifier . '_memory_exceeded', $return);
        }

        /**
         * Get memory limit
         *
         * @return int
         */
        protected function get_memory_limit()
        {
            if (function_exists('ini_get')) {
                $memory_limit = ini_get('memory_limit');
            } else {
                // Sensible default.
                $memory_limit = '128M';
            }

            if (!$memory_limit || -1 === $memory_limit) {
                // Unlimited, set to 32GB.
                $memory_limit = '32000M';
            }

            return intval($memory_limit) * 1024 * 1024;
        }

        /**
         * Time exceeded.
         *
         * Ensures the batch never exceeds a sensible time limit.
         * A timeout limit of 30s is common on shared hosting.
         *
         * @return bool
         */
        protected function time_exceeded()
        {
            $finish = $this->start_time + apply_filters($this->identifier . '_default_time_limit', 20); // 20 seconds
            $return = false;

            if (time() >= $finish) {
                $return = true;
            }

            return apply_filters($this->identifier . '_time_exceeded', $return);
        }

        /**
         * Handle cron healthcheck
         *
         * Restart the background process if not already running
         * and data exists in the queue.
         */
        public function handle_cron_healthcheck()
        {
            if ($this->is_process_running()) {
                // Background process already running.
                exit;
            }

            if ($this->is_queue_empty()) {
                // No data to process.
                $this->clear_scheduled_event();
                exit;
            }

            $this->handle();

            exit;
        }

        /**
         * Clear scheduled event
         */
        protected function clear_scheduled_event()
        {
            $timestamp = wp_next_scheduled($this->cron_hook_identifier);

            if ($timestamp) {
                wp_unschedule_event($timestamp, $this->cron_hook_identifier);
            }
        }

        /**
         * Complete.
         *
         * Override if applicable, but ensure that the below actions are
         * performed, or, call parent::complete().
         */
        protected function complete()
        {
            // Unschedule the cron healthcheck.
            $this->clear_scheduled_event();
        }

        /**
         * Return post image URL
         *
         * @param $post_id
         *
         * @return string
         */
        function wpr_get_post_image_source($post_id)
        {
            $image = '';
            if ($post_id) {
                $post_id = absint($post_id);

                $thumbnail_src = wp_get_attachment_image_src(get_post_thumbnail_id($post_id), 'medium');
                $external_image = get_post_meta($post_id, 'wpr_remote_featured_img', true);

                $content = apply_filters('the_content', get_post_field('post_content', $post_id));
                preg_match_all('((?:www\.)?(?:youtube(?:-nocookie)?\.com\/(?:v\/|watch\?v=|embed\/)|youtu\.be\/)([a-zA-Z0-9?&=_-]*))', $content, $urls);
                $urls = array_filter($urls);

                $wpr_img_url = '';
                if (!empty($urls)) {
                    $youtube_url = $urls[0][0];

                    $youtube_id = '';
                    if (strlen(trim($youtube_url)) > 0) {
                        $youtube_id = getYoutubeIdFromUrl($youtube_url);
                    }

                    if ($youtube_id) {
                        $wpr_sd_img_url = 'https://img.youtube.com/vi/' . trim($youtube_id) . '/sddefault.jpg';
                        $response_sd = wp_remote_head($wpr_sd_img_url);
                        if ($response_sd) {
                            $wpr_img_url = $response_sd;
                        } else {
                            $wpr_img_url = 'https://img.youtube.com/vi/' . trim($youtube_id) . '/hqdefault.jpg';
                        }
                    }
                }

                preg_match('/< *img[^>]*src *= *["\']?([^"\']*)/i', $content, $img_src);
                if (!empty($img_src)) {
                    $image_url = esc_url($img_src[1]);
                }

                if (has_post_thumbnail($post_id)) {
                    $image = esc_attr($thumbnail_src[0]);
                } elseif (!empty($external_image)) {
                    $image = esc_attr($external_image);
                } elseif (!empty($wpr_img_url)) {
                    $image = esc_attr($wpr_img_url);
                } elseif (!empty($image_url)) {
                    $image = $image_url;
                } else {
                    $image = get_stylesheet_directory_uri() . '/assets/images/post-thumb-cropped.svg';
                }
            }

            return $image;
        }

        /**
         * Save Ontraport response data in custom table
         *
         * @param $transaction_type
         * @param $response
         */
        protected function wpr_write_ontraport_log($transaction_type, $response)
        {
            global $wpdb;
            $response_parse = json_decode($response, true);

            $response_code = 1;
            if (isset($response_parse['code'])) {
                $response_code = $response_parse['code'];
            }

            $wpdb->insert(
                $wpdb->prefix . 'wpr_ontraport_log',
                array(
                    'api_url' => esc_attr($transaction_type),
                    'api_response' => maybe_serialize($response_parse),
                    'response_code' => esc_attr($response_code),
                    'date' => current_time('mysql', 1),
                ),
                array(
                    '%s',
                    '%s',
                    '%d',
                    '%s',
                )
            );
        }

        /**
         * Display logs
         */
        public function wpr_ontraport_display_logs()
        {
            $output = array();
            if (is_admin()) {
                global $wpdb;
                $start = $end = 0;

                if (isset($_GET['start'])) {
                    $start = absint($_GET['start']);
                }

                if (isset($_GET['length'])) {
                    $end = absint($_GET['length']);
                }

                $output['draw'] = absint($_GET['draw']);

                $logs = $wpdb->get_results(
                    $wpdb->prepare(
                        "SELECT * FROM {$wpdb->prefix}wpr_ontraport_log ORDER BY id DESC LIMIT %d OFFSET %d",
                        $end,
                        $start
                    )
                );

                $total_count = $wpdb->get_var("SELECT COUNT(*) FROM {$wpdb->prefix}wpr_ontraport_log");
                if ($total_count > 0) {
                    $output['recordsTotal'] = $total_count;
                    $output['recordsFiltered'] = $total_count;
                }

                $logs_array = array();
                if (!empty($logs)) {
                    foreach ($logs as $log) {

                        $process = 'Failed';
                        if (0 == $log->response_code) {
                            $process = 'Success';
                        }

                        $api_response = '';
                        if (!empty($log->api_response)) {
                            $api_response = maybe_unserialize($log->api_response);
                        }

                        $loop = array(
                            $log->api_url,
                            $log->date,
                            $process,
                            print_r($api_response, true),
                        );

                        array_push($logs_array, $loop);
                    }
                    $output['data'] = $logs_array;
                }
            }

            echo wp_json_encode($output);

            wp_die();
        }

        /**
         * Save HubSpot response to log table
         *
         * @param $transaction_type
         * @param $response
         */
        protected function hubspot_log($transaction_type, $response)
        {
            global $wpdb;

            $response_code = $response->getStatusCode();

            $wpdb->insert(
                $wpdb->prefix . 'wpr_hubspot_log',
                array(
                    'api_url' => esc_attr($transaction_type),
                    'api_response' => maybe_serialize($response->toArray()),
                    'response_code' => esc_attr($response_code),
                    'date' => current_time('mysql', 1),
                ),
                array(
                    '%s',
                    '%s',
                    '%d',
                    '%s',
                )
            );
        }

        /**
         * Display HubSpot logs
         */
        public function wpr_hubspot_display_logs()
        {
            $output = array();
            if (is_admin()) {
                global $wpdb;
                $start = $end = 0;

                if (isset($_GET['start'])) {
                    $start = absint($_GET['start']);
                }

                if (isset($_GET['length'])) {
                    $end = absint($_GET['length']);
                }

                $output['draw'] = absint($_GET['draw']);

                $logs = $wpdb->get_results(
                    $wpdb->prepare(
                        "SELECT * FROM {$wpdb->prefix}wpr_hubspot_log ORDER BY id DESC LIMIT %d OFFSET %d",
                        $end,
                        $start
                    )
                );

                $total_count = $wpdb->get_var("SELECT COUNT(*) FROM {$wpdb->prefix}wpr_hubspot_log");
                if ($total_count > 0) {
                    $output['recordsTotal'] = $total_count;
                    $output['recordsFiltered'] = $total_count;
                }

                $logs_array = array();
                if (!empty($logs)) {
                    foreach ($logs as $log) {

                        $process = 'Failed';
                        if (0 == $log->response_code) {
                            $process = 'Success';
                        }

                        $api_response = '';
                        if (!empty($log->api_response)) {
                            $api_response = maybe_unserialize($log->api_response);
                        }

                        $loop = array(
                            $log->api_url,
                            $log->date,
                            $process,
                            print_r($api_response, true),
                        );

                        array_push($logs_array, $loop);
                    }
                    $output['data'] = $logs_array;
                }
            }

            echo wp_json_encode($output);

            wp_die();
        }

        /**
         * Lookup an option from the options array
         *
         * @param $key
         *
         * @return null
         */
        public function get_option($key)
        {
            if (isset($this->options[$key]) && '' != $this->options[$key]) {
                return $this->options[$key];
            } else {
                return null;
            }
        }

        /**
         * Lookup an option from the options array
         *
         * @param $key
         *
         * @return null
         */
        public function get_hubspot_option($key)
        {
            if (isset($this->hubspot_options[$key]) && '' != $this->hubspot_options[$key]) {
                return $this->hubspot_options[$key];
            } else {
                return null;
            }
        }

        /**
         * Has the user favorited a specified post?
         *
         * @param $post_id
         * @param int $site_id
         * @param int $user_id
         * @param int $group_id
         *
         * @return bool
         */
        public function wpr_is_in_favorite($post_id, $site_id = 1, $user_id)
        {
            $favorites = $this->wpr_get_favorites($user_id, $site_id);

            if (empty($favorites)) {
                return false;
            }
            if (in_array($post_id, $favorites[0]['posts'])) {
                return true;
            }

            return false;
        }

        /**
         * Get User's Favorites by Site ID (includes a single site)
         *
         * @param $user_id
         * @param $site_id
         * @param $group_id
         *
         * @return array|mixed|void
         */
        public function wpr_get_favorites($user_id, $site_id)
        {
            $favorites = $this->getLoggedInFavorites($user_id, $site_id);
            $favorites = apply_filters('favorites/user/favorites', $favorites);

            return $favorites;
        }

        /**
         * Get Logged In User Favorites
         *
         * @param $user_id
         * @param $site_id
         * @param $group_id
         *
         * @return array|mixed
         */
        private function getLoggedInFavorites($user_id, $site_id)
        {
            $favorites = get_user_meta($user_id, 'simplefavorites');

            if (empty($favorites)) {
                return array(
                    array(
                        'site_id' => 1,
                        'posts' => array(),
                        'groups' => array(),
                    ),
                );
            }

            $favorites = $this->favoritesWithSiteID($favorites[0]);
            $favorites = $this->favoritesWithGroups($favorites);
            $favorites = $this->pluckSiteFavorites($site_id, $favorites);

            return $favorites;
        }

        /**
         * Check for Site ID in user's favorites
         *
         * @param $favorites
         *
         * @return array
         */
        private function favoritesWithSiteID($favorites)
        {
            foreach ($favorites as $key => $site_favorites) {
                if (!isset($favorites[$key]['site_favorites'])) {
                    continue;
                }
                $favorites[$key]['posts'] = $favorites[$key]['site_favorites'];
                unset($favorites[$key]['site_favorites']);
                if (isset($favorites[$key]['total'])) {
                    unset($favorites[$key]['total']);
                }
            }

            $new_favorites = array(
                array(
                    'site_id' => 1,
                    'posts' => $favorites,
                ),
            );

            return $new_favorites;
        }

        /**
         * Check for Groups array in user's favorites
         *
         * @param $favorites
         *
         * @return mixed
         */
        private function favoritesWithGroups($favorites)
        {
            $favorites[0]['groups'] = array(
                array(
                    'group_id' => 1,
                    'site_id' => $favorites[0]['site_id'],
                    'group_name' => __('Default List', 'favorites'),
                    'posts' => $favorites[0]['posts'],
                ),
            );

            return $favorites;
        }

        /**
         * Add to lookbook
         *
         * @param $user_id
         * @param $post_id
         * @param $site_id
         */
        private function wpr_add_to_lookbook($user_id, $post_id, $site_id)
        {
            $favorites = $this->wpr_get_favorites($user_id, $site_id);

            if (!$this->siteExists($site_id, $favorites)) {
                $favorites[] = array(
                    'site_id' => $site_id,
                    'posts' => array(),
                );
            }

            foreach ($favorites as $key => $site_favorites) {
                if ($site_favorites['site_id'] !== $site_id) {
                    continue;
                }
                $favorites[$key]['posts'][] = $post_id;

                // Add the default group if it doesn't exist yet
                if (!$this->groupsExist($site_favorites)) {
                    $favorites[$key]['groups'] = array(
                        array(
                            'group_id' => 1,
                            'site_id' => $site_id,
                            'group_name' => __('Default List', 'favorites'),
                            'posts' => array(),
                        ),
                    );
                }
                foreach ($favorites[$key]['groups'] as $group_key => $group) {
                    if ($group['group_id'] == 1) {
                        $favorites[$key]['groups'][$group_key]['posts'][] = $post_id;
                    }
                }
            }

            update_user_meta(intval($user_id), 'simplefavorites', $favorites);
        }

        /**
         * Site Exists
         *
         * @param $site_id
         * @param $meta
         *
         * @return bool
         */
        public static function siteExists($site_id, $meta)
        {
            foreach ($meta as $key => $site) {
                if ($site['site_id'] == $site_id) {
                    return true;
                }
            }

            return false;
        }

        /**
         * Groups Exists
         * checks if groups array is in favorites array yet
         * @since 2.2
         * @return boolean
         */
        public static function groupsExist($site_favorites)
        {
            if (isset($site_favorites['groups']) && !empty($site_favorites['groups'])) {
                return true;
            }

            return false;
        }

        /**
         * Pluck the site favorites from saved meta array
         * @since 1.1
         *
         * @param int $site_id
         * @param array $favorites (user meta)
         *
         * @return array
         */
        public static function pluckSiteFavorites($site_id, $all_favorites)
        {
            foreach ($all_favorites as $site_favorites) {
                if ($site_favorites['site_id'] == $site_id && isset($site_favorites['posts'])) {
                    return $site_favorites['posts'];
                }
            }

            return array();
        }

        /**
         * @param $user_id
         * @param $post_id
         */
        public static function wpr_save_my_prospects($user_id, $post_id, $abc_id = null, $dealer_id = null)
        {
            $to_save = array();
            $user_posts = get_user_meta($user_id, 'wpr_my_offers_clicked', true);

            if ($abc_id) {
                array_push($to_save, array('abc_id' => absint($abc_id)));
            }

            if ($dealer_id) {
                array_push($to_save, array('dealer_id' => absint($dealer_id)));
            }

            if ($user_posts) {
                if (!array_key_exists($post_id, $user_posts)) {
                    update_user_meta($user_id, 'wpr_my_offers_clicked', $user_posts + array($post_id => $to_save));
                }
            } else {
                update_user_meta($user_id, 'wpr_my_offers_clicked', array($post_id => $to_save));
            }
        }

        /*
		 * Register REST endpoints.
		 */
        function register_rest_routes()
        {
            register_rest_route('wpr-api/v1', '/abc', array(
                'methods' => 'POST',
                'callback' => array(&$this, 'rest_new_abc'),
            ));

            register_rest_route('wpr-ontraport-api/v1', '/ontraport_dealer', array(
                'methods' => 'POST',
                'callback' => array(&$this, 'rest_ontraport_response'),
            ));
        }

        /**
         * Update Dealership data
         *
         * @param WP_REST_Request $ontraport
         */
        function rest_ontraport_response(WP_REST_Request $ontraport)
        {
            $params = $ontraport->get_body_params();

            if (!empty($params)) {
                $dealership_id = absint($params['dealership_id']);
                $office_phone = esc_attr($params['office_phone']);
                $office_suite = esc_attr($params['office_suite']);
                $office_street_address = esc_attr($params['office_street_address']);
                $office_city = esc_attr($params['office_city']);
                $office_state = esc_attr($params['office_state']);
                $office_zip_code = esc_attr($params['office_zip_code']);

                switch_to_blog(2);
                if (array_key_exists('office_phone', $params)) {
                    if (!empty($office_phone)) {
                        update_post_meta($dealership_id, 'wpr_store_phone_number', $office_phone);
                    }
                    if (!empty($office_suite) && !empty($office_street_address)) {
                        update_post_meta($dealership_id, 'wpr_store_address', $office_suite . ' ' . $office_street_address);
                    }
                    if (!empty($office_city)) {
                        update_post_meta($dealership_id, 'wpr_store_city', $office_city);
                    }
                    if (!empty($office_state)) {
                        update_post_meta($dealership_id, 'wpr_store_state', $office_state);
                    }
                    if (!empty($office_zip_code)) {
                        update_post_meta($dealership_id, 'wpr_store_zip_code', $office_zip_code);
                    }
                } else {
                    wp_update_post(array(
                        'ID' => $dealership_id,
                        'post_status' => 'draft'
                    ));
                }
                restore_current_blog();
            }
        }

        /*
		 * Handle new ABC request.
		 */
        function rest_new_abc(WP_REST_Request $request)
        {
            $params = $request->get_json_params();

            if (isset($params['vid'])) {
                // Get contact information by ID from HubSpot
                try {
                    $hs_abc = $this->_hubspot_api->contacts()->getById($params['vid']);
                } catch (Exception $e) {
                    wp_send_json_error(null, 404);
                }

                if (empty($hs_abc['properties'])) {
                    wp_send_json_error(null, 400);
                }
                // Get properties
                $hs_props = $hs_abc['properties'];

                // Survey completed, create a new user for the ABC.
                if ($hs_props['voiq_call_status']['value'] == 'SC') {
                    switch_to_blog(2); // Switch to Xsellcast

                    $user_id = wp_insert_user(array(
                        'user_login' => $hs_props['email']['value'],
                        'user_email' => $hs_props['email']['value'],
                        'first_name' => $hs_props['firstname']['value'],
                        'last_name' => $hs_props['lastname']['value'],
                        'role' => 'business_associate',
                    ));

                    if (is_wp_error($user_id)) {
                        wp_send_json_error(array(
                            'message' => $user_id->get_error_message(),
                        ), 409);
                    }

                    // Set a quick login token for the ABC.
                    $bakey = wp_hash_password(wp_generate_password(50));
                    update_user_meta($user_id, 'wpr_ba_unique_token', $bakey);

                    // Set the phone number
                    if (!empty($hs_props['phone']['value'])) {
                        update_user_meta($user_id, 'phone', $hs_props['phone']['value']);
                    }

                    // Add Brand and Dealer associations
                    if (!empty($hs_props['dealership_id']['value'])) {
                        $dealership_id = intval($hs_props['dealership_id']['value']);
                        update_user_meta($user_id, 'wpr_dealer_association', $dealership_id);

                        $brand = get_post_meta($dealership_id, 'wpr_category_id', true);
                        if (!empty($brand)) {
                            update_user_meta($user_id, 'wpr_brand_association', $brand);
                        }
                    }

                    // Add prospect information (offer ID, prospect ID, type of request, inquiry time)
                    if (!empty($hs_props['offer_id']['value'])) {
                        update_user_meta($user_id, 'wpr_lc_offer_id', $hs_props['offer_id']['value']);
                    }
                    if (!empty($hs_props['prospect_id']['value'])) {
                        update_user_meta($user_id, 'wpr_lc_id', $hs_props['prospect_id']['value']);
                    }
                    if (!empty($hs_props['type_of_request_for_ba']['value'])) {
                        update_user_meta($user_id, 'wpr_lc_request_type', $hs_props['type_of_request_for_ba']['value']);
                    }
                    if (!empty($hs_props['inquiry_date']['value'])) {
                        update_user_meta($user_id, 'wpr_lc_inquiry_date', $hs_props['inquiry_date']['value']);
                    }
                    if (!empty($hs_props['inquiry_time']['value'])) {
                        update_user_meta($user_id, 'wpr_lc_inquiry_time', $hs_props['inquiry_time']['value']);
                    }

                    $dealer_zipcode = get_post_meta(intval($hs_props['dealership_id']['value']), 'wpr_store_zip_code', true);
                    $quick_login_url = get_permalink(get_page_by_path('qualified-enquiry')) . (!empty($bakey) ? '?bakey=' . $bakey : '');

                    $street = get_post_meta(absint($hs_props['dealership_id']['value']), 'wpr_store_address', true);
                    $city = get_post_meta(absint($hs_props['dealership_id']['value']), 'wpr_store_city', true);
                    $state = get_post_meta(absint($hs_props['dealership_id']['value']), 'wpr_store_state', true);

                    restore_current_blog();

                    // Get Prospect Avatar URL
                    $user_avatar_url = get_user_meta(absint($hs_props['prospect_id']['value']), 'wpr_user_avatar', true);
                    $user_meta_picture = get_user_meta(absint($hs_props['prospect_id']['value']), 'oa_social_login_user_picture', true);

                    $user_avatar = get_avatar_url(absint($hs_props['prospect_id']['value']));
                    if ($user_meta_picture) {
                        $user_avatar = $user_meta_picture;
                    }
                    if ($user_avatar_url) {
                        $user_avatar = $user_avatar_url;
                    }

                    $split_address = $this->wpr_split_address($street);

                    $request_params = array(
                        'objectID' => ObjectType::CONTACT,
                        // Call Status
                        'f1562' => $hs_props['voiq_call_status']['value'],
                        // Email
                        'email' => esc_attr($hs_props['email']['value']),
                        // First Name
                        'firstname' => esc_attr($hs_props['firstname']['value']),
                        // Last Name
                        'lastname' => esc_attr($hs_props['lastname']['value']),
                        // SMS Number
                        'sms_number' => esc_attr($hs_props['phone']['value']),
                        // Company
                        'company' => esc_attr($hs_props['dealership_name']['value']),
                        // Office Phone
                        'office_phone' => esc_attr($hs_props['dealership_phone']['value']),
                        // Zip Code
                        'zip' => esc_attr($dealer_zipcode),

                        // Office suite
                        'f1589' => esc_attr($split_address[1]),
                        // Office Street Address
                        'f1590' => esc_attr($street),
                        // Office City
                        'f1591' => esc_attr($city),
                        // Office state
                        'f1592' => esc_attr($state),
                        // Office Zip Code
                        'f1593' => esc_attr($dealer_zipcode),

                        // LC First name
                        'f1447' => esc_attr($hs_props['lc_first_name']['value']),
                        // LC Last name
                        'f1448' => esc_attr($hs_props['lc_last_name']['value']),
                        // LC Phone
                        'f1450' => esc_attr($hs_props['lc_phone']['value']),
                        'f1451' => esc_attr($hs_props['lc_phone']['value']),
                        // LC Email
                        'f1461' => esc_attr($hs_props['lc_email']['value']),
                        // LC Zipcode
                        'f1521' => esc_attr($hs_props['lc_zipcode']['value']),
                        // LC Avatar URL
                        'f1584' => esc_url($user_avatar),

                        // Offer Title
                        'f1464' => esc_attr($hs_props['posted_item_title_for_ba_msg']['value']),
                        // Offer Link
                        'f1504' => esc_url(get_the_permalink(intval($hs_props['offer_id']['value']))),
                        // Type of request
                        'f1469' => esc_attr($hs_props['type_of_request_for_ba']['value']),
                        // Inquiry Date
                        'f1520' => esc_attr(date('m-d-Y', $hs_props['inquiry_date']['value'] / 1000)),
                        // Inquiry Time
                        'f1558' => esc_attr($hs_props['inquiry_time']['value']),
                        // Quick Login URL
                        'f1559' => esc_url($quick_login_url),
                    );

                    // Send data to Ontraport
                    $this->wpr_send_new_abc_to_ontraport($hs_props, $request_params, $user_id);
                } elseif ('WN' == $hs_props['voiq_call_status']['value'] || 'NIS' == $hs_props['voiq_call_status']['value']) {
                    /**
                     * Send data to OP if Status is WN or NIS
                     */
                    switch_to_blog(2);
                    $street = get_post_meta(absint($hs_props['dealership_id']['value']), 'wpr_store_address', true);
                    $city = get_post_meta(absint($hs_props['dealership_id']['value']), 'wpr_store_city', true);
                    $state = get_post_meta(absint($hs_props['dealership_id']['value']), 'wpr_store_state', true);
                    $website = get_post_meta(absint($hs_props['dealership_id']['value']), 'wpr_store_website_address', true);
                    $split_address = $this->wpr_split_address($street);

                    $dealer_key = hash('ripemd160', absint($hs_props['dealership_id']['value']));
                    update_post_meta(absint($hs_props['dealership_id']['value']), 'wpr_dealer_unique_token', $dealer_key);
                    restore_current_blog();

                    $dealership_webhook_url = get_site_url() . '/wp-json/wpr-ontraport-api/v1/ontraport_dealer';

                    $request_params = array(
                        'objectID' => ObjectType::CONTACT,
                        // Company
                        'company' => esc_attr($hs_props['dealership_name']['value']),
                        // Representing Brand
                        'f1516' => esc_attr($hs_props['brand_name_being_sold']['value']),
                        // Office suite
                        'f1589' => esc_attr($split_address[1]),
                        // Office Street Address
                        'f1590' => esc_attr($street),
                        // Office City
                        'f1591' => esc_attr($city),
                        // Office state
                        'f1592' => esc_attr($state),
                        // Office Phone
                        'office_phone' => esc_attr($hs_props['dealership_phone']['value']),
                        // Website
                        'website' => esc_url($website),
                        // Dealership ID
                        'f1557' => absint($hs_props['dealership_id']['value']),
                        // Dealership Webhook URL
                        'f1594' => esc_url($dealership_webhook_url),
                    );

                    $tag_id = 283;
                    if ('NIS' == $hs_props['voiq_call_status']['value']) {
                        $tag_id = 284;
                    }

                    $this->wpr_dealership_marked_as_wn_or_nis($request_params, $tag_id);
                } else {
                    $dealership_id = intval($hs_props['dealership_id']['value']);
                    $prospect_id = absint($hs_props['prospect_id']['value']);

                    // Resend request to HubSpot with new dealership
                    $data_send = array(
                        'dealer_id' => $dealership_id,
                        'to_do' => $this->wpr_get_waiting_list_to_do($dealership_id, $prospect_id, absint($hs_props['offer_id']['value'])),
                        'offer_id' => absint($hs_props['offer_id']['value']),
                        'current_url' => esc_url_raw(get_the_permalink(absint($hs_props['offer_id']['value']))),
                        'user_id' => $prospect_id,
                    );
                    $this->wpr_rerun_abc_query($data_send);
                }

                wp_send_json_success();
            } else {
                wp_send_json_error(null, 400);
            }

            die();
        }

        /**
         * Send data to OP if Status is WN or NIS
         *
         * @param $params
         * @param $tag
         */
        function wpr_dealership_marked_as_wn_or_nis($params, $tag)
        {
            $this->wp_ontraport_api_call();
            $response = $this->_ontraport_api->object()->create($params);
            $this->wpr_write_ontraport_log('Task created in Ontraport for the CSR', $response);

            if ($response) {
                $response_parse = json_decode($response, true);

                $add_tags = array(
                    'objectID' => ObjectType::CONTACT,
                    'ids' => absint($response_parse['data']['id']),
                    'add_list' => $tag,
                );

                $response_tags = $this->_ontraport_api->object()->addTag($add_tags);
                $this->wpr_write_ontraport_log('Task created in Ontraport for the CSR add tags', $response_tags);
            }
        }

        /**
         * Rerun CTA Button Click
         *
         * @param $data_send
         */
        function wpr_rerun_abc_query($data_send)
        {
            $dealership_id = $data_send['dealer_id'];
            $prospect_id = $data_send['user_id'];
            $to_do = $data_send['to_do'];
            $offer_id = $data_send['offer_id'];
            $current_url = $data_send['current_url'];

            switch_to_blog(2);
            $category_id = get_post_meta($dealership_id, 'wpr_category_id', true);

            // Set Dealer on Draft
            wp_update_post(array(
                'ID' => $dealership_id,
                'post_status' => 'draft'
            ));

            // Search for next Dealer
            $args = array(
                'post_type' => 'wpr_dealer_info',
                'status' => 'publish',
                'meta_key' => 'wpr_category_id',
                'meta_value' => $category_id,
                'posts_per_page' => 1,
            );
            remove_action('pre_get_posts', array($this, 'wpr_hide_show_ba_offers_by_default'), 20);
            add_filter('posts_clauses', function ($clauses, $query) use ($prospect_id) {
                global $wpdb;

                $user_lat = false;
                $user_lng = false;

                if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
                    $ip = $_SERVER['HTTP_CLIENT_IP'];
                } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
                    $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
                } else {
                    $ip = $_SERVER['REMOTE_ADDR'];
                }

                $user_id = $prospect_id;
                if ($user_id > 0) {
                    $get_zip = get_user_meta($user_id, 'zip_code', true);

                    if (!empty($get_zip)) {
                        $geocode = file_get_contents('https://maps.google.com/maps/api/geocode/json?address=' . esc_attr($get_zip) . '&amp;sensor=false');
                        $output = json_decode($geocode);
                        if ($output->results) {
                            $user_lat = $output->results[0]->geometry->location->lat;
                            $user_lng = $output->results[0]->geometry->location->lng;
                        }
                    }
                }

                if ($user_lat == false) {
                    $geocode = @unserialize(file_get_contents('http://ip-api.com/php/' . $ip));
                    if ('fail' != $geocode['status']) {
                        $user_lat = $geocode['lat'];
                        $user_lng = $geocode['lon'];
                    }
                }

                //get the user's current location ( lat/ lng )
                if ($user_lat == false || $user_lng == false) {
                    $user_lat = '40.748194';
                    $user_lng = '-73.984925';
                }

                //abort if user's current location not exist
                if ($user_lat == false || $user_lng == false) {
                    return $clauses;
                }

                //set some values
                $radius = $this->radius; //can be any value
                $orderby = 'distance ASC'; //can be user with post_title, post_date, ID and so on...
                $units = 3959; //3959 for miles or 6371 for kilometers

                // join the location table into the query
                $clauses['join'] .= " INNER JOIN `{$wpdb->prefix}places_locator` gmwlocations ON $wpdb->posts.ID = gmwlocations.post_id ";
                // add the radius calculation and add the locations fields into the results
                $clauses['fields'] .= $wpdb->prepare(
                    ', gmwlocations.formatted_address , ROUND( %d * acos( cos( radians( %s ) ) * cos( radians( gmwlocations.lat ) ) * cos( radians( gmwlocations.long ) - radians( %s ) ) + sin( radians( %s ) ) * sin( radians( gmwlocations.lat) ) ),1 ) AS distance',
                    array($units, $user_lat, $user_lng, $user_lat)
                );
                $clauses['groupby'] = $wpdb->prepare(" $wpdb->posts.ID HAVING distance <= %d OR distance IS NULL", $radius);
                $clauses['orderby'] = $orderby;
                $clauses['order'] = 'DESC';

                return $clauses;
            }, 10, 2);

            $query = new WP_Query($args);
            $posts = $query->posts;

            $data_click = array();
            if ($query->have_posts()) {
                $data_click = array(
                    'dealer_id' => $posts[0]->ID,
                    'categ_id' => $category_id,
                    'to_do' => $to_do,
                    'offer_id' => $offer_id,
                    'current_url' => $current_url,
                    'user_id' => $prospect_id,
                );
            }

            wp_reset_postdata();
            add_action('pre_get_posts', array($this, 'wpr_hide_show_ba_offers_by_default'), 20, 1);

            restore_current_blog();

            if (!empty($data_click)) {
                try {
                    $this->data(
                        array(
                            'cta_clicked' => $data_click,
                        )
                    )->save()->dispatch()->wpr_generate_error();
                } catch (Exception $e) {
                    wp_send_json_error(null, 404);
                }
            }
        }

        /**
         * Send new ABC to Ontraport
         *
         * @param $hs_props
         * @param $params
         * @param $user_id
         */
        function wpr_send_new_abc_to_ontraport($hs_props, $params, $user_id)
        {
            $this->wp_ontraport_api_call();
            $response = $this->_ontraport_api->object()->saveOrUpdate($params);
            $this->wpr_write_ontraport_log('ABC register', $response);

            if ($response) {
                $response_parse = json_decode($response, true);
                if ($user_id) {
                    update_user_meta($user_id, 'wpr_ontraport_id', $response_parse['data']['id']);
                }

                global $wpdb;
                $query = $wpdb->get_row(
                    $wpdb->prepare(
                        "
                                SELECT
                                    page_type
                                FROM
                                    {$wpdb->prefix}wpr_cta_buttons
                                WHERE
                                    abc_id = 0
                                AND
                                    user_id = %d
                                AND
                                    post_id = %d
                                AND
                                    dealer_id = %d
                                AND
                                    btn_type = 'cta'
                                ORDER BY
                                    date
                                ASC
                            ",
                        absint($hs_props['prospect_id']['value']),
                        absint($hs_props['offer_id']['value']),
                        absint($hs_props['dealership_id']['value'])
                    )
                );

                $add_tags = array(
                    'objectID' => ObjectType::CONTACT,
                    'ids' => absint($response_parse['data']['id']),
                );

                if ($query) {
                    if (0 == $query->page_type) {
                        $add_tags['add_list'] = '54,206';
                    } else {
                        $add_tags['add_list'] = '54';
                    }
                } else {
                    $add_tags['add_list'] = '54';
                }

                $response_tags = $this->_ontraport_api->object()->addTag($add_tags);
                $this->wpr_write_ontraport_log('ABC register add tags', $response_tags);
            }
        }

        /**
         * @param $user_id
         *
         * @return bool
         */
        function wpr_user_data_filled($user_id)
        {
            $user_id = absint($user_id);

            $user_phone = get_user_meta($user_id, 'phone', true);
            $user_zip = get_user_meta($user_id, 'zip_code', true);

            if (!empty($user_phone) && !empty($user_zip)) {
                return true;
            }

            return false;
        }

        /**
         * Generate random email address
         *
         * @return string
         */
        function wpr_generate_random_email()
        {
            // array of possible top-level domains
            $tlds = array('com', 'net', 'org', 'biz', 'info');

            // string of possible characters
            $char = '0123456789abcdefghijklmnopqrstuvwxyz';

            // choose random lengths for the username ($ulen) and the domain ($dlen)
            $ulen = mt_rand(5, 10);
            $dlen = mt_rand(7, 17);

            // reset the address
            $a = "";

            // get $ulen random entries from the list of possible characters
            // these make up the username (to the left of the @)
            for ($i = 1; $i <= $ulen; $i++) {
                $a .= substr($char, mt_rand(0, strlen($char)), 1);
            }

            // wouldn't work so well without this
            $a .= "@";

            // now get $dlen entries from the list of possible characters
            // this is the domain name (to the right of the @, excluding the tld)
            for ($i = 1; $i <= $dlen; $i++) {
                $a .= substr($char, mt_rand(0, strlen($char)), 1);
            }

            // need a dot to separate the domain from the tld
            $a .= ".";

            // finally, pick a random top-level domain and stick it on the end
            $a .= $tlds[mt_rand(0, (sizeof($tlds) - 1))];

            return $a;
        }

        /**
         * Split a string into an array consisting of Street, House Number and
         * House extension.
         *
         * @param string $address Address string to split
         *
         * @return array
         */
        function wpr_split_address($address)
        {
            // Get everything up to the first number with a regex
            $hasMatch = preg_match('/^[^0-9]*/', $address, $match);
            // If no matching is possible, return the supplied string as the street
            if (!$hasMatch) {
                return array($address, "", "");
            }
            // Remove the street from the address.
            $address = str_replace($match[0], "", $address);
            $street = trim($match[0]);
            // Nothing left to split, return
            if (strlen($address == 0)) {
                return array($street, "", "");
            }
            // Explode address to an array
            $addrArray = explode(" ", $address);
            // Shift the first element off the array, that is the house number
            $housenumber = array_shift($addrArray);
            // If the array is empty now, there is no extension.
            if (count($addrArray) == 0) {
                return array($street, $housenumber, "");
            }
            // Join together the remaining pieces as the extension.
            $extension = implode(" ", $addrArray);

            return array($street, $housenumber, $extension);
        }
    }
}

function wpr_dealers_location() {
    return WPR_dealers_location::get_instance();
}

$wpr_dealers_location = wpr_dealers_location();
