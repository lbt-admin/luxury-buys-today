<?php
if ( ! defined( 'ABSPATH' ) ) {
	die( 'You are not allowed to call this page directly.' );
}
$active_tab = '';
if ( isset( $_GET['tab'] ) ) {
	$active_tab = $_GET['tab'];
}
?>
<div class="wrap">
    <div id="icon-options-drip" class="icon32"><br></div>
    <h2><?php echo __( 'Ontraport API Settings' ); ?></h2>

    <h3 class="nav-tab-wrapper">
        <a href="?page=wp-ontraport-api" class="nav-tab<?php echo $active_tab == '' ? ' nav-tab-active' : ''; ?>"><?php esc_html_e( 'General Options' ); ?></a>
        <a href="?page=wp-ontraport-api&tab=assign_categories" class="nav-tab<?php echo $active_tab == 'assign_categories' ? ' nav-tab-active' : ''; ?>"><?php esc_html_e( 'Assign categories' ); ?></a>
        <a href="?page=wp-ontraport-api&tab=ontraport_log" class="nav-tab<?php echo $active_tab == 'ontraport_log' ? ' nav-tab-active' : ''; ?>"><?php esc_html_e( 'Logs' ); ?></a>
    </h3>

	<?php if ( $active_tab == 'ontraport_log' ) { ?>
        <table id="ontraport_logs" class="display" cellspacing="0" width="100%">
            <thead>
            <tr>
                <th>API Call Type</th>
                <th>Date</th>
                <th>Response</th>
                <th>Response String</th>
            </tr>
            </thead>
        </table>
	<?php } else { ?>
        <form name="ontraport-settings-form" id="ontraport-settings-form" method="post" action="options.php">
			<?php
			settings_fields( 'ontraport_api_options' );
			if ( '' === $active_tab ) {
				do_settings_sections( 'wp-ontraport-api' );
			} else {
				do_settings_sections( 'wp-ontraport-api-assign-categories' );
			}
			?>
			<?php submit_button(); ?>
        </form>
	<?php } ?>

</div>
