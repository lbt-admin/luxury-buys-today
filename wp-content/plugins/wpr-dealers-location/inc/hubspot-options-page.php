<?php
if ( ! defined( 'ABSPATH' ) ) {
	die( 'You are not allowed to call this page directly.' );
}
$active_tab = '';
if ( isset( $_GET['tab'] ) ) {
	$active_tab = $_GET['tab'];
}
?>
<div class="wrap">
    <div id="icon-options-drip" class="icon32"><br></div>
    <h2><?php echo __( 'HubSpot API Settings' ); ?></h2>

    <h3 class="nav-tab-wrapper">
        <a href="?page=wp-hubspot-api" class="nav-tab<?php echo $active_tab == '' ? ' nav-tab-active' : ''; ?>"><?php esc_html_e( 'General Options' ); ?></a>
        <a href="?page=wp-hubspot-api&tab=hubspot_log" class="nav-tab<?php echo $active_tab == 'hubspot_log' ? ' nav-tab-active' : ''; ?>"><?php esc_html_e( 'Logs' ); ?></a>
    </h3>

	<?php if ( $active_tab == 'hubspot_log' ) { ?>
        <table id="hubspot_logs" class="display" cellspacing="0" width="100%">
            <thead>
            <tr>
                <th>API Call Type</th>
                <th>Date</th>
                <th>Response</th>
                <th>Response String</th>
            </tr>
            </thead>
        </table>
	<?php } else { ?>
        <form name="hubspot-settings-form" id="hubspot-settings-form" method="post" action="options.php">
			<?php
				settings_fields( 'hubspot_api_options' );

				do_settings_sections( 'wp-hubspot-api' );
				
				submit_button();
			?>
        </form>
	<?php } ?>

</div>
