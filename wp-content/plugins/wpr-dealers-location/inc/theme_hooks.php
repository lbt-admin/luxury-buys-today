<?php
add_action( 'wpr_display_cta_buttons_archive_list', 'wpr_display_cta_buttons_archive_list', 10, 1 );
/**
 * Display CTA Buttons on Archive List
 *
 * @param $edgar_cats
 */
function wpr_display_cta_buttons_archive_list( $edgar_cats ) {
	?>
    <a href="#" class="wpr-set-appointment" data-do="appointment"><?php _e( 'Set an appointment', 'novus' ); ?></a>
    <a href="#" class="wpr-learn-more" data-do="learn"><?php _e( 'Learn More', 'novus' ); ?></a>
	<?php if ( ! in_category( $edgar_cats, get_the_ID() ) ) { ?>
        <a href="#" class="wpr-price-request" data-do="price"><?php _e( 'Price', 'novus' ); ?></a>
	<?php } ?>
	<?php
}

add_action( 'wpr_display_cta_buttons_single_post', 'wpr_display_cta_buttons_single_post', 10, 2 );
/**
 * Display Single Article CTA Buttons
 *
 * @param $post_id
 * @param $edgar_cats
 */
function wpr_display_cta_buttons_single_post( $post_id, $edgar_cats ) {
	$output = '';
	if (!is_user_logged_in()) {
        $loginClass = ' popmake-33647 ';
    } else {
	    $loginClass='';
    }
    if ( $post_id ) {
		$count = get_post_meta( $post_id, 'simplefavorites_count', true );
		if ( $count == '' ) {
			$count = 0;
		}
		if(is_user_logged_in()){
		$output .= sprintf( '<a href="#" class="logout-display-none wpr-single-offer-buttons wpr-add-to-lookbook simplefavorite-button' .  $loginClass . '" data-postid="%d" data-siteid="%d" data-favoritecount="%d">%s</a>', $post_id, 1, $count, __( 'Add to lookbook' ) );
	}
	}
	if ( ! in_category( $edgar_cats, get_the_ID() ) ) {
		$output .=  '<a href="#" class="wpr-single-offer-buttons wpr-price-it wpr-price-request' .  $loginClass . '" data-do="price">Price it</a>';
	}
	else {

	$output .='<a href="#" class="wpr-single-offer-buttons wpr-price-it wpr-price-request single_button sticky-custom-img' .  $loginClass . '" data-do="price"><span class="bigScreen">Price it</span><span class="smallScreen" style="display:none;">Price it</span></a>'; }
	$output .='<a href="#" class="wpr-single-offer-buttons wpr-send-info wpr-learn-more' .  $loginClass . '" data-do="learn"><span class="bigScreen">Send me<br /> info</span><span class="smallScreen" style="display:none;">Email me</span></a>';

	$output .= '<a href="#" class="wpr-single-offer-buttons wpr-contact-me' .  $loginClass . '" data-do="contact"><span class="bigScreen">Contact<br /> me</span><span class="smallScreen" style="display:none;">Call me</span></a>';

	$output .= '<a href="#" class="wpr-single-offer-buttons wpr-make-appointment wpr-set-appointment' .  $loginClass . '" data-do="appointment"><span class="bigScreen">Make<br /> Appointment</span><span class="smallScreen" style="display:none;">Make APPOINTMENT</span></a>';
	if (!is_user_logged_in()) {

	$output .= sprintf( '<a href="#" class="login_display_none wpr-single-offer-buttons wpr-add-to-lookbook simplefavorite-button' .  $loginClass . '" data-postid="%d" data-siteid="%d" data-favoritecount="%d">%s</a>', $post_id, 1, $count, __( 'Add to lookbook' ) );
}
	echo $output;
}

add_action( 'wpr_display_dealer_info', 'wpr_display_dealer_info' );
/**
 * Display Dealer Info
 */
function wpr_display_dealer_info() {
	if ( is_user_logged_in() ) {
		echo do_shortcode( '[wpr-dealer-info]' );
	}
}

add_action( 'wpr_display_dealer_offers', 'wpr_display_dealer_offers' );
/**
 * Display BA Custom Offers
 */
function wpr_display_dealer_offers() {
	echo do_shortcode( '[wpr-ba-offers]' );
}
