/**
 * Created by ovidiugeorgeirodiu on 5/4/17.
 */
jQuery(document).ready(function ($) {
    if ($('#userbrand').length) {
        $("#userbrand option.level-0").attr('disabled','disabled');
        var brandClass = $('#userbrand'),
            dealerClass = $('#userdealer'),
            brandval = brandClass.val(),
            dealerSelect = brandClass.parents('form').find(dealerClass);

        wpr_ba_call_dealers(brandval, dealerSelect);

        $(brandClass).change(function () {
            var brand = $(this),
                brandval = brand.val();
            wpr_ba_call_dealers(brandval, dealerSelect);
        });

        dealerClass.select2({
            width: '250px'
        });
    }
});

function wpr_ba_call_dealers(brandval, dealerSelect) {
    jQuery.ajax({
        type: 'POST',
        url: wpr_ajax_object.wpr_dealer_ajax_url,
        data: {brand: brandval, action: 'wpr_get_dealer_name'},
        success: function (data) {
            dealerSelect.empty();
            var options = jQuery.parseJSON(data);
            for (i = 0; i < options.length; i++) {
                dealerSelect.append('<option value="' + options[i].value + '">' + options[i].text + '</option>');
            }
            dealerSelect.removeAttr('disabled').trigger("change.select2");
        }
    });
}