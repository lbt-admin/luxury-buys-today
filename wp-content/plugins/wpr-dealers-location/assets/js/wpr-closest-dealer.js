jQuery(document).ready(function ($) {



    jQuery('.pumClose').on('click' , function(event) {
      jQuery('#popmake-35259').popmake('close');
    });

  jQuery('.pumOpen').on('click' , function(event) {
    jQuery('#popmake-35259').popmake('open');
  });

    jQuery('.pumOpen33647').on('click' , function(event) {
        jQuery('#popmake-33647').popmake('open');
    });

    if ($('.simplefavorite-button').length) {


        $(document).ajaxComplete(function (event, xhr, settings) {
            if (settings.url.indexOf('admin-ajax.php') !== -1 && settings.data.indexOf('action=favorites_favorite') !== -1) {

                var data = {
                    action: 'wpr_my_offer_user',
                    nonce: wpr_ajax_object.nonce,
                    postsd: getUrlParameterFrom('postid', settings.data),
                    status: getUrlParameterFrom('status', settings.data),
                    wprDeal: $('.container').find('#wpr-dealer-location').attr('data-deal'),
                    wprCat: $('.container').find('#wpr-dealer-location').attr('data-loc')
                };
                $.post(wpr_ajax_object.wpr_dealer_ajax_url, data, function (res) {
                    if (Cookies.get('wpr_favorite_click')) {
                        swal({
                            title: '<i class="fa fa-file-image-o" aria-hidden="true"></i>',
                            html: '<b>Your Lookbook is updated.</b><br />',
                            animation: false,
                            showCancelButton: true,
                            confirmButtonColor: '#e8be5c',
                            cancelButtonColor: 'transparent',
                            confirmButtonText: wpr_ajax_object.wpr_popup_ok,
                            cancelButtonText: wpr_ajax_object.wpr_popup_facebook,
                            confirmButtonClass: 'btn btn-success wpr-popup-button-ok',
                            cancelButtonClass: 'wpr-please-like-us',
                            buttonsStyling: true
                        }).then(function () {

                        }, function (dismiss) {
                            if (dismiss === 'cancel') {
                                wpr_got_to_facebook();
                            }
                        });
                        Cookies.remove('wpr_favorite_click', {path: ''});
                    }

                }).fail(function (xhr, textStatus, e) {
                    swal({
                        title: '<i class="fa fa-file-image-o" aria-hidden="true"></i>',
                        text: wpr_ajax_object.wpr_pupup_ajax_error_message,
                        type: 'warning'
                    });
                });
            }
        });
    }

    if ($('.wpr-brand-selectbox').length) {
        var brandClass = $('.wpr-brand-selectbox select'),
            dealerClass = $('.populate-dealer-info select'),
            brandval = brandClass.val(),
            dealerSelect = brandClass.parents('form').find(dealerClass);

        //wpr_call_dealers(brandval, dealerSelect);

        $(brandClass).change(function () {
            var brand = $(this),
                brandval = brand.val();
            wpr_call_dealers(brandval, dealerSelect);
        });
    }

    $('#wprlistingTable').DataTable({
        "bFilter": true,
        "bInfo": false,
        "bUseColVis": true,
        rowReorder: {
            selector: 'td:nth-child(2)'
        },
        responsive: true
    });

    $('#prospectorInfio').DataTable({
        "bFilter": false,
        "bInfo": false,
        "bUseColVis": true,
        "bPaginate": false,
        "ordering": false,
        "info": false,
        rowReorder: {
            selector: 'td:nth-child(2)'
        },
        responsive: true
    });

    var requestRunning = false;
    $('.wpr-learn-more, .wpr-price-request, .wpr-set-appointment, .wpr-contact-me').on('click', function (e) {
      e.preventDefault();

        if (requestRunning) {
            return;
        }
        if ($(this).hasClass('popmake-33647')) {
            PUM.open(33647);
        } else {
            var wprInitial = $(this).html();

            $(this).html('<i class="fa fa-spinner fa-spin"></i> ' + wprInitial);

            var wprDeal = $('.container').find('#wpr-dealer-location').attr('data-deal');
            var wprCat = $('.container').find('#wpr-dealer-location').attr('data-loc');
            var wprTODO = $(this).attr('data-do');
            var wprOffer = $(this).closest('.entry-header').attr('data-deal-id');

            if (!wprOffer) {
                wprOffer = $(this).closest('.wpr-contact-buttons').find('.simplefavorite-button').attr('data-postid');
            }

            if ('map' !== wprTODO) {
                e.preventDefault();
            }

            var data = {
                action: 'save_cta',
                nonce: wpr_ajax_object.nonce,
                dealer_id: wprDeal,
                category_id: wprCat,
                todo: wprTODO,
                current_url: window.location.href,
                post_id: wprOffer
            };

            var fa_icon = 'fa-calendar';

            if ($(this).hasClass('wpr-price-request')) {
                fa_icon = 'fa-money';
            }

            if ($(this).hasClass('wpr-learn-more')) {
                fa_icon = 'fa-envelope-o';
            }

            if ($(this).hasClass('wpr-contact-me')) {
                fa_icon = 'fa-phone';
            }

            if ($(this).hasClass('simplefavorite-button')) {
                fa_icon = 'fa-file-image-o';
            }

            var ajaxOpts = {
                type: 'POST',
                url: wpr_ajax_object.wpr_dealer_ajax_url,
                data: data,
                success: function (data) {

                    $('i').remove('.fa-spinner');


                    if ('map' !== wprTODO && data) {
                      PUM.open(35259);
                        //var obj = jQuery.parseJSON(data);
                   /*   swal({
                        title: '<i class="fa ' + fa_icon + '" aria-hidden="true"></i>',
                        html: 'Thank you for your interest, you will be contacted soon about this product.',
                        animation: false,
                        showCancelButton: true,
                        confirmButtonColor: '#e8be5c',
                        cancelButtonColor: 'transparent',
                        confirmButtonText: wpr_ajax_object.wpr_popup_ok,
                        cancelButtonText: wpr_ajax_object.wpr_popup_facebook,
                        confirmButtonClass: 'btn btn-success wpr-popup-button-ok',
                        cancelButtonClass: 'wpr-please-like-us',
                        buttonsStyling: true
                      }).then(function () {

                      }, function (dismiss) {
                        if (dismiss === 'cancel') {
                          wpr_got_to_facebook();
                        }
                      });
                        /* if (obj.response) {
                            if ('success' === obj.response) {
                                swal({
                                    title: '<i class="fa ' + fa_icon + '" aria-hidden="true"></i>',
                                    html: obj.text,
                                    animation: false,
                                    showCancelButton: true,
                                    confirmButtonColor: '#e8be5c',
                                    cancelButtonColor: 'transparent',
                                    confirmButtonText: wpr_ajax_object.wpr_popup_ok,
                                    cancelButtonText: wpr_ajax_object.wpr_popup_facebook,
                                    confirmButtonClass: 'btn btn-success wpr-popup-button-ok',
                                    cancelButtonClass: 'wpr-please-like-us',
                                    buttonsStyling: true
                                }).then(function () {

                                }, function (dismiss) {
                                    if (dismiss === 'cancel') {
                                        wpr_got_to_facebook();
                                    }
                                });
                            } else if ('redirect' === obj.response) {
                                //window.location.href = obj.text;
                            } else {
                                swal({
                                    title: '<i class="fa ' + fa_icon + '" aria-hidden="true"></i>',
                                    html: obj.text,
                                    animation: false,
                                    showCancelButton: true,
                                    confirmButtonColor: '#e8be5c',
                                    cancelButtonColor: 'transparent',
                                    confirmButtonText: wpr_ajax_object.wpr_popup_ok,
                                    cancelButtonText: wpr_ajax_object.wpr_popup_facebook,
                                    confirmButtonClass: 'btn btn-success wpr-popup-button-ok',
                                    cancelButtonClass: 'wpr-please-like-us',
                                    buttonsStyling: true
                                }).then(function () {

                                }, function (dismiss) {
                                    if (dismiss === 'cancel') {
                                        wpr_got_to_facebook();
                                    }
                                });
                            }
                        } else {
                            //window.location.href = wpr_ajax_object.wpr_register_url;
                        } */
                    }
                },
                complete: function () {
                    requestRunning = false;
                },
                error: function (request, status, error) {
                    swal({
                        title: '<i class="fa ' + fa_icon + '" aria-hidden="true"></i>',
                        text: wpr_ajax_object.wpr_pupup_ajax_error_message,
                        type: 'warning'
                    });
                }
            };

            requestRunning = true;


            $.ajax(ajaxOpts);
        }

        return false;
    });

    $('.wpr-archive-header').on('click', '.wpr-learn-more, .wpr-price-request, .wpr-set-appointment, .wpr-contact-me', function (e) {
        if (requestRunning) {
            return;
        }

        var wprInitial = $(this).html();
        $(this).html('<i class="fa fa-spinner fa-spin"></i> ' + wprInitial);

        var wprDeal = $('.container').find('#wpr-dealer-location').attr('data-deal');
        var wprCat = $('.container').find('#wpr-dealer-location').attr('data-loc');
        var wprTODO = $(this).attr('data-do');
        var wprOffer = $(this).closest('.entry-header').attr('data-deal-id');

        if (!wprOffer) {
            wprOffer = $(this).closest('.wpr-contact-buttons').find('.simplefavorite-button').attr('data-postid');
        }

        if ('map' !== wprTODO) {
            e.preventDefault();
        }

            var data = {
                action: 'save_cta',
                nonce: wpr_ajax_object.nonce,
                dealer_id: wprDeal,
                category_id: wprCat,
                todo: wprTODO,
                current_url: window.location.href,
                post_id: wprOffer
            };

        var fa_icon = 'fa-calendar';

        if ($(this).hasClass('wpr-price-request')) {
            fa_icon = 'fa-money';
        }

        if ($(this).hasClass('wpr-learn-more')) {
            fa_icon = 'fa-envelope-o';
        }

        if ($(this).hasClass('wpr-contact-me')) {
            fa_icon = 'fa-phone';
        }

        if ($(this).hasClass('simplefavorite-button')) {
            fa_icon = 'fa-file-image-o';
        }

        var ajaxOpts = {
            type: 'POST',
            url: wpr_ajax_object.wpr_dealer_ajax_url,
            data: data,
            success: function (data) {

                $('i').remove('.fa-spinner');

                if ('map' !== wprTODO && data) {
                    var obj = jQuery.parseJSON(data);
                    if (obj.response) {
                        if ('success' === obj.response) {
                            swal({
                                title: '<i class="fa ' + fa_icon + '" aria-hidden="true"></i>',
                                html: obj.text,
                                animation: false,
                                showCancelButton: true,
                                confirmButtonColor: '#e8be5c',
                                cancelButtonColor: 'transparent',
                                confirmButtonText: wpr_ajax_object.wpr_popup_ok,
                                cancelButtonText: wpr_ajax_object.wpr_popup_facebook,
                                confirmButtonClass: 'btn btn-success wpr-popup-button-ok',
                                cancelButtonClass: 'wpr-please-like-us',
                                buttonsStyling: true
                            }).then(function () {

                            }, function (dismiss) {
                                if (dismiss === 'cancel') {
                                    wpr_got_to_facebook();
                                }
                            });
                        } else if ('redirect' === obj.response) {
                            window.location.href = obj.text;
                        } else {
                            swal({
                                title: '<i class="fa ' + fa_icon + '" aria-hidden="true"></i>',
                                html: obj.text,
                                animation: false,
                                showCancelButton: true,
                                confirmButtonColor: '#e8be5c',
                                cancelButtonColor: 'transparent',
                                confirmButtonText: wpr_ajax_object.wpr_popup_ok,
                                cancelButtonText: wpr_ajax_object.wpr_popup_facebook,
                                confirmButtonClass: 'btn btn-success wpr-popup-button-ok',
                                cancelButtonClass: 'wpr-please-like-us',
                                buttonsStyling: true
                            }).then(function () {

                            }, function (dismiss) {
                                if (dismiss === 'cancel') {
                                    wpr_got_to_facebook();
                                }
                            });
                        }
                    } else {
                        window.location.href = wpr_ajax_object.wpr_register_url;
                    }
                }
            },
            complete: function () {
                requestRunning = false;
            },
            error: function (request, status, error) {
                swal({
                    title: '<i class="fa ' + fa_icon + '" aria-hidden="true"></i>',
                    text: wpr_ajax_object.wpr_pupup_ajax_error_message,
                    type: 'warning'
                });
            }
        };

        requestRunning = true;
        $.ajax(ajaxOpts);
        return false;
    });

    if ($('#wpr-filter-offers').length) {
        $('input[type="checkbox"]').removeAttr('disabled');

        if (typeof Cookies.get('wpr_expand_filters') != 'undefined') {
            $('#wpr-filter-offers').show();
            $('#wpr-filter-expand-button').hide();
            Cookies.remove('wpr_expand_filters');
        }

        if (Cookies.get('wpr_selected_brands') === null || Cookies.get('wpr_selected_brands') === "" || typeof Cookies.get('wpr_expand_filters') == 'undefined') {
            $('#wpr-filter-offers').hide();
            $('#wpr-filter-expand-button').show();
        } else {
            var wpr_selected_brands = jQuery.parseJSON(Cookies.get('wpr_selected_brands'));
            if (wpr_selected_brands !== null) {
                $('input:checkbox').removeAttr('checked');

                $.each(wpr_selected_brands.brands, function (key, value) {
                    $('input#in-category-' + value).attr('checked', 'checked');
                });
            }
        }
    }

    $('#wpr-filter-offers-form').on('submit', function (e) {
        e.preventDefault();

        var wprInitial = $('.wpr-submit-filter button').text();
        $('.wpr-submit-filter button').html('<i class="fa fa-spinner fa-spin"></i> ' + wprInitial);

        var brand_selected = false;
        var $wpr_filters_offers = $('#wpr-filter-offers-form');

        if ($wpr_filters_offers.length) {
            $('#wpr-filter-offers-form > .wpr-list-brands').each(function () {
                $(this).find('ul > li:visible').each(function () {
                    $(this).find('input').each(function () {
                        if ($(this).prop("checked") == true) {
                            brand_selected = true;
                        }
                    });
                });
            });

            if (!brand_selected) {
                $('i').remove('.fa-spinner');
                swal({
                    title: '<i class="fa ' + "fa-exclamation-triangle" + '" aria-hidden="true"></i>',
                    html: "Please select at least one brand in order to continue.",
                    animation: false,
                    confirmButtonColor: '#e8be5c',
                    confirmButtonText: wpr_ajax_object.wpr_popup_ok,
                    confirmButtonClass: 'btn btn-success wpr-popup-button-ok',
                    buttonsStyling: true
                }).catch(swal.noop);
                return false;
            } else {
                $.ajax({
                    type: 'POST',
                    url: wpr_ajax_object.wpr_dealer_ajax_url,
                    data: {
                        form: $(this).serialize(),
                        nonce: wpr_ajax_object.nonce,
                        current_url: window.location.href,
                        action: 'wpr_choose_the_brands'
                    },
                    success: function (data) {
                        $('i').remove('.fa-spinner');

                        var obj = jQuery.parseJSON(data);
                        if (1 !== obj.url) {
                            // location.reload();
                            window.location.href = obj.url;
                        } else {
                            window.location.href = wpr_ajax_object.wpr_register_url;
                        }
                    }
                });
            }
        }
    });

    $('.wpr-clear-brand-selection').on('click', function (e) {
        e.preventDefault();
        $('#wpr-filter-offers-form input:checkbox').removeAttr('checked');
    });

    $('.category #wpr-filter-expand-button, .single #wpr-filter-expand-button').on('click', 'a', function () {
        Cookies.set('wpr_expand_filters', '1', {expires: 7});
    });

    $('#wpr-filter-expand-button').on('click', 'a', function (e) {
        e.preventDefault();
        $(this).hide();
        $('#wpr-filter-offers').slideToggle('slow');
    });

    $('.wpr-close-filter-panel').on('click', function (e) {
        e.preventDefault();
        $('#wpr-filter-offers').slideToggle('slow');
        $('#wpr-filter-expand-button a').show();
    });

    if ($('.wpr-load-dealer-data1').length) {
        $('.wpr-load-dealer-data1').html('<i class="fa fa-spinner fa-spin"></i>');
        var category = $('.wpr-load-dealer-data1').attr('data-cat');
        var type = $('.wpr-load-dealer-data1').attr('data-type');
        load_dealer_info(category, type);
    }
});

function load_dealer_info(data, type) {
    jQuery.ajax({
        type: 'POST',
        url: wpr_ajax_object.wpr_dealer_ajax_url,
        data: {
            nonce: wpr_ajax_object.nonce,
            action: 'wpr_load_dealer_display',
            onpage: data,
            loctype: type,
            currenturl: window.location.href
        },
        success: function (data) {
            jQuery('.wpr-load-dealer-data1').html(data);
            jQuery('i').remove('.fa-spinner');
        }
    });
let slug = $('#slugName').data('slug');
  jQuery.ajax({
    type: 'POST',
    url: wpr_ajax_object.wpr_dealer_ajax_url,
    data: {
      nonce: wpr_ajax_object.nonce,
      action: 'get_dealership',
      onpage: data,
      slug: slug,
      loctype: type,
      currenturl: window.location.href
    },
    success: function (data) {

    //   var parseObj = JSON.parse(data);
    //   console.log(parseObj.data);

    //   $('.wpr-dealer-details-single').find('.wpr-dealer-name').html(parseObj.data.name);
    //   $('.wpr-dealer-details-single').find('.wpr-map-location').html(parseObj.data.address.line_one + "," + parseObj.data.address.city + "," + parseObj.data.address.state);
    //   $('.wpr-dealer-details-single').find('.wpr-map-location').attr("href","https://www.google.com/maps?q=" + parseObj.data.address.line_one + ", " + parseObj.data.address.city + ", " + parseObj.data.address.state);


    }
  });
}

function wpr_got_to_facebook() {
    console.log('CLICKED!');
    window.open(wpr_ajax_object.wpr_popup_facebook_url, '_blank');
}

function wpr_call_dealers(brandval, dealerSelect) {
    jQuery.ajax({
        type: 'POST',
        url: wpr_ajax_object.wpr_dealer_ajax_url,
        data: {
            brand: brandval,
            action: 'wpr_get_dealer_name'
        },
        success: function (data) {
            dealerSelect.empty();
            var options = jQuery.parseJSON(data);
            for (i = 0; i < options.length; i++) {
                dealerSelect.append('<option value="' + options[i].value + '">' + options[i].text + '</option>');
            }
            dealerSelect.removeAttr('disabled').trigger("chosen:updated");
        }
    });
}

function getUrlParameterFrom(sParam, fromurl) {
    var sURLVariables = fromurl.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
}
