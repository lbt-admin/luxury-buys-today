jQuery(document).ready(function ($) {
    $('#ontraport_logs').DataTable({
        "searching": false,
        "ordering": false,
        "processing": true,
        "serverSide": true,
        "ajax": wpr_ajax_ontraport.wpr_ontraport_ajax_url + '?action=wpr_ontraport_display_logs'
    });

    $('#hubspot_logs').DataTable({
        "searching": false,
        "ordering": false,
        "processing": true,
        "serverSide": true,
        "ajax": wpr_ajax_ontraport.wpr_ontraport_ajax_url + '?action=wpr_hubspot_display_logs'
    });
});