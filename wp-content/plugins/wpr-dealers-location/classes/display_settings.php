<?php

namespace InitiateWPR\DisplaySettings;
/**
 * Load admin settings
 */
if ( ! class_exists( 'Wpr_Load_Settings' ) ) {
	/**
	 * Class Wpr_Load_Settings
	 * @package WPRLoadClasses\DisplaySettings
	 */
	class Wpr_Load_Settings {
		/**
		 * Wpr_Load_Settings constructor.
		 */
		function __construct() {
			add_action( 'init', array( &$this, 'wpr_dealer_info_cpt' ) );
			add_action( 'init', array( &$this, 'wpr_add_user_roles' ) );
			add_filter( 'template_include', array( &$this, 'wpr_call_custom_template' ), 99 );
			add_action( 'category_add_form_fields', array( &$this, 'wpr_add_category_url' ), 10, 2 );
			add_action( 'edit_category_form_fields', array( &$this, 'wpr_add_category_url_edit' ), 10, 2 );
			add_action( 'edited_category', array( &$this, 'wpr_save_category_link' ), 10, 2 );
			add_action( 'create_category', array( &$this, 'wpr_save_category_link' ), 10, 2 );
			add_action( 'admin_enqueue_scripts', array( &$this, 'wpr_add_admin_dealer_select' ) );
			add_action( 'pmxi_update_post_meta', array( &$this, 'wpr_dealer_imported' ), 10, 3 );
			add_action( 'wp_enqueue_scripts', array( &$this, 'wpr_enqueue_scripts' ), 99 );
			add_filter( 'widget_text', array( &$this, 'widget_text_shortcode' ) );

			add_filter( 'gform_pre_render_4', array( &$this, 'wpr_dealer_on_register' ) );
			add_filter( 'gform_pre_validation_4', array( &$this, 'wpr_dealer_on_register' ) );
			add_filter( 'gform_pre_submission_filter_4', array( &$this, 'wpr_dealer_on_register' ) );
			add_filter( 'gform_admin_pre_render_4', array( &$this, 'wpr_dealer_on_register' ) );
			add_action( 'gform_after_submission', array( &$this, 'wpr_ba_post_submission' ), 10, 2 );

			add_action( 'wp', array( &$this, 'wpr_redirect_my_custom_offer' ) );
		}

		/**
		 * Add new roles
		 */
		function wpr_add_user_roles() {
			add_role(
				'wpr_prospect',
				__( 'Prospect' ),
				array(
					'read'         => true,
					'edit_posts'   => false,
					'delete_posts' => false,
				)
			);

			add_role(
				'customer_service_representative',
				__( 'Customer Service Representative' ),
				array(
					'activate_plugins'       => true,
					'delete_others_pages'    => true,
					'delete_others_posts'    => true,
					'delete_pages'           => true,
					'delete_posts'           => true,
					'delete_private_pages'   => true,
					'delete_private_posts'   => true,
					'delete_published_pages' => true,
					'delete_published_posts' => true,
					'edit_dashboard'         => true,
					'edit_others_pages'      => true,
					'edit_others_posts'      => true,
					'edit_pages'             => true,
					'edit_posts'             => true,
					'edit_private_pages'     => true,
					'edit_private_posts'     => true,
					'edit_published_pages'   => true,
					'edit_published_posts'   => true,
					'edit_theme_options'     => true,
					'export'                 => true,
					'import'                 => true,
					'list_users'             => true,
					'manage_categories'      => true,
					'manage_links'           => true,
					'manage_options'         => true,
					'moderate_comments'      => true,
					'promote_users'          => true,
					'publish_pages'          => true,
					'publish_posts'          => true,
					'read_private_pages'     => true,
					'read_private_posts'     => true,
					'read'                   => true,
					'remove_users'           => true,
					'switch_themes'          => true,
					'upload_files'           => true,
					'customize'              => true,
				)
			);

			// Remove font awesome from gmw
			add_filter( 'gmw_font_awesome_enabled', '__return_false' );
		}

		/**
		 * Enqueue scripts
		 */
		function wpr_enqueue_scripts() {
			wp_enqueue_style( 'wpr-custom-dealer', plugin_dir_url( __FILE__ ) . '../assets/css/wpr-dealer.css' );
			wp_enqueue_style( 'sweetalert2', plugin_dir_url( __FILE__ ) . '../assets/css/sweetalert2.min.css' );
			wp_enqueue_script( 'sweetalert2', plugin_dir_url( __FILE__ ) . '../assets/js/sweetalert2.min.js', array( 'jquery' ), '1.0.0', true );
			wp_enqueue_style( 'sweetalert2.custom', plugin_dir_url( __FILE__ ) . '../assets/css/sweetalert2.custom.css' );

			wp_register_script( 'wpr-dealer-plugin-script', plugin_dir_url( __FILE__ ) . '../assets/js/wpr-closest-dealer.js', array( 'jquery' ), time(), true );
			$scheme = 'http';
			if ( is_ssl() ) {
				$scheme = 'https';
			}

			global $wp;
			$current_url = home_url( add_query_arg( array(), $wp->request ) );

			$login_register = sprintf(
				__( 'Please <a href="%1$s">Login</a> or <a href="%2$s">Register</a> to continue.', 'wpr-dealer' ),
				get_permalink( get_page_by_path( 'login' ) ) . '?redirect=' . $current_url,
				get_permalink( get_page_by_path( 'register' ) ) . '?redirect=' . $current_url
			);

			$is_logged_in = 0;
			if ( is_user_logged_in() ) {
				$is_logged_in = 1;
			}

			$register_url   = get_permalink( get_page_by_path( 'register' ) );
			$lookbook_url   = get_permalink( get_page_by_path( 'lookbook' ) );
			$homepage_url   = home_url();
			$popup_image    = plugin_dir_url( __FILE__ ) . '../assets/images/popup-image.png';
			$popup_facebook = 'https://luxurybuystoday.com/wp-content/plugins/wpr-dealers-location/assets/images/popup-facebook.png';

			$args = array(
				'nonce'                        => wp_create_nonce( 'wpr-dealer-location' ),
				'wpr_dealer_ajax_url'          => admin_url( 'admin-ajax.php', $scheme ),
				'wpr_login_message'            => $login_register,
				'wpr_logged_in'                => $is_logged_in,
				'wpr_register_url'             => esc_url( $register_url ),
				'wpr_lookbook_url'             => esc_url( $lookbook_url ),
				'wpr_homepage_url'             => esc_url( $homepage_url ),
				'wpr_popup_image'              => esc_url( $popup_image ),
				'wpr_popup_ok'                 => __( 'Ok' ),
				'wpr_popup_facebook'           => sprintf( '<img src="%s" /> Please like us on Facebook', esc_url( $popup_facebook ) ),
				'wpr_popup_facebook_url'       => __( 'https://www.facebook.com/LuxuryBuysToday/' ),
				'wpr_pupup_ajax_error_message' => __( 'There was an error. Please try again later.', 'wpr-dealer-location' ),
			);
			wp_localize_script( 'wpr-dealer-plugin-script', 'wpr_ajax_object', $args );
			wp_enqueue_script( 'wpr-dealer-plugin-script' );
		}

		/**
		 * Populate Dealer drop down on register form
		 *
		 * @param $form
		 *
		 * @return mixed
		 */
		function wpr_dealer_on_register( $form ) {
			foreach ( $form['fields'] as &$field ) {
				if ( 'select' !== $field->type || strpos( $field->cssClass, 'populate-dealer-info' ) === false ) {
					continue;
				}

				$brand = 30;
				if ( $_POST['input_7'] ) {
					$brand = absint( $_POST['input_7'] );
				}

				$args = array(
					'fields'         => 'ids',
					'post_type'      => 'wpr_dealer_info',
					'category'       => $brand,
					'post_status'    => 'publish',
					'orderby'        => 'title',
					'order'          => 'ASC',
					'posts_per_page' => - 1,
				);

				$posts = get_posts( $args );

				$choices = array();

				foreach ( $posts as $post ) {
					$choices[] = array(
						'text'       => get_the_title( $post ),
						'value'      => $post,
						'isSelected' => false,
					);
				}
				$field->placeholder = _( 'Select dealer', 'wpr-dealer-info' );
				$field->choices     = $choices;
			}

			return $form;
		}

		/**
		 * Associate BA post to his brand category & save extra data
		 *
		 * @param $entry
		 * @param $form
		 */
		public function wpr_ba_post_submission( $entry, $form ) {
			$post = get_post( $entry['post_id'] );
			if ( ! $post ) {
				return;
			}

			$ba_id = $post->post_author;
			if ( ! $ba_id ) {
				return;
			}

			$ba_brand = get_user_meta( $ba_id, 'wpr_brand_association', true );
			if ( $ba_brand ) {
				wp_update_post(
					array(
						'ID'            => $post->ID,
						'post_category' => array( $ba_brand ),
					)
				);
				update_post_meta( $post->ID, 'wpr_brand_id', $ba_brand );
			}

			$ba_dealer = get_user_meta( $ba_id, 'wpr_dealer_association', true );
			if ( $ba_dealer ) {
				update_post_meta( $post->ID, 'wpr_dealer_id', $ba_dealer );
			}
		}

		function wpr_add_admin_dealer_select( $hook ) {
			$scheme = 'http';
			if ( is_ssl() ) {
				$scheme = 'https';
			}

			if ( 'settings_page_wp-ontraport-api' == $hook || 'settings_page_wp-hubspot-api' == $hook ) {
				wp_enqueue_style( 'datatables-style', plugin_dir_url( __FILE__ ) . '../assets/css/datatables.min.css' );
				wp_enqueue_style( 'datatables-rowreorder-style', plugin_dir_url( __FILE__ ) . '../assets/css/rowReorder.dataTables.min.css' );
				wp_enqueue_style( 'datatables-responsive-style', plugin_dir_url( __FILE__ ) . '../assets/css/responsive.dataTables.min.css' );

				wp_enqueue_script( 'datatables-js', plugin_dir_url( __FILE__ ) . '../assets/js/datatables.min.js', array(), '20170206', true );
				wp_enqueue_script( 'datatables-cont', plugin_dir_url( __FILE__ ) . '../assets/js/wpr-datatables.js', array(), time(), true );

				$args = array(
					'nonce'                  => wp_create_nonce( 'wpr-ontraport-log' ),
					'wpr_ontraport_ajax_url' => admin_url( 'admin-ajax.php', $scheme ),
				);
				wp_localize_script( 'datatables-cont', 'wpr_ajax_ontraport', $args );
				wp_enqueue_script( 'datatables-cont' );
			}

			wp_enqueue_script( 'wpr-ba-custom-script', plugin_dir_url( __FILE__ ) . '../assets/js/admin_script.js', array( 'jquery' ), '1.0.0', true );

			$args = array(
				'nonce'               => wp_create_nonce( 'wpr-dealer-location' ),
				'wpr_dealer_ajax_url' => admin_url( 'admin-ajax.php', $scheme ),
			);
			wp_localize_script( 'wpr-ba-custom-script', 'wpr_ajax_object', $args );
			wp_enqueue_script( 'wpr-ba-custom-script' );
		}

		/**
		 * 404 redirect if user is not logged in
		 */
		public function wpr_redirect_my_custom_offer() {
			if ( is_singular() ) {
				$dealer_id = get_post_meta( get_the_ID(), 'wpr_dealer_id', true );
				if ( ! empty( $dealer_id ) && ! is_user_logged_in() ) {
					global $wp_query;
					$wp_query->set_404();
					status_header( 404 );
				}
			}
		}

		/**
		 * Save catalog url
		 *
		 * @param $term_id
		 */
		function wpr_save_category_link( $term_id ) {
			if ( isset( $_POST['wpr_brand_catalog'] ) ) {
				update_term_meta( $term_id, 'wpr_brand_catalog', esc_url( $_POST['wpr_brand_catalog'] ) );
			}

			if ( isset( $_POST['wpr_brand_image_url'] ) ) {
				update_term_meta( $term_id, 'wpr_brand_image_url', esc_url( $_POST['wpr_brand_image_url'] ) );
			}

			if ( isset( $_POST['wpr_brand_image_link'] ) ) {
				update_term_meta( $term_id, 'wpr_brand_image_link', esc_url( $_POST['wpr_brand_image_link'] ) );
			}

			if ( isset( $_POST['wpr_brand_link_text'] ) ) {
				update_term_meta( $term_id, 'wpr_brand_link_text', esc_attr( $_POST['wpr_brand_link_text'] ) );
			}
		}

		public static function widget_text_shortcode( $widget_text ) {
			$widget_text = preg_replace_callback( '/\[wpr-dealer-info.+?\]/i', array( __CLASS__, 'shortcode_on_preg_widget' ), $widget_text );

			return $widget_text;
		}

		public static function shortcode_on_preg_widget( $matches ) {
			if ( ! preg_match( '/context\=/i', $matches[0] ) ) :
				$matches[0] = str_replace( ']', ' context="widget"]', $matches[0] );
			endif;

			return do_shortcode( strtolower( $matches[0] ) );
		}

		/**
		 *
		 */
		function wpr_dealer_info_cpt() {
			include( plugin_dir_path( __FILE__ ) . '../inc/category-image.php' );
			require( plugin_dir_path( __FILE__ ) . '../inc/wpr-dealer-info-fields.php' );
		}

		/**
		 * Category catalog url
		 */
		function wpr_add_category_url() {
			?>
            <div class="form-field">
                <label for="wpr_brand_catalog"><?php _e( 'Catalog URL' ); ?></label>
                <input type="text" name="wpr_brand_catalog" id="wpr_brand_catalog" value="">
                <p class="description"><?php _e( 'Catalog URL' ); ?></p>
            </div>

            <div class="form-field">
                <label for="wpr_brand_image_url"><?php _e( 'Brand Image URL' ); ?></label>
                <input type="text" name="wpr_brand_image_url" id="wpr_brand_image_url" value="">
                <p class="description"><?php _e( 'Brand Image URL' ); ?></p>
            </div>

            <div class="form-field">
                <label for="wpr_brand_image_link"><?php _e( 'Brand Image Link' ); ?></label>
                <input type="text" name="wpr_brand_image_link" id="wpr_brand_image_link" value="">
                <p class="description"><?php _e( 'Brand Image Link' ); ?></p>
            </div>

            <div class="form-field">
                <label for="wpr_brand_link_text"><?php _e( 'Brand Link Text' ); ?></label>
                <input type="text" name="wpr_brand_link_text" id="wpr_brand_link_text" value="">
                <p class="description"><?php _e( 'Brand Link Text' ); ?></p>
            </div>
			<?php
		}

		/**
		 * Edit category catalog url
		 *
		 * @param $term
		 */
		function wpr_add_category_url_edit( $term ) {
			$wpr_brand_catalog    = get_term_meta( $term->term_id, 'wpr_brand_catalog', true );
			$wpr_brand_image_url  = get_term_meta( $term->term_id, 'wpr_brand_image_url', true );
			$wpr_brand_image_link = get_term_meta( $term->term_id, 'wpr_brand_image_link', true );
			$wpr_brand_link_text  = get_term_meta( $term->term_id, 'wpr_brand_link_text', true );
			?>
            <tr class="form-field">
                <th scope="row" valign="top"><label for="wpr_brand_catalog"><?php _e( 'Catalog URL' ); ?></label></th>
                <td>
                    <input type="text" name="wpr_brand_catalog" id="wpr_brand_catalog" value="<?php echo esc_url( $wpr_brand_catalog ); ?>"><br/>
                    <span class="description"><?php _e( 'Catalog URL' ); ?></span>
                </td>
            </tr>

            <tr class="form-field">
                <th scope="row" valign="top"><label for="wpr_brand_image_url"><?php _e( 'Brand Image URL' ); ?></label></th>
                <td>
                    <input type="text" name="wpr_brand_image_url" id="wpr_brand_image_url" value="<?php echo esc_url( $wpr_brand_image_url ); ?>"><br/>
                    <span class="description"><?php _e( 'Brand Image URL' ); ?></span>
                </td>
            </tr>

            <tr class="form-field">
                <th scope="row" valign="top"><label for="wpr_brand_image_link"><?php _e( 'Brand Image Link' ); ?></label></th>
                <td>
                    <input type="text" name="wpr_brand_image_link" id="wpr_brand_image_link" value="<?php echo esc_url( $wpr_brand_image_link ); ?>"><br/>
                    <span class="description"><?php _e( 'Brand Image Link' ); ?></span>
                </td>
            </tr>

            <tr class="form-field">
                <th scope="row" valign="top"><label for="wpr_brand_link_text"><?php _e( 'Brand Link Text' ); ?></label></th>
                <td>
                    <input type="text" name="wpr_brand_link_text" id="wpr_brand_link_text" value="<?php echo esc_attr( $wpr_brand_link_text ); ?>"><br/>
                    <span class="description"><?php _e( 'Brand Link Text' ); ?></span>
                </td>
            </tr>
			<?php

		}

		/**
		 * Define plugin path
		 *
		 * @return string
		 */
		function wpr_plugin_path() {
			return untrailingslashit( plugin_dir_path( __FILE__ ) );
		}

		/**
		 * Call custom template
		 *
		 * @param $template
		 *
		 * @return string
		 */
		function wpr_call_custom_template( $template ) {
			$plugin_path = $this->wpr_plugin_path() . '../template/';

			$template_name = explode( '/', $template );
			$template_name = end( $template_name );

			if ( file_exists( $plugin_path . $template_name ) ) {
				$new_template = $plugin_path . $template_name;

				return $new_template;
			}

			return $template;
		}

		/**
		 * Hook import & geo my wp
		 *
		 * @param $post_id
		 */
		function wpr_dealer_imported( $post_id, $m_key, $m_value ) {
			include_once( GMW_PT_PATH . '/includes/gmw-pt-update-location.php' );

			if ( function_exists( 'gmw_pt_update_location' ) && ( get_post_type( $post_id ) == 'wpr_dealer_info' ) ) {
				$dealer_fields = get_fields( $post_id );

				if ( ! empty( $dealer_fields ) ) {
					$street  = $dealer_fields['wpr_store_address'];
					$city    = $dealer_fields['wpr_store_city'];
					$state   = $dealer_fields['wpr_store_state'];
					$zipcode = $dealer_fields['wpr_store_zip_code'];
					$country = $dealer_fields['wpr_store_country'];
					$lat     = $dealer_fields['wpr_store_latitude'];
					$long    = $dealer_fields['wpr_store_longitude'];
				} else {
					$street  = get_post_meta( $post_id, 'wpr_store_address', true );
					$city    = get_post_meta( $post_id, 'wpr_store_city', true );
					$state   = get_post_meta( $post_id, 'wpr_store_state', true );
					$zipcode = get_post_meta( $post_id, 'wpr_store_zip_code', true );
					$country = get_post_meta( $post_id, 'wpr_store_country', true );
					$lat     = get_post_meta( $post_id, 'wpr_store_latitude', true );
					$long    = get_post_meta( $post_id, 'wpr_store_longitude', true );
				}

				if ( empty( $street ) || empty( $city ) || empty( $country ) || empty( $lat ) || empty( $long ) ) {
					return;
				}

				global $wpdb;
				$wpdb->replace(
					$wpdb->prefix . 'places_locator', array(
						'post_id'           => absint( $post_id ),
						'feature'           => 0,
						'post_type'         => get_post_type( $post_id ),
						'post_title'        => get_the_title( $post_id ),
						'post_status'       => 'publish',
						'street_number'     => '',
						'street_name'       => '',
						'street'            => $street,
						'apt'               => '',
						'city'              => $city,
						'state'             => $state,
						'state_long'        => '',
						'zipcode'           => $zipcode,
						'country'           => $country,
						'country_long'      => '',
						'address'           => '',
						'formatted_address' => '',
						'phone'             => '',
						'fax'               => '',
						'email'             => '',
						'website'           => '',
						'lat'               => $lat,
						'long'              => $long,
						'map_icon'          => '_default.png',
					)
				);
				//gmw_pt_update_location( $args );
			}
		}
	}
}
