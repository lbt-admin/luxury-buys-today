<?php

namespace InitiateWPR\AjaxCalls;
/**
 * Wpr_Ajax_Calls
 */
if ( ! class_exists( 'Wpr_Ajax_Calls' ) ) {
	/**
	 * Class Wpr_Ajax_Calls
	 * @package InitiateWPR\AjaxCalls
	 */
	class Wpr_Ajax_Calls {
		/**
		 * Wpr_Ajax_Calls constructor.
		 */
		function __construct() {
			add_action( 'wp_ajax_wpr_get_dealer_name', array( &$this, 'wpr_get_dealer_name' ) );
			add_action( 'wp_ajax_nopriv_wpr_get_dealer_name', array( &$this, 'wpr_get_dealer_name' ) );
		}

		/**
		 * Dealer ajax call
		 */
		public function wpr_get_dealer_name() {
			$items = array();
			$brand = absint( $_POST['brand'] );

			if ( $brand > 0 ) {
				$dealers = get_posts(
					array(
						'post_type'      => 'wpr_dealer_info',
						'category'       => $brand,
						'post_status'    => 'publish',
						'orderby'        => 'title',
						'order'          => 'ASC',
						'posts_per_page' => - 1,
					)
				);

				$items[] = array(
					'text'  => __( 'Select dealer...', 'wpr-dealer-info' ),
					'value' => '',
				);
				foreach ( $dealers as $dealer ) {
					$items[] = array(
						'text'  => $dealer->post_title,
						'value' => $dealer->ID,
					);
				}
			}

			echo json_encode( $items );
			wp_die();
		}
	}
}
