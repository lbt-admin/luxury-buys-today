<?php

namespace InitiateWPR\Tables;

/**
 * Create needed tables
 */
if ( ! class_exists( 'Wpr_Create_Tables' ) ) {
	/**
	 * Class Wpr_Create_Tables
	 * @package InitiateWPR\Tables
	 */
	class Wpr_Create_Tables {
		function __construct() {
			add_action( 'init', array( &$this, 'wpr_generate_tables' ) );
		}

		function wpr_generate_tables() {
			global $wpdb, $charset_collate;

			$wp_track_table = $wpdb->prefix . 'wpr_ontraport_log';
			if ( $wpdb->get_var( "show tables like '$wp_track_table'" ) !== $wp_track_table ) {

				$sql = 'CREATE TABLE `' . $wp_track_table . '` ( ';
				$sql .= '  `id` bigint(20) NOT NULL auto_increment, ';
				$sql .= '  `api_url` longtext NOT NULL, ';
				$sql .= '  `api_response` longtext, ';
				$sql .= '  `response_code` int(5) NOT NULL, ';
				$sql .= "  `date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',";
				$sql .= '  PRIMARY KEY (`id`) ';
				$sql .= ") $charset_collate;";
				require_once( ABSPATH . '/wp-admin/includes/upgrade.php' );
				dbDelta( $sql );
			}

			$pwr_cta_buttons = $wpdb->prefix . 'wpr_cta_buttons';
			if ( $wpdb->get_var( "show tables like '$pwr_cta_buttons'" ) !== $pwr_cta_buttons ) {
				$sql = 'CREATE TABLE `' . $pwr_cta_buttons . '` ( ';
				$sql .= '  `id` bigint(20) NOT NULL auto_increment, ';
				$sql .= '  `user_id` bigint(20) NOT NULL, ';
				$sql .= '  `post_id` bigint(20) NOT NULL, ';
				$sql .= '  `abc_id` bigint(20) NOT NULL, ';
				$sql .= '  `dealer_id` bigint(20) NOT NULL, ';
				$sql .= '  `brand_id` bigint(20) NOT NULL, ';
				$sql .= '  `btn_type` varchar(10) NOT NULL, ';
				$sql .= '  `btn_string` varchar(30) NOT NULL, ';
				$sql .= '  `page_type` int(1) NOT NULL, ';
				$sql .= "  `date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',";
				$sql .= '  PRIMARY KEY (`id`) ';
				$sql .= ") $charset_collate;";
				require_once( ABSPATH . '/wp-admin/includes/upgrade.php' );
				dbDelta( $sql );
			}

			$wpr_waiting_abc_list = $wpdb->prefix . 'wpr_waiting_abc_list';
			if ( $wpdb->get_var( "show tables like '$wpr_waiting_abc_list'" ) !== $wpr_waiting_abc_list ) {
				$sql = 'CREATE TABLE `' . $wpr_waiting_abc_list . '` ( ';
				$sql .= '  `id` bigint(20) NOT NULL auto_increment, ';
				$sql .= '  `brand_id` bigint(20) NOT NULL, ';
				$sql .= '  `dealer_id` bigint(20) NOT NULL, ';
				$sql .= '  `lc_id` bigint(20) NOT NULL, ';
				$sql .= '  `post_id` bigint(20) NOT NULL, ';
				$sql .= '  `lc_order` int(1) NOT NULL, ';
				$sql .= '  `btn_type` varchar(10) NOT NULL, ';
				$sql .= '  `api_call` varchar(10) NOT NULL, ';
				$sql .= "  `date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',";
				$sql .= '  PRIMARY KEY (`id`) ';
				$sql .= ") $charset_collate;";
				require_once( ABSPATH . '/wp-admin/includes/upgrade.php' );
				dbDelta( $sql );
			}

			$wp_track_table = $wpdb->prefix . 'wpr_hubspot_log';
			if ( $wpdb->get_var( "show tables like '$wp_track_table'" ) !== $wp_track_table ) {

				$sql = 'CREATE TABLE `' . $wp_track_table . '` ( ';
				$sql .= '  `id` bigint(20) NOT NULL auto_increment, ';
				$sql .= '  `api_url` longtext NOT NULL, ';
				$sql .= '  `api_response` longtext NOT NULL, ';
				$sql .= '  `response_code` int(5) NOT NULL, ';
				$sql .= "  `date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',";
				$sql .= '  PRIMARY KEY (`id`) ';
				$sql .= ") $charset_collate;";
				require_once( ABSPATH . '/wp-admin/includes/upgrade.php' );
				dbDelta( $sql );
			}

			$wp_track_table = $wpdb->prefix . 'wpr_ontraport_dealerships';
			if ( $wpdb->get_var( "show tables like '$wp_track_table'" ) !== $wp_track_table ) {
				$sql = 'CREATE TABLE `' . $wp_track_table . '` ( ';
				$sql .= '  `id` bigint(20) NOT NULL auto_increment, ';
				$sql .= '  `brand` text NOT NULL, ';
				$sql .= '  `latitude` varchar(255) NOT NULL, ';
				$sql .= '  `longitude` varchar(255) NOT NULL, ';
				$sql .= "  `ontraport_id` varchar(255) NOT NULL,";
				$sql .= "  `data` longtext NOT NULL,";
				$sql .= '  PRIMARY KEY (`id`) ';
				$sql .= ") $charset_collate;";
				require_once( ABSPATH . '/wp-admin/includes/upgrade.php' );
				dbDelta( $sql );
			}
		}
	}
}
