<?php
if ( ! defined( 'ABSPATH' ) ) {
	die( 'You are not allowed to call this page directly.' );
}
if ( ! class_exists( 'WPR_Dealer_widget' ) ) {
	/**
	 * Class WPR_Dealer_widget
	 */
	class WPR_Dealer_widget extends WP_Widget {
		/**
		 * WPR_Dealer_widget constructor.
		 */
		public function __construct() {
			$widget_options = array(
				'classname'   => 'wpr_dealer_widget',
				'description' => 'This displays Dealer Data',
			);
			parent::__construct( 'wpr_dealer_widget', 'Dealer Widget', $widget_options );
		}

		public function widget( $args, $instance ) {
			$post_id     = get_the_ID() ? get_the_ID() : '';
			$category_id = '';
			if ( $post_id ) {
				$category    = get_the_category( $post_id );
				$category_id = $category[0]->term_id;
			}
			echo sprintf( '<div class="wpr-load-dealer-data" data-cat="%d" data-type="single"></div>', absint( $category_id ) );
		}
	}
}
