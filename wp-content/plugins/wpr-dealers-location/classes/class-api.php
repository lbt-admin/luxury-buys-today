<?php
/**
 * Site Endpoints
 */
class WPR_Locations_API {
	public function __construct() {
		add_action(
			'rest_api_init',
			function() {
				$this->_load_routes();
			}
		);
	}

	/**
	 * Load routes
	 *
	 * Format:  endpoint@callback => http_method
	 *
	 * @return  void
	 */
	private function _load_routes() {
		$routes = array(
			'dealer@remove_dealer' => 'delete',
		);

		$this->_register_endpoints( $routes );
	}

	/**
	 * Register Site Endpoints
	 *
	 * @return  void
	 */
	private function _register_endpoints( $routes ) {
		if ( ! is_array( $routes ) ) {
			error_log( 'Site Endpoints Error: Routes must be an array.' );
			return;
		}

		foreach ( $routes as $action => $http_method ) {
			$action_params = explode( '@', $action );
			$endpoint      = $action_params[0];
			$method        = $action_params[1];

			register_rest_route(
				'lbt/v1',
				'/' . $endpoint,
				array(
					'methods'  => $http_method,
					'callback' => array( $this, $method ),
				)
			);
		}
	}

	/**
	 * Delete dealer from the site
	 *
	 * @param  \WP_REST_Request $request The request.
	 *
	 * @return \WP_Rest_Response
	 */
	public function remove_dealer( \WP_REST_Request $request ) {
		$params = $request->get_params();
        
		if ( empty( $params['email'] ) && empty( $params['ontraport_id'] ) ) {
			$response = array(
				'success' => false,
				'message' => __( 'You need to send dealer "email" or "ontraport_id" ' ),
			);

			return new \WP_REST_Response( $response );
		}

		if ( ! empty( $params['email'] ) ) {
			$email = $params['email'];

			if ( ! is_email( $email ) ) {
				$response = array(
					'success' => false,
					'message' => __( 'Invalid email' ),
				);

                return new \WP_REST_Response( $response );
			}

			switch_to_blog( 1 );

			global $wpdb;

			$table_name = $wpdb->prefix . 'wpr_ontraport_dealerships';

			$sql = $wpdb->prepare( "DELETE FROM {$table_name} WHERE data LIKE %s", '%' . $email . '%' );

			$dealer = $wpdb->query( $sql );

			if ( false !== $dealer && $dealer > 0 ) {
				$dealer_args = array(
					'post_type'  => 'wpr_dealer_info',
					'fields'     => 'ids',
					'meta_query' => array(
						array(
							'key'   => 'wpr_store_email_address',
							'value' => $email,
						),
					),
				);

				$dealers_query = new \WP_Query( $dealer_args );

				if ( $dealers_query->have_posts() ) {
					foreach ( $dealers_query->posts as $dealer_id ) {
						wp_delete_post( $dealer_id, true );
					}
				}

				$response = array(
					'success' => true,
					'message' => __( 'Dealer deleted' ),
				);
			} else {
				$response = array(
					'success' => false,
					'message' => __( 'Dealer not found' ),
				);
			}

			restore_current_blog();
		} elseif ( ! empty( $params['ontraport_id'] ) ) {
			switch_to_blog( 1 );

			global $wpdb;

			$table_name = $wpdb->prefix . 'wpr_ontraport_dealerships';

			$sql = $wpdb->prepare( "DELETE FROM {$table_name} WHERE ontraport_id = %d", $params['ontraport_id'] );

			$dealer = $wpdb->query( $sql );

			restore_current_blog();

			if ( false !== $dealer ) {
				$response = array(
					'success' => true,
					'message' => __( 'Dealer deleted' ),
				);
			} else {
				$response = array(
					'success' => false,
					'message' => __( 'Dealer not found' ),
				);
			}
		}

		return new \WP_REST_Response( $response );
	}
}

add_action( 'plugins_loaded', function() {
    new WPR_Locations_API();
});
