<?php

if ( ! defined('ABSPATH') ) {
    die( 'You are not allowed to call this page directly.' );
}

class WPR_Locations_Ontraport {
    public function __construct() {
        add_action( 'wpr_ontarport_dealerships_sync', function() {
            $this->_fetch_ontraport_dealerships();
        } );
		if( isset($_GET['test123123'])){
			try{
				$this->_fetch_ontraport_dealerships();
			}catch (Exception $e ) {
				echo $e->getMessage();
			}
			exit();
		}
        $this->_trigger_automatic_sync();
    }

    /**
     * Trigger automatic sync
     *
     * @return  void
     */
    private function _trigger_automatic_sync() {
        $next_run = wp_next_scheduled( 'wpr_ontarport_dealerships_sync' );

        if ( ! $next_run ) {
           $res = wp_schedule_event( time(), 'hourly', 'wpr_ontarport_dealerships_sync' );
        }
    }

    /**
     * Fetch ontraport dealerships and insert into database new table
     *
     * @return  void
     */
    private function _fetch_ontraport_dealerships() {
        $start = 0;
        $range = 50;

        global $wpdb;

        $table_name =  $wpdb->prefix . 'wpr_ontraport_dealerships';

        $sql = "SELECT ontraport_id FROM $table_name";

        $results = $wpdb->get_results( $sql );

        $ontraport_ids = [];

        if ( $results ) {
            foreach ( $results as $res ) {
                $ontraport_ids[] = $res->ontraport_id;
            }
        }

        while ( true ) {
            if ( ! function_exists('wp_lbt_ontraport_request') ) {
                break;
            }

            $dealerships = wp_lbt_ontraport_request(
                'Dealership-ABCs?start=' . $start . '&range=' . $range,
                [ 'method' => 'GET' ]
            );

            if ( empty( $dealerships ) || ! is_array( $dealerships ) || ! isset( $dealerships[0]['id'] ) || count( $ontraport_ids ) === count( $dealerships ) ) {
                break;
            }

            foreach ( $dealerships as $dealership ) {
                $condition = array();
                $condition[] = array(
                    'field' => array( 'field' => 'f2963' ),
                    'op' => '=',
                    'value' => array( 'value' => $dealership['id'] ),
                );

                $dealership_contact = wp_lbt_ontraport_request(
                    'Contacts?range=1&condition=' . urlencode( json_encode( $condition ) ),
                    [ 'method' => 'GET' ]
                );

                $dealership['contact_data'] = array();

                if ( is_array( $dealership_contact ) && ! empty( $dealership_contact ) ) {
                    $dealership['contact_data'] = $dealership_contact[0];
                }

                if ( in_array( $dealership['id'], $ontraport_ids ) ) {
                    $wpdb->update(
                        $table_name,
                        [
                            'brand' => sanitize_text_field( $dealership['f1732'] ),
                            'latitude' => sanitize_text_field( $dealership['f2823'] ),
                            'longitude' => sanitize_text_field( $dealership['f2824'] ),
                            'data' => maybe_serialize( $dealership )
                        ],
                        [
                            'ontraport_id' => sanitize_text_field( $dealership['id'] )
                        ],
                        [
                            '%s', '%s', '%s', '%s'
                        ],
                        [
                            '%s'
                        ]
                    );
                } else {
                    $wpdb->insert(
                        $table_name,
                        [
                            'brand' => sanitize_text_field( $dealership['f1732'] ?? '' ),
                            'latitude' => sanitize_text_field( $dealership['f2823'] ?? '' ),
                            'longitude' => sanitize_text_field( $dealership['f2824'] ?? '' ),
                            'ontraport_id' => sanitize_text_field( $dealership['id'] ),
                            'data' => maybe_serialize( $dealership )
                        ],
                        [
                            '%s', '%s', '%s', '%s', '%s'
                        ]
                    );
                }
            }

            $start += $range;
        }
    }
}

add_action( 'plugins_loaded', function() {
    new WPR_Locations_Ontraport();
});
