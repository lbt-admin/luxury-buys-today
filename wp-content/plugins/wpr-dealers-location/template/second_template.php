<?php
$ba_content     = '';
$have_ba_column = '';

$output .= '<div class="wpr-dealer-details-single' . $have_ba_column . ' testClass">';

if ( ! empty( $dealer_fields['wpr_store_address'] ) ) {
	$store_address .= $dealer_fields['wpr_store_address'] . ', ';
	$google_map    .= $dealer_fields['wpr_store_address'] . ', ';
} else {
	$store_address .= get_post_meta( get_the_ID(), 'wpr_store_address', true ) . ', ';
	$google_map    .= get_post_meta( get_the_ID(), 'wpr_store_address', true ) . ', ';
}
if ( ! empty( $dealer_fields['wpr_store_city'] ) ) {
	$store_address .= $dealer_fields['wpr_store_city'] . ', ';
	$google_map    .= $dealer_fields['wpr_store_city'] . ', ';
} else {
	$store_address .= get_post_meta( get_the_ID(), 'wpr_store_city', true ) . ', ';
	$google_map    .= get_post_meta( get_the_ID(), 'wpr_store_city', true ) . ', ';
}
if ( ! empty( $dealer_fields['wpr_store_state'] ) ) {
	$store_address .= $dealer_fields['wpr_store_state'] . ', ';
	$google_map    .= $dealer_fields['wpr_store_state'] . ', ';
} else {
	$store_address .= get_post_meta( get_the_ID(), 'wpr_store_state', true ) . ', ';
	$google_map    .= get_post_meta( get_the_ID(), 'wpr_store_state', true ) . ', ';
}

$output .= '<div class="wpr-dealer-name">' . get_the_title() . '</div>';

$output .= '<div class="wpr-dealer-address">';

$output .= substr( $store_address, 0, - 2 ) . '</div>';
if ( ! empty( $dealer_fields['wpr_store_hours'] ) ) {
	$store_hours = $dealer_fields['wpr_store_hours'];
} else {
	$store_hours = get_post_meta( get_the_ID(), 'wpr_store_hours', true );
}

if ( ! empty( $store_hours ) ) {
	$output .= '<div class="wpr-dealer-hours"><span>' . __( 'Store hours' ) . '</span>' . $store_hours . '</div>';
}

switch_to_blog( 1 );
$wpr_brand_catalog = get_term_meta( $category_id, 'wpr_brand_catalog', true );
restore_current_blog();

if ( ! empty( $google_map ) ) {
	$output .= '<div class="wpr-dealer-loc-button"><a href="https://www.google.com/maps?q=' . substr( $google_map, 0, - 2 ) . '" target="_blank" class="btn wpr-map-location" data-do="map">' . __( 'Map Location', 'wpr-dealers' ) . '</a></div>';
}

if ( $wpr_brand_catalog ) {
	$output .= '<div class="wpr-dealer-loc-button"><a href="' . esc_url( $wpr_brand_catalog ) . '" target="_blank" class="btn wpr-brand-catalog">' . __( 'See brand catalog', 'wpr-dealers' ) . ' <i class="fa fa-external-link" aria-hidden="true"></i></a></div>';
}

$output .= '</div>';

$output .= $ba_content;