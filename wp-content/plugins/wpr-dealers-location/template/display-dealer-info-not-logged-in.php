<?php
$ba_content     = '';
$have_ba_column = ''; 
$store_zip_code =  get_post_meta( get_the_ID(), 'wpr_store_zip_code', true );
if ( ! empty( $dealer_fields['wpr_store_address'] ) ) {
	$store_address .= $dealer_fields['wpr_store_address'] . ', ';
	$google_map    .= $dealer_fields['wpr_store_address'] . ', ';
} else {
	$store_address .= get_post_meta( get_the_ID(), 'wpr_store_address', true ) . ', ';
	$google_map    .= get_post_meta( get_the_ID(), 'wpr_store_address', true ) . ', ';
}
if ( ! empty( $dealer_fields['wpr_store_city'] ) ) {
	$store_address .= $dealer_fields['wpr_store_city'] . ', ';
  $google_map    .= $dealer_fields['wpr_store_city'] . ', ';
  $dealer_city   = $dealer_fields['wpr_store_city'];
} else {
	$store_address .= get_post_meta( get_the_ID(), 'wpr_store_city', true ) . ', ';
  $google_map    .= get_post_meta( get_the_ID(), 'wpr_store_city', true ) . ', ';
  $dealer_city  = get_post_meta( get_the_ID(), 'wpr_store_city', true );
}
if ( ! empty( $dealer_fields['wpr_store_state'] ) ) {
	$store_address .= $dealer_fields['wpr_store_state'] . ', ';
  $google_map    .= $dealer_fields['wpr_store_state'].' '.$store_zip_code;
  $dealer_state   = $dealer_fields['wpr_store_state'];
} else {
	$store_address .= get_post_meta( get_the_ID(), 'wpr_store_state', true ) . ', ';
  $google_map    .= get_post_meta( get_the_ID(), 'wpr_store_state', true ).' '.$store_zip_code;
  $dealer_state  =  get_post_meta( get_the_ID(), 'wpr_store_state', true );
}
if (!empty($dealer_fields['wpr_store_phone_number'])) {
    $phone_number = $dealer_fields['wpr_store_phone_number'];
} else {
    $phone_number = get_post_meta(get_the_ID(), 'wpr_store_phone_number', true);
}

$output .= '<div class="zm-dealer-details-single not-logged-in-brands' . $have_ba_column . '" id="zm-profile-description" data-deal-title="'.get_the_title().'" data-deal-address="' . substr( $google_map, 0, - 2 ) . '" data-deal-contact="'.$phone_number.'" data-deal-state="'.$dealer_state.'" data-deal-city="'.$dealer_city.'" >'; 
 

 
  $wprUser = get_current_user_id();
  if(is_user_logged_in()):
    $ontraport_id = get_user_meta(absint($wprUser), 'wpr_ontraport_id', true); 
    $check_existing_deal = get_user_meta( $wprUser, 'saved_deals_brands'); 
    $deal_id = get_the_ID();
    // echo '<br/>deal id:'.$deal_id.'<br/>'; 
   //  print_r($check_existing_deal);
    if(in_array($deal_id,$check_existing_deal)){  
      $save_brand_btn = '<div class="zm-save" remove-brand="active"><i class="fa fa-heart red" aria-hidden="true"></i>Saved</div>';
     }else{ 
      $save_brand_btn  = '<div class="zm-save txt-trim"><i class="fa fa-heart" aria-hidden="true"></i>Save</div>';
     }
    else:
      $save_brand_btn  = '<div class="zm-cta-btn txt-trim"><i class="fa fa-heart" aria-hidden="true"></i>Save</div>';
 endif;
$output .='
<div class="zm-profile-description">
               <div class="zm-profile-image">
                <img class="profile-img" src="/wp-content/uploads/2019/10/img-avatar.png">
               </div>
              <div class="zm-dealer-top-text">YOUR CLOSEST STORE </div> 
              <div class="zm-dealer-name"><h2>'.get_the_title().'</h2></div>  
              <p>'.$google_map.'</p>
</div>
<div class="zm-cta-actions">
  <div class="zm-contact-me zm-cta-btn" data-do="contact"><i class="fa icon-phone" aria-hidden="true"></i>Call me</div>
  <div class="zm-appt zm-cta-btn" data-do="appointment"><i class="fa icon-calendar"></i>Appt</div>
  <div class="zm-info zm-cta-btn" data-do="learn"><i class="fa icon-info" aria-hidden="true"></i>info</div>
  '.$save_brand_btn.'
</div>';

 
 
 
$output .= '<div class="zm-dealer-details">';
$output .= '<div class="zm-dealer-map">';

if ( ! empty( $google_map ) ) {
    $output .= '<i class="fa fa-map-marker"></i>' . '<a href="https://www.google.com/maps?q=' . substr( $google_map, 0, - 2 ) . '" target="_blank" class=" wpr-map-location" data-do="map">Directions</a>';
}
$output .=   '</div>';
$output .= '<div class="zm-dealer-phone">
<i class="fa icon-cell-phone" aria-hidden="true"></i><a href="tel:'.$phone_number.'">Request call back</a>
    </div>';
//$fb_link =  get_term_meta($category_id, 'wpr_brand_link_text', false);
//if($fb_link){
$output .= '<div class="zm-dealer-facebook">';
$output .= ' 
<i class="fa fa-facebook"></i><a href=" '. esc_url($wpr_brand_link_text).' ">Facebook</a>';
$output .=   '</div>';

//}
 
switch_to_blog( 1 );
$wpr_brand_catalog = get_term_meta( $category_id, 'wpr_brand_catalog', true );
restore_current_blog();


if ( $wpr_brand_catalog ) {
	$output .= '<div class="zm-view-brand-catalog"><i class="fa icon-share"></i><a href="' . esc_url( $wpr_brand_catalog ) . '" target="_blank" class=" wpr-brand-catalog">Brand Catalog </a></div>';
}
$output .= '</div><!-- zm-dealer-details -->';

if ( ! empty( $dealer_fields['wpr_store_hours'] ) ) {
    $store_hours = $dealer_fields['wpr_store_hours'];
} else {
    $store_hours = get_post_meta( get_the_ID(), 'wpr_store_hours', true );
}

if ( ! empty( $store_hours ) ) {


    $output .= '<div class="clearfix"></div><div class="wpr-dealer-hours wpr-dealer-loc-button store_hours">
       <div class="hoursTop profile store-hours"> <span>Store hours</span> <i class="fa fa-chevron-down"></i></div>
        <div class="timeSlots" style="display:none;">
        ';

//$managehours = explode("|", $store_hours);
$managehours = str_replace(',', '<hr>', $store_hours);
//foreach ($managehours as $hours) {
    $output .= '<span class="hoursBlock">' . $managehours. "</span>";
//}
$output .= ' </div>
    </div>'; 
}


$output .= '</div>'; 
 
$output .= $ba_content;