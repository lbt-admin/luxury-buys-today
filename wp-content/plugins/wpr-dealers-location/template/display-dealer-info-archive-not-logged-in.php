<?php
$ba_content = '';
$have_ba_column = '';

if (!empty($dealer_fields['wpr_store_address'])) {
    $store_address .= $dealer_fields['wpr_store_address'] . ', ';
    $google_map .= $dealer_fields['wpr_store_address'] . ', ';
} else {
    $store_address .= get_post_meta(get_the_ID(), 'wpr_store_address', true) . ', ';
    $google_map .= get_post_meta(get_the_ID(), 'wpr_store_address', true) . ', ';
}
if (!empty($dealer_fields['wpr_store_city'])) {
    $store_address .= $dealer_fields['wpr_store_city'] . ', ';
    $google_map .= $dealer_fields['wpr_store_city'] . ', ';
    $dealer_city = $dealer_fields['wpr_store_city'];
} else {
    $store_address .= get_post_meta(get_the_ID(), 'wpr_store_city', true) . ', ';
    $google_map .= get_post_meta(get_the_ID(), 'wpr_store_city', true) . ', ';
    $dealer_city = get_post_meta(get_the_ID(), 'wpr_store_city', true);
}
if (!empty($dealer_fields['wpr_store_state'])) {
    $store_address .= $dealer_fields['wpr_store_state'] . ', ';
    $google_map .= $dealer_fields['wpr_store_state'] . ', ';
    $dealer_state = $dealer_fields['wpr_store_state'];
} else {
    $store_address .= get_post_meta(get_the_ID(), 'wpr_store_state', true) . ', ';
    $google_map .= get_post_meta(get_the_ID(), 'wpr_store_state', true) . ', ';
    $dealer_state = get_post_meta(get_the_ID(), 'wpr_store_state', true);
}
if (!empty($dealer_fields['wpr_store_phone_number'])) {
    $phone_number = $dealer_fields['wpr_store_phone_number'];
} else {
    $phone_number = get_post_meta(get_the_ID(), 'wpr_store_phone_number', true);
}

//$output .= '<div class="zm-dealer-details-single main-brand-page-wrap' . $have_ba_column . '"
// id="zm-profile-description" data-deal-title="' . get_the_title() . '" data-deal-address="' . substr($google_map, 0, -2) . '" data-deal-contact="' . $phone_number . '" data-deal-state="' . $dealer_state . '" data-deal-city="' . $dealer_city . '" >';

$category_id = get_query_var('category_id');
$category_name = get_query_var('category_name');
$brand_image = get_query_var('brand_image');
$brand_catalog = get_query_var('brand_catalog');
$brand_link_text = get_query_var('brand_link_text');

if (is_user_logged_in()) {
    $wprUser = get_current_user_id();
    $ontraport_id = get_user_meta(absint($wprUser), 'wpr_ontraport_id', true);
    $check_existing_deal = get_user_meta($wprUser, 'saved_deals_brands');
    $deal_id = get_the_ID();
    if (in_array($deal_id, $check_existing_deal)) {
        $save_brand_btn = '<div class="zm-save" remove-brand="active"><i class="fa fa-heart red" aria-hidden="true"></i>Saved</div>';
    } else {
        $save_brand_btn = '<div class="zm-save"><i class="fa fa-heart" aria-hidden="true"></i>Save this brand</div>';
    }
} else {
    $save_brand_btn = '<div class="zm-cta-btn"><i class="fa fa-heart" aria-hidden="true"></i>Save this brand</div>';
}

?>
<div id="zm-profile-description"
     class="zm-dealer-details-single main-brand-page-wrap custom-new"
     data-deal-title="<?php echo get_the_title(); ?>"
     data-deal-address="<?php echo substr($google_map, 0, -2); ?>"
     data-deal-contact="<?php echo $phone_number; ?>"
     data-deal-state="<?php echo $dealer_state; ?>"
     data-deal-city="<?php echo $dealer_city; ?>">
    <div class="zm-profile-description main-brand-page">
        <div class="closest-store">YOUR CLOSEST STORE</div>
        <div class="zm-profile-media">
            <div class="profile-image">
                <img src="/wp-content/uploads/2020/07/img-avatar-png.png" alt="Profile Image">
            </div>
            <?php if (is_user_logged_in()) : ?>
                <div class="brand-logo">
                    <img src="<?php echo $brand_image; ?>" alt="<?php echo $category_name; ?>">
                </div>
            <?php endif; ?>
        </div>
        <div class="zm-dealer-name">
            <h2 class="zm-profile-title" data-deal-id="<?php echo get_the_ID(); ?>">
                <?php echo $category_name . ' of ' . get_the_title(); ?>
            </h2>
        </div>
        <div class="store-location"><?php echo substr($google_map, 0, -2); ?></div>
    </div>
    <div class="zm-profile-description main-brand-page">
        <div class="zm-cta-actions">
            <div class="zm-contact-me zm-cta-btn" data-do="contact">
                <i class="fa icon-phone" aria-hidden="true"></i>call me
            </div>
            <div class="zm-appt zm-cta-btn" data-do="appointment">
                <i class="fa icon-calendar"></i>make an appt
            </div>
            <div class="zm-info zm-cta-btn" data-do="learn">
                <i class="fa icon-info" aria-hidden="true"></i>request more info
            </div>
            <?php echo $save_brand_btn; ?>
            <span class="pumOpen" id="pumOpen" style="display: none;"></span>
        </div>
        <div class="zm-dealer-details">
            <div class="zm-dealer-map">
                <?php if (!empty($google_map)) : ?>
                    <i class="fa fa-map-marker"></i>
                    <a href="https://www.google.com/maps?q=<?php echo substr($google_map, 0, -2); ?>"
                       target="_blank" class=" wpr-map-location" data-do="map">Directions</a>
                <?php endif; ?>
            </div>
            <div class="zm-dealer-facebook">
                <i class="fa fa-facebook"></i>
                <a href="<?php echo esc_url($brand_link_text); ?>">Facebook</a>
            </div>
            <div class="zm-dealer-phone">
                <i class="fa icon-cell-phone" aria-hidden="true"></i>
                <a href="tel:<?php echo $phone_number; ?>">Request call back</a>
            </div>
            <?php if (!empty($brand_catalog)) : ?>
                <div class="zm-view-brand-catalog">
                    <i class="fa icon-share"></i>
                    <a href="<?php echo esc_url($brand_catalog); ?>"
                       target="_blank" class=" wpr-brand-catalog">Brand Catalog </a>
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>

<?php
/*if (is_user_logged_in()) {
    $wprUser = get_current_user_id();
    $ontraport_id = get_user_meta(absint($wprUser), 'wpr_ontraport_id', true);
    $check_existing_deal = get_user_meta($wprUser, 'saved_deals_brands');
    $deal_id = get_the_ID();

    if (in_array($deal_id, $check_existing_deal)) {
        $save_brand_btn = '<div class="zm-save" remove-brand="active"><i class="fa fa-heart red" aria-hidden="true"></i>Saved</div>';
    } else {
        $save_brand_btn = '<div class="zm-save"><i class="fa fa-heart" aria-hidden="true"></i>Save</div>';
    }
} else {
    $save_brand_btn = '<div class="zm-cta-btn"><i class="fa fa-heart" aria-hidden="true"></i>Save</div>';

}
$output .= '<div class="zm-profile-description main-brand-page" data-cat="' . $category_id . '"><div class="zm-logo-title">';
$output .= ' <div class="zm-profile-image">
                <img class="profile-img" src="/wp-content/uploads/2019/10/img-avatar.png">
             </div>
             <div class="closest-store">YOUR CLOSEST STORE</div>
             <div class="zm-dealer-name">
             <h2 class="zm-profile-title entry-header" data-deal-id="' . get_the_ID() . '">
                ' . $category_name . ' ' . get_the_title() . '
            </h2>
             </div>
             <div class="store-location">' . $google_map . '</div>';
$output .= '</div>';
if (is_user_logged_in()) {
    $output .= '<div class="zm-dealer-logo">' . '<img class="zm-car-logo" src=" ' . $brand_image . ' "></div>';
}
$output .= '</div>';


$output .= '
<div class="zm-profile-description main-brand-page">
              
               <div class="zm-profile-details">
               ';
if (get_the_title() != "") {
    $output .= '  
                   <p class="zm-profile-subtitle" style="display:none;"><a href="#"> Salesman In, Dealership &amp; Brand</a></p> 
                 ';
} else {
    $output .= '
          <div style="min-height:40px;">
           <strong>Contact us</strong>
          </div>
         ';
}


$output .= '<div class="zm-dealer-details">';
$output .= '<div class="zm-dealer-map">';

if (!empty($google_map)) {
    $output .= '<i class="fa fa-map-marker"></i>' . '<a href="https://www.google.com/maps?q=' . substr($google_map, 0, -2) . '" target="_blank" class=" wpr-map-location" data-do="map">Directions</a>';
}
$output .= '</div>';
$output .= '<div class="zm-dealer-phone">
<i class="fa icon-cell-phone" aria-hidden="true"></i><a href="tel:' . $phone_number . '">Request call back</a>
    </div>';
//$fb_link =  get_term_meta($category_id, 'wpr_brand_link_text', false);
//if($fb_link){
$output .= '<div class="zm-dealer-facebook">';
$output .= ' 
<i class="fa fa-facebook"></i><a href=" ' . esc_url($wpr_brand_link_text) . ' ">Facebook</a>';
$output .= '</div>';

//}

switch_to_blog(1);
$wpr_brand_catalog = get_term_meta($category_id, 'wpr_brand_catalog', true);

restore_current_blog();


if ($wpr_brand_catalog) {
    $output .= '<div class="zm-view-brand-catalog"><i class="fa icon-share"></i><a href="' . esc_url($wpr_brand_catalog) . '" target="_blank" class=" wpr-brand-catalog">Brand Catalog </a></div>';
}
$output .= '</div><!-- zm-dealer-details -->';


$output .= '
               </div>';
if (is_user_logged_in()) {
    $output .= ' 
        <div class="zm-cta-actions full-width-btn">
          <div class="zm-contact-me zm-cta-btn" data-do="contact"><i class="fa icon-phone" aria-hidden="true"></i>Call me</div>
          <div class="zm-appt zm-cta-btn" data-do="appointment"><i class="fa icon-calendar"></i>Make an appointment</div>
          <div class="zm-info zm-cta-btn" data-do="learn"><i class="fa icon-info" aria-hidden="true"></i>send me more info</div>
          ' . $save_brand_btn . '
          <span class="pumOpen" id="pumOpen" style="display: none;"></span>
        </div>';
}
$output .= '</div>';

$output .= '</div>';
if (!is_user_logged_in()) {
    $output .= ' 
    <div class="zm-cta-actions full-width-btn">
      <div class="zm-contact-me zm-cta-btn" data-do="contact"><i class="fa icon-phone" aria-hidden="true"></i>Call me</div>
      <div class="zm-appt zm-cta-btn" data-do="appointment"><i class="fa icon-calendar"></i>Make an appointment</div>
      <div class="zm-info zm-cta-btn" data-do="learn"><i class="fa icon-info" aria-hidden="true"></i>send me more info</div>
      ' . $save_brand_btn . '
      <span class="pumOpen" id="pumOpen" style="display: none;"></span>
    </div>';
}
$output .= $ba_content;*/