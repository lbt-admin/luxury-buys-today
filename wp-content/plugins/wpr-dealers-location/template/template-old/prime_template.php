<?php
$ba_content = '';
$have_ba_column = '';

if (!empty($dealer_fields['wpr_store_address'])) {
    $store_address .= $dealer_fields['wpr_store_address'] . ', ';
    $google_map .= $dealer_fields['wpr_store_address'] . ', ';
} else {
    $store_address .= get_post_meta(get_the_ID(), 'wpr_store_address', true) . ', ';
    $google_map .= get_post_meta(get_the_ID(), 'wpr_store_address', true) . ', ';
}
if (!empty($dealer_fields['wpr_store_city'])) {
    $store_address .= $dealer_fields['wpr_store_city'] . ', ';
    $google_map .= $dealer_fields['wpr_store_city'] . ', ';
} else {
    $store_address .= get_post_meta(get_the_ID(), 'wpr_store_city', true) . ', ';
    $google_map .= get_post_meta(get_the_ID(), 'wpr_store_city', true) . ', ';
}
if (!empty($dealer_fields['wpr_store_state'])) {
    $store_address .= $dealer_fields['wpr_store_state'] . ', ';
    $google_map .= $dealer_fields['wpr_store_state'] . ', ';
} else {
    $store_address .= get_post_meta(get_the_ID(), 'wpr_store_state', true) . ', ';
    $google_map .= get_post_meta(get_the_ID(), 'wpr_store_state', true) . ', ';
}

if (!empty($dealer_fields['wpr_store_phone_number'])) {
    $phone_number = $dealer_fields['wpr_store_phone_number'];
} else {
    $phone_number = get_post_meta(get_the_ID(), 'wpr_store_phone_number', true);
}


if (!empty($dealer_fields['wpr_store_hours'])) {
    $store_hours = $dealer_fields['wpr_store_hours'];
} else {
    $store_hours = get_post_meta(get_the_ID(), 'wpr_store_hours', true);
}

switch_to_blog(1);
$wpr_brand_catalog = get_term_meta($category_id, 'wpr_brand_catalog', true);
restore_current_blog();

$output .= '
<div class="wpr-dealer-details-single' . $have_ba_column . ' dealer-box">

    <div class="wpr-dealer-logo 123">


        Looking
        to
        buy
        ' . $category_name . '?
        Here
        is
        the
        closest
        location
<br>

    </div>

<hr>

    <div class="wpr-dealer-name">
       ' . get_the_title() . '
    </div>

    <div class="wpr-dealer-address wpr-dealer-loc-button">
       <i class="fa fa-map-marker"></i> <a href="https://www.google.com/maps?q=' . substr($google_map, 0, -2) . '"
           target="_blank"
           class=" wpr-map-location"
           data-do="map">
            ' . substr($store_address, 0, -2) . '</a>


    </div>


    <div class="wpr-dealer-phone wpr-dealer-loc-button">
      <i class="fa fa-phone"></i> ' . $phone_number . '
    </div>


    <div class="wpr-display-social-url wpr-dealer-loc-button">
       <i class="fa fa-facebook-f"></i> <a href="' . $wpr_brand_link_text . '"
           target="_blank"
           class=" ">' . $wpr_brand_link_text . '</a>
    </div>


    <div class="wpr-view-brand-catalog wpr-dealer-loc-button">
       <i class="fa fa-mail-forward"></i>  <a href="' . $wpr_brand_catalog . '"
           target="_blank"
           class=" wpr-brand-catalog">View
            Brand
            Catalog
            </a>
    </div>


    <div class="wpr-dealer-hours wpr-dealer-loc-button">
       <div class="hoursTop"><i class="fa fa-clock-o"></i>  <span>Store hours</span> <i class="fa fa-chevron-down"></i></div>
        <div class="timeSlots" style="display:none;">
           ';

$managehours = explode("|", $store_hours);
foreach ($managehours as $hours) {
    $output .= '<span class="hoursBlock">' . $hours . "</span><br>";
}
$output .= ' </div>
    </div>


</div>
';

$output .= $dealer_fields;




