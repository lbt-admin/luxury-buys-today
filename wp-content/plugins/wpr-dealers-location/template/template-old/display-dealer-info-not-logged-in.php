<?php
$ba_content     = '';
$have_ba_column = '';

$output .= '<div class="wpr-dealer-details-single' . $have_ba_column . ' testClass">';

if ( ! empty( $dealer_fields['wpr_store_address'] ) ) {
	$store_address .= $dealer_fields['wpr_store_address'] . ', ';
	$google_map    .= $dealer_fields['wpr_store_address'] . ', ';
} else {
	$store_address .= get_post_meta( get_the_ID(), 'wpr_store_address', true ) . ', ';
	$google_map    .= get_post_meta( get_the_ID(), 'wpr_store_address', true ) . ', ';
}
if ( ! empty( $dealer_fields['wpr_store_city'] ) ) {
	$store_address .= $dealer_fields['wpr_store_city'] . ', ';
	$google_map    .= $dealer_fields['wpr_store_city'] . ', ';
} else {
	$store_address .= get_post_meta( get_the_ID(), 'wpr_store_city', true ) . ', ';
	$google_map    .= get_post_meta( get_the_ID(), 'wpr_store_city', true ) . ', ';
}
if ( ! empty( $dealer_fields['wpr_store_state'] ) ) {
	$store_address .= $dealer_fields['wpr_store_state'] . ', ';
	$google_map    .= $dealer_fields['wpr_store_state'] . ', ';
} else {
	$store_address .= get_post_meta( get_the_ID(), 'wpr_store_state', true ) . ', ';
	$google_map    .= get_post_meta( get_the_ID(), 'wpr_store_state', true ) . ', ';
}

if(is_user_logged_in()){

	$output .='
<div class="profile-description">

                                            <img class="profile-img" src="/wp-content/uploads/2019/10/img-avatar.png">
                        <h1 class="profile-title">'. get_the_title(). '</h1>
                        <h6 class="profile-info"><a href="#"> Salesman In, Dealership &amp; Brand</a></h6>
                        <p class="profile-icons">
                            <a href="https://luxury.local.com/automotive/ferrari"><i class="fa fa-list" title="Ferrari Offers"></i></a>
                            <a href="tel:" data-rel="external"><i class="fa fa-phone" aria-hidden="true"></i></a>
                            <a href="mailto:"><i class="fa fa-envelope-o"></i></a>
                        </p>
                                    </div>';

}
else {
	$output .='<div class="wpr-dealer-logo 12345"> Looking to buy ' . $category_name . '? Here is the closest location <br></div><hr>';
}


$output .= '<div class="wpr-dealer-name jj bird">' . '<img class="car-logo" src=" '. $term_attachment_url. ' ">'. get_the_title() . '</div>';


$output .= '<div class="wpr-dealer-address wpr-dealer-loc-button">';

if ( ! empty( $google_map ) ) {
    $output .= '<p class="profile profile address">' . '<a href="https://www.google.com/maps?q=' . substr( $google_map, 0, - 2 ) . '" target="_blank" class=" wpr-map-location" data-do="map">' . substr( $store_address, 0, - 2 ) . '</a>' . '</p>';
}

$output .=   '</div>';
//$fb_link =  get_term_meta($category_id, 'wpr_brand_link_text', false);
//if($fb_link){
$output .= '<div class="wpr-dealer-address wpr-dealer-loc-button">';
$output .= '<p class="profile fb-address">
    <a href=" '. esc_url($wpr_brand_link_text).' ">www.facebook.com/...</a></p>';
$output .=   '</div>';

//}




switch_to_blog( 1 );
$wpr_brand_catalog = get_term_meta( $category_id, 'wpr_brand_catalog', true );
restore_current_blog();


if ( $wpr_brand_catalog ) {
	$output .= '<div class="wpr-view-brand-catalog wpr-dealer-loc-button profile brand-catalog"><a href="' . esc_url( $wpr_brand_catalog ) . '" target="_blank" class=" wpr-brand-catalog">View Brand Catalog </a></div>';
}




if ( ! empty( $dealer_fields['wpr_store_hours'] ) ) {
    $store_hours = $dealer_fields['wpr_store_hours'];
} else {
    $store_hours = get_post_meta( get_the_ID(), 'wpr_store_hours', true );
}

if ( ! empty( $store_hours ) ) {


    $output .= '<div class="clearfix"></div><div class="wpr-dealer-hours wpr-dealer-loc-button store_hours">
       <div class="hoursTop profile store-hours"> <span>Store hours</span> <i class="fa fa-chevron-down"></i></div>
        <div class="timeSlots" style="display:none;">
        ';

//$managehours = explode("|", $store_hours);
$managehours = str_replace(',', '<hr>', $store_hours);
//foreach ($managehours as $hours) {
    $output .= '<span class="hoursBlock">' . $managehours. "</span>";
//}
$output .= ' </div>
    </div>';


}

$output .= '</div>';

$output .= $ba_content;