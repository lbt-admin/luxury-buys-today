<?php
$ba_content     = '';
$have_ba_column = '';
if ( is_user_logged_in() ) {
	$args = array(
		'role'       => 'business_associate',
		'meta_query' => array(
			'relation' => 'AND',
			array(
				'key'     => 'wpr_brand_association',
				'value'   => $category_id,
				'compare' => '=',
			),
			array(
				'key'     => 'wpr_dealer_association',
				'value'   => get_the_ID(),
				'compare' => '=',
			)
		),
		'number'     => 1,
	);

	$user_query = new WP_User_Query( $args );

	$got_a_ba = false;
	if ( ! empty( $user_query->results ) ) {
		$got_a_ba       = true;
		$have_ba_column = ' wpr-got-the-ba';
		$ba_content     .= '<div class="wpr-ba-details-archive">';
		foreach ( $user_query->results as $ba_info ) {
			$ba_avatar  = get_avatar( $ba_info->ID, 150 );
			$ba_content .= '<div class="wpr-ba-name-details-info">';
			if ( '' !== $ba_avatar ) {
				$ba_content .= '<div class="wpr-ba-image-details-archive">' . $ba_avatar . '</div>';
			}
			$ba_content .= '<div class="wpr-ba-name-details-archive">';

			if ( $ba_info->description ) {
				$ba_content .= sprintf( '<div class="ba-bio">%s</div>', apply_filters( 'the_content', esc_attr( $ba_info->description ) ) );
			}

			$ba_content .= '</div>';

			$ba_content .= '<div class="wpr-ba-name-details-archive-ba-name">';
			$ba_content .= sprintf( '<h3><span>%s</span> %s</h3>', esc_attr( $ba_info->first_name ), esc_attr( $ba_info->last_name ) );
			$ba_content .= sprintf( '<h4>%s</h4>', esc_attr( $ba_info->position ) );
			$ba_content .= '</div>';

			$ba_content .= '<div class="wpr-ba-name-details-archive-ba-buttons">';
//			$ba_content .= '<div class="wpr-dealer-loc-button wpr-follow-button"><a href="#" class="btn wpr-follow" data-do="follow">' . __( 'Follow me', 'wpr-dealers' ) . '</a></div>';
			$ba_content .= '<div class="wpr-dealer-loc-button"><a href="#" class="btn wpr-contact-me" data-do="contact">' . __( 'Contact me', 'wpr-dealers' ) . '</a></div>';
			$ba_content .= '</div></div>';
		}
		$ba_content .= '</div>';
	}
}

if ( ! empty( $dealer_fields['wpr_store_address'] ) ) {
	$store_address .= $dealer_fields['wpr_store_address'] . ', ';
	$google_map    .= $dealer_fields['wpr_store_address'] . ', ';
} else {
	$store_address .= get_post_meta( get_the_ID(), 'wpr_store_address', true ) . ', ';
	$google_map    .= get_post_meta( get_the_ID(), 'wpr_store_address', true ) . ', ';
}
if ( ! empty( $dealer_fields['wpr_store_city'] ) ) {
	$store_address .= $dealer_fields['wpr_store_city'] . ', ';
	$google_map    .= $dealer_fields['wpr_store_city'] . ', ';
} else {
	$store_address .= get_post_meta( get_the_ID(), 'wpr_store_city', true ) . ', ';
	$google_map    .= get_post_meta( get_the_ID(), 'wpr_store_city', true ) . ', ';
}
if ( ! empty( $dealer_fields['wpr_store_state'] ) ) {
	$store_address .= $dealer_fields['wpr_store_state'] . ', ';
	$google_map    .= $dealer_fields['wpr_store_state'] . ', ';
} else {
	$store_address .= get_post_meta( get_the_ID(), 'wpr_store_state', true ) . ', ';
	$google_map    .= get_post_meta( get_the_ID(), 'wpr_store_state', true ) . ', ';
}

if ( ! empty( $dealer_fields['wpr_store_phone_number'] ) ) {
	$store_address .= $dealer_fields['wpr_store_phone_number'] . ', ';
} else {
	$store_address .= get_post_meta( get_the_ID(), 'wpr_store_phone_number', true ) . ', ';
}

$output .= '<div class="wpr-dealer-details-archive-left"><div class="wpr-dealer-details-flex">';
$output .= '<div class="wpr-dealer-details-archive-contact">';

if ( ! empty( $brand_image ) && is_user_logged_in() ) {
	$output .= '<div class="wpr-dealer-logo">' . $brand_image . '</div>';
}

$output .= '<div class="wpr-dealer-name">' . get_the_title() . '</div>';

$output .= '<div class="wpr-dealer-address">';

$output .= substr( $store_address, 0, - 2 ) . '</div>';
if ( ! empty( $dealer_fields['wpr_store_hours'] ) ) {
	$store_hours = $dealer_fields['wpr_store_hours'];
} else {
	$store_hours = get_post_meta( get_the_ID(), 'wpr_store_hours', true );
}

if ( ! empty( $store_hours ) ) {
	$output .= '<div class="wpr-dealer-hours"><span>' . __( 'Store hours' ) . '</span>' . $store_hours . '</div>';
}
$output .= '</div>';

$output .= '<div class="wpr-dealer-details-archive-buttons">';
if ( true === $got_a_ba ) {
	$appointment_btn = ' appointment';
	$learn_more      = ' learn_more';
}

switch_to_blog( 1 );
$wpr_brand_catalog = get_term_meta( $category_id, 'wpr_brand_catalog', true );
restore_current_blog();

if ( is_archive() ) {
	$output .= '<div class="wpr-dealer-loc-button"><a href="#" class="btn wpr-set-appointment" data-do="appointment">' . __( 'Set an appointment', 'wpr-dealers' ) . '</a></div>';
	$output .= '<div class="wpr-dealer-loc-button"><a href="#" class="btn wpr-learn-more" data-do="learn">' . __( 'Learn More', 'wpr-dealers' ) . '</a></div>';

	if ( $wpr_brand_catalog ) {
		$output .= '<div class="wpr-dealer-loc-button"><a href="' . esc_url( $wpr_brand_catalog ) . '" target="_blank" class="btn wpr-brand-catalog">' . __( 'See brand catalog', 'wpr-dealers' ) . ' <i class="fa fa-external-link" aria-hidden="true"></i></a></div>';
	}

	$output .= '<div class="wpr-dealer-loc-button"><a href="#" class="btn wpr-contact-me" data-do="contact">' . __( 'Call me', 'wpr-dealers' ) . '</a></div>';
	$output .= '<div class="wpr-dealer-loc-button"><a href="#" class="btn wpr-price-request" data-do="price">' . __( 'Price', 'wpr-dealers' ) . '</a></div>';
}
if ( ! empty( $google_map ) ) {
	$output .= '<div class="wpr-dealer-loc-button"><a href="https://www.google.com/maps?q=' . substr( $google_map, 0, - 2 ) . '" target="_blank" class="btn wpr-map-location" data-do="map">' . __( 'Map Location', 'wpr-dealers' ) . '</a></div>';
}
$output .= '</div>';

$output .= '</div></div>';

$output .= '<div class="wpr-dealer-details-archive">';
if ( true === $got_a_ba ) {
	$output .= $ba_content;
} else {
	$output .= '<iframe width="100%" height="315" src="https://www.youtube.com/embed/UJJem0upY5A" frameborder="0" allowfullscreen></iframe>';
}


$output .= '</div>';