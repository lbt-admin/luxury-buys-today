<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package Novus
 */
?>

</div>
</div><!-- #content -->

<?php get_sidebar( 'footer-full-width' ); ?>
<?php get_sidebar( 'footer' ); ?>
<footer id="colophon" class="site-footer" role="contentinfo">
	<div class="container">
		<div class="wpr-footer-widget-areas" class="footer-sidebar widget-area" role="complementary">
			  <?php if ( is_active_sidebar( 'footer_column_1' ) ) : ?>
					<?php dynamic_sidebar( 'footer_column_1' ); ?>
			  <?php endif; ?>
			  <?php if ( is_active_sidebar( 'footer_column_2' ) ) : ?>
					<?php dynamic_sidebar( 'footer_column_2' ); ?>
			  <?php endif; ?>
			  <?php if ( is_active_sidebar( 'footer_column_3' ) ) : ?>
					<?php dynamic_sidebar( 'footer_column_3' ); ?>
			  <?php endif; ?>
			  <?php if ( is_active_sidebar( 'footer_column_4' ) ) : ?>
					<?php dynamic_sidebar( 'footer_column_4' ); ?>
			  <?php endif; ?>
		</div>
		<!-- <nav class="footer-navigation" role="navigation">
				<?php
				wp_nav_menu(
					array(
						'theme_location' => 'footer',
						'menu_id'        => 'footer-menu',
					)
				);
				?>
			</nav>#site-navigation -->
		 <?php get_template_part( 'partials/social-networks' ); ?>
		<div class="wpr-small-type">
			  <?php
				printf( '&copy; %s %s', date( 'Y' ), __( 'Luxury Buys Today and Xsellcast.  All names, trademarks and images are copyright their respective owners. Luxury Buys Today is the quintessential Online News Magazine featuring the very best in luxury brand News, Events, & Promotions. Our luxury blog features Fashion, Home Décor, Auto, & Residential Properties. Our publisher provides you guidance, advice, and ratings on each product offer in the marketplace right now.' ) );
				?>
		</div>
	</div><!-- .site-info -->
</footer><!-- #colophon -->
</div><!-- #page -->
<?php
if ( is_single() ) {
	$edgar_cats = array( '36', '37', '38', '39' );
	do_action( 'wpr_display_cta_buttons_single_post_mobile', get_the_ID(), $edgar_cats );
	?>
	<?php
}
?>

<!-- Coming Soon Pop-up [BEGINS] -->
<div class="lbt-modal default" data-target="coming-soon-modal">
	<div class="lbt-modal__body">
		<div class="lbt-modal__close" data-action="lbt-modal-close"></div>
		<div class="row">
			<div class="column">
				<img src="https://luxurybuystoday-media.s3.amazonaws.com/media/uploads/2020/10/91e21b2177f9253059b0a967966710be2x-png.png" alt="Coming Soon">
			</div>
			<div class="column">
				<h3 class="title">Coming Soon To Your Area</h3>
				<p class="subtitle">A better way to get the latest news and offers from your favorite luxury brands.</p>
				<div class="default-content">
					<p>VIP Special Member Discounts</p>
					<p>Prized Invitations To Local Sales Events</p>
					<p>Daily Brand News, Events & Promotions</p>
					<p>Free Live Concierge Services</p>
				</div>
				<div class="cookie-content">
					<p>These features are not yet available in your area keep an eye on your email for a launch announcement.</p>
					<div class="newsletter-wrapper">
						<p>Sign up to get the latest updates</p>
						<?php echo do_shortcode( '[lbt_newsletter]' ); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Coming Soon Pop-up [ENDS] -->

<!-- JS - Cooming Soon [BEGINS] -->
<script>
	(function ($) {
		"use strict";

		var subscribed = LBT_GetCookie('confirm_subscription');
		var newsletter = $('[data-target="coming-soon-modal"]');

		if ( subscribed ) {
			newsletter.addClass('cookie').removeClass('default');
		}

		function LBT_SetCookie(cname, cvalue, exdays) {
			var d = new Date();
			d.setTime(d.getTime() + (exdays*24*60*60*1000));
			var expires = "expires="+ d.toUTCString();
			document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
		}

		function LBT_GetCookie(cname) {
			var name = cname + "=";
			var decodedCookie = decodeURIComponent(document.cookie);
			var ca = decodedCookie.split(';');
			for(var i = 0; i <ca.length; i++) {
				var c = ca[i];
				while (c.charAt(0) == ' ') {
					c = c.substring(1);
				}
				if (c.indexOf(name) == 0) {
					return c.substring(name.length, c.length);
				}
			}
			return "";
		}

		$(document).on('click', '[data-action="lbt-modal-close"]', function (e) {
			$(this).closest('.lbt-modal').hide();
		});
		<?php if ( ! get_current_user_id() ) { ?>
			  $(document).on('click', '[data-action="lbt-coming-soon"]', function (e) {
				$('[data-target="coming-soon-modal"]').show();
			  });
		<?php } ?>

		<?php if ( is_page_template( 'templates/tmp-confirm-subscription.php' ) ) : ?>
		LBT_SetCookie('confirm_subscription', true, 365);
		<?php endif; ?>
	})(jQuery);
</script>
<!-- JS - Cooming Soon [ENDS] -->

<!-- CTA pop up -->
<div class="openCTApopup" style="width:1px; visibility:hidden; position:absolute; opacity:0;"></div>
<div class="cta_overlay_wrap">
	<div class="cta_pop_wrap"> <div class="cta_pop_box">
		<div class="cta_pop_header">
			<img src="/wp-content/themes/lbt-gaia-child/assets/images/popover-logo.png" alt="" /><button type="button" class="CTA-close header-btn" aria-label="Close">
			×            </button>
		</div>
		<p>Thank you for your interest, you will be contacted soon about this product.</p>
		<a href="https://www.facebook.com/pg/LuxuryBuysToday/posts/"><button class="swal2-cancel wpr-please-like-us swal2-styled" style="display: inline-block; background-color: transparent;" type="button"><img src="https://luxurybuystoday.com/wp-content/plugins/wpr-dealers-location/assets/images/popup-facebook.png"> Please like us on Facebook</button></a>
		<button class="CTA-close footer-btn" type="button" aria-label="Close">Close</button>
   </div></div>
</div>
<!-- CTA pop up end -->
<!-- Login register popup -->
<?php if( ! get_current_user_id() ) { ?>
	<div class="login_register_pop">
	<div class="login_register_wrap">
		<div class="login_register_box">
			<div class="modal_header">
				<img src="https://luxurybuystoday-media.s3.amazonaws.com/media/uploads/2020/01/login-logo-png.png" alt="" />
				<button type="button" class="login-close" aria-label="Close">×</button>
			</div>
			<div class="zm-login-form">
				<div style="text-align: center; margin-top: -20px;"><?php echo do_shortcode( '[oa_social_login]' ); ?></div>
				<div class="the-or-word">OR</div>
				<div><?php echo do_shortcode( '[gravityform id="3" ajax="true" title="false" description="false"]' ); ?></div>
				<p style="text-align: center; font-weight: 600;">Don't have an account? <a class="zm-signup" style="text-decoration: underline" href="#">Sign up</a></p>
				<p style="text-align: center"><a class="popmake-reset-password" href="/wp-login.php?action=lostpassword">Lost your password?</a></p>
				<?php if ( ! is_home() ) { ?>
					<iframe style='display:none;width:0px;height:0px;' src='about:blank' name='gravity_login_frame' id='gravity_login_frame'>This iframe contains the logic required to handle Ajax powered Gravity Forms.</iframe>
					<script type='text/javascript'>jQuery(document).ready(function($){$('.login_register_pop #gform_3').attr("target","gravity_login_frame");jQuery('#gravity_login_frame').load( function(){var contents = jQuery(this).contents().find('*').html();var is_postback = contents.indexOf('GF_AJAX_POSTBACK') >= 0;if(!is_postback){return;}var form_content = jQuery(this).contents().find('#gform_wrapper_3');var is_confirmation = jQuery(this).contents().find('#gform_confirmation_wrapper_3').length > 0;var is_redirect = contents.indexOf('gformRedirect(){') >= 0;var is_form = form_content.length > 0 && ! is_redirect && ! is_confirmation;if(is_form){jQuery('#gform_wrapper_3').html(form_content.html());if(form_content.hasClass('gform_validation_error')){jQuery('#gform_wrapper_3').addClass('gform_validation_error');} else {jQuery('#gform_wrapper_3').removeClass('gform_validation_error');}setTimeout( function() { /* delay the scroll by 50 milliseconds to fix a bug in chrome */  }, 50 );if(window['gformInitDatepicker']) {gformInitDatepicker();}if(window['gformInitPriceFields']) {gformInitPriceFields();}var current_page = jQuery('#gform_source_page_number_3').val();jQuery(document).trigger('gform_page_loaded', [3, current_page]);window['gf_submitting_3'] = false;}else if(!is_redirect){var confirmation_content = jQuery(this).contents().find('.GF_AJAX_POSTBACK').html();if(!confirmation_content){confirmation_content = contents;}setTimeout(function(){jQuery('#gform_wrapper_3').replaceWith(confirmation_content);jQuery(document).trigger('gform_confirmation_loaded', [3]);window['gf_submitting_3'] = false;}, 50);}else{jQuery('#gform_3').append(contents);if(window['gformRedirect']) {gformRedirect();}}jQuery(document).trigger('gform_post_render', [3, current_page]);} );});</script>
				 <?php } ?>
			</div>
			<!-- login-form end-->
			<div class="zm-register-form">
				<div class="login-signup">
					<div class="login-signup-container">
						<div class="login-left">
							<h2>Create your account</h2>
							<div class="signup-benifits">
								<ul>
									<li><img src="https://luxurybuystoday-media.s3.amazonaws.com/media/uploads/2018/04/32845-s-1.jpg" alt="" width="150" height="150" class="alignnone size-full wp-image-340807" /> <span>vip special member discount price notices</span></li>
									<li><img src="https://luxurybuystoday-media.s3.amazonaws.com/media/uploads/2018/04/32845-s-2.jpg" alt="" width="150" height="150" class="alignnone size-full wp-image-340806" /> <span>prized invitations to local sales events</span></li>
									<li><img src="https://luxurybuystoday-media.s3.amazonaws.com/media/uploads/2018/04/32845-s-3.jpg" alt="" width="150" height="150" class="alignnone size-full wp-image-340805" /> <span>daily brand news, events and promotions</span></li>
									<li><img src="https://luxurybuystoday-media.s3.amazonaws.com/media/uploads/2018/04/32845-s-4.jpg" alt="" width="150" height="150" class="alignnone size-full wp-image-340804" /> <span>free live concierge services</span></li>
								</ul>
							</div>
							<p style="text-align: center;">Already have an account? <a class="zm-login" href="#">Sign In</a></p>
						</div>
						<div class="login-right">
							<div style="text-align: center;"><?php echo do_shortcode( '[oa_social_login]' ); ?></div>
							<p style="text-align: center;">or create an account</p>
							<?php echo do_shortcode( '[gravityform id="16" ajax="true" title="false" description="false"]' ); ?>
						</div>
			          </div>
				</div>
				<?php if ( ! is_home() ) { ?>
					<iframe style='display:none;width:0px;height:0px;' src='about:blank' name='gravity_register_frame' id='gravity_register_frame'>This iframe contains the logic required to handle Ajax powered Gravity Forms.</iframe>
					<script type='text/javascript'>jQuery(document).ready(function($){$('.login_register_pop #gform_16').attr("target","gravity_register_frame");jQuery('#gravity_register_frame').load( function(){var contents = jQuery(this).contents().find('*').html();var is_postback = contents.indexOf('GF_AJAX_POSTBACK') >= 0;if(!is_postback){return;}var form_content = jQuery(this).contents().find('#gform_wrapper_16');var is_confirmation = jQuery(this).contents().find('#gform_confirmation_wrapper_16').length > 0;var is_redirect = contents.indexOf('gformRedirect(){') >= 0;var is_form = form_content.length > 0 && ! is_redirect && ! is_confirmation;if(is_form){jQuery('#gform_wrapper_16').html(form_content.html());if(form_content.hasClass('gform_validation_error')){jQuery('#gform_wrapper_16').addClass('gform_validation_error');} else {jQuery('#gform_wrapper_16').removeClass('gform_validation_error');}setTimeout( function() { /* delay the scroll by 50 milliseconds to fix a bug in chrome */  }, 50 );if(window['gformInitDatepicker']) {gformInitDatepicker();}if(window['gformInitPriceFields']) {gformInitPriceFields();}var current_page = jQuery('#gform_source_page_number_16').val();jQuery(document).trigger('gform_page_loaded', [16, current_page]);window['gf_submitting_16'] = false;}else if(!is_redirect){var confirmation_content = jQuery(this).contents().find('.GF_AJAX_POSTBACK').html();if(!confirmation_content){confirmation_content = contents;}setTimeout(function(){jQuery('#gform_wrapper_16').replaceWith(confirmation_content);jQuery(document).trigger('gform_confirmation_loaded', [16]);window['gf_submitting_16'] = false;}, 50);}else{jQuery('#gform_16').append(contents);if(window['gformRedirect']) {gformRedirect();}}jQuery(document).trigger('gform_post_render', [16, current_page]);} );} );</script>
			     <?php } ?>
		     </div>
	     <!-- zm-register-form-->
		</div>
	</div>
</div>
<?php } ?>

<!-- Login register popup end-->
<!-- Confirm Remove Saved Brand Start-->
<div class="confirm_remove_pop remove_brand">
	<div class="login_register_wrap">
		<div class="login_register_box">
			<div class="modal_header">
				<img src="https://luxurybuystoday-media.s3.amazonaws.com/media/uploads/2020/01/login-logo-png.png" alt="" />
				<button type="button" class="login-close" aria-label="Close">×</button>
		   </div>
		   <div class="zm-login-form">
			   <p>Please confirm you would like to remove this Brand.</p>
			   <button class="confirm-remove">Confirm</button>
			   <button class="cancel-remove">Cancel</button>
		   </div>
		</div>
	</div>
</div>
<!-- Confirm Remove Saved Brand Start-->
<div class="confirm_remove_pop remove_post">
	<div class="login_register_wrap">
		<div class="login_register_box">
			<div class="modal_header">
				<img src="https://luxurybuystoday-media.s3.amazonaws.com/media/uploads/2020/01/login-logo-png.png" alt="" />
				<button type="button" class="login-close" aria-label="Close">×</button>
		   </div>
		   <div class="zm-login-form">
			   <p>Please confirm you would like to remove this deal.</p>
			   <button class="confirm-remove">Confirm</button>
			   <button class="cancel-remove">Cancel</button>
		   </div>
		</div>
	</div>
</div>
<!-- Confirm Remove Saved Brand End-->
<?php
if ( is_page_template( 'templates/tmp-saved-brands.php' ) ) {
	$profile_action = 'remove_brands_at_profile';
}
if ( is_page_template( 'templates/tmp-lookbook.php' ) ) {
	$profile_action = 'remove_favorites_at_profile';
}
?>
<?php
/*
if(is_page('saved-brands')){
	$profile_action= "remove_brands_at_profile";
}else{
	$profile_action= "remove_favorites_at_profile";
}
	*/
?>
<?php wp_footer(); ?>
<?php
if ( MYREG_VAR != '' ) {
	?>
	<script type="application/javascript">
		alert("bingly bingly beep");
		alertText='<div class="alert alert-success alertText"><span class="welcome">Welcome <?php echo $newRegister; ?></span><br><span class="alertBlock">You can now add offers to your personal lookbook and contact our Authorized Brand Associates. To take full advantage of your membership <a href="">complete your profile.</a> </span>';
		jQuery('#content').append(alertText);
	</script>
<?php } ?>
<script>
function ucwords(str,force){
  str=force ? str.toLowerCase() : str;
  return str.replace(/(\b)([a-zA-Z])/g,
		   function(firstLetter){
			  return   firstLetter.toUpperCase();
		   });
}

	window.onload = function() {
		$(document).ready(function(){

		$('.wpr-cronofy-modal-close').on('click', function() {
			$('.wpr-cronofy-calendar-overlay').hide();
			$('.wpr-cronofy-calendar-modal').hide();
		});

		//********** Transform Uppercase titles to capitalize ***********/
		 $( ".title-1 a,.title-3 a,.entry-title a" ).each(function() {
			var title_value= ucwords($(this).text(),true);
			$(this).text(title_value);
			});
		 <?php if ( ! is_user_logged_in() ) { ?>
		 $('body').on('click', '.zm-cta-btn', function (e) {
			console.log('zm-cta-btn logged out');
			e.preventDefault();
			$('.login_register_pop').show();
		   });
		<?php } else { ?>
			//**********zm-save button CTA action ***********/
		 $('body').on('click', '.zm-cta-btn', function (e) {
			console.log('logged in zm-cta-btn test');
			e.preventDefault();

			var wprInitial = $(this).html();

			$(this).html('<strong></strong> ' + wprInitial);

			var wprDeal = $('.container').find('#wpr-dealer-location').attr('data-deal');
			var wprCat = $('.container').find('#wpr-dealer-location').attr('data-loc');
			var wprTODO = $(this).attr('data-do');
			var wprOffer = $('.container').find('.entry-header').attr('data-deal-id');

			if (!wprOffer) {
				wprOffer = $('.container').find('.single_button').attr('data-postid');
			}

			if ('map' !== wprTODO) {
				e.preventDefault();
			}
			var data = {
				action: 'save_cta',
				nonce: nonceVar,
				dealer_id: wprDeal,
				category_id: wprCat,
				todo: wprTODO,
				current_url: window.location.href
			};

			 <?php if ( is_single() ) : ?>
				data.post_id = wprOffer;
			<?php endif; ?>

			if ( $(this).hasClass('zm-appt') && $('.wpr-cronofy-calendar-modal').length ) {
				$('.wpr-cronofy-calendar-overlay').show();
				$('.wpr-cronofy-calendar-modal').show();
				$('.wpr-cronofy-calendar-modal').removeData('request');
				$('.wpr-cronofy-calendar-modal').data('request', {
					post_id: data.post_id,
					dealer_address: $('.container').find('#wpr-dealer-location #zm-profile-description').data('dealAddress'),
					dealer_state: $('.container').find('#wpr-dealer-location #zm-profile-description').data('dealState')
				});
				$(this).html(wprInitial);
				requestRunning = false;
				return false;
			}

			var ajaxOpts = {
				type: 'POST',
				url: ajaxURLVar,
				data: data,
				beforeSend: function(data) {
					setTimeout(function () {
						$('strong').remove();
						$('body').find('.openCTApopup').trigger('click');
					}, 2000);
				},
				success: function (data) {

				//$('strong').remove();
				//$('body').find('.openCTApopup').trigger('click');
				},
				complete: function () {
					requestRunning = false;
				},
				error: function (request,  status, error) {
				swal({
					title: '',
					text: 'There was an error saving your selection, please try again.',
					type: 'warning'
				});
				}
			};

			requestRunning = true;
			$.ajax(ajaxOpts);
			return false;

			});
		<?php } ?>
			//**********zm-save button CTA action end ***********/
						//**********SAVE favorites TO USER PROFILE***********/
			  $('body').on('click', '.zm-save-favorites', function () {
				$(this).removeClass('zm-save-favorites');
				$(this).addClass('zm-remove-favorites');
				var dataBrandID = $(this).attr('data-brand');
				var userID = '<?php echo get_current_user_id(); ?>';
				var removeFavorite  = $(this).attr('remove-favorite');


				 $.ajax({
						url : '<?php echo admin_url( 'admin-ajax.php', 'https://' ); ?>',
						type : 'post',
						context: this,
						data : {
							action : 'save_favorites_at_profile',
							dataBrandID: dataBrandID,
							userID: userID
						},
						success : function(response)
						{
							$(this).attr("remove-favorite", 'active');
							$(this).html("REMOVE FROM LOOKBOOK");
							// remove the save button
							// so users can only remove it from the lookbook page
							$(this).remove();
						}
					});
			});

			//*******REMOVE SELECTED favorites*****//
			$('body').on('click', '.zm-remove-favorites', function () {
				var dataBrandID = $(this).attr('data-brand');
					$(this).addClass('zm-save-favorites');
					$(this).removeClass('zm-remove-favorites');
					var userID = '<?php echo get_current_user_id(); ?>';
					var wprInitial = $(this).html();
				   $(this).html('<i class="fa fa-spinner fa-spin" style="font-size:14px"></i> ' + wprInitial);
					$.ajax({
						url : '<?php echo admin_url( 'admin-ajax.php', 'https://' ); ?>',
						type : 'post',
						context: this,
						data : {
							action : 'remove_favorites_at_profile',
							dataBrandID: dataBrandID,
							userID: userID
						},
						success : function(response)
						{
							$(this).removeAttr("remove-favorite");
							$(this).html("SAVE TO LOOKBOOK");
						}
					});
			});

			//*******SINGLE PAGE favorites*****//
			$('body').on('click', '.zm-save-favorites1', function () {
				var dataBrandID = $(this).attr('data-brand');
				var userID = '<?php echo get_current_user_id(); ?>';
				var removeFavorite  = $(this).attr('remove-favorite');

				if(removeFavorite == null)
				{
					$.ajax({
						url : '<?php echo admin_url( 'admin-ajax.php', 'https://' ); ?>',
						type : 'post',
						data : {
							action : 'save_favorites_at_profile',
							dataBrandID: dataBrandID,
							userID: userID
						},
						success : function(response)
						{
							$('.zm-save-favorites1').attr("remove-favorite", 'active');
							$('.zm-save-favorites1').html("<span>REMOVE</span> FROM LOOKBOOK");

						}
					});
				}
				//*******REMOVE SELECTED FAV *****//
				else if(removeFavorite == 'active'){
					var userID = '<?php echo get_current_user_id(); ?>';
					var wprInitial = $(this).html();

					$.ajax({
						url : '<?php echo admin_url( 'admin-ajax.php', 'https://' ); ?>',
						type : 'post',
						data : {
							action : 'remove_favorites_at_profile',
							dataBrandID: dataBrandID,
							userID: userID
						},
						success : function(response)
						{
							$('.zm-save-favorites1').removeAttr("remove-favorite", 'active');
							$('.zm-save-favorites1').html("<span>SAVE</span> TO LOOKBOOK");
						}
					});
				}
			});
			//**********SAVE BRANDS AND DEALS TO USER PROFILE***********/

		 $('body').on('click', '.zm-save,.zm-save-favorites,.zm-save-favorites1', function () {
				var dataDealID = $('.container').find('#wpr-dealer-location').attr('data-deal');
				var userID = '<?php echo get_current_user_id(); ?>';
				var removeBrand  = $(this).attr('remove-brand');

				var wprInitial = $(this).html();

				if(removeBrand == null)
				{
					$(this).html('<i class="fa fa-spinner fa-spin" style="font-size:14px"></i> ' + wprInitial);

					$.ajax({
						url : '<?php echo admin_url( 'admin-ajax.php', 'https://' ); ?>',
						type : 'post',
						data : {
							action : 'save_brands_at_profile',
							dataDealID: dataDealID,
							userID: userID
						},
						success : function(response)
						{
							//$('.zm-save').replaceWith('<div class="zm-save"><i class="fa fa-heart" aria-hidden="true"></i>Remove this brand</div>');
							$('.zm-save').attr("remove-brand", 'active');
							$('.zm-save').html("<i class='fa fa-heart red' aria-hidden='true'></i>Saved");

						}
					});
				}
			});
//***********Remove Brand/Deal from lookbook Page at profile***********//
		   $('body').on('click', '.zm-remove-post', function () {
				$('.confirm_remove_pop.remove_post').show();
				$('.confirm_remove_pop.remove_post .confirm-remove').removeClass('removing-brand');
				var dataDealID = $(this).attr('data-dealId');
				var boxID = "post-box-"+dataDealID;
				$('.confirm_remove_pop.remove_post .confirm-remove').attr('data-dealId',dataDealID);
				$('.confirm_remove_pop.remove_post .confirm-remove').attr('data-boxId',boxID);
			});
			//***********Remove Brand/Deal from Save brands Page at profile***********//
			$('body').on('click', '.zm-remove', function () {
				$('.confirm_remove_pop.remove_brand').show();
				$('.confirm_remove_pop.remove_brand .confirm-remove').removeClass('removing-brand');
				var dataDealID = $(this).attr('data-dealId');
				var boxID = "post-box-"+dataDealID;
				$('.confirm_remove_pop.remove_brand .confirm-remove').attr('data-dealId',dataDealID);
				$('.confirm_remove_pop.remove_brand .confirm-remove').attr('data-boxId',boxID);
			});
			$('body').on('click', '.confirm_remove_pop .login-close,.confirm_remove_pop .cancel-remove', function () {
				$('.confirm_remove_pop').hide();
			});

			 $('body').on('click', '.confirm_remove_pop .confirm-remove', function () {
				$(this).addClass('removing-brand');
				var dataDealID = $(this).attr('data-dealId');
				var boxID = "post-box-"+dataDealID;
				var userID = '<?php echo get_current_user_id(); ?>';

				  $.ajax({
						url : '<?php echo admin_url( 'admin-ajax.php', 'https://' ); ?>',
						type : 'post',
						context: this,
						data : {

							action : '<?php echo $profile_action; ?>',
							dataDealID: dataDealID,
							dataBrandID: dataDealID,
							userID: userID
						},
						success : function(response)
						{
							//$(this).html("<i class='fa fa-heart white' aria-hidden='true'></i> Removed");
							$('.confirm_remove_pop').hide();
							$('#'+boxID).hide();
							$('#'+boxID).remove();

							console.log('Brand removed');
						}
					});
			});

			//***********Add CTA from Saved Brand page at profile ***************//
		 $('body').on('click', '.clk-cta', function () {
				var cat_id = $(this).parent().find('.post-cat-id').attr('data-post-cat-id');
				var cat_name = $(this).parent().find('.post-cat-name').attr('data-post-cat-name');
				var data_do = $(this).attr('data-do');
				var current_url = $(this).parent().find('.post-cat-url').attr('data-post-cat-url');
				var cat_image = $(this).parent().find('.post-cat-img').attr('data-post-cat-img');
				var deal_id = $(this).parent().find('.post-data-deal-id').attr('data-post-deal-id');
				var deal_address = $(this).parent().find('.post-data-deal-address').attr('data-post-deal-address');
				var deal_contact = $(this).parent().find('.post-data-deal-contact').attr('data-post-deal-contact');
				var deal_title = $(this).parent().find('.post-title').attr('data-post-title');
				var deal_state = $(this).parent().find('.post-data-deal-state').attr('data-post-deal-state');
				var deal_city = $(this).parent().find('.post-data-deal-city').attr('data-post-deal-city');

				var data = {
					action: 'save_cta',
					nonce: nonceVar,
					deal: deal_id,
					categ: cat_id,
					todo: data_do,
					current_url: current_url,
					current_offer: cat_id,
					current_cat: cat_name,
					cat_image: cat_image,
					deal_title: deal_title,
					deal_address: deal_address,
					deal_contact: deal_contact,
					deal_state: deal_state,
					deal_city: deal_city,
				};

				var ajaxOpts = {
					type: 'POST',
					url: ajaxURLVar,
					data: data,
					success: function (data) {

						$('strong').remove();
						$('body').find('.openCTApopup').trigger('click');
					},
					complete: function () {
						requestRunning = false;
					},
					error: function (request,  status, error) {
						swal({
							title: '',
							text: 'There was an error saving your selection, please try again.',
							type: 'warning'
						});
					}
				};

				requestRunning = true;
				$.ajax(ajaxOpts);
				return false;

			});

		});
	};

	/* Sticky nav */
	var stickyNavigation = jQuery("#main_header"),
		stickyDiv = "sticky_nav",
		stickyHeader = jQuery('#main_header').height(),
		offset = 1;

	if (jQuery('body').hasClass('home')) {
		offset = 81;
	}

	jQuery(window).scroll(function() {
		if( jQuery(this).scrollTop() > offset ) {
			stickyNavigation.addClass(stickyDiv);
		} else {
			stickyNavigation.removeClass(stickyDiv);
		}
	});
	/* Sticky nav */
	jQuery('.nav-toggle,.nav-overlay').click(function() {
		jQuery("#main_header").toggleClass("show_nav");
		jQuery("body").toggleClass("nav_active");
	});
	jQuery('.popmake-close, .pumClose').click(function() {
		jQuery(".pum-overlay").toggleClass("pum-active");
		jQuery(".popmake").toggleClass("active");
		jQuery(".popmake").hide();
		jQuery(".pum-overlay").hide();
	});

	jQuery(document).ready(function(){
		jQuery(".popmake-reset-password").removeClass("popmake-reset-password");
	});

	jQuery(function() {
		var ifr = jQuery("iframe");
		ifr.attr("scrolling", "auto");
		// ifr.attr("src", ifr.attr("src"));
	});

</script>
<script type="text/javascript">
	var nonceVar='<?php echo wp_create_nonce( 'wpr-dealer-location' ); ?>';
	var ajaxURLVar='<?php echo admin_url( 'admin-ajax.php', 'https://' ); ?>';
	 <?php $popup_facebook = 'https://luxurybuystoday.com/wp-content/plugins/wpr-dealers-location/assets/images/popup-facebook.png'; ?>
	var facebookURLVar='<?php echo sprintf( '<img src="%s" /> Please like us on Facebook', esc_url( $popup_facebook ) ); ?>';
	var isLoggedIn='<?php echo is_user_logged_in(); ?>';
</script>
<script>
jQuery(document).ready(function($) {
	$('.openCTApopup').click(function() {
		$('.cta_overlay_wrap').show();
	});
	$('.CTA-close').click(function() {
		$('.cta_overlay_wrap').hide();
	});
	// Add meta data when CTA is clicked
	$('.addMetaCta').click(function(e){
		console.log('comes here!!!!');
		e.preventDefault();
		var post_id = $(this).data( 'id' );
		var CTAcount = $(this).attr( 'CTAcount' );

		jQuery.ajax({
			url  : ajaxURLVar,
			type : 'post',
			data : {
				action : 'metaCta',
				post_id : post_id,
				CTAcount : CTAcount
			},
			success : function( response ) {
				// console.log(response);
			}
		});
	});

	$('.login_register').on('click', function (e) {
		e.preventDefault();
		$('.login_register_pop').show();
	});
	$('.login-close').on('click', function (e) {
		e.preventDefault();
		$('.login_register_pop').hide();
		$('.zm-register-form').hide();
		$('.zm-login-form').show();
		$('.login_register_pop').removeClass('wide');
	});
	$('.zm-signup').on('click', function (e) {
		e.preventDefault();
		$('.login_register_pop').show();
		$('.zm-login-form').hide();
		$('.zm-register-form').show();
		$('.login_register_pop').addClass('wide');
	});
	$('.zm-login').on('click', function (e) {
		e.preventDefault();
		$('.login_register_pop').show();
		$('.zm-register-form').hide();
		$('.zm-login-form').show();
		$('.login_register_pop').removeClass('wide');
	});
});
</script>
<script type="text/javascript" src="https://forms.ontraport.com/v2.4/include/formEditor/genjs-v3.php?html=false&uid=p2c137325f51"></script>
</body>
</html>
