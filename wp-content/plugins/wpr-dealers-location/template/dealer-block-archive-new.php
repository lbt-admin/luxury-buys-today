<?php
$ba_content = '';
$have_ba_column = '';
$store_zip_code =  '';

if ( ! empty( $dealer['f2108'] ) ) {
    $store_zip_code = $dealer['f2108'];
}

if ( ! empty( $dealer['f1716'] ) ) {
	$store_address .= $dealer['f1716'] . ', ';
	$google_map    .= $dealer['f1716'] . ', ';
}

if ( ! empty( $dealer['f1718'] ) ) {
    $store_address .= $dealer['f1718'] . ', ';
    $google_map    .= $dealer['f1718'] . ', ';
    $dealer_city   = $dealer['f1718'];
}

if ( ! empty( $dealer['f2107'] ) ) {
    $store_address .= $dealer['f2107'] . ', ';
    $google_map    .= $dealer['f2107'].' '.$store_zip_code;
    $dealer_state   = $dealer['f2107'];
}

if ( !empty($dealer['f2933']) ) {
    $phone_number = $dealer['f2933'];
}

if ( !empty( $store_zip_code ) ) {
    $address = $google_map;
} else {
    $address = substr($google_map, 0, -2);
}

$profile_image = ! empty( $dealer['contact_data']['f1739'] ) && wpr_remote_check_file( $dealer['contact_data']['f1739'] ) ? $dealer['contact_data']['f1739'] : 'https://luxurybuystoday-media.s3.amazonaws.com/media/uploads/2020/07/img-avatar-png-png.png';

if (is_user_logged_in()) {
    $wprUser = get_current_user_id();
    $ontraport_id = get_user_meta(absint($wprUser), 'wpr_ontraport_id', true);
    $check_existing_deal = get_user_meta($wprUser, 'saved_deals_brands');
    $deal_id = $dealer['id'];
    if (in_array($deal_id, $check_existing_deal)) {
        $save_brand_btn = '<div class="zm-save" remove-brand="active"><i class="fa fa-heart red" aria-hidden="true"></i>Saved</div>';
    } else {
        $save_brand_btn =
            '<div class="zm-save">
                <i class="fa fa-heart" aria-hidden="true"></i>
                <span class="long">Save this brand</span>
                <span class="short">Save</span>
            </div>';
    }
} else {
    $save_brand_btn =
        '<div class="zm-cta-btn">
            <i class="fa fa-heart" aria-hidden="true"></i>
            <span class="long">Save this brand</span>
            <span class="short">Save</span>
        </div>';
}

?>
<div id="zm-profile-description"
     class="zm-dealer-details-single main-brand-page-wrap custom-new"
     data-deal-title="<?php echo get_the_title(); ?>"
     data-deal-address="<?php echo $address ?? ''; ?>"
     data-deal-contact="<?php echo $phone_number ?? ''; ?>"
     data-deal-state="<?php echo $dealer_state ?? ''; ?>"
     data-deal-city="<?php echo $dealer_city ?? ''; ?>">
    <div class="zm-profile-description main-brand-page">
        <div class="closest-store">YOUR CLOSEST STORE</div>
        <div class="zm-profile-media">
            <div class="profile-image">
                <img src="<?php echo $profile_image; ?>"
                     alt="Profile Image">
            </div>
            <?php if (is_user_logged_in()) : ?>
            <div class="brand-logo">
                <img src="<?php echo $wpr_brand_image_url; ?>" alt="<?php echo $category_name; ?>">
            </div>
            <?php endif; ?>
        </div>
        <div class="zm-dealer-name">
            <h2 class="zm-profile-title" data-deal-id="<?php echo $dealer['id']; ?>">
                <?php echo $dealer['f1714']; ?>
            </h2>
        </div>
        <div class="store-location"><?php echo $address; ?></div>
    </div>
    <div class="zm-profile-description main-brand-page">
        <?php
        $display_original = false;

        if ( is_user_logged_in() ) {
            $current_user = wp_get_current_user();
            $current_user_email = $current_user ? $current_user->user_email : false;

            if ( $current_user_email ) {
                $email_domain = explode( '@', $current_user_email )[1];
                $display_original = $email_domain === 'hokuapps.com';
            }
               $display_original = true;
        }

        if ( $display_original ) : ?>
            <div class="zm-cta-actions">
                <div class="zm-contact-me zm-cta-btn" data-do="contact">
                    <i class="fa icon-phone" aria-hidden="true"></i>call me
                </div>
                <div class="zm-appt zm-cta-btn" data-do="appointment">
                    <i class="fa icon-calendar"></i>
                    <span class="long">make an appt</span>
                    <span class="short">appt</span>
                </div>
                <div class="zm-info zm-cta-btn" data-do="learn">
                    <i class="fa icon-info" aria-hidden="true"></i>
                    <span class="long">request more info</span>
                    <span class="short">info</span>
                </div>
                <?php echo $save_brand_btn; ?>
                <span class="pumOpen" id="pumOpen" style="display: none;"></span>
            </div>
        <?php else : ?>
            <div class="zm-cta-actions">
                <div class="zm-contact-me" data-action="lbt-coming-soon">
                    <i class="fa icon-phone" aria-hidden="true"></i>call me
                </div>
                <div class="zm-appt">
                    <i class="fa icon-calendar"></i>
                    <span class="long">make an appt</span>
                    <span class="short">appt</span>
                </div>
                <div class="zm-info" data-action="lbt-coming-soon">
                    <i class="fa icon-info" aria-hidden="true"></i>
                    <span class="long">request more info</span>
                    <span class="short">info</span>
                </div>
                <?php echo $save_brand_btn; ?>
                <span class="pumOpen" id="pumOpen" style="display: none;"></span>
            </div>
        <?php endif; ?>
        <div class="zm-dealer-details">
            <div class="zm-dealer-map">
                <?php if (!empty($google_map)) : ?>
                    <i class="fa fa-map-marker"></i>
                    <a href="https://www.google.com/maps?q=<?php echo substr($google_map, 0, -2); ?>"
                       target="_blank" class=" wpr-map-location" data-do="map">Directions</a>
                <?php endif; ?>
            </div>
            <div class="zm-dealer-facebook">
                <i class="fa fa-facebook"></i>
                <a href="<?php echo esc_url($wpr_brand_link_text); ?>">Facebook</a>
            </div>
            <div class="zm-dealer-phone zm-cta-btn" data-do="contact">
                <i class="fa icon-cell-phone" aria-hidden="true"></i>
                <span>Request call back</span>
            </div>
            <?php if (!empty($catalog_url)) : ?>
            <div class="zm-view-brand-catalog">
                <i class="fa icon-share"></i>
                <a href="<?php echo esc_url($catalog_url); ?>"
                   target="_blank" class=" wpr-brand-catalog">Brand Catalog </a>
            </div>
            <?php endif; ?>
        </div>
    </div>
</div>
