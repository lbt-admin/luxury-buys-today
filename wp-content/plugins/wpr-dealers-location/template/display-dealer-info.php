<?php
$ba_content     = '';
$have_ba_column = '';
if ( is_user_logged_in() ) {
	$args = array(
		'role'       => 'business_associate',
		'meta_query' => array(
			'relation' => 'AND',
			array(
				'key'     => 'wpr_brand_association',
				'value'   => $category_id,
				'compare' => '=',
			),
			array(
				'key'     => 'wpr_dealer_association',
				'value'   => get_the_ID(),
				'compare' => '=',
			)
		),
		'number'     => 1,
	);

	$user_query = new WP_User_Query( $args );

	if ( ! empty( $user_query->results ) ) {
		$got_a_ba       = true;
		$have_ba_column = ' wpr-got-the-ba';
		$ba_content     .= '<div class="wpr-ba-details-archive">';
		foreach ( $user_query->results as $ba_info ) {
			$ba_avatar = get_avatar( $ba_info->ID, 150 );
			if ( is_singular() ) {
				if ( '' !== $ba_avatar ) {
					$ba_content .= '<div class="wpr-ba-details-single">';
					$ba_content .= '<div class="wpr-ba-image-details-archive">' . $ba_avatar . '</div>';
					$ba_content .= sprintf( '<div class="wpr-ba-name"><h3><strong>%s</strong><br /> %s</h3></div>', esc_attr( $ba_info->first_name ), esc_attr( $ba_info->last_name ) );
					$ba_content .= '</div>';
				}
				$ba_content .= '<div class="wpr-ba-name-details-archive">';
				$ba_content .= sprintf( '<h4>%s, %s</h4>', get_the_title(), esc_attr( $ba_info->position ) );

//				$ba_content .= '<div class="wpr-dealer-loc-button wpr-follow-button"><a href="#" class="btn wpr-follow" data-do="follow">' . __( 'Follow me', 'wpr-dealers' ) . '</a></div>';
				$ba_content .= '<div class="wpr-dealer-loc-button"><a href="#" class="btn wpr-contact-me" data-do="contact">' . __( 'Contact me', 'wpr-dealers' ) . '</a></div>';

				$ba_content .= '</div>';
			} else {
				if ( '' !== $ba_avatar ) {
					$ba_content .= '<div class="wpr-ba-image-details-archive">' . $ba_avatar . '</div>';
				}
				$ba_content .= '<div class="wpr-ba-name-details-archive">';
				$ba_content .= sprintf( '<h3>%s %s</h3>', esc_attr( $ba_info->first_name ), esc_attr( $ba_info->last_name ) );
				$ba_content .= sprintf( '<h4>%s, %s</h4>', get_the_title(), esc_attr( $ba_info->position ) );
				$ba_content .= '</div>';
			}
		}
		$ba_content .= '</div>';
	}
}

if ( is_singular() ) {
	$output .= '<div class="wpr-dealer-details-single' . $have_ba_column . ' secondTest">';
}

if ( ! empty( $brand_image ) && is_user_logged_in() ) {
	$output .= '<div class="wpr-dealer-logo">' . $brand_image . '</div>';
}

if ( is_archive() ) {
	$output .= '<div class="wpr-dealer-details-archive">';
	$output .= $ba_content;
}

if ( ! empty( $dealer_fields['wpr_store_address'] ) ) {
	$store_address .= $dealer_fields['wpr_store_address'] . ', ';
	$google_map    .= $dealer_fields['wpr_store_address'] . ', ';
} else {
	$store_address .= get_post_meta( get_the_ID(), 'wpr_store_address', true ) . ', ';
	$google_map    .= get_post_meta( get_the_ID(), 'wpr_store_address', true ) . ', ';
}
if ( ! empty( $dealer_fields['wpr_store_city'] ) ) {
	$store_address .= $dealer_fields['wpr_store_city'] . ', ';
	$google_map    .= $dealer_fields['wpr_store_city'] . ', ';
} else {
	$store_address .= get_post_meta( get_the_ID(), 'wpr_store_city', true ) . ', ';
	$google_map    .= get_post_meta( get_the_ID(), 'wpr_store_city', true ) . ', ';
}
if ( ! empty( $dealer_fields['wpr_store_state'] ) ) {
	$store_address .= $dealer_fields['wpr_store_state'] . ', ';
	$google_map    .= $dealer_fields['wpr_store_state'] . ', ';
} else {
	$store_address .= get_post_meta( get_the_ID(), 'wpr_store_state', true ) . ', ';
	$google_map    .= get_post_meta( get_the_ID(), 'wpr_store_state', true ) . ', ';
}

if ( ! empty( $dealer_fields['wpr_store_phone_number'] ) ) {
	$store_address .= $dealer_fields['wpr_store_phone_number'] . ', ';
} else {
	$store_address .= get_post_meta( get_the_ID(), 'wpr_store_phone_number', true ) . ', ';
}

if ( false === $got_a_ba || is_singular() ) {
	$output .= '<div class="wpr-dealer-name">' . get_the_title() . '</div>';

	$output .= '<div class="wpr-dealer-address">';
	if ( ! is_singular() ) {
		$output .= '<span>' . __( 'Address' ) . '</span>';
	}

	$output .= substr( $store_address, 0, - 2 ) . '</div>';
	if ( ! empty( $dealer_fields['wpr_store_hours'] ) ) {
		$store_hours = $dealer_fields['wpr_store_hours'];
	} else {
		$store_hours = get_post_meta( get_the_ID(), 'wpr_store_hours', true );
	}

	if ( ! empty( $store_hours ) ) {
		$output .= '<div class="wpr-dealer-hours"><span>' . __( 'Store hours' ) . '</span>' . $store_hours . '</div>';
	}
}

if ( true === $got_a_ba ) {
	$appointment_btn = ' appointment';
	$learn_more      = ' learn_more';
}

if ( ! empty( $catalog_url ) && is_single() ) {
//	$output .= '<div class="wpr-dealer-loc-button' . $appointment_btn . '"><a href="#" class="btn appointment wpr-set-appointment" data-do="appointment">' . __( 'Set an appointment', 'wpr-dealers' ) . '</a></div>';
}

if ( ! empty( $catalog_url ) && is_single() ) {
//	$output .= '<div class="wpr-dealer-loc-button' . $learn_more . '"><a href="#" class="btn wpr-learn-more" data-do="learn">' . __( 'Learn More', 'wpr-dealers' ) . '</a></div>';
}

switch_to_blog( 1 );
$wpr_brand_catalog = get_term_meta( $category_id, 'wpr_brand_catalog', true );
restore_current_blog();

if ( ! empty( $google_map ) ) {
	$output .= '<div class="wpr-dealer-loc-button"><a href="https://www.google.com/maps?q=' . substr( $google_map, 0, - 2 ) . '" target="_blank" class="btn wpr-map-location" data-do="map">' . __( 'Map Location', 'wpr-dealers' ) . '</a></div>';
}

if ( $wpr_brand_catalog && is_singular() ) {
	$output .= '<div class="wpr-dealer-loc-button"><a href="' . esc_url( $wpr_brand_catalog ) . '" target="_blank" class="btn wpr-brand-catalog">' . __( 'See brand catalog', 'wpr-dealers' ) . ' <i class="fa fa-external-link" aria-hidden="true"></i></a></div>';
}
if ( is_singular() ) {
	$output .= '</div>';
//	$output .= '<div class="wpr-dealer-loc-button wpr-follow-button"><a href="#" class="btn wpr-follow" data-do="follow">' . __( 'Follow', 'wpr-dealers' ) . '</a></div>';
	$output .= $ba_content;
}

if ( is_archive() ) {
	$output .= '</div>';
}