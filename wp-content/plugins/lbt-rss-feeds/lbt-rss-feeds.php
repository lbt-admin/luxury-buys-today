<?php
/**
 * Plugin Name: LBT RSS Feed
 * Description: Custom RSS Feeds for each category
 * Author:      Webpigment
 * Author URI:  https://www.webpigment.com
 * Version:     1.0.0
 *
 * @package    LBTRSSFeed
 * @author     Webpigment
 * @since      1.0.3
 * @license    GPL-3.0+
 * @copyright  Copyright (c) 2021, Webpigment
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * LBT Rss Feed
 */
class LBT_RSS_Feed {
	/**
	 * Instance of the plugin
	 *
	 * @var LBT_RSS_Feed
	 */
	private static $instance;

	/**
	 * Plugin version
	 *
	 * @var string
	 */
	private $version = '1.0.0';

	/**
	 * Instanciate the plugin
	 */
	public static function get_instance() {
		if ( ! isset( self::$instance ) && ! ( self::$instance instanceof LBT_RSS_Feed ) ) {
			self::$instance = new LBT_RSS_Feed();
			self::$instance->constants();

			add_action( 'plugins_loaded', array( self::$instance, 'initialize' ) );
		}

		return self::$instance;
	}

	/**
	 * Define plugin constants
	 *
	 * @return  void
	 */
	private function constants() {
		// Plugin version.
		if ( ! defined( 'LBT_RSS_FEED_VERSION' ) ) {
			define( 'LBT_RSS_FEED_VERSION', $this->version );
		}

		// Plugin Folder Path.
		if ( ! defined( 'LBT_RSS_FEED_PLUGIN_DIR' ) ) {
			define( 'LBT_RSS_FEED_PLUGIN_DIR', trailingslashit( plugin_dir_path( __FILE__ ) ) );
		}
	}

	/**
	 * Initialize classes / objects here
	 *
	 * @return  void
	 */
	public function initialize() {
		add_action( 'init', array( $this, 'register_rss' ) );
	}

	/**
	 * Register RSS Feeds
	 *
	 * @return  void
	 */
	public function register_rss() {
		add_filter( 'the_excerpt_rss', array( $this, 'image_to_rss' ) );
		add_filter( 'the_content_feed', array( $this, 'image_to_rss' ) );

		add_feed( 'automobiles', array( $this, 'automobiles_rss_feed_template' ) );
		add_feed( 'fashion', array( $this, 'fashion_rss_feed_template' ) );
		add_feed( 'homedecor', array( $this, 'homedecor_rss_feed_template' ) );
		add_feed( 'properties', array( $this, 'properties_rss_feed_template' ) );
	}

	/**
	 * Load RSS Feed Template
	 *
	 * @return  void
	 */
	public function automobiles_rss_feed_template() {
		$category_slug = 'automotive';
		$category_name = 'Automobiles';
		$query         = $this->fetch_posts_by_category_slug( $category_slug );
		require_once LBT_RSS_FEED_PLUGIN_DIR . 'templates/rss/feed.php';
	}

	/**
	 * Load RSS Feed Template
	 *
	 * @return  void
	 */
	public function fashion_rss_feed_template() {
		$category_slug = 'fashion';
		$category_name = 'Fashion';
		$query         = $this->fetch_posts_by_category_slug( $category_slug );
		require_once LBT_RSS_FEED_PLUGIN_DIR . 'templates/rss/feed.php';
	}

	/**
	 * Load RSS Feed Template
	 *
	 * @return  void
	 */
	public function homedecor_rss_feed_template() {
		$category_slug = 'home-decor';
		$category_name = 'Home Decor';
		$query         = $this->fetch_posts_by_category_slug( $category_slug );
		require_once LBT_RSS_FEED_PLUGIN_DIR . 'templates/rss/feed.php';
	}

	/**
	 * Load RSS Feed Template
	 *
	 * @return  void
	 */
	public function properties_rss_feed_template() {
		$category_slug = 'properties';
		$category_name = 'Properties';
		$query         = $this->fetch_posts_by_category_slug( $category_slug );
		require_once LBT_RSS_FEED_PLUGIN_DIR . 'templates/rss/feed.php';
	}

	/**
	 * Fetch posts by category slug
	 *
	 * @param   string $category_slug  The category slug.
	 * @param   int    $total_posts  The number of posts to be fetched.
	 *
	 * @return  WP_Query
	 */
	private function fetch_posts_by_category_slug( $category_slug, $total_posts = 20 ) {
		return new WP_Query(
			array(
				'post_type'      => 'post',
				'posts_per_page' => $total_posts,
				'category_name'  => $category_slug,
			)
		);
	}

	/**
	 * Add Image to RSS
	 *
	 * @param   string $content The content.
	 *
	 * @return  string
	 */
	public function image_to_rss( $content ) {
		global $post;

		$image = get_post_meta( $post->ID, 'lbt_img', true );

		if ( empty( $image ) ) {
			$image = get_post_meta( $post->ID, 'wpr_remote_featured_img', true );
		}

		if ( ! empty( $image ) ) {
			$content = '<img src="' . $image . '" style="margin-bottom: 15px;"/>' . $content;
		}

		return $content;
	}
}

LBT_RSS_Feed::get_instance();
