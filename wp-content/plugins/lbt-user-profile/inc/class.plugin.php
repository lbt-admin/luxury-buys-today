<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class LBT_UserProfile {

    // Nonce handle
    const NONCE = 'LBT_UserProfile';

    /**
     * Plugin Version
     *
     * @since 1.2.0
     * @var string The plugin version.
     */
    const VERSION = '1.0.0';

    /**
     * Singleton instance
     * @var LBT_UserProfile
     */
    protected static $instance = null;

    /**
     * Singleton constructor
     * @return LBT_UserProfile
     */
    public static function instance() {

        if ( !isset( static::$instance ) ) {
            static::$instance = new self;
        }
        return static::$instance;

    }

    /**
     * Constructor
     *
     * @since 1.0.0
     * @access public
     */
    public function __construct() {

        require LBTUP_PATH . 'vendor/autoload.php';

        // Require plugin class files
        require 'class.ontraport.php';
        require 'class.profile-form.php';
        require 'class.avatar-upload.php';

        // Initialize init function on plugin init
        add_action( 'init', [$this, 'init'] );

        // Enqueue JavaScript & CSS files
        add_action( 'wp_enqueue_scripts', [$this, 'enqueue_plugin_files'] );

    }

    public function init() {
        new LBTUP_Ontraport;
        new LBTUP_ProfileForm;
        new LBTUP_AvatarUpload;
    }

    // Enqueue JavaScript & CSS files
    public function enqueue_plugin_files() {

        wp_enqueue_script( 'validate', LBTUP_ASSETS_URL . 'jquery.validate.js', [ 'jquery' ], null, true );
        wp_enqueue_script( 'croppie', LBTUP_ASSETS_URL . 'croppie.js', [ 'jquery' ], null, true );
        wp_enqueue_script( 'lbtup_js', LBTUP_ASSETS_URL . 'app.js', [ 'jquery', 'croppie', 'validate' ], null, true );

        wp_enqueue_style(  'croppie', LBTUP_ASSETS_URL . 'croppie.css', null, null );
        wp_enqueue_style(  'lbtup_css', LBTUP_ASSETS_URL . 'style.css', null, null );

        wp_localize_script( 'lbtup_js', 'LBTUP', array(
            'ajax_url' => admin_url( 'admin-ajax.php' ),
            'nonce' => wp_create_nonce( self::NONCE ),
        ) );

    }

}

LBT_UserProfile::instance();