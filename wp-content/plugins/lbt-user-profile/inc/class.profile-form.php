<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class LBTUP_ProfileForm {

    /**
     * Constructor
     *
     * @since 1.0.0
     * @access public
     */
    public function __construct() {

        add_action( 'wp_ajax_lbtup_profile_edit', [ $this, 'profile_edit_ajax' ], 0 );

		// TODO: why do we need nopriv here?
        add_action( 'wp_ajax_nopriv_lbtup_profile_edit', [ $this, 'profile_edit_ajax' ], 0 );

    }

    public function profile_edit_ajax() {

        $phone = preg_replace( '~.*(\d{3})[^\d]{0,7}(\d{3})[^\d]{0,7}(\d{4}).*~', '($1) $2-$3', $_POST['phone'] );

        $params = array(
            'email' => sanitize_email( $_POST['email'] ),
            'firstname' => $_POST['firstname'],
            'lastname' => $_POST['lastname'],
            'city' => $_POST['city'],
            'state' => $_POST['state'],
            'zip' => $_POST['zip'],
            'phone' => $phone,
            'gender' => $_POST['gender'],
            'facebook' => $_POST['facebook'],
            'linkedin' => $_POST['linkedin'],
            'instagram' => $_POST['instagram'],
            'twitter' => $_POST['twitter'],
            'pinterest' => $_POST['pinterest'],
            'background' => $_POST['background'],
        );

        // Ontraport API call to retrieve the current contact
        $ontraport = new LBTUP_Ontraport();
        $response = $ontraport->update_contact_info( $params );
        
        if ( $response['code'] === 200 ) {
            $this->update_wordpress_user( $params );

            wp_send_json_success( array(
                'response' => $response,
            ) );
        } else {
            wp_send_json_error( array(
                'response' => $response,
	            'data' => $params,
            ) );
        }

    }

    public function update_wordpress_user( $params ) {

        $user = wp_get_current_user();
        $user_id = $user->ID;

        update_user_meta( $user_id, 'first_name', esc_attr( $params['firstname'] ) );
        update_user_meta( $user_id, 'last_name', esc_attr( $params['lastname'] ) );
        update_user_meta( $user_id, 'city', esc_attr( $params['city'] ) );
        update_user_meta( $user_id, 'state', esc_attr( $params['state'] ) );
        update_user_meta( $user_id, 'zip_code', esc_attr( $params['zip'] ) );
        update_user_meta( $user_id, 'phone', esc_attr( $params['phone'] ) );
        update_user_meta( $user_id, 'gender', esc_attr( $params['gender'] ) );
        update_user_meta( $user_id, 'facebook', esc_attr( $params['facebook'] ) );
        update_user_meta( $user_id, 'linkedin', esc_attr( $params['linkedin'] ) );
        update_user_meta( $user_id, 'instagram', esc_attr( $params['instagram'] ) );
        update_user_meta( $user_id, 'twitter', esc_attr( $params['twitter'] ) );
        update_user_meta( $user_id, 'pinterest', esc_attr( $params['pinterest'] ) );
        update_user_meta( $user_id, 'profile_background', esc_attr( $params['background'] ) );

    }

    public function profile_form_html() {

        $user = wp_get_current_user();
        $user_id = $user->ID;
        $user_info = get_userdata( $user_id );

        $email = $user_info->user_email;
        $first_name = get_user_meta( $user_id, 'first_name', true );
        $last_name = get_user_meta( $user_id, 'last_name', true );
        $phone = get_user_meta( $user_id, 'phone', true );
        $city = get_user_meta( $user_id, 'city', true );
        $state = get_user_meta( $user_id, 'state', true );
        $zip_code = get_user_meta( $user_id, 'zip_code', true );
        $gender = get_user_meta( $user_id, 'gender', true );
        $facebook = get_user_meta( $user_id, 'facebook', true );
        $linkedin = get_user_meta( $user_id, 'linkedin', true );
        $instagram = get_user_meta( $user_id, 'instagram', true );
        $twitter = get_user_meta( $user_id, 'twitter', true );
        $pinterest = get_user_meta( $user_id, 'pinterest', true );
        $birthdate = get_user_meta( $user_id, 'birthdate', true );
        $profile_background = get_user_meta( $user_id, 'profile_background', true );
        ?>

        <form class="edit-profile" action="<?php echo get_permalink(); ?>" method="post" data-action="profile-edit">
            <div class="messages">
                <p class="error">Something went wrong! Please try again later.</p>
                <p class="updated">Your profile was updated successfully! You're being redirected in 5 seconds...</p>
            </div>
            <h3 class="title">Account Information</h3>
            <div class="form-group">
                <label for="email_address">Email Address</label>
                <input id="email_address" name="email_address" class="form-control" type="email" value="<?php echo $email; ?>" disabled="disabled">
            </div>
            <div class="form-row">
                <div class="form-column half">
                    <div class="form-group">
                        <label for="first_name">First Name*</label>
                        <input id="first_name" name="first_name" class="form-control" type="text" value="<?php echo $first_name; ?>">
                    </div>
                </div>
                <div class="form-column half">
                    <div class="form-group">
                        <label for="last_name">Last Name*</label>
                        <input id="last_name" name="last_name" class="form-control" type="text" value="<?php echo $last_name; ?>">
                    </div>
                </div>
            </div><!-- .form-row -->
            <div class="form-row">
                <div class="form-column half">
                    <div class="form-group">
                        <label for="city">City</label>
                        <input id="city" name="city" class="form-control" type="text" value="<?php echo $city; ?>">
                    </div>
                </div>
                <div class="form-column quarter">
                    <div class="form-group">
                        <label for="state">State</label>
                        <input id="state" name="state" class="form-control" type="text" value="<?php echo $state; ?>">
                    </div>
                </div>
                <div class="form-column quarter">
                    <div class="form-group">
                        <label for="zip_code">Zip Code*</label>
                        <input id="zip_code" name="zip_code" class="form-control" type="text" value="<?php echo $zip_code; ?>">
                    </div>
                </div>
            </div><!-- .form-row -->
            <div class="form-group">
                <label for="cell_phone">Cell Phone*</label>
                <input id="cell_phone" name="cell_phone" class="form-control" type="tel" value="<?php echo $phone; ?>">
            </div>
	        <div class="form-group">
                <label for="cell_phone">Birthday</label>
                <input id="birthdate" name="birthdate" class="form-control" type="text" value="<?php echo $birthdate; ?>">
            </div>
            <div class="form-group">
                <div class="label">Gender</div>
                <input type="radio" id="male" name="gender" value="male" <?php echo ($gender === 'male') ? 'checked' : ''?>>
                <label for="male">Male</label>
                <span>&nbsp;&nbsp;</span>
                <input type="radio" id="female" name="gender" value="female" <?php echo ($gender === 'female') ? 'checked' : ''?>>
                <label for="female">Female</label>
                <span>&nbsp;&nbsp;</span>
                <input type="radio" id="prefer-not-to-say" name="gender" value="prefer not to say" <?php echo ($gender === 'prefer not to say') ? 'checked' : ''?>>
                <label for="prefer-not-to-say">Prefer not to say</label>
            </div>
            <!-- Social Media -->
            <h3 class="title">My Social Media</h3>
            <div class="form-group">
                <label for="facebook">Facebook URL</label>
                <input id="facebook" name="facebook" class="form-control" type="text" value="<?php echo $facebook; ?>">
            </div>
            <div class="form-group">
                <label for="linkedin">LinkedIn URL</label>
                <input id="linkedin" name="linkedin" class="form-control" type="text" value="<?php echo $linkedin; ?>">
            </div>
            <div class="form-group">
                <label for="instagram">Instagram URL</label>
                <input id="instagram" name="instagram" class="form-control" type="text" value="<?php echo $instagram; ?>">
            </div>
            <div class="form-group">
                <label for="twitter">Twitter URL</label>
                <input id="twitter" name="twitter" class="form-control" type="text" value="<?php echo $twitter; ?>">
            </div>
            <div class="form-group">
                <label for="pinterest">Pinterest URL</label>
                <input id="pinterest" name="pinterest" class="form-control" type="text" value="<?php echo $pinterest; ?>">
            </div>
            <!-- Profile Background -->
            <h3 class="title">Profile Background</h3>
            <div class="form-row">
                <div class="form-column half">
                    <div class="form-group">
                        <label for="profile_background_1" class="profile-background option-1">
                            <input type="radio"
                                   id="profile_background_1"
                                   name="profile_background"
                                   value="option_1"
                                <?php echo ($profile_background === 'option_1') ? 'checked' : ''?>>
                            <span class="box"></span>
                        </label>
                    </div>
                </div>
                <div class="form-column half">
                    <div class="form-group">
                        <label for="profile_background_2" class="profile-background option-2">
                            <input type="radio"
                                   id="profile_background_2"
                                   name="profile_background"
                                   value="option_2"
                                <?php echo ($profile_background === 'option_2') ? 'checked' : ''?>>
                            <span class="box"></span>
                        </label>
                    </div>
                </div>
                <div class="form-column half">
                    <div class="form-group">
                        <label for="profile_background_3" class="profile-background option-3">
                            <input type="radio"
                                   id="profile_background_3"
                                   name="profile_background"
                                   value="option_3"
                                <?php echo ($profile_background === 'option_3') ? 'checked' : ''?>>
                            <span class="box"></span>
                        </label>
                    </div>
                </div>
                <div class="form-column half">
                    <div class="form-group">
                        <label for="profile_background_4" class="profile-background option-4">
                            <input type="radio"
                                   id="profile_background_4"
                                   name="profile_background"
                                   value="option_4"
                                <?php echo ($profile_background === 'option_4') ? 'checked' : ''?>>
                            <span class="box"></span>
                        </label>
                    </div>
                </div>
            </div>
            <input type="submit" class="btn-profile" value="Update Profile"/>
            <?php wp_nonce_field( 'update-user' ) ?>
            <div class="lbt-loading-animation">
                <div class="inner">
                    <img alt="Loading..." src="https://luxurybuystoday.com/wp-content/plugins/wpr-infinite-scroll-masonry/images/loading.gif">
                    <div class="message">Updating your information. Please wait.</div>
                </div>
            </div>
        </form>

        <?php
    }

}
