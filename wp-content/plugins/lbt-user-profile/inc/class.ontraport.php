<?php

use OntraportAPI\Ontraport;
use OntraportAPI\Criteria;
use OntraportAPI\ObjectType;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class LBTUP_Ontraport {

    /*
     * Connects to Ontraport via their API
     */
    public function ontraport() {

        return new Ontraport('2_137325_WSrzvR2KM', '8qCBDoPuo2RGqst');

    }

    public function update_contact_info( $params ) {

        $client = $this->ontraport();

        $contact = $this->check_ontraport_contact( $params['email'] );

		$requestParams = array(
			"firstname" => $params['firstname'],
			"lastname" => $params['lastname'],
			"city" => $params['city'],
			"state" => $params['state'],
			"zip" => $params['zip'],
			"sms_number" => $params['phone'],
			"f3010" => $params['gender'],
			"f1454" => $params['facebook'],
			"f1455" => $params['linkedin'],
			"f2860" => $params['instagram'],
			"f2862" => $params['twitter'],
			"f2861" => $params['pinterest'],
		);

        if ( !empty( $contact ) ) {
            $id = $contact[0]['id'];

            $requestParams['id'] = $id;

            $response = $client->contact()->update($requestParams);

			$wpr_ontraport_id = get_user_meta( get_current_user_id(), 'wpr_ontraport_id', true );

			if ( ! $wpr_ontraport_id ) {
				update_user_meta( get_current_user_id(), 'wpr_ontraport_id', $id );
			}

            return [
                'code' => 200,
                'response' => json_decode( $response, true )
            ];
        } else {
            $requestParams['email'] = $params['email'];
			$requestParams['f2882'] = '183';

			$response = $client->contact()->create($requestParams);
			$response = json_decode( $response, true );

			$wpr_ontraport_id = get_user_meta( get_current_user_id(), 'wpr_ontraport_id', true );

			if ( ! $wpr_ontraport_id && ! empty( $response['data']['id'] ) ) {
				update_user_meta( get_current_user_id(), 'wpr_ontraport_id', $response['data']['id'] );
			}

            return [
                'code' => 200,
                'response' => $response
            ];
		}

        return [
            'code' => 503,
            'response' => 'Something is wrong!'
        ];

    }

    public function update_contact_avatar( $params ) {

        $client = $this->ontraport();

        $contact = $this->check_ontraport_contact( $params['email'] );

        if ( !empty( $contact ) ) {
            $id = $contact[0]['id'];

            $requestParams = array(
                "id"        => $id,
                "f2863" => $params['image'],
            );
            $response = $client->contact()->update($requestParams);

            return [
                'code' => 200,
                'response' => json_decode( $response, true )
            ];
        }

        return [
            'code' => 503,
            'response' => 'Something is wrong!'
        ];

    }

    public function check_ontraport_contact( $email ) {

        $client = $this->ontraport();

        // Conditional filtering to check if the contact email is in use with another account
        $condition = new Criteria('email',  '=', $email );

        $params_validate = array(
            'id' => 0,
            'condition' => $condition->fromArray(),
        );

        $response_validate = $client->contact()->retrieveMultiple($params_validate);
        $data_validate = json_decode( $response_validate, true );

        return $data_validate['data'];

    }

}
