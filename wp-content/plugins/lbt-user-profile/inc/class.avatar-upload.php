<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class LBTUP_AvatarUpload {

    /**
     * Constructor
     *
     * @since 1.0.0
     * @access public
     */
    public function __construct() {

        add_action( 'wp_ajax_lbtup_avatar_upload', [ $this, 'profile_avatar_upload' ], 0 );
        add_action( 'wp_ajax_nopriv_lbtup_avatar_upload', [ $this, 'profile_avatar_upload' ], 0 );

    }

    public function profile_avatar_upload() {

        $user = wp_get_current_user();
        $user_id = $user->ID;
        $email = $user->user_email;
        $username = $user->user_login;
        $image = $_POST['image'];

        $attach_id = $this->save_image( $image, $username );
        $image_url = wp_get_attachment_image_url( $attach_id, '' );

        $params = [
            'email' => $email,
            'image' => $image_url
        ];

        update_user_meta( $user_id, 'lbt_user_avatar', $image_url );

        $ontraport = new LBTUP_Ontraport();
        $reponse = $ontraport->update_contact_avatar( $params );

        wp_send_json_success( array(
            'response' => $reponse,
        ) );

    }

    public function save_image( $base64_img, $title ) {

        // Upload dir.
        $upload_dir  = wp_upload_dir();
        $upload_path = str_replace( '/', DIRECTORY_SEPARATOR, $upload_dir['path'] ) . DIRECTORY_SEPARATOR;

        $img             = str_replace( 'data:image/jpeg;base64,', '', $base64_img );
        $img             = str_replace( ' ', '+', $img );
        $decoded         = base64_decode( $img );
        $filename        = $title . '.jpeg';
        $file_type       = 'image/jpeg';
        $hashed_filename = md5( $filename . microtime() ) . '_' . $filename;

        // Save the image in the uploads directory.
        $upload_file = file_put_contents( $upload_path . $hashed_filename, $decoded );

        $attachment = array(
            'post_mime_type' => $file_type,
            'post_title'     => preg_replace( '/\.[^.]+$/', '', basename( $hashed_filename ) ),
            'post_content'   => '',
            'post_status'    => 'inherit',
            'guid'           => $upload_dir['url'] . '/' . basename( $hashed_filename )
        );

        $attach_id = wp_insert_attachment( $attachment, $upload_dir['path'] . '/' . $hashed_filename );

        return $attach_id;
    }

    public function avatar_upload_html() {
        ?>
        <div class="avatar-uploader" data-target="drop-zone">
            <div class="content-inner">
                <div class="close" data-action="close-modal">Close</div>
                <div class="messages">
                    <p class="error">Something went wrong! Please try again later.</p>
                    <p class="updated">Your photo was updated successfully! You're being redirected in 5 seconds...</p>
                </div>
                <div class="drop-zone" data-action="avatar-input">
                    <span class="text">Drag & Drop your photo or <i>Browse</i></span>
                </div>
                <div class="crop-zone">
                    <div class="cropper" data-target="cropper"></div>
                    <div class="cropper-buttons">
                        <div class="btn-wrapper">
                            <label class="btn-profile" for="avatar_input">Browse</label>
                        </div>
                        <div class="btn-wrapper active">
                            <div class="btn-profile" data-action="avatar-upload">Upload</div>
                        </div>
                    </div>
                </div>
                <div class="lbt-loading-animation">
                    <div class="inner">
                        <img alt="Loading..." src="https://luxurybuystoday.com/wp-content/plugins/wpr-infinite-scroll-masonry/images/loading.gif">
                        <div class="message">Uploading your photo. Please wait.</div>
                    </div>
                </div>
                <input type="file" id="avatar_input" data-target="avatar-input" value="Choose a file" accept="image/*">
            </div>
        </div>
        <?php
    }

}