<?php
/**
 * Plugin Name: LBT User Profile
 * Description: Front-end user profile form with Ontraport integration.
 * Version: 1.0.0
 * Author: Ugurcan Bulut
 * Author URI: https://ugurcan.me
 * Text Domain: lbt-user-profile
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

// Define constant variables
define( 'LBTUP_PATH', plugin_dir_path( __FILE__ ) );
define( 'LBTUP_ASSETS_URL', plugin_dir_url( __FILE__ ) . 'assets/' );

// Require plugin file once
require_once 'inc/class.plugin.php';