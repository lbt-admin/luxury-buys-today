(function ($) {

    "use strict";

    const $document = $(document),
        $body = $('body'),
        $site_url = window.location.origin,
        avatarInput = $body.find('[data-target="avatar-input"]'),
        dropZone = $body.find('[data-target="drop-zone"]'),
        cropper = $body.find('[data-target="cropper"]');

    function LBTUP_ProfileEditForm() {

        $document.on('submit', '[data-action="profile-edit"]', function (e) {
            e.preventDefault();

            const $this = $(this),
                loading = $this.find('.lbt-loading-animation'),
                error = $this.find('.messages .error'),
                success = $this.find('.messages .updated'),
                headerHeight = $body.find('.site-header').outerHeight();

            const params = {
                action: 'lbtup_profile_edit',
                email: $this.find('#email_address').val(),
                firstname: $this.find('#first_name').val(),
                lastname: $this.find('#last_name').val(),
                city: $this.find('#city').val(),
                state: $this.find('#state').val(),
                zip: $this.find('#zip_code').val(),
                phone: $this.find('#cell_phone').val(),
                gender: $this.find('input[name="gender"]:checked').val(),
                facebook: $this.find('#facebook').val(),
                linkedin: $this.find('#linkedin').val(),
                instagram: $this.find('#instagram').val(),
                twitter: $this.find('#twitter').val(),
                pinterest: $this.find('#pinterest').val(),
                background: $this.find('input[name="profile_background"]:checked').val(),
            };

            $.ajax({
                url: LBTUP.ajax_url,
                type: 'POST',
                datatype: 'json',
                data: params,
                beforeSend: function() {
                    loading.show();
                    $('html, body').animate({
                        scrollTop: $('.user-profile').offset().top - headerHeight
                    }, 500);
                },
                success: function(response) {
                    loading.fadeOut(150);

                    if (response.success) {
                        success.addClass('published');

                        setTimeout(function () {
                            window.location.replace(`${$site_url}/my-profile/`);
                        }, 5000);
                    } else {
                        error.addClass('published');
                    }
                }
            });

        });

        $('[data-action="profile-edit"]').validate({
            rules: {
                first_name : {
                    required: true
                },
                last_name : {
                    required: true
                },
                cell_phone : {
                    required: true,
                    phone_us: true,
                    minlength: 10,
                    maxlength: 14
                },
                zip_code: {
                    required: true,
                    digits: true,
                    minlength: 5,
                    maxlength: 5
                }
            }
        });

    }

    $.validator.addMethod('phone_us', function(phone_number, element) {
        phone_number = phone_number.replace(/\s+/g, "");
        return this.optional(element) || phone_number.length > 9 &&
            phone_number.match(/^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/);
    }, 'Please enter a valid phone number');

    function LBTUP_AvatarUpload() {

        cropper.croppie({
            enableExif: true,
            viewport: {
                width: 300,
                height: 300,
                type: 'circle'
            },
            boundary: {
                width: 300,
                height: 300
            }
        });

        //$document.off('click', '[data-action="avatar-input"]');
        $document.on('touchstart click', '[data-action="avatar-input"]', function () {
            avatarInput.click();
        });

        $document.on('click', '[data-action="avatar-popup"]', function () {
            $(this).closest('.lbt-my-profile').find('.avatar-uploader').addClass('active');
        });

        $document.on('click', '[data-action="close-modal"]', function () {
            $(this).closest('.avatar-uploader').removeClass('active');
        });

        $document.on('click', '[data-action="avatar-upload"]', function () {
            const $this = $(this),
                crop_zone = $this.closest('[data-target="drop-zone"]').find('.crop-zone'),
                loading = $this.closest('[data-target="drop-zone"]').find('.lbt-loading-animation'),
                error = $this.closest('[data-target="drop-zone"]').find('.messages .error'),
                success = $this.closest('[data-target="drop-zone"]').find('.messages .updated');

            cropper.croppie('result', {
                type: 'canvas',
                size: 'viewport',
                format: 'jpeg',
                circle: false
            }).then(function(resp) {
                const params = {
                    action: 'lbtup_avatar_upload',
                    image: resp
                };

                $.ajax({
                    url: LBTUP.ajax_url,
                    type: 'POST',
                    datatype: 'json',
                    data: params,
                    beforeSend: function() {
                        loading.show();
                    },
                    success: function(response) {
                        loading.fadeOut(150);

                        if (response.success) {
                            success.addClass('published');
                            crop_zone.hide();

                            setTimeout(function () {
                                window.location.replace(`${$site_url}/my-profile/`);
                            }, 5000);
                        } else {
                            error.addClass('published');
                        }
                    }
                });
            });
        })

        function readFile(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function(e) {
                    cropper.croppie('bind', {
                        url: e.target.result
                    });
                };

                reader.readAsDataURL(input.files[0]);
            }
        }

        avatarInput.on('change', function() {
            readFile(this);
            dropZone.find('.drop-zone').hide();
            dropZone.find('.crop-zone').show();
        });

        dropZone.on('dragover', false).on('drop', function (e) {
            e.preventDefault();
            e.stopPropagation();

            avatarInput.prop('files', e.originalEvent.dataTransfer.files);
            avatarInput.trigger('change');
        });

    }

    $document.ready(function () {

        LBTUP_ProfileEditForm();
        LBTUP_AvatarUpload();

    });

})(jQuery);