<?php
/**
 * Plugin Name: WPRiders redirect to catalogue
 * Plugin URI: http://www.wpriders.com
 * Description: This plugin creates replace the expired newsletter URL to catalogue URL
 * Version: 1.0.0
 * Author: Ovidiu George Irodiu from Wpriders
 * Author URI: http://www.wpriders.com
 * License: GPL2
 */
if ( ! defined( 'ABSPATH' ) ) {
	die( 'You are not allowed to call this page directly.' );
}

if ( ! class_exists( 'WPR_redirect_to_catalogue' ) ) {
	/**
	 * Class WPR_redirect_to_catalogue
	 */
	class WPR_redirect_to_catalogue {

		function __construct() {
			add_action( 'wp', array( &$this, 'wpr_get_post_content' ) );
		}

		function wpr_get_post_content() {
			if ( is_single() ) {
				$post_id = get_the_ID();
				if ( $post_id > 0 ) {
					$update_url = false;

					$check_updated = get_transient( 'wpr_url_changed_' . $post_id );
					if ( ! $check_updated ) {
						$post_url_changed = get_post_meta( $post_id, 'wpr_url_changed', true );
						$is_from_email    = get_post_meta( $post_id, 'wpr_email_source', true );

						if ( empty( $post_url_changed ) && ! empty( $is_from_email ) ) {
							global $wpdb;
							$post_content = get_post_field( 'post_content', $post_id );
							preg_match( '/<a[^>]*href=(["\']?([^"\'>]+)["\'])?>/', $post_content, $match );
							$goto = end($match);

							$url_result = $this->wpr_url_curl( $goto );

							if ( 404 === (int) $url_result['httpcode'] ) {
								$categories    = get_the_category( $post_id );
								$categories_id = array();
								if ( ! empty( $categories ) ) {
									foreach ( $categories as $category ) {
										$categories_id[] = $category->term_id;
									}
								}

								$get_catalog = $wpdb->get_row( $wpdb->prepare( "SELECT wpr_fallback_url FROM {$wpdb->prefix}parser_options WHERE wpr_category IN (%s)", implode( ',', $categories_id ) ) );

								if ( ! empty( $get_catalog ) ) {
									$newsletter_url = $get_catalog->wpr_fallback_url;

									$update_url = $wpdb->query(
										$wpdb->prepare(
											"UPDATE {$wpdb->posts} SET post_content = replace(post_content, '%s', '%s') WHERE ID = {$post_id}",
											$goto,
											$newsletter_url
										)
									);
								}
							} else if ( ! empty( $url_result['location'] ) ) {
								if ( $this->is_redirect_url( $url_result['location'] ) ) {
									$get_location = preg_split( '/(http|https)/', $url_result['location'] );
									if ( substr_count( $url_result['location'], 'https' ) ) {
										$newsletter_url = 'https' . str_replace( ':/', '://', $get_location[1] );
									} else {
										$newsletter_url = 'http' . str_replace( ':/', '://', $get_location[1] );
									}

									$url_result = $this->wpr_url_curl( $newsletter_url ); 

									if ( 404 === (int) $url_result['httpcode'] ) {
										$categories    = get_the_category( $post_id );
										$categories_id = array();
										if ( ! empty( $categories ) ) {
											foreach ( $categories as $category ) {
												$categories_id[] = $category->term_id;
											}
										}

										$get_catalog = $wpdb->get_row( $wpdb->prepare( "SELECT wpr_fallback_url FROM {$wpdb->prefix}parser_options WHERE wpr_category IN (%s)", implode( ',', $categories_id ) ) );

										if ( ! empty( $get_catalog ) ) {
											$catalog_url = $get_catalog->wpr_fallback_url;

											$update_url = $wpdb->query(
												$wpdb->prepare(
													"UPDATE {$wpdb->posts} SET post_content = replace(post_content, '%s', '%s') WHERE ID = {$post_id}",
													$goto,
													$catalog_url
												)
											);
										}
									} else {
										$update_url = $wpdb->query(
											$wpdb->prepare(
												"UPDATE {$wpdb->posts} SET post_content = replace(post_content, '%s', '%s') WHERE ID = {$post_id}",
												$goto,
												$newsletter_url
											)
										);
									}
								}
							}

							if ( $update_url ) {
								update_post_meta( $post_id, 'wpr_url_changed', 1 );
								$data_to_change = array(
									'oldurl' => $goto,
									'newurl' => $newsletter_url,
								);

								add_filter( 'the_content', function ( $content ) use ( $data_to_change ) {
									return str_replace( $data_to_change['oldurl'], $data_to_change['newurl'], $content );
								}, 30 );

								set_transient( 'wpr_url_changed_' . $post_id, 'true', 7 * DAY_IN_SECONDS );
							}
						}
					}
				}
			}
		}

		function wpr_url_curl( $url ) {
			$response = array();

			$ch = curl_init();
			curl_setopt( $ch, CURLOPT_URL, $url );
			curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );
			curl_setopt( $ch, CURLOPT_VERBOSE, 1 );
			curl_setopt( $ch, CURLOPT_HEADER, 1 );

			$head                 = curl_exec( $ch );
			$response['httpcode'] = curl_getinfo( $ch, CURLINFO_HTTP_CODE );
			curl_close( $ch );

			// get the return location
			if ( preg_match( '#Location: (.*)#', $head, $r ) ) {
				$response['location'] = trim( $r[1] );
			}

			return $response;
		}

		function is_redirect_url( $url ) {
			$get_location = preg_split( '/(http|https)/', $url );

			if ( $get_location[0] != '' ) {
				return true;
			} else {
				return false;
			}
		}
	}
}

$wpr_redirect_to_catalogue = new WPR_redirect_to_catalogue();