<?php
/**
 * Plugin Name:  WPRiders Posts Rating
 * Plugin URI:   https://www.wpriders.com/hire-us-ref
 * Description:  This plugin calculates the Broyhill rating
 * Author:       Ovidiu George Irodiu from WPRiders
 * Author URI:   https://www.wpriders.com/hire-us-ref
 * License:      GPL2
 * License URI:  https://www.gnu.org/licenses/gpl-2.0.html
 */

if ( ! defined( 'ABSPATH' ) ) {
    die( 'You are not allowed to call this page directly.' );
}

if ( ! defined( 'WPR_PR_SCORES_TRANSIENT' ) ) {
    define( 'WPR_PR_SCORES_TRANSIENT', 'wpr_pr_scores_transient' );
}


if ( ! class_exists( 'WPR_Posts_Rating' ) ) {

    include( plugin_dir_path( __FILE__ ) . 'inc/wp-async-request.php' );
    include( plugin_dir_path( __FILE__ ) . 'inc/wp-background-process.php' );
    include( plugin_dir_path( __FILE__ ) . 'inc/wpr_run_process.php' );

    /**
     * Class WPR_Posts_Rating
     */
    class WPR_Posts_Rating {
        /**
         * Options
         * @var
         */
        var $options;
        /**
         * Shared Count options
         * @var string
         */
        var $options_name = 'sharedcount_api_options';
        /**
         * @var string
         */
        private $wpr_sharecount_cron = 'wpr_sharecount_cron';

        /**
         * WPR_Posts_Rating constructor.
         */
        public function __construct() {
            /**
             * Create Rating Cron Job Table
             */
            register_activation_hook( __FILE__, array( &$this, 'create_post_rating_table' ) );

            $this->options = get_option( $this->options_name, true );
//			add_action( 'init', array( &$this, 'wpr_pr_update_transients' ) );
            add_shortcode( 'wpr_pr_get_rating', array( &$this, 'wpr_pr_get_rating_func' ) );

            add_action( 'admin_menu', array( &$this, 'sharedcount_admin_menu' ) );
            add_action( 'admin_init', array( &$this, 'admin_register_settings' ) );

            add_filter( 'cron_schedules', array( &$this, 'wpr_define_cron_jobs' ) );

            add_action( 'wpr_fetch_brand_bulk_posts_share', array( $this, 'wpr_fetch_brand_bulk_posts_share_function' ) );
            add_action( 'wpr_reset_fetching_share_count', array( $this, 'wpr_reset_fetching_share_count_function' ) );
        }

        /**
         * Create cron job table
         */
        public function create_post_rating_table() {
            global $wpdb;

            $wpr_sharecount_cron = $wpdb->prefix . $this->wpr_sharecount_cron;
            if ( $wpdb->get_var( "show tables like '$wpr_sharecount_cron'" ) !== $wpr_sharecount_cron ) {
                $sql = 'CREATE TABLE `' . $wpr_sharecount_cron . '` ( ';
                $sql .= '  `id` bigint(20) NOT NULL AUTO_INCREMENT, ';
                $sql .= '  `brand_id` bigint(20) NOT NULL, ';
                $sql .= '  `fetched_weekly_bulk` int(1) NOT NULL DEFAULT "0", ';
                $sql .= '  `fetched_bulk` int(1) NOT NULL DEFAULT "0", ';
                $sql .= '  PRIMARY KEY (`id`) ';
                $sql .= ") ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;";
                require_once( ABSPATH . '/wp-admin/includes/upgrade.php' );
                dbDelta( $sql );
            }

            $this->wpr_save_brands_term();

            // Register cron events
            if ( ! wp_next_scheduled( 'wpr_fetch_brand_bulk_posts_share' ) ) {
                wp_schedule_event( time(), '30min', 'wpr_fetch_brand_bulk_posts_share' );
            }

            if ( ! wp_next_scheduled( 'wpr_reset_fetching_share_count' ) ) {
                wp_schedule_event( time(), 'daily', 'wpr_reset_fetching_share_count' );
            }
        }

        /**
         * Define cron intervals
         *
         * @param $schedules
         *
         * @return mixed
         */
        function wpr_define_cron_jobs( $schedules ) {
            $schedules['30min'] = array(
                'interval' => MINUTE_IN_SECONDS * 30,
                'display'  => __( 'Every 30 minutes' )
            );

            return $schedules;
        }

        /**
         * Reset cron jobs
         */
        function wpr_reset_fetching_share_count_function() {
            global $wpdb;

            $reset_table = $wpdb->prefix . $this->wpr_sharecount_cron;
            $wpdb->query(
                $wpdb->prepare(
                    "
	                 UPDATE $reset_table
					 SET fetched_weekly_bulk = %d, fetched_bulk = %d
					",
                    0,
                    0
                )
            );
        }

        /**
         * Save Brand Categories
         */
        function wpr_save_brands_term() {
            $categories = get_categories( array(
                'orderby' => 'name',
                'order'   => 'ASC',
                'exclude' => array( 1, 8, 13, 14, 17, 36, 37, 38, 39 ),
            ) );

            if ( ! empty( $categories ) ) {
                foreach ( $categories as $category ) {
                    if ( $category->parent > 0 ) {
                        $this->wpr_do_the_brand_saving( $category->term_id );
                    }
                }
            }
        }

        /**
         * Add/Save Brand
         *
         * @param $brand_id
         */
        function wpr_do_the_brand_saving( $brand_id ) {
            if ( ! is_int( $brand_id ) ) {
                return;
            }

            $brand_data = array(
                'brand_id'            => absint( $brand_id ),
                'fetched_weekly_bulk' => 0,
                'fetched_bulk'        => 0,
            );

            if ( $this->wpr_is_brand_saved( $brand_id ) ) {
                $this->wpr_update_brand_data( $brand_data, $brand_id );
            } else {
                $this->wpr_insert_brand_data( $brand_data );
            }
        }

        /**
         * Cron Job to Fetch Weekly brand sharing count Cron Job
         */
        function wpr_fetch_brand_weekly_share_count_function() {
            $brand_id = $this->wpr_fetch_brand_to_execute( 'fetched_weekly_bulk' );

            if ( $brand_id ) {
                $posts_guids = $this->wpr_get_brandposts_guids_for_the_week( $brand_id );

                if ( $posts_guids ) {
                    $posts_urls = '';
                    foreach ( $posts_guids as $posts_guid ) {
                        $posts_urls .= "$posts_guid\n";
                    }

                    $urls     = <<<URLS
$posts_urls

URLS;
                    $response = $this->wpr_get_share_count( 'POST', $urls, '' );

                    if ( $response && array_key_exists( 'bulk_id', $response ) ) {
                        $register_data = array(
                            'fetch_urls_batch_week_sharing' => array(
                                'brand_id' => $brand_id,
                                'bulk_id'  => $response['bulk_id'],
                            ),
                        );

                        $request_share = new WPR_Run_Rating_Process();
                        $request_share->wpr_init_cron( $register_data );
                    }
                }
            }
        }

        /**
         * Cron Job to Fetch share number for last 300 articles for brand id
         */
        function wpr_fetch_brand_bulk_posts_share_function() {
            $brand_id = $this->wpr_fetch_brand_to_execute( 'fetched_bulk' );

            if ( $brand_id ) {
                $posts_guids = $this->wpr_get_brandposts_guids( $brand_id );

                if ( $posts_guids ) {
                    $posts_urls = '';
                    foreach ( $posts_guids as $posts_guid ) {
                        $posts_urls .= "$posts_guid\n";
                    }

                    $urls     = <<<URLS
$posts_urls

URLS;
                    $response = $this->wpr_get_share_count( 'POST', $urls, '' );

                    if ( $response && array_key_exists( 'bulk_id', $response ) ) {
                        $register_data = array(
                            'fetch_urls_batch_sharing' => array(
                                'brand_id' => $brand_id,
                                'bulk_id'  => $response['bulk_id'],
                            ),
                        );

                        $request_share = new WPR_Run_Rating_Process();
                        $request_share->wpr_init_cron( $register_data );

                        // Set as fetched
                        $this->wpr_set_as_fetched( $brand_id );
                    }
                }
            }
        }

        /**
         * Calculate total share
         *
         * @param $share_count
         *
         * @return int
         */
        function wpr_calculate_total_share_count( $share_count ) {
            if ( empty( $share_count ) ) {
                return 0;
            }

            $total_count = 0;
            if ( $share_count['data'] ) {
                foreach ( $share_count['data'] as $counter ) {
                    $total_count += $counter;
                    $total_count += $counter['Pinterest'];
                    $total_count += $counter['Twitter'];
                    $total_count += $counter['LinkedIn'];
                    $total_count += $counter['Facebook']['total_count'];
                    $total_count += $counter['GooglePlusOne'];
                }
            }

            return $total_count;
        }

        /**
         * Count total share for one article
         *
         * @param $count_for_guid
         *
         * @return int
         */
        function wpr_calculate_total_share_for_url( $count_for_guid ) {
            if ( empty( $count_for_guid ) ) {
                return 0;
            }

            $total_count = 0;
            foreach ( $count_for_guid as $counter ) {
                if ( $counter > 0 ) {
                    $total_count += (int) $counter;
                }

                if ( is_array( $counter ) ) {
                    $total_count += (int) $counter['total_count'];
                }
            }

            return $total_count;
        }

        /**
         * Check if Brand is saved in Facebook table
         *
         * @param $brand_id
         *
         * @return bool|null|string
         */
        function wpr_is_brand_saved( $brand_id ) {
            if ( empty( $brand_id ) ) {
                return false;
            }

            $get_brand = false;
            if ( is_int( $brand_id ) ) {
                global $wpdb;
                $wpr_sharecount_cron = $wpdb->prefix . $this->wpr_sharecount_cron;

                $get_brand = $wpdb->get_var( $wpdb->prepare( "SELECT COUNT(*) FROM {$wpr_sharecount_cron} WHERE brand_id = %d", $brand_id ) );
            }

            return $get_brand;
        }

        /**
         * Fetch post id by guid
         *
         * @param $guid
         *
         * @return null|string
         */
        function wpr_get_id_from_guid( $guid ) {
            global $wpdb;

            return $wpdb->get_var( $wpdb->prepare( "SELECT ID FROM $wpdb->posts WHERE guid=%s", $guid ) );
        }

        /**
         * Fetch not fetched Brand
         *
         * @param $column
         *
         * @return array|bool|null|object
         */
        function wpr_fetch_brand_to_execute( $column ) {
            global $wpdb;

            $table_brand = $wpdb->prefix . $this->wpr_sharecount_cron;
            $result      = $wpdb->get_results(
                $wpdb->prepare( "SELECT brand_id FROM $table_brand WHERE $column = %d LIMIT 1", 0 )
                , OBJECT );

            if ( $result ) {
                return $result[0]->brand_id;
            } else {
                return false;
            }
        }

        /**
         * Insert in table
         *
         * @param $save_data
         */
        function wpr_insert_brand_data( $save_data ) {
            global $wpdb;

            $wpdb->insert(
                $wpdb->prefix . $this->wpr_sharecount_cron,
                $save_data,
                array(
                    '%d',
                    '%d',
                    '%d'
                )
            );
        }

        /**
         * Update table
         *
         * @param $update_data
         * @param $brand_id
         */
        function wpr_update_brand_data( $update_data, $brand_id ) {
            global $wpdb;

            $wpdb->update(
                $wpdb->prefix . $this->wpr_sharecount_cron,
                $update_data,
                array( 'brand_id' => $brand_id ),
                array(
                    '%d',
                    '%d',
                    '%d'
                ),
                array( '%d' )
            );
        }

        /**
         * Set Brand as fetched
         *
         * @param $brand_id
         */
        function wpr_set_as_fetched( $brand_id ) {
            global $wpdb;

            $wpdb->update(
                $wpdb->prefix . $this->wpr_sharecount_cron,
                array(
                    'fetched_bulk' => 1,
                ),
                array( 'brand_id' => $brand_id ),
                array(
                    '%d'
                ),
                array( '%d' )
            );
        }

        /**
         * Add Shared Count settings page
         */
        function sharedcount_admin_menu() {
            add_options_page( 'Shared Count API', 'Shared Count API', 'manage_options', 'wp-sharedcount-api', array( &$this, 'sharedcount_api_options_page' ) );
        }

        /**
         * Option page
         */
        function sharedcount_api_options_page() {
            if ( ! current_user_can( 'manage_options' ) ) {
                wp_die( esc_html( 'You do not have sufficient permissions to access this page . ' ) );
            }

            include( plugin_dir_path( __FILE__ ) . 'inc/options-page.php' );
        }

        /**
         * Register all the settings for the options page (Settings API)
         *
         * @uses register_setting()
         * @uses add_settings_section()
         * @uses add_settings_field()
         */
        public function admin_register_settings() {
            register_setting( 'sharedcount_api_options', 'sharedcount_api_options', array( &$this, 'validate_settings' ) );
            add_settings_section( 'sharedcount_api_settings', 'Shared Count account', array( &$this, 'admin_section_code_settings' ), 'wp-sharedcount-api' );
            add_settings_field( 'sharedcount_api_key', 'API Key', array( &$this, 'admin_option_sync_key' ), 'wp-sharedcount-api', 'sharedcount_api_settings' );
        }

        /**
         * Validates user supplied settings and sanitizes the input
         *
         * @param $input
         *
         * @return mixed|void
         */
        public function validate_settings( $input ) {
            if ( isset( $input['sharedcount_api_key'] ) ) {
                $options['sharedcount_api_key'] = trim( $input['sharedcount_api_key'] );
            }

            return $options;
        }

        /**
         * Output the description
         */
        public function admin_section_code_settings() {
            echo '<p>' . esc_html( 'Insert Shared Count API Key' ) . '</p>';
        }

        /**
         * Output the input option
         */
        public function admin_option_sync_key() {
            echo sprintf( "<input type='text' name='sharedcount_api_options[sharedcount_api_key]' size='20' value='%s' />", esc_attr( $this->get_option( 'sharedcount_api_key' ) ) );
        }

        /**
         * Display rating stars
         *
         * @param $atts
         */
        public function wpr_pr_get_rating_func( $atts ) {
            global $wpdb;
            $post_id = get_the_ID();

            if ( ! $post_id ) {
                return;
            }

            $brand_posts_weekly = 0;
            $brand_id           = $this->wpr_get_post_category_id( $post_id );
            if ( $brand_id ) {
                $brand_posts_weekly = $this->wpr_get_brand_max_number_of_views_for_the_week( $brand_id );
            }

            $total_shares = 0;
            if ( get_post_meta( $post_id, 'wpr_article_share_count', true ) ) {
                $total_shares = get_post_meta( $post_id, 'wpr_article_share_count', true );
            }

            $postViewsObject = $wpdb->get_var(
                $wpdb->prepare(
                    "SELECT count FROM {$wpdb->prefix}post_views WHERE id = %d AND period = 'total' ",
                    $post_id
                )
            );
            $total_views     = 0;
            if ( ! empty( $postViewsObject ) ) {
                $total_views = absint( $postViewsObject );
            }

            if ( $brand_posts_weekly > 0 ) {
                $last_week = $brand_posts_weekly * 5;
                $rating    = floor( ( $total_views+10 ) / 10 );
            } else {
                $rating = 1;
            }

            $stars = "";
            // No ZERO rating
            if ( 0 == $rating ) {
                $rating = 1;
            } elseif ( 5 < $rating ) {
                $rating = 5;
            }

            $ratingOutput = <<<OUTPUT
<div class="star-ratings-css">
	<div class="star-ratings-css-top">RATING_STARS</div>

</div>
OUTPUT;
            for ( $i = 0; $i < 5; $i ++ ) {


                $stars .= "<div";
                if($i+1<=$rating) {
                    $stars.=" class='filled' ";
                }

                $stars.=">★</div>";
            }

            $ratingOutput = str_replace( 'RATING_STARS', $stars, $ratingOutput );

            return $ratingOutput;
        }

        /**
         * Used for SEO title
         *
         * @return float|int|void
         */
        public function wpr_seo_broy_rating_no() {
            global $wpdb;
            $post_id = get_the_ID();

            if ( ! $post_id ) {
                return;
            }

            $brand_posts_weekly = 0;
            $brand_id           = $this->wpr_get_post_category_id( $post_id );
            if ( $brand_id ) {
                $brand_posts_weekly = $this->wpr_get_brand_max_number_of_views_for_the_week( $brand_id );
            }

            $total_shares = 0;
            if ( get_post_meta( $post_id, 'wpr_article_share_count', true ) ) {
                $total_shares = get_post_meta( $post_id, 'wpr_article_share_count', true );
            }

            $postViewsObject = $wpdb->get_var(
                $wpdb->prepare(
                    "SELECT count FROM {$wpdb->prefix}post_views WHERE id = %d AND period = 'total' ",
                    $post_id
                )
            );
            $total_views     = 0;
            if ( ! empty( $postViewsObject ) ) {
                $total_views = absint( $postViewsObject );
            }

            if ( $brand_posts_weekly > 0 ) {
                $last_week = $brand_posts_weekly * 5;
                $rating    = round( ( 10 * $total_shares + $total_views ) / $last_week );
            } else {
                $rating = 1;
            }

            // No ZERO rating
            if ( 0 == $rating ) {
                $rating = 1;
            } elseif ( 5 < $rating ) {
                $rating = 5;
            }

            return $rating;
        }

        /**
         * Fetch posts guid in last 7 days
         *
         * @param $brand_id
         *
         * @return array
         */
        public function wpr_get_brandposts_guids_for_the_week( $brand_id ) {
            $args = array(
                'post_type'      => 'post',
                'post_status'    => 'publish',
                'posts_per_page' => - 1,
                'orderby'        => 'date',
                'order'          => 'DESC',
                'cat'            => $brand_id,
                'date_query'     => array(
                    'after' => date( 'Y-m-d', strtotime( '-7 days' ) )
                )
            );

            $posts = new WP_Query( $args );

            $guids = array();

            if ( $posts->posts ) {
                foreach ( $posts->posts as $post ) {
                    array_push( $guids, $post->guid );
                }
            }

            return $guids;
        }

        /**
         * Fetch posts guid 300 rows
         *
         * @param $brand_id
         *
         * @return array
         */
        public function wpr_get_brandposts_guids( $brand_id ) {
            $args = array(
                'post_type'      => 'post',
                'post_status'    => 'publish',
                'posts_per_page' => 300,
                'orderby'        => 'date',
                'order'          => 'DESC',
                'cat'            => $brand_id,
            );

            $posts = new WP_Query( $args );

            $guids = array();

            if ( $posts->posts ) {
                foreach ( $posts->posts as $post ) {
                    array_push( $guids, $post->guid );
                }
            }

            return $guids;
        }

        /**
         * Fetch total views for Brand
         *
         * @param $brand_id
         *
         * @return array
         */
        public function wpr_get_brand_max_number_of_views_for_the_week( $brand_id ) {
            $args = array(
                'post_type'      => 'post',
                'post_status'    => 'publish',
                'posts_per_page' => - 1,
                'orderby'        => 'date',
                'order'          => 'DESC',
                'fields'         => 'ids',
                'cat'            => $brand_id,
                'date_query'     => array(
                    'after' => date( 'Y-m-d', strtotime( '-7 days' ) )
                )
            );

            $posts = new WP_Query( $args );

            $result = $this->wpr_get_total_views_for_brand( $posts->posts );
            wp_reset_postdata();

            return $result;
        }

        /**
         * Fetch total views number
         *
         * @param $posts
         *
         * @return int|mixed
         */
        public function wpr_get_total_views_for_brand( $posts ) {
            global $wpdb;

            $postsObj = array();
            if ( $posts ) {
                $total_views  = 0;
                $total_shares = 0;
                foreach ( $posts as $post ) {
                    $postViewsObject = $wpdb->get_var(
                        $wpdb->prepare(
                            "SELECT count FROM {$wpdb->prefix}post_views WHERE id = %d AND period = 'total' ",
                            $post
                        )
                    );

                    if ( ! empty( $postViewsObject ) ) {
                        $total_views = absint( $postViewsObject );
                    }

                    $total_share = get_post_meta( $post, 'wpr_article_share_count', true );
                    if ( $total_share ) {
                        $total_shares = absint( $total_share );
                    }

                    $postsObj[ $post ] = 10 * $total_shares + $total_views;
                }
            }

            if ( sizeof( $postsObj ) > 0 ) {
                return max( $postsObj );
            } else {
                return 0;
            }
        }

        /**
         * Fetch first posts first category id
         *
         * @param $post_id
         *
         * @return null
         */
        public function wpr_get_post_category_id( $post_id ) {
            if ( ! $post_id ) {
                return null;
            }
            $post_cats = get_the_category( $post_id );

            if ( empty( $post_cats ) ) {
                return null;
            }

            return absint( $post_cats[0]->term_id );
        }

        /**
         * Lookup an option from the options array
         *
         * @param $key
         *
         * @return null
         */
        public function get_option( $key ) {
            if ( isset( $this->options[ $key ] ) && '' != $this->options[ $key ] ) {
                return $this->options[ $key ];
            } else {
                return null;
            }
        }

        /**
         * Send bulk cURL request
         *
         * @param string $method
         * @param $content
         * @param string $bulk_id
         */
        public function wpr_get_share_count( $method = 'GET', $content, $bulk_id = '' ) {
            $ch = curl_init();

            $url = 'https://api.sharedcount.com/bulk?apikey=' . esc_attr( $this->get_option( 'sharedcount_api_key' ) );

            if ( '' != $bulk_id ) {
                $url .= '&bulk_id=' . $bulk_id;
            }

            if ( $method == 'POST' ) {
                curl_setopt( $ch, CURLOPT_POST, 1 );
                curl_setopt( $ch, CURLOPT_POSTFIELDS, $content );
                curl_setopt( $ch, CURLOPT_HTTPHEADER, array( 'Content-Type: text/plain' ) );
                curl_setopt( $ch, CURLOPT_ENCODING, '' );
            } else {
                curl_setopt( $ch, CURLOPT_HTTPHEADER, array( 'Content-Type: application/json' ) );
            }

            curl_setopt( $ch, CURLOPT_URL, $url );
            curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );
            curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, 1 );
            curl_setopt( $ch, CURLOPT_CONNECTTIMEOUT, 1 );
            curl_setopt( $ch, CURLOPT_TIMEOUT, 1 );
            $data = curl_exec( $ch );
            curl_close( $ch );

            return json_decode( $data, true );
        }

    }
}

$wpr_posts_rating = new WPR_Posts_Rating();