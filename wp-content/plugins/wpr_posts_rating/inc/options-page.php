<?php
if ( ! defined( 'ABSPATH' ) ) {
	die( 'You are not allowed to call this page directly.' );
}
?>
<div class="wrap">
	<div id="icon-options-drip" class="icon32"><br></div>
	<h2><?php echo __( 'Shared Count API Settings' ); ?></h2>

	<form name="sharedcount-settings-form" id="sharedcount-settings-form" method="post" action="options.php">
		<?php
		settings_fields( 'sharedcount_api_options' );
		do_settings_sections( 'wp-sharedcount-api' );
		?>
		<?php submit_button(); ?>
	</form>
</div>
