<?php
if ( ! defined( 'ABSPATH' ) ) {
	header( 'Status: 403 Forbidden' );
	header( 'HTTP/1.1 403 Forbidden' );
	exit;
}

if ( ! class_exists( 'WPR_Run_Rating_Process' ) ) {
	class WPR_Run_Rating_Process extends WP_Background_Process {
		/**
		 * @var string
		 */
		protected $action = 'wpr_fetch_rating';

		/**
		 * Cron_hook_identifier
		 *
		 * @var mixed
		 * @access protected
		 */
		protected $cron_hook_identifier;

		/**
		 * Cron_interval_identifier
		 *
		 * @var mixed
		 * @access protected
		 */
		protected $cron_interval_identifier;

		/**
		 * Initiate new background process
		 */
		public function __construct() {
			parent::__construct();

			$this->cron_hook_identifier     = $this->identifier . '_cron';
			$this->cron_interval_identifier = $this->identifier . '_cron_interval';

			add_action( $this->cron_hook_identifier, array( $this, 'handle_cron_healthcheck' ) );
			add_filter( 'cron_schedules', array( $this, 'schedule_cron_healthcheck' ) );
		}

		/**
		 * Schedule cron healthcheck
		 *
		 * @access public
		 *
		 * @param mixed $schedules Schedules.
		 *
		 * @return mixed
		 */
		public function schedule_cron_healthcheck( $schedules ) {
			$interval = apply_filters( $this->identifier . '_cron_interval', 2 );

			if ( property_exists( $this, 'cron_interval' ) ) {
				$interval = apply_filters( $this->identifier . '_cron_interval', $this->cron_interval_identifier );
			}

			// Adds every 3 minutes to the existing schedules.
			$schedules[ $this->identifier . '_cron_interval' ] = array(
				'interval' => MINUTE_IN_SECONDS * $interval,
				'display'  => sprintf( __( 'Every %d Minutes' ), $interval ),
			);

			return $schedules;
		}

		/**
		 * Add data to cron job
		 *
		 * @param $add_data_to_cron
		 */
		public function wpr_init_cron( $add_data_to_cron ) {
			if ( ! empty( $add_data_to_cron ) ) {
				$this->data( $add_data_to_cron )->save()->dispatch();
			}
		}

		/**
		 * Dispatch
		 *
		 * @access public
		 * @return void
		 */
		public function dispatch() {
			// Schedule the cron healthcheck.
			$this->schedule_event();
		}

		/**
		 * Handle
		 *
		 * Pass each queue item to the task handler, while remaining
		 * within server memory and time limit constraints.
		 */
		protected function handle() {
			$this->lock_process();

			do {
				$batch = $this->get_batch();

				// Process tasks
				foreach ( $batch->data as $key => $value ) {

					if ( 'fetch_urls_batch_week_sharing' == $key && ! empty( $value ) && strlen( $value ) > 6 ) {
						$task = $this->fetch_urls_batch_week_sharing( $value );
					} elseif ( 'fetch_urls_batch_sharing' == $key && ! empty( $value ) && strlen( $value ) > 6 ) {
						$task = $this->fetch_urls_batch_sharing( $value );
					} else {
						$task = $this->task( $value );
					}

					if ( false !== $task ) {
						$batch->data[ $key ] = $value;
					} else {
						unset( $batch->data[ $key ] );
					}

					if ( $this->time_exceeded() || $this->memory_exceeded() ) {
						// Batch limits reached.
						break;
					}
				}

				// Update or delete current batch.
				if ( ! empty( $batch->data ) ) {
					$this->update( $batch->key, $batch->data );
				} else {
					$this->delete( $batch->key );
				}
			} while ( ! $this->time_exceeded() && ! $this->memory_exceeded() && ! $this->is_queue_empty() );

			$this->unlock_process();

			// Start next batch or complete process.
			if ( ! $this->is_queue_empty() ) {
				$this->dispatch();
			} else {
				$this->complete();
			}

			wp_die();
		}

		/**
		 * Fetch batch weekly share for URL's
		 *
		 * @param $item
		 *
		 * @return bool
		 */
		protected function fetch_urls_batch_week_sharing( $item ) {
			$response = true;

			try {
				if ( empty( $item ) ) {
					$response = false;
				} else {
					$brand_id      = $item['brand_id'];
					$bulk_id       = $item['bulk_id'];
					$posts_ratings = new WPR_Posts_Rating();

					$response = $posts_ratings->wpr_get_share_count( 'GET', '', $bulk_id );

					if ( $response ) {
						$total_shares = $posts_ratings->wpr_calculate_total_share_count( $response );
						update_term_meta( $brand_id, 'wpr_weekly_share_count', $total_shares );

						$response = false;
					}
				}

				return $response;
			} catch ( Exception $e ) {
				error_log( 'Cron task encountered an exception: ' );
				error_log( $e );

				// Remove from queue
				return $response;
			}
		}

		/**
		 * Fetch batch for posts URL sharing
		 *
		 * @param $item
		 *
		 * @return bool
		 */
		protected function fetch_urls_batch_sharing( $item ) {
			$response = true;

			try {
				if ( empty( $item ) ) {
					$response = false;
				} else {
					$brand_id      = $item['brand_id'];
					$bulk_id       = $item['bulk_id'];
					$posts_ratings = new WPR_Posts_Rating();

					$response = $posts_ratings->wpr_get_share_count( 'GET', '', $bulk_id );

					if ( $response ) {
						if ( $response['data'] ) {
							foreach ( $response['data'] as $key => $value ) {
								$post_id      = $posts_ratings->wpr_get_id_from_guid( $key );
								$total_shares = $posts_ratings->wpr_calculate_total_share_for_url( $value );
								update_post_meta( $post_id, 'wpr_article_share_count', $total_shares );
							}

							$response = false;
						}
					}
				}


				return $response;
			} catch ( Exception $e ) {
				error_log( 'Cron task encountered an exception: ' );
				error_log( $e );

				// Remove from queue
				return $response;
			}
		}

		/**
		 * Needs to be here
		 *
		 * @param mixed $item
		 */
		protected function task( $item ) {
			return false;
		}

		/**
		 * Handle cron healthcheck
		 *
		 * Restart the background process if not already running
		 * and data exists in the queue.
		 */
		public function handle_cron_healthcheck() {
			if ( $this->is_process_running() ) {
				// Background process already running.
				exit;
			}

			if ( $this->is_queue_empty() ) {
				// No data to process.
				$this->clear_scheduled_event();
				exit;
			}

			$this->handle();

			exit;
		}

		/**
		 * Is queue empty
		 *
		 * @return bool
		 */
		protected function is_queue_empty() {
			global $wpdb;

			$table  = $wpdb->options;
			$column = 'option_name';

			if ( is_multisite() ) {
				$table  = $wpdb->sitemeta;
				$column = 'meta_key';
			}

			$key = $this->identifier . '_batch_%';

			$count = $wpdb->get_var( $wpdb->prepare( "
			SELECT COUNT(*)
			FROM {$table}
			WHERE {$column} LIKE %s
		", $key ) );

			return ( $count > 0 ) ? false : true;
		}

		/**
		 * Schedule event
		 */
		protected function schedule_event() {
			if ( ! wp_next_scheduled( $this->cron_hook_identifier ) ) {
				$time = time() + 20;
				wp_schedule_event( $time, $this->cron_interval_identifier, $this->cron_hook_identifier );
			}
		}

		/**
		 * Complete
		 *
		 * Override if applicable, but ensure that the below actions are
		 * performed, or, call parent::complete().
		 */
		protected function complete() {
			parent::complete();
			// Show notice to user or perform some other arbitrary task...
		}
	}
}

$wpr_request_rating = new WPR_Run_Rating_Process();
