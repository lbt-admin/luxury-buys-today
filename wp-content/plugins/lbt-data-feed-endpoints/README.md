# Posts data feed Rest API endpoints plugin

## Short description
We use this plugin to get all posts via REST API that matchs some parameters.

## REST API endpoint
* {site url}/wp-json/datafeed/v1/posts
* The endpoint wil return only the posts having the post meta beginning with `offer_detail_`
* The response will be cached for 60 seconds

### Params
* `lbt_user` - (required) `string` - default user is `site`
* `lbt_secret` - (required) `string` - user password, default is: `jcF42BshvDUhjXWG4m5ugBb`. Located in the class constant `PRODUCT_ENDPOINT_PASSWORDS`.
* `paged` - (optional) `int` - page to return. If this param is not passed, all posts found will be returned
* `posts_per_page` - (optional) `int` - Posts per page to return. If this param is not passed, the default value is `20`
* `cat` - (optional) `string` - Category `slug` or multiple slugs `slug,slug2`
* `tag` - (optional) `string` - Tags slugs separated by a comma without of any space between. Example: `testtttt,anothertag`. This will display all posts containing any of the tags.
* `name` - (optional) `string` - Post slug to target specific post.

## Example response
```json
{
  "paged": 1,
  "cat": 1,
  "tag": "testtttt,anothertag",
  "post_count": 2,
  "found_posts": 2,
  "posts": {
    "432113": {
      "ID": 432113,
      "slug": "odit-quibusdam-in-amet-voluptatem-rem-temporibus",
      "title": "Odit quibusdam in amet voluptatem rem temporibus",
      "image_src": {
        "full": "http:\/\/feedlbt.local\/wp-content\/uploads\/2021\/01\/2583b844-4b0e-3e6d-9f81-d8a21f0be5ec.png"
      },
      "image_html": "http:\/\/feedlbt.local\/wp-content\/uploads\/2021\/01\/2583b844-4b0e-3e6d-9f81-d8a21f0be5ec.png",
      "content": "<hr \/>\n<ol><li>Animi aut<\/li><li>Ut error omnis eius<\/li><li>Quibusdam ut id consequatur et<\/li><li>Fuga necessitatibus aut reprehenderit maiores<\/li><li>Voluptates molestias vel in enim<\/li><\/ol>\n<h6>A odit delectus est earum libero sed ducimus. Voluptas aut fugiat ab error aut sed dicta. At nihil animi pariatur praesentium veniam rem<\/h6>\n<div class=\"more-link-container\"><a class=\"more-link\" href=\"http:\/\/feedlbt.local\/odit-quibusdam-in-amet-voluptatem-rem-temporibus\/#more-432113\">Continue reading <span class=\"screen-reader-text\">Odit quibusdam in amet voluptatem rem temporibus<\/span><\/a><\/div>",
      "offer_details": []
    }
  }
}
```

Please note: The API will return all meta fields that starts with `offer_detail_`
so you can easily add information and use it in the template files.