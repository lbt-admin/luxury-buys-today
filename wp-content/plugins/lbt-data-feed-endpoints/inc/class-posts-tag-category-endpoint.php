<?php

/**
 * Class LBT_Posts_Tag_Category_Endpoint
 */
class LBT_Posts_Tag_Category_Endpoint {

	/**
	 * Pairs of users and passwords for the rest API $_GET params auth
	 */
	private const PRODUCT_ENDPOINT_PASSWORDS = array(
		'site' => 'jcF42BshvDUhjXWG4m5ugBb',
	);

	/**
	 * Transient expiration in seconds
	 */
	private const TRANSIENT_EXPIRATION_INTERVAL = 60;

	/**
	 * Default posts per page to return when no paged param sent
	 */
	private const POSTS_PER_PAGE_DEFAULT = 20;

	/**
	 * Cache key base
	 *
	 * @var string
	 */
	private $transient_key_base;

	/**
	 * LBT_Posts_Tag_Category_Endpoint constructor.
	 */
	public function __construct() {
		add_action(
			'rest_api_init',
			function () {
				register_rest_route(
					'datafeed/v1',
					'/posts',
					array(
						'methods'             => array( 'GET' ),
						'callback'            => array( $this, 'posts' ),
						'permission_callback' => '__return_true',
						'args'                => array(
							'lbt_user' => array(
								'validate_callback' => function( $param, $request, $key ) {
									return in_array( $param, array_keys( self::PRODUCT_ENDPOINT_PASSWORDS ), true );
								},
							),
							'lbt_secret' => array(
								'validate_callback' => function( $param, $request, $key ) {
									if ( ! isset( self::PRODUCT_ENDPOINT_PASSWORDS[ $request->get_param( 'lbt_user' ) ] ) ) {
										return false;
									}

									return $param === self::PRODUCT_ENDPOINT_PASSWORDS[ $request->get_param( 'lbt_user' ) ];
								},
							),
						),
					)
				);
			}
		);

		$this->transient_key_base = 'lbt_posts_zip_cat_rest_api_response';

		add_action( 'save_post', array( $this, 'clear_cache_if_promotion_post' ), 10, 2 );
	}

	/**
	 * Clear promotion cache if the created / updated post is promotion post
	 *
	 * @param  int     $post_id The post id.
	 * @param  WP_Post $post    The post object.
	 *
	 * @return void
	 */
	public function clear_cache_if_promotion_post( $post_id, $post ) {
		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
			return;
		}

		$post_tags = wp_get_post_tags( $post_id );

		if ( is_wp_error( $post_tags ) ) {
			return;
		}

		$dealership_ids = array();

		foreach ( $post_tags as $tag ) {
			if ( 0 === strpos( $tag->slug, 'dealership-' ) ) {
				$tag_parts = explode( '-', $tag->slug );
				
				if ( 2 === count( $tag_parts ) && is_numeric( $tag_parts[1] ) ) {
					$dealership_ids[] = $tag_parts[1];
				}
			}
		}

		if ( ! empty( $dealership_ids ) ) {
			wp_remote_post(
				'https://luxurybuystoday.com/wp-json/lbt/v1/brands/clear-promotions-cache',
				array(
					'body' => wp_json_encode(
						array(
							'ids' => implode( ',', $dealership_ids ),
						)
					),
				)
			);
		}
	}

	/**
	 * Return all simple and variable products
	 *
	 * @param WP_REST_Request $request
	 *
	 * @return array|array[]|WP_Error
	 */
	public function posts( WP_REST_Request $request ) {

		// Validate params if null if the request has no params it will get up to here.
		if ( ! $request->get_param( 'lbt_user' ) || ! $request->get_param( 'lbt_secret' ) ) {
			return new \WP_Error(
				403,
				esc_html__( 'Access forbidden!', 'mydevices-products-endpoints' )
			);
		}

		$args = array(
			'post_type'              => 'post',
			'post_status'            => 'publish',
			'ignore_sticky_posts'    => true,
			'cache_results'          => true,
			'update_post_meta_cache' => true,
			'update_post_term_cache' => false,
		);

		$transient_key = $this->transient_key_base;
		$data          = array();

		// Paged param.
		if ( isset( $_REQUEST['paged'] ) && ! empty( $_REQUEST['paged'] ) ) {
			$args['paged']          = intval( $_REQUEST['paged'] );
			$args['posts_per_page'] = isset( $_REQUEST['posts_per_page'] ) && ! empty( $_REQUEST['posts_per_page'] ) ? intval( $_REQUEST['posts_per_page'] ) : self::POSTS_PER_PAGE_DEFAULT;

			$data['paged']  = $args['paged'];
			$transient_key .= sprintf( '_%s_%s', $args['paged'], $args['posts_per_page'] );
		} else {
			$args['posts_per_page'] = -1;
			$args['nopaging']       = true;
		}

		// Category param.
		if ( isset( $_REQUEST['cat'] ) && ! empty( $_REQUEST['cat'] ) ) {
			$args['category_name'] = sanitize_text_field( $_REQUEST['cat'] );

			$data['cat']    = $args['category_name'];
			$transient_key .= sprintf( '_%s', $args['category_name'] );
		}

		// Tag param.
		if ( isset( $_REQUEST['tag'] ) && ! empty( $_REQUEST['tag'] ) ) {
			$args['tag'] = sanitize_text_field( $_REQUEST['tag'] );

			$data['tag']    = $args['tag'];
			$transient_key .= sprintf( '_%s', str_replace( ',', '_', $args['tag'] ) );
		}

		// Post id param.
		if ( isset( $_REQUEST['name'] ) && ! empty( $_REQUEST['name'] ) ) {
			$args['name'] = sanitize_text_field( $_REQUEST['name'] );

			$data['post_name']    = $args['name'];
			$transient_key .= sprintf( '_%s', str_replace( ',', '_', $args['name'] ) );
		}

		$cached_data = get_transient( $transient_key );
		if ( isset( $_GET['lbt_clear_endpoint_cache'] ) ) {
			$cached_data = false;
		}
		if ( ! empty( $cached_data ) ) {
			return $cached_data;
		}

		$query = new WP_Query( $args );
		$has_posts = 1;
		if ( ! $query->have_posts() ) {
			$has_posts = 0;
			$args['tag'] = 'no-promotion';
			$query = new WP_Query( $args );
		}
		if ( ! $query->have_posts() ) {
			return array();
		}

		try {

			$data['post_count']  = $query->post_count;
			$data['found_posts'] = $query->found_posts;

			while ( $query->have_posts() ) {
				$query->the_post();
				$post_id = get_the_ID();

				$post_meta = get_post_meta( $post_id );

				$offer_details = array();
				if ( is_array( $post_meta ) && ! empty( $post_meta ) ) {
					foreach ( $post_meta as $key => $value ) {

						if ( false === strpos( $key, 'offer_detail_' ) ) {
							continue;
						}

						$offer_details[ $key ] = $value;
					}
				}
				if ( has_post_thumbnail() ) {
					$img_html = get_the_post_thumbnail_url();
					$img_src = array(
						'full' => get_the_post_thumbnail_url( $post_id, 'full' )
					);
				} else {
					$img_html = get_post_meta( $post_id, 'lbt_img', true ) ? get_post_meta( $post_id, 'lbt_img', true ) : get_post_meta( $post_id, 'lbt_img_1', true );
					$img_src = array(
						'full' => $img_html
					);
				}
				$data['posts'][ $post_id ] = array(
					'ID'            => $post_id,
					'slug'          => get_post_field( 'post_name' ),
					'title'         => get_the_title(),
					'image_src'     => $img_src,
					'image_html'    => $img_html,
					'content'       => get_the_content(),
					'offer_details' => $offer_details,
					'is_dealership' => $has_posts,
				);
			}
			wp_reset_postdata();

			set_transient( $transient_key, $data, self::TRANSIENT_EXPIRATION_INTERVAL );

			return $data;
		} catch ( \Exception $e ) {
			return new \WP_Error(
				500,
				esc_html__( 'Error! Something went wrong', 'mydevices-products-endpoints' )
			);
		}

	}
}
