<?php
/**
 * Plugin Name: Posts data feed Rest API endpoints
 * Plugin URI:
 * Description: Enable posts data feed endpoints via REST API
 * Version: 1.0.0
 *
 * @package WordPress
 */

add_action( 'plugins_loaded', static function() {
	require_once plugin_dir_path( __FILE__ ) . '/inc/class-posts-tag-category-endpoint.php';
	new LBT_Posts_Tag_Category_Endpoint();
} );
