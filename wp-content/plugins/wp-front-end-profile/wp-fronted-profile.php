<?php
/*
  Plugin Name: WP Front End Profile (LBT Custom)
  Description: This plugin allows users to easily edit their profile information on the front end rather than having to go into the dashboard to make changes to password, email address and other user meta data.
  Version:     9.9.9.9
  Author:      Mark Wilkinson
  Author URI:  http://markwilkinson.me
  Text Domain: wpptm
  License:     GPL v2 or later
 */

require_once dirname(__FILE__) . '/functions/scripts.php';
require_once dirname(__FILE__) . '/functions/default-fields.php';
require_once dirname(__FILE__) . '/functions/tabs.php';
require_once dirname(__FILE__) . '/functions/wpfep-functions.php';
require_once dirname(__FILE__) . '/functions/save-fields.php';
require_once dirname(__FILE__) . '/functions/shortcode.php';

/**
 * function wp_frontend_profile_output()
 *
 * provides the front end output for the front end profile editing
 */
function wpfep_show_profile() {
    if (!get_current_user_id()) {
        return;
    }
    ?>
    <div class="wpfep-wrapper">
        <?php
        $wpfep_tabs = apply_filters('wpfep_tabs', array());

        /**
         * @hook wpfep_before_tabs
         * fires before the tabs list items are output
         * @param (array) $tabs is all the tabs that have been added
         * @param (int) $current_user_id the user if of the current user to add things targetted to a specific user only.
         */
        do_action('wpfep_before_tabs', $wpfep_tabs, get_current_user_id());
        ?>

        <ul class="wpfep-tabs" id="wpfep-tabs">
            <?php
            /**
             * set an array of tab titles and ids
             * the id set here should match the id given to the content wrapper
             * which has the class tab-content included in the callback function
             * @hooked wpfep_add_profile_tab - 10
             * @hooked wpfep_add_password_tab - 20
             */
            $wpfep_tabs = apply_filters('wpfep_tabs', array());

            if (!empty($wpfep_tabs)) {

                /* loop through each item */
                foreach ($wpfep_tabs as $wpfep_tab) {

                    /* output the tab name as a tab */
                    wpfep_tab_list_item($wpfep_tab);
                }
            }
            ?>
        </ul>

        <?php
        foreach ($wpfep_tabs as $wpfep_tab) {

            /* build the content class */
            $content_class = '';

            /* if we have a class provided */
            if ('' !== $wpfep_tab['content_class']) {

                /* add the content class to our variable */
                $content_class .= ' ' . $wpfep_tab['content_class'];
            }

            /**
             * @hook wpfep_before_tab_content
             * fires before the contents of the tab are outputted
             * @param (string) $tab_id the id of the tab being displayed. This can be used to target a particular tab.
             * @param (int) $current_user_id the user if of the current user to add things targetted to a specific user only.
             */
            do_action('wpfep_before_tab_content', $wpfep_tab['id'], get_current_user_id());
            ?>

            <div class="tab-content<?php echo esc_attr($content_class); ?>" id="<?php echo esc_attr($wpfep_tab['id']); ?>">
                <?php
                if ($wpfep_tab['id'] == 'offers') {
                    ?>
                    <div class="brand-listings">
                        <?php
                        $categories = array(1, 8, 13, 14, 17, 7);
                        $imagesArray = [36 => '/wp-content/uploads/2019/01/general_auto-1-350x200.jpg',
                            30 => '/wp-content/uploads/2019/01/audi-300x133.jpg',
                            9 => '/wp-content/uploads/2019/01/bmw-300x133.jpg',
                            28 => '/wp-content/uploads/2019/01/cadillac-300x133.jpg',
                            5 => '/wp-content/uploads/2019/01/ferrari-300x133.jpg',
                            33 => '/wp-content/uploads/2019/01/jaguar-300x133.jpg',
                            10 => '/wp-content/uploads/2019/01/lexus-300x133.jpg',
                            18 => '/wp-content/uploads/2019/01/mercedes-300x133.jpg',
                            19 => '/wp-content/uploads/2019/01/bloomingdales-300x133.jpg',
                            20 => '/wp-content/uploads/2019/01/brooks_brothers-300x133.jpg',
                            37 => '/wp-content/uploads/2019/01/general_fashion-1-350x200.jpg',
                            21 => '/wp-content/uploads/2019/01/neiman_marcus-300x133.jpg',
                            22 => '/wp-content/uploads/2019/01/saks-300x133.jpg',
                            23 => '/wp-content/uploads/2019/01/tiffany-300x133.jpg',
                            39 => '/wp-content/uploads/2019/01/general_homedecor-1-350x200.jpg',
                            6 => '/wp-content/uploads/2019/01/ethan_allen-300x133.jpg',
                            24 => '/wp-content/uploads/2019/01/havertys-300x133.jpg',
                            39 => '/wp-content/uploads/2019/01/general_properties-2-350x200.jpg',
                            25 => '/wp-content/uploads/2019/01/christies-300x133.jpg',
                            26 => '/wp-content/uploads/2019/01/sothebys-300x133.jpg',
                            38 => '/wp-content/uploads/2019/01/general_homedecor-1-350x200.jpg',
                            15 => '/wp-content/uploads/2019/01/nordstrom-300x133.jpg'];
                        $all_brands = get_terms('category', array('get' => 'all', 'exclude' => $categories));
                        $selected_brands_raw = get_user_meta(get_current_user_id(), 'wpr_selected_brands');
                        $selected_brands = $selected_brands_raw[0];
                        foreach ($all_brands as $all_brand) {
                            ?> 
                            <div class="brands-listings-item" style="background-image: url(<?php echo $imagesArray[$all_brand->term_id]; ?>)">
                                <div class="brands-overlay">
                                    <div class="title-container">
                                        <span class="ubermenu-target-title ubermenu-target-text"><?php echo $all_brand->name; ?></span>
                                    </div>
                                    <div class="image-container">
                                        <?php
                                        if (in_array($all_brand->term_id, $selected_brands)) {
                                            echo '<img data-id="' . $all_brand->term_id . '" src="/wp-content/themes/lbt-gaia-child/assets/images/add_to_lookbook_red.svg" data-postid="69767" title="Remove From Lookbook" class="brand-button selected-brand">';
                                        } else {
                                            echo '<img data-id="' . $all_brand->term_id . '" src="/wp-content/themes/lbt-gaia-child/assets/images/add_to_lookbook_1.svg" data-postid="69777"  title="Add To Lookbook" class="brand-button">';
                                        }
                                        ?>
                                    </div>
                                </div> 
                            </div><?php
                        }
                        ?>
                    </div>
                    <?php
                } else {
                    ?>
                    <form method="post" action="#" class="wpfep-form-<?php echo esc_attr($wpfep_tab['id']); ?>">

                        <?php
                        /* check if callback function exists */
                        if (function_exists($wpfep_tab['callback'])) {

                            /* use custom callback function */
                            $wpfep_tab['callback']($wpfep_tab);

                            /* custom callback does not exist */
                        } else {

                            /* use default callback function */
                            wpfep_default_tab_content($wpfep_tab);
                        }
                        ?>

                        <?php
                        wp_nonce_field(
                                'wpfep_nonce_action',
                                'wpfep_nonce_name'
                        );
                        ?>

                    </form>
                <?php } ?>

            </div>

            <?php
            /**
             * @hook wpfep_after_tab_content
             * fires after the contents of the tab are outputted
             * @param (string) $tab_id the id of the tab being displayed. This can be used to target a particular tab.
             * @param (int) $current_user_id the user if of the current user to add things targetted to a specific user only.
             */
            do_action('wpfep_after_tab_content', $wpfep_tab['id'], get_current_user_id());
        } // end tabs loop
        ?>
    </div><!-- // wpfep-wrapper -->
    <?php
}
