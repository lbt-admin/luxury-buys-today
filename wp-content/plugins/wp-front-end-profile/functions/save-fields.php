<?php
/**
 * function wpfep_save_fields()
 * saves the fields from a tab (except password tab) to user meta
 * @param (array) $tabs is an array of all of the current tabs
 * @param (int) $user_id is the current logged in users id
 */
function wpfep_save_fields( $tabs, $user_id ) {
	if ( ! isset( $_POST['wpfep_nonce_name'] ) || ! wp_verify_nonce( $_POST['wpfep_nonce_name'], 'wpfep_nonce_action' ) ) {
		return;
	}

	/* set an array to store messages in */
	$messages = array();

	/* get the POST data */
	$tabs_data = $_POST;

	/**
	 * remove the following array elements from the data
	 * password
	 * nonce name
	 * wp refere - sent with nonce
	 */
	unset( $tabs_data['password'] );
	unset( $tabs_data['wpfep_nonce_name'] );
	unset( $tabs_data['_wp_http_referer'] );

	/* lets check we have some data to save */
	if ( empty( $tabs_data ) ) {
		return;
	}

	$reserved_ids = apply_filters(
		'wpfep_reserved_ids',
		array(
			'user_email',
			'user_url',
		)
	);

	/**
	 * set an array of registered fields
	 */
	$registered_fields = array();
	foreach ( $tabs as $tab ) {
		$tab_fields = apply_filters(
			'wpfep_fields_' . $tab['id'],
			array(),
			$user_id
		);
		$registered_fields = array_merge( $registered_fields, $tab_fields );
	}

	/* set an array of registered keys */
	$registered_keys = wp_list_pluck( $registered_fields, 'id' );

	// Required fields.
	$required_fields = array(
		'user_email' => __( 'Email Address' ),
		'first_name' => __( 'First Name' ),
		'last_name'  => __( 'Last Name' ),
		'zip_code'   => __( 'Zip Code' ),
		'phone'      => __( 'Cell Phone' ),
	);

	foreach ( $tabs_data as $tab_data ) {
		foreach ( $tab_data as $key => $value ) {
			/* if the key is the save sumbit - move to next in array */
			if ( in_array( $key, array( 'wpfep_save', 'wpfep_nonce_action' ) ) ) {
				continue;
			}

			/* if the key is not in our list of registered keys - move to next in array */
			if ( ! in_array( $key, $registered_keys ) ) {
				continue;
			}

			// Check that required fields have a value.
			if ( array_key_exists( $key, $required_fields ) && empty( trim( $value ) ) ) {
				/* translators: %s: label of the required field */
				$messages[ $key . '_required' ] = '<p class="error">' . sprintf( __( '%s is required!' ), $required_fields[ $key ] ) . '</p>';
				continue;
			}

			// Additional validation for phone number.
			if ( 'phone' === $key && ! preg_match( '/^\(?([1]-)?\(?[0-9]{3}\)?\s?-?[0-9]{3}-?[0-9]{4}$/', $value ) ) {
				$messages['phone_invalid'] = '<p class="error">' . __( 'Phone Number must be in the (XXX) XXX-XXX format.' ) . '</p>';
				continue;
			}

			/* check whether the key is reserved - handled with wp_update_user */
			if ( in_array( $key, $reserved_ids ) ) {
				$user_id = wp_update_user(
					array(
						'ID' => $user_id,
						$key => $value,
					)
				);

				if ( is_wp_error( $user_id ) ) {
					$messages[ $user_id->get_error_code() ] = '<p class="error">' . $user_id->get_error_message() . '</p>';
				}
			} else {
				/* just standard user meta - handle with update_user_meta */
				/* lookup field options by key */
				$registered_field_key = array_search( $key, array_column( $registered_fields, 'id' ) );

				/* sanitize user input based on field type */
				switch ( $registered_fields[ $registered_field_key ]['type'] ) {
					case 'wysiwyg':
						$value = wp_filter_post_kses( $value );
						break;
					case 'select':
						$value = sanitize_text_field( $value );
						break;
					case 'textarea':
						$value = wp_filter_nohtml_kses( $value );
						break;
					case 'checkbox':
						$value = isset( $value ) && '1' === $value ? true : false;
						break;
					case 'email':
						$value = sanitize_email( $value );
						break;
					default:
						$value = sanitize_text_field( $value );
				}

				// Format phone number.
				if ( 'phone' === $key ) {
					$value = preg_replace( '~.*(\d{3})[^\d]{0,7}(\d{3})[^\d]{0,7}(\d{4}).*~', '($1) $2-$3', $value );
				}

				$meta = update_user_meta( $user_id, $key, $value );
			}
		}
	}

	echo '<div class="messages">';
	if ( ! empty( $messages ) ) {
		foreach ( $messages as $message ) {
			echo $message;
		}
	} else {
		echo '<p class="updated">' . __( 'Your profile was updated successfully!' ) . '</p>';
	}
	echo '</div>';
}
add_action( 'wpfep_before_tabs', 'wpfep_save_fields', 5, 2 );

/**
 * function wpfep_save_password()
 * saves the change of password on the profile password tab
 * check for length (filterable with wpfep_password_length) and complexity (upper/lower/numbers)
 * user is logged out on success with a message to login back in with new password.
 *
 * @param (array) $tabs is an array of all of the current tabs
 * @param (int) $user_id is the current logged in users id
 */
function wpfep_save_password( $tabs, $user_id ) {
	
	/* set an array to store messages in */
	$messages = array();
	
	/* get the posted data from the password tab */
	$data = $_POST[ 'password' ];
	
	/* store both password for ease of access */
	$password = $data[ 'user_pass' ];
	$password_check = $data[ 'user_pass_check' ];
	
	/* first lets check we have a password added to save */
	if( empty( $password ) )
		return;
	
	/* now lets check the password match */
	if( $password != $password_check ) {
		
		/* add message indicating no match */
		$messages[ 'password_mismatch' ] = '<p class="error">' . __( 'Please make sure the passwords match.' ) . '</p>';
		
	}
	
	/* get the length of the password entered */
	$pass_length = strlen( $password );
	$min_length  = apply_filters( 'wpfep_password_length', 6 );
	
	/* check the password match the correct length */
	if ( $pass_length < $min_length ) {
		
		/* translators: %d: minimum password length */
		$messages[ 'password_length' ] = '<p class="error">' . sprintf( __( 'Please make sure your password is at least %d characters long.' ), $min_length ) . '</p>';
	}
	
	/**
	 * match the password against a regex of complexity
	 * at least 1 upper, 1 lower case letter and 1 number
	 */
	$pass_complexity = preg_match( apply_filters( 'wpfep_password_regex', '/^(?=.*[a-z])(?=.*[A-Z])(?=.*[\d,.;:]).+$/' ), $password );
	
	/* check whether the password passed the regex check of complexity */
	if( $pass_complexity == false ) {
		
		/* add message indicating complexity issue */
		$messages[ 'password_complexity' ] = '<p class="error">' . __( 'Your password must contain at least 1 uppercase, 1 lowercase letter and at least 1 number.' ) . '</p>';
		
	}
	
	/* check we have any messages in the messages array - if we have password failed at some point */
	if( empty( $messages ) ) {
		
		/**
		 * ok if we get this far we have passed all the checks above
		 * the password can now be updated and redirect the user to the login page
		 */
		
		wp_set_password( $password, $user_id );
		/* translators: %1$s: beginning link tag, %2$s: ending link tag */
		echo '<div class="messages"><p class="updated">' . sprintf( __( 'Your password was successfully changed and you have been logged out. Please %1$slogin again here%2$s.' ), '<a href="' . esc_url( get_permalink( get_page_by_path( 'login' ) ) ) . '">', '</a>' ) . '</p></div>';
	
	/* messages not empty therefore password failed */
	} else {
		
		?>
		<div class="messages">
		<?php
		
		/* lets loop through the messages stored */
		foreach( $messages as $message ) {
			
			/* output the message */
			echo $message;
			
		}
		
		?>
		</div><!-- // messages -->
		<?php
		
	}
	
}

add_action( 'wpfep_before_tabs', 'wpfep_save_password', 10, 2 );