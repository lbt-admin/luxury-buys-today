<?php
/*
Plugin Name:  WPRiders Filter Restrictions
Plugin URI:   https://www.wpriders.com/hire-us-ref
Description:  This plugin is adding restrictions on the Brands filters in order to check if al least on option is selected
Author:   	Alex Bordei from WPRiders
Author URI:   https://www.wpriders.com/hire-us-ref
License:  	GPL2
License URI:  https://www.gnu.org/licenses/gpl-2.0.html
*/

if (!defined('ABSPATH')) {
    die('You are not allowed to call this page directly.');
}

if (!class_exists('WPR_Filter_Restrictions')) {
    /**
     * Class WPR_Filter_Restrictions
     */
    class WPR_Filter_Restrictions
    {
        function __construct()
        {
            add_action('wp_enqueue_scripts', array(&$this, 'wpr_enqueue_scripts'));
        }

        /**
         * Enqueue scripts
         */
        function wpr_enqueue_scripts()
        {
            if (is_front_page() || is_home() || is_category() || is_single()) {

                if (wp_style_is('sweetalert2', $list = 'enqueued')) {
                    wp_enqueue_style('sweetalert2', plugin_dir_url(__FILE__) . '/assets/css/sweetalert2.min.css');
                }
                if (wp_script_is('sweetalert2', $list = 'enqueued')) {
                    wp_enqueue_script('sweetalert2_js', plugin_dir_url(__FILE__) . '/assets/js/sweetalert2.min.js', array('jquery'), '1.0.0', true);
                }
                if (wp_style_is('sweetalert2.custom', $list = 'enqueued')) {
                    wp_enqueue_style('sweetalert2.custom', plugin_dir_url(__FILE__) . '/assets/css/sweetalert2.custom.css');
                }
                wp_enqueue_script('wpr_filter_restrictions', plugin_dir_url(__FILE__) . '/assets/js/wpr_filter_restrictons.js', array('jquery'), '1.0.0', true);
            }
        }
    }
}
$wpr_filter_restrictions = new WPR_Filter_Restrictions();